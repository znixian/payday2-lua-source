#!/bin/bash

mkdir extract
echo "Please unpack your game bundles into the 'extract' directory"

# Download the Lua decompiler
git clone -b ljv2.1 https://github.com/jjdredd/ljd.git

# Make the decompiler executable
chmod +x ljd/main.py

# Compile the decryptor
g++ lua_decrypt.cpp -o uncrypt

# Compile the IDString tool
gcc idstring_hash.c -o idstring

