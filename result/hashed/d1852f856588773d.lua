ArmorSkinEditor = ArmorSkinEditor or class()
ArmorSkinEditor.allowed_extensions = SkinEditor.allowed_extensions
ArmorSkinEditor.menu_node_name = "armor_skin_editor"
ArmorSkinEditor.texture_types = {
	"base_gradient",
	"pattern_gradient",
	"pattern",
	"sticker"
}
ArmorSkinEditor.init = function (self)
	Global.armor_skin_editor = {
		skins = {}
	}
	self._global = Global.armor_skin_editor
	self._current_skin = 1
	self._active = false

	return 
end
ArmorSkinEditor.active = function (self)
	return self._active
end
ArmorSkinEditor.set_active = function (self, active)
	self._active = active

	return 
end
ArmorSkinEditor.init_items = function (self)
	self._global.skins = {}
	self._current_skin = 1

	for _, item in ipairs(managers.workshop:items()) do
		if item.config(item).name and item.config(item).data and item.config(item).type == "armor_skin" then
			self.add_literal_paths(self, item)
			self._append_skin(self, item)
			self.load_textures(self, item)
		end
	end

	return 
end
ArmorSkinEditor.create_new_skin = function (self, data)
	local local_skin_id = #self._global.skins + 1
	local new_skin = managers.workshop:create_item()

	self._append_skin(self, new_skin)
	self.save_skin(self, new_skin, "New Skin " .. local_skin_id, data)
	self.setup_texture_folders(self, new_skin)

	return local_skin_id
end
ArmorSkinEditor._append_skin = function (self, skin)
	self._global.skins = self._global.skins or {}

	table.insert(self._global.skins, skin)

	return 
end
ArmorSkinEditor.delete_current = function (self)
	local skin = self.get_current_skin(self)

	if not skin then
		return 
	end

	table.remove(self._global.skins, self._current_skin)
	managers.workshop:delete_item(skin)

	self._current_skin = math.max(self._current_skin - 1, 1)

	self.reload_current_skin(self)
	managers.menu:active_menu().logic:get_node(ArmorSkinEditor.menu_node_name)

	return 
end
ArmorSkinEditor.select_skin = function (self, local_skin_id)
	local is_reload = self._current_skin == local_skin_id
	self._current_skin = local_skin_id
	local skin = self.get_current_skin(self)

	if not skin then
		self.apply_changes_to_character(self, {})

		return 
	end

	if not skin.config(skin).data then
		skin.config(skin).data = {}
	end

	local new_cosmetics_data = deep_clone(skin.config(skin).data)
	new_cosmetics_data.id = skin.config(skin).data.name_id or "skin_default"

	local function cb()
		managers.menu_scene._disable_item_updates = false

		managers.menu_scene:update(TimerManager:main():time(), TimerManager:main():delta_time())

		managers.menu_scene._disable_item_updates = true
		local unsaved = self._unsaved

		self:apply_changes(new_cosmetics_data)

		self._unsaved = (is_reload and unsaved) or false

		return 
	end

	managers.menu:active_menu().logic:get_node(ArmorSkinEditor.menu_node_name)
	self.apply_changes_to_character(self, new_cosmetics_data)

	return 
end
ArmorSkinEditor.reload_current_skin = function (self)
	self.select_skin(self, self._current_skin)

	return 
end
ArmorSkinEditor.save_skin = function (self, skin, name, data)
	skin.config(skin).name = name or skin.config(skin).name
	skin.config(skin).data = data or skin.config(skin).data
	skin.config(skin).type = "armor_skin"
	local tags = self.get_current_item_tags(self)

	skin.clear_tags(skin)

	for _, tag in ipairs(tags) do
		skin.add_tag(skin, tag)
	end

	local original = self.remove_literal_paths(self, skin)

	skin.save(skin)

	skin.config(skin).data = original
	self._unsaved = false

	return 
end
ArmorSkinEditor.save_current_skin = function (self, name, data)
	self.save_skin(self, self.get_current_skin(self), name, data)

	return 
end
ArmorSkinEditor.skins = function (self)
	return self._global.skins
end
ArmorSkinEditor.skin_count = function (self)
	return #self._global.skins
end
ArmorSkinEditor.get_skin = function (self, local_skin_id)
	self._global.skins = self._global.skins or {}

	return self._global.skins[local_skin_id]
end
ArmorSkinEditor.get_current_skin = function (self)
	return self.get_skin(self, self._current_skin)
end
ArmorSkinEditor.unsaved = function (self)
	return self._unsaved and not self._ignore_unsaved
end
ArmorSkinEditor.set_ignore_unsaved = function (self, ignore)
	self._ignore_unsaved = ignore

	return 
end
ArmorSkinEditor.get_texture_list = SkinEditor.get_texture_list
ArmorSkinEditor.get_texture_list_by_type = SkinEditor.get_texture_list_by_type
ArmorSkinEditor.load_textures = SkinEditor.load_textures
ArmorSkinEditor._load_textures_by_types = SkinEditor._load_textures_by_types
ArmorSkinEditor.get_texture_path_by_type = SkinEditor.get_texture_path_by_type
ArmorSkinEditor.get_texture_string = SkinEditor.get_texture_string
ArmorSkinEditor.get_texture_idstring = SkinEditor.get_texture_idstring
ArmorSkinEditor.check_texture_db = SkinEditor.check_texture_db
ArmorSkinEditor.check_texture_disk = SkinEditor.check_texture_disk
ArmorSkinEditor.check_texture = SkinEditor.check_texture
ArmorSkinEditor.apply_changes = function (self, cosmetics_data)
	local skin = self.get_current_skin(self)

	if cosmetics_data then
		self._unsaved = true
		skin.config(skin).data = cosmetics_data
	end

	local textures = self.get_all_applied_textures(self, skin)

	for _, texture in ipairs(textures) do
		local texture_string = self.get_texture_string(self, skin, texture.name, texture.type)

		if not self.check_texture(self, texture_string) then
			self.remove_texture_by_name(self, skin, texture.name)
		end
	end

	self.apply_changes_to_character(self, cosmetics_data)

	return 
end
ArmorSkinEditor.apply_changes_to_character = function (self, data)
	local character = managers.menu_scene._character_unit

	if character then
		character.base(character)._cosmetics_data = data

		character.base(character):_apply_cosmetics({})
	end

	return 
end
ArmorSkinEditor.remove_texture_by_name = SkinEditor.remove_texture_by_name
ArmorSkinEditor.get_all_applied_textures = SkinEditor.get_all_applied_textures
ArmorSkinEditor.remove_literal_paths = SkinEditor.remove_literal_paths
ArmorSkinEditor.add_literal_paths = SkinEditor.add_literal_paths
ArmorSkinEditor.setup_texture_folders = SkinEditor.setup_texture_folders
ArmorSkinEditor.has_texture_folders = SkinEditor.has_texture_folders
ArmorSkinEditor.get_texture_types = function (self)
	return ArmorSkinEditor.texture_types
end
ArmorSkinEditor.clear_current_skin = function (self)
	local skin = self.get_current_skin(self)

	if skin then
		skin.config(skin).data = {}
	end

	self.reload_current_skin(self)

	return 
end
ArmorSkinEditor.get_current_item_tags = function (self)
	local tags = {}

	table.insert(tags, "Armor")

	return tags
end
ArmorSkinEditor.hide_screenshot_bg = SkinEditor.hide_screenshot_bg
ArmorSkinEditor.leave_screenshot_mode = SkinEditor.leave_screenshot_mode
ArmorSkinEditor.get_screenshot_name = SkinEditor.get_screenshot_name
ArmorSkinEditor.get_screenshot_rect = SkinEditor.get_screenshot_rect
ArmorSkinEditor.has_screenshots = SkinEditor.has_screenshots
ArmorSkinEditor.get_screenshot_path = SkinEditor.get_screenshot_path
ArmorSkinEditor.get_screenshot_list = SkinEditor.get_screenshot_list
ArmorSkinEditor.enter_screenshot_mode = function (self)
	local vp = managers.environment_controller._vp:vp()
	self._old_bloom_setting = vp.get_post_processor_effect_name(vp, "World", Idstring("bloom_combine_post_processor"))

	vp.set_post_processor_effect(vp, "World", Idstring("bloom_combine_post_processor"), Idstring("bloom_combine_empty"))
	World:effect_manager():set_rendering_enabled(false)

	managers.menu_scene._disable_item_updates = false

	managers.menu_scene:update(TimerManager:main():time(), TimerManager:main():delta_time())

	managers.menu_scene._disable_item_updates = true
	local unsaved = self._unsaved

	self.apply_changes(self)

	self._unsaved = unsaved

	if not alive(self._screenshot_ws) then
		self._spawn_screenshot_background(self)
	end

	return 
end
ArmorSkinEditor._spawn_screenshot_background = function (self)
	managers.menu_scene._bg_unit:set_visible(false)

	local unit = managers.menu_scene._character_unit
	local gui = World:newgui()
	local offset_x = Vector3(0, 600, 0):rotate_with(unit.rotation(unit))
	local offset_y = Vector3(0, 0, 500):rotate_with(unit.rotation(unit))
	local pos_offset = Vector3(0, 450, 100):rotate_with(unit.rotation(unit))
	self._screenshot_ws = gui.create_world_workspace(gui, 1000, 1000, unit.position(unit) - pos_offset, offset_x, offset_y)

	self._screenshot_ws:panel():rect({
		name = "bg",
		layer = 20000,
		color = Color(0, 1, 0)
	})
	self._screenshot_ws:set_billboard(Workspace.BILLBOARD_BOTH)
	self.hide_screenshot_bg(self)

	return 
end
ArmorSkinEditor.set_screenshot_color = function (self, color)
	managers.menu_scene._bg_unit:set_visible(false)
	self._screenshot_ws:show()
	self._screenshot_ws:panel():child("bg"):set_color(color)
	managers.menu_scene:delete_workbench_room()

	return 
end
ArmorSkinEditor.hide_screenshot_bg = function (self)
	managers.menu_scene._bg_unit:set_visible(true)
	managers.menu_scene:spawn_workbench_room()
	self._screenshot_ws:hide()

	return 
end
ArmorSkinEditor.publish_skin = function (self, skin, title, desc, changelog, callb)
	if skin.is_submitting(skin) then
		return 
	end

	local function cb(result)
		if result == "success" then
			local id = managers.blackmarket:skin_editor():get_current_skin():id()

			if id then
				Steam:overlay_activate("url", "steam://url/CommunityFilePage/" .. id)
			end
		else
			local dialog_data = {
				title = managers.localization:text("dialog_error_title")
			}
			local result_text = (managers.localization:exists(result) and managers.localization:text(result)) or result
			dialog_data.text = managers.localization:text("debug_wskn_submit_failed") .. "\n" .. result_text
			local ok_button = {
				text = managers.localization:text("dialog_ok")
			}
			dialog_data.button_list = {
				ok_button
			}

			managers.system_menu:show(dialog_data)
		end

		if SystemFS:exists(Application:nice_path(skin:staging_path(), false)) and Application:nice_path(skin:staging_path(), false) ~= Application:nice_path(skin:path(), false) then
			SystemFS:delete_file(Application:nice_path(skin:staging_path(), false))
		end

		if SystemFS:exists(Application:nice_path(skin:path(), true) .. "preview.png") then
			SystemFS:delete_file(Application:nice_path(skin:path(), true) .. "preview.png")
		end

		if callb then
			callb(result)
		end

		if self._publish_bar then
			self._publish_bar:remove()

			self._publish_bar = nil
		end

		return 
	end

	skin.set_title(skin, title)
	skin.set_description(skin, desc)
	self.save_skin(self, skin)

	local staging = managers.workshop:create_staging_directory()

	for _, type in ipairs(self.get_texture_types(self)) do
		if not SystemFS:exists(Application:nice_path(staging, true) .. type) then
			SystemFS:make_dir(Application:nice_path(staging, true) .. type)
		end
	end

	local textures = self.get_all_applied_textures(self, skin)
	local files = {}

	for _, texture in ipairs(textures) do
		local path = texture.type .. "/" .. texture.name

		table.insert(files, path)
	end

	table.insert(files, "info.xml")
	table.insert(files, "item.xml")
	table.insert(files, "preview.png")

	local copy_data = {}

	for _, file in ipairs(files) do
		local pair = {}

		table.insert(pair, Application:nice_path(skin.path(skin), true) .. file)
		table.insert(pair, Application:nice_path(staging, true) .. file)
		table.insert(copy_data, pair)
	end

	local function copy_cb(success, message)
		if success then
			if skin:submit(changelog, cb) then
				local bar_radius = 20
				local panel = managers.menu:active_menu().renderer.ws:panel()
				self._publish_bar = CircleBitmapGuiObject:new(panel, {
					use_bg = true,
					current = 0,
					blend_mode = "add",
					layer = 2,
					radius = bar_radius,
					sides = bar_radius,
					total = bar_radius,
					color = Color.white:with_alpha(1)
				})

				self._publish_bar:set_position(0, panel.h(panel) - bar_radius*2)

				local function update_publish(o)
					local current = 0
					local skin_editor = managers.blackmarket:skin_editor()

					while current < 1 do
						local bar = skin_editor._publish_bar

						if not bar then
							break
						end

						current = current + skin_editor.get_current_skin(skin_editor):get_update_progress()

						bar.set_current(bar, current)
						coroutine.yield()
					end

					return 
				end

				panel.animate(panel, update_publish)
			end
		else
			cb("copy_failed:" .. message)
		end

		return 
	end

	local function sub(result)
		if result == "success" then
			skin:set_staging_path(staging)
			SystemFS:copy_files_async(copy_data, copy_cb)
		else
			cb(result)
		end

		return 
	end

	if not skin.item_exists(skin) then
		skin.prepare_for_submit(skin, sub)
	else
		sub("success")
	end

	return 
end

return 
