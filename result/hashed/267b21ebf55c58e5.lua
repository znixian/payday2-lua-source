require("lib/input/HandState")

EmptyHandState = EmptyHandState or class(HandState)
EmptyHandState.init = function (self)
	EmptyHandState.super.init(self)

	self._connections = {
		warp_right = {
			hand = 1,
			inputs = {
				"trackpad_button_"
			}
		},
		warp_left = {
			hand = 2,
			inputs = {
				"trackpad_button_"
			}
		},
		touchpad_warp_target = {
			inputs = {
				"dpad_"
			}
		},
		interact_right = {
			hand = 1,
			inputs = {
				"grip_"
			}
		},
		interact_left = {
			hand = 2,
			inputs = {
				"grip_"
			}
		},
		automove = {
			inputs = {
				"trigger_"
			}
		}
	}

	return 
end
PointHandState = PointHandState or class(HandState)
PointHandState.init = function (self)
	PointHandState.super.init(self)

	self._connections = {
		warp_right = {
			hand = 1,
			inputs = {
				"trackpad_button_"
			}
		},
		warp_left = {
			hand = 2,
			inputs = {
				"trackpad_button_"
			}
		},
		touchpad_warp_target = {
			inputs = {
				"dpad_"
			}
		},
		automove = {
			inputs = {
				"trigger_"
			}
		}
	}

	return 
end
WeaponHandState = WeaponHandState or class(HandState)
WeaponHandState.init = function (self)
	WeaponHandState.super.init(self)

	self._connections = {
		primary_attack = {
			inputs = {
				"trigger_"
			}
		},
		reload = {
			inputs = {
				"grip_"
			}
		},
		switch_hands = {
			inputs = {
				"d_up_"
			}
		},
		weapon_firemode = {
			inputs = {
				"d_left_"
			}
		},
		weapon_gadget = {
			inputs = {
				"d_right_"
			}
		},
		menu_snap = {
			inputs = {
				"d_down_"
			}
		},
		touchpad_primary = {
			inputs = {
				"dpad_"
			}
		}
	}

	return 
end
AkimboHandState = AkimboHandState or class(HandState)
AkimboHandState.init = function (self)
	AkimboHandState.super.init(self, 1)

	self._connections = {
		akimbo_fire = {
			inputs = {
				"trigger_"
			}
		}
	}

	return 
end
MaskHandState = MaskHandState or class(HandState)
MaskHandState.init = function (self)
	MaskHandState.super.init(self)

	self._connections = {
		use_item = {
			inputs = {
				"trigger_"
			}
		}
	}

	return 
end
ItemHandState = ItemHandState or class(HandState)
ItemHandState.init = function (self)
	ItemHandState.super.init(self, 1)

	self._connections = {
		use_item_vr = {
			inputs = {
				"trigger_"
			}
		},
		unequip = {
			inputs = {
				"grip_"
			}
		}
	}

	return 
end
AbilityHandState = AbilityHandState or class(HandState)
AbilityHandState.init = function (self)
	AbilityHandState.super.init(self, 2)

	self._connections = {
		throw_grenade = {
			inputs = {
				"grip_"
			}
		}
	}

	return 
end
EquipmentHandState = EquipmentHandState or class(HandState)
EquipmentHandState.init = function (self)
	EquipmentHandState.super.init(self, 1)

	self._connections = {
		use_item = {
			inputs = {
				"trigger_"
			}
		},
		unequip = {
			inputs = {
				"grip_"
			}
		}
	}

	return 
end
TabletHandState = TabletHandState or class(HandState)
TabletHandState.init = function (self)
	TabletHandState.super.init(self)

	return 
end
BeltHandState = BeltHandState or class(HandState)
BeltHandState.init = function (self)
	BeltHandState.super.init(self, 1)

	self._connections = {
		belt_right = {
			hand = 1,
			inputs = {
				"grip_"
			}
		},
		belt_left = {
			hand = 2,
			inputs = {
				"grip_"
			}
		},
		disabled = {
			inputs = {
				"trigger_"
			}
		}
	}

	return 
end
RepeaterHandState = RepeaterHandState or class(HandState)
RepeaterHandState.init = function (self)
	RepeaterHandState.super.init(self, 2)

	return 
end
DrivingHandState = DrivingHandState or class(HandState)
DrivingHandState.init = function (self)
	DrivingHandState.super.init(self)

	self._connections = {
		hand_brake = {
			hand = 2,
			inputs = {
				"trackpad_button_"
			}
		},
		interact_right = {
			hand = 1,
			inputs = {
				"grip_"
			}
		},
		interact_left = {
			hand = 2,
			inputs = {
				"grip_"
			}
		}
	}

	return 
end

return 
