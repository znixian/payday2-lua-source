core:import("CoreMenuItem")

MenuItemDummy = MenuItemDummy or class(CoreMenuItem.Item)
MenuItemDummy.TYPE = "dummy"
MenuItemDummy.init = function (self, data_node, parameters)
	CoreMenuItem.Item.init(self, data_node, parameters)

	self._type = MenuItemDummy.TYPE
	self.hide_highlight = true

	return 
end
MenuItemDummy.setup_gui = function (self, node, row_item)
	row_item.gui_panel = node.item_panel:panel({
		w = 0,
		h = 0
	})

	row_item.gui_panel:set_left(node._mid_align(node))

	self.no_select = true
	self.no_mouse_select = true
	self._highlighted = false

	return true
end
MenuItemDummy.trigger = function (self)
	MenuItemDummy.super.trigger(self)

	if self._actual_item and self._actual_item.dummy_trigger then
		self._actual_item:dummy_trigger()
	end

	return 
end
MenuItemDummy.set_actual_item = function (self, item)
	self._actual_item = item
	self.no_select = nil

	if self._actual_item.dummy_set_highlight then
		self._actual_item:dummy_set_highlight(false)
	end

	return 
end
MenuItemDummy.highlight_row_item = function (self, node, row_item, mouse_over)
	self._highlighted = true

	if self._actual_item and self._actual_item.dummy_set_highlight then
		self._actual_item:dummy_set_highlight(true, node, row_item, mouse_over)
	end

	return true
end
MenuItemDummy.fade_row_item = function (self, node, row_item, mouse_over)
	self._highlighted = false

	if self._actual_item and self._actual_item.dummy_set_highlight then
		self._actual_item:dummy_set_highlight(false, node, row_item, mouse_over)
	end

	return true
end
MenuItemDummy.is_highlighted = function (self)
	return self._highlighted
end
MenuItemDummy.reload = function (self)
	return true
end
MenuItemDummy.menu_unselected_visible = function (self)
	return false
end

return 
