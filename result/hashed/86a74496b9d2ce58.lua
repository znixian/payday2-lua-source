GenericSideJobsManager = GenericSideJobsManager or class()
GenericSideJobsManager.init = function (self)
	self._side_jobs = {}

	return 
end
GenericSideJobsManager.register = function (self, manager)
	if table.find_value(self._side_jobs, function (v)
		return v.manager == manager
	end) then
		return 
	end

	table.insert(self._side_jobs, {
		manager = manager
	})

	return 
end
GenericSideJobsManager.side_jobs = function (self)
	return self._side_jobs
end
GenericSideJobsManager.get_challenge = function (self, id)
	for _, side_job_dlc in ipairs(self._side_jobs) do
		local challenge = side_job_dlc.manager:get_challenge(id)

		if challenge then
			return challenge
		end
	end

	return 
end
GenericSideJobsManager.has_completed_and_claimed_rewards = function (self, id)
	for _, side_job_dlc in ipairs(self._side_jobs) do
		local challenge = side_job_dlc.manager:get_challenge(id)

		if challenge then
			return side_job_dlc.manager:has_completed_and_claimed_rewards(id)
		end
	end

	return false
end
GenericSideJobsManager.award = function (self, id)
	for _, side_job_dlc in ipairs(self._side_jobs) do
		side_job_dlc.manager:award(id)
	end

	return 
end
GenericSideJobsManager.save = function (self, cache)
	for _, side_job_dlc in ipairs(self._side_jobs) do
		side_job_dlc.manager:save(cache)
	end

	return 
end
GenericSideJobsManager.load = function (self, cache, version)
	for _, side_job_dlc in ipairs(self._side_jobs) do
		side_job_dlc.manager:load(cache, version)
	end

	return 
end
GenericSideJobsManager.reset = function (self)
	local list = table.list_copy(self._side_jobs)

	for _, side_job_dlc in ipairs(list) do
		side_job_dlc.manager:reset()
	end

	return 
end

return 
