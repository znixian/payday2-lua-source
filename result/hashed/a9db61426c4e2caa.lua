require("lib/modifiers/BaseModifier")
require("lib/modifiers/ModifierCivilianAlarm")
require("lib/modifiers/ModifierCloakerKick")
require("lib/modifiers/ModifierEnemyDamage")
require("lib/modifiers/ModifierEnemyHealth")
require("lib/modifiers/ModifierEnemyHealthAndDamage")
require("lib/modifiers/ModifierExplosionImmunity")
require("lib/modifiers/ModifierHeavies")
require("lib/modifiers/ModifierLessConcealment")
require("lib/modifiers/ModifierLessPagers")
require("lib/modifiers/ModifierMoreSpecials")
require("lib/modifiers/ModifierMoreDozers")
require("lib/modifiers/ModifierNoHurtAnims")
require("lib/modifiers/ModifierShieldReflect")
require("lib/modifiers/ModifierSkulldozers")
require("lib/modifiers/ModifierHealSpeed")
require("lib/modifiers/ModifierMoreMedics")
require("lib/modifiers/ModifierAssaultExtender")
require("lib/modifiers/ModifierCloakerArrest")
require("lib/modifiers/ModifierCloakerTearGas")
require("lib/modifiers/ModifierDozerMedic")
require("lib/modifiers/ModifierDozerMinigun")
require("lib/modifiers/ModifierDozerRage")
require("lib/modifiers/ModifierHeavySniper")
require("lib/modifiers/ModifierMedicAdrenaline")
require("lib/modifiers/ModifierMedicDeathwish")
require("lib/modifiers/ModifierMedicRage")
require("lib/modifiers/ModifierShieldPhalanx")
require("lib/modifiers/ModifierTaserOvercharge")
require("lib/modifiers/boosts/GageModifier")
require("lib/modifiers/boosts/GageModifierMaxHealth")
require("lib/modifiers/boosts/GageModifierMaxArmor")
require("lib/modifiers/boosts/GageModifierMaxStamina")
require("lib/modifiers/boosts/GageModifierMaxAmmo")
require("lib/modifiers/boosts/GageModifierMaxLives")
require("lib/modifiers/boosts/GageModifierMaxBodyBags")
require("lib/modifiers/boosts/GageModifierMaxDeployables")
require("lib/modifiers/boosts/GageModifierMaxThrowables")
require("lib/modifiers/boosts/GageModifierQuickReload")
require("lib/modifiers/boosts/GageModifierQuickSwitch")
require("lib/modifiers/boosts/GageModifierQuickPagers")
require("lib/modifiers/boosts/GageModifierQuickLocks")
require("lib/modifiers/boosts/GageModifierFastCrouching")
require("lib/modifiers/boosts/GageModifierDamageAbsorption")
require("lib/modifiers/boosts/GageModifierExplosionImmunity")
require("lib/modifiers/boosts/GageModifierPassivePanic")
require("lib/modifiers/boosts/GageModifierMeleeInvincibility")
require("lib/modifiers/boosts/GageModifierLifeSteal")

CrimeSpreeManager = CrimeSpreeManager or class()
CrimeSpreeManager.CS_VERSION = 2
CrimeSpreeManager.init = function (self)
	self._setup(self)

	return 
end
CrimeSpreeManager._setup = function (self)
	if not Global.crime_spree then
		Global.crime_spree = {}
	end

	self._global = Global.crime_spree

	self._setup_temporary_job(self)
	self._setup_modifiers(self)

	if self.is_active(self) then
		managers.assets:_setup_mission_assets()
	end

	return 
end
CrimeSpreeManager._setup_modifiers = function (self)
	if not self.is_active(self) then
		return 
	end

	local modifiers_to_activate = {}

	for _, active_data in ipairs(self.server_active_modifiers(self)) do
		local modifier = self.get_modifier(self, active_data.id)

		if modifier then
			local new_data = modifiers_to_activate[modifier.class] or {}

			for key, value_data in pairs(modifier.data) do
				local value = value_data[1]
				local stack_method = value_data[2]

				if stack_method == "none" then
					new_data[key] = value
				elseif stack_method == "add" then
					new_data[key] = (new_data[key] or 0) + value
				elseif stack_method == "sub" then
					new_data[key] = (new_data[key] or 0) - value
				elseif stack_method == "min" then
					new_data[key] = math.min(new_data[key] or math.huge, value)
				elseif stack_method == "max" then
					new_data[key] = math.max(new_data[key] or -math.huge, value)
				end
			end

			modifiers_to_activate[modifier.class] = new_data
		else
			Application:error("[CrimeSpreeManager] Can not activate modifier as it does not exist! Was it deleted?", active_data.id)
		end
	end

	if self._modifiers then
		for _, modifier in ipairs(self._modifiers) do
			modifier.destroy(modifier)
		end
	end

	self._modifiers = {}

	for class, data in pairs(modifiers_to_activate) do
		local mod_class = _G[class]

		if mod_class then
			table.insert(self._modifiers, mod_class.new(mod_class, data))
		else
			Application:error("Can not activate modifier as it does not exist!", class)
		end
	end

	return 
end
CrimeSpreeManager.get_modifier_stack_data = function (self, modifier_type)
	local stack_data = {}
	local modifiers = (self.is_active(self) and self.server_active_modifiers(self)) or self.active_modifiers(self)

	for _, active_data in ipairs(modifiers) do
		local modifier = self.get_modifier(self, active_data.id)

		if modifier and modifier.class == modifier_type then
			for key, value_data in pairs(modifier.data) do
				local value = value_data[1]
				local stack_method = value_data[2]

				if stack_method == "none" then
					stack_data[key] = value
				elseif stack_method == "add" then
					stack_data[key] = (stack_data[key] or 0) + value
				elseif stack_method == "sub" then
					stack_data[key] = (stack_data[key] or 0) - value
				elseif stack_method == "min" then
					stack_data[key] = math.min(stack_data[key] or math.huge, value)
				elseif stack_method == "max" then
					stack_data[key] = math.max(stack_data[key] or -math.huge, value)
				end
			end
		end
	end

	return stack_data
end
CrimeSpreeManager.clear = function (self)
	Global.crime_spree = nil
	self._global = nil

	self._setup(self)

	return 
end
CrimeSpreeManager.save = function (self, data)
	local available_missions = {}
	slot3 = pairs
	slot4 = self._global.available_missions or {}

	for _, mission in slot3(slot4) do
		table.insert(available_missions, mission.id)
	end

	local save_data = {
		in_progress = self._global.in_progress or false,
		spree_level = self._global.spree_level or 0,
		reward_level = self._global.reward_level or 0,
		missions = available_missions,
		randomization_cost = self._global.randomization_cost or false,
		start_data = self._global.start_data or nil,
		failure = self._global.failure_data or nil,
		highest_level = self._global.highest_level or 0,
		modifiers = self._global.modifiers or false,
		refund_allowed = self._global.refund_allowed,
		unshown_rewards = self._global.unshown_rewards or {},
		winning_streak = self._global.winning_streak or 0,
		cs_version = CrimeSpreeManager.CS_VERSION
	}
	data.crime_spree = save_data

	return 
end
CrimeSpreeManager.load = function (self, data, version)
	local save_data = data.crime_spree or {}
	save_data.highest_level = save_data.highest_level or data.highest_level

	if save_data.cs_version == CrimeSpreeManager.CS_VERSION then
		self._global.in_progress = save_data.in_progress or false
		self._global.spree_level = save_data.spree_level or 0
		self._global.reward_level = save_data.reward_level or 0
		self._global.randomization_cost = save_data.randomization_cost or false
		self._global.start_data = save_data.start_data or nil
		self._global.failure_data = save_data.failure or nil
		self._global.modifiers = save_data.modifiers or {}
		self._global.refund_allowed = save_data.refund_allowed
		self._global.unshown_rewards = save_data.unshown_rewards or {}
		self._global.winning_streak = save_data.winning_streak or 0
		self._global.available_missions = {}
		slot4 = pairs
		slot5 = save_data.missions or {}

		for _, id in slot4(slot5) do
			local mission = self.get_mission(self, id)

			if mission then
				table.insert(self._global.available_missions, mission)
			end
		end
	else
		self.reset_crime_spree(self)

		self._global.cleared = true
	end

	self._global.highest_level = save_data.highest_level or 0

	return 
end
CrimeSpreeManager.sync_save = function (self, data)
	data.crime_spree = {
		level = self.spree_level(self),
		mission_id = self.current_mission(self),
		unlocked_assets = self._unlocked_assets,
		unlocked_assets_peers = self._unlocked_assets_peers
	}

	return 
end
CrimeSpreeManager.sync_load = function (self, data)
	if not data.crime_spree then
		return 
	end

	if data.crime_spree.level then
		self.set_peer_spree_level(self, 1, data.crime_spree.level)
	end

	if data.crime_spree.mission_id then
		self._global.current_mission = data.crime_spree.mission_id
	end

	slot2 = ipairs
	slot3 = data.crime_spree.unlocked_assets or {}

	for _, asset_id in slot2(slot3) do
		self._on_asset_unlocked(self, asset_id, nil, true)
	end

	slot2 = ipairs
	slot3 = data.crime_spree.unlocked_assets_peers or {}

	for _, id in slot2(slot3) do
		if id == managers.network:session():local_peer():user_id() then
			self._has_unlocked_asset = true
		end
	end

	return 
end
CrimeSpreeManager.update = function (self, t, dt)
	if tweak_data.crime_spree.crash_causes_loss and game_state_machine:current_state_name() == "menu_main" and self._global.start_data and self._global.start_data.mission_id then
		self._on_mission_failed(self, self._global.start_data.mission_id)

		self._show_crash_dialog = true
	end

	if self._frame_callbacks then
		local clbks = self._frame_callbacks
		self._frame_callbacks = nil

		for _, clbk in ipairs(clbks) do
			clbk()
		end
	end

	if managers.menu_component:crimenet_sidebar_gui() then
		local button = managers.menu_component:crimenet_sidebar_gui():get_button("crime_spree")

		if button then
			if self.in_progress(self) then
				button.set_text(button, managers.localization:text("cn_crime_spree_level", {
					level = managers.experience:cash_string(self.spree_level(self), "")
				}))
				button.set_color(button, tweak_data.screen_colors.crime_spree_risk*0.8)
				button.set_highlight_color(button, tweak_data.screen_colors.crime_spree_risk)
			else
				button.set_text(button, managers.localization:text("cn_crime_spree"))
				button.set_color(button, nil)
				button.set_highlight_color(button, nil)
			end
		end
	end

	return 
end
CrimeSpreeManager.reset_crime_spree = function (self)
	self._global.in_progress = false
	self._global.spree_level = 0
	self._global.reward_level = 0
	self._global.winning_streak = 0
	self._global.available_missions = {}
	self._global.randomization_cost = false
	self._global.current_mission = nil
	self._global.failure_data = nil
	self._global.refund_allowed = true
	self._global.unshown_rewards = {}
	self._global.modifiers = {}

	return 
end
CrimeSpreeManager._add_frame_callback = function (self, clbk)
	self._frame_callbacks = self._frame_callbacks or {}

	table.insert(self._frame_callbacks, clbk)

	return 
end
CrimeSpreeManager._is_host = function (self)
	return Network:is_server() or Global.game_settings.single_player
end
CrimeSpreeManager.is_active = function (self)
	if game_state_machine then
		return game_state_machine:gamemode().id == GamemodeCrimeSpree.id
	else
		return Global.game_settings.gamemode == GamemodeCrimeSpree.id
	end

	return 
end
CrimeSpreeManager.unlocked = function (self)
	return tweak_data.crime_spree.unlock_level <= managers.experience:current_level() or 0 < managers.experience:current_rank()
end
CrimeSpreeManager.in_progress = function (self)
	return self._global.in_progress
end
CrimeSpreeManager.spree_level = function (self)
	return (not self.in_progress(self) or (not self._global.spree_level and 0)) and -1
end
CrimeSpreeManager.server_spree_level = function (self)
	if not self._is_host(self) then
		return self.get_peer_spree_level(self, 1)
	else
		return self.spree_level(self)
	end

	return 
end
CrimeSpreeManager.reward_level = function (self)
	return (not self.in_progress(self) or (not self._global.reward_level and 0)) and -1
end
CrimeSpreeManager.spree_level_gained = function (self)
	return self._spree_add or 0
end
CrimeSpreeManager.mission_completion_gain = function (self)
	return self._mission_completion_gain or 0
end
CrimeSpreeManager.catchup_bonus = function (self)
	return math.floor(self._catchup_bonus or 0)
end
CrimeSpreeManager.winning_streak_bonus = function (self)
	return math.floor(self._winning_streak or 0)
end
CrimeSpreeManager.mission_start_spree_level = function (self)
	if self._global.start_data then
		return self._global.start_data and self._global.start_data.spree_level
	elseif self._global._start_data then
		return self._global._start_data and self._global._start_data.spree_level
	end

	return 
end
CrimeSpreeManager.missions = function (self)
	return self._global.available_missions or {}
end
CrimeSpreeManager.server_missions = function (self)
	if not self._is_host(self) then
		return self._global.server_missions or {}
	else
		return self.missions(self)
	end

	return 
end
CrimeSpreeManager.randomization_cost = function (self)
	return self._global.randomization_cost or tweak_data.crime_spree.randomization_cost
end
CrimeSpreeManager.current_mission = function (self)
	return self._global.current_mission
end
CrimeSpreeManager.current_played_mission = function (self)
	if self._global.start_data and self._global.start_data.mission_id then
		return self._global.start_data.mission_id
	elseif self._global._start_data and self._global._start_data.mission_id then
		return self._global._start_data.mission_id
	elseif self._global.failure_data and self._global.failure_data.mission_id then
		return self._global.failure_data.mission_id
	end

	return 
end
CrimeSpreeManager.was_cleared = function (self)
	return not not self._global.cleared
end
CrimeSpreeManager.show_cleared_dialog = function (self)
	self._global.cleared = nil

	managers.menu:show_crime_spree_cleared_dialog()

	return 
end
CrimeSpreeManager.has_failed = function (self)
	return self._global.failure_data ~= nil
end
CrimeSpreeManager.server_has_failed = function (self)
	if self._global.peers_spree_state and self._global.peers_spree_state[1] then
		return self._global.peers_spree_state[1] == "failed"
	end

	return false
end
CrimeSpreeManager.show_crash_dialog = function (self)
	return self._show_crash_dialog and self.has_failed(self)
end
CrimeSpreeManager.clear_crash_dialog = function (self)
	self._show_crash_dialog = nil

	return 
end
CrimeSpreeManager.highest_level = function (self)
	return self._global.highest_level or 0
end
CrimeSpreeManager.active_modifiers = function (self)
	return self._global.modifiers
end
CrimeSpreeManager.server_active_modifiers = function (self)
	if not self._is_host(self) then
		return self._global.server_modifiers or {}
	else
		return (self.in_progress(self) and self.active_modifiers(self)) or {}
	end

	return 
end
CrimeSpreeManager.active_modifier_classes = function (self)
	return self._modifiers or {}
end
CrimeSpreeManager.modifiers_to_select = function (self, table_name, add_repeating)
	local modifiers_table = tweak_data.crime_spree.modifiers[table_name]
	local base_number = self.server_spree_level(self)/tweak_data.crime_spree.modifier_levels[table_name]
	local active_number = 0

	if not add_repeating then
		base_number = math.min(base_number, #modifiers_table)
	end

	for _, modifier_data in ipairs(self.server_active_modifiers(self)) do
		local contains = false

		for _, modifier in ipairs(modifiers_table) do
			if modifier.id == modifier_data.id then
				contains = true

				break
			end
		end

		if contains then
			active_number = active_number + 1
		else
			local repeating, tbl = self.is_repeating_modifier(self, modifier_data.id)

			if repeating and tbl == table_name then
				active_number = active_number + 1
			end
		end
	end

	return math.floor(math.max(base_number - active_number, 0))
end
CrimeSpreeManager.next_modifier_level = function (self, table_name, level, additional)
	local total = #tweak_data.crime_spree.modifiers[table_name]
	local is_repeating = tweak_data.crime_spree.repeating_modifiers[table_name] ~= nil
	local count = math.floor((level or self.server_spree_level(self))/tweak_data.crime_spree.modifier_levels[table_name])

	if not is_repeating and total <= count then
		return nil
	end

	return (count + 1 + (additional or 0))*tweak_data.crime_spree.modifier_levels[table_name]
end
CrimeSpreeManager.active_gage_assets = function (self)
	return self._active_assets or {}
end
CrimeSpreeManager.can_refund_entry_fee = function (self)
	return self._global.refund_allowed
end
CrimeSpreeManager.get_narrative_tweak_data_for_mission_level = function (self, mission_id)
	local mission = self.get_mission(self, mission_id)
	local narrative_id, narrative_data, day, variant = nil

	for job_id, data in pairs(tweak_data.narrative.jobs) do
		slot12 = ipairs
		slot13 = data.chain or {}

		for idx, level_data in slot12(slot13) do
			if type(level_data) == "table" and not level_data.level_id then
				local found = false

				for variant_idx, level_data in ipairs(level_data) do
					if level_data == mission.level then
						narrative_id = job_id
						narrative_data = data
						day = idx
						variant = variant_idx
						found = true

						break
					end
				end

				if found then
					break
				end
			elseif level_data == mission.level then
				narrative_id = job_id
				narrative_data = data
				day = idx

				break
			end
		end
	end

	if narrative_data then
		for job_id, data in pairs(tweak_data.narrative.jobs) do
			if data.job_wrapper and table.contains(data.job_wrapper, narrative_id) then
				narrative_data = data

				break
			end
		end
	end

	return narrative_data, day, variant
end
CrimeSpreeManager.get_mission = function (self, mission_id)
	mission_id = mission_id or self.current_mission(self)

	for category, tbl in pairs(tweak_data.crime_spree.missions) do
		for idx, data in pairs(tbl) do
			if data.id == mission_id then
				return data
			end
		end
	end

	return 
end
CrimeSpreeManager.get_random_missions = function (self, prev_missions)
	local mission_lists = tweak_data.crime_spree.missions

	return {
		table.random(mission_lists[1]),
		table.random(mission_lists[2]),
		table.random(mission_lists[3])
	}
end
CrimeSpreeManager.get_random_mission = function (self)
	return table.random(self.get_random_missions(self))
end
CrimeSpreeManager.get_start_cost = function (self, level)
	return math.floor(tweak_data.crime_spree.initial_cost + tweak_data.crime_spree.cost_per_level*(level or 0))
end
CrimeSpreeManager.get_continue_cost = function (self, level)
	if level == 0 then
		return 0
	else
		return math.floor(tweak_data.crime_spree.continue_cost[1] + tweak_data.crime_spree.continue_cost[2]*(level or 0))
	end

	return 
end
CrimeSpreeManager.set_starting_level = function (self, level)
	self._starting_level = level

	return 
end
CrimeSpreeManager.starting_level = function (self)
	return self._starting_level or 0
end
CrimeSpreeManager.check_forced_modifiers = function (self)
	if not self.is_active(self) then
		return 
	end

	local count = self.modifiers_to_select(self, "forced", true)

	if 0 < count then
		self._add_frame_callback(self, callback(managers.menu, managers.menu, "open_node", "crime_spree_forced_modifiers"))
	end

	return 
end
CrimeSpreeManager.get_forced_modifiers = function (self)
	return self._get_modifiers(self, "forced", self.modifiers_to_select(self, "forced", true), true)
end
CrimeSpreeManager.get_loud_modifiers = function (self)
	return self._get_modifiers(self, "loud", tweak_data.crime_spree.max_modifiers_displayed)
end
CrimeSpreeManager.get_stealth_modifiers = function (self)
	return self._get_modifiers(self, "stealth", tweak_data.crime_spree.max_modifiers_displayed)
end
CrimeSpreeManager._get_modifiers = function (self, table_name, max_count, add_repeating)
	local modifiers = {}
	local modifiers_table = tweak_data.crime_spree.modifiers[table_name]
	local repeating_modifiers_table = tweak_data.crime_spree.repeating_modifiers[table_name]

	if not modifiers_table or not modifiers_table[1] then
		Application:error("Can not get modifiers from table as there is no starting modifier: ", table_name)

		return {}
	end

	for _, modifier in ipairs(modifiers_table) do
		local contains = false

		for _, modifier_data in ipairs(self.server_active_modifiers(self)) do
			if modifier_data.id == modifier.id then
				contains = true

				break
			end
		end

		if not contains then
			table.insert(modifiers, modifier)

			if max_count <= #modifiers then
				break
			end
		end
	end

	if add_repeating then
		local diff = max_count - #modifiers

		if 0 < diff then
			for i = 1, diff, 1 do
				local idx = i%#repeating_modifiers_table + 1
				local mod_data = repeating_modifiers_table[idx]
				local new_mod = deep_clone(mod_data)
				new_mod.id = new_mod.id .. tostring(math.floor(self.server_spree_level(self)/new_mod.level)*i)

				table.insert(modifiers, new_mod)
			end
		end
	end

	return modifiers
end
CrimeSpreeManager.select_modifier = function (self, modifier_id)
	if table.contains(self._global.modifiers, modifier_id) then
		Application:error("Can not add the same modifier twice! ", modifier_id)

		return false
	end

	local data = {
		id = modifier_id,
		level = self.spree_level(self)
	}

	table.insert(self._global.modifiers, data)

	if self._is_host(self) then
		self.send_crime_spree_modifier(self, nil, data, true)
	end

	return true
end
CrimeSpreeManager.get_modifier = function (self, modifier_id)
	for _, modifiers_table in pairs(tweak_data.crime_spree.modifiers) do
		for _, data in pairs(modifiers_table) do
			if data.id == modifier_id then
				return data
			end
		end
	end

	for _, mod_table in pairs(tweak_data.crime_spree.repeating_modifiers) do
		for _, mod_data in pairs(mod_table) do
			if string.sub(modifier_id, 1, string.len(mod_data.id)) == mod_data.id then
				return mod_data
			end
		end
	end

	return 
end
CrimeSpreeManager.is_repeating_modifier = function (self, modifier_id)
	for id, mod_table in pairs(tweak_data.crime_spree.repeating_modifiers) do
		for _, mod_data in pairs(mod_table) do
			if string.sub(modifier_id, 1, string.len(mod_data.id)) == mod_data.id then
				return true, id
			end
		end
	end

	return 
end
CrimeSpreeManager.get_reward_amount = function (self, reward_id)
	self._global.unshown_rewards = self._global.unshown_rewards or {}

	return math.floor(self._global.unshown_rewards[reward_id] or 0)
end
CrimeSpreeManager.flush_reward_amount = function (self, reward_id)
	self._global.unshown_rewards = self._global.unshown_rewards or {}

	if self._global.unshown_rewards[reward_id] then
		self._global.unshown_rewards[reward_id] = self._global.unshown_rewards[reward_id] - math.floor(self._global.unshown_rewards[reward_id])
	end

	return 
end
CrimeSpreeManager._get_modifier_tables = function (self)
	return {
		self.active_modifier_classes(self),
		self.active_gage_assets(self)
	}
end
CrimeSpreeManager.run_func = function (self, func_name, ...)
	for _, modifiers_table in ipairs(self._get_modifier_tables(self)) do
		for i, modifier in pairs(modifiers_table) do
			if modifier and modifier[func_name] then
				modifier[func_name](modifier, ...)
			end
		end
	end

	return 
end
CrimeSpreeManager.modify_value = function (self, id, value, ...)
	for _, modifiers_table in ipairs(self._get_modifier_tables(self)) do
		for i, modifier in pairs(modifiers_table) do
			if modifier and modifier.modify_value then
				local new_value, override = modifier.modify_value(modifier, id, value, ...)

				if new_value ~= nil or override then
					value = new_value
				end
			end
		end
	end

	return value
end
CrimeSpreeManager.start_crime_spree = function (self, starting_level)
	print("CrimeSpreeManager:start_crime_spree")

	if not self.can_start_spree(self, starting_level) then
		return false
	end

	local cost = self.get_start_cost(self, starting_level)

	managers.custom_safehouse:deduct_coins(cost)
	self.reset_crime_spree(self)

	self._global.in_progress = true
	self._global.spree_level = starting_level or 0

	self.generate_new_mission_set(self)

	return true
end
CrimeSpreeManager.continue_crime_spree = function (self)
	if not self.can_continue_spree(self) then
		return false
	end

	local cost = self.get_continue_cost(self, self.spree_level(self))

	managers.custom_safehouse:deduct_coins(cost)

	self._global.failure_data = nil
	self._global.randomization_cost = false

	self.generate_new_mission_set(self)
	self._send_crime_spree_level_to_peers(self)

	return true
end
CrimeSpreeManager.generate_new_mission_set = function (self)
	if not self.in_progress(self) then
		Application:error("Can not generate new missions if a crime spree is not in progress!")

		return 
	end

	self._global.available_missions = self.get_random_missions(self, self._global.available_missions)

	if self._is_host(self) then
		for i, mission in pairs(self._global.available_missions) do
			self.send_crime_spree_mission_data(self, i, mission.id, false, false)
		end
	end

	return 
end
CrimeSpreeManager.randomize_mission_set = function (self)
	if not self.in_progress(self) then
		Application:error("Can not randomize missions if a crime spree is not in progress!")

		return 
	end

	if not self._global.randomization_cost then
		self._global.randomization_cost = tweak_data.crime_spree.randomization_cost
	else
		self._global.randomization_cost = self._global.randomization_cost*tweak_data.crime_spree.randomization_multiplier
	end

	self.generate_new_mission_set(self)

	if self._is_host(self) then
		for idx, mission_data in ipairs(self.missions(self)) do
			self.send_crime_spree_mission_data(self, idx, mission_data.id, false, true)
		end
	end

	return 
end
CrimeSpreeManager.calculate_rewards = function (self)
	local rewards = {}

	for _, reward in ipairs(tweak_data.crime_spree.rewards) do
		local amt = math.floor(reward.amount*self.reward_level(self))
		rewards[reward.id] = amt or 0
	end

	return rewards
end
CrimeSpreeManager.generate_loot_drops = function (self, amount)
	self._generated_loot_drops = {}
	self._loot_drops_coroutine = managers.lootdrop:new_make_mass_drop(amount, tweak_data.crime_spree.loot_drop_reward_pay_class, self._generated_loot_drops)

	return 
end
CrimeSpreeManager.loot_drops = function (self)
	return self._loot_drops or {}
end
CrimeSpreeManager._give_all_cosmetics_reward = function (self, amount)
	local all_cosmetics_reward = tweak_data.crime_spree.all_cosmetics_reward
	local type, amt = nil

	if all_cosmetics_reward then
		type = all_cosmetics_reward.type

		if all_cosmetics_reward.type == "continental_coins" then
			amt = all_cosmetics_reward.amount*amount

			managers.custom_safehouse:add_coins(amt)
		end
	end

	return {
		{
			type = type,
			amount = amt
		}
	}
end
CrimeSpreeManager.generate_cosmetic_drops = function (self, amount)
	local tradables_inventory = managers.blackmarket:get_inventory_tradable()
	local rewards_pool = {}
	local final_rewards = {}

	for i, reward in ipairs(tweak_data.crime_spree.cosmetic_rewards) do
		local unlocked = nil

		if reward.type == "armor" then
			unlocked = managers.blackmarket:armor_skin_unlocked(reward.id)

			if not unlocked then
				for _, data in pairs(tradables_inventory) do
					if data.id == reward.id then
						unlocked = true

						break
					end
				end
			end
		end

		if not unlocked then
			table.insert(rewards_pool, reward)
		end
	end

	local num_rewards = #rewards_pool

	if num_rewards == 0 then
		return self._give_all_cosmetics_reward(self, amount)
	end

	if num_rewards < amount then
		local rewards = self._give_all_cosmetics_reward(self, amount - num_rewards)

		for _, reward in ipairs(rewards) do
			table.insert(final_rewards, reward)
		end
	end

	for i = 1, math.min(amount, num_rewards), 1 do
		local idx = math.random(#rewards_pool)
		local reward = rewards_pool[idx]

		if reward.type == "armor" then
			managers.blackmarket:on_aquired_armor_skin(reward.id)
		end

		table.insert(final_rewards, reward)
		table.remove(rewards_pool, idx)
	end

	return final_rewards
end
CrimeSpreeManager.cosmetic_rewards = function (self)
	return self._cosmetic_drops or {}
end
CrimeSpreeManager.award_rewards = function (self, rewards_table)
	for id, amount in pairs(rewards_table) do
		if id == "experience" then
			managers.experience:give_experience(amount, true)
		elseif id == "cash" then
			managers.money:add_to_total(amount)
		elseif id == "continental_coins" then
			managers.custom_safehouse:add_coins(amount)
		elseif id == "loot_drop" then
			self.generate_loot_drops(self, amount)
		elseif id == "random_cosmetic" then
			self._cosmetic_drops = self.generate_cosmetic_drops(self, amount)
		end
	end

	return 
end
CrimeSpreeManager.can_start_spree = function (self, starting_level)
	if self.in_progress(self) then
		Application:error("Can not start a Crime Spree if one is already in progress!")

		return false
	end

	local cost = self.get_start_cost(self, starting_level)

	if managers.custom_safehouse:coins() < cost then
		Application:error("Can not start a Crime Spree if you can not pay the cost! Cost: ", cost)

		return false
	end

	return true
end
CrimeSpreeManager.can_select_mission = function (self, mission_id)
	for _, data in ipairs(self.missions(self)) do
		if mission_id == data[1] then
			return true
		end
	end

	return false
end
CrimeSpreeManager.select_mission = function (self, mission_id)
	if mission_id == false then
		self._global.current_mission = nil

		return 
	end

	local mission_data = self.get_mission(self, mission_id)

	if mission_data then
		self._global.current_mission = mission_data.id

		self._setup_temporary_job(self)
		managers.job:activate_temporary_job("crime_spree", mission_data.level.level_id)
		self._setup_global_from_mission_id(self, mission_id)

		if Network:is_server() then
			MenuCallbackHandler:update_matchmake_attributes()
		end
	end

	return 
end
CrimeSpreeManager._setup_global_from_mission_id = function (self, mission_id)
	local mission_data = self.get_mission(self, mission_id)

	if mission_data then
		Global.game_settings.difficulty = tweak_data.crime_spree.base_difficulty
		Global.game_settings.level_id = mission_data.level.level_id
		Global.game_settings.mission = mission_data.mission or "none"
	end

	return 
end
CrimeSpreeManager.can_continue_spree = function (self)
	if not self.in_progress(self) then
		Application:error("Can not continue a Crime Spree if one is not in progress!")

		return false
	end

	local cost = self.get_continue_cost(self, self.spree_level(self))

	if managers.custom_safehouse:coins() < cost then
		Application:error("Can not continue a Crime Spree if you can not pay the cost! Cost: ", cost)

		return false
	end

	return true
end
CrimeSpreeManager.GageAssetEvents = {
	Unlock = 2,
	SendAlreadyUnlocked = 1
}
CrimeSpreeManager.can_unlock_asset_is_in_game = function (self)
	return game_state_machine:current_state_name() ~= "ingame_waiting_for_players" or 0 < managers.criminals:get_num_player_criminals()
end
CrimeSpreeManager.can_unlock_asset = function (self)
	if self.can_unlock_asset_is_in_game(self) then
		return false, "menu_cs_ga_in_progress"
	end

	local check_is_dropin = game_state_machine and game_state_machine:current_state() and game_state_machine:current_state().check_is_dropin and game_state_machine:current_state():check_is_dropin()

	return not self._has_unlocked_asset, "menu_cs_ga_limit_reached"
end
CrimeSpreeManager.is_asset_unlocked = function (self, asset_id)
	if self._unlocked_assets then
		return table.contains(self._unlocked_assets, asset_id)
	else
		return false
	end

	return 
end
CrimeSpreeManager._can_asset_be_unlocked = function (self, asset_id)
	if self._unlocked_assets then
		return not table.contains(self._unlocked_assets, asset_id)
	else
		return true
	end

	return 
end
CrimeSpreeManager.unlock_gage_asset = function (self, asset_id)
	local asset_tweak_data = tweak_data.crime_spree.assets[asset_id]

	if not asset_tweak_data then
		Application:error("Can not unlock gage asset that doesn't exist!", asset_id)

		return false
	end

	if not self.can_unlock_asset(self) then
		return false
	end

	if self._unlocked_assets and tweak_data.crime_spree.max_assets_unlocked <= #self._unlocked_assets then
		Application:error("Attempting to unlock a gage asset when the limit to gage assets has already been reached!", asset_id)

		return false
	end

	if not self._can_asset_be_unlocked(self, asset_id) then
		return false
	end

	managers.custom_safehouse:deduct_coins(asset_tweak_data.cost)

	local params = {
		CrimeSpreeManager.GageAssetEvents.Unlock,
		asset_id
	}

	managers.network:session():send_to_peers("sync_crime_spree_gage_asset_event", unpack(params))

	self._has_unlocked_asset = true

	self._on_asset_unlocked(self, asset_id)

	return true
end
CrimeSpreeManager._on_asset_unlocked = function (self, asset_id, peer, forced)
	local asset_tweak_data = tweak_data.crime_spree.assets[asset_id]
	peer = peer or managers.network:session():local_peer()

	if not asset_tweak_data then
		Application:error("Can not unlock gage asset that doesn't exist!", asset_id)

		return false
	end

	if self._unlocked_assets and tweak_data.crime_spree.max_assets_unlocked <= #self._unlocked_assets then
		Application:error("Attempting to unlock a gage asset when the limit to gage assets has already been reached!", asset_id, peer)

		return false
	end

	if not self._can_asset_be_unlocked(self, asset_id) then
		return false
	end

	managers.menu_component:unlock_gage_asset_mission_briefing_gui(asset_id)

	if WalletGuiObject then
		WalletGuiObject.refresh()
	end

	if managers.chat and peer and not forced then
		managers.chat:feed_system_message(ChatManager.GAME, managers.localization:text("menu_chat_peer_unlocked_gage_asset", {
			name = peer.name(peer),
			asset = managers.localization:text(asset_tweak_data.name_id)
		}))
	end

	if not forced then
		self._unlocked_assets_peers = self._unlocked_assets_peers or {}

		table.insert(self._unlocked_assets_peers, peer.user_id(peer))
	end

	self._unlocked_assets = self._unlocked_assets or {}

	table.insert(self._unlocked_assets, asset_id)

	local mod_class = _G[asset_tweak_data.class]

	if mod_class then
		self._active_assets = self._active_assets or {}

		table.insert(self._active_assets, mod_class.new(mod_class, asset_tweak_data))
	else
		Application:error("Can not activate gage asset as it's modifier class does not exist!", class)
	end

	return true
end
CrimeSpreeManager.on_gage_asset_event = function (self, event_id, asset_id, peer)
	if event_id == CrimeSpreeManager.GageAssetEvents.Unlock then
		self._on_asset_unlocked(self, asset_id, peer)
	end

	return 
end
CrimeSpreeManager.set_temporary_mission = function (self, mission_id)
	self._global.current_mission = mission_id

	self._setup_global_from_mission_id(self, mission_id)
	self._setup_temporary_job(self)

	return 
end
CrimeSpreeManager._setup_temporary_job = function (self)
	if not self.current_mission(self) then
		return 
	end

	local mission_data = self.get_mission(self, self.current_mission(self))
	tweak_data.narrative.jobs.crime_spree.chain = {
		mission_data and mission_data.level
	}

	return 
end
CrimeSpreeManager.on_mission_started = function (self, mission_id)
	if not self.is_active(self) then
		return 
	end

	self._global.start_data = {
		mission_id = mission_id,
		spree_level = self.spree_level(self),
		server_spree_level = self.server_spree_level(self),
		reward_level = self.reward_level(self)
	}
	self._global.refund_allowed = false

	MenuCallbackHandler:save_progress()

	return 
end
CrimeSpreeManager.on_mission_completed = function (self, mission_id)
	if not self.is_active(self) then
		return 
	end

	managers.mission:clear_job_values()

	if not self.has_failed(self) then
		local mission_data = self.get_mission(self, mission_id)
		local spree_add = mission_data.add
		self._mission_completion_gain = mission_data.add

		if not self._is_host(self) and self._global.start_data and self._global.start_data.server_spree_level then
			local server_level = (self._global.start_data and self._global.start_data.server_spree_level) or -1

			if server_level < 0 then
				server_level = self.server_spree_level(self)
			end

			self.set_peer_spree_level(self, 1, server_level + spree_add)
		end

		if not self._is_host(self) and self.spree_level(self) < self.server_spree_level(self) then
			local diff = self.server_spree_level(self) - self.spree_level(self)
			self._catchup_bonus = math.floor(diff*tweak_data.crime_spree.catchup_bonus)
			spree_add = spree_add + self._catchup_bonus
		end

		if not self._is_host(self) and self.server_spree_level(self) < self.spree_level(self) then
			local diff = self.spree_level(self) - self.server_spree_level(self)
			spree_add = spree_add - diff
		end

		spree_add = math.max(math.floor(spree_add), 0)
		self._spree_add = spree_add
		local reward_add = spree_add

		if self.spree_level(self) <= self.server_spree_level(self) then
			self._global.winning_streak = (self._global.winning_streak or 1) + spree_add*tweak_data.crime_spree.winning_streak

			if self._global.winning_streak < 1 then
				self._global.winning_streak = self._global.winning_streak + 1
			end

			local pre_winning = reward_add
			reward_add = reward_add*self._global.winning_streak
			self._winning_streak = reward_add - pre_winning
		end

		reward_add = math.max(math.floor(reward_add), 0)

		if self.in_progress(self) then
			self._global.spree_level = self._global.spree_level + spree_add

			self._check_highest_level(self, self._global.spree_level or 0)
		end

		self._global.reward_level = self._global.reward_level + reward_add
		self._global.unshown_rewards = self._global.unshown_rewards or {}

		for _, reward in ipairs(tweak_data.crime_spree.rewards) do
			self._global.unshown_rewards[reward.id] = (self._global.unshown_rewards[reward.id] or 0) + reward_add*reward.amount
		end
	end

	self._global.current_mission = nil
	self._global._start_data = self._global.start_data
	self._global.start_data = nil
	self._global.randomization_cost = false

	self.generate_new_mission_set(self)
	self.check_achievements(self)
	MenuCallbackHandler:save_progress()

	if Network:is_server() then
		MenuCallbackHandler:update_matchmake_attributes()
	end

	return 
end
CrimeSpreeManager.on_mission_failed = function (self, mission_id)
	if not self.is_active(self) then
		return 
	end

	self._on_mission_failed(self, mission_id)

	return 
end
CrimeSpreeManager._on_mission_failed = function (self, mission_id)
	if not self._is_host(self) and self.server_spree_level(self) + tweak_data.crime_spree.protection_threshold < self.spree_level(self) then
		return 
	end

	self._global.failure_data = {
		mission_id = mission_id,
		highest_level = self._global.start_data.spree_level or self._global.spree_level,
		reward_level = self.reward_level(self)
	}
	self._global.start_data = nil

	if tweak_data.crime_spree.winning_streak_reset_on_failure then
		self._global.winning_streak = nil
	end

	self._global.current_mission = nil

	MenuCallbackHandler:save_progress()

	return 
end
CrimeSpreeManager.check_achievements = function (self)
	if not self.is_active(self) or not self.in_progress(self) then
		return 
	end

	for id, achievement in pairs(tweak_data.achievement.crime_spree) do
		if achievement.level <= self.spree_level(self) then
			managers.achievment:award_data(achievement)
		end
	end

	return 
end
CrimeSpreeManager.on_spree_complete = function (self)
	if not self.in_progress(self) then
		return 
	end

	local rewards = self.calculate_rewards(self)

	self.award_rewards(self, rewards)
	self._check_highest_level(self, self.spree_level(self) or 0)

	self._global.in_progress = false

	MenuCallbackHandler:save_progress()

	return 
end
CrimeSpreeManager.is_generating_rewards = function (self)
	return self._loot_drops_coroutine ~= nil
end
CrimeSpreeManager.reward_generation_progress = function (self)
	if self._generated_loot_drops then
		return self._generated_loot_drops.progress.current or 0, self._generated_loot_drops.progress.total or 1
	end

	return 1
end
CrimeSpreeManager.has_finished_generating_rewards = function (self)
	if self._loot_drops_coroutine then
		local status = coroutine.status(self._loot_drops_coroutine)

		if status == "dead" then
			self._loot_drops = clone(self._generated_loot_drops.items)
			self._generated_loot_drops = nil
			self._loot_drops_coroutine = nil

			MenuCallbackHandler:save_progress()

			return true
		elseif status == "suspended" then
			coroutine.resume(self._loot_drops_coroutine)

			return false
		else
			return false
		end
	end

	return true
end
CrimeSpreeManager.enable_crime_spree_gamemode = function (self)
	game_state_machine:change_gamemode_by_name("crime_spree")

	return 
end
CrimeSpreeManager.disable_crime_spree_gamemode = function (self)
	game_state_machine:change_gamemode_by_name("standard")

	return 
end
CrimeSpreeManager.apply_matchmake_attributes = function (self, lobby_attributes)
	print("[CrimeSpreeManager] Applying lobby attributes...")

	if self.in_progress(self) and self.is_active(self) then
		lobby_attributes.crime_spree = self.spree_level(self)
		lobby_attributes.crime_spree_mission = self._global.current_mission
	else
		lobby_attributes.crime_spree = -1
	end

	return 
end
CrimeSpreeManager.join_server = function (self, server_data)
	self.set_peer_spree_level(self, 1, server_data.crime_spree)

	self._global.current_mission = server_data.crime_spree_mission

	return 
end
CrimeSpreeManager.on_entered_lobby = function (self)
	if not self.is_active(self) then
		return 
	end

	if not self._is_host(self) then
		local lobby_data = managers.network.matchmake.lobby_handler and managers.network.matchmake.lobby_handler:get_lobby_data()

		if lobby_data then
			local spree_level = tonumber(lobby_data.crime_spree)

			if spree_level and 0 <= spree_level then
				local peer_id = nil

				for id, peer in pairs(managers.network:session():peers()) do
					if peer.is_host(peer) then
						peer_id = id

						break
					end
				end

				if peer_id then
					self.set_peer_spree_level(self, peer_id, spree_level)
				end
			end
		end
	end

	self._send_crime_spree_level_to_peers(self)

	return 
end
CrimeSpreeManager.on_left_lobby = function (self)
	self.disable_crime_spree_gamemode(self)

	self._global.start_data = nil
	self._global.peer_spree_levels = nil
	self._global.peers_spree_state = nil
	self._global.server_missions = nil
	self._global.current_mission = nil
	self._global.server_modifiers = nil

	return 
end
CrimeSpreeManager.on_peer_finished_loading = function (self, peer)
	if not self.is_active(self) then
		return 
	end

	local missions_gui = managers.menu_component:crime_spree_missions_gui()

	self._send_crime_spree_level_to_peers(self)

	if self._is_host(self) then
		for idx, mission_data in ipairs(self.missions(self)) do
			self.send_crime_spree_mission_data(self, idx, mission_data.id, missions_gui and missions_gui.get_selected_index(missions_gui) == idx, false)
		end
	end

	if self._is_host(self) then
		for _, modifier_data in ipairs(self.active_modifiers(self)) do
			self.send_crime_spree_modifier(self, peer, modifier_data, false)
		end

		managers.network:session():send_to_peer(peer, "sync_crime_spree_modifiers_finalize")
	end

	return 
end
CrimeSpreeManager._send_crime_spree_level_to_peers = function (self)
	if not self.is_active(self) then
		return 
	end

	local params = {
		managers.network:session():local_peer():id(),
		(self.in_progress(self) and self.spree_level(self)) or -1,
		self.has_failed(self)
	}

	managers.network:session():send_to_peers("sync_crime_spree_level", unpack(params))

	return 
end
CrimeSpreeManager.send_crime_spree_mission_data = function (self, mission_slot, mission_id, selected, perform_randomize)
	if not self.is_active(self) or not mission_slot or not mission_id then
		return 
	end

	local params = {
		mission_slot,
		mission_id,
		(selected ~= nil and selected) or false,
		(perform_randomize ~= nil and perform_randomize) or false
	}

	managers.network:session():send_to_peers("sync_crime_spree_mission", unpack(params))

	return 
end
CrimeSpreeManager.send_crime_spree_modifier = function (self, peer, modifier_data, announce)
	if not self.is_active(self) or not modifier_data then
		return 
	end

	local params = {
		modifier_data.id,
		modifier_data.level,
		announce or false
	}

	if peer then
		managers.network:session():send_to_peer(peer, "sync_crime_spree_modifier", unpack(params))
	else
		managers.network:session():send_to_peers("sync_crime_spree_modifier", unpack(params))
	end

	return 
end
CrimeSpreeManager.get_peer_spree_level = function (self, peer_id)
	if Network:multiplayer() and managers.network:session() then
		local peer = managers.network:session():local_peer()

		if peer and peer.id(peer) == peer_id then
			return self.spree_level(self)
		end
	end

	if self._global.peer_spree_levels then
		return self._global.peer_spree_levels[peer_id] or -1
	else
		return -1
	end

	return 
end
CrimeSpreeManager.set_peer_spree_level = function (self, peer_id, level, has_failed)
	self._global.peer_spree_levels = self._global.peer_spree_levels or {}
	self._global.peer_spree_levels[peer_id] = level
	self._global.peers_spree_state = self._global.peers_spree_state or {}
	self._global.peers_spree_state[peer_id] = (has_failed and "failed") or "normal"

	if managers.menu_component and managers.menu_component:crime_spree_missions_gui() then
		managers.menu_component:crime_spree_missions_gui():refresh()
	end

	return 
end
CrimeSpreeManager.set_server_mission = function (self, mission_slot, mission_id, selected, perform_randomize)
	self._global.server_missions = self._global.server_missions or {}
	self._global.server_missions[mission_slot] = self.get_mission(self, mission_id)

	if selected then
		self._global.current_mission = mission_id

		self._setup_global_from_mission_id(self, mission_id)
	end

	local missions_gui = managers.menu_component and managers.menu_component:crime_spree_missions_gui()

	if missions_gui then
		missions_gui.update_mission(missions_gui, mission_slot)

		if selected then
			missions_gui._select_mission(missions_gui, mission_slot)
		end

		if perform_randomize then
			missions_gui.randomize_crimespree(missions_gui, mission_slot)
		end
	elseif selected then
		self.set_consumable_value(self, "mission_gui_selected_slot", mission_slot)
	end

	return 
end
CrimeSpreeManager.set_server_modifier = function (self, modifier_id, modifier_level, announce)
	self._global.server_modifiers = self._global.server_modifiers or {}

	for _, data in ipairs(self._global.server_modifiers) do
		if data.id == modifier_id then
			Application:error("Can not add the same server modifier twice!", modifier_id)

			return 
		end
	end

	table.insert(self._global.server_modifiers, {
		id = modifier_id,
		level = modifier_level
	})
	self._add_frame_callback(self, callback(managers.menu_component, managers.menu_component, "refresh_crime_spree_details_gui"))

	if announce then
		self._add_frame_callback(self, callback(self, self, "_announce_modifier", modifier_id))
	end

	return 
end
CrimeSpreeManager._announce_modifier = function (self, modifier_id)
	if managers.menu_component and managers.menu_component:crime_spree_details_gui() then
		managers.menu_component:crime_spree_details_gui():show_new_modifier(modifier_id)
	else
		managers.menu:post_event(tweak_data.crime_spree.announce_modifier_stinger)
	end

	return 
end
CrimeSpreeManager.on_finalize_modifiers = function (self)
	if not self._is_host(self) then
		self._setup_modifiers(self)
	end

	return 
end
CrimeSpreeManager.set_consumable_value = function (self, name, value)
	self._consumable_values = self._consumable_values or {}
	self._consumable_values[name] = value

	return 
end
CrimeSpreeManager.has_consumable_value = function (self, name)
	self._consumable_values = self._consumable_values or {}

	return self._consumable_values[name] ~= nil
end
CrimeSpreeManager.consumable_value = function (self, name)
	self._consumable_values = self._consumable_values or {}

	if self._consumable_values[name] ~= nil then
		local value = self._consumable_values[name]
		self._consumable_values[name] = nil

		return value
	end

	return 
end
CrimeSpreeManager._check_highest_level = function (self, value)
	if (self._global.highest_level or 0) < value then
		self._global.highest_level = value

		managers.mission:call_global_event(Message.OnHighestCrimeSpree, value)
	end

	return 
end

return 
