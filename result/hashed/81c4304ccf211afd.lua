EnvironmentControllerManager = EnvironmentControllerManager or class(CoreEnvironmentControllerManager)
EnvironmentControllerManager.init = function (self)
	EnvironmentControllerManager.super.init(self)

	if _G.IS_VR then
		self.set_dof_setting(self, "none")
	end

	return 
end
EnvironmentControllerManager.set_dof_setting = function (self, setting)
	if _G.IS_VR then
		setting = "none"
	end

	EnvironmentControllerManager.super.set_dof_setting(self, setting)

	return 
end

CoreClass.override_class(CoreEnvironmentControllerManager, EnvironmentControllerManager)

return 
