IngameUIExt = IngameUIExt or class()
IngameUIExt.init = function (self, unit)
	self._unit = unit

	if self.objects then
		for name, object in pairs(self.objects) do
			managers.hud:register_ingame_workspace(name, self._unit:get_object(Idstring(object)))
		end
	end

	return 
end

return 
