require("lib/states/GameState")

IngameHost = IngameHost or class(GameState)
IngameHost.init = function (self, game_state_machine)
	GameState.init(self, "ingame_host", game_state_machine)

	return 
end
IngameHost.at_enter = function (self)
	print("[IngameHost] Enter")
	managers.network:session():local_peer():set_waiting_for_player_ready(true)
	managers.network:session():on_set_member_ready(managers.network:session():local_peer():id(), true, true, false)

	return 
end
IngameHost.at_exit = function (self)
	print("[IngameHost] Exit")

	return 
end

return 
