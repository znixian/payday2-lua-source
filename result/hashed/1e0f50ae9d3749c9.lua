local massive_font = tweak_data.menu.pd2_massive_font
local large_font = tweak_data.menu.pd2_large_font
local medium_font = tweak_data.menu.pd2_medium_font
local small_font = tweak_data.menu.pd2_small_font
local tiny_font = tweak_data.menu.pd2_tiny_font
local massive_font_size = tweak_data.menu.pd2_massive_font_size
local large_font_size = tweak_data.menu.pd2_large_font_size
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font_size = tweak_data.menu.pd2_small_font_size
local tiny_font_size = tweak_data.menu.pd2_tiny_font_size
HudTrackedAchievement = HudTrackedAchievement or class(GrowPanel)
HudTrackedAchievement.init = function (self, parent, id, black_bg)
	HudTrackedAchievement.super.init(self, parent, {
		border = 10,
		padding = 4,
		fixed_w = parent.w(parent)
	})

	self._info = managers.achievment:get_info(id)
	self._visual = tweak_data.achievement.visual[id]
	self._progress = self._visual and self._visual.progress
	local placer = self.placer(self)
	local texture, texture_rect = tweak_data.hud_icons:get_icon_or(self._visual.icon_id, "guis/dlcs/unfinished/textures/placeholder")
	local bitmap = placer.add_bottom(placer, self.bitmap(self, {
		w = 50,
		h = 50,
		texture = texture,
		texture_rect = texture_rect
	}))
	local awarded = self._info.awarded

	if not awarded then
		bitmap.set_color(bitmap, Color.white:with_alpha(0.1))
		self._panel:bitmap({
			texture = "guis/dlcs/trk/textures/pd2/lock",
			w = bitmap.w(bitmap),
			h = bitmap.h(bitmap),
			x = bitmap.x(bitmap),
			y = bitmap.y(bitmap)
		})
	end

	placer.add_right(placer, self.fine_text(self, {
		text_id = self._visual.name_id,
		font = medium_font,
		font_size = medium_font_size
	}))

	local desc = self.text(self, {
		wrap = true,
		word_wrap = true,
		text_id = self._visual.desc_id,
		font = tiny_font,
		font_size = tiny_font_size,
		color = tweak_data.screen_colors.achievement_grey,
		w = self.row_w(self) - placer.current_left(placer)
	})

	self.limit_text_rows(desc, 2)
	placer.add_bottom(placer, self.make_fine_text(desc), 0)

	if self._progress then
		self._bar = placer.add_bottom(placer, TextProgressBar:new(self, {
			w = 300,
			h = 10,
			back_color = Color(255, 60, 60, 65)/255,
			max = self._progress.max
		}, {
			font_size = 12,
			font = tiny_font
		}, self._progress:get()))
	end

	if black_bg then
		self.rect(self, {
			layer = -1,
			color = Color.black:with_alpha(0.6)
		})
	end

	return 
end
HudTrackedAchievement.update_progress = function (self)
	if self._bar then
		if self._info.awarded then
			self._bar:set_progress(self._progress.max)
		else
			self._bar:set_progress(self._progress:get())
		end
	end

	return 
end
HUDStatsScreen = HUDStatsScreen or class(ExtendedPanel)
HUDStatsScreen.init = function (self)
	HUDStatsScreen.super.init(self, managers.hud:script(managers.hud.STATS_SCREEN_FULLSCREEN).panel, {
		use_given = true
	})
	self.clear(self)

	local padding = 10
	self._leftpos = {
		padding,
		padding
	}
	self._rightpos = {
		self.w(self) - padding,
		padding
	}
	self._left = ExtendedPanel:new(self, {
		w = self.w(self)/3 - padding*2,
		h = self.h(self) - padding*2
	})
	self._right = ExtendedPanel:new(self, {
		w = self._left:w(),
		h = self._left:h()
	})
	self._bottom = self.panel(self)

	self._left:set_righttop(self.left(self), self._leftpos[2])
	self._right:set_lefttop(self.right(self), self._rightpos[2])
	self.recreate_left(self)
	self.recreate_right(self)

	return 
end
HUDStatsScreen.recreate_left = function (self)
	self._left:clear()
	self._left:bitmap({
		texture = "guis/textures/test_blur_df",
		layer = -1,
		render_template = "VertexColorTexturedBlur3D",
		valign = "grow",
		w = self._left:w(),
		h = self._left:h()
	})

	local lb = HUDBGBox_create(self._left, {}, {
		blend_mode = "normal",
		color = Color.white
	})

	lb.child(lb, "bg"):set_color(Color(0, 0, 0):with_alpha(0.75))
	lb.child(lb, "bg"):set_alpha(1)

	local placer = UiPlacer:new(10, 10, 0, 8)
	local job_data = managers.job:current_job_data()
	local stage_data = managers.job:current_stage_data()

	if job_data and managers.job:current_job_id() == "safehouse" and Global.mission_manager.saved_job_values.playedSafeHouseBefore then
		self._left:set_visible(false)

		return 
	end

	if stage_data then
		if managers.crime_spree:is_active() then
			local level_data = managers.job:current_level_data()
			local mission = managers.crime_spree:get_mission(managers.crime_spree:current_played_mission())

			if mission then
				local level_str = managers.localization:to_upper_text(tweak_data.levels[mission.level.level_id].name_id) or ""

				placer.add_row(placer, self._left:fine_text({
					font = large_font,
					font_size = tweak_data.hud_stats.objectives_title_size,
					text = level_str
				}))
			end

			placer.add_row(placer, self._left:fine_text({
				font = medium_font,
				font_size = tweak_data.hud_stats.loot_size,
				text = managers.localization:to_upper_text("menu_lobby_difficulty_title"),
				color = tweak_data.screen_colors.text
			}), 8, 0)

			local str = managers.localization:text("menu_cs_level", {
				level = managers.experience:cash_string(managers.crime_spree:server_spree_level(), "")
			})

			placer.add_right(placer, self._left:fine_text({
				font = medium_font,
				font_size = tweak_data.hud_stats.loot_size,
				text = str,
				color = tweak_data.screen_colors.crime_spree_risk
			}))
		else
			local job_chain = managers.job:current_job_chain_data()
			local day = managers.job:current_stage()
			local days = (job_chain and #job_chain) or 0
			local day_title = placer.add_bottom(placer, self._left:fine_text({
				font = tweak_data.hud_stats.objectives_font,
				font_size = tweak_data.hud_stats.loot_size,
				text = managers.localization:to_upper_text("hud_days_title", {
					DAY = day,
					DAYS = days
				})
			}))

			if managers.job:is_level_ghostable(managers.job:current_level_id()) then
				local is_whisper_mode = managers.groupai and managers.groupai:state():whisper_mode()
				local ghost_color = (is_whisper_mode and Color.white) or tweak_data.screen_colors.important_1
				local ghost = placer.add_right(placer, self._left:bitmap({
					texture = "guis/textures/pd2/cn_minighost",
					name = "ghost_icon",
					h = 16,
					blend_mode = "add",
					w = 16,
					color = ghost_color
				}))

				ghost.set_center_y(ghost, day_title.center_y(day_title))
			end

			placer.new_row(placer, 8)

			local level_data = managers.job:current_level_data()

			if level_data then
				placer.add_bottom(placer, self._left:fine_text({
					font = large_font,
					font_size = tweak_data.hud_stats.objectives_title_size,
					text = managers.localization:to_upper_text(level_data.name_id)
				}))
			end

			placer.add_bottom(placer, self._left:fine_text({
				font = medium_font,
				font_size = tweak_data.hud_stats.loot_size,
				text = managers.localization:to_upper_text("menu_lobby_difficulty_title"),
				color = tweak_data.screen_colors.text
			}), 0)

			if job_data then
				local job_stars = managers.job:current_job_stars()
				local difficulty_stars = managers.job:current_difficulty_stars()
				local difficulty = tweak_data.difficulties[difficulty_stars + 2] or 1
				local difficulty_string = managers.localization:to_upper_text(tweak_data.difficulty_name_ids[difficulty])

				placer.add_right(placer, self._left:fine_text({
					font = medium_font,
					font_size = tweak_data.hud_stats.loot_size,
					text = difficulty_string,
					color = (0 < difficulty_stars and tweak_data.screen_colors.risk) or tweak_data.screen_colors.text
				}))
			end

			placer.new_row(placer, 8, 0)

			local payout = managers.localization:text("hud_day_payout", {
				MONEY = managers.experience:cash_string(managers.money:get_potential_payout_from_current_stage())
			})

			placer.add_bottom(placer, self._left:fine_text({
				keep_w = true,
				font = tweak_data.hud_stats.objectives_font,
				font_size = tweak_data.hud_stats.loot_size,
				text = payout
			}), 0)
		end

		placer.new_row(placer)
	end

	placer.add_bottom(placer, self._left:fine_text({
		vertical = "top",
		align = "left",
		font_size = tweak_data.hud_stats.objectives_title_size,
		font = tweak_data.hud_stats.objectives_font,
		text = managers.localization:to_upper_text("hud_objective")
	}), 16)
	placer.new_row(placer, 8)

	local row_w = self._left:w() - placer.current_left(placer)*2

	for i, data in pairs(managers.objectives:get_active_objectives()) do
		placer.add_bottom(placer, self._left:fine_text({
			word_wrap = true,
			wrap = true,
			align = "left",
			text = utf8.to_upper(data.text),
			font = tweak_data.hud.medium_font,
			font_size = tweak_data.hud.active_objective_title_font_size,
			w = row_w
		}))
		placer.add_bottom(placer, self._left:fine_text({
			word_wrap = true,
			wrap = true,
			font_size = 24,
			align = "left",
			text = data.description,
			font = tweak_data.hud_stats.objective_desc_font,
			w = row_w
		}), 0)
	end

	local loot_panel = ExtendedPanel:new(self._left, {
		w = self._left:w() - 16 - 8
	})
	placer = UiPlacer:new(16, 0, 8, 4)
	local mandatory_bags_data = managers.loot:get_mandatory_bags_data()
	local mandatory_amount = mandatory_bags_data and mandatory_bags_data.amount
	local secured_amount = managers.loot:get_secured_mandatory_bags_amount()
	local bonus_amount = managers.loot:get_secured_bonus_bags_amount()
	local bag_text = placer.add_bottom(placer, loot_panel.fine_text(loot_panel, {
		keep_w = true,
		text = managers.localization:text("hud_stats_bags_secured"),
		font = medium_font,
		font_size = medium_font_size
	}))

	placer.add_right(placer, nil, 0)

	local bag_texture, bag_rect = tweak_data.hud_icons:get_icon_data("bag_icon")
	local bag_icon = placer.add_left(placer, loot_panel.fit_bitmap(loot_panel, {
		w = 16,
		h = 16,
		texture = bag_texture,
		texture_rect = bag_rect
	}))

	bag_icon.set_center_y(bag_icon, bag_text.center_y(bag_text))

	if mandatory_amount and 0 < mandatory_amount then
		local str = (0 < bonus_amount and string.format("%d/%d+%d", secured_amount, mandatory_amount, bonus_amount)) or string.format("%d/%d", secured_amount, mandatory_amount)

		placer.add_left(placer, loot_panel.fine_text(loot_panel, {
			text = str,
			font = medium_font,
			font_size = medium_font_size
		}))
	else
		placer.add_left(placer, loot_panel.fine_text(loot_panel, {
			text = tostring(bonus_amount),
			font = medium_font,
			font_size = medium_font_size
		}))
	end

	placer.new_row(placer)

	local body_text = placer.add_bottom(placer, loot_panel.fine_text(loot_panel, {
		keep_w = true,
		text = managers.localization:to_upper_text("hud_body_bags"),
		font = medium_font,
		font_size = medium_font_size
	}))

	placer.add_right(placer, nil, 0)

	local body_texture, body_rect = tweak_data.hud_icons:get_icon_data("equipment_body_bag")
	local body_icon = placer.add_left(placer, loot_panel.fit_bitmap(loot_panel, {
		w = 17,
		h = 17,
		texture = body_texture,
		texture_rect = body_rect
	}))

	body_icon.set_center_y(body_icon, body_text.center_y(body_text))
	placer.add_left(placer, loot_panel.fine_text(loot_panel, {
		text = tostring(managers.player:get_body_bags_amount()),
		font = medium_font,
		font_size = medium_font_size
	}), 7)
	placer.new_row(placer)

	local secured_bags_money = managers.experience:cash_string(managers.money:get_secured_mandatory_bags_money() + managers.money:get_secured_bonus_bags_money())

	placer.add_bottom(placer, loot_panel.fine_text(loot_panel, {
		keep_w = true,
		text_id = "hud_stats_bags_secured_value",
		font = medium_font,
		font_size = medium_font_size
	}), 12)
	placer.add_right(placer, nil, 0)
	placer.add_left(placer, loot_panel.fine_text(loot_panel, {
		text = secured_bags_money,
		font = medium_font,
		font_size = medium_font_size
	}))
	placer.new_row(placer)

	local instant_cash = managers.experience:cash_string(managers.loot:get_real_total_small_loot_value())

	placer.add_bottom(placer, loot_panel.fine_text(loot_panel, {
		keep_w = true,
		text = managers.localization:to_upper_text("hud_instant_cash"),
		font = medium_font,
		font_size = medium_font_size
	}))
	placer.add_right(placer, nil, 0)
	placer.add_left(placer, loot_panel.fine_text(loot_panel, {
		text = instant_cash,
		font = medium_font,
		font_size = medium_font_size
	}))
	loot_panel.set_size(loot_panel, placer.most_rightbottom(placer))
	loot_panel.set_leftbottom(loot_panel, 0, self._left:h() - 16)

	return 
end
HUDStatsScreen.recreate_right = function (self)
	self._right:clear()
	self._right:bitmap({
		texture = "guis/textures/test_blur_df",
		layer = -1,
		render_template = "VertexColorTexturedBlur3D",
		valign = "grow",
		w = self._right:w(),
		h = self._right:h()
	})

	local rb = HUDBGBox_create(self._right, {}, {
		blend_mode = "normal",
		color = Color.white
	})

	rb.child(rb, "bg"):set_color(Color(0, 0, 0):with_alpha(0.75))
	rb.child(rb, "bg"):set_alpha(1)

	if managers.mutators:are_mutators_active() then
		self._create_mutators_list(self, self._right)
	else
		self._create_tracked_list(self, self._right)
	end

	local track_text = self._right:fine_text({
		text = managers.localization:to_upper_text("menu_es_playing_track") .. " " .. managers.music:current_track_string(),
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.text
	})

	track_text.set_leftbottom(track_text, 10, self._right:h() - 10)

	return 
end
HUDStatsScreen._create_tracked_list = function (self, panel)
	local placer = UiPlacer:new(10, 10, 0, 8)

	placer.add_bottom(placer, self._right:fine_text({
		text_id = "hud_stats_tracked",
		font = large_font,
		font_size = tweak_data.hud_stats.objectives_title_size
	}))

	local tracked = managers.achievment:get_tracked_fill()

	if #tracked == 0 then
		placer.add_bottom(placer, self._right:fine_text({
			wrap = true,
			word_wrap = true,
			text_id = "hud_stats_no_tracked",
			font = medium_font,
			font_size = medium_font_size,
			w = self._right:w() - placer.current_left(placer)*2
		}))
	end

	self._tracked_items = {}
	local placer = UiPlacer:new(0, placer.most(placer).bottom, 0, 0)
	local with_bg = true

	for _, id in pairs(tracked) do
		local t = placer.add_row(placer, HudTrackedAchievement:new(self._right, id, with_bg), 0, 0)

		if t._progress and t._progress.update and table.contains({
			"realtime",
			"second"
		}, t._progress.update) then
			table.insert(self._tracked_items, t)
		end

		with_bg = not with_bg
	end

	return 
end
HUDStatsScreen._create_mutators_list = function (self, panel)
	local placer = UiPlacer:new(10, 10)

	placer.add_bottom(placer, self._right:fine_text({
		text = managers.localization:to_upper_text("menu_mutators"),
		font = large_font,
		font_size = tweak_data.hud_stats.objectives_title_size
	}))

	for i, active_mutator in ipairs(managers.mutators:active_mutators()) do
		placer.add_row(placer, self._right:fine_text({
			text = active_mutator.mutator:name(),
			font = tweak_data.hud_stats.objectives_font,
			font_size = tweak_data.hud_stats.day_description_size
		}), 8, 2)
	end

	return 
end
HUDStatsScreen.hide = function (self)
	local left_panel = self._left
	local right_panel = self._right
	local bottom_panel = self._bottom

	left_panel.stop(left_panel)

	local teammates_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("teammates_panel")
	local objectives_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("objectives_panel")
	local chat_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("chat_panel")

	left_panel.animate(left_panel, callback(self, self, "_animate_hide_stats_left_panel"), right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)

	return 
end
HUDStatsScreen.show = function (self)
	self.recreate_left(self)
	self.recreate_right(self)

	local safe = managers.hud.STATS_SCREEN_SAFERECT
	local full = managers.hud.STATS_SCREEN_FULLSCREEN

	managers.hud:show(full)

	local left_panel = self._left
	local right_panel = self._right
	local bottom_panel = self._bottom

	left_panel.stop(left_panel)

	local teammates_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("teammates_panel")
	local objectives_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("objectives_panel")
	local chat_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("chat_panel")

	left_panel.animate(left_panel, callback(self, self, "_animate_show_stats_left_panel"), right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)

	return 
end
HUDStatsScreen.loot_value_updated = function (self)
	self.recreate_left(self)

	return 
end
HUDStatsScreen.on_ext_inventory_changed = function (self)
	self.recreate_left(self)

	return 
end
HUDStatsScreen._rec_round_object = function (self, object)
	if object.children then
		for i, d in ipairs(object.children(object)) do
			self._rec_round_object(self, d)
		end
	end

	local x, y = object.position(object)

	object.set_position(object, math.round(x), math.round(y))

	return 
end
HUDStatsScreen._animate_show_stats_left_panel = function (self, left_panel, right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)
	local start_x = left_panel.x(left_panel)
	local start_a = start_x/-left_panel.w(left_panel) - 1
	local TOTAL_T = start_x/-left_panel.w(left_panel)*0.33
	local t = 0

	while t < TOTAL_T do
		local dt = coroutine.yield()*TimerManager:game():multiplier()/1
		t = t + dt
		local a = math.lerp(start_a, 1, t/TOTAL_T)

		left_panel.set_alpha(left_panel, a)
		left_panel.set_x(left_panel, math.lerp(start_x, self._leftpos[1], t/TOTAL_T))
		right_panel.set_alpha(right_panel, a)
		right_panel.set_x(right_panel, right_panel.parent(right_panel):w() - (left_panel.x(left_panel) + right_panel.w(right_panel)))
		bottom_panel.set_alpha(bottom_panel, a)
		bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h() - (left_panel.x(left_panel) + bottom_panel.h(bottom_panel)))

		local a_half = (a - 1)*0.5 + 0.5

		teammates_panel.set_alpha(teammates_panel, a_half)
		objectives_panel.set_alpha(objectives_panel, a - 1)
		chat_panel.set_alpha(chat_panel, a_half)
	end

	left_panel.set_lefttop(left_panel, unpack(self._leftpos))
	left_panel.set_alpha(left_panel, 1)
	teammates_panel.set_alpha(teammates_panel, 0.5)
	objectives_panel.set_alpha(objectives_panel, 0)
	chat_panel.set_alpha(chat_panel, 0.5)
	right_panel.set_alpha(right_panel, 1)
	right_panel.set_righttop(right_panel, unpack(self._rightpos))
	bottom_panel.set_alpha(bottom_panel, 1)
	bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h() - bottom_panel.h(bottom_panel))
	self._rec_round_object(self, left_panel)
	self._rec_round_object(self, right_panel)
	self._rec_round_object(self, bottom_panel)

	return 
end
HUDStatsScreen._animate_hide_stats_left_panel = function (self, left_panel, right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)
	local start_x = left_panel.x(left_panel)
	local start_a = start_x/-left_panel.w(left_panel) - 1
	local TOTAL_T = (start_x/-left_panel.w(left_panel) - 1)*0.33
	local t = 0

	while t < TOTAL_T do
		local dt = coroutine.yield()*TimerManager:game():multiplier()/1
		t = t + dt
		local a = math.lerp(start_a, 0, t/TOTAL_T)

		left_panel.set_alpha(left_panel, a)
		left_panel.set_x(left_panel, math.lerp(start_x, -left_panel.w(left_panel), t/TOTAL_T))
		right_panel.set_alpha(right_panel, a)
		right_panel.set_x(right_panel, right_panel.parent(right_panel):w() - (left_panel.x(left_panel) + right_panel.w(right_panel)))
		bottom_panel.set_alpha(bottom_panel, a)
		bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h() - (left_panel.x(left_panel) + bottom_panel.h(bottom_panel)))

		local a_half = (a - 1)*0.5 + 0.5

		teammates_panel.set_alpha(teammates_panel, a_half)
		objectives_panel.set_alpha(objectives_panel, a - 1)
		chat_panel.set_alpha(chat_panel, a_half)
	end

	left_panel.set_x(left_panel, -left_panel.w(left_panel))
	left_panel.set_alpha(left_panel, 0)
	teammates_panel.set_alpha(teammates_panel, 1)
	objectives_panel.set_alpha(objectives_panel, 1)
	chat_panel.set_alpha(chat_panel, 1)
	right_panel.set_alpha(right_panel, 0)
	right_panel.set_x(right_panel, right_panel.parent(right_panel):w())
	bottom_panel.set_alpha(bottom_panel, 0)
	bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h())

	return 
end
HUDStatsScreen.update = function (self, t, dt)
	slot3 = pairs
	slot4 = self._tracked_items or {}

	for _, v in slot3(slot4) do
		v.update_progress(v)
	end

	return 
end

return 
