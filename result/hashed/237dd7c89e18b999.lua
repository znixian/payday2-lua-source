local massive_font = tweak_data.menu.pd2_massive_font
local large_font = tweak_data.menu.pd2_large_font
local medium_font = tweak_data.menu.pd2_medium_font
local small_font = tweak_data.menu.pd2_small_font
local tiny_font = tweak_data.menu.pd2_tiny_font
local massive_font_size = tweak_data.menu.pd2_massive_font_size
local large_font_size = tweak_data.menu.pd2_large_font_size
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font_size = tweak_data.menu.pd2_small_font_size
local tiny_font_size = tweak_data.menu.pd2_tiny_font_size

local function HUDBGBox_create_ex(panel, config)
	local box_panel = panel
	local color = config and config.color
	local bg_color = (config and config.bg_color) or Color(1, 0, 0, 0)
	local blend_mode = config and config.blend_mode

	box_panel.rect(box_panel, {
		blend_mode = "normal",
		name = "bg",
		halign = "grow",
		alpha = 0.4,
		layer = -1,
		valign = "grow",
		color = bg_color
	})

	local left_top = box_panel.bitmap(box_panel, {
		texture = "guis/textures/pd2/hud_corner",
		name = "left_top",
		visible = true,
		layer = 0,
		y = 0,
		halign = "left",
		x = 0,
		valign = "top",
		color = color,
		blend_mode = blend_mode
	})
	local left_bottom = box_panel.bitmap(box_panel, {
		texture = "guis/textures/pd2/hud_corner",
		name = "left_bottom",
		visible = true,
		layer = 0,
		x = 0,
		y = 0,
		halign = "left",
		rotation = -90,
		valign = "bottom",
		color = color,
		blend_mode = blend_mode
	})

	left_bottom.set_bottom(left_bottom, box_panel.h(box_panel))

	local right_top = box_panel.bitmap(box_panel, {
		texture = "guis/textures/pd2/hud_corner",
		name = "right_top",
		visible = true,
		layer = 0,
		x = 0,
		y = 0,
		halign = "right",
		rotation = 90,
		valign = "top",
		color = color,
		blend_mode = blend_mode
	})

	right_top.set_right(right_top, box_panel.w(box_panel))

	local right_bottom = box_panel.bitmap(box_panel, {
		texture = "guis/textures/pd2/hud_corner",
		name = "right_bottom",
		visible = true,
		layer = 0,
		x = 0,
		y = 0,
		halign = "right",
		rotation = 180,
		valign = "bottom",
		color = color,
		blend_mode = blend_mode
	})

	right_bottom.set_right(right_bottom, box_panel.w(box_panel))
	right_bottom.set_bottom(right_bottom, box_panel.h(box_panel))

	return box_panel
end

local box_speed = 1500

local function animate_open(panel, done_cb)
	local target_w = panel.w(panel)

	panel.set_w(panel, 0)
	panel.set_visible(panel, true)

	local speed = box_speed
	local TOTAL_T = target_w/speed
	local t = TOTAL_T

	while 0 < t do
		coroutine.yield()

		local dt = TimerManager:main():delta_time()
		t = t - dt

		panel.set_w(panel, (t/TOTAL_T - 1)*target_w)
	end

	panel.set_w(panel, target_w)
	done_cb()

	return 
end

local function animate_close(panel, done_cb)
	local speed = box_speed
	local cw = panel.w(panel)
	local TOTAL_T = cw/speed
	local t = TOTAL_T

	while 0 < t do
		coroutine.yield()

		local dt = TimerManager:main():delta_time()
		t = t - dt

		panel.set_w(panel, t/TOTAL_T*cw)
	end

	panel.set_w(panel, 0)
	done_cb()

	return 
end

local function wait_global(seconds)
	local t = 0

	while t < seconds do
		coroutine.yield()

		local dt = TimerManager:main():delta_time()
		t = t + dt
	end

	return 
end

HudChallangeNotification = HudChallangeNotification or class(ExtendedPanel)
HudChallangeNotification.ICON_SIZE = 80
HudChallangeNotification.BOX_MAX_W = 400
HudChallangeNotification.default_queue = HudChallangeNotification.default_queue or {}
HudChallangeNotification.queue = function (title, text, icon, queue)
	if Application:editor() then
		return 
	end

	queue = queue or HudChallangeNotification.default_queue

	table.insert(queue, {
		title,
		text,
		icon,
		queue
	})

	if #queue == 1 then
		HudChallangeNotification:new(unpack(queue[1]))
	end

	return 
end
HudChallangeNotification.init = function (self, title, text, icon, queue)
	self._ws = managers.gui_data:create_fullscreen_workspace()

	HudChallangeNotification.super.init(self, self._ws:panel())
	self.set_layer(self, 1000)

	self._queue = queue or {}
	self._top = ExtendedPanel:new(self)
	local title = self._top:fine_text({
		layer = 1,
		text = title or "ACHIEVEMENT UNLOCKED!",
		font = small_font,
		font_size = small_font_size,
		color = Color.black
	})

	title.move(title, 8, 0)
	self._top:set_size(title.right(title) + 8, title.h(title))
	self._top:bitmap({
		texture = "guis/textures/pd2/shared_tab_box",
		w = self._top:w(),
		h = self._top:h()
	})

	local box_size = ((icon and self.ICON_SIZE*2) or self.ICON_SIZE) + math.max(180, self._top:w())
	self._box = GrowPanel:new(self, {
		padding = 4,
		border = 4,
		w = box_size,
		y = self._top:bottom()
	})
	local placer = self._box:placer()
	local icon_texture, icon_texture_rect = tweak_data.hud_icons:get_icon_or(icon, nil)

	if icon_texture then
		placer.add_right(placer, self._box:fit_bitmap({
			texture = icon_texture,
			texture_rect = icon_texture_rect,
			w = self.ICON_SIZE,
			h = self.ICON_SIZE
		}))
		self._top:set_left(placer.current_right(placer))
	end

	placer.add_right(placer, self._box:fine_text({
		wrap = true,
		word_wrap = true,
		text = text or "HELLO WORLD!",
		font = medium_font,
		font_size = medium_font_size,
		w = self.BOX_MAX_W - placer.current_left(placer)
	}))

	local total_w = self._box:right()

	self.set_w(self, total_w)
	self.set_h(self, self._box:bottom())
	self.set_center_x(self, self.parent(self):w()/2)
	self.set_bottom(self, (self.parent(self):h()*5)/7)

	self._box_bg = self.panel(self)

	self._box_bg:set_shape(self._box:shape())
	HUDBGBox_create_ex(self._box_bg)
	self._box:set_visible(false)
	self._box_bg:animate(animate_open, function ()
		self._box:set_visible(true)
		wait_global(2)
		self._box:set_visible(false)
		self._box_bg:animate(animate_close, function ()
			self:close()

			return 
		end)

		return 
	end)

	return 
end
HudChallangeNotification.close = function (self)
	self.remove_self(self)

	if self._ws then
		managers.gui_data:destroy_workspace(self._ws)

		self._ws = nil
	end

	table.remove(self._queue, 1)

	if 0 < #self._queue then
		HudChallangeNotification:new(unpack(self._queue[1]))
	end

	return 
end

return 
