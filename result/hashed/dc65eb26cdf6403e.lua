TearGasGrenade = TearGasGrenade or class()
TearGasGrenade.init = function (self, unit)
	self._unit = unit
	self.radius = 0
	self.duration = 0
	self.damage = 0

	return 
end
TearGasGrenade.destroy = function (self)
	if self._smoke_effect then
		World:effect_manager():fade_kill(self._smoke_effect)
	end

	return 
end
TearGasGrenade.set_properties = function (self, props)
	self.radius = props.radius or 0
	self.duration = props.duration or 0
	self.damage = props.damage or 0

	if Network:is_server() then
		managers.network:session():send_to_peers("sync_tear_gas_grenade_properties", self._unit, self.radius, self.damage*10)
	end

	return 
end
TearGasGrenade.update = function (self, unit, t, dt)
	if Network:is_server() and self._remove_t and self._remove_t < t then
		self._unit:set_slot(0)

		return 
	end

	if self._damage_t and self._damage_t < t then
		self._damage_t = self._damage_t + 1
		local player = managers.player:player_unit()
		local radius_sq = self.radius*self.radius
		local in_range = player and mvector3.distance_sq(player.position(player), self._unit:position()) <= radius_sq

		if player and in_range then
			player.character_damage(player):damage_killzone({
				variant = "killzone",
				damage = self.damage,
				col_ray = {
					ray = math.UP
				}
			})
		end
	end

	return 
end
TearGasGrenade.detonate = function (self)
	local now = TimerManager:game():time()
	self._remove_t = now + self.duration
	self._damage_t = now + 1
	local position = self._unit:position()
	local sound_source = SoundDevice:create_source("tear_gas_source")

	sound_source.set_position(sound_source, position)
	sound_source.post_event(sound_source, "grenade_gas_explode")
	World:effect_manager():spawn({
		effect = Idstring("effects/particles/explosions/explosion_smoke_grenade"),
		position = position,
		normal = self._unit:rotation():y()
	})

	local parent = self._unit:orientation_object()
	self._smoke_effect = World:effect_manager():spawn({
		effect = Idstring("effects/payday2/environment/cs_gas_damage_area"),
		parent = parent
	})

	if Network:is_server() then
		managers.network:session():send_to_peers("sync_tear_gas_grenade_detonate", self._unit)
	end

	return 
end

return 
