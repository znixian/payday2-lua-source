MenuCallbackHandler.crime_spree_is_active = function (self)
	return managers.crime_spree:is_active()
end
MenuCallbackHandler.crime_spree_not_is_active = function (self)
	return not managers.crime_spree:is_active()
end
MenuCallbackHandler.crime_spree_in_progress = function (self)
	return managers.crime_spree:in_progress()
end
MenuCallbackHandler.crime_spree_not_in_progress = function (self)
	return not managers.crime_spree:in_progress()
end
MenuCallbackHandler.crime_spree_not_failed = function (self)
	return not managers.crime_spree:has_failed()
end
MenuCallbackHandler.crime_spree_failed = function (self)
	return managers.crime_spree:has_failed()
end
MenuCallbackHandler.show_crime_spree_start = function (self)
	return not self.show_crime_spree_select_modifier(self)
end
MenuCallbackHandler.show_crime_spree_reroll = function (self)
	return self.show_crime_spree_start(self)
end
MenuCallbackHandler.is_playing = function (self)
	return Global.game_settings.is_playing
end
MenuCallbackHandler.is_not_playing = function (self)
	return not Global.game_settings.is_playing
end
MenuCallbackHandler.show_crime_spree_select_modifier = function (self)
	local loud = managers.crime_spree:modifiers_to_select("loud")
	local stealth = managers.crime_spree:modifiers_to_select("stealth")

	return 0 < loud or 0 < stealth
end
MenuCallbackHandler.show_crime_spree_claim_rewards = function (self)
	return 0 < managers.crime_spree:reward_level()
end
MenuCallbackHandler.not_show_crime_spree_claim_rewards = function (self)
	return 0 >= managers.crime_spree:reward_level()
end
MenuCallbackHandler.return_to_crime_spree_lobby_visible = function (self)
	local state = game_state_machine:current_state_name()

	return state == "victoryscreen" or state == "gameoverscreen"
end
MenuCallbackHandler.accept_crime_spree_contract = function (self, item, node)
	if Global.game_settings.single_player then
		self._accept_crime_spree_contract_sp(self, item, node)
	else
		self._accept_crime_spree_contract_mp(self, item, node)
	end

	return 
end
MenuCallbackHandler._accept_crime_spree_contract_sp = function (self, item, node)
	if not managers.crime_spree:in_progress() and 0 <= managers.crime_spree:starting_level() then
		if not managers.crime_spree:can_start_spree(managers.crime_spree:starting_level() or 0) then
			return 
		end

		managers.crime_spree:start_crime_spree(managers.crime_spree:starting_level())
	end

	managers.crime_spree:enable_crime_spree_gamemode()
	MenuCallbackHandler:save_progress()
	managers.menu:active_menu().logic:select_node("crime_spree_lobby", true, {})

	return 
end
MenuCallbackHandler._accept_crime_spree_contract_mp = function (self, item, node)
	if not managers.crime_spree:in_progress() and 0 <= managers.crime_spree:starting_level() then
		if not managers.crime_spree:can_start_spree(managers.crime_spree:starting_level() or 0) then
			return 
		end

		managers.crime_spree:start_crime_spree(managers.crime_spree:starting_level())
	end

	local matchmake_attributes = self.get_matchmake_attributes(self)

	managers.crime_spree:enable_crime_spree_gamemode()

	if Network:is_server() then
		managers.network.matchmake:set_server_attributes(matchmake_attributes)
	else
		managers.network.matchmake:create_lobby(matchmake_attributes)
	end

	managers.menu_component:set_max_lines_game_chat(tweak_data.crime_spree.gui.max_chat_lines.lobby)
	MenuCallbackHandler:save_progress()

	return 
end
MenuCallbackHandler.accept_crimenet_contract_crime_spree = function (self, item, node)
	if not managers.crime_spree:in_progress() and 0 <= managers.crime_spree:starting_level() then
		managers.crime_spree:start_crime_spree(managers.crime_spree:starting_level())
	end

	self.accept_crimenet_contract(self, item, node)

	return 
end
MenuCallbackHandler.claim_crime_spree_rewards = function (self, item, node)
	if 0 < managers.crime_spree:reward_level() then
		local dialog_data = {
			title = managers.localization:text("dialog_cs_claim_rewards"),
			text = managers.localization:text("dialog_cs_claim_rewards_text"),
			id = "crime_spree_rewards"
		}
		local yes_button = {
			text = managers.localization:text("dialog_yes"),
			callback_func = callback(self, self, "_dialog_crime_spree_claim_rewards_yes")
		}
		local no_button = {
			text = managers.localization:text("dialog_no"),
			callback_func = callback(self, self, "_dialog_crime_spree_claim_rewards_no"),
			cancel_button = true
		}
		dialog_data.button_list = {
			yes_button,
			no_button
		}

		managers.system_menu:show(dialog_data)
	else
		local dialog_data = {
			title = managers.localization:text("dialog_cs_claim_rewards"),
			text = managers.localization:text("dialog_cs_cant_claim_rewards_text"),
			id = "crime_spree_rewards"
		}
		local no_button = {
			text = managers.localization:text("dialog_ok"),
			callback_func = callback(self, self, "_dialog_crime_spree_claim_rewards_no"),
			cancel_button = true
		}
		dialog_data.button_list = {
			no_button
		}

		managers.system_menu:show(dialog_data)
	end

	return 
end
MenuCallbackHandler._dialog_crime_spree_claim_rewards_yes = function (self)
	self._dialog_leave_lobby_yes(self)
	managers.menu:open_node("crime_spree_claim_rewards", {})

	return 
end
MenuCallbackHandler._dialog_crime_spree_claim_rewards_no = function (self)
	return 
end
MenuCallbackHandler.show_crime_spree_crash_dialog = function (self)
	local dialog_data = {
		title = managers.localization:text("dialog_cs_crash_fail"),
		text = managers.localization:text("dialog_cs_crash_fail_text"),
		id = "crime_spree_fail"
	}
	local no_button = {
		text = managers.localization:text("dialog_ok"),
		cancel_button = true
	}
	dialog_data.button_list = {
		no_button
	}

	managers.system_menu:show(dialog_data)

	return true
end
MenuCallbackHandler.end_crime_spree = function (self, item, node)
	local dialog_data = {
		title = managers.localization:text("dialog_warning_title")
	}

	if managers.crime_spree:can_refund_entry_fee() then
		local cost = managers.crime_spree:get_start_cost(managers.crime_spree:spree_level())
		dialog_data.text = managers.localization:text("dialog_are_you_sure_you_want_stop_cs_refund", {
			coins = cost
		})
	else
		dialog_data.text = managers.localization:text("dialog_are_you_sure_you_want_stop_cs")
	end

	dialog_data.id = "stop_crime_spree"
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_dialog_end_crime_spree_yes")
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = callback(self, self, "_dialog_end_crime_spree_no"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler._dialog_end_crime_spree_yes = function (self)
	if managers.crime_spree:can_refund_entry_fee() then
		local cost = managers.crime_spree:get_start_cost(managers.crime_spree:spree_level())

		managers.custom_safehouse:add_coins(cost)
	end

	managers.crime_spree:reset_crime_spree()
	self._dialog_leave_lobby_yes(self)
	MenuCallbackHandler:save_progress()

	return 
end
MenuCallbackHandler._dialog_end_crime_spree_no = function (self)
	return 
end
MenuCallbackHandler.return_to_crime_spree_lobby = function (self)
	if game_state_machine:current_state_name() == "disconnected" then
		return 
	end

	local dialog_data = {
		title = managers.localization:text("dialog_warning_title"),
		text = managers.localization:text("dialog_return_to_cs_lobby")
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = function ()
			if game_state_machine:current_state_name() ~= "disconnected" then
				self:load_start_menu_lobby()
			end

			return 
		end
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler.leave_crime_spree_lobby = function (self)
	if game_state_machine:current_state_name() == "ingame_lobby_menu" then
		self.end_game(self)

		return 
	end

	local dialog_data = {
		title = managers.localization:text("dialog_warning_title"),
		text = managers.localization:text("dialog_are_you_sure_you_want_leave_cs"),
		id = "leave_lobby"
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_dialog_leave_lobby_yes")
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = callback(self, self, "_dialog_leave_lobby_no"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return true
end
MenuCallbackHandler.end_game_crime_spree = function (self)
	local fail_on_quit = true

	if not Global.game_settings.is_playing then
		fail_on_quit = false
	end

	local dialog_data = {
		title = managers.localization:text("dialog_warning_title")
	}

	if Global.game_settings.is_playing then
		if managers.crime_spree:has_failed() then
			dialog_data.text = managers.localization:text("dialog_are_you_sure_you_want_to_leave_game")
		else
			dialog_data.text = managers.localization:text("dialog_are_you_sure_you_want_to_leave_game_crime_spree")
		end
	else
		dialog_data.text = managers.localization:text("dialog_are_you_sure_you_want_leave_cs")
	end

	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_dialog_end_game_crime_spree_yes", fail_on_quit)
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = callback(self, self, "_dialog_end_game_crime_spree_no"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler._dialog_end_game_crime_spree_no = function (self)
	return 
end
MenuCallbackHandler._dialog_end_game_crime_spree_yes = function (self, failed)
	managers.platform:set_playing(false)
	managers.job:clear_saved_ghost_bonus()
	managers.statistics:stop_session({
		quit = true
	})
	managers.savefile:save_progress()
	managers.job:deactivate_current_job()
	managers.gage_assignment:deactivate_assignments()
	managers.custom_safehouse:flush_completed_trophies()

	if failed == nil or failed then
		managers.crime_spree:on_mission_failed(managers.crime_spree:current_mission())
	end

	managers.crime_spree:on_left_lobby()

	if Network:multiplayer() then
		Network:set_multiplayer(false)
		managers.network:session():send_to_peers("set_peer_left")
		managers.network:queue_stop_network()
	end

	managers.network.matchmake:destroy_game()
	managers.network.voice_chat:destroy_voice()

	if managers.groupai then
		managers.groupai:state():set_AI_enabled(false)
	end

	managers.menu:post_event("menu_exit")
	managers.menu:close_menu("menu_pause")
	setup:load_start_menu()

	return 
end
MenuCallbackHandler.crime_spree_continue = function (self)
	local cost = managers.crime_spree:get_continue_cost(managers.crime_spree:spree_level())
	local params = {
		level = managers.crime_spree:spree_level(),
		cost = cost
	}

	if managers.custom_safehouse:coins() < cost then
		local dialog_data = {
			title = managers.localization:text("dialog_cant_continue_cs_title"),
			text = managers.localization:text("dialog_cant_continue_cs_text", params),
			id = "continue_crime_spree"
		}
		local no_button = {
			text = managers.localization:text("dialog_ok"),
			callback_func = callback(self, self, "_dialog_crime_spree_continue_no"),
			cancel_button = true
		}
		dialog_data.button_list = {
			no_button
		}

		managers.system_menu:show(dialog_data)
	else
		local dialog_data = {
			title = managers.localization:text("dialog_continue_cs_title"),
			text = managers.localization:text("dialog_continue_cs_text", params),
			id = "continue_crime_spree"
		}
		local yes_button = {
			text = managers.localization:text("dialog_yes"),
			callback_func = callback(self, self, "_dialog_crime_spree_continue_yes")
		}
		local no_button = {
			text = managers.localization:text("dialog_no"),
			callback_func = callback(self, self, "_dialog_crime_spree_continue_no"),
			cancel_button = true
		}
		dialog_data.button_list = {
			yes_button,
			no_button
		}

		managers.system_menu:show(dialog_data)
	end

	return true
end
MenuCallbackHandler._dialog_crime_spree_continue_yes = function (self)
	managers.crime_spree:continue_crime_spree()
	managers.menu:active_menu().logic:refresh_node("main")

	if managers.menu_component:crime_spree_mission_end_gui() then
		local node = managers.menu_component:crime_spree_mission_end_gui()._node

		managers.menu_component:close_crime_spree_mission_end_gui(node)
		managers.menu_component:create_crime_spree_mission_end_gui(node)
	end

	managers.menu_component:create_crime_spree_missions_gui(managers.menu:active_menu().logic:selected_node())
	managers.menu_component:refresh_crime_spree_details_gui()
	WalletGuiObject.refresh()

	return 
end
MenuCallbackHandler._dialog_crime_spree_continue_no = function (self)
	return 
end
MenuCallbackHandler.create_server_left_crime_spree_dialog = function (self)
	local dialog_data = {
		title = managers.localization:text("dialog_warning_title")
	}

	if Global.on_server_left_message then
		dialog_data.text = managers.localization:text("dialog_on_server_left_message_cs", {
			message = managers.localization:text(Global.on_server_left_message)
		})
		Global.on_server_left_message = nil
	else
		dialog_data.text = managers.localization:text("dialog_the_host_has_left_the_game_cs")
	end

	dialog_data.id = "server_left_dialog"
	local ok_button = {
		text = managers.localization:text("dialog_ok"),
		callback_func = callback(self, self, "_on_server_left_ok_pressed")
	}
	dialog_data.button_list = {
		ok_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler._on_server_left_ok_pressed = function (self)
	self._dialog_end_game_crime_spree_yes(self, false)

	return 
end
MenuCallbackHandler.show_peer_kicked_crime_spree_dialog = function (self, params)
	local dialog_data = {
		title = managers.localization:text((Global.on_remove_peer_message and "dialog_information_title") or "dialog_mp_kicked_out_title")
	}

	if Global.on_remove_peer_message then
		dialog_data.text = managers.localization:text("dialog_on_server_left_message_cs", {
			message = managers.localization:text(Global.on_remove_peer_message)
		})
	else
		dialog_data.text = managers.localization:text("dialog_on_server_left_message_cs", {
			message = managers.localization:text("dialog_mp_kicked_out_message")
		})
	end

	local ok_button = {
		text = managers.localization:text("dialog_ok"),
		callback_func = callback(self, self, "_on_server_left_ok_pressed")
	}
	dialog_data.button_list = {
		ok_button
	}

	managers.system_menu:show(dialog_data)

	Global.on_remove_peer_message = nil

	return 
end
MenuCallbackHandler.crime_spree_reroll = function (self)
	local mission_gui = managers.menu_component:crime_spree_missions_gui()

	if mission_gui and mission_gui.is_randomizing(mission_gui) then
		managers.menu:post_event("menu_error")

		return 
	end

	local can_afford = managers.crime_spree:randomization_cost() <= managers.custom_safehouse:coins()
	local dialog_data = {
		title = managers.localization:text("menu_cs_reroll_title"),
		id = "reroll_crime_spree"
	}

	if can_afford then
		dialog_data.text = managers.localization:text("menu_cs_reroll_text", {
			cost = managers.crime_spree:randomization_cost()
		})
		local yes_button = {
			text = managers.localization:text("dialog_yes"),
			callback_func = callback(self, self, "_dialog_crime_spree_reroll_yes")
		}
		local no_button = {
			text = managers.localization:text("dialog_no"),
			callback_func = callback(self, self, "_dialog_crime_spree_reroll_no"),
			cancel_button = true
		}
		dialog_data.button_list = {
			yes_button,
			no_button
		}
	else
		dialog_data.text = managers.localization:text("menu_cs_reroll_text_cant_afford", {
			cost = managers.crime_spree:randomization_cost()
		})
		local no_button = {
			text = managers.localization:text("dialog_ok"),
			callback_func = callback(self, self, "_dialog_crime_spree_reroll_no"),
			cancel_button = true
		}
		dialog_data.button_list = {
			no_button
		}
	end

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler._dialog_crime_spree_reroll_yes = function (self)
	managers.custom_safehouse:deduct_coins(managers.crime_spree:randomization_cost())
	managers.crime_spree:randomize_mission_set()

	if managers.menu_component:crime_spree_missions_gui() then
		managers.menu_component:crime_spree_missions_gui():randomize_crimespree()
	end

	WalletGuiObject.refresh()
	MenuCallbackHandler:save_progress()

	return 
end
MenuCallbackHandler._dialog_crime_spree_reroll_no = function (self)
	return 
end
MenuCallbackHandler.crime_spree_select_modifier = function (self)
	if self.show_crime_spree_select_modifier(self) then
		managers.menu:open_node("crime_spree_select_modifiers", {})
	end

	return 
end
MenuCallbackHandler.crime_spree_start_game = function (self)
	if managers.crime_spree:current_mission() == nil then
		managers.menu:post_event("menu_error")
	else
		self.start_the_game(self)
	end

	return 
end
MenuManager.show_confirm_mission_gage_asset_buy = function (self, params)
	local asset_tweak_data = tweak_data.crime_spree.assets[params.asset_id]
	local dialog_data = {
		title = managers.localization:text("dialog_assets_buy_title"),
		text = managers.localization:text("dialog_mission_asset_buy", {
			asset_desc = managers.localization:text(asset_tweak_data.unlock_desc_id or "menu_asset_unknown_unlock_desc", asset_tweak_data.data),
			cost = managers.localization:text("bm_cs_continental_coin_cost", {
				cost = managers.experience:cash_string(asset_tweak_data.cost, "")
			})
		}),
		focus_button = 2
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = params.yes_func
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = params.no_func,
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuManager.show_gage_assets_unlock_prevented = function (self, params)
	local asset_tweak_data = tweak_data.crime_spree.assets[params.asset_id]
	local dialog_data = {}

	if managers.crime_spree:can_unlock_asset_is_in_game() then
		dialog_data.title = managers.localization:text("dialog_cs_ga_in_progress")
		dialog_data.text = managers.localization:text("dialog_cs_ga_in_progress_text")
	else
		dialog_data.title = managers.localization:text("dialog_cs_ga_already_purchased")
		dialog_data.text = managers.localization:text("dialog_cs_ga_already_purchased_text")
	end

	dialog_data.focus_button = 1
	local no_button = {
		text = managers.localization:text("dialog_ok"),
		cancel_button = true
	}
	dialog_data.button_list = {
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuManager.show_gage_asset_desc = function (self, params)
	local asset_tweak_data = tweak_data.crime_spree.assets[params.asset_id]
	local dialog_data = {
		title = managers.localization:text(asset_tweak_data.name_id),
		text = managers.localization:text(asset_tweak_data.unlock_desc_id or "menu_asset_unknown_unlock_desc", asset_tweak_data.data),
		focus_button = 1
	}
	local no_button = {
		text = managers.localization:text("dialog_ok"),
		cancel_button = true
	}
	dialog_data.button_list = {
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler.choice_gamemode_filter = function (self, item)
	Global.game_settings.gamemode_filter = item.value(item)

	managers.user:set_setting("crimenet_gamemode_filter", item.value(item))
	managers.menu:back()
	managers.menu:open_node("crimenet_filters", {})

	return 
end
MenuCallbackHandler.choice_spree_difference_filter = function (self, item)
	Global.game_settings.crime_spree_max_lobby_diff = item.value(item)

	managers.user:set_setting("crime_spree_lobby_diff", item.value(item))
	managers.network.matchmake:search_lobby(managers.network.matchmake:search_friends_only())

	return 
end
MenuCallbackHandler.debug_crime_spree_reset = function (self)
	managers.crime_spree:reset_crime_spree()
	MenuCallbackHandler:save_progress()

	return 
end
MenuCallbackHandler.clear_crime_spree_record = function (self)
	local dialog_data = {
		title = managers.localization:text("dialog_warning_title"),
		text = managers.localization:text("dialog_clear_crime_spree_record_confirmation_text")
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_dialog_clear_crime_spree_record_yes")
	}
	local no_button = {
		cancel_button = true,
		text = managers.localization:text("dialog_no"),
		callback_func = callback(self, self, "_dialog_clear_crime_spree_record_no")
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler._dialog_clear_crime_spree_record_yes = function (self)
	Global.crime_spree.highest_level = nil

	managers.savefile:save_progress()

	return 
end
MenuCallbackHandler._dialog_clear_crime_spree_record_no = function (self)
	return 
end

return 
