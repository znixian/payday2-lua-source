SafehouseNPCSound = SafehouseNPCSound or class(CivilianHeisterSound)
SafehouseNPCSound.RND_MIN = 4
SafehouseNPCSound.RND_MAX = 6
SafehouseNPCSound.init = function (self, unit)
	SafehouseNPCSound.super.init(self, unit)

	self._speech_t = 0
	local player_unit = managers.player:player_unit()
	local player_character = managers.criminals:character_name_by_unit(player_unit)
	local player_voice = "rb1"

	for idx, data in ipairs(tweak_data.criminals.characters) do
		if data.name == player_character then
			player_voice = data.static_data.voice

			break
		end
	end

	self.set_interactor_voice(self, player_voice)

	local level = managers.custom_safehouse:avarage_level()

	self.set_room_level(self, level)

	if self.character and tweak_data.safehouse.heisters[self.character].voice then
		self.set_voice(self, tweak_data.safehouse.heisters[self.character].voice)
	end

	return 
end
SafehouseNPCSound._randomize_speech_time = function (self)
	local offset = (self.character and tweak_data.safehouse.heisters[self.character] and tweak_data.safehouse.heisters[self.character].idle_offset) or 0
	self._speech_t = Application:time() + offset + math.random(self.RND_MIN, self.RND_MAX)

	return 
end
SafehouseNPCSound.sound_callback = function (self, instance, event_type, unit, sound_source, label, identifier, position)
	if not alive(unit) then
		return 
	end

	unit.sound(unit):snd_clbk()

	if event_type == "end_of_event" then
		if unit.interaction(unit) and unit.interaction(unit)._reenable_ext then
			unit.interaction(unit):_reenable_ext()
		end

		unit.sound(unit)._speaking = nil
	end

	return 
end
SafehouseNPCSound.update = function (self, unit, t, dt)
	local is_speaking = self.speaking(self, t)

	if self._speech_t <= t then
		local skip = false

		if self.character and tweak_data.safehouse.heisters[self.character] and tweak_data.safehouse.heisters[self.character].anim_lines then
			for _, line in ipairs(tweak_data.safehouse.heisters[self.character].anim_lines) do
				if line.line_type == "idle" and not self._unit:anim_data()[line.anim_value] then
					skip = true

					break
				end
			end
		end

		if not is_speaking and not skip then
			self._sound_start_muttering(self)
			self._unit:set_extension_update_enabled(Idstring("sound"), false)
		else
			self._randomize_speech_time(self)
		end
	end

	return 
end
SafehouseNPCSound.sound_start = function (self, ...)
	if self._snd_start_clbk then
		self._snd_start_clbk()

		self._snd_start_clbk = nil
	end

	return 
end
SafehouseNPCSound.snd_clbk = function (self)
	if self._snd_clbk then
		self._snd_clbk()
	end

	return 
end
SafehouseNPCSound._on_muttering_done = function (self)
	self._randomize_speech_time(self)
	self._unit:set_extension_update_enabled(Idstring("sound"), true)

	self._snd_clbk = nil

	return 
end
SafehouseNPCSound._sound_start_muttering = function (self, override_sound)
	if not self.character then
		debug_pause("[SafehouseNPCSound:_sound_start_muttering] no character set!")

		return 
	end

	if self._unit:interaction()._reenable_ext then
		self._unit:interaction():set_active(false)
	end

	self._snd_clbk = callback(self, self, "_on_muttering_done")

	self.say(self, override_sound or string.format("Play_%s_idle", self.character), false, true)

	return 
end

return 
