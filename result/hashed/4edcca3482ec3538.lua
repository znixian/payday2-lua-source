core:import("CoreGameStateMachine")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateStandard")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateReady")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateWeapon")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateItem")
require("lib/units/beings/player/states/vr/hand/PlayerHandStatePoint")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateBelt")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateMelee")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateSwipe")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateAkimbo")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateWeaponAssist")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateCuffed")
require("lib/units/beings/player/states/vr/hand/PlayerHandStateDriving")

PlayerHandStateMachine = PlayerHandStateMachine or class(CoreGameStateMachine.GameStateMachine)
PlayerHandStateMachine.init = function (self, hand_unit, hand_id)
	self._hand_id = hand_id
	self._hand_unit = hand_unit
	local idle = PlayerHandStateStandard:new(self, "idle", hand_unit, "idle")
	local weapon = PlayerHandStateWeapon:new(self, "weapon", hand_unit, "grip_wpn")
	local item = PlayerHandStateItem:new(self, "item", hand_unit, "grip_wpn")
	local point = PlayerHandStatePoint:new(self, "point", hand_unit, "point")
	local ready = PlayerHandStateReady:new(self, "ready", hand_unit, "ready")
	local swipe = PlayerHandStateSwipe:new(self, "swipe", hand_unit, "point")
	local belt = PlayerHandStateBelt:new(self, "belt", hand_unit, "ready")
	local melee = PlayerHandStateMelee:new(self, "melee", hand_unit, "grip_wpn")
	local akimbo = PlayerHandStateAkimbo:new(self, "akimbo", hand_unit, "grip_wpn")
	local weapon_assist = PlayerHandStateWeaponAssist:new(self, "weapon_assist", hand_unit, "grip_wpn")
	local cuffed = PlayerHandStateCuffed:new(self, "cuffed", hand_unit, "grip")
	local driving = PlayerHandStateDriving:new(self, "driving", hand_unit, "idle")
	local idle_func = callback(nil, idle, "default_transition")
	local weapon_func = callback(nil, weapon, "default_transition")
	local item_func = callback(nil, item, "default_transition")
	local point_func = callback(nil, point, "default_transition")
	local ready_func = callback(nil, ready, "default_transition")
	local swipe_func = callback(nil, swipe, "default_transition")
	local belt_func = callback(nil, belt, "default_transition")
	local melee_func = callback(nil, melee, "default_transition")
	local akimbo_func = callback(nil, akimbo, "default_transition")
	local weapon_assist_func = callback(nil, weapon_assist, "default_transition")
	local cuffed_func = callback(nil, cuffed, "default_transition")
	local driving_func = callback(nil, driving, "default_transition")
	local item_to_swipe = callback(nil, item, "swipe_transition")
	local swipe_to_item = callback(nil, swipe, "item_transition")

	CoreGameStateMachine.GameStateMachine.init(self, idle)
	self.add_transition(self, idle, weapon, idle_func)
	self.add_transition(self, idle, item, idle_func)
	self.add_transition(self, idle, point, idle_func)
	self.add_transition(self, idle, ready, idle_func)
	self.add_transition(self, idle, belt, idle_func)
	self.add_transition(self, idle, swipe, idle_func)
	self.add_transition(self, idle, akimbo, idle_func)
	self.add_transition(self, idle, weapon_assist, idle_func)
	self.add_transition(self, idle, cuffed, idle_func)
	self.add_transition(self, idle, driving, idle_func)
	self.add_transition(self, weapon, idle, weapon_func)
	self.add_transition(self, weapon, item, weapon_func)
	self.add_transition(self, weapon, point, weapon_func)
	self.add_transition(self, weapon, ready, weapon_func)
	self.add_transition(self, weapon, belt, weapon_func)
	self.add_transition(self, weapon, swipe, weapon_func)
	self.add_transition(self, weapon, akimbo, weapon_func)
	self.add_transition(self, weapon, cuffed, weapon_func)
	self.add_transition(self, weapon, driving, weapon_func)
	self.add_transition(self, point, idle, point_func)
	self.add_transition(self, point, weapon, point_func)
	self.add_transition(self, point, ready, point_func)
	self.add_transition(self, point, akimbo, point_func)
	self.add_transition(self, ready, idle, ready_func)
	self.add_transition(self, ready, weapon, ready_func)
	self.add_transition(self, ready, point, ready_func)
	self.add_transition(self, ready, akimbo, ready_func)
	self.add_transition(self, ready, driving, ready_func)
	self.add_transition(self, ready, item, ready_func)
	self.add_transition(self, belt, idle, belt_func)
	self.add_transition(self, belt, weapon, belt_func)
	self.add_transition(self, belt, item, belt_func)
	self.add_transition(self, belt, melee, belt_func)
	self.add_transition(self, belt, akimbo, belt_func)
	self.add_transition(self, swipe, idle, swipe_func)
	self.add_transition(self, swipe, weapon, swipe_func)
	self.add_transition(self, swipe, akimbo, swipe_func)
	self.add_transition(self, swipe, melee, swipe_func)
	self.add_transition(self, swipe, item, swipe_to_item)
	self.add_transition(self, melee, idle, melee_func)
	self.add_transition(self, melee, weapon, melee_func)
	self.add_transition(self, melee, akimbo, melee_func)
	self.add_transition(self, melee, swipe, melee_func)
	self.add_transition(self, item, idle, item_func)
	self.add_transition(self, item, weapon, item_func)
	self.add_transition(self, item, akimbo, item_func)
	self.add_transition(self, item, swipe, item_to_swipe)
	self.add_transition(self, akimbo, idle, akimbo_func)
	self.add_transition(self, akimbo, weapon, akimbo_func)
	self.add_transition(self, akimbo, item, akimbo_func)
	self.add_transition(self, akimbo, point, akimbo_func)
	self.add_transition(self, akimbo, ready, akimbo_func)
	self.add_transition(self, akimbo, belt, akimbo_func)
	self.add_transition(self, akimbo, swipe, akimbo_func)
	self.add_transition(self, akimbo, cuffed, akimbo_func)
	self.add_transition(self, akimbo, driving, akimbo_func)
	self.add_transition(self, weapon_assist, idle, weapon_assist_func)
	self.add_transition(self, cuffed, idle, cuffed_func)
	self.add_transition(self, cuffed, weapon, cuffed_func)
	self.add_transition(self, cuffed, akimbo, cuffed_func)
	self.add_transition(self, driving, idle, driving_func)
	self.add_transition(self, driving, weapon, driving_func)
	self.add_transition(self, driving, akimbo, driving_func)
	self.set_default_state(self, "idle")

	self._weapon_hand_changed_clbk = callback(self, self, "on_default_weapon_hand_changed")

	managers.vr:add_setting_changed_callback("default_weapon_hand", self._weapon_hand_changed_clbk)

	return 
end
PlayerHandStateMachine.destroy = function (self)
	PlayerHandStateMachine.super.destroy(self)
	managers.vr:remove_setting_changed_callback("default_weapon_hand", self._weapon_hand_changed_clbk)

	return 
end
PlayerHandStateMachine.on_default_weapon_hand_changed = function (self, setting, old, new)
	if old == new then
		return 
	end

	local old_hand_id = PlayerHand.hand_id(old)

	if old_hand_id == self.hand_id(self) and self.default_state_name(self) == "weapon" then
		self.queue_default_state_switch(self, self.other_hand(self):default_state_name(), self.default_state_name(self))
	end

	return 
end
PlayerHandStateMachine.queue_default_state_switch = function (self, state, other_hand_state)
	self._queued_default_state_switch = {
		state,
		other_hand_state
	}

	return 
end
PlayerHandStateMachine.set_default_state = function (self, state_name)
	if self._default_state and state_name == self._default_state:name() then
		return 
	end

	local new_default = assert(self._states[state_name], "[PlayerHandStateMachine] Name '" .. tostring(state_name) .. "' does not correspond to a valid state.")

	if self._default_state == self.current_state(self) and self.can_change_state(self, new_default) then
		self.change_state(self, new_default)
	end

	self._default_state = new_default

	return 
end
PlayerHandStateMachine.change_to_default = function (self, params, front)
	if self.can_change_state(self, self._default_state) then
		self.change_state(self, self._default_state, params, front)
	end

	return 
end
PlayerHandStateMachine.default_state_name = function (self)
	return self._default_state and self._default_state:name()
end
PlayerHandStateMachine.hand_id = function (self)
	return self._hand_id
end
PlayerHandStateMachine.hand_unit = function (self)
	return self._hand_unit
end
PlayerHandStateMachine.enter_controller_state = function (self, state_name)
	managers.vr:hand_state_machine():enter_hand_state(self._hand_id, state_name)

	return 
end
PlayerHandStateMachine.exit_controller_state = function (self, state_name)
	managers.vr:hand_state_machine():exit_hand_state(self._hand_id, state_name)

	return 
end
PlayerHandStateMachine.set_other_hand = function (self, hsm)
	self._other_hand = hsm

	return 
end
PlayerHandStateMachine.other_hand = function (self)
	return self._other_hand
end
PlayerHandStateMachine.can_change_state_by_name = function (self, state_name)
	local state = assert(self._states[state_name], "[PlayerHandStateMachine] Name '" .. tostring(state_name) .. "' does not correspond to a valid state.")

	return self.can_change_state(self, state)
end
PlayerHandStateMachine.change_state = function (self, state, params, front)
	if front then
		self._queued_transitions = self._queued_transitions or {}

		table.insert(self._queued_transitions, 1, {
			state,
			params
		})
	else
		PlayerHandStateMachine.super.change_state(self, state, params)
	end

	return 
end
PlayerHandStateMachine.change_state_by_name = function (self, state_name, params, front)
	local state = assert(self._states[state_name], "[PlayerHandStateMachine] Name '" .. tostring(state_name) .. "' does not correspond to a valid state.")

	self.change_state(self, state, params, front)

	return 
end
PlayerHandStateMachine.is_controller_enabled = function (self)
	return true
end
PlayerHandStateMachine.update = function (self, t, dt)
	if self._queued_default_state_switch then
		self.set_default_state(self, self._queued_default_state_switch[1])
		self.other_hand(self):set_default_state(self._queued_default_state_switch[2])

		self._queued_default_state_switch = nil
	end

	return PlayerHandStateMachine.super.update(self, t, dt)
end
PlayerHandStateMachine.set_position = function (self, pos)
	self._position = pos

	return 
end
PlayerHandStateMachine.position = function (self)
	return self._position or self.hand_unit(self):position()
end

return 
