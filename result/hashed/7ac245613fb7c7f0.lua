PromoUnlockManager = PromoUnlockManager or class()
PromoUnlockManager.save_version = 1
PromoUnlockManager.init = function (self)
	self._items = {}

	for cat_id, category in pairs(tweak_data.promo_unlocks.promos) do
		for item_id, data in pairs(category) do
			self._items[item_id] = clone(data)
			self._items[item_id].category = cat_id
		end
	end

	return 
end
PromoUnlockManager.check_unlocks = function (self)
	return 
end
PromoUnlockManager._check_achievement = function (self, achievement_id)
	for item_id, item in pairs(self._items) do
		if item.achievement == achievement_id then
			item.unlocked = true

			print("[PromoUnlockManager] Unlocked promo item: ", item_id)
		end
	end

	return 
end
PromoUnlockManager._get_item = function (self, id)
	for item_id, item in pairs(self._items) do
		if item_id == id then
			return item
		end
	end

	return nil
end
PromoUnlockManager._unlock_item = function (self, id)
	local item = self._get_item(self, id)

	if item then
		item.unlocked = true

		print("[PromoUnlockManager] Unlocked promo item: ", id)
	end

	return 
end
PromoUnlockManager.has_unlocked = function (self, item_id)
	return self._items[item_id] and self._items[item_id].unlocked
end
PromoUnlockManager.get_data_for_weapon = function (self, weapon_id)
	for item_id, item in pairs(self._items) do
		if item.weapon_id and item.weapon_id == weapon_id then
			return item
		end
	end

	return nil
end
PromoUnlockManager.save = function (self, cache)
	local data = {
		version = self.save_version,
		items = {}
	}

	for item_id, item in pairs(self._items) do
		data.items[item_id] = item.unlocked or false
	end

	cache.promo_unlocks = data

	return 
end
PromoUnlockManager.load = function (self, cache, version)
	local data = cache.promo_unlocks

	if data and data.version == self.save_version then
		for saved_item_id, unlocked in pairs(data.items) do
			if unlocked then
				self._unlock_item(self, saved_item_id)
			end
		end
	end

	return 
end

return 
