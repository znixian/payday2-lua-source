ValueModifier = ValueModifier or class()
ValueModifier.init = function (self)
	self._modifiers = {}

	return 
end
ValueModifier.add_modifier = function (self, type, callback)
	local key = {}
	local modifiers = self._modifiers[type] or {}
	self._modifiers[type] = modifiers
	modifiers[key] = callback

	return key
end
ValueModifier.remove_modifier = function (self, type, key)
	local modifiers = self._modifiers[type] or {}

	if not modifiers then
		return 
	end

	modifiers[key] = nil

	return 
end
ValueModifier.modify_value = function (self, type, base_value, ...)
	local modifiers = self._modifiers[type]
	local new_value = base_value

	if modifiers then
		for _, callback in pairs(modifiers) do
			new_value = new_value + (callback(base_value, ...) or 0)
		end
	end

	return new_value
end

return 
