SequenceActivator = SequenceActivator or class()
SequenceActivator.init = function (self, unit)
	local count = #self._sequences

	for i = 1, count, 1 do
		unit.damage(unit):run_sequence_simple(self._sequences[i])

		self._sequences[i] = nil
	end

	self._sequences = nil

	return 
end

return 
