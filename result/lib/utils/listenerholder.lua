ListenerHolder = ListenerHolder or class()
ListenerHolder.add = function (self, key, clbk)
	if self._calling then
		self._set_new(self, key, clbk)
	else
		self._add(self, key, clbk)
	end

	return 
end
ListenerHolder.remove = function (self, key)
	if self._calling then
		self._set_trash(self, key)
	else
		self._remove(self, key)
	end

	return 
end
ListenerHolder.call = function (self, ...)
	if self._listeners then
		self._calling = true

		for key, clbk in pairs(self._listeners) do
			if self._not_trash(self, key) then
				clbk(...)
			end
		end

		self._calling = nil

		self._append_new_additions(self)
		self._dispose_trash(self)
	end

	return 
end
ListenerHolder._remove = function (self, key)
	self._listeners[key] = nil

	if not next(self._listeners) then
		self._listeners = nil
	end

	return 
end
ListenerHolder._add = function (self, key, clbk)
	self._listeners = self._listeners or {}
	self._listeners[key] = clbk

	return 
end
ListenerHolder._set_trash = function (self, key)
	self._trash = self._trash or {}
	self._trash[key] = true

	if self._additions then
		self._additions[key] = nil
	end

	return 
end
ListenerHolder._set_new = function (self, key, clbk)
	self._additions = self._additions or {}
	self._additions[key] = clbk

	if self._trash then
		self._trash[key] = nil
	end

	return 
end
ListenerHolder._append_new_additions = function (self)
	if self._additions then
		for key, clbk in pairs(self._additions) do
			self._add(self, key, clbk)
		end

		self._additions = nil
	end

	return 
end
ListenerHolder._dispose_trash = function (self)
	if self._trash then
		for key, _ in pairs(self._trash) do
			self._remove(self, key)
		end

		self._trash = nil
	end

	return 
end
ListenerHolder._not_trash = function (self, key)
	return not self._trash or not self._trash[key]
end
ListenerHolder.is_empty = function (self)
	return not self._listeners
end

return 
