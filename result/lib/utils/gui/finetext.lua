FineText = FineText or class(GUIObjectWrapper)
FineText.init = function (self, parent, config)
	config = config or {}
	config.font_size = config.font_size or tweak_data.menu.pd2_medium_font_size
	config.font = config.font or tweak_data.menu.pd2_medium_font
	config.color = config.color or tweak_data.screen_colors.text
	local text_obj = parent.text(parent, config)

	self.super.init(self, text_obj)
	self.shrink_wrap(self)

	return 
end
FineText.shrink_wrap = function (self)
	local x, y, w, h = self._gui_obj:text_rect()

	self._gui_obj:set_size(w, h)
	self._gui_obj:set_world_position(math.round(x), math.round(y))

	return 
end
FineText.set_text = function (self, ...)
	self._gui_obj:set_text(...)
	self.shrink_wrap(self)

	return 
end
FineText.text = function (self)
	self._gui_obj:text()

	return 
end

return 
