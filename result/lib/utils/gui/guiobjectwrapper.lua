GUIObjectWrapper = GUIObjectWrapper or class()
GUIObjectWrapper.init = function (self, gui_obj)
	self._gui_obj = gui_obj

	return 
end
GUIObjectWrapper.bottom = function (self, ...)
	return self._gui_obj:bottom(...)
end
GUIObjectWrapper.center = function (self, ...)
	return self._gui_obj:center(...)
end
GUIObjectWrapper.center_x = function (self, ...)
	return self._gui_obj:center_x(...)
end
GUIObjectWrapper.center_y = function (self, ...)
	return self._gui_obj:center_y(...)
end
GUIObjectWrapper.h = function (self, ...)
	return self._gui_obj:h(...)
end
GUIObjectWrapper.height = function (self, ...)
	return self._gui_obj:height(...)
end
GUIObjectWrapper.left = function (self, ...)
	return self._gui_obj:left(...)
end
GUIObjectWrapper.leftbottom = function (self, ...)
	return self._gui_obj:leftbottom(...)
end
GUIObjectWrapper.lefttop = function (self, ...)
	return self._gui_obj:lefttop(...)
end
GUIObjectWrapper.position = function (self, ...)
	return self._gui_obj:position(...)
end
GUIObjectWrapper.right = function (self, ...)
	return self._gui_obj:right(...)
end
GUIObjectWrapper.rightbottom = function (self, ...)
	return self._gui_obj:rightbottom(...)
end
GUIObjectWrapper.righttop = function (self, ...)
	return self._gui_obj:righttop(...)
end
GUIObjectWrapper.size = function (self, ...)
	return self._gui_obj:size(...)
end
GUIObjectWrapper.top = function (self, ...)
	return self._gui_obj:top(...)
end
GUIObjectWrapper.w = function (self, ...)
	return self._gui_obj:w(...)
end
GUIObjectWrapper.width = function (self, ...)
	return self._gui_obj:width(...)
end
GUIObjectWrapper.world_bottom = function (self, ...)
	return self._gui_obj:world_bottom(...)
end
GUIObjectWrapper.world_center = function (self, ...)
	return self._gui_obj:world_center(...)
end
GUIObjectWrapper.world_center_x = function (self, ...)
	return self._gui_obj:world_center_x(...)
end
GUIObjectWrapper.world_center_y = function (self, ...)
	return self._gui_obj:world_center_y(...)
end
GUIObjectWrapper.world_left = function (self, ...)
	return self._gui_obj:world_left(...)
end
GUIObjectWrapper.world_leftbottom = function (self, ...)
	return self._gui_obj:world_leftbottom(...)
end
GUIObjectWrapper.world_lefttop = function (self, ...)
	return self._gui_obj:world_lefttop(...)
end
GUIObjectWrapper.world_position = function (self, ...)
	return self._gui_obj:world_position(...)
end
GUIObjectWrapper.world_right = function (self, ...)
	return self._gui_obj:world_right(...)
end
GUIObjectWrapper.world_rightbottom = function (self, ...)
	return self._gui_obj:world_rightbottom(...)
end
GUIObjectWrapper.world_righttop = function (self, ...)
	return self._gui_obj:world_righttop(...)
end
GUIObjectWrapper.world_top = function (self, ...)
	return self._gui_obj:world_top(...)
end
GUIObjectWrapper.world_x = function (self, ...)
	return self._gui_obj:world_x(...)
end
GUIObjectWrapper.world_y = function (self, ...)
	return self._gui_obj:world_y(...)
end
GUIObjectWrapper.x = function (self, ...)
	return self._gui_obj:x(...)
end
GUIObjectWrapper.y = function (self, ...)
	return self._gui_obj:y(...)
end
GUIObjectWrapper.set_bottom = function (self, ...)
	self._gui_obj:set_bottom(...)

	return 
end
GUIObjectWrapper.set_center = function (self, ...)
	self._gui_obj:set_center(...)

	return 
end
GUIObjectWrapper.set_center_x = function (self, ...)
	self._gui_obj:set_center_x(...)

	return 
end
GUIObjectWrapper.set_center_y = function (self, ...)
	self._gui_obj:set_center_y(...)

	return 
end
GUIObjectWrapper.set_debug = function (self, ...)
	self._gui_obj:set_debug(...)

	return 
end
GUIObjectWrapper.set_h = function (self, ...)
	self._gui_obj:set_h(...)

	return 
end
GUIObjectWrapper.set_height = function (self, ...)
	self._gui_obj:set_height(...)

	return 
end
GUIObjectWrapper.set_left = function (self, ...)
	self._gui_obj:set_left(...)

	return 
end
GUIObjectWrapper.set_leftbottom = function (self, ...)
	self._gui_obj:set_leftbottom(...)

	return 
end
GUIObjectWrapper.set_lefttop = function (self, ...)
	self._gui_obj:set_lefttop(...)

	return 
end
GUIObjectWrapper.set_position = function (self, ...)
	self._gui_obj:set_position(...)

	return 
end
GUIObjectWrapper.set_right = function (self, ...)
	self._gui_obj:set_right(...)

	return 
end
GUIObjectWrapper.set_rightbottom = function (self, ...)
	self._gui_obj:set_rightbottom(...)

	return 
end
GUIObjectWrapper.set_righttop = function (self, ...)
	self._gui_obj:set_righttop(...)

	return 
end
GUIObjectWrapper.set_size = function (self, ...)
	self._gui_obj:set_size(...)

	return 
end
GUIObjectWrapper.set_top = function (self, ...)
	self._gui_obj:set_top(...)

	return 
end
GUIObjectWrapper.set_w = function (self, ...)
	self._gui_obj:set_w(...)

	return 
end
GUIObjectWrapper.set_width = function (self, ...)
	self._gui_obj:set_width(...)

	return 
end
GUIObjectWrapper.set_world_bottom = function (self, ...)
	self._gui_obj:set_world_bottom(...)

	return 
end
GUIObjectWrapper.set_world_center = function (self, ...)
	self._gui_obj:set_world_center(...)

	return 
end
GUIObjectWrapper.set_world_center_x = function (self, ...)
	self._gui_obj:set_world_center_x(...)

	return 
end
GUIObjectWrapper.set_world_center_y = function (self, ...)
	self._gui_obj:set_world_center_y(...)

	return 
end
GUIObjectWrapper.set_world_left = function (self, ...)
	self._gui_obj:set_world_left(...)

	return 
end
GUIObjectWrapper.set_world_leftbottom = function (self, ...)
	self._gui_obj:set_world_leftbottom(...)

	return 
end
GUIObjectWrapper.set_world_lefttop = function (self, ...)
	self._gui_obj:set_world_lefttop(...)

	return 
end
GUIObjectWrapper.set_world_position = function (self, ...)
	self._gui_obj:set_world_position(...)

	return 
end
GUIObjectWrapper.set_world_right = function (self, ...)
	self._gui_obj:set_world_right(...)

	return 
end
GUIObjectWrapper.set_world_rightbottom = function (self, ...)
	self._gui_obj:set_world_rightbottom(...)

	return 
end
GUIObjectWrapper.set_world_righttop = function (self, ...)
	self._gui_obj:set_world_righttop(...)

	return 
end
GUIObjectWrapper.set_world_top = function (self, ...)
	self._gui_obj:set_world_top(...)

	return 
end
GUIObjectWrapper.set_world_x = function (self, ...)
	self._gui_obj:set_world_x(...)

	return 
end
GUIObjectWrapper.set_world_y = function (self, ...)
	self._gui_obj:set_world_y(...)

	return 
end
GUIObjectWrapper.set_x = function (self, ...)
	self._gui_obj:set_x(...)

	return 
end
GUIObjectWrapper.set_y = function (self, ...)
	self._gui_obj:set_y(...)

	return 
end
GUIObjectWrapper.layer = function (self, ...)
	return self._gui_obj:layer(...)
end
GUIObjectWrapper.set_layer = function (self, ...)
	self._gui_obj:set_layer(...)

	return 
end
GUIObjectWrapper.set_world_layer = function (self, ...)
	self._gui_obj:set_world_layer(...)

	return 
end
GUIObjectWrapper.world_layer = function (self, ...)
	return self._gui_obj:world_layer(...)
end
GUIObjectWrapper.inside = function (self, ...)
	return self._gui_obj:inside(...)
end
GUIObjectWrapper.outside = function (self, ...)
	return self._gui_obj:outside(...)
end

return 
