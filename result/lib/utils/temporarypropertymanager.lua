TemporaryPropertyManager = TemporaryPropertyManager or class()
TemporaryPropertyManager.init = function (self)
	self._properties = {}

	return 
end
TemporaryPropertyManager.activate_property = function (self, prop, time, value)
	self._properties[prop] = {
		value,
		Application:time() + time
	}

	return 
end
TemporaryPropertyManager.get_property = function (self, prop, default)
	local time = Application:time()

	if self._properties[prop] and time <= self._properties[prop][2] then
		return self._properties[prop][1]
	elseif self._properties[prop] then
		self._properties[prop] = nil
	end

	return default
end
TemporaryPropertyManager.add_to_property = function (self, prop, time, value)
	if not self.has_active_property(self, prop) then
		self.activate_property(self, prop, time, value)
	else
		self._properties[prop][1] = self._properties[prop][1] + value
		self._properties[prop][2] = self._properties[prop][2] + time
	end

	return 
end
TemporaryPropertyManager.mul_to_property = function (self, prop, time, value)
	if not self.has_active_property(self, prop) then
		self.activate_property(self, prop, time, value)
	else
		self._properties[prop][1] = self._properties[prop][1]*value
	end

	return 
end
TemporaryPropertyManager.set_time = function (self, prop, time)
	if self.has_active_property(self, prop) then
		self._properties[prop][2] = Application:time() + time
	end

	return 
end
TemporaryPropertyManager.has_active_property = function (self, prop)
	local time = Application:time()
	slot3 = self._properties[prop] and slot3

	if self._properties[prop] and time <= self._properties[prop][2] then
		return true
	elseif self._properties[prop] then
		self._properties[prop] = nil
	end

	return false
end
TemporaryPropertyManager.remove_property = function (self, prop)
	if self._properties[prop] then
		self._properties[prop] = nil
	end

	return 
end

return 
