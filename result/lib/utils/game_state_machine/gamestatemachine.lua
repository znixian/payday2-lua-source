core:import("CoreGameStateMachine")
require("lib/states/BootupState")
require("lib/states/MenuTitlescreenState")
require("lib/states/MenuMainState")
require("lib/states/EditorState")
require("lib/states/WorldCameraState")
require("lib/utils/game_state_machine/GameStateFilters")
require("lib/states/IngamePlayerBase")
require("lib/states/IngameStandard")
require("lib/states/IngameMaskOff")
require("lib/states/IngameBleedOut")
require("lib/states/IngameFatalState")
require("lib/states/IngameWaitingForPlayers")
require("lib/states/IngameWaitingForRespawn")
require("lib/states/IngameWaitingForSpawnAllowed")
require("lib/states/IngameArrested")
require("lib/states/IngameElectrified")
require("lib/states/IngameIncapacitated")
require("lib/states/IngameClean")
require("lib/states/IngameCivilian")
require("lib/states/MissionEndState")
require("lib/states/VictoryState")
require("lib/states/GameOverState")
require("lib/states/ServerLeftState")
require("lib/states/DisconnectedState")
require("lib/states/KickedState")
require("lib/states/IngameLobbyMenu")
require("lib/states/IngameAccessCamera")
require("lib/states/IngameDriving")
require("lib/states/IngameParachuting")
require("lib/states/IngameFreefall")
require("lib/gamemodes/Gamemode")
require("lib/gamemodes/GamemodeStandard")
require("lib/gamemodes/GamemodeCrimeSpree")

GameStateMachine = GameStateMachine or class(CoreGameStateMachine.GameStateMachine)
GameStateMachine.init = function (self)
	if not Global.game_state_machine then
		Global.game_state_machine = {
			is_boot_intro_done = false,
			is_boot_from_sign_out = false
		}
	end

	self._is_boot_intro_done = Global.game_state_machine.is_boot_intro_done
	Global.game_state_machine.is_boot_intro_done = true
	self._is_boot_from_sign_out = Global.game_state_machine.is_boot_from_sign_out
	Global.game_state_machine.is_boot_from_sign_out = false
	local setup_boot = not self._is_boot_intro_done and not Application:editor()
	local setup_title = (setup_boot or self._is_boot_from_sign_out) and not Application:editor()
	local states = {}
	self._controller_enabled_count = 1
	local empty = GameState:new("empty", self)
	self._empty_state = empty

	CoreGameStateMachine.GameStateMachine.init(self, empty)

	local gamemode = (Global.game_settings and Global.game_settings.gamemode) or "standard"

	self.change_gamemode_by_name(self, gamemode, setup_boot, setup_title)
	managers.menu:add_active_changed_callback(callback(self, self, "menu_active_changed_callback"))
	managers.system_menu:add_active_changed_callback(callback(self, self, "dialog_active_changed_callback"))

	return 
end
GameStateMachine.init_finilize = function (self)
	if managers.hud then
		managers.hud:add_chatinput_changed_callback(callback(self, self, "chatinput_changed_callback"))
	end

	return 
end
GameStateMachine.set_boot_intro_done = function (self, is_boot_intro_done)
	Global.game_state_machine.is_boot_intro_done = is_boot_intro_done
	self._is_boot_intro_done = is_boot_intro_done

	return 
end
GameStateMachine.is_boot_intro_done = function (self)
	return self._is_boot_intro_done
end
GameStateMachine.set_boot_from_sign_out = function (self, is_boot_from_sign_out)
	Global.game_state_machine.is_boot_from_sign_out = is_boot_from_sign_out

	return 
end
GameStateMachine.is_boot_from_sign_out = function (self)
	return self._is_boot_from_sign_out
end
GameStateMachine.menu_active_changed_callback = function (self, active)
	self._set_controller_enabled(self, not active)

	return 
end
GameStateMachine.dialog_active_changed_callback = function (self, active)
	self._set_controller_enabled(self, not active)

	return 
end
GameStateMachine.chatinput_changed_callback = function (self, active)
	self._set_controller_enabled(self, not active)

	return 
end
GameStateMachine.is_controller_enabled = function (self)
	return 0 < self._controller_enabled_count
end
GameStateMachine._set_controller_enabled = function (self, enabled)
	local was_enabled = self.is_controller_enabled(self)

	if enabled then
		self._controller_enabled_count = self._controller_enabled_count + 1
	else
		self._controller_enabled_count = self._controller_enabled_count - 1
	end

	if not was_enabled ~= not self.is_controller_enabled(self) then
		local state = self.current_state(self)

		if state then
			state.set_controller_enabled(state, enabled)
		end
	end

	return 
end
GameStateMachine.gamemode = function (self)
	return self._gamemode
end
GameStateMachine.change_gamemode_by_name = function (self, gamemode, setup_boot, setup_title)
	Global.game_settings.gamemode = gamemode
	local gamemode_class = Gamemode.MAP[gamemode] or GamemodeStandard
	self._gamemode = gamemode_class.new(gamemode_class)

	self._gamemode:setup_gsm(self, self._empty_state, setup_boot, setup_title)

	return 
end
GameStateMachine.can_change_state_by_name = function (self, state_name)
	local name = self.gamemode(self):get_state(state_name)
	local state = assert(self._states[name], "[GameStateMachine] Name '" .. tostring(name) .. "' does not correspond to a valid state.")

	return self.can_change_state(self, state)
end
GameStateMachine.change_state_by_name = function (self, state_name, params)
	local name = self.gamemode(self):get_state(state_name)
	local state = assert(self._states[name], "[GameStateMachine] Name '" .. tostring(name) .. "' does not correspond to a valid state.")

	self.change_state(self, state, params)

	return 
end
GameStateMachine.verify_game_state = function (self, filter, state)
	state = state or self.last_queued_state_name(self)

	return filter[state]
end

return 
