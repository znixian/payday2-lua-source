EventListenerHolder = EventListenerHolder or class()
EventListenerHolder.add = function (self, key, event_types, clbk)
	if self._calling then
		self._set_new(self, key, event_types, clbk)
	else
		self._add(self, key, event_types, clbk)
	end

	return 
end
EventListenerHolder.remove = function (self, key)
	if self._calling then
		self._set_trash(self, key)
	else
		self._remove(self, key)
	end

	return 
end
EventListenerHolder.call = function (self, event, ...)
	if self._listeners then
		local event_listeners = self._listeners[event]

		if event_listeners then
			self._calling = true

			for key, clbk in pairs(event_listeners) do
				if self._not_trash(self, key) then
					clbk(...)
				end
			end

			self._calling = nil

			self._append_new_additions(self)
			self._dispose_trash(self)
		end
	end

	return 
end
EventListenerHolder._remove = function (self, key)
	local listeners = self._listeners

	if not self._listener_keys then
		return 
	end

	local listeners_keys = self._listener_keys[key]

	if listeners_keys then
		for _, event in pairs(listeners_keys) do
			listeners[event][key] = nil

			if not next(listeners[event]) then
				listeners[event] = nil
			end
		end

		if next(listeners) then
			self._listener_keys[key] = nil
		else
			self._listeners = nil
			self._listener_keys = nil
		end
	end

	return 
end
EventListenerHolder._add = function (self, key, event_types, clbk)
	if self._listener_keys and self._listener_keys[key] then
		debug_pause("[EventListenerHolder:_add] duplicate", key, inspect(event_types), clbk)

		return 
	end

	local listeners = self._listeners

	if not listeners then
		self._listeners = {}
		self._listener_keys = {}
		listeners = self._listeners
	end

	for _, event in pairs(event_types) do
		listeners[event] = listeners[event] or {}
		listeners[event][key] = clbk
	end

	self._listener_keys[key] = event_types

	return 
end
EventListenerHolder._set_trash = function (self, key)
	self._trash = self._trash or {}
	self._trash[key] = true

	if self._additions then
		self._additions[key] = nil
	end

	return 
end
EventListenerHolder._set_new = function (self, key, event_types, clbk)
	if self._additions and self._additions[key] then
		debug_pause("[EventListenerHolder:_set_new] duplicate", key, inspect(event_types), clbk)

		return 
	end

	self._additions = self._additions or {}
	self._additions[key] = {
		clbk,
		event_types
	}

	if self._trash then
		self._trash[key] = nil
	end

	return 
end
EventListenerHolder._append_new_additions = function (self)
	if self._additions then
		local listeners = self._listeners

		if not listeners then
			self._listeners = {}
			self._listener_keys = {}
			listeners = self._listeners
		end

		for key, new_entry in pairs(self._additions) do
			for _, event in ipairs(new_entry[2]) do
				listeners[event] = listeners[event] or {}
				listeners[event][key] = new_entry[1]
			end

			self._listener_keys[key] = new_entry[2]
		end

		self._additions = nil
	end

	return 
end
EventListenerHolder._dispose_trash = function (self)
	if self._trash then
		for key, _ in pairs(self._trash) do
			self._remove(self, key)
		end

		self._trash = nil
	end

	return 
end
EventListenerHolder._not_trash = function (self, key)
	return not self._trash or not self._trash[key]
end
EventListenerHolder.has_listeners_for_event = function (self, event)
	return self._listeners and self._listeners[event]
end

return 
