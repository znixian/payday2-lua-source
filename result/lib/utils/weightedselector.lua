WeightedSelector = WeightedSelector or class()
WeightedSelector.init = function (self)
	self.clear(self)

	return 
end
WeightedSelector.add = function (self, value, weight)
	table.insert(self._values, {
		value = value,
		weight = weight
	})

	self._total_weight = self._total_weight + weight

	return 
end
WeightedSelector.select = function (self)
	local rand = math.random()*self._total_weight

	for idx, item in ipairs(self._values) do
		if rand < item.weight then
			return item.value
		end

		rand = rand - item.weight
	end

	return nil
end
WeightedSelector.clear = function (self)
	self._values = {}
	self._total_weight = 0

	return 
end

return 
