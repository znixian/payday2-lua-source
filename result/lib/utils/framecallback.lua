FrameCallback = FrameCallback or class()
FrameCallback.init = function (self)
	self._callbacks = {}

	return 
end
FrameCallback.update = function (self)
	for i, obj in pairs(self._callbacks) do
		obj.current = obj.current + 1

		if obj.target%obj.current == 0 then
			obj.clbk()
		end
	end

	return 
end
FrameCallback.add = function (self, key, clbk, target)
	self._callbacks = self._callbacks or {}

	if not self._callbacks[key] then
		self._callbacks[key] = {
			current = 0,
			clbk = clbk,
			target = target
		}
	end

	return 
end
FrameCallback.remove = function (self, key)
	self._callbacks = self._callbacks or {}

	if self._callbacks[key] then
		self._callbacks[key].clbk = nil
		self._callbacks[key].target = nil
		self._callbacks[key].current = nil
		self._callbacks[key] = nil
	end

	return 
end
FrameCallback.reset_counter = function (self, key)
	self._callbacks = self._callbacks or {}

	if self._callbacks[key] then
		self._callbacks[key].current = 0
	end

	return 
end

return 
