require("lib/utils/Messages")

MessageSystem = MessageSystem or class()
MessageSystem.init = function (self)
	self._listeners = {}
	self._remove_list = {}
	self._add_list = {}
	self._messages = {}

	return 
end
MessageSystem.register = function (self, message, uid, func)
	table.insert(self._add_list, {
		message = message,
		uid = uid,
		func = func
	})

	return 
end
MessageSystem.unregister = function (self, message, uid)
	table.insert(self._remove_list, {
		message = message,
		uid = uid
	})

	return 
end
MessageSystem.notify = function (self, message, uid, ...)
	local arg = {
		...
	}

	table.insert(self._messages, {
		message = message,
		uid = uid,
		arg = arg
	})

	return 
end
MessageSystem.notify_now = function (self, message, uid, ...)
	local arg = {
		...
	}

	if self._listeners[message] then
		if uid and self._listeners[message][uid] then
			self._listeners[message][uid](unpack(arg))
		else
			for key, value in pairs(self._listeners[message]) do
				value(unpack(arg))
			end
		end
	end

	return 
end
MessageSystem._notify = function (self)
	local messages = table.list_copy(self._messages)
	local count = #self._messages

	for i = 1, count, 1 do
		self._messages[i] = nil
	end

	self._messages = nil
	self._messages = {}

	for i, m in ipairs(messages) do
		if self._listeners[m.message] then
			if m.uid then
				self._listeners[m.message][m.uid](unpack(m.arg))
			else
				for key, value in pairs(self._listeners[m.message]) do
					value(unpack(m.arg))
				end
			end
		end
	end

	return 
end
MessageSystem.flush = function (self)
	if 0 < #self._remove_list then
		self._remove(self)
	end

	if 0 < #self._add_list then
		self._add(self)
	end

	return 
end
MessageSystem.update = function (self)
	self.flush(self)
	self._notify(self)

	return 
end
MessageSystem._remove = function (self)
	local count = #self._remove_list

	for i = 1, count, 1 do
		local data = self._remove_list[i]

		self._unregister(self, self._remove_list[i].message, self._remove_list[i].uid)

		self._remove_list[i].message = nil
		self._remove_list[i].uid = nil
	end

	self._remove_list = nil
	self._remove_list = {}

	return 
end
MessageSystem._add = function (self)
	local count = #self._add_list

	for i = 1, count, 1 do
		local data = self._add_list[i]

		self._register(self, data.message, data.uid, data.func)

		self._add_list[i].message = nil
		self._add_list[i].uid = nil
		self._add_list[i].func = nil
	end

	self._add_list = nil
	self._add_list = {}

	return 
end
MessageSystem._register = function (self, message, uid, func)
	if not self._listeners[message] then
		self._listeners[message] = {}
	end

	if not self._listeners[message][uid] then
		self._listeners[message][uid] = func
	end

	return 
end
MessageSystem._unregister = function (self, message, uid)
	if self._listeners[message] then
		self._listeners[message][uid] = nil
	end

	return 
end

return 
