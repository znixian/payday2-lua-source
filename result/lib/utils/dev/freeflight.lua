core:module("FreeFlight")
core:import("CoreFreeFlight")
core:import("CoreClass")

FreeFlight = FreeFlight or class(CoreFreeFlight.FreeFlight)
FreeFlight.enable = function (self, ...)
	FreeFlight.super.enable(self, ...)

	if managers.hud then
		managers.hud:set_freeflight_disabled()
	end

	return 
end
FreeFlight.disable = function (self, ...)
	FreeFlight.super.disable(self, ...)

	if managers.hud then
		managers.hud:set_freeflight_enabled()
	end

	return 
end
FreeFlight._pause = function (self)
	Application:set_pause(true)

	return 
end
FreeFlight._unpause = function (self)
	Application:set_pause(false)

	return 
end

CoreClass.override_class(CoreFreeFlight.FreeFlight, FreeFlight)

return 
