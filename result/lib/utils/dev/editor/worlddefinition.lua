core:import("CoreWorldDefinition")

WorldDefinition = WorldDefinition or class(CoreWorldDefinition.WorldDefinition)
WorldDefinition.init = function (self, ...)
	WorldDefinition.super.init(self, ...)

	return 
end
WorldDefinition._project_assign_unit_data = function (self, unit, data)
	return 
end
WorldDefinition.get_cover_data = function (self)
	local path = self.world_dir(self) .. "cover_data"

	if not DB:has("cover_data", path) then
		return false
	end

	return self._serialize_to_script(self, "cover_data", path)
end

CoreClass.override_class(CoreWorldDefinition.WorldDefinition, WorldDefinition)

return 
