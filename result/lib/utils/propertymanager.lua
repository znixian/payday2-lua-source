PropertyManager = PropertyManager or class()
PropertyManager.init = function (self)
	self._properties = {}

	return 
end
PropertyManager.add_to_property = function (self, prop, value)
	if not self._properties[prop] then
		self._properties[prop] = value
	else
		self._properties[prop] = self._properties[prop] + value
	end

	return 
end
PropertyManager.mul_to_property = function (self, prop, value)
	if not self._properties[prop] then
		self._properties[prop] = value
	else
		self._properties[prop] = self._properties[prop]*value
	end

	return 
end
PropertyManager.set_property = function (self, prop, value)
	self._properties[prop] = value

	return 
end
PropertyManager.get_property = function (self, prop, default)
	if self._properties[prop] then
		return self._properties[prop]
	end

	return default
end
PropertyManager.has_property = function (self, prop)
	if self._properties[prop] then
		return true
	end

	return false
end
PropertyManager.remove_property = function (self, prop)
	if self._properties[prop] then
		self._properties[prop] = nil
	end

	return 
end

return 
