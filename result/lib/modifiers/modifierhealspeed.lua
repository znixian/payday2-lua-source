ModifierHealSpeed = ModifierHealSpeed or class(BaseModifier)
ModifierHealSpeed._type = "ModifierHealSpeed"
ModifierHealSpeed.name_id = "none"
ModifierHealSpeed.desc_id = "menu_cs_modifier_medic_speed"
ModifierHealSpeed.default_value = "speed"
ModifierHealSpeed.total_localization = "menu_cs_modifier_total_generic_percent"
ModifierHealSpeed.get_cooldown_multiplier = function (self)
	return self.value(self)/100 - 1
end
ModifierHealSpeed.modify_value = function (self, id, value)
	if id == "MedicDamage:CooldownTime" then
		return value*self.get_cooldown_multiplier(self)
	end

	return value
end

return 
