ModifierAssaultExtender = ModifierAssaultExtender or class(BaseModifier)
ModifierAssaultExtender._type = "ModifierAssaultExtender"
ModifierAssaultExtender.name_id = "none"
ModifierAssaultExtender.desc_id = "menu_cs_modifier_assault_extender"
ModifierAssaultExtender.default_value = "duration"
ModifierAssaultExtender.init = function (self, data)
	ModifierAssaultExtender.super.init(self, data)

	self._sustain_start_time = 0
	self._base_duration = 0
	self._hostage_time = 0
	self._hostage_count = 0
	self._hostage_average_count = 0
	self._hostage_last_update = 0

	return 
end
ModifierAssaultExtender._update_hostage_time = function (self)
	local now = TimerManager:game():time()
	local diff = now - self._hostage_last_update
	self._hostage_time = self._hostage_time + diff*self._hostage_count
	self._hostage_average_count = self._hostage_time/(now - self._sustain_start_time)
	self._hostage_last_update = now

	return 
end
ModifierAssaultExtender._update_hostage_count = function (self)
	local num_hostages = managers.groupai:state():hostage_count()
	local num_minions = managers.groupai:state():get_amount_enemies_converted_to_criminals()
	self._hostage_count = math.min(num_hostages + num_minions, self.value(self, "max_hostages"))

	return 
end
ModifierAssaultExtender.OnHostageCountChanged = function (self)
	self._update_hostage_time(self)
	self._update_hostage_count(self)

	return 
end
ModifierAssaultExtender.OnMinionAdded = function (self)
	self._update_hostage_time(self)
	self._update_hostage_count(self)

	return 
end
ModifierAssaultExtender.OnMinionRemoved = function (self)
	self._update_hostage_time(self)
	self._update_hostage_count(self)

	return 
end
ModifierAssaultExtender.OnEnterSustainPhase = function (self, duration)
	local now = TimerManager:game():time()
	self._sustain_start_time = now
	self._base_duration = duration
	self._hostage_time = 0
	self._hostage_last_update = now

	return 
end
ModifierAssaultExtender.modify_value = function (self, id, value, ...)
	if id == "GroupAIStateBesiege:SustainEndTime" then
		self._update_hostage_time(self)

		local now = TimerManager:game():time()
		local extension = self.value(self, "duration")*0.01
		local deduction = self.value(self, "deduction")*0.01*self._hostage_average_count
		local value = value + self._base_duration*(extension - deduction)

		return value
	elseif id == "GroupAIStateBesiege:SustainSpawnAllowance" then
		self._update_hostage_time(self)

		local now = TimerManager:game():time()
		local base_pool = ...
		local extension = self.value(self, "spawn_pool")*0.01
		local deduction = self.value(self, "deduction")*0.01*self._hostage_average_count
		local value = value + math.floor(base_pool*(extension - deduction))

		return value
	end

	return value
end

return 
