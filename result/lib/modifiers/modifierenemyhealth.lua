ModifierEnemyHealth = ModifierEnemyHealth or class(BaseModifier)
ModifierEnemyHealth._type = "ModifierEnemyHealth"
ModifierEnemyHealth.name_id = "none"
ModifierEnemyHealth.desc_id = "menu_cs_modifier_enemy_health"
ModifierEnemyHealth.default_value = "health"
ModifierEnemyHealth.total_localization = "menu_cs_modifier_total_generic_percent"
ModifierEnemyHealth.init = function (self, data)
	ModifierEnemyHealth.super.init(self, data)
	MutatorEnemyHealth:modify_character_tweak_data(tweak_data.character, self.get_health_multiplier(self))

	return 
end
ModifierEnemyHealth.get_health_multiplier = function (self)
	return self.value(self)/100 + 1
end

return 
