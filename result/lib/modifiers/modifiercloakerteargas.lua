ModifierCloakerTearGas = ModifierCloakerTearGas or class(BaseModifier)
ModifierCloakerTearGas._type = "ModifierCloakerTearGas"
ModifierCloakerTearGas.name_id = "none"
ModifierCloakerTearGas.desc_id = "menu_cs_modifier_cloaker_tear_gas"
ModifierCloakerTearGas.OnEnemyDied = function (self, unit, damage_info)
	if Network:is_client() then
		return 
	end

	if unit.base(unit)._tweak_table == "spooc" then
		local grenade = World:spawn_unit(Idstring("units/pd2_dlc_drm/weapons/smoke_grenade_tear_gas/smoke_grenade_tear_gas"), unit.position(unit), unit.rotation(unit))

		grenade.base(grenade):set_properties({
			radius = self.value(self, "diameter")*0.5*100,
			damage = self.value(self, "damage")*0.1,
			duration = self.value(self, "duration")
		})
		grenade.base(grenade):detonate()
	end

	return 
end

return 
