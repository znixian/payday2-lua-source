GageModifierQuickSwitch = GageModifierQuickSwitch or class(GageModifier)
GageModifierQuickSwitch._type = "GageModifierQuickSwitch"
GageModifierQuickSwitch.default_value = "speed"
GageModifierQuickSwitch.get_speed_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierQuickSwitch.modify_value = function (self, id, value)
	if id == "PlayerStandard:GetSwapSpeedMultiplier" then
		return value*self.get_speed_multiplier(self)
	end

	return value
end

return 
