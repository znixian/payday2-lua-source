GageModifierMaxHealth = GageModifierMaxHealth or class(GageModifier)
GageModifierMaxHealth._type = "GageModifierMaxHealth"
GageModifierMaxHealth.default_value = "health"
GageModifierMaxHealth.get_health_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierMaxHealth.modify_value = function (self, id, value)
	if id == "PlayerDamage:GetMaxHealth" then
		return value*self.get_health_multiplier(self)
	end

	return value
end

return 
