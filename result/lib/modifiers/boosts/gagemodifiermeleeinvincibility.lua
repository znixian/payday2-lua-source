GageModifierMeleeInvincibility = GageModifierMeleeInvincibility or class(GageModifier)
GageModifierMeleeInvincibility._type = "GageModifierMeleeInvincibility"
GageModifierMeleeInvincibility.default_value = "time"
GageModifierMeleeInvincibility.OnPlayerManagerKillshot = function (self, player_unit, unit_tweak, variant)
	if variant == "melee" and table.contains(StatisticsManager.special_unit_ids, unit_tweak) then
		self._special_kill_t = TimerManager:game():time()
	end

	return 
end
GageModifierMeleeInvincibility.modify_value = function (self, id, value)
	if self._special_kill_t and id == "PlayerDamage:CheckCanTakeDamage" and TimerManager:game():time() <= self._special_kill_t + self.value(self) then
		return false
	end

	return value
end

return 
