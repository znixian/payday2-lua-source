GageModifierMaxAmmo = GageModifierMaxAmmo or class(GageModifier)
GageModifierMaxAmmo._type = "GageModifierMaxAmmo"
GageModifierMaxAmmo.default_value = "ammo"
GageModifierMaxAmmo.get_ammo_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierMaxAmmo.modify_value = function (self, id, value)
	if id == "WeaponBase:GetMaxAmmoMultiplier" then
		return value*self.get_ammo_multiplier(self)
	end

	return value
end

return 
