GageModifierDamageAbsorption = GageModifierDamageAbsorption or class(GageModifier)
GageModifierDamageAbsorption._type = "GageModifierDamageAbsorption"
GageModifierDamageAbsorption.default_value = "absorption"
GageModifierDamageAbsorption.modify_value = function (self, id, value)
	if id == "PlayerManager:GetDamageAbsorption" then
		return value + self.value(self)
	end

	return value
end

return 
