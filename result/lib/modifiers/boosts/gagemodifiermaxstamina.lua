GageModifierMaxStamina = GageModifierMaxStamina or class(GageModifier)
GageModifierMaxStamina._type = "GageModifierMaxStamina"
GageModifierMaxStamina.default_value = "stamina"
GageModifierMaxStamina.get_stamina_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierMaxStamina.modify_value = function (self, id, value)
	if id == "PlayerManager:GetStaminaMultiplier" then
		return value*self.get_stamina_multiplier(self)
	end

	return value
end

return 
