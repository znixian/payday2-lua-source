GageModifierQuickReload = GageModifierQuickReload or class(GageModifier)
GageModifierQuickReload._type = "GageModifierQuickReload"
GageModifierQuickReload.default_value = "speed"
GageModifierQuickReload.get_speed_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierQuickReload.modify_value = function (self, id, value)
	if id == "WeaponBase:GetReloadSpeedMultiplier" then
		return value*self.get_speed_multiplier(self)
	end

	return value
end

return 
