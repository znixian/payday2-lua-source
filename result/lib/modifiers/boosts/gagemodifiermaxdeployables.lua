GageModifierMaxDeployables = GageModifierMaxDeployables or class(GageModifier)
GageModifierMaxDeployables._type = "GageModifierMaxDeployables"
GageModifierMaxDeployables.default_value = "deployables"
GageModifierMaxDeployables.get_amount_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierMaxDeployables.modify_value = function (self, id, value)
	if id == "PlayerManager:GetEquipmentMaxAmount" and 0 < value then
		local new_val = math.floor(value*self.get_amount_multiplier(self))
		new_val = math.max(new_val, value + 1)

		return new_val
	end

	return value
end

return 
