GageModifierQuickLocks = GageModifierQuickLocks or class(GageModifier)
GageModifierQuickLocks._type = "GageModifierQuickLocks"
GageModifierQuickLocks.default_value = "speed"
GageModifierQuickLocks.get_speed_divisor = function (self)
	return self.value(self)/100 + 1
end
GageModifierQuickLocks.modify_value = function (self, id, value, interact_object)
	if id == "PlayerStandard:OnStartInteraction" then
		local tweak_id = interact_object.interaction(interact_object).tweak_data
		local tweak = tweak_id and tweak_data.interaction[tweak_id]

		if tweak and tweak.is_lockpicking then
			return value/self.get_speed_divisor(self)
		end
	end

	return value
end

return 
