GageModifierPassivePanic = GageModifierPassivePanic or class(GageModifier)
GageModifierPassivePanic._type = "GageModifierPassivePanic"
GageModifierPassivePanic.default_value = "panic"
GageModifierPassivePanic.get_chance_increase = function (self)
	return self.value(self)/100
end
GageModifierPassivePanic.modify_value = function (self, id, value)
	if id == "PlayerManager:GetKillshotPanicChance" and value ~= -1 then
		return value + self.get_chance_increase(self)
	end

	return value
end

return 
