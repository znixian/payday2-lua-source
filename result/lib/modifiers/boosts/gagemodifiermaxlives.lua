GageModifierMaxLives = GageModifierMaxLives or class(GageModifier)
GageModifierMaxLives._type = "GageModifierMaxLives"
GageModifierMaxLives.default_value = "lives"
GageModifierMaxLives.modify_value = function (self, id, value)
	if id == "PlayerDamage:GetMaximumLives" then
		return value + self.value(self)
	end

	return value
end

return 
