GageModifierExplosionImmunity = GageModifierExplosionImmunity or class(GageModifier)
GageModifierExplosionImmunity._type = "GageModifierExplosionImmunity"
GageModifierExplosionImmunity.modify_value = function (self, id, value)
	if id == "PlayerDamage:OnTakeExplosionDamage" then
		return 0
	end

	return value
end

return 
