GageModifierQuickPagers = GageModifierQuickPagers or class(GageModifier)
GageModifierQuickPagers._type = "GageModifierQuickPagers"
GageModifierQuickPagers.default_value = "speed"
GageModifierQuickPagers.get_speed_divisor = function (self)
	return self.value(self)/100 + 1
end
GageModifierQuickPagers.modify_value = function (self, id, value, interact_object)
	if id == "PlayerStandard:OnStartInteraction" then
		local tweak = interact_object.interaction(interact_object).tweak_data

		if tweak == "corpse_alarm_pager" then
			return value/self.get_speed_divisor(self)
		end
	end

	return value
end

return 
