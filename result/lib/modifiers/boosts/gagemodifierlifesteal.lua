GageModifierLifeSteal = GageModifierLifeSteal or class(GageModifier)
GageModifierLifeSteal._type = "GageModifierLifeSteal"
GageModifierLifeSteal.default_value = "cooldown"
GageModifierLifeSteal.OnPlayerManagerKillshot = function (self, player_unit, unit_tweak, variant)
	local can_steal = not self._last_killshot_t or self._last_killshot_t + self.value(self, "cooldown") < TimerManager:game():time()

	if can_steal then
		local armor = self.value(self, "armor_restored")

		if armor and 0 < armor then
			player_unit.character_damage(player_unit):restore_armor(armor)
		end

		local health = self.value(self, "health_restored")

		if health and 0 < health then
			player_unit.character_damage(player_unit):restore_health(health)
		end

		self._last_killshot_t = TimerManager:game():time()
	end

	return 
end

return 
