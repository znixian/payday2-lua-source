GageModifierMaxBodyBags = GageModifierMaxBodyBags or class(GageModifier)
GageModifierMaxBodyBags._type = "GageModifierMaxBodyBags"
GageModifierMaxBodyBags.default_value = "bags"
GageModifierMaxBodyBags.modify_value = function (self, id, value)
	if id == "PlayerManager:GetTotalBodyBags" then
		return value + self.value(self)
	end

	return value
end

return 
