GageModifierMaxThrowables = GageModifierMaxThrowables or class(GageModifier)
GageModifierMaxThrowables._type = "GageModifierMaxThrowables"
GageModifierMaxThrowables.default_value = "throwables"
GageModifierMaxThrowables.get_amount_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierMaxThrowables.modify_value = function (self, id, value)
	if id == "PlayerManager:GetThrowablesMaxAmount" then
		local new_val = math.floor(value*self.get_amount_multiplier(self))
		new_val = math.max(new_val, value + 1)

		return new_val
	end

	return value
end

return 
