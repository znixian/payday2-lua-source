GageModifierMaxArmor = GageModifierMaxArmor or class(GageModifier)
GageModifierMaxArmor._type = "GageModifierMaxArmor"
GageModifierMaxArmor.default_value = "armor"
GageModifierMaxArmor.init = function (self, data)
	GageModifierMaxArmor.super.init(self, data)

	if managers.player and alive(managers.player:local_player()) then
		managers.player:local_player():character_damage():_regenerate_armor(true)
	end

	return 
end
GageModifierMaxArmor.get_armor_multiplier = function (self)
	return self.value(self)/100 + 1
end
GageModifierMaxArmor.modify_value = function (self, id, value)
	if id == "PlayerDamage:GetMaxArmor" then
		return value*self.get_armor_multiplier(self)
	end

	return value
end

return 
