ModifierMedicAdrenaline = ModifierMedicAdrenaline or class(BaseModifier)
ModifierMedicAdrenaline._type = "ModifierMedicAdrenaline"
ModifierMedicAdrenaline.name_id = "none"
ModifierMedicAdrenaline.desc_id = "menu_cs_modifier_medic_adrenaline"
ModifierMedicAdrenaline.OnEnemyHealed = function (self, medic, target)
	target.base(target):add_buff("base_damage", self.value(self, "damage")*0.01)

	return 
end

return 
