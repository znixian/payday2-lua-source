ModifierDozerRage = ModifierDozerRage or class(BaseModifier)
ModifierDozerRage._type = "ModifierDozerRage"
ModifierDozerRage.name_id = "none"
ModifierDozerRage.desc_id = "menu_cs_modifier_dozer_rage"
ModifierDozerRage.OnTankVisorShatter = function (self, unit, damage_info)
	unit.base(unit):add_buff("base_damage", self.value(self, "damage")*0.01)

	return 
end

return 
