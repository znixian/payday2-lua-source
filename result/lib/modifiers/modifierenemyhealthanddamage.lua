ModifierEnemyHealthAndDamage = ModifierEnemyHealthAndDamage or class(BaseModifier)
ModifierEnemyHealthAndDamage._type = "ModifierEnemyHealthAndDamage"
ModifierEnemyHealthAndDamage.name_id = "none"
ModifierEnemyHealthAndDamage.desc_id = "menu_cs_modifier_enemy_health_damage"
ModifierEnemyHealthAndDamage.get_description = function (self, modifier_id, idx)
	local desc = ModifierEnemyHealthAndDamage.super.get_description(self, modifier_id, idx)

	if idx == 1 then
		local data = managers.crime_spree:get_modifier_stack_data(self._type)
		local params = {
			health = managers.experience:cash_string(data.health or 0, ""),
			damage = managers.experience:cash_string(data.damage or 0, "")
		}
		desc = desc .. " " .. managers.localization:text("menu_cs_modifier_health_damage_total", params)
	end

	return desc
end
ModifierEnemyHealthAndDamage.init = function (self, data)
	ModifierEnemyHealthAndDamage.super.init(self, data)
	MutatorEnemyHealth:modify_character_tweak_data(tweak_data.character, self.get_health_multiplier(self))

	return 
end
ModifierEnemyHealthAndDamage.get_health_multiplier = function (self)
	return self.value(self, "health")/100 + 1
end
ModifierEnemyHealthAndDamage.get_damage_multiplier = function (self)
	return self.value(self, "damage")/100 + 1
end
ModifierEnemyHealthAndDamage.modify_value = function (self, id, value)
	if id == "PlayerDamage:TakeDamageBullet" then
		return value*self.get_damage_multiplier(self)
	end

	return value
end

return 
