ModifierLessConcealment = ModifierLessConcealment or class(BaseModifier)
ModifierLessConcealment._type = "ModifierLessConcealment"
ModifierLessConcealment.name_id = "none"
ModifierLessConcealment.desc_id = "menu_cs_modifier_concealment"
ModifierLessConcealment.default_value = "conceal"
ModifierLessConcealment.total_localization = "menu_cs_modifier_total_generic_value"
ModifierLessConcealment.stealth = true
ModifierLessConcealment.modify_value = function (self, id, value)
	if id == "BlackMarketManager:GetConcealment" then
		return value + self.value(self)
	end

	return value
end

return 
