ModifierTaserOvercharge = ModifierTaserOvercharge or class(BaseModifier)
ModifierTaserOvercharge._type = "ModifierTaserOvercharge"
ModifierTaserOvercharge.name_id = "none"
ModifierTaserOvercharge.desc_id = "menu_cs_modifier_taser_overcharge"
ModifierTaserOvercharge.default_value = "speed"
ModifierTaserOvercharge.modify_value = function (self, id, value)
	if id == "PlayerTased:TasedTime" then
		value = value/(self.value(self)*0.01 + 1)
	end

	return value
end

return 
