ModifierEnemyDamage = ModifierEnemyDamage or class(BaseModifier)
ModifierEnemyDamage._type = "ModifierEnemyDamage"
ModifierEnemyDamage.name_id = "none"
ModifierEnemyDamage.desc_id = "menu_cs_modifier_enemy_damage"
ModifierEnemyDamage.default_value = "damage"
ModifierEnemyDamage.total_localization = "menu_cs_modifier_total_generic_percent"
ModifierEnemyDamage.get_damage_multiplier = function (self)
	return self.value(self)/100 + 1
end
ModifierEnemyDamage.modify_value = function (self, id, value)
	if id == "PlayerDamage:TakeDamageBullet" then
		return value*self.get_damage_multiplier(self)
	end

	return value
end

return 
