require("lib/states/GameState")

IngameDriving = IngameDriving or class(IngamePlayerBaseState)
IngameDriving.DRIVING_GUI_SAFERECT = Idstring("guis/driving_saferect")
IngameDriving.DRIVING_GUI_FULLSCREEN = Idstring("guis/driving_fullrect")
IngameDriving.init = function (self, game_state_machine)
	IngameDriving.super.init(self, "ingame_driving", game_state_machine)

	return 
end
IngameDriving._update_driving_hud = function (self)
	local vehicle = managers.player:get_vehicle().vehicle_unit:vehicle()
	local vehicle_state = vehicle.get_state(vehicle)
	local speed = vehicle_state.get_speed(vehicle_state)*3.6
	local rpm = vehicle_state.get_rpm(vehicle_state)
	local gear = vehicle_state.get_gear(vehicle_state) - 1

	if gear == 0 then
		gear = "N"
	elseif gear < 0 then
		gear = "R"
	end

	managers.hud:set_driving_vehicle_state(speed, rpm, gear)

	return 
end
IngameDriving.update = function (self, t, dt)
	return 
end
IngameDriving.update_player_stamina = function (self, t, dt)
	return 
end
IngameDriving._player_damage = function (self, info)
	print("IngameDriving:_player_damage()")

	return 
end
IngameDriving.at_enter = function (self, old_state, ...)
	print("IngameDriving:at_enter()")

	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
	end

	SoundDevice:set_rtpc("stamina", 100)

	self._old_state = old_state.name(old_state)
	local vehicle_ext = managers.player:get_vehicle().vehicle_unit:vehicle_driving()
	local seat = vehicle_ext.find_seat_for_player(vehicle_ext, player)

	if false then
		if not managers.hud:exists(IngameDriving.DRIVING_GUI_SAFERECT) then
			print("Loading HUD...")
			managers.hud:load_hud(IngameDriving.DRIVING_GUI_FULLSCREEN, false, true, false, {})
			managers.hud:load_hud(IngameDriving.DRIVING_GUI_SAFERECT, false, true, true, {})
		end

		managers.hud:show(IngameDriving.DRIVING_GUI_SAFERECT)
		managers.hud:show(IngameDriving.DRIVING_GUI_FULLSCREEN)
		managers.hud:start_driving()
	else
		managers.hud:show(PlayerBase.PLAYER_HUD)
		managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
		managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	end

	if vehicle_ext.has_driving_seat(vehicle_ext) and tweak_data.achievement.drive_away then
		managers.achievment:award(tweak_data.achievement.drive_away)
	end

	return 
end
IngameDriving.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	managers.hud:hide(PlayerBase.PLAYER_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	return 
end
IngameDriving.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameDriving.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameDriving.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
