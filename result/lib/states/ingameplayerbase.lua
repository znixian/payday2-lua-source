require("lib/states/GameState")

IngamePlayerBaseState = IngamePlayerBaseState or class(GameState)
IngamePlayerBaseState.init = function (self, ...)
	GameState.init(self, ...)

	return 
end
IngamePlayerBaseState.set_controller_enabled = function (self, enabled)
	local players = managers.player:players()

	for _, player in ipairs(players) do
		local controller = player.base(player):controller()

		if controller then
			controller.set_enabled(controller, enabled)
		end

		if enabled then
			controller.clear_input_pressed_state(controller, "duck")
			controller.clear_input_pressed_state(controller, "jump")

			if controller.get_input_bool(controller, "stats_screen") then
				player.base(player):set_stats_screen_visible(true)
			end
		else
			player.base(player):set_stats_screen_visible(false)
		end
	end

	return 
end

return 
