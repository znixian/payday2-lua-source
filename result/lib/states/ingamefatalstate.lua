require("lib/states/GameState")

IngameFatalState = IngameFatalState or class(IngamePlayerBaseState)
IngameFatalState.init = function (self, game_state_machine)
	IngameFatalState.super.init(self, "ingame_fatal", game_state_machine)

	return 
end
IngameFatalState.on_local_player_dead = function ()
	local peer_id = managers.network:session():local_peer():id()
	local player = managers.player:player_unit()

	player.network(player):send("sync_player_movement_state", "dead", player.character_damage(player):down_time(), player.id(player))
	managers.groupai:state():on_player_criminal_death(peer_id)

	return 
end
IngameFatalState.update = function (self, t, dt)
	local player_manager = managers.player
	local player = player_manager.player_unit(player_manager)

	if not alive(player) then
		return 
	end

	if player.character_damage(player):update_downed(t, dt) then
		player_manager.on_enter_custody(player_manager, player)
	end

	return 
end
IngameFatalState.at_enter = function (self)
	local player_manager = managers.player
	local players = player_manager.players(player_manager)

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	managers.statistics:downed({
		fatal = true
	})

	local player = player_manager.player_unit(player_manager)

	if player then
		player.base(player):set_enabled(true)
	end

	local hud_manager = managers.hud

	hud_manager.show(hud_manager, PlayerBase.PLAYER_INFO_HUD)
	hud_manager.show(hud_manager, PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	hud_manager.show(hud_manager, PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameFatalState.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	local hud_manager = managers.hud

	hud_manager.hide(hud_manager, PlayerBase.PLAYER_INFO_HUD)
	hud_manager.hide(hud_manager, PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	hud_manager.hide(hud_manager, PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameFatalState.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameFatalState.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameFatalState.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
