require("lib/states/GameState")

IngameArrestedState = IngameArrestedState or class(IngamePlayerBaseState)
IngameArrestedState.init = function (self, game_state_machine)
	IngameArrestedState.super.init(self, "ingame_arrested", game_state_machine)

	return 
end
IngameArrestedState.update = function (self, t, dt)
	local player = managers.player:player_unit()

	if not alive(player) then
		return 
	end

	player.character_damage(player):update_arrested(t, dt)

	return 
end
IngameArrestedState.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	managers.statistics:downed({
		bleed_out = true
	})

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
	end

	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	managers.hud:show(PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameArrestedState.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	managers.hud:hide(PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameArrestedState.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameArrestedState.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameArrestedState.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
