core:import("CoreInternalGameState")

GameState = GameState or class(CoreInternalGameState.GameState)
GameState.freeflight_drop_player = function (self, pos, rot, velocity)
	if managers.player then
		managers.player:warp_to(pos, rot, nil, velocity)
	end

	return 
end
GameState.set_controller_enabled = function (self, enabled)
	return 
end
GameState.default_transition = function (self, next_state, params)
	self.at_exit(self, next_state, params)
	self.set_controller_enabled(self, false)

	if self.gsm(self):is_controller_enabled() then
		next_state.set_controller_enabled(next_state, true)
	end

	next_state.at_enter(next_state, self, params)

	return 
end
GameState.on_disconnected = function (self)
	game_state_machine:change_state_by_name("disconnected")

	return 
end
GameState.on_server_left = function (self)
	game_state_machine:change_state_by_name("server_left")

	return 
end

CoreClass.override_class(CoreInternalGameState.GameState, GameState)

return 
