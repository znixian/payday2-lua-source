require("lib/states/GameState")

IngameCleanState = IngameCleanState or class(IngamePlayerBaseState)
IngameCleanState.init = function (self, game_state_machine)
	IngameCleanState.super.init(self, "ingame_clean", game_state_machine)

	return 
end
IngameCleanState.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
		player.character_damage(player):set_invulnerable(true)
	end

	return 
end
IngameCleanState.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
		player.character_damage(player):set_invulnerable(false)
	end

	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	return 
end
IngameCleanState.on_server_left = function (self)
	print("[IngameCleanState:on server_left]")
	game_state_machine:change_state_by_name("server_left")

	return 
end
IngameCleanState.on_kicked = function (self)
	print("[IngameCleanState:on on_kicked]")
	game_state_machine:change_state_by_name("kicked")

	return 
end
IngameCleanState.on_disconnected = function (self)
	game_state_machine:change_state_by_name("disconnected")

	return 
end

return 
