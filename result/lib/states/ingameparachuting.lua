require("lib/states/GameState")

IngameParachuting = IngameParachuting or class(IngamePlayerBaseState)
IngameParachuting.init = function (self, game_state_machine)
	IngameParachuting.super.init(self, "ingame_parachuting", game_state_machine)

	return 
end
IngameParachuting.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
	end

	managers.hud:show(PlayerBase.PLAYER_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	return 
end
IngameParachuting.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	local unit = safe_spawn_unit(Idstring("units/pd2_dlc_jerry/props/jry_equipment_parachute/jry_equipment_parachute_ragdoll"), player.position(player), player.rotation(player))

	unit.damage(unit):run_sequence_simple("make_dynamic")

	return 
end
IngameParachuting.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameParachuting.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameParachuting.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
