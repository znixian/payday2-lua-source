require("lib/states/GameState")

DisconnectedState = DisconnectedState or class(MissionEndState)
DisconnectedState.init = function (self, game_state_machine, setup)
	DisconnectedState.super.init(self, "disconnected", game_state_machine, setup)

	return 
end
DisconnectedState.at_enter = function (self, ...)
	self._success = false
	self._completion_bonus_done = true

	DisconnectedState.super.at_enter(self, ...)
	managers.network.voice_chat:destroy_voice(true)

	if managers.network.matchmake then
		managers.network.matchmake._room_id = nil
	end

	self._create_disconnected_dialog(self)

	return 
end
DisconnectedState._create_disconnected_dialog = function (self)
	MenuMainState._create_disconnected_dialog(self)

	return 
end
DisconnectedState.on_server_left_ok_pressed = function (self)
	return 
end
DisconnectedState.on_disconnected = function (self)
	self._completion_bonus_done = true

	self._set_continue_button_text(self)

	return 
end
DisconnectedState.on_server_left = function (self)
	self._completion_bonus_done = true

	self._set_continue_button_text(self)

	return 
end
DisconnectedState._load_start_menu = function (self)
	if not managers.job:stage_success() or not managers.job:on_last_stage() then
		setup:load_start_menu()
	end

	return 
end

return 
