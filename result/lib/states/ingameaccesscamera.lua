require("lib/states/GameState")

IngameAccessCamera = IngameAccessCamera or class(IngamePlayerBaseState)
IngameAccessCamera.GUI_SAFERECT = Idstring("guis/access_camera_saferect")
IngameAccessCamera.GUI_FULLSCREEN = Idstring("guis/access_camera_fullrect")
local old_buttons = not _G.IS_VR
local tmp_vec1 = Vector3()
local tmp_rot1 = Rotation()
IngameAccessCamera.init = function (self, game_state_machine)
	IngameAccessCamera.super.init(self, "ingame_access_camera", game_state_machine)

	return 
end
IngameAccessCamera._setup_controller = function (self)
	self._controller = managers.controller:create_controller("ingame_access_camera", managers.controller:get_default_wrapper_index(), false)
	self._leave_cb = callback(self, self, "cb_leave")

	if _G.IS_VR then
		self._leave_cb = callback(self, self, "cb_leave_vr")

		managers.menu:player():attach_controller(self._controller)
	end

	self._prev_camera_cb = callback(self, self, "_prev_camera")
	self._next_camera_cb = callback(self, self, "_next_camera")

	self._controller:add_trigger((old_buttons and "jump") or "suvcam_exit", self._leave_cb)
	self._controller:add_trigger((old_buttons and "primary_attack") or "suvcam_prev", self._prev_camera_cb)
	self._controller:add_trigger((old_buttons and "secondary_attack") or "suvcam_next", self._next_camera_cb)
	self._controller:set_enabled(true)
	managers.controller:set_ingame_mode("access_camera")

	return 
end
IngameAccessCamera._clear_controller = function (self)
	if self._controller then
		if _G.IS_VR then
			managers.menu:player():dettach_controller(self._controller)
		end

		self._controller:remove_trigger((old_buttons and "jump") or "suvcam_exit", self._leave_cb)
		self._controller:remove_trigger((old_buttons and "primary_attack") or "suvcam_prev", self._prev_camera_cb)
		self._controller:remove_trigger((old_buttons and "secondary_attack") or "suvcam_next", self._next_camera_cb)
		self._controller:set_enabled(false)
		self._controller:destroy()

		self._controller = nil

		managers.controller:set_ingame_mode("main")
	end

	return 
end
IngameAccessCamera.set_controller_enabled = function (self, enabled)
	if self._controller then
		self._controller:set_enabled(enabled)
	end

	return 
end
IngameAccessCamera.cb_leave = function (self)
	game_state_machine:change_state_by_name(self._old_state)

	return 
end
IngameAccessCamera.cb_leave_vr = function (self)
	local active_menu = managers.menu:active_menu()

	if active_menu and active_menu.name ~= "ingame_access_camera_menu" then
		return 
	end

	game_state_machine:change_state_by_name(self._old_state)

	return 
end
IngameAccessCamera._get_cameras = function (self)
	self._cameras = {}

	for _, script in pairs(managers.mission:scripts()) do
		local access_cameras = script.element_group(script, "ElementAccessCamera")

		if access_cameras then
			for _, access_camera in ipairs(access_cameras) do
				table.insert(self._cameras, {
					access_camera = access_camera
				})
			end
		end
	end

	return 
end
IngameAccessCamera._next_index = function (self)
	self._camera_data.index = self._camera_data.index + 1

	if #self._cameras < self._camera_data.index then
		self._camera_data.index = 1
	end

	if not self._cameras[self._camera_data.index].access_camera:enabled() then
		self._next_index(self)
	end

	return 
end
IngameAccessCamera._prev_index = function (self)
	self._camera_data.index = self._camera_data.index - 1

	if self._camera_data.index < 1 then
		self._camera_data.index = #self._cameras
	end

	if not self._cameras[self._camera_data.index].access_camera:enabled() then
		self._prev_index(self)
	end

	return 
end
IngameAccessCamera._prev_camera = function (self)
	if self._no_feeds then
		return 
	end

	self._prev_index(self)
	self._show_camera(self)

	return 
end
IngameAccessCamera._next_camera = function (self)
	if self._no_feeds then
		return 
	end

	self._next_index(self)
	self._show_camera(self)

	return 
end
IngameAccessCamera.on_destroyed = function (self)
	local access_camera = self._cameras[self._camera_data.index].access_camera

	managers.hud:set_access_camera_destroyed(access_camera.value(access_camera, "destroyed"))

	return 
end
IngameAccessCamera._show_camera = function (self)
	self._sound_source:post_event("camera_monitor_change")

	if self._last_access_camera then
		self._last_access_camera:remove_trigger("IngameAccessCamera", "destroyed")
	end

	local access_camera = self._cameras[self._camera_data.index].access_camera

	access_camera.add_trigger(access_camera, "IngameAccessCamera", "destroyed", callback(self, self, "on_destroyed"))

	self._last_access_camera = access_camera

	access_camera.trigger_accessed(access_camera, managers.player:player_unit())
	self._cam_unit:set_position(access_camera.camera_position(access_camera))
	self._cam_unit:camera():set_rotation(access_camera.value(access_camera, "rotation"))
	self._cam_unit:camera():start(math.rand(30))

	self._yaw = 0
	self._pitch = 0
	self._target_yaw = 0
	self._target_pitch = 0
	self._yaw_limit = access_camera.value(access_camera, "yaw_limit") or 25
	self._pitch_limit = access_camera.value(access_camera, "pitch_limit") or 25

	managers.hud:set_access_camera_destroyed(access_camera.value(access_camera, "destroyed"))

	local text_id = (access_camera.value(access_camera, "text_id") ~= "debug_none" and access_camera.value(access_camera, "text_id")) or "hud_cam_access_camera_test_generated"
	local number = ((self._camera_data.index < 10 and "0") or "") .. self._camera_data.index

	managers.hud:set_access_camera_name(managers.localization:text(text_id, {
		NUMBER = number
	}))

	return 
end
IngameAccessCamera.update = function (self, t, dt)
	if _G.IS_VR then
		local active_menu = managers.menu:active_menu()

		if active_menu and active_menu.name == "ingame_access_camera_menu" then
			self._controller:set_active(true)
		else
			self._controller:set_active(false)
		end
	end

	if self._no_feeds then
		return 
	end

	t = managers.player:player_timer():time()
	dt = managers.player:player_timer():delta_time()
	local roll = 0
	local access_camera = self._cameras[self._camera_data.index].access_camera

	if access_camera and access_camera.is_moving and access_camera.is_moving(access_camera) then
		local m_rot = self._cam_unit:camera():get_original_rotation()

		if m_rot then
			access_camera.m_camera_rotation(access_camera, m_rot)
		end

		access_camera.m_camera_position(access_camera, tmp_vec1)
		self._cam_unit:set_position(tmp_vec1)

		roll = mrotation.roll(m_rot)
	end

	local look_d = self._controller:get_input_axis("look")
	local zoomed_value = self._cam_unit:camera():zoomed_value()
	self._target_yaw = self._target_yaw - look_d.x*zoomed_value

	if self._yaw_limit ~= -1 then
		self._target_yaw = math.clamp(self._target_yaw, -self._yaw_limit, self._yaw_limit)
	end

	self._target_pitch = self._target_pitch + look_d.y*zoomed_value

	if self._pitch_limit ~= -1 then
		self._target_pitch = math.clamp(self._target_pitch + look_d.y*zoomed_value, -self._pitch_limit, self._pitch_limit)
	end

	self._yaw = math.step(self._yaw, self._target_yaw, dt*10)
	self._pitch = math.step(self._pitch, self._target_pitch, dt*10)

	self._cam_unit:camera():set_offset_rotation(self._yaw, self._pitch, roll)

	local move_d = self._controller:get_input_axis("move")

	self._cam_unit:camera():modify_fov(-move_d.y*dt*12)

	if self._do_show_camera then
		self._do_show_camera = false

		managers.hud:set_access_camera_destroyed(access_camera.value(access_camera, "destroyed"))
	end

	local units = World:find_units_quick("all", 3, 16, 21, managers.slot:get_mask("enemies"))
	local amount = 0

	for i, unit in ipairs(units) do
		if World:in_view_with_options(unit.movement(unit):m_head_pos(), 0, 0, 4000) then
			local ray = nil

			if self._last_access_camera and self._last_access_camera:has_camera_unit() then
				ray = self._cam_unit:raycast("ray", unit.movement(unit):m_head_pos(), self._cam_unit:position(), "ray_type", "ai_vision", "slot_mask", managers.slot:get_mask("world_geometry"), "ignore_unit", self._last_access_camera:camera_unit(), "report")
			else
				ray = self._cam_unit:raycast("ray", unit.movement(unit):m_head_pos(), self._cam_unit:position(), "ray_type", "ai_vision", "slot_mask", managers.slot:get_mask("world_geometry"), "report")
			end

			if not ray then
				amount = amount + 1

				managers.hud:access_camera_track(amount, self._cam_unit:camera()._camera, unit.movement(unit):m_head_pos())

				if self._last_access_camera and not self._last_access_camera:value("destroyed") and managers.player:upgrade_value("player", "sec_camera_highlight", false) and unit.base(unit)._tweak_table and ((managers.groupai:state():whisper_mode() and tweak_data.character[unit.base(unit)._tweak_table].silent_priority_shout) or tweak_data.character[unit.base(unit)._tweak_table].priority_shout) then
					managers.game_play_central:auto_highlight_enemy(unit, true)
				end
			end
		end
	end

	managers.hud:access_camera_track_max_amount(amount)

	return 
end
IngameAccessCamera.update_player_stamina = function (self, t, dt)
	local player = managers.player:player_unit()

	if player and player.movement(player) then
		player.movement(player):update_stamina(t, dt, true)
	end

	return 
end
IngameAccessCamera._player_damage = function (self, info)
	self.cb_leave(self)

	return 
end
IngameAccessCamera.at_enter = function (self, old_state, ...)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
		player.base(player):set_visible(false)
		player.character_damage(player):add_listener("IngameAccessCamera", {
			"hurt",
			"death"
		}, callback(self, self, "_player_damage"))
		SoundDevice:set_rtpc("stamina", 100)
	end

	self._sound_source = self._sound_source or SoundDevice:create_source("IngameAccessCamera")

	self._sound_source:post_event("camera_monitor_engage")
	managers.enemy:set_gfx_lod_enabled(false)

	self._old_state = old_state.name(old_state)

	if not managers.hud:exists(self.GUI_SAFERECT) then
		managers.hud:load_hud(self.GUI_FULLSCREEN, false, true, false, {})
		managers.hud:load_hud(self.GUI_SAFERECT, false, true, true, {})
	end

	managers.hud:show(self.GUI_SAFERECT)
	managers.hud:show(self.GUI_FULLSCREEN)
	managers.hud:start_access_camera()

	self._saved_default_color_grading = managers.environment_controller:default_color_grading()

	managers.environment_controller:set_default_color_grading("color_sin", true)

	self._cam_unit = CoreUnit.safe_spawn_unit("units/gui/background_camera_01/access_camera", Vector3(), Rotation())

	self._get_cameras(self)

	self._camera_data = {
		index = 0
	}
	self._no_feeds = not self._any_enabled_cameras(self)

	if self._no_feeds then
		managers.hud:set_access_camera_destroyed(true, true)
	else
		self._next_camera(self)
	end

	self._setup_controller(self)

	if _G.IS_VR then
		managers.menu:open_menu("ingame_access_camera_menu")
	end

	return 
end
IngameAccessCamera._any_enabled_cameras = function (self)
	if not self._cameras or #self._cameras == 0 then
		return false
	end

	for _, data in ipairs(self._cameras) do
		if data.access_camera:enabled() then
			return true
		end
	end

	return false
end
IngameAccessCamera.on_camera_access_changed = function (self, camera_unit)
	local access_camera = self._camera_data.index and self._cameras[self._camera_data.index] and self._cameras[self._camera_data.index].access_camera
	self._no_feeds = not self._any_enabled_cameras(self)

	if access_camera then
		managers.hud:set_access_camera_destroyed(not access_camera.enabled(access_camera), self._no_feeds)
	elseif self._no_feeds then
		managers.hud:set_access_camera_destroyed(true, true)
	else
		self._next_camera(self)
	end

	return 
end
IngameAccessCamera.at_exit = function (self)
	self._sound_source:post_event("camera_monitor_leave")
	managers.environment_controller:set_default_color_grading(self._saved_default_color_grading)
	managers.enemy:set_gfx_lod_enabled(true)
	self._clear_controller(self)
	World:delete_unit(self._cam_unit)
	managers.hud:hide(self.GUI_SAFERECT)
	managers.hud:hide(self.GUI_FULLSCREEN)
	managers.hud:stop_access_camera()

	if self._last_access_camera then
		self._last_access_camera:remove_trigger("IngameAccessCamera", "destroyed")
	end

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
		player.base(player):set_visible(true)
		player.character_damage(player):remove_listener("IngameAccessCamera")
	end

	if _G.IS_VR then
		managers.menu:close_menu("ingame_access_camera_menu")
	end

	return 
end
IngameAccessCamera.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameAccessCamera.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameAccessCamera.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
