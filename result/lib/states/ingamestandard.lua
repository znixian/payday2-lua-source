require("lib/states/GameState")

IngameStandardState = IngameStandardState or class(IngamePlayerBaseState)
IngameStandardState.init = function (self, game_state_machine)
	IngameStandardState.super.init(self, "ingame_standard", game_state_machine)

	return 
end
IngameStandardState.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
	end

	managers.hud:show(PlayerBase.PLAYER_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	return 
end
IngameStandardState.at_exit = function (self)
	managers.environment_controller:set_dof_distance()
	managers.hud:hide(PlayerBase.PLAYER_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	return 
end
IngameStandardState.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameStandardState.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameStandardState.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
