require("lib/states/GameState")

WorldCameraState = WorldCameraState or class(GameState)
WorldCameraState.init = function (self, game_state_machine)
	GameState.init(self, "world_camera", game_state_machine)

	return 
end
WorldCameraState.at_enter = function (self)
	return 
end
WorldCameraState.at_exit = function (self)
	return 
end

return 
