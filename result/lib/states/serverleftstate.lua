require("lib/states/GameState")

ServerLeftState = ServerLeftState or class(MissionEndState)
ServerLeftState.init = function (self, game_state_machine, setup)
	ServerLeftState.super.init(self, "server_left", game_state_machine, setup)

	return 
end
ServerLeftState.at_enter = function (self, ...)
	self._success = false
	self._server_left = true
	self._completion_bonus_done = true

	ServerLeftState.super.at_enter(self, ...)

	if Network:multiplayer() then
		self._shut_down_network(self)
	end

	if managers.network.matchmake then
		managers.network.matchmake._room_id = nil
	end

	if managers.crime_spree:is_active() then
		MenuCallbackHandler:create_server_left_crime_spree_dialog()
	else
		self._create_server_left_dialog(self)
	end

	return 
end
ServerLeftState.on_server_left = function (self)
	return 
end
ServerLeftState._create_server_left_dialog = function (self)
	MenuMainState._create_server_left_dialog(self)

	return 
end
ServerLeftState._load_start_menu = function (self)
	if not managers.job:stage_success() or not managers.job:on_last_stage() then
		setup:load_start_menu()
	end

	return 
end
ServerLeftState.on_server_left_ok_pressed = function (self)
	self._completion_bonus_done = true

	self._set_continue_button_text(self)

	return 
end

return 
