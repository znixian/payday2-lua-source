require("lib/states/GameState")

IngameElectrifiedState = IngameElectrifiedState or class(IngamePlayerBaseState)
IngameElectrifiedState.init = function (self, game_state_machine)
	IngamePlayerBaseState.super.init(self, "ingame_electrified", game_state_machine)

	return 
end
IngameElectrifiedState.update = function (self, t, dt)
	return 
end
IngameElectrifiedState.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
	end

	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	return 
end
IngameElectrifiedState.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	return 
end
IngameElectrifiedState.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameElectrifiedState.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameElectrifiedState.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
