require("lib/states/GameState")

IngameIncapacitatedState = IngameIncapacitatedState or class(IngamePlayerBaseState)
IngameIncapacitatedState.init = function (self, game_state_machine)
	IngameIncapacitatedState.super.init(self, "ingame_incapacitated", game_state_machine)

	return 
end
IngameIncapacitatedState.update = function (self, t, dt)
	local player = managers.player:player_unit()

	if not alive(player) then
		return 
	end

	if player.character_damage(player):update_incapacitated(t, dt) then
		IngameFatalState.on_local_player_dead()
		managers.player:on_enter_custody(player, true)
	end

	return 
end
IngameIncapacitatedState.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	managers.statistics:downed({
		incapacitated = true
	})

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
	end

	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	managers.hud:show(PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameIncapacitatedState.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	managers.hud:hide(PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameIncapacitatedState.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameIncapacitatedState.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameIncapacitatedState.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
