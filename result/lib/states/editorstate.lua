require("lib/states/GameState")

EditorState = EditorState or class(GameState)
EditorState.init = function (self, game_state_machine)
	GameState.init(self, "editor", game_state_machine)

	return 
end
EditorState.at_enter = function (self)
	cat_print("game_state_machine", "GAME STATE EditorState ENTER")

	return 
end
EditorState.at_exit = function (self)
	cat_print("game_state_machine", "GAME STATE EditorState ENTER")

	return 
end

return 
