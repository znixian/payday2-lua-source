require("lib/states/GameState")

IngameCivilianState = IngameCivilianState or class(IngamePlayerBaseState)
IngameCivilianState.init = function (self, game_state_machine)
	IngameCivilianState.super.init(self, "ingame_civilian", game_state_machine)

	return 
end
IngameCivilianState.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	managers.hud:hide_local_player_gear()

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
		player.character_damage(player):set_invulnerable(true)
	end

	return 
end
IngameCivilianState.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
		player.character_damage(player):set_invulnerable(false)
	end

	managers.hud:show_local_player_gear()
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)

	return 
end
IngameCivilianState.on_server_left = function (self)
	print("[IngameCivilianState:on server_left]")
	game_state_machine:change_state_by_name("server_left")

	return 
end
IngameCivilianState.on_kicked = function (self)
	print("[IngameCivilianState:on on_kicked]")
	game_state_machine:change_state_by_name("kicked")

	return 
end
IngameCivilianState.on_disconnected = function (self)
	game_state_machine:change_state_by_name("disconnected")

	return 
end

return 
