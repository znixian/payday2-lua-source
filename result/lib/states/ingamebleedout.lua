require("lib/states/GameState")

IngameBleedOutState = IngameBleedOutState or class(IngamePlayerBaseState)
IngameBleedOutState.init = function (self, game_state_machine)
	IngameBleedOutState.super.init(self, "ingame_bleed_out", game_state_machine)

	return 
end
IngameBleedOutState.update = function (self, t, dt)
	local player = managers.player:player_unit()

	if not alive(player) then
		return 
	end

	if player.movement(player):nav_tracker() and player.character_damage(player):update_downed(t, dt) then
		managers.player:on_enter_custody(player)
	end

	return 
end
IngameBleedOutState.at_enter = function (self)
	local players = managers.player:players()

	for k, player in ipairs(players) do
		local vp = player.camera(player):viewport()

		if vp then
			vp.set_active(vp, true)
		else
			Application:error("No viewport for player " .. tostring(k))
		end
	end

	managers.statistics:downed({
		bleed_out = true
	})

	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(true)
	end

	managers.hud:show(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:show(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	managers.hud:show(PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameBleedOutState.at_exit = function (self)
	local player = managers.player:player_unit()

	if player then
		player.base(player):set_enabled(false)
	end

	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD)
	managers.hud:hide(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
	managers.hud:hide(PlayerBase.PLAYER_DOWNED_HUD)

	return 
end
IngameBleedOutState.on_server_left = function (self)
	IngameCleanState.on_server_left(self)

	return 
end
IngameBleedOutState.on_kicked = function (self)
	IngameCleanState.on_kicked(self)

	return 
end
IngameBleedOutState.on_disconnected = function (self)
	IngameCleanState.on_disconnected(self)

	return 
end

return 
