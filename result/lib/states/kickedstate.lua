require("lib/states/GameState")

KickedState = KickedState or class(MissionEndState)
KickedState.init = function (self, game_state_machine, setup)
	KickedState.super.init(self, "kicked", game_state_machine, setup)

	return 
end
KickedState.at_enter = function (self, ...)
	self._kicked = true
	self._success = false
	self._completion_bonus_done = true

	KickedState.super.at_enter(self, ...)

	if Network:multiplayer() then
		self._shut_down_network(self)
	end

	self._create_kicked_dialog(self)

	return 
end
KickedState._create_kicked_dialog = function (self)
	if managers.crime_spree:is_active() then
		MenuCallbackHandler:show_peer_kicked_crime_spree_dialog()
	else
		managers.menu:show_peer_kicked_dialog()
	end

	return 
end
KickedState.on_kicked_ok_pressed = function (self)
	self._completion_bonus_done = true

	self._set_continue_button_text(self)

	return 
end
KickedState._load_start_menu = function (self)
	if not managers.job:stage_success() or not managers.job:on_last_stage() then
		setup:load_start_menu()
	end

	return 
end

return 
