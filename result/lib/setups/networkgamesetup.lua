require("lib/setups/GameSetup")
require("lib/network/base/NetworkManager")
require("lib/wip")

NetworkGameSetup = NetworkGameSetup or class(GameSetup)
NetworkGameSetup.init_managers = function (self, managers)
	GameSetup.init_managers(self, managers)

	managers.network = NetworkManager:new()

	return 
end
NetworkGameSetup.init_finalize = function (self)
	GameSetup.init_finalize(self)
	managers.network:init_finalize()

	return 
end
NetworkGameSetup.update = function (self, t, dt)
	GameSetup.update(self, t, dt)
	managers.network:update(t, dt)

	return 
end
NetworkGameSetup.paused_update = function (self, t, dt)
	GameSetup.paused_update(self, t, dt)
	managers.network:update(t, dt)

	return 
end
NetworkGameSetup.end_update = function (self, t, dt)
	GameSetup.end_update(self, t, dt)
	managers.network:end_update()

	return 
end
NetworkGameSetup.paused_end_update = function (self, t, dt)
	GameSetup.paused_end_update(self, t, dt)
	managers.network:end_update()

	return 
end

return NetworkGameSetup
