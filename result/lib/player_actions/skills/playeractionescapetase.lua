PlayerAction.EscapeTase = {
	Priority = 1,
	Function = function (player_manager, target_time)
		local time = Application:time()
		local controller = player_manager.player_unit(player_manager):base():controller()
		local co = coroutine.running()

		while time < target_time and player_manager.current_state(player_manager) == "tased" do
			time = Application:time()

			if controller.get_input_pressed(controller, "interact") then
				player_manager.send_message(player_manager, Message.EscapeTase, nil, nil)

				break
			end

			coroutine.yield(co)
		end

		return 
	end
}

return 
