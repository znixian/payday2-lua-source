PlayerAction.MessiahGetUp = {
	Priority = 1,
	Function = function (player_manager)
		managers.hint:show_hint("skill_messiah_get_up")

		local controller = player_manager.player_unit(player_manager):base():controller()
		local co = coroutine.running()

		while player_manager.current_state(player_manager) == "bleed_out" do
			if controller.get_input_pressed(controller, "jump") then
				player_manager.use_messiah_charge(player_manager)
				player_manager.send_message(player_manager, Message.RevivePlayer, nil, nil)

				break
			end

			coroutine.yield(co)
		end

		return 
	end
}

return 
