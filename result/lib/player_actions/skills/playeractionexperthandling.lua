PlayerAction.ExpertHandling = {
	Priority = 1,
	Function = function (player_manager, accuracy_bonus, max_stacks, max_time)
		local co = coroutine.running()
		local current_time = Application:time()
		local current_stacks = 0

		local function on_hit()
			current_stacks = current_stacks + 1

			if current_stacks <= max_stacks then
				player_manager:mul_to_property("desperado", accuracy_bonus)
			end

			return 
		end

		on_hit()
		player_manager.register_message(player_manager, Message.OnEnemyShot, co, on_hit)

		while current_time < max_time do
			current_time = Application:time()

			if not player_manager.is_current_weapon_of_category(player_manager, "pistol") then
				break
			end

			coroutine.yield(co)
		end

		player_manager.remove_property(player_manager, "desperado")
		player_manager.unregister_message(player_manager, Message.OnEnemyShot, co)

		return 
	end
}

return 
