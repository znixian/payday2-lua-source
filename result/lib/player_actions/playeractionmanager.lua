PlayerActionManager = PlayerActionManager or class()
PlayerActionManager.init = function (self)
	self._actions = {}
	self._buffer = {}

	return 
end
PlayerActionManager.update = function (self, t, dt)
	self._add(self)

	for key, value in pairs(self._actions) do
		if value and value.update(value, t, dt) then
			if value.on_exit then
				value.on_exit(value)
			end

			self._actions[key] = nil
		end
	end

	return 
end
PlayerActionManager.add_action = function (self, name, action)
	if not self._buffer[name] and not self._actions[name] then
		self._buffer[name] = action
	end

	return 
end
PlayerActionManager._add = function (self)
	for key, value in pairs(self._buffer) do
		if value.on_enter then
			value.on_enter(value)
		end

		self._actions[key] = value
		self._buffer[key] = nil
	end

	self._buffer = nil
	self._buffer = {}

	return 
end
PlayerActionManager.is_running = function (self, name)
	return self._actions[name]
end

return 
