local tmp_rot1 = Rotation()
UnitNetworkHandler = UnitNetworkHandler or class(BaseNetworkHandler)
UnitNetworkHandler.set_unit = function (self, unit, character_name, outfit_string, outfit_version, peer_id, team_id)
	print("[UnitNetworkHandler:set_unit]", unit, character_name, peer_id)
	Application:stack_dump()

	if not alive(unit) then
		return 
	end

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if peer_id == 0 then
		local loadout = managers.blackmarket:unpack_henchman_loadout_string(outfit_string)

		managers.blackmarket:verfify_recived_crew_loadout(loadout, true)

		local crim_data = managers.criminals:character_data_by_name(character_name)

		if not crim_data or not crim_data.ai then
			managers.criminals:add_character(character_name, unit, peer_id, true, loadout)
		else
			managers.criminals:set_unit(character_name, unit, loadout)
		end

		unit.movement(unit):set_character_anim_variables()

		return 
	end

	local peer = managers.network:session():peer(peer_id)

	if not peer then
		return 
	end

	if peer ~= managers.network:session():local_peer() then
		peer.set_outfit_string(peer, outfit_string, outfit_version)
	end

	peer.set_unit(peer, unit, character_name)
	self._chk_flush_unit_too_early_packets(self, unit)

	return 
end
UnitNetworkHandler.switch_weapon = function (self, unit, unequip_multiplier, equip_multiplier, sender)
	if not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.movement(unit):sync_switch_weapon(unequip_multiplier, equip_multiplier)

	return 
end
UnitNetworkHandler.set_equipped_weapon = function (self, unit, item_index, blueprint_string, cosmetics_string, sender)
	if not self._verify_character(unit) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	unit.inventory(unit):synch_equipped_weapon(item_index, blueprint_string, cosmetics_string, peer)

	return 
end
UnitNetworkHandler.set_weapon_gadget_state = function (self, unit, gadget_state, sender)
	if not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.inventory(unit):synch_weapon_gadget_state(gadget_state)

	return 
end
UnitNetworkHandler.set_weapon_gadget_color = function (self, unit, red, green, blue, sender)
	if not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.inventory(unit):sync_weapon_gadget_color(Color(red/255, green/255, blue/255))

	return 
end
UnitNetworkHandler.first_aid_kit_sync = function (self, unit, min_distance)
	if min_distance ~= 0 then
		unit.base(unit):sync_auto_recovery(min_distance)
	end

	return 
end
UnitNetworkHandler.set_look_dir = function (self, unit, yaw_in, pitch_in, sender)
	if not self._verify_character_and_sender(unit, sender) then
		return 
	end

	local dir = Vector3()
	local yaw = (yaw_in*360)/255
	local pitch = math.lerp(-85, 85, pitch_in/127)
	local rot = Rotation(yaw, pitch, 0)

	mrotation.y(rot, dir)
	unit.movement(unit):sync_look_dir(dir)

	return 
end
UnitNetworkHandler.action_walk_start = function (self, unit, first_nav_point, nav_link_yaw, nav_link_act_index, from_idle, haste_code, end_yaw, no_walk, no_strafe, pose_code, end_pose_code)
	if not self._verify_character(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local end_rot = nil

	if end_yaw ~= 0 then
		end_rot = Rotation(((end_yaw - 1)*360)/254, 0, 0)
	end

	local nav_path = {}

	if nav_link_act_index ~= 0 then
		local nav_link_rot = (nav_link_yaw*360)/255
		local nav_link = unit.movement(unit)._actions.walk.synthesize_nav_link(first_nav_point, nav_link_rot, unit.movement(unit)._actions.act:_get_act_name_from_index(nav_link_act_index), from_idle)
		nav_link.element.value = function (element, name)
			return element[name]
		end
		nav_link.element.nav_link_wants_align_pos = function (element)
			return element.from_idle
		end

		table.insert(nav_path, nav_link)
	else
		table.insert(nav_path, first_nav_point)
	end

	local pose = nil

	if pose_code == 1 then
		pose = "stand"
	elseif pose_code == 2 then
		pose = "crouch"
	end

	local end_pose = nil

	if end_pose_code == 1 then
		end_pose = "stand"
	elseif end_pose_code == 2 then
		end_pose = "crouch"
	end

	local action_desc = {
		path_simplified = true,
		type = "walk",
		persistent = true,
		body_part = 2,
		variant = (haste_code == 1 and "walk") or "run",
		end_rot = end_rot,
		nav_path = nav_path,
		no_walk = no_walk,
		no_strafe = no_strafe,
		pose = pose,
		end_pose = end_pose,
		blocks = {
			act = -1,
			idle = -1,
			turn = -1,
			walk = -1
		}
	}

	unit.movement(unit):action_request(action_desc)

	return 
end
UnitNetworkHandler.action_walk_nav_point = function (self, unit, nav_point, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_walk_nav_point(nav_point)

	return 
end
UnitNetworkHandler.player_action_walk_nav_point = function (self, unit, nav_point, speed, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_walk_nav_point(nav_point, speed)

	return 
end
UnitNetworkHandler.action_change_run = function (self, unit, is_running, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_change_run(is_running)

	return 
end
UnitNetworkHandler.action_change_speed = function (self, unit, speed, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_change_speed(speed)

	return 
end
UnitNetworkHandler.action_walk_stop = function (self, unit)
	if not self._verify_character(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_walk_stop()

	return 
end
UnitNetworkHandler.action_walk_nav_link = function (self, unit, pos, yaw, anim_index, from_idle)
	if not self._verify_character(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local rot = (yaw*360)/255

	unit.movement(unit):sync_action_walk_nav_link(pos, rot, anim_index, from_idle)

	return 
end
UnitNetworkHandler.action_change_pose = function (self, unit, pose_code, pos)
	if not self._verify_character(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_change_pose(pose_code, pos)

	return 
end
UnitNetworkHandler.action_spooc_start = function (self, unit, target_u_pos, flying_strike, action_id)
	if not self._verify_character(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local action_desc = {
		block_type = "walk",
		type = "spooc",
		path_index = 1,
		body_part = 1,
		nav_path = {
			unit.position(unit)
		},
		target_u_pos = target_u_pos,
		flying_strike = flying_strike,
		action_id = action_id,
		blocks = {
			act = -1,
			idle = -1,
			turn = -1,
			walk = -1
		}
	}

	if flying_strike then
		action_desc.blocks.light_hurt = -1
		action_desc.blocks.hurt = -1
		action_desc.blocks.heavy_hurt = -1
		action_desc.blocks.expl_hurt = -1
	end

	unit.movement(unit):action_request(action_desc)

	return 
end
UnitNetworkHandler.action_spooc_stop = function (self, unit, pos, nav_index, action_id, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_spooc_stop(pos, nav_index, action_id)

	return 
end
UnitNetworkHandler.action_spooc_nav_point = function (self, unit, pos, action_id, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_spooc_nav_point(pos, action_id)

	return 
end
UnitNetworkHandler.action_spooc_strike = function (self, unit, pos, action_id, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):sync_action_spooc_strike(pos, action_id)

	return 
end
UnitNetworkHandler.action_warp_start = function (self, unit, has_pos, pos, has_rot, yaw, sender)
	if not self._verify_character(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local action_desc = {
		body_part = 1,
		type = "warp",
		position = has_pos and pos,
		rotation = has_rot and Rotation(((yaw - 1)*360)/254, 0, 0)
	}

	unit.movement(unit):action_request(action_desc)

	return 
end
UnitNetworkHandler.friendly_fire_hit = function (self, subject_unit)
	if not self._verify_character(subject_unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	subject_unit.character_damage(subject_unit):friendly_fire_hit()

	return 
end
UnitNetworkHandler.damage_bullet = function (self, subject_unit, attacker_unit, damage, i_body, height_offset, variant, death, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	subject_unit.character_damage(subject_unit):sync_damage_bullet(attacker_unit, damage, i_body, height_offset, variant, death)

	return 
end
UnitNetworkHandler.damage_explosion_fire = function (self, subject_unit, attacker_unit, damage, i_attack_variant, death, direction, weapon_unit, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	if i_attack_variant == 3 then
		subject_unit.character_damage(subject_unit):sync_damage_fire(attacker_unit, damage, i_attack_variant, death, direction)
	else
		subject_unit.character_damage(subject_unit):sync_damage_explosion(attacker_unit, damage, i_attack_variant, death, direction, weapon_unit)
	end

	return 
end
UnitNetworkHandler.damage_explosion_stun = function (self, subject_unit, attacker_unit, damage, i_attack_variant, death, direction, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	if i_attack_variant then
		subject_unit.character_damage(subject_unit):sync_damage_stun(attacker_unit, damage, i_attack_variant, death, direction)
	end

	return 
end
UnitNetworkHandler.damage_fire = function (self, subject_unit, attacker_unit, damage, start_dot_dance_antimation, death, direction, weapon_type, weapon_unit, healed, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	subject_unit.character_damage(subject_unit):sync_damage_fire(attacker_unit, damage, start_dot_dance_antimation, death, direction, weapon_type, weapon_unit, healed)

	return 
end
UnitNetworkHandler.damage_dot = function (self, subject_unit, attacker_unit, damage, death, variant, hurt_animation, weapon_id, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	subject_unit.character_damage(subject_unit):sync_damage_dot(attacker_unit, damage, death, variant, hurt_animation, weapon_id)

	return 
end
UnitNetworkHandler.damage_tase = function (self, subject_unit, attacker_unit, damage, variant, death, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	subject_unit.character_damage(subject_unit):sync_damage_tase(attacker_unit, damage, variant, death)

	return 
end
UnitNetworkHandler.damage_melee = function (self, subject_unit, attacker_unit, damage, damage_effect, i_body, height_offset, variant, death, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	subject_unit.character_damage(subject_unit):sync_damage_melee(attacker_unit, damage, damage_effect, i_body, height_offset, variant, death)

	return 
end
UnitNetworkHandler.from_server_damage_bullet = function (self, subject_unit, attacker_unit, hit_offset_height, result_index, sender)
	if not self._verify_character_and_sender(subject_unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	subject_unit.character_damage(subject_unit):sync_damage_bullet(attacker_unit, hit_offset_height, result_index)

	return 
end
UnitNetworkHandler.from_server_damage_explosion_fire = function (self, subject_unit, attacker_unit, result_index, i_attack_variant, sender)
	if not self._verify_character(subject_unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	if i_attack_variant == 3 then
		subject_unit.character_damage(subject_unit):sync_damage_fire(attacker_unit, result_index, i_attack_variant)
	else
		subject_unit.character_damage(subject_unit):sync_damage_explosion(attacker_unit, result_index, i_attack_variant)
	end

	return 
end
UnitNetworkHandler.from_server_damage_melee = function (self, subject_unit, attacker_unit, hit_offset_height, result_index, sender)
	if not self._verify_character(subject_unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(attacker_unit) or attacker_unit.key(attacker_unit) == subject_unit.key(subject_unit) then
		attacker_unit = nil
	end

	subject_unit.character_damage(subject_unit):sync_damage_melee(attacker_unit, attacker_unit, hit_offset_height, result_index)

	return 
end
UnitNetworkHandler.from_server_damage_incapacitated = function (self, subject_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(subject_unit) then
		return 
	end

	subject_unit.character_damage(subject_unit):sync_damage_incapacitated()

	return 
end
UnitNetworkHandler.from_server_damage_bleeding = function (self, subject_unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(subject_unit) then
		return 
	end

	subject_unit.character_damage(subject_unit):sync_damage_bleeding()

	return 
end
UnitNetworkHandler.from_server_damage_tase = function (self, subject_unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(subject_unit) then
		return 
	end

	subject_unit.character_damage(subject_unit):sync_damage_tase()

	return 
end
UnitNetworkHandler.from_server_unit_recovered = function (self, subject_unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(subject_unit) then
		return 
	end

	subject_unit.character_damage(subject_unit):sync_unit_recovered()

	return 
end
UnitNetworkHandler.shot_blank = function (self, shooting_unit, impact, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(shooting_unit, sender) then
		return 
	end

	shooting_unit.movement(shooting_unit):sync_shot_blank(impact)

	return 
end
UnitNetworkHandler.sync_start_auto_fire_sound = function (self, shooting_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(shooting_unit, sender) then
		return 
	end

	shooting_unit.movement(shooting_unit):sync_start_auto_fire_sound()

	return 
end
UnitNetworkHandler.sync_stop_auto_fire_sound = function (self, shooting_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(shooting_unit, sender) then
		return 
	end

	shooting_unit.movement(shooting_unit):sync_stop_auto_fire_sound()

	return 
end
UnitNetworkHandler.shot_blank_reliable = function (self, shooting_unit, impact, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(shooting_unit, sender) then
		return 
	end

	shooting_unit.movement(shooting_unit):sync_shot_blank(impact)

	return 
end
UnitNetworkHandler.reload_weapon = function (self, unit, empty_reload, reload_speed_multiplier, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.movement(unit):sync_reload_weapon(empty_reload, reload_speed_multiplier)

	return 
end
UnitNetworkHandler.reload_weapon_cop = function (self, unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.inventory(unit):equipped_unit():base():ammo_base():set_ammo_remaining_in_clip(0)

	return 
end
UnitNetworkHandler.reload_weapon_interupt = function (self, unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.movement(unit):sync_reload_weapon_interupt()

	return 
end
UnitNetworkHandler.run_mission_element = function (self, id, unit, orientation_element_index)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		if self._verify_gamestate(self._gamestate_filter.any_end_game) then
			managers.mission:client_run_mission_element_end_screen(id, unit, orientation_element_index)

			return 
		end

		print("UnitNetworkHandler:run_mission_element discarded id:", id)

		return 
	end

	managers.mission:client_run_mission_element(id, unit, orientation_element_index)

	return 
end
UnitNetworkHandler.run_mission_element_no_instigator = function (self, id, orientation_element_index)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		if self._verify_gamestate(self._gamestate_filter.any_end_game) then
			managers.mission:client_run_mission_element_end_screen(id, nil, orientation_element_index)
		end

		return 
	end

	managers.mission:client_run_mission_element(id, nil, orientation_element_index)

	return 
end
UnitNetworkHandler.to_server_mission_element_trigger = function (self, id, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.mission:server_run_mission_element_trigger(id, unit)

	return 
end
UnitNetworkHandler.to_server_area_event = function (self, event_id, id, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.mission:to_server_area_event(event_id, id, unit)

	return 
end
UnitNetworkHandler.to_server_access_camera_trigger = function (self, id, trigger, instigator)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.mission:to_server_access_camera_trigger(id, trigger, instigator)

	return 
end
UnitNetworkHandler.sync_body_damage_bullet = function (self, body, attacker, normal_yaw, normal_pitch, position, direction_yaw, direction_pitch, damage, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(body) then
		return 
	end

	if not body.extension(body) then
		print("[UnitNetworkHandler:sync_body_damage_bullet] body has no extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage then
		print("[UnitNetworkHandler:sync_body_damage_bullet] body has no damage extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage.damage_bullet then
		print("[UnitNetworkHandler:sync_body_damage_bullet] body has no damage damage_bullet function", body.name(body), body.unit(body):name())

		return 
	end

	local normal = Vector3()

	mrotation.set_yaw_pitch_roll(tmp_rot1, math.lerp(-180, 180, normal_yaw/127), math.lerp(-90, 90, normal_pitch/63), 0)
	mrotation.y(tmp_rot1, normal)

	local direction = Vector3()

	mrotation.set_yaw_pitch_roll(tmp_rot1, math.lerp(-180, 180, direction_yaw/127), math.lerp(-90, 90, direction_pitch/63), 0)
	mrotation.y(tmp_rot1, direction)
	body.extension(body).damage:damage_bullet(attacker, normal, position, direction, 1)
	body.extension(body).damage:damage_damage(attacker, normal, position, direction, damage/163.84)

	return 
end
UnitNetworkHandler.sync_body_damage_lock = function (self, body, damage, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(body) then
		return 
	end

	if not body.extension(body) then
		print("[UnitNetworkHandler:sync_body_damage_lock] body has no extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage then
		print("[UnitNetworkHandler:sync_body_damage_lock] body has no damage extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage.damage_lock then
		print("[UnitNetworkHandler:sync_body_damage_lock] body has no damage damage_lock function", body.name(body), body.unit(body):name())

		return 
	end

	body.extension(body).damage:damage_lock(nil, nil, nil, nil, damage)

	return 
end
UnitNetworkHandler.sync_body_damage_explosion = function (self, body, attacker, normal, position, direction, damage, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(body) then
		return 
	end

	if not body.extension(body) then
		print("[UnitNetworkHandler:sync_body_damage_explosion] body has no extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage then
		print("[UnitNetworkHandler:sync_body_damage_explosion] body has no damage extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage.damage_explosion then
		print("[UnitNetworkHandler:sync_body_damage_explosion] body has no damage damage_explosion function", body.name(body), body.unit(body):name())

		return 
	end

	body.extension(body).damage:damage_explosion(attacker, normal, position, direction, damage/163.84)
	body.extension(body).damage:damage_damage(attacker, normal, position, direction, damage/163.84)

	return 
end
UnitNetworkHandler.sync_body_damage_explosion_no_attacker = function (self, body, normal, position, direction, damage, sender)
	self.sync_body_damage_explosion(self, body, nil, normal, position, direction, damage, sender)

	return 
end
UnitNetworkHandler.sync_body_damage_fire = function (self, body, attacker, normal, position, direction, damage, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(body) then
		return 
	end

	if not body.extension(body) then
		print("[UnitNetworkHandler:sync_body_damage_fire] body has no extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage then
		print("[UnitNetworkHandler:sync_body_damage_fire] body has no damage extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage.damage_fire then
		print("[UnitNetworkHandler:sync_body_damage_fire] body has no damage damage_fire function", body.name(body), body.unit(body):name())

		return 
	end

	body.extension(body).damage:damage_fire(attacker, normal, position, direction, damage/163.84)
	body.extension(body).damage:damage_damage(attacker, normal, position, direction, damage/163.84)

	return 
end
UnitNetworkHandler.sync_body_damage_fire_no_attacker = function (self, body, normal, position, direction, damage, sender)
	self.sync_body_damage_fire(self, body, nil, normal, position, direction, damage, sender)

	return 
end
UnitNetworkHandler.sync_body_damage_melee = function (self, body, attacker, normal, position, direction, damage, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(body) then
		return 
	end

	if not body.extension(body) then
		print("[UnitNetworkHandler:sync_body_damage_melee] body has no extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage then
		print("[UnitNetworkHandler:sync_body_damage_melee] body has no damage extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage.damage_melee then
		print("[UnitNetworkHandler:sync_body_damage_melee] body has no damage damage_melee function", body.name(body), body.unit(body):name())

		return 
	end

	body.extension(body).damage:damage_melee(attacker, normal, position, direction, damage)

	return 
end
UnitNetworkHandler.sync_interacted = function (self, unit, unit_id, tweak_setting, status, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	if Network:is_server() and unit_id ~= -2 then
		if alive(unit) and unit.interaction(unit) and unit.interaction(unit).tweak_data == tweak_setting and unit.interaction(unit):active() then
			sender.sync_interaction_reply(sender, true)
		else
			sender.sync_interaction_reply(sender, false)

			return 
		end
	end

	if alive(unit) and unit.interaction(unit) then
		if unit.interaction(unit)._special_equipment and unit.interaction(unit).apply_item_pickup then
			managers.network:session():send_to_peer(peer, "special_eq_response", unit)

			if unit.interaction(unit):can_remove_item() then
				unit.set_slot(unit, 0)
			end
		end

		local char_unit = managers.criminals:character_unit_by_peer_id(peer.id(peer))

		unit.interaction(unit):sync_interacted(peer, char_unit, status)
	end

	return 
end
UnitNetworkHandler.sync_multiple_equipment_bag_interacted = function (self, unit, amount_wanted, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	if unit and alive(unit) and unit.interaction(unit) then
		local char_unit = managers.criminals:character_unit_by_peer_id(peer.id(peer))

		unit.interaction(unit):sync_interacted(peer, char_unit, amount_wanted)
	end

	return 
end
UnitNetworkHandler.sync_interacted_by_id = function (self, unit_id, tweak_setting, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) and not self._verify_sender(sender) then
		return 
	end

	local u_data = managers.enemy:get_corpse_unit_data_from_id(unit_id)

	if not u_data then
		sender.sync_interaction_reply(sender, false)

		return 
	end

	self.sync_interacted(self, u_data.unit, unit_id, tweak_setting, 1, sender)

	return 
end
UnitNetworkHandler.sync_interaction_reply = function (self, status)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(managers.player:player_unit()) then
		return 
	end

	managers.player:from_server_interaction_reply(status)

	return 
end
UnitNetworkHandler.interaction_set_active = function (self, unit, u_id, active, tweak_data, flash, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(unit) then
		local u_data = managers.enemy:get_corpse_unit_data_from_id(u_id)

		if not u_data then
			return 
		end

		unit = u_data.unit
	end

	unit.interaction(unit):set_tweak_data(tweak_data)
	unit.interaction(unit):set_active(active)

	if flash then
		unit.interaction(unit):set_outline_flash_state(true, nil)
	end

	return 
end
UnitNetworkHandler.sync_teammate_progress = function (self, type_index, enabled, tweak_data_id, timer, success, sender)
	local sender_peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not sender_peer then
		return 
	end

	local peer_id = sender_peer.id(sender_peer)

	managers.hud:teammate_progress(peer_id, type_index, enabled, tweak_data_id, timer, success)

	return 
end
UnitNetworkHandler.action_aim_state = function (self, cop)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(cop) then
		return 
	end

	local shoot_action = {
		block_type = "action",
		body_part = 3,
		type = "shoot"
	}

	cop.movement(cop):action_request(shoot_action)

	return 
end
UnitNetworkHandler.action_aim_end = function (self, cop)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(cop) then
		return 
	end

	cop.movement(cop):sync_action_aim_end()

	return 
end
UnitNetworkHandler.action_hurt_start = function (self, unit, hurt_type, body_part, death_type, type, variant, direction_vec, hit_pos)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	if unit.movement(unit):can_request_actions() then
		local hurt_action = {
			allow_network = false,
			block_type = CopActionHurt.idx_to_hurt_type(hurt_type),
			blocks = {
				tase = -1,
				act = -1,
				action = -1,
				walk = -1,
				aim = -1
			},
			body_part = body_part,
			death_type = CopActionHurt.idx_to_death_type(death_type),
			hurt_type = CopActionHurt.idx_to_hurt_type(hurt_type),
			type = CopActionHurt.idx_to_type(type),
			variant = CopActionHurt.idx_to_variant(variant),
			direction_vec = direction_vec,
			hit_pos = hit_pos
		}

		unit.movement(unit):action_request(hurt_action)
	end

	return 
end
UnitNetworkHandler.action_hurt_end = function (self, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.movement(unit):sync_action_hurt_end()

	return 
end
UnitNetworkHandler.set_attention = function (self, unit, target_unit, reaction, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	if not alive(target_unit) then
		unit.movement(unit):synch_attention()

		return 
	end

	local handler = nil

	if target_unit.attention(target_unit) then
		handler = target_unit.attention(target_unit)
	elseif target_unit.brain(target_unit) and target_unit.brain(target_unit).attention_handler then
		handler = target_unit.brain(target_unit):attention_handler()
	elseif target_unit.movement(target_unit) and target_unit.movement(target_unit).attention_handler then
		handler = target_unit.movement(target_unit):attention_handler()
	elseif target_unit.base(target_unit) and target_unit.base(target_unit).attention_handler then
		handler = target_unit.base(target_unit):attention_handler()
	end

	if not handler and (not target_unit.movement(target_unit) or not target_unit.movement(target_unit).m_head_pos) then
		debug_pause_unit(target_unit, "[UnitNetworkHandler:set_attention] no attention handler or m_head_pos", target_unit)

		return 
	end

	unit.movement(unit):synch_attention({
		unit = target_unit,
		u_key = target_unit.key(target_unit),
		handler = handler,
		reaction = reaction
	})

	return 
end
UnitNetworkHandler.cop_set_attention_pos = function (self, unit, pos)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.movement(unit):synch_attention({
		pos = pos,
		reaction = AIAttentionObject.REACT_IDLE
	})

	return 
end
UnitNetworkHandler.set_allow_fire = function (self, unit, state)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.movement(unit):synch_allow_fire(state)

	return 
end
UnitNetworkHandler.set_stance = function (self, unit, stance_code, instant, execute_queued, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.movement(unit):sync_stance(stance_code, instant, execute_queued)

	return 
end
UnitNetworkHandler.set_pose = function (self, unit, pose_code, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.movement(unit):sync_pose(pose_code)

	return 
end
UnitNetworkHandler.long_dis_interaction = function (self, target_unit, amount, aggressor_unit, secondary)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(target_unit) or not self._verify_character(aggressor_unit) then
		return 
	end

	local target_is_criminal = target_unit.in_slot(target_unit, managers.slot:get_mask("criminals")) or target_unit.in_slot(target_unit, managers.slot:get_mask("harmless_criminals"))
	local target_is_civilian = not target_is_criminal and target_unit.in_slot(target_unit, 21)
	local aggressor_is_criminal = aggressor_unit.in_slot(aggressor_unit, managers.slot:get_mask("criminals")) or aggressor_unit.in_slot(aggressor_unit, managers.slot:get_mask("harmless_criminals"))

	if target_is_criminal then
		if aggressor_is_criminal then
			if target_unit.brain(target_unit) then
				if target_unit.brain(target_unit).on_long_dis_interacted then
					target_unit.movement(target_unit):set_cool(false)
					target_unit.brain(target_unit):on_long_dis_interacted(amount, aggressor_unit, secondary)
				end
			elseif amount == 1 then
				target_unit.movement(target_unit):on_morale_boost(aggressor_unit)
			end
		else
			target_unit.brain(target_unit):on_intimidated(amount/10, aggressor_unit)
		end
	elseif amount == 0 and target_is_civilian and aggressor_is_criminal then
		if self._verify_in_server_session() then
			aggressor_unit.movement(aggressor_unit):sync_call_civilian(target_unit)
		end
	elseif target_unit.brain(target_unit) then
		target_unit.brain(target_unit):on_intimidated(amount/10, aggressor_unit)
	end

	return 
end
UnitNetworkHandler.alarm_pager_interaction = function (self, u_id, tweak_table, status, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local unit_data = managers.enemy:get_corpse_unit_data_from_id(u_id)

	if unit_data and unit_data.unit:interaction():active() and unit_data.unit:interaction().tweak_data == tweak_table then
		local peer = self._verify_sender(sender)

		if peer then
			local status_str = nil

			if status == 1 then
				status_str = "started"
			elseif status == 2 then
				status_str = "interrupted"
			else
				status_str = "complete"
			end

			unit_data.unit:interaction():sync_interacted(peer, nil, status_str)
		end
	end

	return 
end
UnitNetworkHandler.remove_corpse_by_id = function (self, u_id, carry_bodybag, peer_id, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if carry_bodybag and Network:is_client() then
		managers.player:register_carry(managers.network:session():peer(peer_id), "person")
	end

	managers.enemy:remove_corpse_by_id(u_id)

	return 
end
UnitNetworkHandler.unit_tied = function (self, unit, aggressor, can_flee)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.brain(unit):on_tied(aggressor, false, can_flee)

	return 
end
UnitNetworkHandler.unit_traded = function (self, unit, position, rotation)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.brain(unit):on_trade(position, rotation, true)

	return 
end
UnitNetworkHandler.hostage_trade = function (self, unit, enable, trade_success, skip_hint)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	CopLogicTrade.hostage_trade(unit, enable, trade_success, skip_hint)

	return 
end
UnitNetworkHandler.set_unit_invulnerable = function (self, unit, enable)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.character_damage(unit):set_invulnerable(enable)

	return 
end
UnitNetworkHandler.set_trade_countdown = function (self, enable)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.trade:set_trade_countdown(enable)

	return 
end
UnitNetworkHandler.set_trade_death = function (self, criminal_name, respawn_penalty, hostages_killed)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.trade:sync_set_trade_death(criminal_name, respawn_penalty, hostages_killed)

	return 
end
UnitNetworkHandler.set_trade_spawn = function (self, criminal_name)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.trade:sync_set_trade_spawn(criminal_name)

	return 
end
UnitNetworkHandler.set_trade_replace = function (self, replace_ai, criminal_name1, criminal_name2, respawn_penalty)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.trade:sync_set_trade_replace(replace_ai, criminal_name1, criminal_name2, respawn_penalty)

	return 
end
UnitNetworkHandler.action_idle_start = function (self, unit, body_part, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.movement(unit):action_request({
		type = "idle",
		body_part = body_part
	})

	return 
end
UnitNetworkHandler.action_act_start = function (self, unit, act_index, blocks_hurt, clamp_to_graph, needs_full_blend)
	self.action_act_start_align(self, unit, act_index, blocks_hurt, clamp_to_graph, needs_full_blend, nil, nil)

	return 
end
UnitNetworkHandler.action_act_start_align = function (self, unit, act_index, blocks_hurt, clamp_to_graph, needs_full_blend, start_yaw, start_pos)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	local start_rot = nil

	if start_yaw and start_yaw ~= 0 then
		start_rot = Rotation(((start_yaw - 1)*360)/254, 0, 0)
	end

	unit.movement(unit):sync_action_act_start(act_index, blocks_hurt, clamp_to_graph, needs_full_blend, start_rot, start_pos)

	return 
end
UnitNetworkHandler.action_act_end = function (self, unit)
	if not alive(unit) or unit.character_damage(unit):dead() then
		return 
	end

	unit.movement(unit):sync_action_act_end()

	return 
end
UnitNetworkHandler.action_dodge_start = function (self, unit, body_part, variation, side, rotation, speed, shoot_acc)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.movement(unit):sync_action_dodge_start(body_part, variation, side, rotation, speed, shoot_acc)

	return 
end
UnitNetworkHandler.action_dodge_end = function (self, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.movement(unit):sync_action_dodge_end()

	return 
end
UnitNetworkHandler.action_tase_event = function (self, taser_unit, event_id, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(taser_unit) then
		return 
	end

	local sender_peer = self._verify_sender(sender)

	if not sender_peer then
		return 
	end

	if event_id == 1 then
		if not managers.network:session():is_client() or managers.network:session():server_peer() ~= sender_peer then
			return 
		end

		local tase_action = {
			body_part = 3,
			type = "tase"
		}

		taser_unit.movement(taser_unit):action_request(tase_action)
	elseif event_id == 2 then
		if not managers.network:session():is_client() or managers.network:session():server_peer() ~= sender_peer then
			return 
		end

		taser_unit.movement(taser_unit):sync_action_tase_end()
	else
		taser_unit.movement(taser_unit):sync_taser_fire()
	end

	return 
end
UnitNetworkHandler.alert = function (self, alerted_unit, aggressor)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(alerted_unit) or not self._verify_character(aggressor) then
		return 
	end

	local record = managers.groupai:state():criminal_record(aggressor.key(aggressor))

	if not record then
		return 
	end

	local aggressor_pos = nil

	if aggressor.movement(aggressor) and aggressor.movement(aggressor).m_head_pos then
		aggressor_pos = aggressor.movement(aggressor):m_head_pos()
	else
		aggressor_pos = aggressor.position(aggressor)
	end

	alerted_unit.brain(alerted_unit):on_alert({
		"aggression",
		aggressor_pos,
		false,
		[5] = aggressor
	})

	return 
end
UnitNetworkHandler.revive_player = function (self, revive_health_level, revive_damage_reduction, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.need_revive) or not peer then
		return 
	end

	local player = managers.player:player_unit()

	if 0 < revive_health_level and alive(player) then
		player.character_damage(player):set_revive_boost(revive_health_level)
	end

	if 0 < revive_damage_reduction then
		revive_damage_reduction = math.clamp(revive_damage_reduction, 1, 2)
		local tweak = tweak_data.upgrades.first_aid_kit.revived_damage_reduction[revive_damage_reduction]

		managers.player:activate_temporary_property("revived_damage_reduction", tweak[2], tweak[1])
	end

	if alive(player) then
		player.character_damage(player):revive()
	end

	return 
end
UnitNetworkHandler.start_revive_player = function (self, timer, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.downed) or not peer then
		return 
	end

	local player = managers.player:player_unit()

	if alive(player) then
		player.character_damage(player):pause_downed_timer(timer, peer.id(peer))
	end

	return 
end
UnitNetworkHandler.pause_downed_timer = function (self, unit, sender)
	if alive(unit) and unit.interaction then
		unit.interaction(unit):set_waypoint_paused(true)
	end

	return 
end
UnitNetworkHandler.unpause_downed_timer = function (self, unit, sender)
	if alive(unit) and unit.interaction then
		unit.interaction(unit):set_waypoint_paused(false)
	end

	return 
end
UnitNetworkHandler.interupt_revive_player = function (self, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.downed) or not peer then
		return 
	end

	local player = managers.player:player_unit()

	if alive(player) then
		player.character_damage(player):unpause_downed_timer(peer.id(peer))
	end

	return 
end
UnitNetworkHandler.start_free_player = function (self, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.arrested) or not peer then
		return 
	end

	local player = managers.player:player_unit()

	if alive(player) then
		player.character_damage(player):pause_arrested_timer(peer.id(peer))
	end

	return 
end
UnitNetworkHandler.interupt_free_player = function (self, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.arrested) or not peer then
		return 
	end

	local player = managers.player:player_unit()

	if alive(player) then
		player.character_damage(player):unpause_arrested_timer(peer.id(peer))
	end

	return 
end
UnitNetworkHandler.pause_arrested_timer = function (self, unit, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer or not self._verify_character(unit) then
		return 
	end

	unit.character_damage(unit):pause_arrested_timer(peer.id(peer))

	return 
end
UnitNetworkHandler.unpause_arrested_timer = function (self, unit, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer or not self._verify_character(unit) then
		return 
	end

	unit.character_damage(unit):unpause_arrested_timer(peer.id(peer))

	return 
end
UnitNetworkHandler.revive_unit = function (self, unit, reviving_unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) or not alive(reviving_unit) then
		return 
	end

	unit.interaction(unit):interact(reviving_unit)

	return 
end
UnitNetworkHandler.pause_bleed_out = function (self, unit, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer or not self._verify_character(unit) then
		return 
	end

	unit.character_damage(unit):pause_bleed_out(peer.id(peer))

	return 
end
UnitNetworkHandler.unpause_bleed_out = function (self, unit, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer or not self._verify_character(unit) then
		return 
	end

	unit.character_damage(unit):unpause_bleed_out(peer.id(peer))

	return 
end
UnitNetworkHandler.interaction_set_waypoint_paused = function (self, unit, paused, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(unit) then
		return 
	end

	if not unit.interaction(unit) then
		return 
	end

	unit.interaction(unit):set_waypoint_paused(paused)

	return 
end
UnitNetworkHandler.place_trip_mine = function (self, pos, normal, sensor_upgrade, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	if not managers.player:verify_equipment(peer.id(peer), "trip_mine") then
		return 
	end

	local rot = Rotation(normal, math.UP)
	local peer = self._verify_sender(rpc)
	local unit = TripMineBase.spawn(pos, rot, sensor_upgrade, peer.id(peer))

	unit.base(unit):set_server_information(peer.id(peer))
	rpc.activate_trip_mine(rpc, unit)

	return 
end
UnitNetworkHandler.activate_trip_mine = function (self, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if alive(unit) then
		unit.base(unit):set_active(true, managers.player:player_unit())
	end

	return 
end
UnitNetworkHandler.sync_trip_mine_setup = function (self, unit, sensor_upgrade, peer_id)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.player:verify_equipment(peer_id, "trip_mine")
	unit.base(unit):sync_setup(sensor_upgrade)

	return 
end
UnitNetworkHandler.sync_trip_mine_explode = function (self, unit, user_unit, ray_from, ray_to, damage_size, damage, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(user_unit) then
		user_unit = nil
	end

	if alive(unit) then
		unit.base(unit):sync_trip_mine_explode(user_unit, ray_from, ray_to, damage_size, damage)
	end

	return 
end
UnitNetworkHandler.sync_trip_mine_explode_spawn_fire = function (self, unit, user_unit, ray_from, ray_to, damage_size, damage, added_time, range_multiplier, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(user_unit) then
		user_unit = nil
	end

	if alive(unit) then
		unit.base(unit):sync_trip_mine_explode_and_spawn_fire(user_unit, ray_from, ray_to, damage_size, damage, added_time, range_multiplier)
	end

	return 
end
UnitNetworkHandler.sync_trip_mine_explode_no_user = function (self, unit, ray_from, ray_to, damage_size, damage, sender)
	self.sync_trip_mine_explode(self, unit, nil, ray_from, ray_to, damage_size, damage, sender)

	return 
end
UnitNetworkHandler.sync_trip_mine_set_armed = function (self, unit, bool, length, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.base(unit):sync_trip_mine_set_armed(bool, length)

	return 
end
UnitNetworkHandler.request_place_ecm_jammer = function (self, pos, normal, battery_life_upgrade_lvl, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	local owner_unit = peer.unit(peer)

	if not alive(owner_unit) or owner_unit.id(owner_unit) == -1 then
		rpc.from_server_ecm_jammer_place_result(rpc, nil)

		return 
	end

	if not managers.player:verify_equipment(peer.id(peer), "ecm_jammer") then
		return 
	end

	local rot = Rotation(normal, math.UP)
	local peer = self._verify_sender(rpc)
	local unit = ECMJammerBase.spawn(pos, rot, battery_life_upgrade_lvl, owner_unit, peer.id(peer))

	unit.base(unit):set_server_information(peer.id(peer))
	unit.base(unit):set_active(true)
	rpc.from_server_ecm_jammer_place_result(rpc, unit)

	return 
end
UnitNetworkHandler.from_server_ecm_jammer_place_result = function (self, unit, rpc)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit = (alive(unit) and unit) or nil

	if alive(managers.player:player_unit()) then
		managers.player:player_unit():equipment():from_server_ecm_jammer_placement_result((unit and true) or false)
	end

	if not unit then
		return 
	end

	unit.base(unit):set_owner(managers.player:player_unit())

	return 
end
UnitNetworkHandler.from_server_ecm_jammer_rejected = function (self, rpc)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if alive(managers.player:player_unit()) then
		managers.player:player_unit():equipment():from_server_ecm_jammer_placement_result(false)
	end

	return 
end
UnitNetworkHandler.sync_unit_event_id_16 = function (self, unit, ext_name, event_id, rpc)
	local peer = self._verify_sender(rpc)

	if not peer or not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local extension = unit[ext_name](unit)

	if not extension then
		debug_pause("[UnitNetworkHandler:sync_unit_event_id_16] unit", unit, "does not have extension", ext_name)

		return 
	end

	extension.sync_net_event(extension, event_id, peer)

	return 
end
UnitNetworkHandler.m79grenade_explode_on_client = function (self, position, normal, user, damage, range, curve_pow, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(user, sender) then
		return 
	end

	ProjectileBase._explode_on_client(position, normal, user, damage, range, curve_pow)

	return 
end
UnitNetworkHandler.element_explode_on_client = function (self, position, normal, damage, range, curve_pow, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.explosion:client_damage_and_push(position, normal, nil, damage, range, curve_pow)

	return 
end
UnitNetworkHandler.picked_up_sentry_gun = function (self, unit, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	if alive(unit) then
		local sentry_type = unit.base(unit):get_type()
		local sentry_type_index = (sentry_type == "sentry_gun" and 1) or (sentry_type == "sentry_gun_silent" and 2)

		managers.network:session():send_to_peer(peer, "picked_up_sentry_gun_response", unit.id(unit), unit.weapon(unit):ammo_total(), unit.weapon(unit):ammo_max(), sentry_type_index)
		unit.base(unit):remove()
	end

	return 
end
UnitNetworkHandler.picked_up_sentry_gun_response = function (self, sentry_uid, ammo, max_ammo, sentry_type_index, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	local sentry_type_list = {
		"sentry_gun",
		"sentry_gun_silent"
	}
	local sentry_type = sentry_type_list[sentry_type_index]
	local ammo_ratio = ammo/max_ammo

	SentryGunBase.on_picked_up(sentry_type, ammo_ratio, sentry_uid)

	return 
end
UnitNetworkHandler.place_sentry_gun = function (self, pos, rot, equipment_selection_index, user_unit, unit_idstring_index, ammo_level, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	local unit, spread_level, rot_level = SentryGunBase.spawn(user_unit, pos, rot, peer.id(peer), true, unit_idstring_index)

	if unit then
		unit.base(unit):set_server_information(peer.id(peer))

		local has_shield = unit.base(unit):has_shield()

		managers.network:session():send_to_peers_synched("from_server_sentry_gun_place_result", peer.id(peer), equipment_selection_index or 0, unit, rot_level, spread_level, has_shield, ammo_level)
	end

	return 
end
UnitNetworkHandler.from_server_sentry_gun_place_result = function (self, owner_peer_id, equipment_selection_index, sentry_gun_unit, rot_level, spread_level, shield, ammo_level, rpc)
	local local_peer = managers.network:session():local_peer()

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(rpc) or not alive(sentry_gun_unit) or not managers.network:session():peer(owner_peer_id) then
		if alive(local_peer.unit(local_peer)) then
			local_peer.unit(local_peer):equipment():from_server_sentry_gun_place_result(sentry_gun_unit.id(sentry_gun_unit))
		end

		return 
	end

	sentry_gun_unit.base(sentry_gun_unit):set_owner_id(owner_peer_id)

	if owner_peer_id == local_peer.id(local_peer) and alive(local_peer.unit(local_peer)) then
		managers.player:from_server_equipment_place_result(equipment_selection_index, local_peer.unit(local_peer), sentry_gun_unit)
	end

	if shield then
		sentry_gun_unit.base(sentry_gun_unit):enable_shield()
	end

	local rot_speed_mul = SentryGunBase.ROTATION_SPEED_MUL[rot_level]

	sentry_gun_unit.movement(sentry_gun_unit):setup(rot_speed_mul)
	sentry_gun_unit.brain(sentry_gun_unit):setup(rot_speed_mul/1)

	local spread_mul = SentryGunBase.SPREAD_MUL[spread_level]
	local setup_data = {
		spread_mul = spread_mul,
		ignore_units = {
			sentry_gun_unit
		}
	}

	sentry_gun_unit.weapon(sentry_gun_unit):setup(setup_data)

	local ammo_mul = SentryGunBase.AMMO_MUL[ammo_level]

	sentry_gun_unit.weapon(sentry_gun_unit):setup_virtual_ammo(ammo_mul)
	sentry_gun_unit.event_listener(sentry_gun_unit):call("on_setup", sentry_gun_unit.base(sentry_gun_unit):is_owner())
	sentry_gun_unit.base(sentry_gun_unit):post_setup()

	return 
end
UnitNetworkHandler.sentrygun_ammo = function (self, unit, ammo_ratio, owner_id)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.weapon(unit):sync_ammo(ammo_ratio)

	return 
end
UnitNetworkHandler.sentrygun_sync_armor_piercing = function (self, unit, use_armor_piercing)
	unit.weapon(unit):set_fire_mode_net(use_armor_piercing)

	return 
end
UnitNetworkHandler.sync_fire_mode_interaction = function (self, unit, fire_mode_unit, owner_id)
	unit.weapon(unit):interaction_setup(fire_mode_unit, owner_id)

	return 
end
UnitNetworkHandler.sentrygun_health = function (self, unit, health_ratio)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.character_damage(unit):sync_health(health_ratio)

	return 
end
UnitNetworkHandler.turret_idle_state = function (self, unit, state)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.brain(unit):set_idle(state)

	return 
end
UnitNetworkHandler.turret_update_shield_smoke_level = function (self, unit, ratio, up)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.character_damage(unit):update_shield_smoke_level(ratio, up)

	return 
end
UnitNetworkHandler.turret_repair = function (self, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):repair()

	return 
end
UnitNetworkHandler.turret_complete_repairing = function (self, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.movement(unit):complete_repairing()

	return 
end
UnitNetworkHandler.turret_repair_shield = function (self, unit)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.character_damage(unit):repair_shield()

	return 
end
UnitNetworkHandler.sync_unit_module = function (self, parent_unit, module_unit, align_obj_name, module_id, parent_extension_name)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(parent_unit) or not alive(module_unit) then
		return 
	end

	parent_unit[parent_extension_name](parent_unit):spawn_module(module_unit, align_obj_name, module_id)

	return 
end
UnitNetworkHandler.run_unit_module_function = function (self, parent_unit, module_id, parent_extension_name, module_extension_name, func_name, params)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(parent_unit) then
		return 
	end

	local params_split = string.split(params, " ")

	parent_unit[parent_extension_name](parent_unit):run_module_function_unsafe(module_id, module_extension_name, func_name, params_split[1], params_split[2])

	return 
end
UnitNetworkHandler.sync_equipment_setup = function (self, unit, upgrade_lvl, peer_id)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.base(unit):sync_setup(upgrade_lvl, peer_id)

	return 
end
UnitNetworkHandler.sync_ammo_bag_setup = function (self, unit, upgrade_lvl, peer_id, bullet_storm_level)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.base(unit):sync_setup(upgrade_lvl, peer_id, bullet_storm_level)

	return 
end
UnitNetworkHandler.sync_ammo_bag_ammo_taken = function (self, unit, amount, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.base(unit):sync_ammo_taken(amount)

	return 
end
UnitNetworkHandler.sync_grenade_crate_grenade_taken = function (self, unit, amount, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.base(unit):sync_grenade_taken(amount)

	return 
end
UnitNetworkHandler.place_deployable_bag = function (self, class_name, pos, rot, upgrade_lvl, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	local class_name_to_deployable_id = tweak_data.equipments.class_name_to_deployable_id

	if not managers.player:verify_equipment(peer.id(peer), class_name_to_deployable_id[class_name]) then
		return 
	end

	local class = CoreSerialize.string_to_classtable(class_name)

	if class then
		local unit = class.spawn(pos, rot, upgrade_lvl, peer.id(peer))

		unit.base(unit):set_server_information(peer.id(peer))
	end

	return 
end
UnitNetworkHandler.place_ammo_bag = function (self, pos, rot, upgrade_lvl, bullet_storm_level, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	if not managers.player:verify_equipment(peer.id(peer), "ammo_bag") then
		return 
	end

	local unit = AmmoBagBase.spawn(pos, rot, upgrade_lvl, peer.id(peer), bullet_storm_level)

	if unit then
		unit.base(unit):set_server_information(peer.id(peer))
	end

	return 
end
UnitNetworkHandler.used_deployable = function (self, rpc)
	local peer = self._verify_sender(rpc)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	peer.set_used_deployable(peer, true)

	return 
end
UnitNetworkHandler.sync_doctor_bag_taken = function (self, unit, amount, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.base(unit):sync_taken(amount)

	return 
end
UnitNetworkHandler.sync_money_wrap_money_taken = function (self, unit, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.base(unit):sync_money_taken()

	return 
end
UnitNetworkHandler.sync_pickup = function (self, unit, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local pickup_extension = unit.base(unit)

	if not pickup_extension or not pickup_extension.sync_pickup then
		pickup_extension = unit.pickup(unit)
	end

	if pickup_extension and pickup_extension.sync_pickup then
		pickup_extension.sync_pickup(pickup_extension, self._verify_sender(sender))
	end

	return 
end
UnitNetworkHandler.unit_sound_play = function (self, unit, event_id, source, sender)
	if not alive(unit) or not self._verify_sender(sender) then
		return 
	end

	if source == "" then
		source = nil
	end

	unit.sound(unit):play(event_id, source, false)

	return 
end
UnitNetworkHandler.corpse_sound_play = function (self, unit_id, event_id, source)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local u_data = managers.enemy:get_corpse_unit_data_from_id(unit_id)

	if not u_data then
		return 
	end

	if not u_data.unit then
		debug_pause("[UnitNetworkHandler:corpse_sound_play] u_data without unit", inspect(u_data))

		return 
	end

	if not u_data.unit:sound() then
		return 
	end

	u_data.unit:sound():play(event_id, source, false)

	return 
end
UnitNetworkHandler.say = function (self, unit, event_id, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if unit.in_slot(unit, managers.slot:get_mask("all_criminals")) and not managers.groupai:state():is_enemy_converted_to_criminal(unit) then
		unit.sound(unit):say(event_id, nil, false)
	else
		unit.sound(unit):say(event_id, nil, true)
	end

	return 
end
UnitNetworkHandler.sync_remove_one_teamAI = function (self, name, replace_with_player)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_remove_one_teamAI(name, replace_with_player)

	return 
end
UnitNetworkHandler.sync_smoke_grenade = function (self, detonate_pos, shooter_pos, duration, flashbang)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_smoke_grenade(detonate_pos, shooter_pos, duration, flashbang)

	return 
end
UnitNetworkHandler.sync_smoke_grenade_kill = function (self)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_smoke_grenade_kill()

	return 
end
UnitNetworkHandler.sync_cs_grenade = function (self, detonate_pos, shooter_pos, duration)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_cs_grenade(detonate_pos, shooter_pos, duration)

	return 
end
UnitNetworkHandler.sync_cs_grenade_kill = function (self)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_cs_grenade_kill()

	return 
end
UnitNetworkHandler.sync_hostage_headcount = function (self, value)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_hostage_headcount(value)

	return 
end
UnitNetworkHandler.play_distance_interact_redirect = function (self, unit, redirect, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.movement(unit):play_redirect(redirect)

	return 
end
UnitNetworkHandler.play_distance_interact_redirect_delay = function (self, unit, redirect, delay, sender)
	local sender_peer = self._verify_sender(sender)

	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not sender_peer then
		return 
	end

	delay = delay - sender_peer.qos(sender_peer).ping/1000

	if unit.movement(unit).play_redirect_delayed then
		unit.movement(unit):play_redirect_delayed(redirect, nil, delay)
	else
		unit.movement(unit):play_redirect(redirect)
	end

	return 
end
UnitNetworkHandler.start_timer_gui = function (self, unit, timer, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.timer_gui(unit):sync_start(timer)

	return 
end
UnitNetworkHandler.give_equipment = function (self, equipment, amount, transfer, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.player:add_special({
		name = equipment,
		amount = amount,
		transfer = transfer
	})

	return 
end
UnitNetworkHandler.on_sole_criminal_respawned = function (self, peer_id, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.player:on_sole_criminal_respawned(peer_id)

	return 
end
UnitNetworkHandler.killzone_set_unit = function (self, type)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.killzone:set_unit(managers.player:player_unit(), type)

	return 
end
UnitNetworkHandler.dangerzone_set_level = function (self, level)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.player:player_unit():character_damage():set_danger_level(level)

	return 
end
UnitNetworkHandler.sync_player_movement_state = function (self, unit, state, down_time, unit_id_str)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	self._chk_unit_too_early(self, unit, unit_id_str, "sync_player_movement_state", 1, unit, state, down_time, unit_id_str)

	if not alive(unit) then
		return 
	end

	Application:trace("[UnitNetworkHandler:sync_player_movement_state]: ", unit.movement(unit):current_state_name(), "->", state)

	local local_peer = managers.network:session():local_peer()

	if local_peer.unit(local_peer) and unit.key(unit) == local_peer.unit(local_peer):key() then
		local valid_transitions = {
			standard = {
				arrested = true,
				incapacitated = true,
				carry = true,
				bleed_out = true,
				tased = true
			},
			carry = {
				arrested = true,
				incapacitated = true,
				standard = true,
				bleed_out = true,
				tased = true
			},
			mask_off = {
				arrested = true,
				carry = true,
				standard = true
			},
			bleed_out = {
				carry = true,
				fatal = true,
				standard = true
			},
			fatal = {
				carry = true,
				standard = true
			},
			arrested = {
				carry = true,
				standard = true
			},
			tased = {
				incapacitated = true,
				carry = true,
				standard = true
			},
			incapacitated = {
				carry = true,
				standard = true
			},
			clean = {
				arrested = true,
				carry = true,
				mask_off = true,
				standard = true,
				civilian = true
			},
			civilian = {
				arrested = true,
				carry = true,
				clean = true,
				standard = true,
				mask_off = true
			}
		}

		if unit.movement(unit):current_state_name() == state then
			return 
		end

		if unit.movement(unit):current_state_name() and valid_transitions[unit.movement(unit):current_state_name()][state] then
			managers.player:set_player_state(state)
		else
			debug_pause_unit(unit, "[UnitNetworkHandler:sync_player_movement_state] received invalid transition", unit, unit.movement(unit):current_state_name(), "->", state)
		end
	else
		unit.movement(unit):sync_movement_state(state, down_time)
	end

	return 
end
UnitNetworkHandler.sync_show_hint = function (self, id, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.hint:sync_show_hint(id)

	return 
end
UnitNetworkHandler.sync_show_action_message = function (self, unit, id, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.action_messaging:sync_show_message(id, unit)

	return 
end
UnitNetworkHandler.sync_waiting_for_player_start = function (self, variant, soundtrack)
	if not self._verify_gamestate(self._gamestate_filter.waiting_for_players) then
		return 
	end

	game_state_machine:current_state():sync_start(variant, soundtrack)

	return 
end
UnitNetworkHandler.sync_waiting_for_player_skip = function (self)
	if not self._verify_gamestate(self._gamestate_filter.waiting_for_players) then
		return 
	end

	game_state_machine:current_state():sync_skip()

	return 
end
UnitNetworkHandler.criminal_hurt = function (self, criminal_unit, attacker_unit, damage_ratio, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(criminal_unit, sender) then
		return 
	end

	if not alive(attacker_unit) or criminal_unit.key(criminal_unit) == attacker_unit.key(attacker_unit) then
		attacker_unit = nil
	end

	managers.hud:set_mugshot_damage_taken(criminal_unit.unit_data(criminal_unit).mugshot_id)
	managers.groupai:state():criminal_hurt_drama(criminal_unit, attacker_unit, damage_ratio*0.01)

	return 
end
UnitNetworkHandler.arrested = function (self, unit)
	if not alive(unit) then
		return 
	end

	unit.movement(unit):sync_arrested()

	return 
end
UnitNetworkHandler.suspect_uncovered = function (self, enemy_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local suspect_member = managers.network:session():local_peer()
	local suspect_unit = suspect_member and suspect_member.unit(suspect_member)

	if not suspect_unit then
		return 
	end

	suspect_unit.movement(suspect_unit):on_uncovered(enemy_unit)

	return 
end
UnitNetworkHandler.add_synced_team_upgrade = function (self, category, upgrade, level, sender)
	local sender_peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not sender_peer then
		return 
	end

	local peer_id = sender_peer.id(sender_peer)

	managers.player:add_synced_team_upgrade(peer_id, category, upgrade, level)

	return 
end
UnitNetworkHandler.sync_deployable_equipment = function (self, deployable, amount, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:set_synced_deployable_equipment(peer, deployable, amount)

	return 
end
UnitNetworkHandler.sync_cable_ties = function (self, amount, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:set_synced_cable_ties(peer.id(peer), amount)

	return 
end
UnitNetworkHandler.sync_grenades = function (self, grenade, amount, register_peer_id, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:set_synced_grenades(peer.id(peer), grenade, amount, register_peer_id)

	return 
end
UnitNetworkHandler.sync_grenades_cooldown = function (self, end_time, duration, sender)
	local peer = self._verify_sender(sender)

	if not peer or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local teammate_panel = managers.hud:get_teammate_panel_by_peer(peer)

	if teammate_panel then
		teammate_panel.set_grenade_cooldown(teammate_panel, {
			end_time = end_time,
			duration = duration
		})
	end

	return 
end
UnitNetworkHandler.sync_ammo_amount = function (self, selection_index, max_clip, current_clip, current_left, max, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:set_synced_ammo_info(peer.id(peer), selection_index, max_clip, current_clip, current_left, max)

	return 
end
UnitNetworkHandler.activate_temporary_team_upgrade = function (self, category, upgrade, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:activate_synced_temporary_team_upgrade(peer.id(peer), category, upgrade)

	return 
end
UnitNetworkHandler.sync_bipod = function (self, bipod_pos, body_pos, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:set_synced_bipod(peer, bipod_pos, body_pos)

	return 
end
UnitNetworkHandler.sync_carry = function (self, carry_id, multiplier, dye_initiated, has_dye_pack, dye_value_multiplier, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:set_synced_carry(peer, carry_id, multiplier, dye_initiated, has_dye_pack, dye_value_multiplier)

	return 
end
UnitNetworkHandler.sync_remove_carry = function (self, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:remove_synced_carry(peer)

	return 
end
UnitNetworkHandler.server_drop_carry = function (self, carry_id, carry_multiplier, dye_initiated, has_dye_pack, dye_value_multiplier, position, rotation, dir, throw_distance_multiplier_upgrade_level, zipline_unit, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.player:server_drop_carry(carry_id, carry_multiplier, dye_initiated, has_dye_pack, dye_value_multiplier, position, rotation, dir, throw_distance_multiplier_upgrade_level, zipline_unit, peer)

	return 
end
UnitNetworkHandler.sync_carry_data = function (self, unit, carry_id, carry_multiplier, dye_initiated, has_dye_pack, dye_value_multiplier, position, dir, throw_distance_multiplier_upgrade_level, zipline_unit, peer_id, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.player:verify_carry(managers.network:session():peer(peer_id), carry_id)
	managers.player:sync_carry_data(unit, carry_id, carry_multiplier, dye_initiated, has_dye_pack, dye_value_multiplier, position, dir, throw_distance_multiplier_upgrade_level, zipline_unit, peer_id)

	return 
end
UnitNetworkHandler.sync_cocaine_stacks = function (self, amount, in_use, upgrade_level, power_level, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	local peer_id = peer.id(peer)
	local current_cocaine_stacks = managers.player:get_synced_cocaine_stacks(peer_id)

	if current_cocaine_stacks then
		amount = math.min(((current_cocaine_stacks and current_cocaine_stacks.amount) or 0) + (tweak_data.upgrades.max_cocaine_stacks_per_tick or 20), amount)
	end

	managers.player:set_synced_cocaine_stacks(peer_id, amount, in_use, upgrade_level, power_level)

	return 
end
UnitNetworkHandler.request_throw_projectile = function (self, projectile_type, position, dir, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	local peer_id = peer.id(peer)
	local projectile_entry = tweak_data.blackmarket:get_projectile_name_from_index(projectile_type)
	local no_cheat_count = tweak_data.blackmarket.projectiles[projectile_entry].no_cheat_count

	if not no_cheat_count and not managers.player:verify_grenade(peer_id) then
		return 
	end

	ProjectileBase.throw_projectile(projectile_type, position, dir, peer_id)

	return 
end
UnitNetworkHandler.sync_throw_projectile = function (self, unit, pos, dir, projectile_type, peer_id, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		print("_verify failed!!!")

		return 
	end

	local projectile_entry = tweak_data.blackmarket:get_projectile_name_from_index(projectile_type)
	local tweak_entry = tweak_data.blackmarket.projectiles[projectile_entry]

	if tweak_entry.client_authoritative then
		if not unit then
			local unit_name = Idstring(tweak_entry.local_unit)
			unit = World:spawn_unit(unit_name, pos, Rotation(dir, math.UP))
		end

		unit.base(unit):set_owner_peer_id(peer_id)
	end

	if not alive(unit) then
		print("unit is not alive!!!")

		return 
	end

	local server_peer = managers.network:session():server_peer()

	if tweak_entry.throwable and not peer == server_peer then
		print("projectile is throwable, should not be thrown by client!!!")

		return 
	end

	local no_cheat_count = tweak_entry.no_cheat_count

	if not no_cheat_count then
		managers.player:verify_grenade(peer_id)
	end

	local member = managers.network:session():peer(peer_id)
	local thrower_unit = member and member.unit(member)

	if alive(thrower_unit) then
		unit.base(unit):set_thrower_unit(thrower_unit)

		if not tweak_entry.throwable and thrower_unit.movement(thrower_unit) and thrower_unit.movement(thrower_unit):current_state() then
			unit.base(unit):set_weapon_unit(thrower_unit.movement(thrower_unit):current_state()._equipped_unit)
		end
	end

	unit.base(unit):sync_throw_projectile(dir, projectile_type)

	return 
end
UnitNetworkHandler.sync_attach_projectile = function (self, unit, instant_dynamic_pickup, parent_unit, parent_body, parent_object, local_pos, dir, projectile_type, peer_id, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		print("_verify failed!!!")

		return 
	end

	local world_position = (parent_object and local_pos.rotate_with(local_pos, parent_object.rotation(parent_object)) + parent_object.position(parent_object)) or local_pos

	if Network:is_server() then
		local projectile_entry = tweak_data.blackmarket:get_projectile_name_from_index(projectile_type)
		local tweak_entry = tweak_data.blackmarket.projectiles[projectile_entry]
		local unit_name = Idstring(tweak_entry.unit)
		local synced_unit = World:spawn_unit(unit_name, world_position, Rotation(dir, math.UP))

		managers.network:session():send_to_peers_synched("sync_attach_projectile", synced_unit, instant_dynamic_pickup, (alive(parent_unit) and parent_unit.id(parent_unit) ~= -1 and parent_unit) or nil, (alive(parent_unit) and parent_unit.id(parent_unit) ~= -1 and parent_body) or nil, (alive(parent_unit) and parent_unit.id(parent_unit) ~= -1 and parent_object) or nil, local_pos, dir, projectile_type, peer_id)
		synced_unit.base(synced_unit):set_thrower_unit_by_peer_id(peer_id)
		synced_unit.base(synced_unit):sync_attach_to_unit(instant_dynamic_pickup, parent_unit, parent_body, parent_object, local_pos, dir)
	elseif unit then
		unit.set_position(unit, world_position)
		unit.base(unit):set_thrower_unit_by_peer_id(peer_id)
		unit.base(unit):sync_attach_to_unit(instant_dynamic_pickup, parent_unit, parent_body, parent_object, local_pos, dir)
	end

	if peer_id ~= 1 then
		local dummy_unit = ArrowBase.find_nearest_arrow(peer_id, world_position)

		if dummy_unit then
			dummy_unit.set_slot(dummy_unit, 0)
		end
	end

	return 
end
UnitNetworkHandler.sync_detonate_incendiary_grenade = function (self, unit, ext_name, event_id, normal, rpc)
	local peer = self._verify_sender(rpc)

	if not peer or not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local extension = unit[ext_name](unit)

	if not extension then
		debug_pause("[UnitNetworkHandler:sync_detonate_incendiary_grenade] unit", unit, "does not have extension", ext_name)

		return 
	end

	extension.sync_detonate_incendiary_grenade(extension, event_id, normal, peer)

	return 
end
UnitNetworkHandler.sync_detonate_molotov_grenade = function (self, unit, ext_name, event_id, normal, rpc)
	local peer = self._verify_sender(rpc)

	if not peer or not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local extension = unit[ext_name](unit)

	if not extension then
		debug_pause("[UnitNetworkHandler:sync_detonate_molotov_grenade] unit", unit, "does not have extension", ext_name)

		return 
	end

	extension.sync_detonate_molotov_grenade(extension, event_id, normal, peer)

	return 
end
UnitNetworkHandler.sync_add_doted_enemy = function (self, enemy_unit, fire_damage_received_time, weapon_unit, dot_length, dot_damage, user_unit, is_molotov, rpc)
	local peer = self._verify_sender(rpc)

	managers.fire:sync_add_fire_dot(enemy_unit, fire_damage_received_time, weapon_unit, dot_length, dot_damage, user_unit, peer, is_molotov)

	return 
end
UnitNetworkHandler.server_secure_loot = function (self, carry_id, multiplier_level, peer_id, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.loot:server_secure_loot(carry_id, multiplier_level, nil, peer_id)

	return 
end
UnitNetworkHandler.sync_secure_loot = function (self, carry_id, multiplier_level, silent, peer_id, sender)
	if (not self._verify_gamestate(self._gamestate_filter.any_ingame) and not self._verify_gamestate(self._gamestate_filter.any_end_game)) or not self._verify_sender(sender) then
		return 
	end

	managers.loot:sync_secure_loot(carry_id, multiplier_level, silent, peer_id)

	return 
end
UnitNetworkHandler.sync_small_loot_taken = function (self, unit, multiplier_level, sender)
	if not alive(unit) and self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	unit.base(unit):taken(multiplier_level, peer.id(peer))

	return 
end
UnitNetworkHandler.server_unlock_asset = function (self, asset_id, is_show_chat_message, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.assets:server_unlock_asset(asset_id, is_show_chat_message, peer)

	return 
end
UnitNetworkHandler.sync_unlock_asset = function (self, asset_id, is_show_chat_message, unlocker_peer_id, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	local peer = managers.network:session():peer(unlocker_peer_id)

	managers.assets:sync_unlock_asset(asset_id, is_show_chat_message, peer)

	return 
end
UnitNetworkHandler.sync_heist_time = function (self, time, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.game_play_central:sync_heist_time(time)

	return 
end
UnitNetworkHandler.run_mission_door_sequence = function (self, unit, sequence_name, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.base(unit):run_sequence_simple(sequence_name)

	return 
end
UnitNetworkHandler.set_mission_door_device_powered = function (self, unit, powered, interaction_enabled, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	MissionDoor.set_mission_door_device_powered(unit, powered, interaction_enabled)

	return 
end
UnitNetworkHandler.run_mission_door_device_sequence = function (self, unit, sequence_name, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	MissionDoor.run_mission_door_device_sequence(unit, sequence_name)

	return 
end
UnitNetworkHandler.server_place_mission_door_device = function (self, unit, player, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	local result = unit.interaction(unit):server_place_mission_door_device(player, sender)

	return 
end
UnitNetworkHandler.result_place_mission_door_device = function (self, unit, result, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	unit.interaction(unit):result_place_mission_door_device(result)

	return 
end
UnitNetworkHandler.set_armor = function (self, unit, percent, max_mul, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	local peer = self._verify_sender(sender)
	local peer_id = peer.id(peer)
	local character_data = managers.criminals:character_data_by_peer_id(peer_id)

	if character_data and character_data.panel_id then
		managers.hud:set_teammate_armor(character_data.panel_id, {
			current = percent*max_mul,
			total = max_mul*100,
			max = max_mul*100
		})
	else
		managers.hud:set_mugshot_armor(unit.unit_data(unit).mugshot_id, percent/100)
	end

	return 
end
UnitNetworkHandler.set_health = function (self, unit, percent, max_mul, sender)
	if not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	local peer = self._verify_sender(sender)
	local peer_id = peer.id(peer)
	local character_data = managers.criminals:character_data_by_peer_id(peer_id)

	if character_data and character_data.panel_id then
		managers.hud:set_teammate_health(character_data.panel_id, {
			current = percent*max_mul,
			total = max_mul*100,
			max = max_mul*100
		})
	else
		managers.hud:set_mugshot_health(unit.unit_data(unit).mugshot_id, percent/100)
	end

	if percent ~= 100 then
		managers.mission:call_global_event("player_damaged")
	end

	return 
end
UnitNetworkHandler.sync_equipment_possession = function (self, peer_id, equipment, amount, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.player:set_synced_equipment_possession(peer_id, equipment, amount)

	return 
end
UnitNetworkHandler.sync_remove_equipment_possession = function (self, peer_id, equipment, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	local equipment_peer = managers.network:session():peer(peer_id)

	if not equipment_peer then
		print("[UnitNetworkHandler:sync_remove_equipment_possession] unknown peer", peer_id)

		return 
	end

	managers.player:remove_equipment_possession(peer_id, equipment)

	return 
end
UnitNetworkHandler.sync_start_anticipation = function (self)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.hud:sync_start_anticipation()

	return 
end
UnitNetworkHandler.sync_start_anticipation_music = function (self)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.hud:sync_start_anticipation_music()

	return 
end
UnitNetworkHandler.sync_start_assault = function (self, assault_number)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.hud:sync_start_assault(assault_number)

	return 
end
UnitNetworkHandler.sync_end_assault = function (self, result)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.hud:sync_end_assault(result)

	return 
end
UnitNetworkHandler.sync_assault_dialog = function (self, index)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.hud:sync_assault_dialog(index)

	return 
end
UnitNetworkHandler.sync_contour_state = function (self, unit, u_id, type, state, multiplier, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	local contour_unit = nil

	if alive(unit) and unit.id(unit) ~= -1 then
		contour_unit = unit
	else
		local unit_data = managers.enemy:get_corpse_unit_data_from_id(u_id)

		if unit_data then
			contour_unit = unit_data.unit
		end
	end

	if not contour_unit then
		Application:error("[UnitNetworkHandler:sync_contour_state] Unit is missing")

		return 
	end

	if state then
		contour_unit.contour(contour_unit):add(ContourExt.indexed_types[type], false, multiplier)
	else
		contour_unit.contour(contour_unit):remove(ContourExt.indexed_types[type], nil)
	end

	return 
end
UnitNetworkHandler.mark_minion = function (self, unit, minion_owner_peer_id, convert_enemies_health_multiplier_level, passive_convert_enemies_health_multiplier_level, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	local health_multiplier = 1

	if 0 < convert_enemies_health_multiplier_level then
		health_multiplier = health_multiplier*tweak_data.upgrades.values.player.convert_enemies_health_multiplier[convert_enemies_health_multiplier_level]
	end

	if 0 < passive_convert_enemies_health_multiplier_level then
		health_multiplier = health_multiplier*tweak_data.upgrades.values.player.passive_convert_enemies_health_multiplier[passive_convert_enemies_health_multiplier_level]
	end

	unit.character_damage(unit):convert_to_criminal(health_multiplier)
	unit.contour(unit):add("friendly", false)
	managers.groupai:state():sync_converted_enemy(unit)

	if minion_owner_peer_id == managers.network:session():local_peer():id() then
		managers.player:count_up_player_minions()
	end

	return 
end
UnitNetworkHandler.remove_minion = function (self, unit, sender)
	if not self._verify_sender(sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if unit then
		local u_key = unit.key(unit)

		managers.groupai:state():_set_converted_police(u_key, nil)
	end

	return 
end
UnitNetworkHandler.spot_enemy = function (self, unit)
	return 
end
UnitNetworkHandler.count_down_player_minions = function (self)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.player:count_down_player_minions()

	return 
end
UnitNetworkHandler.sync_teammate_helped_hint = function (self, hint, helped_unit, helping_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(helped_unit, sender) or not self._verify_character(helping_unit, sender) then
		return 
	end

	managers.trade:sync_teammate_helped_hint(helped_unit, helping_unit, hint)

	return 
end
UnitNetworkHandler.sync_assault_mode = function (self, enabled)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_assault_mode(enabled)

	return 
end
UnitNetworkHandler.sync_hostage_killed_warning = function (self, warning)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_hostage_killed_warning(warning)

	return 
end
UnitNetworkHandler.set_interaction_voice = function (self, unit, voice, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	unit.brain(unit):set_interaction_voice((voice ~= "" and voice) or nil)

	return 
end
UnitNetworkHandler.sync_teammate_comment = function (self, message, pos, pos_based, radius, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.groupai:state():sync_teammate_comment(message, pos, pos_based, radius)

	return 
end
UnitNetworkHandler.sync_teammate_comment_instigator = function (self, unit, message)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.groupai:state():sync_teammate_comment_instigator(unit, message)

	return 
end
UnitNetworkHandler.begin_gameover_fadeout = function (self)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():begin_gameover_fadeout()

	return 
end
UnitNetworkHandler.send_statistics = function (self, total_kills, total_specials_kills, total_head_shots, accuracy, downs, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_end_game) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	managers.network:session():on_statistics_recieved(peer.id(peer), total_kills, total_specials_kills, total_head_shots, accuracy, downs)

	return 
end
UnitNetworkHandler.sync_statistics_result = function (self, ...)
	if game_state_machine:current_state().on_statistics_result then
		game_state_machine:current_state():on_statistics_result(...)
	end

	return 
end
UnitNetworkHandler.statistics_tied = function (self, name, sender)
	if not self._verify_sender(sender) then
		return 
	end

	managers.statistics:tied({
		name = name
	})

	return 
end
UnitNetworkHandler.bain_comment = function (self, bain_line, sender)
	if not self._verify_sender(sender) then
		return 
	end

	if managers.dialog and managers.groupai and managers.groupai:state():bain_state() then
		managers.dialog:queue_dialog(bain_line, {})
	end

	return 
end
UnitNetworkHandler.is_inside_point_of_no_return = function (self, is_inside, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	managers.groupai:state():set_is_inside_point_of_no_return(peer.id(peer), is_inside)

	return 
end
UnitNetworkHandler.mission_ended = function (self, win, num_is_inside, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if managers.platform:presence() == "Playing" then
		if win then
			game_state_machine:change_state_by_name("victoryscreen", {
				num_winners = num_is_inside,
				personal_win = not managers.groupai:state()._failed_point_of_no_return and alive(managers.player:player_unit())
			})
		else
			game_state_machine:change_state_by_name("gameoverscreen")
		end
	end

	return 
end
UnitNetworkHandler.sync_level_up = function (self, level, sender)
	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	peer.set_level(peer, level)

	return 
end
UnitNetworkHandler.sync_disable_shout = function (self, unit, state, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	ElementDisableShout.sync_function(unit, state)

	return 
end
UnitNetworkHandler.sync_run_sequence_char = function (self, unit, seq, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	if unit and alive(unit) and unit.damage(unit) then
		unit.damage(unit):run_sequence_simple(seq)
	end

	return 
end
UnitNetworkHandler.sync_player_kill_statistic = function (self, tweak_table_name, is_headshot, weapon_unit, variant, stats_name, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) or not alive(weapon_unit) then
		return 
	end

	local data = {
		name = tweak_table_name,
		stats_name = stats_name,
		head_shot = is_headshot,
		weapon_unit = weapon_unit,
		variant = variant
	}

	managers.statistics:killed_by_anyone(data)

	local attacker_state = managers.player:current_state()
	data.attacker_state = attacker_state

	managers.statistics:killed(data)

	return 
end
UnitNetworkHandler.set_attention_enabled = function (self, unit, setting_index, state, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character_and_sender(unit, sender) then
		return 
	end

	if unit.in_slot(unit, managers.slot:get_mask("players")) and unit.base(unit).is_husk_player then
		local setting_name = tweak_data.attention:get_attention_name(setting_index)

		unit.movement(unit):sync_attention_setting(setting_name, state, false)
	else
		debug_pause_unit(unit, "[UnitNetworkHandler:set_attention_enabled] invalid unit", unit)
	end

	return 
end
UnitNetworkHandler.link_attention_no_rot = function (self, parent_unit, attention_object, parent_object, local_pos, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not alive(parent_unit) or not alive(attention_object) then
		return 
	end

	attention_object.attention(attention_object):link(parent_unit, parent_object, local_pos)

	return 
end
UnitNetworkHandler.unlink_attention = function (self, attention_object, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not alive(attention_object) then
		return 
	end

	attention_object.attention(attention_object):link(nil)

	return 
end
UnitNetworkHandler.suspicion = function (self, suspect_peer_id, susp_value, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local suspect_member = managers.network:session():peer(suspect_peer_id)

	if not suspect_member then
		return 
	end

	local suspect_unit = suspect_member.unit(suspect_member)

	if not suspect_unit then
		return 
	end

	if susp_value == 0 then
		susp_value = false
	elseif susp_value == 255 then
		susp_value = true
	else
		susp_value = susp_value/254
	end

	suspect_unit.movement(suspect_unit):on_suspicion(nil, susp_value)

	return 
end
UnitNetworkHandler.suspicion_hud = function (self, observer_unit, status)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not alive(observer_unit) then
		return 
	end

	if status == 0 then
		status = false
	elseif status == 1 then
		status = 1
	elseif status == 2 then
		status = true
	elseif status == 3 then
		status = "calling"
	elseif status == 4 then
		status = "called"
	elseif status == 5 then
		status = "call_interrupted"
	end

	managers.groupai:state():on_criminal_suspicion_progress(nil, observer_unit, status)

	return 
end
UnitNetworkHandler.group_ai_event = function (self, event_id, blame_id, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():sync_event(event_id, blame_id)

	return 
end
UnitNetworkHandler.start_timespeed_effect = function (self, effect_id, timer_name, affect_timer_names_str, speed, fade_in, sustain, fade_out, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	local affect_timer_names = nil

	if affect_timer_names_str ~= "" then
		affect_timer_names = string.split(affect_timer_names_str, ";")
	end

	local effect_desc = {
		timer = timer_name,
		affect_timer = affect_timer_names,
		speed = speed,
		fade_in = fade_in,
		sustain = sustain,
		fade_out = fade_out
	}

	managers.time_speed:play_effect(effect_id, effect_desc)

	return 
end
UnitNetworkHandler.stop_timespeed_effect = function (self, effect_id, fade_out_duration, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.time_speed:stop_effect(effect_id, fade_out_duration)

	return 
end
UnitNetworkHandler.sync_upgrade = function (self, upgrade_category, upgrade_name, upgrade_level, sender)
	local peer = self._verify_sender(sender)

	if not peer then
		print("[UnitNetworkHandler:sync_upgrade] missing peer", upgrade_category, upgrade_name, upgrade_level, sender.ip_at_index(sender, 0))

		return 
	end

	local unit = peer.unit(peer)

	if not unit then
		print("[UnitNetworkHandler:sync_upgrade] missing unit", upgrade_category, upgrade_name, upgrade_level, sender.ip_at_index(sender, 0))

		return 
	end

	unit.base(unit):set_upgrade_value(upgrade_category, upgrade_name, upgrade_level)

	return 
end
UnitNetworkHandler.sync_temporary_upgrade_owned = function (self, upgrade_category, upgrade_name, upgrade_level, index, sender)
	local peer = self._verify_sender(sender)

	if not peer then
		print("[UnitNetworkHandler:sync_temporary_upgrade_owned] missing peer", upgrade_category, upgrade_name, upgrade_level, index, sender.ip_at_index(sender, 0))

		return 
	end

	local unit = peer.unit(peer)

	if not unit then
		print("[UnitNetworkHandler:sync_temporary_upgrade_owned] missing unit", upgrade_category, upgrade_name, upgrade_level, index, sender.ip_at_index(sender, 0))

		return 
	end

	unit.base(unit):set_temporary_upgrade_owned(upgrade_category, upgrade_name, upgrade_level, index)

	return 
end
UnitNetworkHandler.sync_temporary_upgrade_activated = function (self, upgrade_index, sender)
	local peer = self._verify_sender(sender)

	if not peer then
		print("[UnitNetworkHandler:sync_temporary_upgrade_activated] missing peer", upgrade_index, time, sender.ip_at_index(sender, 0))

		return 
	end

	local unit = peer.unit(peer)

	if not unit then
		print("[UnitNetworkHandler:sync_temporary_upgrade_activated] missing unit", upgrade_index, time, sender.ip_at_index(sender, 0))

		return 
	end

	unit.base(unit):activate_temporary_upgrade(upgrade_index)

	return 
end
UnitNetworkHandler.suppression = function (self, unit, ratio, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	local sup_tweak = unit.base(unit):char_tweak().suppression

	if not sup_tweak then
		debug_pause_unit(unit, "[UnitNetworkHandler:suppression] husk missing suppression settings", unit)

		return 
	end

	local amount_max = sup_tweak.brown_point or sup_tweak.react_point[2]
	local amount, panic_chance = nil

	if ratio == 16 then
		amount = "max"
		panic_chance = -1
	else
		amount = (ratio == 15 and "max") or (0 < amount_max and (amount_max*ratio)/15) or "max"
	end

	unit.character_damage(unit):build_suppression(amount, panic_chance)

	return 
end
UnitNetworkHandler.suppressed_state = function (self, unit, state, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_character(unit) then
		return 
	end

	unit.movement(unit):on_suppressed(state)

	return 
end
UnitNetworkHandler.camera_yaw_pitch = function (self, cam_unit, yaw_255, pitch_255)
	if not alive(cam_unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local yaw = (yaw_255*360)/255 - 180
	local pitch = (pitch_255*180)/255 - 90

	cam_unit.base(cam_unit):apply_rotations(yaw, pitch)

	return 
end
UnitNetworkHandler.loot_link = function (self, loot_unit, parent_unit, sender)
	if not alive(loot_unit) or not alive(parent_unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if loot_unit == parent_unit then
		loot_unit.carry_data(loot_unit):unlink()
	else
		loot_unit.carry_data(loot_unit):link_to(parent_unit)
	end

	return 
end
UnitNetworkHandler.remove_unit = function (self, unit, sender)
	if not alive(unit) then
		return 
	end

	if unit.id(unit) ~= -1 then
		Network:detach_unit(unit)
	end

	unit.set_slot(unit, 0)

	return 
end
UnitNetworkHandler.sync_gui_net_event = function (self, unit, event_id, value, sender)
	if not alive(unit) or not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.digital_gui(unit):sync_gui_net_event(event_id, value)

	return 
end
UnitNetworkHandler.sync_proximity_activation = function (self, unit, proximity_name, range_data_string, sender)
	if not alive(unit) or not alive(unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	unit.damage(unit):sync_proximity_activation(proximity_name, range_data_string)

	return 
end
UnitNetworkHandler.sync_inflict_body_damage = function (self, body, unit, normal, position, direction, damage, velocity, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(body) then
		return 
	end

	if not body.extension(body) then
		print("[UnitNetworkHandler:sync_inflict_body_damage] body has no extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage then
		print("[UnitNetworkHandler:sync_inflict_body_damage] body has no damage extension", body.name(body), body.unit(body):name())

		return 
	end

	if not body.extension(body).damage.damage_fire then
		print("[UnitNetworkHandler:sync_inflict_body_damage] body has no damage damage_bullet function", body.name(body), body.unit(body):name())

		return 
	end

	body.extension(body).damage:damage_fire(unit, normal, position, direction, damage, velocity)

	return 
end
UnitNetworkHandler.sync_team_relation = function (self, team_index_1, team_index_2, relation_code)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local team_id_1 = tweak_data.levels:get_team_names_indexed()[team_index_1]
	local team_id_2 = tweak_data.levels:get_team_names_indexed()[team_index_2]
	local relation = (relation_code == 1 and "neutral") or (relation_code == 2 and "friend") or "foe"

	if not team_id_1 or not team_id_2 or relation_code < 1 or 3 < relation_code then
		debug_pause("[UnitNetworkHandler:sync_team_relation] invalid params", team_index_1, team_index_2, relation_code, Global.level_data.level_id)

		return 
	end

	managers.groupai:state():set_team_relation(team_id_1, team_id_2, relation, nil)

	return 
end
UnitNetworkHandler.sync_char_team = function (self, unit, team_index, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not self._verify_character(unit) then
		return 
	end

	local team_id = tweak_data.levels:get_team_names_indexed()[team_index]
	local team_data = managers.groupai:state():team_data(team_id)

	unit.movement(unit):set_team(team_data)

	return 
end
UnitNetworkHandler.sync_drill_upgrades = function (self, unit, autorepair_level_1, autorepair_level_2, drill_speed_level, silent, reduced_alert)
	unit.base(unit):set_skill_upgrades(Drill.create_upgrades(autorepair_level_1, autorepair_level_2, drill_speed_level, silent, reduced_alert))

	return 
end
UnitNetworkHandler.sync_vehicle_driving = function (self, action, unit, player)
	Application:debug("[DRIVING_NET] sync_vehicle_driving " .. action)

	if not alive(unit) then
		return 
	end

	local ext = unit.npc_vehicle_driving(unit)
	ext = ext or unit.vehicle_driving(unit)

	if action == "start" then
		ext.sync_start(ext, player)
	elseif action == "stop" then
		ext.sync_stop(ext)
	end

	return 
end
UnitNetworkHandler.sync_vehicle_set_input = function (self, unit, accelerate, steer, brake, handbrake, gear_up, gear_down, forced_gear)
	if not alive(unit) then
		return 
	end

	unit.vehicle_driving(unit):sync_set_input(accelerate, steer, brake, handbrake, gear_up, gear_down, forced_gear)

	return 
end
UnitNetworkHandler.sync_vehicle_state = function (self, unit, position, rotation, velocity)
	if not alive(unit) then
		return 
	end

	unit.vehicle_driving(unit):sync_state(position, rotation, velocity)

	return 
end
UnitNetworkHandler.sync_enter_vehicle_host = function (self, vehicle, seat_name, peer_id, player)
	Application:debug("[DRIVING_NET] sync_enter_vehicle_host", seat_name)

	if not alive(vehicle) then
		return 
	end

	managers.player:server_enter_vehicle(vehicle, peer_id, player, seat_name)

	return 
end
UnitNetworkHandler.sync_vehicle_player = function (self, action, vehicle, peer_id, player, seat_name)
	Application:debug("[DRIVING_NET] sync_vehicle_player " .. action)

	if action == "enter" then
		managers.player:sync_enter_vehicle(vehicle, peer_id, player, seat_name)
	elseif action == "exit" then
		managers.player:sync_exit_vehicle(peer_id, player)
	end

	return 
end
UnitNetworkHandler.sync_vehicle_data = function (self, vehicle, state_name, occupant_driver, occupant_left, occupant_back_left, occupant_back_right, is_trunk_open)
	Application:debug("[DRIVING_NET] sync_vehicles_data")

	if not alive(vehicle) then
		return 
	end

	managers.vehicle:sync_vehicle_data(vehicle, state_name, occupant_driver, occupant_left, occupant_back_left, occupant_back_right, is_trunk_open)

	return 
end
UnitNetworkHandler.sync_npc_vehicle_data = function (self, vehicle, state_name, target_unit)
	Application:debug("[DRIVING_NET] sync_npc_vehicle_data", vehicle, state_name)

	if not alive(vehicle) then
		return 
	end

	managers.vehicle:sync_npc_vehicle_data(vehicle, state_name, target_unit)

	return 
end
UnitNetworkHandler.sync_vehicle_loot = function (self, vehicle, carry_id1, multiplier1, carry_id2, multiplier2, carry_id3, multiplier3)
	Application:debug("[DRIVING_NET] sync_vehicle_loot")

	if not alive(vehicle) then
		return 
	end

	managers.vehicle:sync_vehicle_loot(vehicle, carry_id1, multiplier1, carry_id2, multiplier2, carry_id3, multiplier3)

	return 
end
UnitNetworkHandler.sync_ai_vehicle_action = function (self, action, vehicle, data, unit)
	Application:debug("[DRIVING_NET] sync_ai_vehicle_action: ", action, data)

	if not alive(vehicle) then
		return 
	end

	if action == "health" then
		vehicle.character_damage(vehicle):sync_vehicle_health(data)
	elseif action == "revive" then
		vehicle.character_damage(vehicle):sync_vehicle_revive(data)
	elseif action == "state" then
		vehicle.vehicle_driving(vehicle):sync_vehicle_state(data)
	else
		if not alive(unit) then
			return 
		end

		vehicle.vehicle_driving(vehicle):sync_ai_vehicle_action(action, data, unit)
	end

	return 
end
UnitNetworkHandler.server_store_loot_in_vehicle = function (self, vehicle, loot_bag)
	Application:debug("[DRIVING_NET] server_store_loot_in_vehicle")

	if not alive(vehicle) or not alive(loot_bag) then
		return 
	end

	vehicle.vehicle_driving(vehicle):server_store_loot_in_vehicle(loot_bag)

	return 
end
UnitNetworkHandler.sync_vehicle_change_stance = function (self, shooting_unit, stance)
	Application:debug("[DRIVING_NET] sync_vehicle_change_stance")

	if not alive(shooting_unit) then
		return 
	end

	shooting_unit.movement(shooting_unit):sync_vehicle_change_stance(stance)

	return 
end
UnitNetworkHandler.sync_store_loot_in_vehicle = function (self, vehicle, loot_bag, carry_id, multiplier)
	Application:debug("[DRIVING_NET] sync_store_loot_in_vehicle")

	if not alive(vehicle) or not alive(loot_bag) then
		return 
	end

	vehicle.vehicle_driving(vehicle):sync_store_loot_in_vehicle(loot_bag, carry_id, multiplier)

	return 
end
UnitNetworkHandler.server_give_vehicle_loot_to_player = function (self, vehicle, peer_id)
	Application:debug("[DRIVING_NET] server_give_vehicle_loot_to_player")
	vehicle.vehicle_driving(vehicle):server_give_vehicle_loot_to_player(peer_id)

	return 
end
UnitNetworkHandler.sync_give_vehicle_loot_to_player = function (self, vehicle, carry_id, multiplier, peer_id)
	Application:debug("[DRIVING_NET] sync_give_vehicle_loot_to_player")
	vehicle.vehicle_driving(vehicle):sync_give_vehicle_loot_to_player(carry_id, multiplier, peer_id)

	return 
end
UnitNetworkHandler.sync_vehicle_interact_trunk = function (self, vehicle, peer_id)
	Application:debug("[DRIVING_NET] sync_vehicle_interact_trunk")
	vehicle.vehicle_driving(vehicle):_interact_trunk(vehicle)

	return 
end
UnitNetworkHandler.sync_damage_reduction_buff = function (self, damage_reduction)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not damage_reduction then
		debug_pause("[UnitNetworkHandler:sync_damage_reduction_buff] invalid params", damage_reduction)

		return 
	end

	managers.groupai:state():set_phalanx_damage_reduction_buff(damage_reduction)

	return 
end
UnitNetworkHandler.sync_assault_endless = function (self, enabled)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	managers.groupai:state():set_assault_endless(enabled)

	return 
end
UnitNetworkHandler.action_jump = function (self, unit, pos, jump_vec, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(unit) then
		return 
	end

	unit.movement(unit):sync_action_jump(pos, jump_vec)

	return 
end
UnitNetworkHandler.action_jump_middle = function (self, unit, pos, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(unit) then
		return 
	end

	unit.movement(unit):sync_action_jump_middle(pos)

	return 
end
UnitNetworkHandler.action_land = function (self, unit, pos, sender)
	if not self._verify_character_and_sender(unit, sender) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(unit) then
		return 
	end

	unit.movement(unit):sync_action_land(pos)

	return 
end
UnitNetworkHandler.sync_player_swansong = function (self, unit, active, sender)
	local peer = self._verify_sender(sender)

	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not peer then
		return 
	end

	if alive(unit) and unit.character_damage(unit) then
		unit.character_damage(unit).swansong = active

		managers.network:session():send_to_peers_except(peer.id(peer), "sync_swansong_hud", unit, peer.id(peer))
		self.sync_swansong_hud(self, unit, peer.id(peer))
	end

	return 
end
UnitNetworkHandler.special_eq_response = function (self, unit, sender)
	if unit.interaction(unit).apply_item_pickup then
		unit.interaction(unit):apply_item_pickup()
	end

	return 
end
UnitNetworkHandler.sync_swansong_hud = function (self, unit, peer_id)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local panel_id = nil

	for i, panel in ipairs(managers.hud._teammate_panels) do
		if panel._peer_id == peer_id then
			panel_id = i

			break
		end
	end

	if panel_id then
		managers.hud:set_teammate_condition(panel_id, "mugshot_swansong", managers.localization:text("debug_mugshot_downed"))
	else
		Application:error(string.format("Panel not found for peer: %d", peer_id))
	end

	return 
end
UnitNetworkHandler.sync_swansong_timer = function (self, unit, current, total, revives, peer_id)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local panel_id = nil

	for i, panel in ipairs(managers.hud._teammate_panels) do
		if panel._peer_id == peer_id then
			panel_id = i

			break
		end
	end

	if panel_id then
		managers.hud:set_teammate_custom_radial(panel_id, {
			current = current,
			total = total,
			revives = revives
		})
	else
		Application:error(string.format("Panel not found for peer: %d", peer_id))
	end

	return 
end
UnitNetworkHandler.sync_fall_position = function (self, unit, pos, rot)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if alive(unit) then
		unit.movement(unit):sync_fall_position(pos, rot)
	end

	return 
end
UnitNetworkHandler.sync_spawn_extra_ammo = function (self, unit)
	managers.player:spawn_extra_ammo(unit)

	return 
end
UnitNetworkHandler.sync_stored_pos = function (self, unit, sync, pos, rot)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if alive(unit) then
		unit.base(unit):sync_stored_pos(sync, pos, rot)
	end

	return 
end
UnitNetworkHandler.sync_team_ai_stopped = function (self, unit, stopped)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if alive(unit) then
		unit.movement(unit):set_should_stay(stopped)
	end

	return 
end
UnitNetworkHandler.sync_damage_achievements = function (self, unit, weapon_unit, attacker_unit, distance, damage, head_shot, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if alive(unit) and unit.character_damage(unit) then
		unit.character_damage(unit):client_check_damage_achievements(weapon_unit, attacker_unit, distance, damage, head_shot)
	end

	return 
end
UnitNetworkHandler.sync_medic_heal = function (self, unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if alive(unit) and unit.movement(unit) then
		local action_data = {
			body_part = 3,
			type = "heal",
			client_interrupt = Network:is_client()
		}

		unit.movement(unit):action_request(action_data)
	end

	return 
end
UnitNetworkHandler.sync_explosion_to_client = function (self, unit, position, normal, damage, range, curve_pow, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	managers.explosion:give_local_player_dmg(position, range, damage)
	managers.explosion:explode_on_client(position, normal, unit, damage, range, curve_pow)

	return 
end
UnitNetworkHandler.sync_friendly_fire_damage = function (self, peer_id, unit, damage, variant, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if managers.network:session():local_peer():id() == peer_id then
		local player_unit = managers.player:player_unit()

		if alive(player_unit) and alive(unit) then
			local attack_info = {
				ignore_suppression = true,
				range = 1000,
				attacker_unit = unit,
				damage = damage,
				variant = variant,
				col_ray = {
					position = unit.position(unit)
				},
				push_vel = Vector3()
			}

			if variant == "bullet" or variant == "projectile" then
				player_unit.character_damage(player_unit):damage_bullet(attack_info)
			elseif variant == "melee" then
				player_unit.character_damage(player_unit):damage_melee(attack_info)
			elseif variant == "fire" then
				player_unit.character_damage(player_unit):damage_fire(attack_info)
			end
		end
	end

	managers.job:set_memory("trophy_flawless", true, false)

	return 
end
UnitNetworkHandler.sync_flashbang_event = function (self, unit, event_id, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if alive(unit) then
		unit.base(unit):on_network_event(event_id)
	end

	return 
end
UnitNetworkHandler.sync_ability_hud = function (self, end_time, time_total, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	local panel_id = nil

	for i, panel in ipairs(managers.hud._teammate_panels) do
		if panel._peer_id == peer.id(peer) then
			panel_id = i

			break
		end
	end

	if panel_id then
		local current_time = managers.game_play_central:get_heist_timer()

		managers.hud:activate_teammate_ability_radial(panel_id, end_time - current_time, time_total)
	else
		Application:error("HUD panel not found from peer id!")
	end

	return 
end
UnitNetworkHandler.sync_ability_hud_cooldown = function (self, ability_id, cooldown, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	local panel_id = nil

	for i, panel in ipairs(managers.hud._teammate_panels) do
		if panel._peer_id == peer.id(peer) then
			panel_id = i

			break
		end
	end

	if panel_id then
		managers.hud:set_teammate_grenade_cooldown(panel_id, {
			cooldown = cooldown
		})
	else
		Application:error("HUD panel not found from peer id!")
	end

	return 
end
UnitNetworkHandler.sync_underbarrel_switch = function (self, selection_index, underbarrel_id, is_on, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	local unit = managers.criminals:character_unit_by_peer_id(peer.id(peer))

	if not unit then
		return 
	end

	unit.inventory(unit):set_weapon_underbarrel(selection_index, underbarrel_id, is_on)

	return 
end
UnitNetworkHandler.sync_ai_throw_bag = function (self, unit, carry_unit, target_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if alive(unit) and alive(carry_unit) then
		unit.movement(unit):sync_throw_bag(carry_unit, target_unit)
	end

	return 
end
UnitNetworkHandler.request_carried_bag_unit = function (self, ai_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local peer = self._verify_sender(sender)

	if not peer then
		return 
	end

	if alive(ai_unit) then
		local carry_unit = ai_unit.movement(ai_unit)._carry_unit

		if carry_unit then
			managers.network:session():send_to_peer(peer, "sync_carried_bag_unit", ai_unit, carry_unit)
		end
	end

	return 
end
UnitNetworkHandler.sync_carried_bag_unit = function (self, ai_unit, carry_unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if alive(ai_unit) and alive(carry_unit) then
		ai_unit.movement(ai_unit):set_carrying_bag(carry_unit)
		carry_unit.carry_data(carry_unit):link_to(ai_unit)
	end

	return 
end
UnitNetworkHandler.sync_unit_spawn = function (self, parent_unit, spawn_unit, align_obj_name, unit_id, parent_extension_name)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(parent_unit) or not alive(spawn_unit) then
		return 
	end

	parent_unit[parent_extension_name](parent_unit):spawn_unit(unit_id, align_obj_name, spawn_unit)

	return 
end
UnitNetworkHandler.sync_unit_surrendered = function (self, unit, surrendered)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(unit) or not unit.brain(unit) or not unit.brain(unit).sync_surrender then
		return 
	end

	unit.brain(unit):sync_surrender(surrendered)

	return 
end
UnitNetworkHandler.sync_link_spawned_unit = function (self, parent_unit, unit_id, joint_table, parent_extension_name)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(parent_unit) then
		return 
	end

	parent_unit[parent_extension_name](parent_unit):_link_joints(unit_id, joint_table)

	return 
end
UnitNetworkHandler.run_spawn_unit_sequence = function (self, parent_unit, parent_extension_name, unit_id, sequence_name)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(parent_unit) then
		return 
	end

	parent_unit[parent_extension_name](parent_unit):_spawn_run_sequence(unit_id, sequence_name)

	return 
end
UnitNetworkHandler.run_local_push_child_unit = function (self, parent_unit, parent_extension_name, unit_id, mass, pow, vec3_a, vec3_b)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(parent_unit) then
		return 
	end

	parent_unit[parent_extension_name](parent_unit):local_push_child_unit(unit_id, mass, pow, vec3_a, vec3_b)

	return 
end
UnitNetworkHandler.sync_special_character_material = function (self, character_unit, material_name)
	if not alive(character_unit) then
		return 
	end

	local mtr_ids = Idstring(material_name)

	if DB:has(Idstring("material_config"), mtr_ids) then
		character_unit.set_material_config(character_unit, mtr_ids, true)

		if character_unit.armor_skin(character_unit) then
			character_unit.armor_skin(character_unit):_apply_cosmetics()
		end
	end

	return 
end
UnitNetworkHandler.sync_enemy_buff = function (self, enemy_unit, buff_category, buff_total, sender)
	if not alive(enemy_unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	enemy_unit.base(enemy_unit):_sync_buff_total(buff_category, buff_total)

	return 
end
UnitNetworkHandler.sync_tear_gas_grenade_properties = function (self, grenade, radius, damage, duration)
	grenade.base(grenade):set_properties({
		radius = radius,
		damage = damage*0.1
	})

	return 
end
UnitNetworkHandler.sync_tear_gas_grenade_detonate = function (self, grenade)
	grenade.base(grenade):detonate()

	return 
end
UnitNetworkHandler.sync_spawn_smoke_screen = function (self, unit, dodge_bonus)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	if not alive(unit) then
		return 
	end

	managers.player:_sync_activate_smoke_screen(unit, dodge_bonus)

	return 
end
UnitNetworkHandler.sync_melee_start = function (self, unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(unit) then
		return 
	end

	unit.movement(unit):sync_melee_start()

	return 
end
UnitNetworkHandler.sync_melee_discharge = function (self, unit, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(unit) then
		return 
	end

	unit.movement(unit):sync_melee_discharge()

	return 
end
UnitNetworkHandler.sync_interaction_anim = function (self, unit, is_start, tweak_data, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(unit) then
		return 
	end

	if is_start then
		unit.movement(unit):sync_interaction_anim_start(tweak_data)
	else
		unit.movement(unit):sync_interaction_anim_end()
	end

	return 
end
UnitNetworkHandler.sync_shotgun_push = function (self, unit, hit_pos, dir, distance, attacker, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(unit) then
		return 
	end

	managers.game_play_central:_do_shotgun_push(unit, hit_pos, dir, distance, attacker, sender)

	return 
end
UnitNetworkHandler.sync_carry_set_position_and_throw = function (self, unit, destination, direction, force, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) or not self._verify_sender(sender) then
		return 
	end

	if not alive(unit) then
		return 
	end

	unit.carry_data(unit):set_position_and_throw(destination, direction, force)

	return 
end
UnitNetworkHandler.sync_delayed_damage_hud = function (self, delayed_damage, sender)
	local peer = self._verify_sender(sender)

	if not peer or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local teammate_panel = managers.hud:get_teammate_panel_by_peer(peer)

	if teammate_panel then
		teammate_panel.set_delayed_damage(teammate_panel, delayed_damage)
	end

	return 
end
UnitNetworkHandler.sync_damage_absorption_hud = function (self, absorption_amount, sender)
	local peer = self._verify_sender(sender)

	if not peer or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return 
	end

	local teammate_panel = managers.hud:get_teammate_panel_by_peer(peer)

	if teammate_panel then
		teammate_panel.set_absorb_active(teammate_panel, absorption_amount)
	end

	return 
end

return 
