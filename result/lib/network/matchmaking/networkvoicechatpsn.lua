NetworkVoiceChatPSN = NetworkVoiceChatPSN or class()
NetworkVoiceChatPSN.init = function (self)
	self._started = false
	self._room_id = nil
	self._team = 1
	self._restart_session = nil
	self._peers = {}

	self._load_globals(self)

	self._muted_players = {}

	return 
end
NetworkVoiceChatPSN.check_status_information = function (self)
	return 
end
NetworkVoiceChatPSN.open = function (self)
	return 
end
NetworkVoiceChatPSN.voice_type = function (self)
	return "voice_psn"
end
NetworkVoiceChatPSN.pause = function (self)
	return 
end
NetworkVoiceChatPSN.resume = function (self)
	return 
end
NetworkVoiceChatPSN.set_volume = function (self, volume)
	PSNVoice:set_volume(volume)

	return 
end
NetworkVoiceChatPSN.init_voice = function (self)
	if self._started == false and not self._starting then
		self._starting = true

		PSNVoice:assign_callback(function (...)
			self:_callback(...)

			return 
		end)
		PSNVoice:init(4, 4, 50, 8000)
		self.set_volume(self, managers.user:get_setting("voice_volume"))

		self._started = true
	end

	return 
end
NetworkVoiceChatPSN.destroy_voice = function (self, disconnected)
	if self._started == true then
		self._started = false

		if self._room_id and not disconnected then
			self.close_session(self)
		end

		PSNVoice:destroy()

		self._closing = nil
		self._room_id = nil
		self._restart_session = nil
		self._team = 1
	end

	return 
end
NetworkVoiceChatPSN.num_peers = function (self)
	local l = PSNVoice:get_players_info()

	if l then
		local x = 0

		for k, v in pairs(l) do
			if v.joined == 1 then
				x = x + 1
			end
		end

		return #l <= x
	end

	return true
end
NetworkVoiceChatPSN.open_session = function (self, roomid)
	if self._room_id and self._room_id == roomid then
		print("Voice: same_room")

		return 
	end

	if self._restart_session and self._restart_session == roomid then
		print("Voice: restart")

		return 
	end

	if self._closing or self._joining then
		print("Voice: closing|joining")

		self._restart_session = roomid

		return 
	end

	if self._started == false then
		self._restart_session = roomid

		self.init_voice(self)
	end

	if self._room_id then
		print("Voice: restart room")

		self._restart_session = roomid

		self.close_session(self)

		return 
	end

	self._room_id = roomid
	self._joining = true

	PSNVoice:start_session(roomid)

	return 
end
NetworkVoiceChatPSN.close_session = function (self)
	if self._joining then
		self._close = true

		return 
	end

	if self._room_id and not self._closing then
		self._closing = true

		if not PSNVoice:stop_session() then
			self._closing = nil
			self._room_id = nil
			self._delay_frame = TimerManager:wall():time() + 1
		end
	elseif not self._closing then
		self._restart_session = nil
		self._delay_frame = nil
	end

	return 
end
NetworkVoiceChatPSN.open_channel_to = function (self, player_info, context)
	print("NetworkVoiceChatPSN:open_channel_to")

	return 
end
NetworkVoiceChatPSN.close_channel_to = function (self, player_info)
	print("NetworkVoiceChatPSN:close_channel_to")
	PSNVoice:stop_sending_to(player_info._name)

	return 
end
NetworkVoiceChatPSN.lost_peer = function (self, peer)
	return 
end
NetworkVoiceChatPSN.close_all = function (self)
	if self._room_id then
		self.close_session(self)
	end

	self._room_id = nil
	self._closing = nil

	return 
end
NetworkVoiceChatPSN.set_team = function (self, team)
	if self._room_id then
		PSN:change_team(self._room_id, PSN:get_local_userid(), team)
		PSNVoice:set_team_target(team)
	end

	self._team = team

	return 
end
NetworkVoiceChatPSN.clear_team = function (self)
	if self._room_id and PSN:get_local_userid() then
		PSN:change_team(self._room_id, PSN:get_local_userid(), 1)
		PSNVoice:set_team_target(1)

		self._team = 1
	end

	return 
end
NetworkVoiceChatPSN.set_drop_in = function (self, data)
	self._drop_in = data

	return 
end
NetworkVoiceChatPSN._load_globals = function (self)
	if Global.psn and Global.psn.voice then
		self._started = Global.psn.voice.started
	end

	if PSN:is_online() and Global.psn and Global.psn.voice then
		PSNVoice:assign_callback(function (...)
			return 
		end)

		self._room_id = Global.psn.voice.room
		self._team = Global.psn.voice.team

		if Global.psn.voice.drop_in then
			self.open_session(self, Global.psn.voice.drop_in.room_id)
		end

		if Global.psn.voice.restart then
			self._restart_session = restart
			self._delay_frame = TimerManager:wall():time() + 2
		else
			PSNVoice:assign_callback(function (...)
				self:_callback(...)

				return 
			end)

			if self._room_id then
				self.set_team(self, self._team)
			end
		end

		Global.psn.voice = nil
	end

	return 
end
NetworkVoiceChatPSN._save_globals = function (self, disable_voice)
	if disable_voice == nil then
		return 
	end

	if not Global.psn then
		Global.psn = {}
	end

	local function f(...)
		return 
	end

	PSNVoice:assign_callback(f)

	Global.psn.voice = {
		started = self._started,
		drop_in = self._drop_in
	}

	if type(disable_voice) == "boolean" then
		if disable_voice == true then
			Global.psn.voice.room = self._room_id
			Global.psn.voice.team = self._team
		else
			Global.psn.voice.team = 1
		end
	else
		self.close_all(self)

		Global.psn.voice.restart = disable_voice
		Global.psn.voice.team = 1
	end

	return 
end
NetworkVoiceChatPSN._callback = function (self, info)
	if info and PSN:get_local_userid() then
		if info.load_succeeded ~= nil then
			self._starting = nil

			if info.load_succeeded then
				self._started = true
				self._delay_frame = TimerManager:wall():time() + 1
			end

			return 
		end

		if info.join_succeeded ~= nil then
			self._joining = nil

			if info.join_succeeded == false then
				self._room_id = nil
			else
				self.set_team(self, self._team)
			end

			if self._restart_session then
				self._delay_frame = TimerManager:wall():time() + 1
			end

			if self._close then
				self._close = nil

				self.close_session(self)
			end
		end

		if info.leave_succeeded ~= nil then
			self._closing = nil
			self._room_id = nil

			if self._restart_session then
				self._delay_frame = TimerManager:wall():time() + 1
			end
		end

		if info.unload_succeeded ~= nil then
			local function f(...)
				return 
			end

			PSNVoice:assign_callback(f)
		end
	end

	return 
end
NetworkVoiceChatPSN.update = function (self)
	if self._delay_frame and self._delay_frame < TimerManager:wall():time() then
		self._delay_frame = nil

		if self._restart_session then
			PSNVoice:assign_callback(function (...)
				self:_callback(...)

				return 
			end)

			local r = self._restart_session
			self._restart_session = nil

			self.open_session(self, r)
		end
	end

	return 
end
NetworkVoiceChatPSN.psn_session_destroyed = function (self, roomid)
	if self._room_id and self._room_id == roomid then
		self._room_id = nil
		self._closing = nil
	end

	return 
end
NetworkVoiceChatPSN._get_peer_user_id = function (self, peer)
	if not self._room_id then
		return 
	end

	local members = PSN:get_info_session(self._room_id).memberlist
	local name = peer.name(peer)

	for i, member in ipairs(members) do
		if tostring(member.user_id) == name then
			return member.user_id
		end
	end

	return 
end
NetworkVoiceChatPSN.mute_player = function (self, mute, peer)
	self._muted_players[peer.name(peer)] = mute

	PSNVoice:mute_player(mute, peer.name(peer))

	return 
end
NetworkVoiceChatPSN.is_muted = function (self, peer)
	return self._muted_players[peer.name(peer)] or false
end

return 
