NetworkFriendsPSN = NetworkFriendsPSN or class()
NetworkFriendsPSN.init = function (self)
	cat_print("lobby", "friends = NetworkFriendsPSN")

	self._friends = {}
	self._callback = {}
	self._updated_list_friends = PSN:update_list_friends()
	self._last_info = {
		friends = 0,
		friends_map = {},
		friends_status_map = {}
	}

	PSN:set_matchmaking_callback("friends_updated", function ()
		managers.network.friends:psn_update_friends()

		return 
	end)
	PSN:update_async_friends(true, 20)

	return 
end
NetworkFriendsPSN.destroy = function (self)
	PSN:set_matchmaking_callback("friends_updated", function ()
		return 
	end)
	PSN:update_async_friends(false, 20)

	return 
end
NetworkFriendsPSN.set_visible = function (self, set)
	if set == true then
		PSN:update_async_friends(true, 5)
	else
		PSN:update_async_friends(true, 20)
	end

	return 
end
NetworkFriendsPSN.call_callback = function (self, func, ...)
	if self._callback[func] then
		self._callback[func](...)
	else
		Application:error("Callback", func, "is not registred.")
	end

	return 
end
NetworkFriendsPSN.call_silent_callback = function (self, func, ...)
	if self._callback[func] then
		self._callback[func](...)
	end

	return 
end
NetworkFriendsPSN.get_friends_list = function (self)
	return self._friends

	local npids = {}
	local friends = PSN:get_list_friends()

	for _, f in pairs(friends) do
		table.insert(npids, f)
	end

	return npids
end
NetworkFriendsPSN.get_names_friends_list = function (self)
	if not self._updated_list_friends then
		self._updated_list_friends = PSN:update_list_friends()
	end

	local names = {}
	local friends = PSN:get_list_friends()

	if not friends then
		return names
	end

	for _, f in pairs(friends) do
		if f.friend ~= PSN:get_local_userid() then
			names[tostring(f.friend)] = true
		end
	end

	return names
end
NetworkFriendsPSN.get_npid_friends_list = function (self)
	local npids = {}
	local friends = PSN:get_list_friends()

	if not friends then
		return npids
	end

	for _, f in pairs(friends) do
		if f.friend ~= PSN:get_local_userid() then
			table.insert(npids, f.friend)
		end
	end

	return npids
end
NetworkFriendsPSN.get_friends = function (self)
	cat_print("lobby", "NetworkFriendsPSN:get_friends()")

	if not self._updated_list_friends then
		self._updated_list_friends = PSN:update_list_friends()
	end

	self._friends = {}
	local name = managers.network.account:player_id()
	local friends = PSN:get_list_friends()

	if friends then
		self._last_info.friends = #friends

		self._fill_li_friends_map(self, friends)

		self._last_info.friends_status_map = {}

		for k, v in pairs(friends) do
			if tostring(v.friend) ~= name then
				local online_status = "not_signed_in"
				local info_mod = 1
				self._last_info.friends_status_map[tostring(v.friend)] = v.status*info_mod

				if managers.network.matchmake:user_in_lobby(v.friend) then
					online_status = "in_group"
				elseif managers.network:session() and managers.network:session():is_kicked(tostring(v.friend)) then
					online_status = "banned"
				elseif managers.network.group:find(v.friend) then
					online_status = "in_group"
				elseif v.status ~= 0 or false then
					if v.status == 1 then
						online_status = "signed_in"
					elseif v.status == 2 then
						online_status = "signed_in"
					end
				end

				local f = NetworkFriend:new(v.friend, tostring(v.friend), online_status)

				table.insert(self._friends, f)
				self.call_callback(self, "status_change", f)
			end
		end

		self.call_callback(self, "get_friends_done", self._friends)
	end

	return 
end
NetworkFriendsPSN.register_callback = function (self, event, callback)
	self._callback[event] = callback

	return 
end
NetworkFriendsPSN.send_friend_request = function (self, nickname)
	return 
end
NetworkFriendsPSN.remove_friend = function (self, id)
	return 
end
NetworkFriendsPSN.has_builtin_screen = function (self)
	return false
end
NetworkFriendsPSN.accept_friend_request = function (self, player_id)
	return 
end
NetworkFriendsPSN.ignore_friend_request = function (self, player_id)
	return 
end
NetworkFriendsPSN.num_pending_friend_requests = function (self)
	return 0
end
NetworkFriendsPSN.debug_update = function (self, t, dt)
	return 
end
NetworkFriendsPSN.psn_disconnected = function (self)
	self._updated_list_friends = false

	return 
end
NetworkFriendsPSN.psn_update_friends = function (self)
	local friends = PSN:get_list_friends() or {}

	if 0 <= #friends then
		local change_of_friends = false

		for k, v in pairs(friends) do
			local friend_in_list = self._last_info.friends_map[tostring(v.friend)]

			if not friend_in_list then
				change_of_friends = true

				break
			end

			self._last_info.friends_map[tostring(v.friend)] = nil
		end

		for k, v in pairs(self._last_info.friends_map) do
			change_of_friends = true

			break
		end

		self._fill_li_friends_map(self, friends)

		if change_of_friends then
			self._last_info.friends = #friends
			self._updated_list_friends = PSN:update_list_friends()

			self.call_silent_callback(self, "friends_reset")

			return 
		elseif self._count_online(self, friends) then
			self.call_silent_callback(self, "friends_reset")

			return 
		end
	end

	return 
end
NetworkFriendsPSN.is_friend = function (self, id)
	local friends = PSN:get_list_friends()

	if not friends then
		return false
	end

	for _, data in ipairs(friends) do
		if data.friend == id then
			return true
		end
	end

	return false
end
NetworkFriendsPSN._fill_li_friends_map = function (self, friends)
	self._last_info.friends_map = {}

	for k, v in pairs(friends) do
		self._last_info.friends_map[tostring(v.friend)] = true
	end

	return 
end
NetworkFriendsPSN._fill_li_friends_status_map = function (self, friends)
	self._last_info.friends_status_map = {}

	for k, v in pairs(friends) do
		local info_mod = 1

		if v.status == 2 and v.info and v.info == managers.platform:presence() then
			info_mod = -1
		end

		self._last_info.friends_status_map[tostring(v.friend)] = v.status*info_mod
	end

	return 
end
NetworkFriendsPSN._count_online = function (self, friends)
	local name = managers.network.account:player_id()
	local status_changed = false

	for k, v in pairs(friends) do
		local friend_status = self._last_info.friends_status_map[tostring(v.friend)] or 42
		local info_mod = 1

		if tostring(v.friend) ~= name and friend_status ~= v.status*info_mod then
			status_changed = true

			break
		end
	end

	if not status_changed then
		return false
	end

	self._fill_li_friends_status_map(self, friends)

	return true
end

return 
