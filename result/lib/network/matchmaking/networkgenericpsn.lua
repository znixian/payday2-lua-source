NetworkGenericPSN = NetworkGenericPSN or class()
NetworkGenericPSN.init = function (self)
	cat_print("lobby", "generic = NetworkGenericPSN")

	local function f()
		return 
	end

	PSN:set_matchmaking_callback("room_invitation", f)

	local function psn_left(...)
		self:psn_member_left(...)

		return 
	end

	PSN:set_matchmaking_callback("member_left", psn_left)

	local function psn_join(...)
		self:psn_member_joined(...)

		return 
	end

	PSN:set_matchmaking_callback("member_joined", psn_join)

	local function psn_destroyed(...)
		self:psn_session_destroyed(...)

		return 
	end

	PSN:set_matchmaking_callback("session_destroyed", psn_destroyed)

	return 
end
NetworkGenericPSN.update = function (self, time)
	managers.network.voice_chat:update()

	return 
end
NetworkGenericPSN.start_game = function (self)
	Global.rendezvous = {
		rendevous = managers.network.shared_rdv:rendezvousonline(),
		is_online = managers.network.shared_rdv:is_online()
	}

	managers.network.voice_chat:_save_globals(true)
	managers.network.group:_save_global()
	managers.network.matchmake:_save_global()

	return 
end
NetworkGenericPSN.end_game = function (self)
	Global.rendezvous = {
		rendevous = managers.network.shared_rdv:rendezvousonline(),
		is_online = managers.network.shared_rdv:is_online()
	}

	managers.network.generic:set_entermenu(true)
	managers.network.voice_chat:_save_globals(managers.network.group:room_id() or false)
	managers.network.group:_save_global()

	return 
end
NetworkGenericPSN.psn_member_joined = function (self, info)
	managers.network.matchmake:psn_member_joined(info)

	return 
end
NetworkGenericPSN.psn_member_left = function (self, info)
	managers.network.matchmake:psn_member_left(info)

	return 
end
NetworkGenericPSN.psn_session_destroyed = function (self, info)
	cat_print("lobby", "NetworkGenericPSN:_session_destroyed_cb")
	cat_print_inspect("lobby", info)
	managers.network.voice_chat:psn_session_destroyed(info.room_id)
	managers.network.matchmake:_session_destroyed_cb(info.room_id)
	managers.network.group:_session_destroyed_cb(info.room_id)

	return 
end

return 
