NetworkFriendsXBL = NetworkFriendsXBL or class()
NetworkFriendsXBL.init = function (self)
	self._callback = {}

	return 
end
NetworkFriendsXBL.destroy = function (self)
	return 
end
NetworkFriendsXBL.set_visible = function (self, set)
	return 
end
NetworkFriendsXBL.get_friends_list = function (self)
	local player_index = managers.user:get_platform_id()

	if not player_index then
		Application:error("Player map not ready yet.")

		player_index = 0
	end

	local friend_list = XboxLive:friends(player_index)
	local friends = {}

	for i, friend in ipairs(friend_list) do
		table.insert(friends, NetworkFriend:new(friend.xuid, friend.gamertag))
	end

	return friends
end
NetworkFriendsXBL.get_friends_by_name = function (self)
	local player_index = managers.user:get_platform_id()
	local friend_list = XboxLive:friends(player_index)
	local friends = {}

	for i, friend in ipairs(friend_list) do
		friends[friend.gamertag] = friend
	end

	return friends
end
NetworkFriendsXBL.get_friends = function (self)
	if not self._initialized then
		self._initialized = true

		self._callback.initialization_done()
	end

	return 
end
NetworkFriendsXBL.register_callback = function (self, event, callback)
	self._callback[event] = callback

	return 
end
NetworkFriendsXBL.send_friend_request = function (self, nickname)
	return 
end
NetworkFriendsXBL.remove_friend = function (self, id)
	return 
end
NetworkFriendsXBL.has_builtin_screen = function (self)
	return true
end
NetworkFriendsXBL.accept_friend_request = function (self, player_id)
	return 
end
NetworkFriendsXBL.ignore_friend_request = function (self, player_id)
	return 
end
NetworkFriendsXBL.num_pending_friend_requests = function (self)
	return 0
end

return 
