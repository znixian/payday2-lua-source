require("lib/network/matchmaking/NetworkGroupLobby")

NetworkGroupLobbyPSN = NetworkGroupLobbyPSN or class(NetworkGroupLobby)
NetworkGroupLobbyPSN.init = function (self)
	NetworkGroupLobby.init(self)
	cat_print("lobby", "group = NetworkGroupLobbyPSN")

	self.OPEN_SLOTS = 4
	self._players = {}
	self._returned_players = {}
	self._room_id = nil
	self._inlobby = false
	self._join_enable = true
	self._is_server_var = false
	self._is_client_var = false
	self._callback_map = {}

	local function f(...)
		self:_custom_message_cb(...)

		return 
	end

	PSN:set_matchmaking_callback("custom_message", f)

	self._time_to_leave = nil

	self._load_globals(self)

	return 
end
NetworkGroupLobbyPSN._session_destroyed_cb = function (self, room_id)
	cat_print("lobby", "NetworkGroupLobbyPSN:_session_destroyed_cb")

	if room_id == self._room_id then
		self.leave_group_lobby_cb(self)
	end

	return 
end
NetworkGroupLobbyPSN.destroy = function (self)
	return 
end
NetworkGroupLobbyPSN.update = function (self, time)
	if self._time_to_leave and self._time_to_leave < TimerManager:wall():time() then
		self._time_to_leave = nil

		self.leave_group_lobby_cb(self)
	end

	if self._try_time and self._try_time < TimerManager:wall():time() then
		self._try_time = nil

		self.leave_group_lobby_cb(self, "join_failed")
	end

	return 
end
NetworkGroupLobbyPSN.create_group_lobby = function (self)
	cat_print("lobby", "NetworkGroupLobbyPSN:create_group_lobby()")

	self._players = {}
	local world_list = PSN:get_world_list()

	local function session_created(roomid)
		managers.network.group:_created_group_lobby(roomid)

		return 
	end

	PSN:set_matchmaking_callback("session_created", session_created)
	PSN:create_session(0, world_list[1].world_id, 0, self.OPEN_SLOTS, 0)

	return 
end
NetworkGroupLobbyPSN.join_group_lobby = function (self, room_info)
	self._is_server(self, false)
	self._is_client(self, true)

	if Global.psn_invite_id then
		Global.psn_invite_id = Global.psn_invite_id + 1

		if 990 < Global.psn_invite_id then
			Global.psn_invite_id = 1
		end
	end

	self._room_id = room_info.room_id

	local function f(...)
		self:_join_invite(...)

		return 
	end

	PSN:set_matchmaking_callback("connection_etablished", f)

	self._try_time = TimerManager:wall():time() + 30

	PSN:join_session(self._room_id)

	return 
end
NetworkGroupLobbyPSN.send_go_to_lobby = function (self)
	if self._is_server(self) then
		for k, v in pairs(self._players) do
			if v.rpc then
				v.rpc:grp_go_to_lobby()
			end
		end
	end

	return 
end
NetworkGroupLobbyPSN.go_to_lobby = function (self)
	if self._callback_map.go_to_lobby then
		self._call_callback(self, "go_to_lobby")
	else
		self.leave_group_lobby(self)
	end

	return 
end
NetworkGroupLobbyPSN.send_return_group_lobby = function (self)
	local playerid = managers.network.account:player_id()

	cat_print("lobby", "Now telling server that im back and ready. My playerid is: ", tostring(playerid))

	local timeout = 40

	if Application:bundled() then
		timeout = 15
	end

	self._server_rpc:lobby_return(managers.network.account:player_id())

	for k, v in pairs(self._players) do
		if v.is_server then
			managers.network.generic:ping_watch(self._server_rpc, false, callback(self, self, "_server_timed_out"), v.pnid, timeout)

			return 
		end
	end

	return 
end
NetworkGroupLobbyPSN._handle_returned_players = function (self)
	if #self._returned_players ~= 0 and self._callback_map.player_returned then
		cat_print("lobby", "We now have a return callback so now handling players")

		for index, playerid in pairs(self._returned_players) do
			local v, k = nil
			k, v = self.find(self, playerid)

			if k then
				local res = self._call_callback(self, "player_returned", v)

				if res == true then
					v.rpc:lobby_return_answer("yes")
					managers.network.generic:ping_watch(v.rpc, false, callback(self, self, "_client_timed_out"), v.pnid)
				else
					v.rpc:lobby_return_answer("no")
				end
			end
		end

		self._returned_players = {}
	end

	return 
end
NetworkGroupLobbyPSN.return_group_lobby = function (self, playerid, sender)
	cat_print("lobby", "Client reports that it has returned to group lobby. ", tostring(playerid))
	table.insert(self._returned_players, playerid)

	if self._callback_map.player_returned then
		self._handle_returned_players(self)
	else
		cat_print("lobby", "No player_returned callback so save these returns for later")
	end

	return 
end
NetworkGroupLobbyPSN.lobby_return_answer = function (self, answer, sender)
	cat_print("lobby", "Group leader tell us lobby_return_answer. ", tostring(answer), tostring(self._server_rpc))

	if answer == "yes" then
		for k, v in pairs(self._players) do
			if v.is_server then
				managers.network.generic:ping_watch(sender, false, callback(self, self, "_server_timed_out"), v.pnid)

				return 
			end
		end
	else
		self.leave_group_lobby(self)
	end

	return 
end
NetworkGroupLobbyPSN.find = function (self, playerid)
	for k, v in pairs(self._players) do
		if tostring(v.playerid) == tostring(playerid) then
			return k, v
		end
	end

	return nil, nil
end
NetworkGroupLobbyPSN.leave_group_lobby = function (self, instant)
	if self._is_server(self) and #self._players == 0 then
		self.leave_group_lobby_cb(self)

		return nil
	end

	self._try_time = nil

	if not instant then
		if self._is_server(self) then
			for k, v in pairs(self._players) do
				managers.network.generic:ping_remove(v.rpc, false)
				v.rpc:psn_grp_unregister_player(managers.network.account:player_id(), true)
			end
		elseif self._server_rpc then
			self._server_rpc:psn_grp_unregister_player(managers.network.account:player_id(), false)
			managers.network.generic:ping_remove(self._server_rpc)
		end

		self._time_to_leave = TimerManager:wall():time() + 2
	else
		self.leave_group_lobby_cb(self)
	end

	return 
end
NetworkGroupLobbyPSN.leave_group_lobby_cb = function (self, error_callback)
	if self._room_id then
		managers.network.voice_chat:close_session()

		if self._is_server(self) then
			PSN:destroy_session(self._room_id)
		else
			PSN:leave_session(self._room_id)
		end
	end

	self._room_id = nil
	self._inlobby = false
	self._is_server_var = false
	self._is_client_var = false
	self._players = {}

	if self._server_rpc then
		managers.network.generic:ping_remove(self._server_rpc, false)

		self._server_rpc = nil
	end

	self._call_callback(self, error_callback or "left_group")

	return 
end
NetworkGroupLobbyPSN.set_join_enabled = function (self, enabled)
	self._join_enable = enabled

	if enabled and not managers.network.systemlink:is_lan() then
		managers.platform:set_presence("MPLobby")
	else
		managers.platform:set_presence("MPLobby_no_invite")
	end

	return 
end
NetworkGroupLobbyPSN.send_group_lobby_invite = function (self, network_friend)
	if self._room_id == nil then
		return false
	end

	for k, v in pairs(self._players) do
		if tostring(v.pnid) == tostring(network_friend) then
			return false
		end
	end

	local friends = PSN:get_list_friends()

	if friends then
		for k, v in pairs(friends) do
			if tostring(v.friend) == tostring(network_friend) and v.status == 2 and v.info and v.info == managers.platform:presence() then
				local msg = {
					join_invite = true
				}

				if not Global.psn_invite_id then
					Global.psn_invite_id = 1
				end

				msg.invite_id = Global.psn_invite_id

				PSN:send_message_custom(network_friend, self._room_id, msg)

				return true
			end
		end
	end

	return false
end
NetworkGroupLobbyPSN.kick_player = function (self, player_id, timeout)
	local v, k, rpc = nil
	k, v = self.find(self, player_id)

	if k and v.rpc then
		rpc = v.rpc

		rpc.lobby_return_answer(rpc, "no")
	end

	self._unregister_player(self, player_id, false, rpc)

	return 
end
NetworkGroupLobbyPSN.accept_group_lobby_invite = function (self, room, accept)
	if accept == true then
		self._call_callback(self, "accepted_group_lobby_invite", room)
	end

	return 
end
NetworkGroupLobbyPSN.send_game_id = function (self, id, private, created)
	if created and created == true then
		for k, v in pairs(self._players) do
			self._call_callback(self, "reserv_slot", v.pnid)
		end
	end

	for k, v in pairs(self._players) do
		v.rpc:psn_send_mm_id(PSN:convert_sessionid_to_string(id), private)
	end

	return 
end
NetworkGroupLobbyPSN.register_callback = function (self, event, callback)
	self._callback_map[event] = callback

	return 
end
NetworkGroupLobbyPSN.start_game = function (self)
	self._call_callback(self, "game_started")

	return 
end
NetworkGroupLobbyPSN.end_game = function (self)
	return 
end
NetworkGroupLobbyPSN.ingame_start_game = function (self)
	if self._server_rpc then
		for k, v in pairs(self._players) do
			if v.is_server then
				managers.network.generic:ping_watch(self._server_rpc, false, callback(self, self, "_server_timed_out"), v.pnid)

				return 
			end
		end
	end

	return 
end
NetworkGroupLobbyPSN.say = function (self, message)
	if self._is_server(self) then
		for k, v in pairs(self._players) do
			v.rpc:say_toclient(message)
		end
	end

	return 
end
NetworkGroupLobbyPSN.membervoted = function (self, player, votes)
	if self._is_server(self) then
		for k, v in pairs(self._players) do
			v.rpc:membervoted_toclient(player, votes)
		end
	end

	return 
end
NetworkGroupLobbyPSN.is_group_leader = function (self)
	return self._is_server(self) == true
end
NetworkGroupLobbyPSN.has_pending_invite = function (self)
	return false
end
NetworkGroupLobbyPSN.is_in_group = function (self)
	if self._inlobby then
		return true
	end

	return false
end
NetworkGroupLobbyPSN.num_group_players = function (self)
	local x = 0

	for k, v in pairs(self._players) do
		x = x + 1
	end

	return x
end
NetworkGroupLobbyPSN.get_group_players = function (self)
	return self._players
end
NetworkGroupLobbyPSN.is_full = function (self)
	if #self._players == self.OPEN_SLOTS - 1 then
		return true
	end

	return false
end
NetworkGroupLobbyPSN.get_leader_rpc = function (self)
	return self._server_rpc
end
NetworkGroupLobbyPSN.get_members_rpcs = function (self)
	local rpcs = {}

	for _, v in pairs(self._players) do
		if v.rpc then
			table.insert(rpcs, v.rpc)
		else
			Application:throw_exception("A player without an RPC. This is not good!")
		end
	end

	return rpcs
end
NetworkGroupLobbyPSN.resync_screen = function (self)
	managers.network:bind_port()

	if self.is_group_leader(self) then
		local playerinfo = {
			name = managers.network.account:username(),
			player_id = managers.network.account:player_id(),
			group_id = tostring(self._room_id),
			rpc = Network:self("TCP_IP")
		}

		self._call_callback(self, "player_joined", playerinfo)
	else
		local playerinfo = {
			name = managers.network.account:username(),
			player_id = managers.network.account:player_id(),
			group_id = tostring(self._room_id),
			rpc = Network:self("TCP_IP")
		}

		self._call_callback(self, "player_joined", playerinfo)
	end

	for k, v in pairs(self._players) do
		local playerinfo = {
			name = v.name,
			player_id = v.pnid,
			group_id = v.group,
			rpc = v.rpc
		}

		self._call_callback(self, "player_joined", playerinfo)
	end

	return 
end
NetworkGroupLobbyPSN.room_id = function (self)
	return self._room_id
end
NetworkGroupLobbyPSN._load_globals = function (self)
	if Global.psn and Global.psn.group then
		self._room_id = Global.psn.group.room_id
		self._inlobby = Global.psn.group.inlobby
		self._is_server_var = Global.psn.group.is_server
		self._is_client_var = Global.psn.group.is_client
		self._players = Global.psn.group.players
		self._server_rpc = Global.psn.group.server_rpc
		self._returned_players = Global.psn.group._returned_players
		Global.psn.group = nil
	end

	return 
end
NetworkGroupLobbyPSN._save_global = function (self)
	if not Global.psn then
		Global.psn = {}
	end

	Global.psn.group = {
		room_id = self._room_id,
		inlobby = self._inlobby,
		is_server = self._is_server_var,
		is_client = self._is_client_var,
		players = self._players,
		server_rpc = self._server_rpc,
		_returned_players = self._returned_players
	}

	return 
end
NetworkGroupLobbyPSN._call_callback = function (self, name, ...)
	if self._callback_map[name] then
		return self._callback_map[name](...)
	else
		Application:error("Callback " .. name .. " not found.")
	end

	return 
end
NetworkGroupLobbyPSN._is_server = function (self, set)
	if set == true or set == false then
		self._is_server_var = set
	else
		return self._is_server_var
	end

	return 
end
NetworkGroupLobbyPSN._is_client = function (self, set)
	if set == true or set == false then
		self._is_client_var = set
	else
		return self._is_client_var
	end

	return 
end
NetworkGroupLobbyPSN._custom_message_cb = function (self, message)
	if message.custom_table and message.custom_table.join_invite and self._join_enable then
		self._invite_id = message.custom_table.invite_id

		self._call_callback(self, "receive_group_lobby_invite", message, message.sender)
	end

	return 
end
NetworkGroupLobbyPSN._recv_game_id = function (self, id, private)
	self._call_callback(self, "receive_game_id", id, private)

	return 
end
NetworkGroupLobbyPSN._created_group_lobby = function (self, room_id)
	if not room_id then
		self._call_callback(self, "create_group_failed")

		return 
	end

	PSN:set_matchmaking_callback("session_created", function ()
		return 
	end)
	self._call_callback(self, "created_group")
	cat_print("lobby", "NetworkGroupLobbyPSN:_created_group_lobby()")

	self._room_id = room_id

	PSN:hide_session(self._room_id)

	self._inlobby = true
	self._room_info = PSN:get_info_session(self._room_id)

	self._is_server(self, true)
	self._is_client(self, false)
	managers.network:bind_port()
	managers.network.voice_chat:open_session(self._room_id)

	local playerinfo = {
		name = managers.network.account:username(),
		player_id = managers.network.account:player_id(),
		group_id = tostring(room_id),
		rpc = Network:self("TCP_IP")
	}

	self._call_callback(self, "player_joined", playerinfo)

	return 
end
NetworkGroupLobbyPSN._clear_psn_callback = function (self, cb)
	local function f()
		return 
	end

	PSN:set_matchmaking_callback(cb, f)

	return 
end
NetworkGroupLobbyPSN._join_invite = function (self, info)
	if info.room_id == self._room_id and info.user_id == info.owner_id then
		self._clear_psn_callback(self, "connection_etablished")

		self._try_time = nil
		self._room_info = PSN:get_info_session(self._room_id)
		self._server_rpc = Network:handshake(info.external_ip, info.port)

		if not self._server_rpc then
			Application:error("Could not connect with rpc")

			return 
		end

		Network:set_timeout(self._server_rpc, 10)

		self._try_time = TimerManager:wall():time() + 10

		self._server_rpc:psn_grp_hello(self._invite_id)
	end

	return 
end
NetworkGroupLobbyPSN._server_alive = function (self, server)
	if self._server_rpc and self._server_rpc:ip_at_index(0) == server.ip_at_index(server, 0) then
		self._try_time = nil

		Network:set_timeout(self._server_rpc, 3000)
		self._server_rpc:psn_grp_register_player(managers.network.account:username(), managers.network.account:player_id(), tostring(managers.network.group:room_id()), false)

		self._inlobby = true
		self._try_time = TimerManager:wall():time() + 10
	end

	return 
end
NetworkGroupLobbyPSN._register_player = function (self, name, pnid, group, rpc, is_server)
	if self.OPEN_SLOTS <= #self._players + 1 then
		return 
	end

	self._try_time = nil
	local new_player = {
		name = name,
		pnid = pnid,
		playerid = pnid,
		group = group
	}

	if self._is_server(self) then
		new_player.rpc = rpc

		rpc.psn_grp_register_player(rpc, managers.network.account:username(), managers.network.account:player_id(), tostring(managers.network.group:room_id()), true)

		for k, v in pairs(self._players) do
			v.rpc:psn_grp_register_player(name, pnid, group, false)
			rpc.psn_grp_register_player(rpc, v.name, v.pnid, v.group, false)
		end

		managers.network.generic:ping_watch(rpc, false, callback(self, self, "_client_timed_out"), pnid)
	end

	if is_server and is_server == true then
		new_player.is_server = true

		managers.network.generic:ping_watch(self._server_rpc, false, callback(self, self, "_server_timed_out"), pnid)
		managers.network.voice_chat:open_session(self._room_id)
		self._call_callback(self, "player_joined", {
			player_id = managers.network.account:player_id(),
			group_id = tostring(self._room_id),
			name = managers.network.account:username(),
			rpc = Network:self("TCP_IP")
		})
	end

	table.insert(self._players, new_player)

	if MPFriendsScreen.instance then
		MPFriendsScreen.instance:reset_list()
	end

	local playerinfo = {
		name = name,
		player_id = pnid,
		group_id = group,
		rpc = rpc
	}

	self._call_callback(self, "player_joined", playerinfo)

	return 
end
NetworkGroupLobbyPSN._unregister_player = function (self, pnid, is_server, rpc)
	if self._is_client(self) and is_server == true then
		self.leave_group_lobby_cb(self)

		return 
	end

	cat_print("lobby", "_unregister_player: didn't leave group")

	local new_list = {}

	for k, v in pairs(self._players) do
		if v.pnid ~= pnid then
			table.insert(new_list, v)
		end
	end

	self._players = new_list

	if MPFriendsScreen.instance then
		MPFriendsScreen.instance:reset_list()
	end

	if self._is_server(self) then
		managers.network.generic:ping_remove(rpc, false)

		for k, v in pairs(self._players) do
			v.rpc:psn_grp_unregister_player(pnid, false)
		end
	end

	self._call_callback(self, "player_left", {
		reason = "went home to mama",
		player_id = pnid
	})

	return 
end
NetworkGroupLobbyPSN._in_list = function (self, id)
	for k, v in pairs(self._players) do
		if tostring(v.pnid) == tostring(id) then
			return true
		end
	end

	return false
end
NetworkGroupLobbyPSN._server_timed_out = function (self, rpc)
	NetworkGroupLobby._server_timed_out(self, rpc)
	self._unregister_player(self, nil, true, rpc)

	return 
end
NetworkGroupLobbyPSN._client_timed_out = function (self, rpc)
	for k, v in pairs(self._players) do
		if v.rpc and v.rpc:ip_at_index(0) == rpc.ip_at_index(rpc, 0) then
			self._unregister_player(self, v.pnid, false, v.rpc)

			return 
		end
	end

	return 
end
NetworkGroupLobbyPSN.leaving_game = function (self)
	if self._is_server(self) then
		self.leave_group_lobby(self, true)
	end

	return 
end

return 
