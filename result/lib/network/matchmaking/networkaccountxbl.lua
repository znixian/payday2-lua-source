require("lib/network/matchmaking/NetworkAccount")

NetworkAccountXBL = NetworkAccountXBL or class(NetworkAccount)
NetworkAccountXBL.init = function (self)
	NetworkAccount.init(self)

	return 
end
NetworkAccountXBL.signin_state = function (self)
	local xbl_state = managers.user:signed_in_state(managers.user:get_index())
	local game_signin_state = self._translate_signin_state(self, xbl_state)

	return game_signin_state
end
NetworkAccountXBL.local_signin_state = function (self)
	local xbl_state = managers.user:signed_in_state(managers.user:get_index())

	if xbl_state == "not_signed_in" then
		return "not signed in"
	end

	if xbl_state == "signed_in_locally" then
		return "signed in"
	end

	if xbl_state == "signed_in_to_live" then
		return "signed in"
	end

	return "not signed in"
end
NetworkAccountXBL.show_signin_ui = function (self)
	return 
end
NetworkAccountXBL.username_id = function (self)
	return (Global.user_manager.user_index and Global.user_manager.user_map[Global.user_manager.user_index].username) or ""
end
NetworkAccountXBL.player_id = function (self)
	return managers.user:get_xuid(nil)
end
NetworkAccountXBL.is_connected = function (self)
	return true
end
NetworkAccountXBL.lan_connection = function (self)
	return true
end
NetworkAccountXBL.publish_statistics = function (self, stats, force_store)
	Application:error("NetworkAccountXBL:publish_statistics( stats, force_store )")
	Application:stack_dump()

	return 
end
NetworkAccountXBL.challenges_loaded = function (self)
	self._challenges_loaded = true

	return 
end
NetworkAccountXBL.experience_loaded = function (self)
	self._experience_loaded = true

	return 
end
NetworkAccountXBL._translate_signin_state = function (self, xbl_state)
	if xbl_state == "signed_in_to_live" then
		return "signed in"
	end

	return "not signed in"
end

return 
