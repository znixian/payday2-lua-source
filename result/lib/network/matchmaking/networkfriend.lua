NetworkFriend = NetworkFriend or class()
NetworkFriend.init = function (self, id, name, signin_status)
	self._id = id
	self._name = name
	self._signin_status = signin_status

	return 
end
NetworkFriend.id = function (self)
	return self._id
end
NetworkFriend.name = function (self)
	return self._name
end
NetworkFriend.signin_status = function (self)
	return self._signin_status
end

return 
