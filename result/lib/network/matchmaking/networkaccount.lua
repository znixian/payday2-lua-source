NetworkAccount = NetworkAccount or class()
NetworkAccount.init = function (self)
	self._postprocess_username = callback(self, self, "_standard_username")

	self.set_lightfx(self)

	return 
end
NetworkAccount.update = function (self)
	return 
end
NetworkAccount.create_account = function (self, name, password, email)
	return 
end
NetworkAccount.reset_password = function (self, name, email)
	return 
end
NetworkAccount.login = function (self, name, password, cdkey)
	return 
end
NetworkAccount.logout = function (self)
	return 
end
NetworkAccount.register_callback = function (self, event, callback)
	return 
end
NetworkAccount.register_post_username = function (self, cb)
	self._postprocess_username = cb

	return 
end
NetworkAccount.username = function (self)
	return self._postprocess_username(self.username_id(self))
end
NetworkAccount.username_id = function (self)
	return 0
end
NetworkAccount.username_by_id = function (self)
	return ""
end
NetworkAccount.signin_state = function (self)
	return "not signed in"
end
NetworkAccount.set_lightfx = function (self)
	if SystemInfo:platform() ~= Idstring("WIN32") then
		return 
	end

	if managers.user:get_setting("use_lightfx") then
		print("[NetworkAccount:init] Initializing LightFX...")

		self._has_alienware = LightFX:initialize() and LightFX:has_lamps()

		if self._has_alienware then
			LightFX:set_lamps(0, 255, 0, 255)
		end
	else
		self._has_alienware = nil
	end

	return 
end
NetworkAccount.has_alienware = function (self)
	return self._has_alienware
end
NetworkAccount.clan_tag = function (self)
	if managers.save.get_profile_setting and managers.save:get_profile_setting("clan_tag") and 0 < string.len(managers.save:get_profile_setting("clan_tag")) then
		return "[" .. managers.save:get_profile_setting("clan_tag") .. "]"
	end

	return ""
end
NetworkAccount._standard_username = function (self, name)
	return name
end
NetworkAccount.set_playing = function (self, state)
	return 
end
NetworkAccount._load_globals = function (self)
	return 
end
NetworkAccount._save_globals = function (self)
	return 
end
NetworkAccount.inventory_load = function (self)
	return 
end
NetworkAccount.inventory_is_loading = function (self)
	return 
end
NetworkAccount.inventory_reward = function (self, item)
	return false
end
NetworkAccount.inventory_reward_dlc = function (self)
	return 
end
NetworkAccount.inventory_reward_unlock = function (self, box, key)
	return 
end
NetworkAccount.inventory_reward_open = function (self, item)
	return 
end
NetworkAccount.inventory_outfit_refresh = function (self)
	return 
end
NetworkAccount.inventory_outfit_verify = function (self, id, outfit_data, outfit_callback)
	return 
end
NetworkAccount.inventory_outfit_signature = function (self)
	return ""
end
NetworkAccount.inventory_repair_list = function (self, list)
	return 
end
NetworkAccount.is_ready_to_close = function (self)
	return true
end

return 
