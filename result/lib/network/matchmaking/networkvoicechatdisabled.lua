NetworkVoiceChatDisabled = NetworkVoiceChatDisabled or class()
NetworkVoiceChatDisabled.init = function (self, quiet)
	self._quiet = quiet or false

	if self._quiet then
		cat_print("lobby", "Voice is quiet.")
	else
		cat_print("lobby", "Voice is disabled.")
	end

	return 
end
NetworkVoiceChatDisabled.check_status_information = function (self)
	return 
end
NetworkVoiceChatDisabled.open = function (self)
	return 
end
NetworkVoiceChatDisabled.set_volume = function (self, volume)
	return 
end
NetworkVoiceChatDisabled.voice_type = function (self)
	if self._quiet == true then
		return "voice_quiet"
	else
		return "voice_disabled"
	end

	return 
end
NetworkVoiceChatDisabled.set_drop_in = function (self, data)
	return 
end
NetworkVoiceChatDisabled.pause = function (self)
	return 
end
NetworkVoiceChatDisabled.resume = function (self)
	return 
end
NetworkVoiceChatDisabled.init_voice = function (self)
	return 
end
NetworkVoiceChatDisabled.destroy_voice = function (self)
	return 
end
NetworkVoiceChatDisabled.num_peers = function (self)
	return true
end
NetworkVoiceChatDisabled.open_session = function (self, roomid)
	return 
end
NetworkVoiceChatDisabled.close_session = function (self)
	return 
end
NetworkVoiceChatDisabled.open_channel_to = function (self, player_info, context)
	return 
end
NetworkVoiceChatDisabled.close_channel_to = function (self, player_info)
	return 
end
NetworkVoiceChatDisabled.lost_peer = function (self, peer)
	return 
end
NetworkVoiceChatDisabled.close_all = function (self)
	return 
end
NetworkVoiceChatDisabled.set_team = function (self, team)
	return 
end
NetworkVoiceChatDisabled.peer_team = function (self, xuid, team, rpc)
	return 
end
NetworkVoiceChatDisabled._open_close_peers = function (self)
	return 
end
NetworkVoiceChatDisabled.mute_player = function (self, mute, peer)
	return 
end
NetworkVoiceChatDisabled.update = function (self)
	return 
end
NetworkVoiceChatDisabled._load_globals = function (self)
	return 
end
NetworkVoiceChatDisabled._save_globals = function (self, disable_voice)
	return 
end
NetworkVoiceChatDisabled._display_warning = function (self)
	if self._quiet == false and self._have_displayed_warning(self) == true then
		managers.menu:show_err_no_chat_parental_control()
	end

	return 
end
NetworkVoiceChatDisabled._have_displayed_warning = function (self)
	if Global.psn_parental_voice and Global.psn_parental_voice == true then
		return false
	end

	Global.psn_parental_voice = true

	return true
end
NetworkVoiceChatDisabled.clear_team = function (self)
	return 
end
NetworkVoiceChatDisabled.psn_session_destroyed = function (self)
	if Global.psn and Global.psn.voice then
		Global.psn.voice.restart = nil
	end

	return 
end

return 
