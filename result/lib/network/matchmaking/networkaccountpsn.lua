require("lib/network/matchmaking/NetworkAccount")

NetworkAccountPSN = NetworkAccountPSN or class(NetworkAccount)
NetworkAccountPSN.init = function (self)
	NetworkAccount.init(self)

	return 
end
NetworkAccountPSN.signin_state = function (self)
	if PSN:is_online() == true then
		return "signed in"
	end

	return "not signed in"
end
NetworkAccountPSN.local_signin_state = function (self)
	if not PSN:cable_connected() then
		return false
	end

	local n = PSN:get_localinfo()

	if not n then
		return false
	end

	if not n.local_ip then
		return false
	end

	return true
end
NetworkAccountPSN.show_signin_ui = function (self)
	PSN:display_online_connection()

	return 
end
NetworkAccountPSN.username_id = function (self)
	local online_name = PSN:get_npid_user()

	if online_name then
		return online_name
	else
		local local_user_info_name = PS3:get_userinfo()

		if local_user_info_name then
			return local_user_info_name
		end
	end

	return managers.localization:text("menu_mp_player")
end
NetworkAccountPSN.player_id = function (self)
	if PSN:get_npid_user() == nil then
		local n = PSN:get_localinfo()

		if n and n.local_ip then
			return n.local_ip
		end

		Application:error("Could not get local ip, returning \"player_id\" VERY BAD!.")

		return "player_id"
	end

	return PSN:get_npid_user()
end
NetworkAccountPSN.is_connected = function (self)
	return true
end
NetworkAccountPSN.lan_connection = function (self)
	return PSN:cable_connected()
end
NetworkAccountPSN._lan_ip = function (self)
	local l = PSN:get_lan_info()

	if l and l.lan_ip then
		return l.lan_ip
	end

	return "player_lan"
end
NetworkAccountPSN.publish_statistics = function (self, stats, force_store)
	Application:error("NetworkAccountPSN:publish_statistics( stats, force_store )")
	Application:stack_dump()

	return 
end
NetworkAccountPSN.achievements_fetched = function (self)
	self._achievements_fetched = true

	return 
end
NetworkAccountPSN.challenges_loaded = function (self)
	self._challenges_loaded = true

	return 
end
NetworkAccountPSN.experience_loaded = function (self)
	self._experience_loaded = true

	return 
end

return 
