NetworkVoiceChatSTEAM = NetworkVoiceChatSTEAM or class()
NetworkVoiceChatSTEAM.init = function (self)
	self.handler = Steam:voip_handler()
	self._enabled = false
	self._users_talking = {}

	return 
end
NetworkVoiceChatSTEAM.set_volume = function (self, volume)
	self.handler:set_out_volume(volume)

	return 
end
NetworkVoiceChatSTEAM.open = function (self)
	self._push_to_talk = managers.user:get_setting("push_to_talk")

	if not self._enabled and managers.user:get_setting("voice_chat") then
		self.handler:open()

		self._enabled = true

		if not self._push_to_talk then
			self.handler:start_recording()
		end
	end

	return 
end
NetworkVoiceChatSTEAM.destroy_voice = function (self, disconnected)
	if self._enabled then
		self.handler:stop_recording()
		self.handler:close()

		self._enabled = false
	end

	return 
end
NetworkVoiceChatSTEAM._load_globals = function (self)
	if Global.steam and Global.steam.voip then
		self.handler = Global.steam.voip.handler
		Global.steam.voip = nil
	end

	return 
end
NetworkVoiceChatSTEAM._save_globals = function (self)
	if not Global.steam then
		Global.steam = {}
	end

	Global.steam.voip = {
		handler = self.handler
	}

	return 
end
NetworkVoiceChatSTEAM.enabled = function (self)
	return managers.user:get_setting("voice_chat")
end
NetworkVoiceChatSTEAM.set_recording = function (self, enabled)
	if not self._push_to_talk then
		return 
	end

	if enabled then
		self.handler:start_recording()
	else
		self.handler:stop_recording()
	end

	return 
end
NetworkVoiceChatSTEAM.update = function (self)
	self.handler:update()

	local t = Application:time()
	local playing = self.handler:get_voice_receivers_playing()

	for id, pl in pairs(playing) do
		if not self._users_talking[id] then
			self._users_talking[id] = {
				time = 0
			}
		end

		if pl then
			self._users_talking[id].time = t
		end

		local active = t < self._users_talking[id].time + 0.15

		if active ~= self._users_talking[id].active then
			self._users_talking[id].active = active

			if managers.network:session() then
				local peer = managers.network:session():peer(id)

				if peer then
					managers.menu:set_slot_voice(peer, id, active)

					if managers.hud then
						local crim_data = managers.criminals:character_data_by_peer_id(id)

						if crim_data then
							local mugshot = crim_data.mugshot_id

							managers.hud:set_mugshot_voice(mugshot, active)
						end
					end
				end
			end
		end
	end

	return 
end
NetworkVoiceChatSTEAM.on_member_added = function (self, peer, mute)
	if peer.rpc(peer) then
		self.handler:add_receiver(peer.id(peer), peer.rpc(peer), mute)
	end

	return 
end
NetworkVoiceChatSTEAM.on_member_removed = function (self, peer)
	self.handler:remove_receiver(peer.id(peer))

	return 
end
NetworkVoiceChatSTEAM.mute_player = function (self, peer, mute)
	self.handler:mute_voice_receiver(peer.id(peer), mute)

	return 
end
NetworkVoiceChatSTEAM.is_muted = function (self, peer)
	return self.handler:is_voice_receiver_muted(peer.id(peer))
end

return 
