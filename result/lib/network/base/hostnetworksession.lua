require("lib/network/base/session_states/HostStateBase")
require("lib/network/base/session_states/HostStateInLobby")
require("lib/network/base/session_states/HostStateInGame")
require("lib/network/base/session_states/HostStateLoadout")
require("lib/network/base/session_states/HostStateGameEnd")
require("lib/network/base/session_states/HostStateClosing")

HostNetworkSession = HostNetworkSession or class(BaseNetworkSession)
HostNetworkSession._STATES = {
	in_lobby = HostStateInLobby,
	loadout = HostStateLoadout,
	in_game = HostStateInGame,
	game_end = HostStateGameEnd,
	closing = HostStateClosing
}
HostNetworkSession._DEAD_CONNECTION_REPORT_PROCESS_DELAY = math.max(HostNetworkSession.CONNECTION_TIMEOUT, HostNetworkSession.LOADING_CONNECTION_TIMEOUT) + 1.5
HostNetworkSession._LOAD_COUNTER_LIMITS = {
	1,
	1024
}
HostNetworkSession.init = function (self)
	HostNetworkSession.super.init(self)

	self._state_data = {
		session = self,
		peers = self._peers,
		kicked_list = self._kicked_list
	}
	self._peer_outfit_loaded_status_request_id = math.random(1, 100)
	self._load_counter = math.random(self._LOAD_COUNTER_LIMITS[1], self._LOAD_COUNTER_LIMITS[2])

	print("[HostNetworkSession:init] self._load_counter", self._load_counter)

	return 
end
HostNetworkSession.create_local_peer = function (self, load_outfit)
	HostNetworkSession.super.create_local_peer(self, load_outfit)

	self._state_data.local_peer = self._local_peer

	self.register_local_peer(self, 1)
	self.set_state(self, "in_lobby")

	if Network:multiplayer() then
		Network:set_server()
	end

	return 
end
HostNetworkSession.on_join_request_received = function (self, peer_name, preferred_character, dlcs, xuid, peer_level, peer_rank, gameversion, join_attempt_identifier, auth_ticket, sender)
	return self._state.on_join_request_received and self._state:on_join_request_received(self._state_data, peer_name, preferred_character, dlcs, xuid, peer_level, peer_rank, gameversion, join_attempt_identifier, auth_ticket, sender)
end
HostNetworkSession.send_to_host = function (self, ...)
	debug_pause("[HostNetworkSession:send_to_host] This is dumb. call the function directly instead of sending it...")

	return 
end
HostNetworkSession.is_host = function (self)
	return true
end
HostNetworkSession.is_client = function (self)
	return false
end
HostNetworkSession.load_level = function (self, ...)
	self._inc_load_counter(self)
	self._state:on_load_level(self._state_data)
	managers.network.matchmake:set_server_state("loading")

	for _, peer in pairs(self._peers) do
		peer.set_synched(peer, false)
		peer.set_loaded(peer, false)
	end

	self._local_peer:set_loaded(false)
	self.set_state(self, "closing")
	self.send_to_peers(self, "set_loading_state", true, self._load_counter)
	self.send_ok_to_load_level(self)
	self._load_level(self, ...)

	return 
end
HostNetworkSession.load_lobby = function (self, ...)
	self._inc_load_counter(self)
	managers.network.matchmake:set_server_state("loading")

	for _, peer in pairs(self._peers) do
		peer.set_synched(peer, false)
		peer.set_loaded(peer, false)
		peer.set_in_lobby(peer, true)
	end

	self._local_peer:set_loaded(false)
	self.set_state(self, "closing")
	self.send_to_peers(self, "set_loading_state", true, self._load_counter)
	self.send_ok_to_load_lobby(self)
	self._load_lobby(self, ...)

	return 
end
HostNetworkSession.broadcast_server_up = function (self)
	print("[HostNetworkSession:broadcast_server_up]")
	Network:broadcast(NetworkManager.DEFAULT_PORT):server_up()

	return 
end
HostNetworkSession.on_server_up_received = function (self)
	return 
end
HostNetworkSession.load = function (self, data)
	if Global.load_start_menu_lobby then
		self.set_state(self, "in_lobby")
	else
		self.set_state(self, "loadout")
	end

	HostNetworkSession.super.load(self, data)

	self._state_data = {
		session = self,
		peers = self._peers,
		kicked_list = self._kicked_list,
		local_peer = self._local_peer
	}

	return 
end
HostNetworkSession.on_peer_connection_established = function (self, sender_peer, introduced_peer_id)
	print("[HostNetworkSession:on_peer_connection_established]", sender_peer.id(sender_peer), introduced_peer_id)

	if not self._peers[introduced_peer_id] then
		print("introduced peer does not exist. ignoring.")

		return 
	end

	print("status:", sender_peer.handshakes(sender_peer)[introduced_peer_id], self._peers[introduced_peer_id]:handshakes()[sender_peer.id(sender_peer)])

	if sender_peer.handshakes(sender_peer)[introduced_peer_id] == "asked" then
		sender_peer.set_handshake_status(sender_peer, introduced_peer_id, "exchanging_info")

		local introduced_peer = self._peers[introduced_peer_id]

		if introduced_peer.handshakes(introduced_peer)[sender_peer.id(sender_peer)] == "exchanging_info" then
			sender_peer.send(sender_peer, "peer_exchange_info", introduced_peer_id)
			introduced_peer.send(introduced_peer, "peer_exchange_info", sender_peer.id(sender_peer))
		end

		return 
	elseif sender_peer.handshakes(sender_peer)[introduced_peer_id] == "exchanging_info" then
		sender_peer.set_handshake_status(sender_peer, introduced_peer_id, true)
	else
		print("peer was not asked. ignoring.")

		return 
	end

	if self._state.on_handshake_confirmation then
		self._state:on_handshake_confirmation(self._state_data, sender_peer, introduced_peer_id)
	end

	if sender_peer.loaded(sender_peer) then
		sender_peer.send(sender_peer, "set_member_ready", introduced_peer_id, (self._peers[introduced_peer_id]:waiting_for_player_ready() and 1) or 0, 1, "")
	end

	self.chk_initiate_dropin_pause(self, sender_peer)

	if self._peers[introduced_peer_id] then
		self.chk_initiate_dropin_pause(self, self._peers[introduced_peer_id])
	end

	return 
end
HostNetworkSession.set_game_started = function (self, state)
	self._game_started = state
	self._state_data.game_started = state

	if state then
		self.set_state(self, "in_game")
	end

	return 
end
HostNetworkSession.chk_peer_already_in = function (self, rpc)
	local old_peer = nil

	if rpc.protocol_at_index(rpc, 0) == "STEAM" then
		old_peer = self.peer_by_user_id(self, rpc.ip_at_index(rpc, 0))
	else
		old_peer = self.peer_by_ip(self, rpc.ip_at_index(rpc, 0))
	end

	return old_peer
end
HostNetworkSession.send_ok_to_load_level = function (self)
	for peer_id, peer in pairs(self._peers) do
		peer.send(peer, "ok_to_load_level", self._load_counter)
	end

	return 
end
HostNetworkSession.send_ok_to_load_lobby = function (self)
	for peer_id, peer in pairs(self._peers) do
		peer.send(peer, "ok_to_load_lobby", self._load_counter)
	end

	return 
end
HostNetworkSession.on_peer_save_received = function (self, event, event_data)
	if managers.network:stopping() then
		return 
	end

	local peer = self.peer_by_ip(self, event_data.ip_address)

	if not peer then
		Application:error("[HostNetworkSession:on_peer_save_received] A nonregistered peer confirmed save packet.")

		return 
	end

	if event_data.index then
		local packet_index = event_data.index
		local total_nr_packets = event_data.total
		local progress_ratio = packet_index/total_nr_packets
		local progress_percentage = math.floor(math.clamp(progress_ratio*100, 0, 100))
		local is_playing = BaseNetworkHandler._gamestate_filter.any_ingame_playing[game_state_machine:last_queued_state_name()]

		if is_playing then
			managers.menu:update_person_joining(peer.id(peer), progress_percentage)
		elseif BaseNetworkHandler._gamestate_filter.any_ingame[game_state_machine:last_queued_state_name()] then
			managers.menu:get_menu("kit_menu").renderer:set_dropin_progress(peer.id(peer), progress_percentage, "join")
		end

		self.send_to_peers_synched_except(self, peer.id(peer), "dropin_progress", peer.id(peer), progress_percentage)
	else
		cat_print("multiplayer_base", "[HostNetworkSession:on_peer_save_received]", peer, peer.id(peer))
		peer.set_synched(peer, true)
		peer.send(peer, "set_peer_synched", 1)

		for old_peer_id, old_peer in pairs(self._peers) do
			if old_peer ~= peer and old_peer.synched(old_peer) then
				peer.send(peer, "set_peer_synched", old_peer_id)
			end
		end

		for _, old_peer in pairs(self._peers) do
			if old_peer ~= peer then
				old_peer.send_after_load(old_peer, "set_peer_synched", peer.id(peer))
			end
		end

		if NetworkManager.DROPIN_ENABLED then
			for other_peer_id, other_peer in pairs(self._peers) do
				if other_peer_id ~= peer.id(peer) and other_peer.expecting_dropin(other_peer) then
					self.set_dropin_pause_request(self, peer, other_peer_id, "asked")
				end
			end

			for old_peer_id, old_peer in pairs(self._peers) do
				if old_peer_id ~= peer.id(peer) and old_peer.is_expecting_pause_confirmation(old_peer, peer.id(peer)) then
					self.set_dropin_pause_request(self, old_peer, peer.id(peer), false)
				end
			end

			if self._local_peer:is_expecting_pause_confirmation(peer.id(peer)) then
				self._local_peer:set_expecting_drop_in_pause_confirmation(peer.id(peer), nil)
				self.on_drop_in_pause_request_received(self, peer.id(peer), peer.name(peer), false)
			end
		end

		for other_peer_id, other_peer in pairs(self._peers) do
			self.chk_spawn_member_unit(self, other_peer, other_peer_id)
		end

		self.on_peer_sync_complete(self, peer, peer.id(peer))
	end

	return 
end
HostNetworkSession.update = function (self)
	HostNetworkSession.super.update(self)
	self.process_dead_con_reports(self)

	return 
end
HostNetworkSession.set_peer_loading_state = function (self, peer, state, load_counter)
	print("[HostNetworkSession:set_peer_loading_state]", peer.id(peer), state, load_counter)

	if load_counter ~= self._load_counter then
		Application:error("wrong load counter", self._load_counter)

		if not state then
			if Global.load_start_menu_lobby then
				self.send_ok_to_load_lobby(self)
			else
				self.send_ok_to_load_level(self)
			end
		end

		return 
	end

	HostNetworkSession.super.set_peer_loading_state(self, peer, state)
	peer.set_loading(peer, state)

	if Global.load_start_menu_lobby then
		return 
	end

	if not state then
		for other_peer_id, other_peer in pairs(self._peers) do
			if other_peer ~= peer and peer.handshakes(peer)[other_peer_id] == true then
				peer.send_after_load(peer, "set_member_ready", other_peer_id, (other_peer.waiting_for_player_ready(other_peer) and 1) or 0, 1, "")
			end
		end

		peer.send_after_load(peer, "set_member_ready", self._local_peer:id(), (self._local_peer:waiting_for_player_ready() and 1) or 0, 1, "")

		if self._local_peer:is_outfit_loaded() then
			peer.send_after_load(peer, "set_member_ready", self._local_peer:id(), 100, 2, "")
		end

		self.chk_request_peer_outfit_load_status(self)

		if self._local_peer:loaded() and NetworkManager.DROPIN_ENABLED then
			if self._state.on_peer_finished_loading then
				self._state:on_peer_finished_loading(self._state_data, peer)
			end

			peer.set_expecting_pause_sequence(peer, true)

			local dropin_pause_ok = self.chk_initiate_dropin_pause(self, peer)

			if dropin_pause_ok then
				self.chk_drop_in_peer(self, peer)
			else
				print(" setting set_expecting_pause_sequence", peer.id(peer))
			end
		end
	end

	return 
end
HostNetworkSession.on_drop_in_pause_confirmation_received = function (self, dropin_peer_id, sender_peer)
	print("[HostNetworkSession:on_drop_in_pause_confirmation_received]", sender_peer.id(sender_peer), " paused for ", dropin_peer_id)

	local is_expecting = sender_peer.is_expecting_pause_confirmation(sender_peer, dropin_peer_id)
	local dropin_peer = self._peers[dropin_peer_id]

	if dropin_peer and dropin_peer.expecting_dropin(dropin_peer) then
		if is_expecting == "asked" then
			self.set_dropin_pause_request(self, sender_peer, dropin_peer_id, "paused")
		else
			print("peer", sender_peer.id(sender_peer), "was not asked for confirmation. is_expecting:", is_expecting)
		end

		self.chk_drop_in_peer(self, dropin_peer)
	else
		self.set_dropin_pause_request(self, sender_peer, dropin_peer_id, false)

		if dropin_peer then
			self.chk_initiate_dropin_pause(self, dropin_peer)
		end
	end

	return 
end
HostNetworkSession.chk_initiate_dropin_pause = function (self, dropin_peer)
	print("[HostNetworkSession:chk_initiate_dropin_pause]", dropin_peer.id(dropin_peer))

	if not dropin_peer.expecting_pause_sequence(dropin_peer) then
		print("not expecting")

		return 
	end

	if not self.chk_peer_handshakes_complete(self, dropin_peer) then
		print("misses handshakes")

		return 
	end

	if not self.all_peers_done_loading_outfits(self) then
		print("peers still streaming outfits")

		return 
	end

	if not dropin_peer.other_peer_outfit_loaded_status(dropin_peer) then
		print("dropin peer has not loaded outfits")

		return 
	end

	for peer_id, peer in pairs(self._peers) do
		local is_expecting = peer.is_expecting_pause_confirmation(peer, dropin_peer.id(dropin_peer))

		if is_expecting then
			print(" peer", peer_id, "is still to confirm", is_expecting)

			return 
		end
	end

	for other_peer_id, other_peer in pairs(self._peers) do
		if other_peer_id ~= dropin_peer.id(dropin_peer) and not other_peer.is_expecting_pause_confirmation(other_peer, dropin_peer.id(dropin_peer)) then
			self.set_dropin_pause_request(self, other_peer, dropin_peer.id(dropin_peer), "asked")
		end
	end

	if not self._local_peer:is_expecting_pause_confirmation(dropin_peer.id(dropin_peer)) then
		self._local_peer:set_expecting_drop_in_pause_confirmation(dropin_peer.id(dropin_peer), "paused")
		self.on_drop_in_pause_request_received(self, dropin_peer.id(dropin_peer), dropin_peer.name(dropin_peer), true)
	end

	dropin_peer.set_expecting_pause_sequence(dropin_peer, nil)
	dropin_peer.set_expecting_dropin(dropin_peer, true)

	return true
end
HostNetworkSession.chk_drop_in_peer = function (self, dropin_peer)
	local dropin_peer_id = dropin_peer.id(dropin_peer)

	if not dropin_peer.expecting_dropin(dropin_peer) then
		print("[HostNetworkSession:chk_drop_in_peer] not expecting drop-in", dropin_peer_id)

		return 
	end

	print("[HostNetworkSession:chk_drop_in_peer]", dropin_peer_id)

	for peer_id, peer in pairs(self._peers) do
		if dropin_peer_id ~= peer_id and peer.synched(peer) then
			local is_expecting = peer.is_expecting_pause_confirmation(peer, dropin_peer_id)

			if is_expecting ~= "paused" then
				print("peer", peer_id, "is expected to confirm", is_expecting)

				if not is_expecting then
					debug_pause("was not asked!")
				end

				return 
			end
		end
	end

	print("doing drop-in!")
	Application:stack_dump()
	dropin_peer.set_expecting_dropin(dropin_peer, nil)
	dropin_peer.on_sync_start(dropin_peer)
	dropin_peer.chk_enable_queue(dropin_peer)
	dropin_peer.set_is_dropin(dropin_peer, true)

	self._dropin_peer = dropin_peer

	Network:drop_in(dropin_peer.rpc(dropin_peer))

	self._dropin_peer = nil

	return true
end
HostNetworkSession.dropin_peer = function (self)
	return self._dropin_peer
end
HostNetworkSession.add_peer = function (self, name, rpc, in_lobby, loading, synched, id, character, user_id, xuid, xnaddr)
	id = id or self._get_free_client_id(self, user_id)

	if not id then
		return 
	end

	name = name or "Player " .. tostring(id)

	if managers.wait then
		managers.wait:clear_peer(id)
	end

	local peer = nil
	id, peer = HostNetworkSession.super.add_peer(self, name, rpc, in_lobby, loading, synched, id, character, user_id, xuid, xnaddr)

	self.chk_server_joinable_state(self)

	return id, peer
end
HostNetworkSession._get_free_client_id = function (self, user_id)
	local i = 2

	repeat
		if not self._peers[i] then
			local is_dirty = false

			for peer_id, peer in pairs(self._peers) do
				if peer.handshakes(peer)[i] then
					is_dirty = true
				end
			end

			if not is_dirty then
				return i
			end
		end

		i = i + 1
	until i == tweak_data.max_players + 1

	return 
end
HostNetworkSession.remove_peer = function (self, peer, peer_id, reason)
	print("[HostNetworkSession:remove_peer]", inspect(peer), peer_id, reason)
	HostNetworkSession.super.remove_peer(self, peer, peer_id, reason)
	managers.vote:abort_vote(peer_id)

	if self._dead_con_reports then
		local i = #self._dead_con_reports

		while 0 < i do
			local dead_con_report = self._dead_con_reports[i]

			if dead_con_report.reporter == peer or dead_con_report.reported == peer then
				table.remove(self._dead_con_reports, i)
			end

			i = i - 1
		end

		if not next(self._dead_con_reports) then
			self._dead_con_reports = nil
		end
	end

	if NetworkManager.DROPIN_ENABLED then
		for other_peer_id, other_peer in pairs(self._peers) do
			if other_peer.is_expecting_pause_confirmation(other_peer, peer_id) then
				self.set_dropin_pause_request(self, other_peer, peer_id, false)
			end
		end

		if self._local_peer:is_expecting_pause_confirmation(peer_id) then
			self._local_peer:set_expecting_drop_in_pause_confirmation(peer_id, nil)
			self.on_drop_in_pause_request_received(self, peer_id, "", false)
		end

		for other_peer_id, other_peer in pairs(self._peers) do
			self.chk_initiate_dropin_pause(self, other_peer)
			self.chk_drop_in_peer(self, other_peer)
			self.chk_spawn_member_unit(self, other_peer, other_peer_id)
		end
	end

	local info_msg_type = "kick_peer"
	local info_msg_id = nil

	if reason == "kicked" then
		info_msg_id = 0
	elseif reason == "auth_fail" then
		info_msg_id = 2
	else
		info_msg_id = 1
	end

	for other_peer_id, other_peer in pairs(self._peers) do
		if other_peer.handshakes(other_peer)[peer_id] == true or other_peer.handshakes(other_peer)[peer_id] == "asked" or other_peer.handshakes(other_peer)[peer_id] == "exchanging_info" then
			other_peer.send(other_peer, info_msg_type, peer_id, info_msg_id)
			other_peer.set_handshake_status(other_peer, peer_id, "removing")
		end
	end

	if reason ~= "left" and reason ~= "kicked" and reason ~= "auth_fail" then
		peer.send(peer, info_msg_type, peer_id, info_msg_id)
	end

	self.chk_server_joinable_state(self)

	return 
end
HostNetworkSession.on_remove_peer_confirmation = function (self, sender_peer, removed_peer_id)
	print("[HostNetworkSession:on_remove_peer_confirmation]", sender_peer.id(sender_peer), removed_peer_id)

	if sender_peer.handshakes(sender_peer)[removed_peer_id] ~= "removing" then
		print("peer should not remove. ignoring.")

		return 
	end

	sender_peer.set_handshake_status(sender_peer, removed_peer_id, nil)
	self.chk_server_joinable_state(self)
	self.check_start_game_intro(self)

	return 
end
HostNetworkSession.on_dead_connection_reported = function (self, reporter_peer_id, other_peer_id)
	print("[HostNetworkSession:on_dead_connection_reported]", reporter_peer_id, other_peer_id)

	if not self._peers[reporter_peer_id] or not self._peers[other_peer_id] then
		return 
	end

	self._dead_con_reports = self._dead_con_reports or {}
	local entry = {
		process_t = TimerManager:wall():time() + self._DEAD_CONNECTION_REPORT_PROCESS_DELAY,
		reporter = self._peers[reporter_peer_id],
		reported = self._peers[other_peer_id]
	}

	table.insert(self._dead_con_reports, entry)

	return 
end
HostNetworkSession.process_dead_con_reports = function (self)
	if self._dead_con_reports then
		local t = TimerManager:wall():time()
		local first_dead_con_report = self._dead_con_reports[1]

		if first_dead_con_report.process_t < t then
			if #self._dead_con_reports == 1 then
				self._dead_con_reports = nil
			else
				table.remove(self._dead_con_reports, 1)
			end

			local kick_peer = nil
			local reporter_peer = first_dead_con_report.reporter
			local reported_peer = first_dead_con_report.reported

			if reported_peer.creation_t(reported_peer) < reporter_peer.creation_t(reporter_peer) then
				print("[HostNetworkSession:process_dead_con_reports] kicking reporter ", reporter_peer.id(reporter_peer), reported_peer.id(reported_peer), reporter_peer.creation_t(reporter_peer), reported_peer.creation_t(reported_peer))

				kick_peer = reporter_peer
			else
				print("[HostNetworkSession:process_dead_con_reports] kicking reported ", reporter_peer.id(reporter_peer), reported_peer.id(reported_peer), reporter_peer.creation_t(reporter_peer), reported_peer.creation_t(reported_peer))

				kick_peer = reported_peer
			end

			self.on_peer_kicked(self, kick_peer, kick_peer.id(kick_peer), 1)
		end
	end

	return 
end
HostNetworkSession.chk_spawn_member_unit = function (self, peer, peer_id)
	print("[HostNetworkSession:chk_spawn_member_unit]", peer.name(peer), peer_id)

	if not self._game_started then
		print("Game not started yet")

		return 
	end

	if peer.spawn_unit_called(peer) or not peer.waiting_for_player_ready(peer) or not peer.is_streaming_complete(peer) then
		print("not ready to spawn unit: peer:spawn_unit_called()", peer.spawn_unit_called(peer), "peer:waiting_for_player_ready()", peer.waiting_for_player_ready(peer), "peer:is_streaming_complete()", peer.is_streaming_complete(peer))

		return 
	end

	for other_peer_id, other_peer in pairs(self._peers) do
		if not other_peer.synched(other_peer) then
			print("peer", other_peer_id, "is not synched")

			return 
		end
	end

	if not self.chk_all_handshakes_complete(self) then
		return 
	end

	if not self.all_peers_done_loading_outfits(self) then
		return 
	end

	if managers.wait:check_if_waiting_needed() then
		self._add_waiting(self, peer)
	else
		self._spawn_dropin_player(self, peer)
	end

	return 
end
HostNetworkSession._add_waiting = function (self, peer)
	managers.wait:add_waiting(peer.id(peer))
	peer.make_waiting(peer)

	return 
end
HostNetworkSession._spawn_dropin_player = function (self, peer)
	managers.achievment:set_script_data("cant_touch_fail", true)
	peer.spawn_unit(peer, 0, true)
	managers.groupai:state():fill_criminal_team_with_AI(true)

	return 
end
HostNetworkSession.chk_server_joinable_state = function (self)
	for peer_id, peer in pairs(self._peers) do
		if peer.force_open_lobby_state(peer) then
			print("force-opening lobby for peer", peer_id)
			managers.network.matchmake:set_server_joinable(true)

			return 
		end
	end

	if tweak_data.max_players - 1 <= table.size(self._peers) then
		managers.network.matchmake:set_server_joinable(false)

		return 
	end

	local game_state_name = game_state_machine:last_queued_state_name()

	if BaseNetworkHandler._gamestate_filter.any_end_game[game_state_name] then
		managers.network.matchmake:set_server_joinable(false)

		return 
	end

	if not self._get_free_client_id(self) then
		managers.network.matchmake:set_server_joinable(false)

		return 
	end

	if not self._state:is_joinable(self._state_data) then
		managers.network.matchmake:set_server_joinable(false)

		return 
	end

	if NetworkManager.DROPIN_ENABLED then
		if BaseNetworkHandler._gamestate_filter.lobby[game_state_name] then
			managers.network.matchmake:set_server_joinable(true)

			return 
		elseif (managers.groupai and not managers.groupai:state():chk_allow_drop_in()) or not Global.game_settings.drop_in_allowed then
			managers.network.matchmake:set_server_joinable(false)

			return 
		end
	elseif not BaseNetworkHandler._gamestate_filter.lobby[game_state_name] then
		managers.network.matchmake:set_server_joinable(false)

		return 
	end

	managers.network.matchmake:set_server_joinable(true)

	return 
end
HostNetworkSession.on_load_complete = function (self, simulation)
	HostNetworkSession.super.on_load_complete(self, simulation)

	if not simulation then
		if Global.load_start_menu_lobby then
			managers.network.matchmake:set_server_state("in_lobby")
			self.chk_server_joinable_state(self)

			return 
		else
			managers.network.matchmake:set_server_state("in_game")
		end

		self.chk_server_joinable_state(self)

		if NetworkManager.DROPIN_ENABLED then
			for peer_id, peer in pairs(self._peers) do
				if peer.loaded(peer) then
					if not self._has_client(self, peer) then
						Network:add_client(peer.rpc(peer))
					end

					if not peer.synched(peer) then
						peer.set_expecting_pause_sequence(peer, true)

						local dropin_pause_ok = self.chk_initiate_dropin_pause(self, peer)

						if dropin_pause_ok then
							self.chk_drop_in_peer(self, peer)
						end
					end
				end
			end
		end

		self._reset_outfit_loading_status_request(self)
	end

	self._local_peer:set_synched(true)

	return 
end
HostNetworkSession.prepare_to_close = function (self, ...)
	HostNetworkSession.super.prepare_to_close(self, ...)
	self.set_state(self, "closing")

	return 
end
HostNetworkSession.chk_peer_handshakes_complete = function (self, peer)
	local peer_id = peer.id(peer)
	local peer_handshakes = peer.handshakes(peer)

	for other_peer_id, other_peer in pairs(self._peers) do
		if other_peer_id ~= peer_id and other_peer.loaded(other_peer) then
			if peer_handshakes[other_peer_id] ~= true then
				print("[HostNetworkSession:chk_peer_handshakes_complete]", peer_id, "is missing handshake for", other_peer_id)

				return false
			end

			if other_peer.handshakes(other_peer)[peer_id] ~= true then
				print("[HostNetworkSession:chk_peer_handshakes_complete]", peer_id, "is not known by", other_peer_id)

				return false
			end
		end
	end

	return true
end
HostNetworkSession.chk_all_handshakes_complete = function (self)
	for peer_id, peer in pairs(self._peers) do
		local peer_handshakes = peer.handshakes(peer)

		for other_peer_id, other_peer in pairs(self._peers) do
			if other_peer_id ~= peer_id and peer_handshakes[other_peer_id] ~= true then
				print("[HostNetworkSession:chk_all_handshakes_complete]", peer_id, "is missing handshake for", other_peer_id)

				return false
			end
		end
	end

	return true
end
HostNetworkSession.set_dropin_pause_request = function (self, peer, dropin_peer_id, state)
	if state == "asked" then
		local dropin_peer = self._peers[dropin_peer_id]

		peer.set_expecting_drop_in_pause_confirmation(peer, dropin_peer_id, state)
		peer.send_after_load(peer, "request_drop_in_pause", dropin_peer_id, dropin_peer.name(dropin_peer) or "unknown_peer", true)
	elseif state == "paused" then
		peer.set_expecting_drop_in_pause_confirmation(peer, dropin_peer_id, state)
	elseif not state then
		peer.set_expecting_drop_in_pause_confirmation(peer, dropin_peer_id, nil)
		peer.send_after_load(peer, "request_drop_in_pause", dropin_peer_id, "", false)
	else
		debug_pause("[HostNetworkSession:set_dropin_pause_request] unknown state", state)
	end

	return 
end
HostNetworkSession.chk_send_ready_to_unpause = function (self)
	for peer_id, peer in pairs(self._peers) do
		if peer.loaded(peer) and not peer.synched(peer) then
			return 
		end
	end

	for peer_id, peer in pairs(self._peers) do
		self.set_dropin_pause_request(self, peer, peer_id, false)
	end

	if self._local_peer:is_expecting_pause_confirmation(self._local_peer:id()) then
		self._local_peer:set_expecting_drop_in_pause_confirmation(self._local_peer:id(), nil)
		self.on_drop_in_pause_request_received(self, self._local_peer:id(), "", false)
	end

	return true
end
HostNetworkSession.set_state = function (self, name, enter_params)
	local state = self._STATES[name]
	local state_data = self._state_data

	if self._state then
		self._state:exit(state_data, name, enter_params)
	end

	state_data.name = name
	state_data.state = state
	self._state = state
	self._state_name = name

	state.enter(state, state_data, enter_params)

	return 
end
HostNetworkSession.on_re_open_lobby_request = function (self, peer, state)
	if state then
		peer.send(peer, "re_open_lobby_reply", true)
	end

	peer.set_force_open_lobby_state(peer, state)
	self.chk_server_joinable_state(self)

	return 
end
HostNetworkSession.all_peers_done_loading_outfits = function (self)
	if not self.are_all_peer_assets_loaded(self) then
		return false
	end

	for peer_id, peer in pairs(self._peers) do
		if peer.waiting_for_player_ready(peer) and not peer.other_peer_outfit_loaded_status(peer) then
			print("[HostNetworkSession:all_peers_done_loading_outfits] waiting for", peer_id, "to load outfits.")

			return false
		end
	end

	return true
end
HostNetworkSession.chk_request_peer_outfit_load_status = function (self)
	print("[HostNetworkSession:chk_request_peer_outfit_load_status]")
	Application:stack_dump()

	local outfit_versions_str = self._get_peer_outfit_versions_str(self)
	local req_id = self._increment_outfit_loading_status_request_id(self)

	for peer_id, peer in pairs(self._peers) do
		if peer.loaded(peer) then
			peer.set_other_peer_outfit_loaded_status(peer, false)
			print("[HostNetworkSession:request_peer_outfit_load_status] peer_id", peer_id, "req_id", req_id)
			peer.send(peer, "set_member_ready", self._local_peer:id(), req_id, 4, outfit_versions_str)
		end
	end

	return 
end
HostNetworkSession.on_peer_finished_loading_outfit = function (self, peer, request_id, outfit_versions_str_in)
	print("[HostNetworkSession:on_peer_finished_loading_outfit] peer:id()", peer.id(peer), "request_id", request_id, "self._peer_outfit_loaded_status_request_id", self._peer_outfit_loaded_status_request_id, "outfit_versions_str_in", outfit_versions_str_in, "self:_get_peer_outfit_versions_str()", self._get_peer_outfit_versions_str(self))

	if outfit_versions_str_in == "proactive" then
		managers.network:session():chk_request_peer_outfit_load_status()
	else
		if request_id ~= self._peer_outfit_loaded_status_request_id then
			return 
		end

		if outfit_versions_str_in ~= self._get_peer_outfit_versions_str(self) then
			return 
		end

		peer.set_other_peer_outfit_loaded_status(peer, true)
	end

	for _peer_id, _peer in pairs(self._peers) do
		self.chk_initiate_dropin_pause(self, _peer)
		self.chk_drop_in_peer(self, _peer)
		self.chk_spawn_member_unit(self, _peer, _peer_id)
	end

	return 
end
HostNetworkSession.on_set_member_ready = function (self, peer_id, ready, state_changed, from_network)
	HostNetworkSession.super.on_set_member_ready(self, peer_id, ready, state_changed, from_network)
	self.check_start_game_intro(self)

	if from_network then
		self.chk_request_peer_outfit_load_status(self)
	end

	return 
end
HostNetworkSession._increment_outfit_loading_status_request_id = function (self)
	if self._peer_outfit_loaded_status_request_id == 100 then
		self._peer_outfit_loaded_status_request_id = 0
	else
		self._peer_outfit_loaded_status_request_id = self._peer_outfit_loaded_status_request_id + 1
	end

	return self._peer_outfit_loaded_status_request_id
end
HostNetworkSession._reset_outfit_loading_status_request = function (self)
	self._increment_outfit_loading_status_request_id(self)

	for peer_id, peer in pairs(self._peers) do
		peer.set_other_peer_outfit_loaded_status(peer, false)
	end

	return 
end
HostNetworkSession.on_peer_outfit_loaded = function (self, peer)
	print("[HostNetworkSession:on_peer_outfit_loaded]", peer.id(peer))

	for _peer_id, _peer in pairs(self._peers) do
		self.chk_initiate_dropin_pause(self, _peer)
		self.chk_drop_in_peer(self, _peer)
		self.chk_spawn_member_unit(self, _peer, _peer_id)
	end

	return 
end
HostNetworkSession._inc_load_counter = function (self)
	if self._load_counter == self._LOAD_COUNTER_LIMITS[2] then
		self._load_counter = self._LOAD_COUNTER_LIMITS[1]
	else
		self._load_counter = self._load_counter + 1
	end

	return 
end

return 
