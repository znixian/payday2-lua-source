NetworkBaseExtension = NetworkBaseExtension or class()
NetworkBaseExtension.init = function (self, unit)
	self._unit = unit

	return 
end
NetworkBaseExtension.send = function (self, func, ...)
	if managers.network:session() then
		managers.network:session():send_to_peers_synched(func, self._unit, ...)
	end

	return 
end
NetworkBaseExtension.send_to_host = function (self, func, ...)
	if managers.network:session() then
		managers.network:session():send_to_host(func, self._unit, ...)
	end

	return 
end
NetworkBaseExtension.send_to_unit = function (self, params)
	if managers.network:session() then
		local peer = managers.network:session():peer_by_unit(self._unit)

		if peer then
			managers.network:session():send_to_peer(peer, unpack(params))
		end
	end

	return 
end
NetworkBaseExtension.peer = function (self)
	if managers.network:session() then
		return managers.network:session():peer_by_unit(self._unit)
	end

	return 
end

return 
