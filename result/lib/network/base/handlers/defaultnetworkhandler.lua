DefaultNetworkHandler = DefaultNetworkHandler or class()
DefaultNetworkHandler.init = function (self)
	return 
end
DefaultNetworkHandler.lost_peer = function (peer_ip)
	cat_print("multiplayer_base", "Lost Peer (DefaultNetworkHandler)")

	if managers.network:session() then
		local peer = managers.network:session():peer_by_ip(peer_ip.ip_at_index(peer_ip, 0))

		if peer then
			managers.network:session():on_peer_lost(peer, peer.id(peer))
		end
	end

	return 
end
DefaultNetworkHandler.lost_client = function (peer_ip)
	Application:error("[DefaultNetworkHandler] Lost client", peer_ip)

	if managers.network:session() then
		local peer = managers.network:session():peer_by_ip(peer_ip.ip_at_index(peer_ip, 0))

		if peer then
			managers.network:session():on_peer_lost(peer, peer.id(peer))
		end
	end

	return 
end
DefaultNetworkHandler.lost_server = function (peer_ip)
	Application:error("[DefaultNetworkHandler] Lost server", peer_ip)

	if managers.network:session() then
		local peer = managers.network:session():peer_by_ip(peer_ip.ip_at_index(peer_ip, 0))

		if peer then
			managers.network:session():on_peer_lost(peer, peer.id(peer))
		end
	end

	return 
end

return 
