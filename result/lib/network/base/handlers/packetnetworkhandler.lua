PacketNetworkHandler = PacketNetworkHandler or class(BaseNetworkHandler)
PacketNetworkHandler._set_shared_data = function (self, packet_id, target_peer, sender_peer, cb_id, arb_cb_id)
	self._shared_data.packet_id = packet_id
	self._shared_data.target_peer = target_peer
	self._shared_data.sender_peer = sender_peer
	self._shared_data.cb_id = cb_id
	self._shared_data.arb_cb_id = arb_cb_id

	return 
end
PacketNetworkHandler.forward_message_req_ack = function (self, packet_id, target_peer, sender_peer, cb_id)
	self._set_shared_data(self, packet_id, target_peer, sender_peer, cb_id, nil)

	return 
end
PacketNetworkHandler.message_req_ack = function (self, packet_id, sender_peer, cb_id)
	self._set_shared_data(self, packet_id, nil, sender_peer, cb_id, nil)

	return 
end
PacketNetworkHandler.forward_message_arb_req_ack = function (self, packet_id, target_peer, sender_peer, cb_id, arb_cb_id)
	self._set_shared_data(self, packet_id, target_peer, sender_peer, cb_id, arb_cb_id)

	return 
end
PacketNetworkHandler.message_arb_req_ack = function (self, packet_id, sender_peer, cb_id, arb_cb_id)
	self._set_shared_data(self, packet_id, nil, sender_peer, cb_id, arb_cb_id)

	return 
end
PacketNetworkHandler.message_arbitrate_answer = function (self, cb_id, answer, sender)
	self._do_cb(self, cb_id, answer)

	return 
end
PacketNetworkHandler.message_ack = function (self, target_peer, cb_id, sender)
	self._do_cb(self, cb_id)

	return 
end

return 
