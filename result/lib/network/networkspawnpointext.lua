NetworkSpawnPointExt = NetworkSpawnPointExt or class()
NetworkSpawnPointExt.init = function (self, unit)
	slot2 = managers.network and slot2

	return 
end
NetworkSpawnPointExt.get_data = function (self, unit)
	return {
		position = unit.position(unit),
		rotation = unit.rotation(unit)
	}
end
NetworkSpawnPointExt.destroy = function (self, unit)
	return 
end

return 
