HandState = HandState or class()
HandState.init = function (self, level)
	self._level = level or 0

	return 
end
HandState.level = function (self)
	return self._level
end
HandState.connnection_names = function (self)
	local names = {}

	if not self._connections then
		return names
	end

	for name, _ in pairs(self._connections) do
		table.insert(names, name)
	end

	return names
end
HandState.apply = function (self, hand, key_map)
	if not self._connections then
		return 
	end

	local hand_name = (hand == 1 and "r") or "l"

	for connection_name, connection_data in pairs(self._connections) do
		if connection_data.hand == hand or not connection_data.hand then
			for _, input in ipairs(connection_data.inputs) do
				key_map[input .. hand_name] = connection_name
			end
		end
	end

	return 
end

return 
