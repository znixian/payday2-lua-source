MenuGuiComponent = MenuGuiComponent or class()
MenuGuiComponent.init = function (self, ...)
	return 
end
MenuGuiComponent.close = function (self, ...)
	return 
end
MenuGuiComponent.refresh = function (self)
	return 
end
MenuGuiComponent.update = function (self, t, dt)
	return 
end
MenuGuiComponent.accept_input = function (self, accept)
	return 
end
MenuGuiComponent.input_focus = function (self)
	return 
end
MenuGuiComponent.scroll_up = function (self)
	return 
end
MenuGuiComponent.scroll_down = function (self)
	return 
end
MenuGuiComponent.move_up = function (self)
	return 
end
MenuGuiComponent.move_down = function (self)
	return 
end
MenuGuiComponent.move_left = function (self)
	return 
end
MenuGuiComponent.move_right = function (self)
	return 
end
MenuGuiComponent.next_page = function (self)
	return 
end
MenuGuiComponent.previous_page = function (self)
	return 
end
MenuGuiComponent.confirm_pressed = function (self)
	return 
end
MenuGuiComponent.back_pressed = function (self)
	return 
end
MenuGuiComponent.special_btn_pressed = function (self, ...)
	return 
end
MenuGuiComponent.mouse_pressed = function (self, o, button, x, y)
	return 
end
MenuGuiComponent.mouse_released = function (self, o, button, x, y)
	return 
end
MenuGuiComponent.mouse_wheel_up = function (self, x, y)
	return 
end
MenuGuiComponent.mouse_wheel_down = function (self, x, y)
	return 
end
MenuGuiComponent.mouse_clicked = function (self, o, button, x, y)
	return 
end
MenuGuiComponent.mouse_double_click = function (self, o, button, x, y)
	return 
end
MenuGuiComponent.mouse_moved = function (self, o, x, y)
	return 
end

return 
