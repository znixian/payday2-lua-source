require("lib/managers/menu/ExtendedUiElemets")

local massive_font = tweak_data.menu.pd2_massive_font
local large_font = tweak_data.menu.pd2_large_font
local medium_font = tweak_data.menu.pd2_medium_font
local small_font = tweak_data.menu.pd2_small_font
local tiny_font = tweak_data.menu.pd2_tiny_font
local massive_font_size = tweak_data.menu.pd2_massive_font_size
local large_font_size = tweak_data.menu.pd2_large_font_size
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font_size = tweak_data.menu.pd2_small_font_size
local tiny_font_size = tweak_data.menu.pd2_tiny_font_size
OnPressedTextButton = OnPressedTextButton or class(TextButton)
OnPressedTextButton.mouse_clicked = function (self)
	return 
end
OnPressedTextButton.mouse_pressed = function (self, button, x, y)
	if button == Idstring("0") and self._text:inside(x, y) then
		self._trigger()

		return true
	end

	return 
end
local AchievementRecentListItem = AchievementRecentListItem or class(GrowPanel)
AchievementRecentListItem.init = function (self, parent, item, black_bg)
	AchievementRecentListItem.super.init(self, parent, {
		border = 10,
		padding = 4,
		fixed_w = parent.w(parent)
	})

	self._visual = tweak_data.achievement.visual[item.id]
	local placer = self.placer(self)
	local texture, texture_rect = tweak_data.hud_icons:get_icon_or(self._visual.icon_id, "guis/dlcs/unfinished/textures/placeholder")
	local bitmap = placer.add_bottom(placer, self.bitmap(self, {
		w = 50,
		h = 50,
		texture = texture,
		texture_rect = texture_rect
	}))

	placer.add_right(placer, self.fine_text(self, {
		text_id = self._visual.name_id,
		font = medium_font,
		font_size = medium_font_size
	}))
	placer.add_bottom(placer, self.fine_text(self, {
		text = managers.localization:text("menu_achievement_unlock_date", {
			DATE = os.date("%d %b %Y %H:%M", item.unlock_time)
		}),
		font = tiny_font,
		font_size = tiny_font_size,
		color = tweak_data.screen_colors.achievement_grey
	}))

	local icons = self._visual.unlock_icons

	if icons then
		local fixed_w = self.w(self)/3
		local panel = GrowPanel:new(self, {
			fixed_w = fixed_w
		})
		local g_placer = panel.placer(panel)

		g_placer.set_start(g_placer, fixed_w)

		local ICON_SIZE = 32

		for _, data in pairs(icons) do
			local texture, rect = tweak_data.hud_icons:get_icon_or(data.texture, data.texture, data.texture_rect)
			local i = panel.fit_bitmap(panel, {
				texture = data.texture,
				texture_rect = rect,
				w = ICON_SIZE,
				h = ICON_SIZE,
				render_template = data.render_template
			})

			if i.h(i) < i.w(i)*1.5 then
				panel.make_bitmap_fit(i, ICON_SIZE*2, ICON_SIZE)
			end

			g_placer.add_left(g_placer, i)

			if i.x(i) < 0 then
				g_placer.new_row(g_placer)
				g_placer.add_left(g_placer, i)
			end
		end

		panel.set_right(panel, self.w(self) - 10)

		if self.h(self) < panel.h(panel) then
			self.set_h(self, panel.h(panel))
		else
			panel.set_center_y(panel, self.h(self)/2)
		end
	end

	if black_bg then
		self.rect(self, {
			layer = -1,
			color = Color.black:with_alpha(0.6)
		})
	end

	return 
end
AchievementRecentListGui = AchievementRecentListGui or class(ExtendedPanel)
AchievementRecentListGui.init = function (self, parent, list, back_callback)
	self._back_callback = back_callback

	AchievementRecentListGui.super.init(self, parent, {
		w = 650,
		layer = 50,
		h = 440,
		input = true
	})
	self.set_center(self, parent.w(parent)/2, parent.h(parent)/2)

	if not managers.menu:is_pc_controller() then
		self._legends = TextLegendsBar:new(parent, nil, {
			layer = self.layer(self)
		})

		self._legends:add_items({
			"menu_legend_back"
		})
		self._legends:set_world_rightbottom(parent.world_rightbottom(parent))
	end

	local border = 20
	local row_w = self.w(self) - border*2
	local placer = UiPlacer:new(border, border, 8, 8)

	placer.add_bottom(placer, self.fine_text(self, {
		text = managers.localization:text("menu_achievements_recent_unlocked", {
			COUNT = #list
		}),
		font = medium_font,
		font_size = medium_font_size
	}))
	placer.set_at(placer, self.w(self) - border - 6, nil)

	local safe = placer.add_left(placer, self.fit_bitmap(self, {
		w = 32,
		texture = "guis/dlcs/trk/textures/pd2/unlocked",
		h = 32
	}))
	local text = placer.add_left(placer, self.fine_text(self, {
		text_id = "menu_achievements_rewards",
		font = medium_font,
		font_size = medium_font_size,
		color = tweak_data.screen_colors.achievement_grey
	}))

	safe.set_center_y(safe, text.center_y(text))
	placer.new_row(placer)

	local scroll = placer.add_bottom(placer, ScrollableList:new(self, {
		scrollbar_padding = 5,
		input = true,
		w = row_w,
		h = self.h(self) - placer.most(placer).bottom - 48
	}))

	scroll.add_lines_and_static_down_indicator(scroll)

	self._scroll = scroll
	local s_placer = scroll.canvas(scroll):placer()
	local black_bg = true

	for _, v in pairs(list) do
		s_placer.add_bottom(s_placer, AchievementRecentListItem:new(scroll.canvas(scroll), v, black_bg))

		black_bg = not black_bg
	end

	if managers.menu:is_pc_controller() then
		local back = OnPressedTextButton:new(self, {
			input = true,
			text_id = "menu_back",
			font = medium_font,
			font_size = medium_font_size
		}, function ()
			self._back_callback()

			return 
		end)

		back.set_righttop(back, scroll.scroll_item(scroll):scroll_panel():right(), scroll.bottom(scroll))
		back.move(back, 0, 6)
	end

	local back_panel = self.panel(self, {
		layer = -2
	})

	back_panel.rect(back_panel, {
		color = Color(255, 15, 18, 24)/255
	})
	BoxGuiObject:new(back_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	self._back = back_panel

	return 
end
AchievementRecentListGui.close = function (self)
	self.remove_self(self)

	if self._legends then
		self._legends:remove_self()
	end

	return 
end
AchievementRecentListGui.update = function (self, ...)
	if not managers.menu:is_pc_controller() and self.allow_input(self) and (not managers.system_menu or not managers.system_menu:is_active() or not not managers.system_menu:is_closing()) then
		local axis_x, axis_y = managers.menu_component:get_left_controller_axis()

		if axis_y ~= 0 and self._scroll then
			self._scroll:perform_scroll(axis_y)
		end
	end

	return 
end
AchievementRecentListGui.back_pressed = function (self)
	self._back_callback()

	return true
end

return 
