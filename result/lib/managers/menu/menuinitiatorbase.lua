MenuInitiatorBase = MenuInitiatorBase or class()
MenuInitiatorBase.modify_node = function (self, original_node, data)
	return original_node
end
MenuInitiatorBase.create_divider = function (self, node, id, text_id, size, color)
	local params = {
		name = "divider_" .. id,
		no_text = not text_id,
		text_id = text_id,
		size = size or 8,
		color = color
	}
	local data_node = {
		type = "MenuItemDivider"
	}
	local new_item = node.create_item(node, data_node, params)

	node.add_item(node, new_item)

	return new_item
end
MenuInitiatorBase.create_toggle = function (self, node, params, index)
	local data_node = {
		{
			w = 24,
			y = 0,
			h = 24,
			s_y = 24,
			value = "on",
			s_w = 24,
			s_h = 24,
			s_x = 24,
			_meta = "option",
			icon = "guis/textures/menu_tickbox",
			x = 24,
			s_icon = "guis/textures/menu_tickbox"
		},
		{
			w = 24,
			y = 0,
			h = 24,
			s_y = 24,
			value = "off",
			s_w = 24,
			s_h = 24,
			s_x = 0,
			_meta = "option",
			icon = "guis/textures/menu_tickbox",
			x = 0,
			s_icon = "guis/textures/menu_tickbox"
		},
		type = "CoreMenuItemToggle.ItemToggle"
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_enabled(new_item, params.enabled)

	if index then
		node.insert_item(node, new_item, index)
	else
		node.add_item(node, new_item)
	end

	return new_item
end
MenuInitiatorBase.create_item = function (self, node, params)
	local data_node = {}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_enabled(new_item, params.enabled)
	node.add_item(node, new_item)

	return new_item
end
MenuInitiatorBase.create_multichoice = function (self, node, choices, params, index)
	if #choices == 0 then
		return 
	end

	local data_node = {
		type = "MenuItemMultiChoice"
	}

	for _, choice in ipairs(choices) do
		table.insert(data_node, choice)
	end

	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, choices[1].value)

	if index then
		node.insert_item(node, new_item, index)
	else
		node.add_item(node, new_item)
	end

	return new_item
end
MenuInitiatorBase.create_slider = function (self, node, params)
	local data_node = {
		type = "CoreMenuItemSlider.ItemSlider",
		show_value = params.show_value,
		min = params.min,
		max = params.max,
		step = params.step,
		show_value = params.show_value
	}
	local new_item = node.create_item(node, data_node, params)

	node.add_item(node, new_item)

	return new_item
end
MenuInitiatorBase.create_input = function (self, node, params)
	local data_node = {
		type = "MenuItemInput"
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_enabled(new_item, params.enabled)
	node.add_item(node, new_item)

	return new_item
end
MenuInitiatorBase.create_textbox = function (self, node, params)
	local data_node = {
		type = "MenuItemTextBox"
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_enabled(new_item, params.enabled)
	node.add_item(node, new_item)

	return new_item
end
MenuInitiatorBase.add_back_button = function (self, node)
	node.delete_item(node, "back")

	local params = {
		visible_callback = "is_pc_controller",
		name = "back",
		back = true,
		text_id = "menu_back",
		last_item = true,
		previous_node = true
	}
	local new_item = node.create_item(node, nil, params)

	node.add_item(node, new_item)

	return new_item
end

return 
