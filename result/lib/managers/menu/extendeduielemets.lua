require("lib/managers/menu/ExtendedPanel")
require("lib/managers/menu/UiPlacer")
require("lib/managers/menu/ScrollablePanel")

local massive_font = tweak_data.menu.pd2_massive_font
local large_font = tweak_data.menu.pd2_large_font
local medium_font = tweak_data.menu.pd2_medium_font
local small_font = tweak_data.menu.pd2_small_font
local tiny_font = tweak_data.menu.pd2_tiny_font
local massive_font_size = tweak_data.menu.pd2_massive_font_size
local large_font_size = tweak_data.menu.pd2_large_font_size
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font_size = tweak_data.menu.pd2_small_font_size
local tiny_font_size = tweak_data.menu.pd2_tiny_font_size

local function set_defaults(target, source)
	target = target or {}

	for k, v in pairs(source) do
		if target[k] == nil then
			target[k] = v
		end
	end

	return target
end

GrowPanel = GrowPanel or class(ExtendedPanel)
GrowPanel.init = function (self, parent, config)
	config = config or {}

	GrowPanel.super.init(self, parent, config)

	self._fixed_w = config.fixed_w and ((config.fixed_w ~= true and config.fixed_w) or config.w or parent.w(parent))
	self._fixed_h = config.fixed_h and ((config.fixed_h ~= true and config.fixed_h) or config.h or parent.h(parent))

	self.set_size(self, self._fixed_w or config.w or 0, self._fixed_h or config.h or 0)

	local padd_x = config.padding_x or config.padding or 0
	local padd_y = config.padding_y or config.padding or 0
	local bord_x = config.border_x or config.border or padd_x
	local bord_y = config.border_y or config.border or padd_y
	self._placer = ResizingPlacer:new(self, {
		border_x = bord_x,
		border_y = bord_y,
		padding_x = padd_x,
		padding_y = padd_y
	})

	return 
end
GrowPanel.clear = function (self)
	GrowPanel.super.clear(self)
	self._placer:clear()
	self._set_ensure_size(self, self._fixed_w or 0, self._fixed_h or 0)

	return 
end
GrowPanel.placer = function (self)
	return self._placer
end
GrowPanel.set_fixed_w = function (self, w)
	self._fixed_w = w

	self.set_w(self, w)

	return 
end
GrowPanel.set_fixed_h = function (self, h)
	self._fixed_h = h

	self.set_h(self, h)

	return 
end
GrowPanel._ensure_size = function (self, w, h)
	w = math.max(self._fixed_w or w, self.w(self))
	h = math.max(self._fixed_h or h, self.h(self))

	self._set_ensure_size(self, w, h)

	return 
end
GrowPanel._set_ensure_size = function (self, w, h)
	self.set_size(self, w, h)

	return 
end
GrowPanel.row_w = function (self)
	return self.w(self) - self._placer._border_padding_x*2
end
ScrollGrowPanel = ScrollGrowPanel or class(GrowPanel)
ScrollGrowPanel.init = function (self, scroll, config)
	config = set_defaults(config, {
		use_given = true
	})

	ScrollGrowPanel.super.init(self, scroll.canvas(scroll), config)

	self._scroll = scroll

	return 
end
ScrollGrowPanel.clear = function (self)
	self._scroll:set_canvas_size(0, 0)
	ScrollGrowPanel.super.clear(self)

	return 
end
ScrollGrowPanel._set_ensure_size = function (self, w, h)
	self._scroll:set_canvas_size(w, h)

	return 
end
local ScrollablePanelExt = ScrollablePanelExt or class(ScrollablePanel)
ScrollablePanelExt.init = function (self, parent_panel, name, data)
	self._scroll_padding = data and data.scrollbar_padding

	ScrollablePanelExt.super.init(self, parent_panel, name, data)

	return 
end
ScrollablePanelExt.scrollbar_padding = function (self)
	return self._scroll_padding or ScrollablePanelExt.super.scrollbar_padding(self)
end
ScrollablePanelExt.set_canvas_size = function (self, w, h)
	ScrollablePanelExt.super.set_canvas_size(self, w, h)

	if self.on_canvas_resized then
		self.on_canvas_resized(self)
	end

	return 
end
ScrollableList = ScrollableList or class(ExtendedPanel)
ScrollableList.init = function (self, parent, scroll_config, canvas_config)
	scroll_config = set_defaults(scroll_config, {
		ignore_up_indicator = true,
		padding = 0,
		ignore_down_indicator = true,
		layer = parent.layer(parent)
	})
	self._scroll = ScrollablePanelExt:new(parent, nil, scroll_config)
	scroll_config.use_given = true
	local add_as_input = scroll_config.input
	scroll_config.input = nil

	ScrollableList.super.init(self, self._scroll:panel(), scroll_config)

	local scrollbar_padding = scroll_config.scrollbar_padding or scroll_config.padding or self._scroll:padding()
	canvas_config = canvas_config or {}
	local add_canvas = canvas_config.input or add_as_input
	canvas_config.input = nil
	canvas_config.fixed_w = self._scroll._scroll_bar:left() - scrollbar_padding
	self._canvas = ScrollGrowPanel:new(self._scroll, canvas_config)

	self._scroll:set_canvas_size(canvas_config.w or canvas_config.fixed_w, canvas_config.h or 0)

	if add_canvas then
		self.add_input_component(self, self._canvas)
	end

	if add_as_input then
		if parent.add_input_component then
			parent.add_input_component(parent, self)
		else
			error("Trying to add as input component on parent that doesn't have a add_input_component function")
		end
	end

	return 
end
ScrollableList.resize = function (self, w, h)
	w = w or self.w(self)
	h = h or self.h(self)

	self._scroll:set_size(w, h)

	return 
end
ScrollableList.resize_canvas = function (self, w, h)
	w = w or self._canvas:w()
	h = h or self._canvas:h()

	self._canvas:_set_ensure_size(w, h)

	return 
end
ScrollableList.clear = function (self)
	self._canvas:clear()

	return 
end
ScrollableList.mouse_moved = function (self, button, x, y)
	local h, c = self._scroll:mouse_moved(button, x, y)

	if not self._scroll:panel():inside(x, y) then
		return h, c
	end

	local hover, cursor = ScrollableList.super.mouse_moved(self, button, x, y)
	hover = hover or h
	cursor = cursor or c

	return hover, cursor
end
ScrollableList.mouse_clicked = function (self, o, button, x, y)
	self._scroll:mouse_clicked(o, button, x, y)

	if not self._scroll:panel():inside(x, y) then
		return 
	end

	return ScrollableList.super.mouse_clicked(self, o, button, x, y)
end
ScrollableList.mouse_pressed = function (self, button, x, y)
	self._scroll:mouse_pressed(button, x, y)

	if not self._scroll:panel():inside(x, y) then
		return 
	end

	return ScrollableList.super.mouse_pressed(self, button, x, y)
end
ScrollableList.mouse_released = function (self, button, x, y)
	self._scroll:mouse_released(button, x, y)

	if not self._scroll:panel():inside(x, y) then
		return 
	end

	return ScrollableList.super.mouse_released(self, button, x, y)
end
ScrollableList.mouse_wheel_up = function (self, x, y)
	self._scroll:scroll(x, y, 1)

	if not self._scroll:panel():inside(x, y) then
		return 
	end

	return ScrollableList.super.mouse_wheel_up(self, x, y)
end
ScrollableList.mouse_wheel_down = function (self, x, y)
	self._scroll:scroll(x, y, -1)

	if not self._scroll:panel():inside(x, y) then
		return 
	end

	return ScrollableList.super.mouse_wheel_down(self, x, y)
end
ScrollableList.canvas = function (self)
	return self._canvas
end
ScrollableList.scroll_item = function (self)
	return self._scroll
end
ScrollableList.perform_scroll = function (self, val)
	self._scroll:perform_scroll(val, 1)

	return 
end
ScrollableList.scroll_to_show = function (self, top_or_item, bottom)
	local top = nil

	if type(top_or_item) == "table" and top_or_item.top and top_or_item.bottom then
		top = top_or_item.top(top_or_item)
		bottom = top_or_item.bottom(top_or_item)
	else
		top = top_or_item
		bottom = top_or_item
	end

	bottom = bottom - self.h(self)
	local cur = -self._canvas:y()

	print("scroll", cur, top)

	if top < cur then
		print("scroll", top)
		self._scroll:scroll_to(top)
	elseif cur < bottom then
		self._scroll:scroll_to(bottom)
		print("scroll", bottom)
	end

	return 
end
ScrollableList.scroll_to_show_item_at_world = function (self, item, world_y)
	self._scroll:perform_scroll(world_y - item.world_y(item), 1)

	return 
end
ScrollableList.add_lines_and_static_down_indicator = function (self)
	local box = BoxGuiObject:new(self.scroll_item(self):scroll_panel(), {
		w = self.canvas(self):w(),
		sides = {
			1,
			1,
			2,
			0
		}
	})
	local down_no_scroll = BoxGuiObject:new(box._panel, {
		sides = {
			0,
			0,
			0,
			1
		}
	})
	local down_scroll = BoxGuiObject:new(box._panel, {
		sides = {
			0,
			0,
			0,
			2
		}
	})

	local function update_down_indicator()
		local indicate = self:scroll_item()._scroll_bar:visible()

		down_no_scroll:set_visible(not indicate)
		down_scroll:set_visible(indicate)

		return 
	end

	update_down_indicator()

	self._scroll.on_canvas_resized = update_down_indicator

	return 
end
ScrollItemList = ScrollItemList or class(ScrollableList)
ScrollItemList.init = function (self, parent, scroll_config, canvas_config)
	ScrollItemList.super.init(self, parent, scroll_config, canvas_config)

	self._input_focus = scroll_config.input_focus
	self._all_items = {}
	self._current_items = {}

	return 
end
ScrollItemList.clear = function (self)
	self._all_items = {}
	self._current_items = {}
	self._selected_item = nil

	ScrollItemList.super.clear(self)

	return 
end
ScrollItemList.all_items = function (self)
	return self._all_items
end
ScrollItemList.items = function (self)
	return self._current_items
end
ScrollItemList.set_input_focus = function (self, state)
	self._input_focus = state

	return 
end
ScrollItemList.input_focus = function (self)
	return self.allow_input(self) and self._input_focus
end
ScrollItemList.mouse_moved = function (self, button, x, y)
	if not self._scroll:panel():inside(x, y) then
		return 
	end

	for k, v in pairs(self._current_items) do
		if v.inside(v, x, y) then
			if self._selected_item ~= v then
				self.select_item(self, v)
			end

			break
		end
	end

	return ScrollItemList.super.mouse_moved(self, button, x, y)
end
ScrollItemList._on_selected_changed = function (self, selected)
	if self._on_selected_callback then
		self._on_selected_callback(selected)
	end

	return 
end
ScrollItemList.set_selected_callback = function (self, func)
	self._on_selected_callback = func

	return 
end
ScrollItemList.selected_item = function (self)
	return self._selected_item
end
ScrollItemList.select_index = function (self, index)
	self.select_item(self, self._current_items[index])

	return 
end
ScrollItemList.move_selection = function (self, move)
	if not self._selected_item then
		self.select_index(self, 1)
	else
		local index = table.index_of(self._current_items, self._selected_item)
		local new_index = index + move
		new_index = math.clamp(new_index, 1, #self._current_items)

		self.select_index(self, new_index)
	end

	if self._selected_item then
		self.scroll_to_show(self, self._selected_item)
	end

	return 
end
ScrollItemList.select_item = function (self, item)
	if item == self._selected_item then
		return 
	end

	if self._selected_item then
		self._selected_item:set_selected(false)

		self._selected_item = nil
	end

	if item then
		self._selected_item = item

		item.set_selected(item, true)
	end

	self._on_selected_changed(self, item)

	return 
end
ScrollItemList.add_item = function (self, item, force_visible)
	if force_visible ~= nil then
		item.set_visible(item, force_visible)
	end

	if item.visible(item) then
		self._canvas:placer():add_row(item)
		table.insert(self._current_items, item)
	end

	table.insert(self._all_items, item)

	return item
end
ScrollItemList.move_up = function (self)
	if not self.input_focus(self) then
		return 
	end

	self.move_selection(self, -1)

	return true
end
ScrollItemList.move_down = function (self)
	if not self.input_focus(self) then
		return 
	end

	self.move_selection(self, 1)

	return true
end
ScrollItemList.sort_items = function (self, sort_function, mod_placer, keep_selection)
	table.sort(self._current_items, sort_function)
	table.sort(self._all_items, sort_function)

	local placer = self._canvas:placer()

	placer.clear(placer)

	if mod_placer then
		mod_placer(placer)
	end

	local w_y = self._selected_item and self._selected_item:world_y()

	for _, item in ipairs(self._current_items) do
		placer.add_row(placer, item)
	end

	if not keep_selection and self._selected_item then
		self.select_index(self, 1)
	elseif self._selected_item and w_y then
		self.scroll_to_show_item_at_world(self, self._selected_item, w_y)
	end

	return 
end
ScrollItemList.filter_items = function (self, filter_function, mod_start, keep_selection)
	local placer = self._canvas:placer()

	placer.clear(placer)
	self._canvas:set_size(0, 0)

	if mod_start then
		mod_start(placer, self._canvas)
	end

	local w_y = self._selected_item and self._selected_item:world_y()
	self._current_items = {}

	for _, item in ipairs(self._all_items) do
		if filter_function(item) then
			item.set_visible(item, true)
			placer.add_row(placer, item)
			table.insert(self._current_items, item)
		else
			item.set_visible(item, false)
		end
	end

	if (self._selected_item and not self._selected_item:visible()) or not keep_selection then
		self.select_index(self, 1)
	elseif self._selected_item and w_y then
		self.scroll_to_show_item_at_world(self, self._selected_item, w_y)
	end

	return 
end
ListItem = ListItem or class(ExtendedPanel)
ListItem.init = function (self, ...)
	ListItem.super.init(self, ...)

	return 
end
ListItem._selected_changed = function (self, state)
	if self._select_panel then
		self._select_panel:set_visible(state)
	end

	return 
end
ListItem.set_selected = function (self, state)
	if self._selected == state then
		return 
	end

	self._selected = state

	self._selected_changed(self, state)

	local _ = state and managers.menu_component:post_event("highlight")

	return 
end
BaseButton = BaseButton or class(ExtendedPanel)
BaseButton.init = function (self, parent, config)
	config = set_defaults(config, {
		input = true
	})

	BaseButton.super.init(self, parent, config)

	self._binding = config.binding and Idstring(config.binding)
	self._enabled = (config.enabled == nil and true) or config.enabled
	self._hover = false

	return 
end
BaseButton.set_enabled = function (self, state)
	if self._enabled == state then
		return 
	end

	self._enabled = state

	self._enabled_changed(self, state)

	return 
end
BaseButton._enabled_changed = function (self, state)
	return 
end
BaseButton._hover_changed = function (self, state)
	return 
end
BaseButton._trigger = function (self)
	return 
end
BaseButton.allow_input = function (self)
	return self._enabled and BaseButton.super.allow_input(self)
end
BaseButton.mouse_moved = function (self, o, x, y)
	local hover = self.inside(self, x, y)

	if self._hover ~= hover then
		self._hover = hover

		self._hover_changed(self, hover)
	end

	if hover then
		return true, "link"
	end

	return 
end
BaseButton.mouse_clicked = function (self, o, button, x, y)
	if button == Idstring("0") and self.inside(self, x, y) then
		self._trigger(self)

		return true
	end

	return 
end
BaseButton.special_btn_pressed = function (self, button)
	if button == self._binding then
		self._trigger(self)

		return true
	end

	return 
end
TextButton = TextButton or class(BaseButton)
TextButton.init = function (self, parent, text_config, func, panel_config)
	panel_config = set_defaults(panel_config, {
		binding = text_config.binding
	})

	TextButton.super.init(self, parent, panel_config)

	self._normal_color = text_config.normal_color or text_config.color or tweak_data.screen_colors.button_stage_3
	self._hover_color = text_config.hover_color or text_config.color or tweak_data.screen_colors.button_stage_2
	self._disabled_color = text_config.disabled_color or tweak_data.menu.default_disabled_text_color
	text_config.color = self._normal_color

	if text_config.text_id and text_config.binding then
		text_config.text = managers.localization:text(text_config.text_id, {
			MY_BTN = managers.localization:btn_macro(text_config.binding, true)
		})
		text_config.text_id = nil
	end

	self._text = self.fine_text(self, text_config)
	self._trigger = func or function ()
		return 
	end
	self._is_fixed = panel_config.fixed_size

	if not panel_config.fixed_size then
		self._panel:set_size(self._text:rightbottom())
	end

	self._enabled_changed(self, self._enabled)

	return 
end
TextButton._enabled_changed = function (self, state)
	self._text:set_color((state and self._normal_color) or self._disabled_color)

	return 
end
TextButton._hover_changed = function (self, hover)
	self._text:set_color((hover and self._hover_color) or self._normal_color)

	return 
end
TextButton.set_text = function (self, text)
	self._text:set_text(text)
	self.make_fine_text(self._text)

	if not self._is_fixed then
		self.set_size(self, self._text:rightbottom())
	end

	return 
end
IconButton = IconButton or class(BaseButton)
IconButton.init = function (self, parent, icon_config, func)
	IconButton.super.init(self, parent, {
		binding = icon_config.binding
	}, func)

	self._select_panel = ExtendedPanel:new(self)
	self._normal_color = icon_config.normal_color or icon_config.color
	self._hover_color = icon_config.hover_color or icon_config.color or self._normal_color
	self._disabled_color = icon_config.disabled_color or self._normal_color
	self._button = self._select_panel:bitmap(icon_config)

	self._set_color(self, self._normal_color)

	self._trigger = func or function ()
		return 
	end

	self.set_size(self, self._button:size())

	return 
end
IconButton._set_color = function (self, col)
	if col then
		self._button:set_color(col)
	end

	return 
end
IconButton.icon = function (self)
	return self._button
end
IconButton._hover_changed = function (self, hover)
	self._set_color(self, (hover and self._hover_color) or self._normal_color)

	return 
end
IconButton._enabled_changed = function (self, state)
	self._set_color(self, (state and self._normal_color) or self._disabled_color)

	return 
end
ProgressBar = ProgressBar or class(ExtendedPanel)
ProgressBar.init = function (self, parent, config, progress)
	ProgressBar.super.init(self, parent, config)

	config = config or {}
	self._max = config.max or 1
	self._back_color = (config.back_config and config.back_config.color) or config.back_color or Color.black
	self._progress_color = (config.progress_config and config.progress_config.color) or config.progress_color or Color.white
	self._back = self.rect(self, config.back_config or {
		color = self._back_color
	})
	self._progress = self.rect(self, config.progress_config or {
		w = 0,
		color = self._progress_color
	})

	if progress then
		self.set_progress(self, progress)
	end

	return 
end
ProgressBar.max = function (self)
	return self._max
end
ProgressBar.set_progress = function (self, v)
	v = math.clamp(v, 0, self._max)

	self._progress:set_w((self._back:w()*v)/self._max)

	return v
end
ProgressBar.set_max = function (self, v, dont_scale_current)
	local current = (dont_scale_current and self._progress:w()/self._back:w()*self._max) or self._progress:w()/self._back:w()*v
	self._max = v

	self.set_progress(self, current)

	return 
end
TextProgressBar = TextProgressBar or class(ProgressBar)
TextProgressBar.init = function (self, parent, config, text_config, progress)
	TextProgressBar.super.init(self, parent, config)

	text_config = text_config or {}
	self._on_back_color = text_config.on_back_color or text_config.color or self._progress_color
	self._on_progress_color = text_config.on_progress_color or text_config.color or self._back_color

	if config.percent ~= nil then
		self._percentage = config.percent
	else
		self._percentage = not config.max
	end

	text_config.text = text_config.text or (self._percentage and " 0% ") or string.format(" 0 / %d ", self._max)
	text_config.color = text_config.color or self._on_back_color
	text_config.layer = text_config.layer or self._progress:layer() + 1
	self._text = self.fine_text(self, text_config)

	self._text:set_center_y(self._back:center_y())

	if progress then
		self.set_progress(self, progress)
	end

	return 
end
TextProgressBar.set_progress = function (self, v)
	v = TextProgressBar.super.set_progress(self, v)
	local at = self._progress:right()
	local max = self._back:right()

	if self._percentage then
		local num = v*100

		self._text:set_text(string.format(" %d%% ", num))
	else
		self._text:set_text(string.format(" %d / %d ", v, self._max))
	end

	self.make_fine_text(self._text)
	self._text:set_left(at)

	local col = self._on_back_color

	if max < self._text:right() then
		col = self._on_progress_color

		self._text:set_right(at)
	end

	self._text:set_color(col)

	return 
end
SpecialButtonBinding = SpecialButtonBinding or class()
SpecialButtonBinding.init = function (self, binding, func, add_to_panel)
	self._binding = Idstring(binding)
	self._on_trigger = func or function ()
		return 
	end
	self._enabled = true

	if add_to_panel then
		add_to_panel.add_input_component(add_to_panel, self)
	end

	return 
end
SpecialButtonBinding.allow_input = function (self)
	return self._enabled
end
SpecialButtonBinding.special_btn_pressed = function (self, button)
	if button == self._binding then
		self._on_trigger()

		return true
	end

	return 
end
SpecialButtonBinding.set_enabled = function (self, state)
	self._enabled = state

	return 
end
ButtonLegendsBar = ButtonLegendsBar or class(GrowPanel)
ButtonLegendsBar.PADDING = 10
ButtonLegendsBar.init = function (self, panel, config, panel_config)
	panel_config = set_defaults(panel_config, {
		border = 0,
		padding_y = 0,
		input = true,
		w = panel.w(panel),
		padding = self.PADDING
	})
	panel_config = set_defaults(panel_config, {
		fixed_w = panel_config.w
	})

	ButtonLegendsBar.super.init(self, panel, panel_config)

	self._text_config = set_defaults(config, {
		font = small_font,
		font_size = small_font_size
	})
	self._wrap = panel_config.wrap
	self._legends_only = self._text_config.no_buttons or not panel_config.input or not managers.menu:is_pc_controller()
	self._items = {}
	self._lookup = {}

	return 
end
ButtonLegendsBar.add_item = function (self, data, id, dont_update)
	if type(data) == "string" then
		local text = (managers.localization:exists(data) and managers.localization:to_upper_text(data)) or data

		table.insert(self._items, self._create_legend(self, nil, text))
	else
		macro_name = data.macro_name or "MY_BTN"

		if not data.text then
			local text = managers.localization:to_upper_text(data.text_id, {
				[macro_name] = managers.localization:btn_macro(data.binding, true) or ""
			})
		end

		local item = nil

		if data.func and not self._legends_only then
			table.insert(self._items, self._create_btn(self, data, text))
		else
			table.insert(self._items, self._create_legend(self, data, text))
		end
	end

	if id or data.id then
		self._lookup[id or data.id] = #self._items
	end

	if dont_update or data.enabled == false then
		return 
	end

	self._update_items(self)

	return 
end
ButtonLegendsBar._create_btn = function (self, data, text)
	local config = clone(self._text_config)
	config.text = text
	config.binding = data.binding
	local button = TextButton:new(self, config, data.func)

	button.set_visible(button, false)

	local item = {
		button = true,
		item = button,
		enabled = data.enabled ~= false,
		force_break = data.force_break
	}

	return item
end
ButtonLegendsBar._create_legend = function (self, data, text)
	data = data or {}
	local config = data.config or clone(self._text_config)
	config.text = text
	local text = self.fine_text(self, config)

	text.set_visible(text, false)

	local item = {
		text = true,
		item = text,
		enabled = data.enabled ~= false,
		force_break = data.force_break
	}

	if data.binding and data.func then
		item.listener = SpecialButtonBinding:new(data.binding, data.func, self)
	end

	return item
end
ButtonLegendsBar.add_items = function (self, list)
	for k, v in pairs(list) do
		self.add_item(self, v, nil, true)
	end

	self._update_items(self)

	return 
end
ButtonLegendsBar.set_item_enabled = function (self, id_or_pos, state)
	local id = (type(id_or_pos) == "number" and id_or_pos) or self._lookup[id_or_pos]
	local data = self._items[id]

	if data and data.enabled ~= state then
		data.enabled = state

		self._update_items(self)
	end

	return 
end
ButtonLegendsBar._update_items = function (self)
	local placer = self.placer(self)

	placer.clear(placer)
	placer.set_start(placer, self.w(self), 0)
	self.set_size(self, 0, 0)

	for _, v in pairs(self._items) do
		v.item:set_visible(v.enabled)

		if v.force_break and not placer.is_first_in_row(placer) then
			placer.new_row(placer)
		end

		if v.enabled then
			placer.add_left(placer, v.item)

			if self._wrap and v.item:left() < 0 and not placer.is_first_in_row(placer) then
				placer.new_row(placer)
				placer.add_left(placer, v.item)
			end
		end

		if v.listener then
			v.listener:set_enabled(v.enabled)
		end
	end

	return 
end
TextLegendsBar = TextLegendsBar or class(ButtonLegendsBar)
TextLegendsBar.SEPERATOR = "  |  "
TextLegendsBar.init = function (self, panel, config, panel_config)
	TextLegendsBar.super.init(self, panel, config, panel_config)

	self._text_config = set_defaults(self._text_config, {
		text = " ",
		align = "right",
		keep_w = true
	})
	self._seperator = self._text_config.seperator or TextLegendsBar.SEPERATOR
	self._lines = {}

	return 
end
TextLegendsBar._create_btn = function (self, data, text)
	return self._create_legend(self, data, text)
end
TextLegendsBar._create_legend = function (self, data, text)
	data = data or {}
	local item = {
		item = text,
		enabled = data.enabled ~= false,
		force_break = data.force_break
	}

	if data.binding and data.func then
		item.listener = SpecialButtonBinding:new(data.binding, data.func, self)
	end

	return item
end
TextLegendsBar._update_items = function (self)
	for _, v in pairs(self._lines) do
		self.remove(self, v)
	end

	self._lines = {}
	local placer = self.placer(self)

	placer.clear(placer)
	placer.set_start(placer, self.w(self), 0)
	self.set_size(self, self.w(self), 0)

	local function complete_line(text_item)
		self.make_fine_text(text_item, true)
		placer:add_left(text_item)
		placer:new_row()
		self:set_h(text_item.bottom(text_item))
		table.insert(self._lines, text_item)

		return 
	end

	local text_item = nil

	for _, v in pairs(self._items) do
		if v.force_break then
			text_item = nil
		end

		if v.enabled then
			if text_item then
				local str = text_item.text(text_item)

				text_item.set_text(text_item, v.item .. self._seperator .. str)

				local _, _, w, _ = text_item.text_rect(text_item)

				if self._wrap and self.w(self) < w then
					text_item.set_text(text_item, str)
					complete_line(text_item)

					text_item = nil
				end
			end

			if not text_item then
				text_item = self.text(self, self._text_config)

				text_item.set_text(text_item, v.item)
			end
		end

		if v.listener then
			v.listener:set_enabled(v.enabled)
		end
	end

	if text_item then
		complete_line(text_item)
	end

	return 
end

return 
