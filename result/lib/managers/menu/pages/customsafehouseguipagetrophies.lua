local massive_font = tweak_data.menu.pd2_massive_font
local large_font = tweak_data.menu.pd2_large_font
local medium_font = tweak_data.menu.pd2_medium_font
local small_font = tweak_data.menu.pd2_small_font
local massive_font_size = tweak_data.menu.pd2_massive_font_size
local large_font_size = tweak_data.menu.pd2_large_font_size
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font_size = tweak_data.menu.pd2_small_font_size
local PANEL_PADDING = 10
local REWARD_SIZE = 100
local MAX_REWARDS_DISPLAYED = 2
CustomSafehouseGuiPageTrophies = CustomSafehouseGuiPageTrophies or class(CustomSafehouseGuiPage)
CustomSafehouseGuiPageTrophies.init = function (self, page_id, page_panel, fullscreen_panel, gui)
	CustomSafehouseGuiPageTrophies.super.init(self, page_id, page_panel, fullscreen_panel, gui)

	self.make_fine_text = BlackMarketGui.make_fine_text
	self._scrollable_panels = {}

	self._setup_trophies_counter(self)
	self._setup_trophies_info(self)
	self._setup_trophies_list(self)

	return 
end
CustomSafehouseGuiPageTrophies._setup_trophies_list = function (self)
	self._trophies = {}
	local scroll = ScrollablePanel:new(self.panel(self), "TrophiesPanel", {
		padding = 0
	})

	BoxGuiObject:new(scroll.panel(scroll), {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	self._trophies_scroll = scroll

	table.insert(self._scrollable_panels, scroll)

	local trophies = {}

	for idx, trophy in ipairs(managers.custom_safehouse:trophies()) do
		if (not trophy.secret or trophy.completed) and not trophy.hidden_in_list then
			table.insert(trophies, trophy)
		end
	end

	table.sort(trophies, function (a, b)
		return managers.localization:text(a.name_id) < managers.localization:text(b.name_id)
	end)

	for idx, trophy in ipairs(trophies) do
		local trophy_btn = CustomSafehouseGuiTrophyItem:new(scroll.canvas(scroll), trophy, 0, idx)

		table.insert(self._trophies, trophy_btn)
	end

	table.sort(self._trophies, function (a, b)
		return a.priority(a) < b.priority(b)
	end)

	local canvas_h = 0
	slot4 = ipairs
	slot5 = self._trophies or {}

	for idx, trophy in slot4(slot5) do
		trophy.set_position(trophy, idx)
		trophy.link(trophy, self._trophies[idx - 1], self._trophies[idx + 1])

		canvas_h = math.max(canvas_h, trophy.bottom(trophy))
	end

	scroll.set_canvas_size(scroll, nil, canvas_h)

	if 0 < #self._trophies then
		self._set_selected(self, self._trophies[1], true)
	end

	return 
end
CustomSafehouseGuiPageTrophies._setup_trophies_info = function (self)
	local buttons_panel = self.info_panel(self):panel({
		name = "buttons_panel"
	})
	local trophy_panel = self.info_panel(self):panel({
		name = "trophy_panel"
	})
	local buttons = {}
	local button_panel_h = nil

	if not Global.game_settings.is_playing then
		if not managers.menu:is_pc_controller() then
			table.insert(buttons, {
				btn = "BTN_A",
				name_id = "menu_trophy_change_display_to_off"
			})
		end

		table.insert(buttons, {
			btn = "BTN_X",
			name_id = "menu_trophy_display_all",
			pc_btn = "menu_remove_item",
			callback = callback(self, self, "_show_all_trophies")
		})
		table.insert(buttons, {
			btn = "BTN_Y",
			name_id = "menu_trophy_hide_all",
			pc_btn = "menu_modify_item",
			callback = callback(self, self, "_hide_all_trophies")
		})

		button_panel_h = #buttons*medium_font_size + 10
	else
		button_panel_h = 0
	end

	self._buttons = {}
	self._controllers_pc_mapping = {}
	self._controllers_mapping = {}
	local btn_x = 10

	for idx, btn_data in pairs(buttons) do
		local new_button = CustomSafehouseGuiButtonItem:new(buttons_panel, btn_data, btn_x, idx)
		self._buttons[idx] = new_button

		if btn_data.pc_btn then
			self._controllers_mapping[btn_data.pc_btn:key()] = new_button
		end
	end

	if 0 < button_panel_h then
		trophy_panel.set_h(trophy_panel, self.info_panel(self):h() - button_panel_h - PANEL_PADDING)
	else
		trophy_panel.set_h(trophy_panel, self.info_panel(self):h())
	end

	buttons_panel.set_h(buttons_panel, button_panel_h)
	buttons_panel.set_bottom(buttons_panel, self.info_panel(self):bottom())

	self._buttons_box_panel = BoxGuiObject:new(buttons_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})
	local scroll = ScrollablePanel:new(trophy_panel, "TrophyInfoPanel")

	scroll.on_canvas_updated_callback(scroll, callback(self, self, "update_info_panel_width"))

	self._trophy_box_panel = BoxGuiObject:new(scroll.panel(scroll), {
		sides = {
			1,
			1,
			1,
			1
		}
	})
	self._info_scroll = scroll

	table.insert(self._scrollable_panels, scroll)

	local trophy_title = scroll.canvas(scroll):text({
		name = "TitleText",
		blend_mode = "add",
		align = "left",
		vertical = "top",
		valign = "top",
		halign = "left",
		layer = 1,
		font_size = medium_font_size,
		font = medium_font,
		color = tweak_data.screen_colors.title,
		text = utf8.to_upper("Trophies"),
		w = scroll.canvas(scroll):w(),
		h = medium_font_size
	})
	local image_panel = scroll.canvas(scroll):panel({
		layer = 10,
		name = "TrophyImagePanel"
	})

	image_panel.set_w(image_panel, scroll.canvas(scroll):w())
	image_panel.set_h(image_panel, image_panel.w(image_panel)/2)
	image_panel.set_top(image_panel, trophy_title.bottom(trophy_title) + PANEL_PADDING)

	local trophy_image = image_panel.bitmap(image_panel, {
		layer = 40,
		name = "TrophyImage",
		texture_rect = {
			0,
			0,
			512,
			256
		},
		w = image_panel.w(image_panel),
		h = image_panel.h(image_panel)
	})
	local image_scanlines = image_panel.bitmap(image_panel, {
		texture = "guis/dlcs/chill/textures/pd2/rooms/safehouse_room_preview_effect",
		name = "TrophyImageScanlines",
		wrap_mode = "wrap",
		layer = 50,
		texture_rect = {
			0,
			0,
			512,
			512
		},
		w = image_panel.w(image_panel),
		h = image_panel.h(image_panel)*4,
		y = image_panel.h(image_panel)*2*-1
	})
	self._scanline_effect = image_scanlines
	self._image_outline = BoxGuiObject:new(image_panel, {
		sides = {
			2,
			2,
			2,
			2
		}
	})

	self._image_outline:set_color(Color(0.2, 1, 1, 1))
	self._image_outline:set_blend_mode("add")

	local complete_banner = scroll.canvas(scroll):panel({
		name = "CompleteBannerPanel",
		h = small_font_size
	})

	complete_banner.set_top(complete_banner, image_panel.bottom(image_panel) + PANEL_PADDING)
	complete_banner.rect(complete_banner, {
		name = "CompleteBannerFill",
		alpha = 0.4,
		color = tweak_data.screen_colors.challenge_completed_color
	})

	local complete_text = complete_banner.text(complete_banner, {
		blend_mode = "add",
		name = "CompleteText",
		vertical = "top",
		valign = "scale",
		align = "center",
		halign = "scale",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.challenge_completed_color:with_alpha(0.8),
		text = managers.localization:to_upper_text("menu_trophy_displayed")
	})
	local desc_text = scroll.canvas(scroll):text({
		name = "DescText",
		blend_mode = "add",
		wrap = true,
		align = "left",
		word_wrap = true,
		vertical = "top",
		valign = "top",
		halign = "left",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.title,
		text = managers.localization:text("menu_cs_daily_available"),
		w = scroll.canvas(scroll):w()
	})

	desc_text.set_top(desc_text, image_panel.bottom(image_panel) + PANEL_PADDING)
	self.make_fine_text(self, desc_text)

	local unlock_text = scroll.canvas(scroll):text({
		name = "ObjectiveHeader",
		blend_mode = "add",
		vertical = "top",
		align = "left",
		valign = "top",
		halign = "left",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.challenge_title,
		text = utf8.to_upper(managers.localization:text("menu_unlock_condition")),
		w = scroll.canvas(scroll):w()
	})

	self.make_fine_text(self, unlock_text)
	unlock_text.set_top(unlock_text, desc_text.bottom(desc_text) + PANEL_PADDING)

	local objective_text = scroll.canvas(scroll):text({
		name = "ObjectiveText",
		blend_mode = "add",
		wrap = true,
		align = "left",
		word_wrap = true,
		vertical = "top",
		valign = "top",
		halign = "left",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.title,
		text = managers.localization:text("menu_cs_daily_available"),
		w = scroll.canvas(scroll):w()
	})

	objective_text.set_top(objective_text, unlock_text.bottom(unlock_text))
	self.make_fine_text(self, objective_text)

	local reward_header = scroll.canvas(scroll):text({
		name = "RewardHeader",
		blend_mode = "add",
		vertical = "top",
		align = "left",
		valign = "top",
		halign = "left",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.challenge_title,
		text = managers.localization:to_upper_text("menu_reward"),
		w = scroll.canvas(scroll):w()
	})

	self.make_fine_text(self, reward_header)
	reward_header.set_top(reward_header, objective_text.bottom(objective_text) + PANEL_PADDING)

	local reward_text = scroll.canvas(scroll):text({
		name = "RewardText",
		blend_mode = "add",
		wrap = true,
		align = "left",
		word_wrap = true,
		vertical = "top",
		valign = "top",
		halign = "left",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.title,
		text = managers.localization:text("bm_cs_continental_coin_cost", {
			cost = tweak_data.safehouse.rewards.challenge
		}),
		w = scroll.canvas(scroll):w()
	})

	reward_text.set_top(reward_text, reward_header.bottom(reward_header))
	self.make_fine_text(self, reward_text)

	local progress_header = scroll.canvas(scroll):text({
		name = "ProgressHeader",
		blend_mode = "add",
		align = "left",
		vertical = "top",
		valign = "top",
		halign = "left",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.challenge_title,
		text = utf8.to_upper(managers.localization:text("menu_unlock_progress")),
		w = scroll.canvas(scroll):w(),
		h = small_font_size
	})

	self.make_fine_text(self, progress_header)
	progress_header.set_top(progress_header, objective_text.bottom(objective_text) + PANEL_PADDING)
	scroll.update_canvas_size(scroll)

	return 
end
CustomSafehouseGuiPageTrophies._setup_trophies_counter = function (self)
	local total = 0
	local completed = 0

	for _, trophy in ipairs(managers.custom_safehouse:trophies()) do
		if not trophy.hidden_in_list then
			total = total + 1

			if trophy.completed then
				completed = completed + 1
			end
		end
	end

	local text = managers.localization:to_upper_text("menu_cs_trophy_counter", {
		total = total,
		completed = completed
	})
	self._trophy_counter = self._gui._panel:text({
		visible = false,
		text = text,
		font = medium_font,
		font_size = medium_font_size,
		color = tweak_data.screen_colors.text
	})

	self.make_fine_text(self, self._trophy_counter)
	self._trophy_counter:set_right(self._gui._panel:w())

	return 
end
CustomSafehouseGuiPageTrophies.set_active = function (self, active)
	self._trophy_counter:set_visible(active)

	return CustomSafehouseGuiPageTrophies.super.set_active(self, active)
end
CustomSafehouseGuiPageTrophies.update_info_panel_width = function (self, new_width)
	local info_panel = self._info_scroll:canvas()
	local desc_text = info_panel.child(info_panel, "DescText")
	local objective_text = info_panel.child(info_panel, "ObjectiveText")
	local reward_text = info_panel.child(info_panel, "RewardText")
	local image_panel = info_panel.child(info_panel, "TrophyImagePanel")

	desc_text.set_w(desc_text, new_width)
	objective_text.set_w(objective_text, new_width)
	reward_text.set_w(reward_text, new_width)
	image_panel.set_w(image_panel, new_width)
	image_panel.set_h(image_panel, new_width/2)
	self._image_outline:close()

	self._image_outline = BoxGuiObject:new(image_panel, {
		sides = {
			2,
			2,
			2,
			2
		}
	})

	self._image_outline:set_color(Color(0.2, 1, 1, 1))
	self._image_outline:set_blend_mode("add")

	if self._progress_items then
		for _, item in ipairs(self._progress_items) do
			item.set_w(item, new_width)
		end
	end

	if self._selected_trophy then
		self.set_trophy_info(self, self._selected_trophy, false)
	end

	return 
end
CustomSafehouseGuiPageTrophies.set_trophy_info = function (self, trophy, update_size)
	local info_panel = self._info_scroll:canvas()
	local title_text = info_panel.child(info_panel, "TitleText")
	local image_panel = info_panel.child(info_panel, "TrophyImagePanel")
	local trophy_image = image_panel.child(image_panel, "TrophyImage")
	local complete_banner = info_panel.child(info_panel, "CompleteBannerPanel")
	local complete_text = complete_banner.child(complete_banner, "CompleteText")
	local complete_fill = complete_banner.child(complete_banner, "CompleteBannerFill")
	local desc_text = info_panel.child(info_panel, "DescText")
	local objective_header = info_panel.child(info_panel, "ObjectiveHeader")
	local objective_text = info_panel.child(info_panel, "ObjectiveText")
	local reward_header = info_panel.child(info_panel, "RewardHeader")
	local reward_text = info_panel.child(info_panel, "RewardText")
	local progress_header = info_panel.child(info_panel, "ProgressHeader")
	local data = trophy.trophy_data(trophy)

	title_text.set_text(title_text, utf8.to_upper(managers.localization:text(data.name_id)))
	desc_text.set_text(desc_text, managers.localization:text(data.desc_id))
	trophy_image.set_image(trophy_image, "guis/dlcs/chill/textures/pd2/trophies/" .. tostring(data.image_id))

	if data.completed then
		complete_banner.set_visible(complete_banner, true)
		desc_text.set_top(desc_text, complete_banner.bottom(complete_banner) + PANEL_PADDING)
	else
		complete_banner.set_visible(complete_banner, false)
		desc_text.set_top(desc_text, image_panel.bottom(image_panel) + PANEL_PADDING)
	end

	if data.displayed then
		complete_text.set_text(complete_text, managers.localization:to_upper_text("menu_trophy_displayed"))
		complete_text.set_color(complete_text, tweak_data.screen_colors.challenge_completed_color)
		complete_fill.set_color(complete_fill, tweak_data.screen_colors.challenge_completed_color)
	else
		complete_text.set_text(complete_text, managers.localization:to_upper_text("menu_trophy_not_displayed"))
		complete_text.set_color(complete_text, tweak_data.screen_colors.important_1)
		complete_fill.set_color(complete_fill, tweak_data.screen_colors.important_1)
	end

	local _, _, _, h = desc_text.text_rect(desc_text)

	desc_text.set_h(desc_text, h)
	objective_header.set_top(objective_header, desc_text.bottom(desc_text) + PANEL_PADDING)
	objective_text.set_top(objective_text, objective_header.bottom(objective_header))
	reward_header.set_top(reward_header, objective_text.bottom(objective_text) + PANEL_PADDING)
	reward_text.set_top(reward_text, reward_header.bottom(reward_header))
	reward_header.set_visible(reward_header, data.gives_reward ~= false)
	reward_text.set_visible(reward_text, data.gives_reward ~= false)

	local macros = {}

	if 0 < #data.objectives then
		local max = 0

		for idx, objective in ipairs(data.objectives) do
			max = math.max(max, objective.max_progress)
		end

		macros.max_progress = tostring(max)
	end

	objective_text.set_text(objective_text, managers.localization:text(data.objective_id, macros))

	local _, _, _, h = objective_text.text_rect(objective_text)

	objective_text.set_h(objective_text, h)

	if self._progress_items then
		for _, item in ipairs(self._progress_items) do
			item.destroy(item)
		end

		self._progress_items = {}
	end

	if data.show_progress then
		progress_header.set_visible(progress_header, true)

		if data.gives_reward ~= false then
			progress_header.set_top(progress_header, reward_text.bottom(reward_text) + PANEL_PADDING)
		else
			progress_header.set_top(progress_header, objective_text.bottom(objective_text) + PANEL_PADDING)
		end

		self._progress_items = {}

		for idx, objective in ipairs(data.objectives) do
			if data.completed then
				objective.completed = true
				objective.progress = objective.max_progress
			end

			local item = CustomSafehouseGuiProgressItem:new(info_panel, objective)

			table.insert(self._progress_items, item)

			local pos = progress_header.bottom(progress_header) + CustomSafehouseGuiProgressItem.h*(idx - 1)

			item.set_top(item, pos)
		end
	else
		progress_header.set_visible(progress_header, false)
	end

	if update_size then
		self._info_scroll:update_canvas_size()
	end

	return 
end
CustomSafehouseGuiPageTrophies._show_all_trophies = function (self)
	for i, trophy in ipairs(self._trophies) do
		managers.custom_safehouse:set_trophy_displayed(trophy.trophy_data(trophy).id, true)
		trophy.refresh(trophy)
	end

	self.refresh(self)

	return 
end
CustomSafehouseGuiPageTrophies._hide_all_trophies = function (self)
	for i, trophy in ipairs(self._trophies) do
		managers.custom_safehouse:set_trophy_displayed(trophy.trophy_data(trophy).id, false)
		trophy.refresh(trophy)
	end

	self.refresh(self)

	return 
end
CustomSafehouseGuiPageTrophies._set_selected = function (self, trophy, skip_sound)
	if not trophy then
		return false
	end

	if self._selected_trophy then
		self._selected_trophy:set_selected(false)
	end

	self._selected_trophy = trophy

	self._selected_trophy:set_selected(true, not skip_sound)
	self.set_trophy_info(self, self._selected_trophy, true)

	local scroll_panel = self._trophies_scroll:scroll_panel()
	local y = self._trophies_scroll:canvas():y() + trophy.bottom(trophy)

	if scroll_panel.h(scroll_panel) < y then
		self._trophies_scroll:perform_scroll(y - scroll_panel.h(scroll_panel), -1)
	else
		y = self._trophies_scroll:canvas():y() + trophy.top(trophy)

		if y < 0 then
			self._trophies_scroll:perform_scroll(math.abs(y), 1)
		end
	end

	if self._buttons[1] and self._buttons[1]:button_data().btn == "BTN_A" then
		self._buttons[1]:set_hidden(false)

		if trophy.trophy_data(trophy).completed then
			local text_id = (trophy.trophy_data(trophy).displayed and "menu_trophy_change_display_to_off") or "menu_trophy_change_display_to_on"

			self._buttons[1]:set_text(managers.localization:to_upper_text(text_id))
		else
			self._buttons[1]:set_hidden(true)
		end

		self.update_info_panel_size(self)
	end

	return 
end
CustomSafehouseGuiPageTrophies.refresh = function (self)
	CustomSafehouseGuiPageTrophies.super.refresh(self)
	self._set_selected(self, self._selected_trophy, true)

	return 
end
CustomSafehouseGuiPageTrophies.update_info_panel_size = function (self)
	local active_buttons = 0
	local button_panel_h = 0

	if not Global.game_settings.is_playing then
		for i, button in ipairs(self._buttons) do
			if not button.hidden(button) then
				active_buttons = active_buttons + 1

				button.reorder(button, active_buttons)
			end
		end

		button_panel_h = active_buttons*medium_font_size + 10
	end

	local trophy_panel = self.info_panel(self):child("trophy_panel")
	local buttons_panel = self.info_panel(self):child("buttons_panel")

	if 0 < button_panel_h then
		trophy_panel.set_h(trophy_panel, self.info_panel(self):h() - button_panel_h - PANEL_PADDING)
	else
		trophy_panel.set_h(trophy_panel, self.info_panel(self):h())
	end

	self._info_scroll:set_size(self._info_scroll:panel():w(), trophy_panel.h(trophy_panel))
	buttons_panel.set_h(buttons_panel, button_panel_h)
	buttons_panel.set_bottom(buttons_panel, self.info_panel(self):bottom())

	if self._buttons_box_panel then
		self._buttons_box_panel:close()

		self._buttons_box_panel = nil
	end

	if self._trophy_box_panel then
		self._trophy_box_panel:close()

		self._trophy_box_panel = nil
	end

	self._buttons_box_panel = BoxGuiObject:new(buttons_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})
	self._trophy_box_panel = BoxGuiObject:new(self._info_scroll:panel(), {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	return 
end
CustomSafehouseGuiPageTrophies.update = function (self, t, dt)
	local cx, cy = managers.menu_component:get_right_controller_axis()

	if cy ~= 0 and self._info_scroll then
		self._info_scroll:perform_scroll(math.abs(cy*500*dt), math.sign(cy))
	end

	if self._scanline_effect then
		local h = self._scanline_effect:h()*0.25*-1

		self._scanline_effect:move(0, dt*10)

		if h <= self._scanline_effect:top() then
			self._scanline_effect:set_top(self._scanline_effect:top() + h)
		end
	end

	return 
end
CustomSafehouseGuiPageTrophies.mouse_moved = function (self, button, x, y)
	if not self._active then
		return 
	end

	for i, panel in pairs(self._scrollable_panels) do
		if panel then
			local values = {
				panel.mouse_moved(panel, button, x, y)
			}

			if values[1] ~= nil then
				return unpack(values)
			end
		end
	end

	if self.panel(self):inside(x, y) then
		slot4 = ipairs
		slot5 = self._trophies or {}

		for idx, trophy in slot4(slot5) do
			if trophy.inside(trophy, x, y) then
				if self._selected_trophy ~= trophy then
					self._set_selected(self, trophy)
				end

				return true, "link"
			end
		end
	end

	local used, pointer = nil

	for _, button in ipairs(self._buttons) do
		if button.inside(button, x, y) and not used then
			button.set_selected(button, true)

			pointer = "link"
			used = true
		else
			button.set_selected(button, false)
		end
	end

	return used, pointer
end
CustomSafehouseGuiPageTrophies.confirm_pressed = function (self)
	if Global.game_settings.is_playing then
		return 
	end

	if managers.menu:is_pc_controller() then
		for _, button in ipairs(self._buttons) do
			if button.is_selected(button) then
				button.trigger(button, self)

				return 
			end
		end
	end

	if self._selected_trophy then
		self._selected_trophy:trigger(self)
	end

	return 
end
CustomSafehouseGuiPageTrophies.mouse_pressed = function (self, button, x, y)
	if not self._active then
		return 
	end

	for i, panel in pairs(self._scrollable_panels) do
		if panel then
			local values = {
				panel.mouse_pressed(panel, button, x, y)
			}

			if values[1] ~= nil then
				return unpack(values)
			end
		end
	end

	if self.panel(self):inside(x, y) then
		slot4 = ipairs
		slot5 = self._trophies or {}

		for idx, trophy in slot4(slot5) do
			if trophy.inside(trophy, x, y) and button == Idstring("0") then
				trophy.trigger(trophy, self)

				return true
			end
		end
	end

	for _, button in ipairs(self._buttons) do
		if button.inside(button, x, y) then
			button.trigger(button)

			return true
		end
	end

	return 
end
CustomSafehouseGuiPageTrophies.mouse_released = function (self, button, x, y)
	if not self._active then
		return 
	end

	for i, panel in pairs(self._scrollable_panels) do
		if panel then
			local values = {
				panel.mouse_released(panel, button, x, y)
			}

			if values[1] ~= nil then
				self._prevent_click = (self._prevent_click or 0) + 1

				return unpack(values)
			end
		end
	end

	return 
end
CustomSafehouseGuiPageTrophies.mouse_wheel_up = function (self, x, y)
	if not self._active then
		return 
	end

	for i, panel in pairs(self._scrollable_panels) do
		if panel then
			local values = {
				panel.scroll(panel, x, y, 1)
			}

			if values[1] ~= nil then
				return unpack(values)
			end
		end
	end

	return 
end
CustomSafehouseGuiPageTrophies.mouse_wheel_down = function (self, x, y)
	if not self._active then
		return 
	end

	for i, panel in pairs(self._scrollable_panels) do
		if panel then
			local values = {
				panel.scroll(panel, x, y, -1)
			}

			if values[1] ~= nil then
				return unpack(values)
			end
		end
	end

	return 
end
CustomSafehouseGuiPageTrophies.move_up = function (self)
	if self._selected_trophy then
		self._set_selected(self, self._selected_trophy:get_linked("up"))
		self._gui:update_legend()
	end

	return 
end
CustomSafehouseGuiPageTrophies.move_down = function (self)
	if self._selected_trophy then
		self._set_selected(self, self._selected_trophy:get_linked("down"))
		self._gui:update_legend()
	end

	return 
end
CustomSafehouseGuiPageTrophies.get_legend = function (self)
	local legend = {}

	table.insert(legend, "move")

	if self._info_scroll:is_scrollable() then
		table.insert(legend, "scroll")
	end

	table.insert(legend, "back")

	return legend
end
CustomSafehouseGuiTrophyItem = CustomSafehouseGuiTrophyItem or class(CustomSafehouseGuiItem)
CustomSafehouseGuiTrophyItem.init = function (self, panel, data, x, priority)
	CustomSafehouseGuiTrophyItem.super.init(self, panel, data)

	self._data = data
	self._priority = priority or 0
	self._is_complete = false
	self._panel = panel.panel(panel, {
		layer = 10,
		x = x,
		y = x,
		w = panel.w(panel) - x*2,
		h = large_font_size
	})
	local size = self._panel:h() - 16
	self._complete_checkbox = self._panel:bitmap({
		x = 8,
		y = 8,
		w = size,
		h = size
	})

	self._complete_checkbox:set_image("guis/textures/pd2/mission_briefing/gui_tickbox")

	self._complete_checkbox_highlight = self._panel:bitmap({
		x = 8,
		y = 8,
		w = size,
		h = size
	})

	self._complete_checkbox_highlight:set_image("guis/textures/pd2/mission_briefing/gui_tickbox")
	self._complete_checkbox_highlight:set_visible(false)

	self._btn_text = self._panel:text({
		text = "",
		name = "text",
		align = "left",
		blend_mode = "add",
		x = 10,
		layer = 1,
		font_size = medium_font_size,
		font = medium_font,
		color = tweak_data.screen_colors.button_stage_3
	})

	self.set_text(self, managers.localization:text(data.name_id))

	self._select_rect = self._panel:rect({
		blend_mode = "add",
		name = "select_rect",
		halign = "scale",
		alpha = 0.3,
		valign = "scale",
		color = tweak_data.screen_colors.button_stage_3
	})

	self._select_rect:set_visible(false)

	if data.completed then
		self.complete(self)
	end

	self.refresh(self)

	return 
end
CustomSafehouseGuiTrophyItem.trophy_data = function (self)
	return self._data
end
CustomSafehouseGuiTrophyItem.set_text = function (self, text)
	self._btn_text:set_text(utf8.to_upper(text))

	local _, _, w, h = self._btn_text:text_rect()

	self._btn_text:set_size(w, h)
	self._btn_text:set_left(self._complete_checkbox:right() + 10)
	self._btn_text:set_top(10)

	return 
end
CustomSafehouseGuiTrophyItem.inside = function (self, x, y)
	return self._panel:inside(x, y)
end
CustomSafehouseGuiTrophyItem.show = function (self)
	self._select_rect:set_visible(true)
	self._complete_checkbox_highlight:set_visible(true)
	self._btn_text:set_alpha(1)

	if self.trophy_data(self).completed and not self.trophy_data(self).displayed then
		self._btn_text:set_color(tweak_data.screen_colors.important_1)
	else
		self._btn_text:set_color(tweak_data.screen_colors.button_stage_2)
	end

	return 
end
CustomSafehouseGuiTrophyItem.hide = function (self)
	self._select_rect:set_visible(false)
	self._complete_checkbox_highlight:set_visible(false)
	self._btn_text:set_alpha(1)

	if self.trophy_data(self).completed and not self.trophy_data(self).displayed then
		self._btn_text:set_color(tweak_data.screen_colors.important_1)
		self._btn_text:set_alpha(0.8)
	else
		self._btn_text:set_color(tweak_data.screen_colors.button_stage_3)
	end

	return 
end
CustomSafehouseGuiTrophyItem.top = function (self)
	return self._panel:top()
end
CustomSafehouseGuiTrophyItem.bottom = function (self)
	return self._panel:bottom()
end
CustomSafehouseGuiTrophyItem.visible = function (self)
	return self._select_rect:visible()
end
CustomSafehouseGuiTrophyItem.refresh = function (self)
	if self._selected then
		self.show(self)
	else
		self.hide(self)
	end

	return 
end
CustomSafehouseGuiTrophyItem._update_position = function (self)
	self._panel:set_y((self._scroll_offset or 0) + (self._priority - 1)*large_font_size + self._priority)

	return 
end
CustomSafehouseGuiTrophyItem.set_position = function (self, i)
	self._priority = i

	self._update_position(self)

	return 
end
CustomSafehouseGuiTrophyItem.set_scroll_offset = function (self, offset)
	self._scroll_offset = offset

	self._update_position(self)

	return 
end
CustomSafehouseGuiTrophyItem.priority = function (self)
	return self._priority
end
CustomSafehouseGuiTrophyItem.complete = function (self)
	if not self._is_complete then
		self._is_complete = true
		self._priority = self._priority + #tweak_data.safehouse.trophies
		local complete_color = tweak_data.screen_color_grey

		self._complete_checkbox:set_image("guis/textures/pd2/mission_briefing/gui_tickbox_ready")
	end

	return 
end
CustomSafehouseGuiTrophyItem.is_complete = function (self)
	return self._is_complete
end
CustomSafehouseGuiTrophyItem.link = function (self, up, down)
	self._links = {
		up = up,
		down = down
	}

	return 
end
CustomSafehouseGuiTrophyItem.get_linked = function (self, link)
	return self._links and self._links[link]
end
CustomSafehouseGuiTrophyItem.trigger = function (self, parent)
	if not Global.game_settings.is_playing then
		managers.custom_safehouse:set_trophy_displayed(self.trophy_data(self).id, not self.trophy_data(self).displayed)
		self.refresh(self)

		if parent then
			parent.refresh(parent)
		end
	end

	return 
end
CustomSafehouseGuiProgressItem = CustomSafehouseGuiProgressItem or class(CustomSafehouseGuiItem)
CustomSafehouseGuiProgressItem.h = small_font_size*1.3
CustomSafehouseGuiProgressItem.init = function (self, parent_panel, trophy_objective)
	self._parent = parent_panel
	self._objective = trophy_objective
	self._panel = parent_panel.panel(parent_panel, {
		w = parent_panel.w(parent_panel),
		h = self.h
	})
	self._text = self._panel:text({
		name = "text",
		blend_mode = "add",
		align = "left",
		vertical = "center",
		valign = "scale",
		halign = "scale",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.text,
		text = managers.localization:text(tostring(trophy_objective.name_id)),
		w = self._panel:w(),
		h = self._panel:h()
	})

	if 1 < trophy_objective.max_progress then
		self._progress_panel = self._panel:panel({
			w = self._panel:w(),
			h = self._panel:h()
		})
		self._progress_outline = BoxGuiObject:new(self._progress_panel, {
			sides = {
				1,
				1,
				1,
				1
			}
		})
		local color = (trophy_objective.completed and tweak_data.screen_colors.challenge_completed_color) or tweak_data.screen_colors.button_stage_3
		self._progress_fill = self._progress_panel:rect({
			w = self._panel:w()*trophy_objective.progress/trophy_objective.max_progress,
			color = color.with_alpha(color, 0.4)
		})

		self._text:set_x(PANEL_PADDING)

		self._progress_text = self._panel:text({
			name = "progress_text",
			blend_mode = "add",
			align = "right",
			vertical = "center",
			valign = "scale",
			halign = "scale",
			layer = 1,
			font_size = small_font_size,
			font = small_font,
			color = tweak_data.screen_colors.text,
			text = tostring(trophy_objective.progress) .. "/" .. tostring(trophy_objective.max_progress),
			w = self._panel:w() - PANEL_PADDING*2,
			h = self._progress_panel:h(),
			x = PANEL_PADDING
		})
	else
		local texture = "guis/textures/menu_tickbox"
		local texture_rect = {
			(trophy_objective.completed and 24) or 0,
			0,
			24,
			24
		}
		self._checkbox = self._panel:bitmap({
			name = "checkbox",
			layer = 1,
			visible = true,
			valign = "scale",
			halign = "scale",
			texture = texture,
			texture_rect = texture_rect
		})

		self._checkbox:set_right(self._panel:w())
		self._checkbox:set_top(self._panel:h()*0.5 - self._checkbox:h()*0.5)
	end

	return 
end
CustomSafehouseGuiProgressItem.destroy = function (self)
	self._parent:remove(self._panel)

	return 
end
CustomSafehouseGuiProgressItem.top = function (self)
	return self._panel:top()
end
CustomSafehouseGuiProgressItem.bottom = function (self)
	return self._panel:bottom()
end
CustomSafehouseGuiProgressItem.set_top = function (self, y)
	return self._panel:set_top(y)
end
CustomSafehouseGuiProgressItem.set_bottom = function (self, y)
	return self._panel:set_bottom(y)
end
CustomSafehouseGuiProgressItem.set_w = function (self, w)
	self._panel:set_w(w)

	if alive(self._progress_panel) then
		self._progress_panel:set_w(w)
		self._progress_fill:set_w(w*self._objective.progress/self._objective.max_progress)
		self._progress_text:set_w(self._panel:w() - PANEL_PADDING*2)
		self._progress_outline:close()

		self._progress_outline = BoxGuiObject:new(self._progress_panel, {
			sides = {
				1,
				1,
				1,
				1
			}
		})
	end

	if alive(self._checkbox) then
		self._checkbox:set_right(self._panel:w())
	end

	return 
end

return 
