local massive_font = tweak_data.menu.pd2_massive_font
local large_font = tweak_data.menu.pd2_large_font
local medium_font = tweak_data.menu.pd2_medium_font
local small_font = tweak_data.menu.pd2_small_font
local massive_font_size = tweak_data.menu.pd2_massive_font_size
local large_font_size = tweak_data.menu.pd2_large_font_size
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font_size = tweak_data.menu.pd2_small_font_size
local IS_WIN_32 = SystemInfo:platform() == Idstring("WIN32")
local NOT_WIN_32 = not IS_WIN_32
local WIDTH_MULTIPLIER = (NOT_WIN_32 and 0.68) or 0.71
local BOX_GAP = 13.5
local PANEL_PADDING = 8
local LINE_PADDING = 4

local function anim_select(o, w, h, instant)
	local current_width = o.w(o)
	local current_height = o.h(o)
	local end_width = w
	local end_height = h
	local cx, cy = o.center(o)

	if instant then
		o.set_size(o, math.lerp(current_width, end_width, p), math.lerp(current_height, end_height, p))
		o.set_center(o, cx, cy)
	else
		over(0.2, function (p)
			o:set_size(math.lerp(current_width, end_width, p), math.lerp(current_height, end_height, p))
			o:set_center(cx, cy)

			return 
		end)
	end

	return 
end

local function anim_deselect(o, w, h, instant)
	local current_width = o.w(o)
	local current_height = o.h(o)
	local end_width = w*0.8
	local end_height = h*0.8
	local cx, cy = o.center(o)

	if instant then
		o.set_size(o, math.lerp(current_width, end_width, p), math.lerp(current_height, end_height, p))
		o.set_center(o, cx, cy)
	else
		over(0.2, function (p)
			o:set_size(math.lerp(current_width, end_width, p), math.lerp(current_height, end_height, p))
			o:set_center(cx, cy)

			return 
		end)
	end

	return 
end

local function anim_invalid(o)
	local color = tweak_data.screen_colors.item_stage_1

	o.set_color(o, color)

	local lerp_color = nil

	over(0.4, function (t)
		lerp_color = math.lerp(color, tweak_data.screen_colors.important_1, t - 1)

		o:set_color(lerp_color)

		return 
	end)

	return 
end

CustomSafehouseGuiPageMap = CustomSafehouseGuiPageMap or class(CustomSafehouseGuiPage)
CustomSafehouseGuiPageMap.init = function (self, page_id, page_panel, fullscreen_panel, gui)
	CustomSafehouseGuiPageMap.super.init(self, page_id, page_panel, fullscreen_panel, gui)

	self.make_fine_text = BlackMarketGui.make_fine_text

	self.panel(self):set_layer(45)
	self._setup_info_panel(self)
	self._setup_map(self)
	self._setup_help_text(self)

	return 
end
CustomSafehouseGuiPageMap.is_being_raided = function (self)
	return managers.custom_safehouse:is_being_raided()
end
CustomSafehouseGuiPageMap._setup_map = function (self)
	self._scanline = self.panel(self):bitmap({
		texture = "guis/dlcs/big_bank/textures/pd2/pre_planning/scanline",
		name = "scanline",
		h = 128,
		wrap_mode = "wrap",
		layer = 7,
		blend_mode = "add",
		texture_rect = {
			0,
			0,
			self.panel(self):w(),
			64
		}
	})

	self._scanline:set_bottom(-math.random(100))

	self._map_size = tweak_data.safehouse.map.size
	self._map_panel = self.panel(self):panel({
		alpha = 0.9,
		name = "map",
		layer = 0,
		w = self._map_size,
		h = self._map_size
	})

	self._map_panel:set_center(self.panel(self):w()/2, self.panel(self):h()/2)

	self._map_x, self._map_y = self._map_panel:position()
	self._map_zoom = 0
	self._grid_panel = self._panel:panel({
		name = "grid",
		w = self._map_size,
		h = self._map_size
	})
	self._map_grid = self._grid_panel:bitmap({
		texture = "guis/dlcs/big_bank/textures/pd2/pre_planning/bg_grid",
		layer = -1,
		valign = "scale",
		wrap_mode = "wrap",
		blend_mode = "add",
		halign = "scale",
		w = self._grid_panel:w(),
		h = self._grid_panel:h(),
		texture_rect = {
			0,
			0,
			self._grid_panel:w()*2,
			self._grid_panel:h()*2
		}
	})
	self._min_zoom = 1
	self._max_zoom = 5
	self._lerp_map = {
		x = self._panel:w()/2 - self._map_panel:w()/2,
		y = self._panel:h()/2 - self._map_panel:h()/2,
		clbk = callback(self, self, "set_lerp_zoom", 0)
	}
	self._floors = {}
	self._all_points = {}
	self._selected_floor = 1

	for i, floor in ipairs(tweak_data.safehouse.map.floors) do
		local floor_map = CustomSafehouseMapFloor:new(self._panel, self._map_panel, floor)

		floor_map.hide(floor_map)

		for _, id in pairs(floor.rooms) do
			local point = CustomSafehouseMapPoint:new(self, self._map_panel, id)

			floor_map.add_point(floor_map, point)
			table.insert(self._all_points, point)
			point.hide(point)
		end

		self._floors[i] = floor_map
	end

	if not managers.menu:is_pc_controller() then
		self._mouse_pointer = self._panel:bitmap({
			texture = "guis/textures/mouse_pointer",
			name = "pointer",
			h = 23,
			rotation = 360,
			w = 19,
			layer = 1000,
			texture_rect = {
				0,
				0,
				19,
				23
			},
			x = self._panel:w()/2,
			y = self._panel:h()/2,
			color = Color(1, 0.7, 0.7, 0.7)
		})

		self._mouse_pointer:hide()

		self._wanted_pointer = {
			x = self._mouse_pointer:world_x(),
			y = self._mouse_pointer:world_y()
		}
	end

	self._floor_control_panel = self._panel:panel({
		layer = 20,
		name = "FloorControlPanel"
	})

	self._floor_control_panel:rect({
		valign = "scale",
		halign = "scale",
		color = Color.black:with_alpha(0.5)
	})

	local title = self._floor_control_panel:text({
		name = "FloorControlTitle",
		layer = 21,
		text = managers.localization:to_upper_text("menu_cs_change_floor"),
		x = PANEL_PADDING,
		y = PANEL_PADDING,
		font = medium_font,
		font_size = medium_font_size
	})

	self.make_fine_text(self, title)

	local max_w = title.w(title) + PANEL_PADDING*2
	local button_panel = self._floor_control_panel:panel({
		layer = 21,
		name = "FloorContolButtonPanel",
		y = title.bottom(title)
	})
	self._floor_control_buttons = {}
	local h = 0

	for i, floor in ipairs(self._floors) do
		local new_button = CustomSafehouseGuiButtonItem:new(button_panel, {
			align = "left",
			name_id = floor.name_id(floor),
			y = h,
			color = tweak_data.screen_colors.button_stage_3,
			custom = floor.name_id(floor),
			callback = callback(self, self, "floor_button_pressed")
		}, 0)

		self.make_fine_text(self, new_button._btn_text)
		new_button._btn_text:set_left(PANEL_PADDING)
		table.insert(self._floor_control_buttons, new_button)

		max_w = math.max(new_button._btn_text:w() + PANEL_PADDING*2, max_w)
		h = h + new_button._panel:h()
	end

	button_panel.set_w(button_panel, max_w)
	button_panel.set_h(button_panel, h + PANEL_PADDING)

	for _, btn in ipairs(self._floor_control_buttons) do
		btn._panel:set_w(max_w)
	end

	self._floor_control_panel:set_w(max_w)
	self._floor_control_panel:set_h(title.h(title) + button_panel.h(button_panel) + PANEL_PADDING)
	self._floor_control_panel:set_left(PANEL_PADDING)
	self._floor_control_panel:set_bottom(self._panel:h() - PANEL_PADDING)

	if self.is_being_raided(self) then
		local raid_panel = self.panel(self):panel({})
		local raid_layer = 50
		self._raid_colour_panel = raid_panel.rect(raid_panel, {
			alpha = 0.4,
			color = Color(255, 196, 0, 0)/255,
			layer = raid_layer
		})

		raid_panel.rect(raid_panel, {
			alpha = 0.4,
			color = Color.black,
			layer = raid_layer + 1
		})

		local title_size = large_font_size*0.8
		local title = raid_panel.text(raid_panel, {
			name = "raid_title",
			align = "center",
			text = managers.localization:to_upper_text("menu_chill_combat_under_attack"),
			y = self.panel(self):h()*0.4 - title_size,
			w = self.panel(self):w(),
			h = large_font_size,
			font = large_font,
			font_size = title_size,
			layer = raid_layer + 10
		})

		raid_panel.rect(raid_panel, {
			alpha = 0.4,
			color = Color.black,
			layer = raid_layer + 9,
			h = large_font_size,
			y = title.top(title) - large_font_size*0.15
		})

		local text = raid_panel.text(raid_panel, {
			name = "raid_desc",
			align = "center",
			text = managers.localization:text("menu_chill_combat_under_attack_desc"),
			y = self.panel(self):h()*0.4 + PANEL_PADDING,
			w = self.panel(self):w(),
			h = medium_font_size*3,
			font = medium_font,
			font_size = medium_font_size,
			layer = raid_layer + 10
		})
		local new_button = CustomSafehouseGuiRaidButton:new(raid_panel, raid_layer + 10, text.bottom(text) + PANEL_PADDING*2, callback(self, self, "defend_safehouse"))

		table.insert(self._buttons, new_button)
	end

	BoxGuiObject:new(self._floor_control_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})
	self.select_floor(self, self._selected_floor)

	return 
end
CustomSafehouseGuiPageMap.map_to_panel = function (self, x, y)
	x = x + self._map_panel:x()/self.current_zoom(self)
	y = y + self._map_panel:y()/self.current_zoom(self)

	return x, y
end
CustomSafehouseGuiPageMap.panel_to_map = function (self, x, y)
	x = x - self._map_panel:x()*self.current_zoom(self)
	y = y - self._map_panel:y()*self.current_zoom(self)

	return x, y
end
CustomSafehouseGuiPageMap.map_to_world = function (self, x, y)
	x, y = self.map_to_panel(self, x, y)
	x = x + self._panel:world_x()
	y = y + self._panel:world_y()

	return x, y
end
CustomSafehouseGuiPageMap.world_to_map = function (self, x, y)
	x = x - self._panel:world_x()
	y = y - self._panel:world_y()

	return self.panel_to_map(self, x, y)
end
CustomSafehouseGuiPageMap.floor_button_pressed = function (self)
	if self._current_floor_button then
		self.select_floor(self, self._current_floor_button)
	else
		for i, button in ipairs(self._floor_control_buttons) do
			if button.is_selected(button) then
				self.select_floor(self, i)

				break
			end
		end
	end

	return 
end
CustomSafehouseGuiPageMap.select_floor = function (self, floor)
	self.current_floor(self):hide()

	floor = math.clamp(floor, 1, #self._floors)
	self._selected_floor = floor
	local wanted_zoom = (self.current_floor(self):start_zoom() and self.convert_zoom_to_map(self, self.current_floor(self):start_zoom())) or self._map_zoom

	self._set_zoom(self, wanted_zoom, self._map_panel:w()/2, self._map_panel:h()/2)
	self._set_map_position(self, (-self._map_panel:w()/2 + self._panel:w()/2) - self.current_zoom(self)*240, -self._map_panel:h()/2 + self._panel:h()/2 + self.current_zoom(self)*200)
	self.current_floor(self):set_zoom_value(self._map_zoom)
	self.current_floor(self):show()
	self.current_floor(self):update()
	self.refresh(self)
	self.set_title_text(self, managers.localization:to_upper_text(self.current_floor(self):name_id()))
	self.set_room_image(self, nil)
	self.set_warning_text(self, "")
	self.set_help_text(self, (self.current_floor(self):desc_id() and managers.localization:text(self.current_floor(self):desc_id())) or "")

	if self._mouse_pointer then
		self._mouse_pointer:set_visible(not self.current_floor(self):should_disable_cursor())
	end

	for i, button in ipairs(self._floor_control_buttons) do
		button.set_selected(button, i == floor)

		if i == floor then
			button.set_color(button, tweak_data.screen_colors.button_stage_3, tweak_data.screen_colors.text)
		else
			button.set_color(button, tweak_data.screen_colors.button_stage_3, tweak_data.screen_colors.button_stage_2)
		end
	end

	return 
end
CustomSafehouseGuiPageMap.convert_zoom_to_map = function (self, wanted_zoom)
	return (wanted_zoom - self.current_floor(self):min_zoom() or self._min_zoom)/(self.current_floor(self):max_zoom() or self._max_zoom - self.current_floor(self):min_zoom() or self._min_zoom)
end
CustomSafehouseGuiPageMap.cycle_floor = function (self)
	local floor = self._selected_floor%#self._floors + 1

	self.select_floor(self, floor)

	return 
end
CustomSafehouseGuiPageMap.current_floor = function (self)
	return self._floors[self._selected_floor]
end
CustomSafehouseGuiPageMap._setup_info_panel = function (self)
	local buttons = {}

	if not managers.menu:is_pc_controller() and not self.is_being_raided(self) then
		table.insert(buttons, {
			btn = "BTN_A",
			name_id = "menu_cs_upgrade_room"
		})
		table.insert(buttons, {
			btn = "BTN_Y",
			name_id = "menu_cs_change_floor",
			pc_btn = "menu_modify_item",
			callback = callback(self, self, "cycle_floor")
		})
	end

	local show_play_safehouse_btn = Global.game_settings.single_player or Network:is_server() or not managers.network:session()

	if show_play_safehouse_btn then
		if managers.custom_safehouse:is_being_raided() then
			table.insert(buttons, {
				btn = "BTN_X",
				name_id = "menu_cn_chill_combat_defend",
				pc_btn = "menu_remove_item",
				callback = callback(self, self, "defend_safehouse")
			})
		else
			table.insert(buttons, {
				btn = "BTN_X",
				name_id = "menu_cs_enter_safehouse",
				pc_btn = "menu_remove_item",
				callback = callback(self, self, "go_to_safehouse")
			})
		end
	end

	local remaining_height = self.info_panel(self):h()

	local function new_info_panel(parent, name, h)
		local panel = parent.panel(parent, {
			layer = 1,
			name = name,
			w = parent.w(parent),
			h = h or remaining_height
		})

		BoxGuiObject:new(panel, {
			sides = {
				1,
				1,
				1,
				1
			}
		})

		remaining_height = remaining_height - h - BOX_GAP

		return panel
	end

	self._buttons_panel = new_info_panel(self.info_panel(self), "ButtonsPanel", small_font_size*#buttons + PANEL_PADDING*2 + LINE_PADDING)
	self._coins_panel = new_info_panel(self.info_panel(self), "CoinsInfoPanel", small_font_size*2 + PANEL_PADDING*2 + LINE_PADDING)
	self._text_info_panel = new_info_panel(self.info_panel(self), "TextInfoPanel", remaining_height)
	local panels = {
		self._text_info_panel,
		self._coins_panel,
		self._buttons_panel
	}

	self.stack_panels(self, BOX_GAP, panels)

	self._buttons = {}
	self._controllers_pc_mapping = {}
	self._controllers_mapping = {}
	local btn_x = 10

	for btn, btn_data in pairs(buttons) do
		local new_button = CustomSafehouseGuiButtonItem:new(self._buttons_panel, btn_data, btn_x, btn)
		self._buttons[btn] = new_button

		if btn_data.pc_btn then
			self._controllers_mapping[btn_data.pc_btn:key()] = new_button
		end
	end

	local coins_text = self._coins_panel:text({
		blend_mode = "add",
		name = "CoinsText",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.text,
		text = utf8.to_upper(managers.localization:text("menu_cs_coins"))
	})

	coins_text.set_left(coins_text, PANEL_PADDING)
	coins_text.set_y(coins_text, PANEL_PADDING)

	local coins_value = self._coins_panel:text({
		blend_mode = "add",
		name = "CoinsValue",
		layer = 1,
		font_size = medium_font_size,
		font = medium_font,
		color = tweak_data.screen_colors.text
	})

	coins_value.set_y(coins_value, coins_text.y(coins_text))
	self.update_coins_value(self)

	local total_upgrades_text = self._coins_panel:text({
		blend_mode = "add",
		name = "TotalUpgradesText",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.text,
		text = utf8.to_upper(managers.localization:text("menu_cs_total_upgrades"))
	})

	total_upgrades_text.set_left(total_upgrades_text, PANEL_PADDING)
	total_upgrades_text.set_y(total_upgrades_text, PANEL_PADDING + small_font_size + LINE_PADDING)

	local total_upgrades_value = self._coins_panel:text({
		blend_mode = "add",
		name = "TotalUpgradesValue",
		layer = 1,
		font_size = medium_font_size,
		font = medium_font,
		color = tweak_data.screen_colors.text,
		text = managers.localization:text("menu_cs_total_upgrades_value")
	})

	total_upgrades_value.set_y(total_upgrades_value, total_upgrades_text.y(total_upgrades_text))
	self.update_upgrades_purchased(self)

	local text_title = self._text_info_panel:text({
		name = "TitleText",
		blend_mode = "add",
		align = "left",
		vertical = "top",
		valign = "scale",
		text = "",
		halign = "scale",
		layer = 1,
		font_size = medium_font_size,
		font = medium_font,
		color = tweak_data.screen_colors.title,
		w = self._text_info_panel:w() - PANEL_PADDING*2,
		h = medium_font_size
	})

	text_title.set_left(text_title, PANEL_PADDING)
	text_title.set_top(text_title, PANEL_PADDING)

	local image_panel = self._text_info_panel:panel({
		name = "RoomImagePanel",
		layer = 10,
		x = PANEL_PADDING,
		w = self._text_info_panel:w() - PANEL_PADDING*2
	})

	image_panel.set_h(image_panel, image_panel.w(image_panel)/1.7777777777777777)
	image_panel.set_top(image_panel, text_title.bottom(text_title) + PANEL_PADDING)
	image_panel.set_visible(image_panel, false)

	local room_image = image_panel.bitmap(image_panel, {
		layer = 40,
		name = "RoomImage",
		texture_rect = {
			0,
			0,
			512,
			256
		},
		w = image_panel.w(image_panel),
		h = image_panel.h(image_panel)
	})
	local image_scanlines = image_panel.bitmap(image_panel, {
		texture = "guis/dlcs/chill/textures/pd2/rooms/safehouse_room_preview_effect",
		name = "RoomImageScanlines",
		wrap_mode = "wrap",
		layer = 50,
		texture_rect = {
			0,
			0,
			512,
			512
		},
		w = image_panel.w(image_panel),
		h = image_panel.h(image_panel)*4,
		y = image_panel.h(image_panel)*2*-1
	})
	self._scanline_effect = image_scanlines
	local text_warning = self._text_info_panel:text({
		blend_mode = "add",
		name = "WarningText",
		wrap = true,
		align = "left",
		word_wrap = true,
		text = "",
		vertical = "top",
		halign = "scale",
		valign = "scale",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.important_1,
		w = self._text_info_panel:w() - PANEL_PADDING*2,
		h = small_font_size*2
	})

	text_warning.set_left(text_warning, PANEL_PADDING)
	text_warning.set_top(text_warning, image_panel.bottom(image_panel) + 10)

	local text_help = self._text_info_panel:text({
		blend_mode = "add",
		name = "HelpText",
		wrap = true,
		align = "left",
		word_wrap = true,
		text = "",
		vertical = "top",
		halign = "scale",
		valign = "scale",
		layer = 1,
		font_size = small_font_size,
		font = small_font,
		color = tweak_data.screen_colors.text,
		w = self._text_info_panel:w() - PANEL_PADDING*2,
		h = self._text_info_panel:h() - PANEL_PADDING*2
	})

	text_help.set_left(text_help, PANEL_PADDING)
	text_help.set_top(text_help, text_warning.bottom(text_warning))

	return 
end
CustomSafehouseGuiPageMap.update_coins_value = function (self)
	local coins_value = self._coins_panel:child("CoinsValue")

	coins_value.set_text(coins_value, managers.experience:cash_string(math.floor(managers.custom_safehouse:coins()), ""))
	self._right_align(self, coins_value, self._coins_panel)

	return 
end
CustomSafehouseGuiPageMap.update_upgrades_purchased = function (self)
	local macro = {
		owned = tostring(managers.custom_safehouse:total_room_unlocks_purchased()),
		total = tostring(managers.custom_safehouse:total_room_unlocks())
	}
	local upgrades_value = self._coins_panel:child("TotalUpgradesValue")

	upgrades_value.set_text(upgrades_value, managers.localization:text("menu_cs_total_upgrades_value", macro))
	self._right_align(self, upgrades_value, self._coins_panel)

	return 
end
CustomSafehouseGuiPageMap._right_align = function (self, text_element, panel)
	self.make_fine_text(self, text_element)
	text_element.set_right(text_element, panel.w(panel) - 8)

	return 
end
CustomSafehouseGuiPageMap._go_to_safehouse = function (self)
	if Global.game_settings.single_player then
		MenuCallbackHandler:play_single_player()
		MenuCallbackHandler:start_single_player_job({
			difficulty = "normal",
			job_id = "chill"
		})
	else
		MenuCallbackHandler:start_job({
			difficulty = "normal",
			job_id = "chill"
		})
	end

	return 
end
CustomSafehouseGuiPageMap.go_to_safehouse = function (self, params)
	if params and params.skip_question then
		self._go_to_safehouse(self)

		return 
	end

	local dialog_data = {
		focus_button = 1,
		title = managers.localization:text("dialog_safehouse_title"),
		text = managers.localization:text("dialog_safehouse_goto_text")
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_go_to_safehouse")
	}
	local no_button = {
		cancel_button = true,
		text = managers.localization:text("dialog_no")
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
CustomSafehouseGuiPage.defend_safehouse = function (self)
	managers.menu:open_node("crimenet_contract_chill")

	return 
end
CustomSafehouseGuiPageMap._setup_help_text = function (self)
	self._help_text = self._gui._panel:text({
		text = "",
		visible = false,
		font = small_font,
		font_size = small_font_size,
		color = tweak_data.screen_colors.text
	})

	self.make_fine_text(self, self._help_text)
	self._help_text:set_right(self._gui._panel:w())

	return 
end
CustomSafehouseGuiPageMap.set_top_help_text = function (self, text_id, data)
	local text = ""

	if text_id then
		text = managers.localization:to_upper_text(text_id, data)
	end

	self._help_text:set_text(text)
	self.make_fine_text(self, self._help_text)
	self._help_text:set_right(self._gui._panel:w())

	return 
end
CustomSafehouseGuiPageMap.get_legend = function (self)
	if self.is_being_raided(self) then
		return {
			"back"
		}
	else
		return {
			"move",
			"zoom",
			"back"
		}
	end

	return 
end
CustomSafehouseGuiPageMap.set_active = function (self, active)
	self._help_text:set_visible(active)
	CustomSafehouseGuiPageMap.super.set_active(self, active)

	if not managers.menu:is_pc_controller() and alive(self._mouse_pointer) then
		self._mouse_pointer:set_visible(active and not self.current_floor(self):should_disable_cursor() and not self.is_being_raided(self))
	end

	return 
end
CustomSafehouseGuiPageMap.set_pointer_image = function (self, type)
	local types = {
		arrow = {
			0,
			0,
			19,
			23
		},
		link = {
			20,
			0,
			19,
			23
		},
		hand = {
			40,
			0,
			19,
			23
		},
		grab = {
			60,
			0,
			19,
			23
		}
	}
	local rect = types[type]

	if rect and self._mouse_pointer_image ~= type then
		self._mouse_pointer_image = type

		self._mouse_pointer:set_texture_rect(rect[1], rect[2], rect[3], rect[4])
	end

	return 
end
CustomSafehouseGuiPageMap.refresh = function (self)
	CustomSafehouseGuiPageMap.super.refresh(self)

	if not self._active then
		return 
	end

	self.update_upgrades_purchased(self)
	self.update_coins_value(self)
	self.current_floor(self):refresh()

	return 
end
CustomSafehouseGuiPageMap.set_title_text = function (self, text)
	local text_title = self._text_info_panel:child("TitleText")

	text_title.set_text(text_title, text)
	managers.menu_component:make_color_text(text_title)

	local image_panel = self._text_info_panel:child("RoomImagePanel")

	image_panel.set_top(image_panel, text_title.bottom(text_title) + ((IS_WIN_32 and 10) or 0))
	image_panel.set_visible(image_panel, true)

	local text_warning = self._text_info_panel:child("WarningText")

	text_warning.set_top(text_warning, image_panel.bottom(image_panel) + ((IS_WIN_32 and 10) or 0))

	return 
end
CustomSafehouseGuiPageMap.set_room_image = function (self, image)
	local image_panel = self._text_info_panel:child("RoomImagePanel")
	local room_image = image_panel.child(image_panel, "RoomImage")

	if image then
		room_image.set_image(room_image, image)
		image_panel.set_visible(image_panel, true)
	else
		image_panel.set_visible(image_panel, false)
	end

	return 
end
CustomSafehouseGuiPageMap.set_warning_text = function (self, text)
	if text and text ~= "" then
		local text_warning = self._text_info_panel:child("WarningText")

		text_warning.set_text(text_warning, text)
		managers.menu_component:make_color_text(text_warning)

		local text_help = self._text_info_panel:child("HelpText")

		text_help.set_top(text_help, text_warning.bottom(text_warning) + ((IS_WIN_32 and 5) or 0))
	else
		local text_warning = self._text_info_panel:child("WarningText")

		text_warning.set_text(text_warning, "")
		self._text_info_panel:child("HelpText"):set_top(self._text_info_panel:child("RoomImagePanel"):bottom() + ((IS_WIN_32 and 5) or 0))
	end

	return 
end
CustomSafehouseGuiPageMap.set_help_text = function (self, text)
	local text_help = self._text_info_panel:child("HelpText")

	text_help.set_text(text_help, text)
	managers.menu_component:make_color_text(text_help)

	local image_panel = self._text_info_panel:child("RoomImagePanel")

	if image_panel.visible(image_panel) then
		text_help.set_top(text_help, image_panel.bottom(image_panel) + ((IS_WIN_32 and 5) or 0))
	else
		text_help.set_top(text_help, image_panel.top(image_panel))
	end

	self._fix_help_text(self)

	return 
end
CustomSafehouseGuiPageMap._fix_help_text = function (self)
	local text_help = self._text_info_panel:child("HelpText")

	text_help.set_font_size(text_help, small_font_size)
	text_help.set_w(text_help, self._text_info_panel:w() - PANEL_PADDING*2)
	self.make_fine_text(self, text_help)

	local max_h = self._text_info_panel:bottom() - self._text_info_panel:child("RoomImagePanel"):bottom()

	while max_h < text_help.h(text_help) do
		text_help.set_font_size(text_help, text_help.font_size(text_help) - 1)
		text_help.set_w(text_help, self._text_info_panel:w())
		self.make_fine_text(self, text_help)
	end

	return 
end
CustomSafehouseGuiPageMap.confirm_pressed = function (self)
	self.current_floor(self):confirm_pressed()

	return 
end
CustomSafehouseGuiPageMap.set_lerp_zoom = function (self, zoom)
	local min = self.min_zoom(self)
	local max = self.max_zoom(self)
	local new_zoom = math.clamp(zoom, min, max)

	if self._map_zoom ~= new_zoom then
		self._lerp_zoom = new_zoom
	end

	return 
end
CustomSafehouseGuiPageMap.floor_offset = function (self)
	local floor_offset = {
		x = (self.current_floor(self):x() or 0)*self._map_size*self.current_zoom(self),
		y = (self.current_floor(self):y() or 0)*self._map_size*self.current_zoom(self),
		w = ((self.current_floor(self):w() or 1) - 1)*self._map_size*self.current_zoom(self),
		h = ((self.current_floor(self):h() or 1) - 1)*self._map_size*self.current_zoom(self)
	}

	return floor_offset
end
CustomSafehouseGuiPageMap.update = function (self, t, dt)
	if not managers.menu:is_pc_controller() then
		if not managers.system_menu or not managers.system_menu:is_active() or not not managers.system_menu:is_closing() then
			local axis_x, axis_y = managers.menu_component:get_left_controller_axis()

			if self.is_being_raided(self) then
				axis_y = 0
				axis_x = 0
			end

			if axis_x ~= 0 or axis_y ~= 0 then
				local speed = dt*500

				self._move_map_position(self, -axis_x*speed, axis_y*speed)

				self._lerp_map = nil
				self._lerp_zoom = nil
			end

			axis_x, axis_y = managers.menu_component:get_right_controller_axis()

			if self.is_being_raided(self) then
				axis_y = 0
				axis_x = 0
			end

			if axis_y ~= 0 then
				local zoomed = self._change_zoom(self, axis_y*dt, self._mouse_pointer:x(), self._mouse_pointer:y())
				self._lerp_map = nil
				self._lerp_zoom = nil

				if zoomed and (not self._next_zoom or self._next_zoom < t) then
					self._next_zoom = t + math.lerp(0.2, 0.1, math.abs(axis_y))

					if 0 < axis_y then
						managers.menu_component:post_event("zoom_in")
					else
						managers.menu_component:post_event("zoom_out")
					end
				end
			else
				self._next_zoom = nil
			end
		end

		local x, y = self._mouse_pointer:world_position()
		local mouse_vec = Vector3(x, y, 0)
		local dist = 10000000
		local closest_point = nil
		local point_selected = false

		for _, point in ipairs(self.current_floor(self):points()) do
			if not point_selected and point._panel:inside(x, y) then
				if not point.is_selected(point) then
					point.set_selected(point, true)
				end

				self.set_pointer_image(self, "link")

				point_selected = true
			elseif point.is_selected(point) then
				point.set_selected(point, false)
				self.set_pointer_image(self, "arrow")
			end

			local d = mvector3.distance(mouse_vec, Vector3(point._panel:world_center_x(), point._panel:world_center_y(), 0))

			if d < dist then
				closest_point = point
				dist = d
			end
		end

		local max_dist = 100

		if self.is_being_raided(self) then
			max_dist = 0
		end

		if dist < max_dist then
			local px = closest_point._panel:world_center_x()
			local py = closest_point._panel:world_center_y()
			local dx = math.abs(self._wanted_pointer.x - px)
			local dy = math.abs(self._wanted_pointer.y - py)

			if dx < max_dist and dy < max_dist then
				self._pointer_lerp = {
					x = px,
					y = py
				}
			else
				self._pointer_lerp = nil
			end
		else
			self._pointer_lerp = nil
		end

		local pforce = dt*2

		if self._pointer_lerp then
			self._mouse_pointer:set_world_x(math.lerp(self._mouse_pointer:world_x(), self._pointer_lerp.x, pforce))
			self._mouse_pointer:set_world_y(math.lerp(self._mouse_pointer:world_y(), self._pointer_lerp.y, pforce))
		else
			self._mouse_pointer:set_world_x(math.lerp(self._mouse_pointer:world_x(), self._wanted_pointer.x, pforce))
			self._mouse_pointer:set_world_y(math.lerp(self._mouse_pointer:world_y(), self._wanted_pointer.y, pforce))
		end
	end

	if self._scanline then
		self._scanline:move(0, dt*50)

		if self.panel(self):h() + 25 <= self._scanline:top() then
			self._scanline:set_bottom(-25)
		end
	end

	if self._scanline_effect then
		local h = self._scanline_effect:h()*0.25*-1

		self._scanline_effect:move(0, dt*10)

		if h <= self._scanline_effect:top() then
			self._scanline_effect:set_top(self._scanline_effect:top() + h)
		end
	end

	local size_min, width_padding, height_padding, left, right, top, bottom, mleft, mright, mtop, mbottom = nil

	if self._released_map or not self._grabbed_map then
		size_min = math.min(self._panel:w(), self._panel:h())
		width_padding = (self._panel:w() - size_min)/2
		height_padding = (self._panel:h() - size_min)/2
		left = self._panel:w()*0
		right = self._panel:w()*1
		top = self._panel:h()*0
		bottom = self._panel:h()*1
		local floor_offset = self.floor_offset(self)
		mleft = -(left - self._grid_panel:left()) + floor_offset.x
		mright = -(right - self._grid_panel:right()) - floor_offset.w
		mtop = -(top - self._grid_panel:top()) + floor_offset.y
		mbottom = -(bottom - self._grid_panel:bottom()) - floor_offset.h

		if self._lerp_map then
			if -5 <= mleft or mright <= 5 then
				self._lerp_map.x = self._map_x
			end

			if -5 <= mtop or mbottom <= 5 then
				self._lerp_map.y = self._map_y
			end

			local speed = 5
			local step = dt*speed
			local dx = self._lerp_map.x - self._map_x
			local dy = self._lerp_map.y - self._map_y

			if dx ~= 0 or dy ~= 0 then
				local mx = (math.abs(dx) <= step and dx) or math.lerp(0, dx, step)
				local my = (math.abs(dy) <= step and dy) or math.lerp(0, dy, step)

				self._move_map_position(self, mx, my)

				if self._lerp_map.clbk and math.abs(mx) < 1 and math.abs(my) < 1 then
					self._lerp_map.x = self._map_x
					self._lerp_map.y = self._map_y
				end
			else
				if self._lerp_map.clbk then
					self._lerp_map.clbk()
				end

				self._lerp_map = nil
			end
		end

		if self._lerp_zoom then
			local speed = 10
			local step = dt*speed
			local dz = self._lerp_zoom - self._map_zoom

			if dz ~= 0 then
				local mz = (math.abs(dz) <= step and self._lerp_zoom) or math.lerp(self._map_zoom, self._lerp_zoom, step)

				self._set_zoom(self, mz, self._panel:w()/2, self._panel:h()/2)
			else
				self._lerp_zoom = nil
			end
		end
	else
		self._lerp_map = nil
		self._lerp_zoom = nil
	end

	if self._released_map then
		self._released_map.dx = math.lerp(self._released_map.dx, 0, dt*2)
		self._released_map.dy = math.lerp(self._released_map.dy, 0, dt*2)

		self._move_map_position(self, self._released_map.dx, self._released_map.dy)

		if -5 <= mleft or mright <= 5 then
			self._released_map.dx = 0
		end

		if -5 <= mtop or mbottom <= 5 then
			self._released_map.dy = 0
		end

		self._released_map.t = self._released_map.t - dt

		if self._released_map.t < 0 then
			self._released_map = nil
		end
	end

	if not self._grabbed_map then
		local speed = 2
		local step = dt*speed
		local padding = 25

		if -padding < mleft then
			local mx = math.lerp(0, -padding - mleft, step)

			self._move_map_position(self, mx, 0)

			if self._lerp_map and 0 < mx then
				self._lerp_map.x = self._map_x
			end
		end

		if mright < padding then
			local mx = math.lerp(0, padding - mright, step)

			self._move_map_position(self, mx, 0)

			if self._lerp_map and mx < 0 then
				self._lerp_map.x = self._map_x
			end
		end

		if -padding < mtop then
			local my = math.lerp(0, -padding - mtop, step)

			self._move_map_position(self, 0, my)

			if self._lerp_map and 0 < my then
				self._lerp_map.y = self._map_y
			end
		end

		if mbottom < padding then
			local my = math.lerp(0, padding - mbottom, step)

			self._move_map_position(self, 0, my)

			if self._lerp_map and my < 0 then
				self._lerp_map.y = self._map_y
			end
		end
	end

	self._one_frame_input_delay = false

	if alive(self._raid_colour_panel) then
		local ct = (math.sin(t*200) + 1)*0.5
		local color = math.lerp(Color(255, 128, 0, 0)/255, Color(255, 0, 0, 128)/255, ct)

		self._raid_colour_panel:set_color(color)
	end

	self.current_floor(self):set_zoom_value(self._map_zoom)

	return 
end
CustomSafehouseGuiPageMap.mouse_wheel_up = function (self, x, y)
	if self._one_scroll_in_delay then
		self._one_scroll_in_delay = nil
	end

	self.zoom_in(self, x, y)

	return true
end
CustomSafehouseGuiPageMap.mouse_wheel_down = function (self, x, y)
	if self._one_scroll_out_delay then
		self._one_scroll_out_delay = nil
	end

	self.zoom_out(self, x, y)

	return true
end
CustomSafehouseGuiPageMap.mouse_moved = function (self, o, x, y)
	if not self._active then
		return 
	end

	local inside_panel = self.panel(self):inside(x, y)
	local used = false
	local pointer = "arrow"
	self._mouse_moved = not self._last_x or not self._last_y or self._last_x ~= x or self._last_y ~= y
	self._last_x = x
	self._last_y = y

	for _, button in ipairs(self._buttons) do
		if button.inside(button, x, y) then
			button.set_selected(button, true)

			pointer = "link"
			used = true
		else
			button.set_selected(button, false)
		end
	end

	if not self.panel(self):inside(x, y) then
		return used, pointer
	end

	if self.is_being_raided(self) then
		return used, pointer
	end

	for i, button in ipairs(self._floor_control_buttons) do
		if not used and button.inside(button, x, y) and inside_panel then
			button.set_selected(button, true)

			pointer = "link"
			used = true
		elseif i ~= self._selected_floor then
			button.set_selected(button, false)
		end
	end

	if self.current_floor(self):is_static_overlay() then
		return used, pointer
	end

	if not used then
		local u, p = self.current_floor(self):mouse_moved(o, x, y)

		if u then
			pointer = p
			used = u
		end
	end

	if not used and self._grabbed_map then
		local left = self._grabbed_map.x < x
		local right = not left
		local up = self._grabbed_map.y < y
		local down = not up
		local mx = x - self._grabbed_map.x
		local my = y - self._grabbed_map.y

		table.insert(self._grabbed_map.dirs, 1, {
			mx,
			my
		})

		self._grabbed_map.dirs[10] = nil

		self._move_map_position(self, mx, my)

		self._grabbed_map.x = x
		self._grabbed_map.y = y

		return true, "grab"
	end

	if not used and self._panel:inside(x, y) then
		pointer = "hand"
		used = true
	end

	return used, pointer
end
CustomSafehouseGuiPageMap.mouse_pressed = function (self, button, x, y)
	if (button ~= Idstring("0") and button ~= Idstring("1")) or not self._active then
		return 
	end

	local ret = nil

	for _, button in ipairs(self._buttons) do
		if button.inside(button, x, y) then
			button.trigger(button)

			ret = true
		end
	end

	if not self.panel(self):inside(x, y) then
		return ret
	end

	if self.is_being_raided(self) then
		return ret
	end

	for i, button in ipairs(self._floor_control_buttons) do
		if button.inside(button, x, y) then
			self._current_floor_button = i

			button.trigger(button)

			return true
		end
	end

	if self.current_floor(self):is_static_overlay() then
		return ret
	end

	ret = self.current_floor(self):mouse_pressed(button, x, y) or ret

	if not ret and self._panel:inside(x, y) then
		self._released_map = nil
		self._grabbed_map = {
			x = x,
			y = y,
			dirs = {}
		}
		ret = true
	end

	return ret
end
CustomSafehouseGuiPageMap.mouse_released = function (self, o, button, x, y)
	if button ~= Idstring("0") then
		return 
	end

	if self._grabbed_map and 0 < #self._grabbed_map.dirs then
		local dx = 0
		local dy = 0

		for _, values in ipairs(self._grabbed_map.dirs) do
			dx = dx + values[1]
			dy = dy + values[2]
		end

		dx = dx/#self._grabbed_map.dirs
		dy = dy/#self._grabbed_map.dirs
		self._released_map = {
			t = 2,
			dx = dx,
			dy = dy
		}
		self._grabbed_map = nil
	end

	return 
end
CustomSafehouseGuiPageMap._move_map_position = function (self, mx, my)
	self._set_map_position(self, self._map_x + mx, self._map_y + my)

	return 
end
CustomSafehouseGuiPageMap._set_zoom = function (self, zoom, x, y, ignore_update)
	self._lerp_map = nil
	local new_zoom = math.clamp(zoom, 0, 1)

	if self.current_zoom(self, new_zoom) ~= self._last_zoom then
		local w1, h1 = self._map_panel:size()
		local wx1 = (x - self._map_x)/w1
		local wy1 = (y - self._map_y)/h1
		self._map_zoom = new_zoom

		self._map_panel:set_size(self._map_size*self.current_zoom(self), self._map_size*self.current_zoom(self))
		self._grid_panel:set_size(self._map_size*self.current_zoom(self), self._map_size*self.current_zoom(self))

		local w2, h2 = self._map_panel:size()

		self._move_map_position(self, (w1 - w2)*wx1, (h1 - h2)*wy1)
		self.current_floor(self):update()

		self._last_zoom = self.current_zoom(self)

		if self.current_floor(self):is_static_overlay() then
			return false
		end

		return true
	else
		self._one_scroll_in_delay = true
	end

	return false
end
CustomSafehouseGuiPageMap._change_zoom = function (self, zoom, x, y)
	return self._set_zoom(self, self._map_zoom + zoom, x, y)
end
CustomSafehouseGuiPageMap.zoom_out = function (self, x, y)
	if self.is_being_raided(self) then
		return 
	end

	if self._change_zoom(self, -0.05, x, y) then
		managers.menu_component:post_event("zoom_out")
	end

	return 
end
CustomSafehouseGuiPageMap.zoom_in = function (self, x, y)
	if self.is_being_raided(self) then
		return 
	end

	if self._change_zoom(self, 0.05, x, y) then
		managers.menu_component:post_event("zoom_in")
	end

	return 
end
CustomSafehouseGuiPageMap.min_zoom = function (self)
	return self.current_floor(self):min_zoom() or self._min_zoom
end
CustomSafehouseGuiPageMap.max_zoom = function (self)
	return self.current_floor(self):max_zoom() or self._max_zoom
end
CustomSafehouseGuiPageMap.current_zoom = function (self, custom_zoom)
	return (custom_zoom or self._map_zoom)*(self.max_zoom(self) - self.min_zoom(self)) + self.min_zoom(self)
end
CustomSafehouseGuiPageMap._set_map_position = function (self, x, y, location)
	self._map_panel:set_position(x, y)
	self._grid_panel:set_center(self._map_panel:center())

	local floor_offset = self.floor_offset(self)
	local left = self._panel:w()*0 - floor_offset.x
	local right = self._panel:w()*1 + floor_offset.w
	local top = self._panel:h()*0 - floor_offset.y
	local bottom = self._panel:h()*1 + floor_offset.h

	if left - self._grid_panel:left() < 0 then
		self._map_panel:move(left - self._grid_panel:left(), 0)
	end

	if 0 < right - self._grid_panel:right() then
		self._map_panel:move(right - self._grid_panel:right(), 0)
	end

	if top - self._grid_panel:top() < 0 then
		self._map_panel:move(0, top - self._grid_panel:top())
	end

	if 0 < bottom - self._grid_panel:bottom() then
		self._map_panel:move(0, bottom - self._grid_panel:bottom())
	end

	self._map_x, self._map_y = self._map_panel:position()

	self._grid_panel:set_center(self._map_panel:center())

	return 
end
CustomSafehouseMapFloor = CustomSafehouseMapFloor or class()
CustomSafehouseMapFloor.init = function (self, panel, map_panel, tweak)
	if tweak.static_overlay then
		if tweak.video then
			self._bg = panel.video(panel, {
				loop = true,
				layer = 10,
				video = tweak.static_overlay
			})
		else
			self._bg = panel.bitmap(panel, {
				layer = 10,
				texture = tweak.static_overlay
			})
		end

		local aspect = self._bg:w()/self._bg:h()
		local panel_aspect = panel.w(panel)/panel.h(panel)

		if panel_aspect < aspect then
			self._bg:set_h(panel.h(panel))
			self._bg:set_w(panel.h(panel)*aspect)
		else
			self._bg:set_w(panel.w(panel))
			self._bg:set_h(panel.w(panel)*aspect)
		end

		self._bg:set_center(panel.w(panel)/2, panel.h(panel)/2)

		self._static_overlay = true
	else
		self._bg = map_panel.bitmap(map_panel, {
			alpha = 0.5,
			halign = "scale",
			layer = -1,
			blend_mode = "add",
			valign = "scale",
			texture = tweak.texture,
			w = map_panel.w(map_panel)/2,
			h = map_panel.h(map_panel)/2
		})

		self._bg:set_center(map_panel.w(map_panel)/2, map_panel.h(map_panel)/2)
	end

	self._name_id = tweak.name_id
	self._desc_id = tweak.desc_id
	self._alpha_limit = tweak.alpha_limit or 0.1
	self._min_zoom = tweak.min_zoom
	self._max_zoom = tweak.max_zoom
	self._start_zoom = tweak.start_zoom

	if not managers.menu:is_pc_controller() and tweak.controller_shape then
		self._x, self._y, self._w, self._h = unpack(tweak.controller_shape)
	elseif tweak.shape then
		self._x, self._y, self._w, self._h = unpack(tweak.shape)
	end

	self._disable_cursor = tweak.disable_cursor
	self._points = {}

	return 
end
CustomSafehouseMapFloor.is_static_overlay = function (self)
	return self._static_overlay
end
CustomSafehouseMapFloor.should_disable_cursor = function (self)
	return self._disable_cursor
end
CustomSafehouseMapFloor.min_zoom = function (self)
	return self._min_zoom
end
CustomSafehouseMapFloor.max_zoom = function (self)
	return self._max_zoom
end
CustomSafehouseMapFloor.start_zoom = function (self)
	return self._start_zoom
end
CustomSafehouseMapFloor.x = function (self)
	return self._x
end
CustomSafehouseMapFloor.y = function (self)
	return self._y
end
CustomSafehouseMapFloor.w = function (self)
	return self._w
end
CustomSafehouseMapFloor.h = function (self)
	return self._h
end
CustomSafehouseMapFloor.name_id = function (self)
	return self._name_id
end
CustomSafehouseMapFloor.desc_id = function (self)
	return self._desc_id
end
CustomSafehouseMapFloor.add_point = function (self, point)
	table.insert(self._points, point)

	return 
end
CustomSafehouseMapFloor.points = function (self)
	return self._points
end
CustomSafehouseMapFloor.hide = function (self)
	self._bg:hide()

	for _, point in ipairs(self._points) do
		point.hide(point)
	end

	return 
end
CustomSafehouseMapFloor.show = function (self)
	self._bg:show()

	for _, point in ipairs(self._points) do
		point.show(point)
	end

	return 
end
CustomSafehouseMapFloor.update = function (self)
	for _, point in ipairs(self._points) do
		point.update(point)
	end

	return 
end
CustomSafehouseMapFloor.refresh = function (self)
	for _, point in ipairs(self._points) do
		point.refresh(point)
	end

	return 
end
CustomSafehouseMapFloor.confirm_pressed = function (self)
	for _, point in ipairs(self._points) do
		if point.is_selected(point) then
			point.attempt_purchase(point)

			return 
		end
	end

	return 
end
CustomSafehouseMapFloor.mouse_moved = function (self, o, x, y)
	local used, pointer = nil

	for _, point in ipairs(self._points) do
		local u, p = point.mouse_moved(point, o, x, y)

		if u then
			pointer = p
			used = u
		end
	end

	return used, pointer
end
CustomSafehouseMapFloor.mouse_pressed = function (self, button, x, y)
	local ret = nil

	for _, point in ipairs(self._points) do
		ret = point.mouse_pressed(point, button, x, y) or ret
	end

	return ret
end
CustomSafehouseMapFloor.set_zoom_value = function (self, zoom)
	for _, point in ipairs(self._points) do
		point.set_zoom_value(point, zoom, self._alpha_limit)
	end

	return 
end
CustomSafehouseMapPoint = CustomSafehouseMapPoint or class()
CustomSafehouseMapPoint.WIDTH = 64
CustomSafehouseMapPoint.HEIGHT = 64
CustomSafehouseMapPoint.FRAME_WIDTH = 96
CustomSafehouseMapPoint.FRAME_HEIGHT = 96
CustomSafehouseMapPoint.PADDING = 2
CustomSafehouseMapPoint.colors = {
	selected = tweak_data.screen_colors.button_stage_2:with_alpha(1),
	locked = Color.white:with_alpha(0.25),
	unlocked = Color.white,
	unavailable = tweak_data.screen_colors.important_1,
	current = tweak_data.screen_colors.button_stage_3:with_alpha(1)
}
CustomSafehouseMapPoint.init = function (self, parent, map_panel, id)
	self.make_fine_text = BlackMarketGui.make_fine_text
	self._parent = parent
	self._id = id
	self._room_data = table.find_value(tweak_data.safehouse.rooms, function (v)
		return v.room_id == id
	end)
	self._name_id = self._room_data.name_id
	self._map_width = map_panel.w(map_panel)
	self._map_height = map_panel.h(map_panel)
	self._map_panel = map_panel
	self._current_size_mod = 1
	self._alpha = 1
	self._current_tier = managers.custom_safehouse:get_room_current_tier(self._id)
	local tweak_table = tweak_data.safehouse.map.rooms[id]
	self._x = tweak_table.x/self._map_width
	self._y = tweak_table.y/self._map_height
	self._panel = map_panel.panel(map_panel, {
		valign = "scale",
		halign = "scale"
	})
	local title_text = managers.localization:to_upper_text(self._name_id)
	self._title = self._panel:text({
		name = "title",
		text = title_text,
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		color = tweak_data.screen_colors.button_stage_2
	})

	self.make_fine_text(self, self._title)
	self._title:set_top(0)

	local path = "guis/dlcs/chill/textures/pd2/character_icon/"

	if tweak_table.path then
		path = tweak_table.path
	end

	self._image = self._panel:bitmap({
		layer = 12,
		texture = path .. tweak_table.icon,
		w = CustomSafehouseMapPoint.WIDTH,
		h = CustomSafehouseMapPoint.HEIGHT
	})

	self._image:set_top(self._title:bottom() + CustomSafehouseMapPoint.PADDING)

	self._frame_image = self._panel:bitmap({
		layer = 13,
		texture = tweak_data.safehouse.map.frame_texture[1],
		w = CustomSafehouseMapPoint.FRAME_WIDTH,
		w = CustomSafehouseMapPoint.FRAME_HEIGHT
	})

	self.set_tier_image(self, self._current_tier)

	local _, _, text_w = self._title:text_rect()

	self._panel:set_h(self._frame_image:bottom())
	self._panel:set_w(math.max(CustomSafehouseMapPoint.FRAME_WIDTH, text_w))
	self._panel:set_center(self._map_width*self._x, self._map_height*self._y)

	self._width = self._panel:w()
	self._height = self._panel:h()

	self._image:set_center_x(self._panel:w()/2)
	self._title:set_center_x(self._image:center_x())
	self._frame_image:set_center_x(self._image:center_x())
	self._frame_image:set_center_y(self._image:center_y())
	self._image:animate(anim_deselect, CustomSafehouseMapPoint.WIDTH, CustomSafehouseMapPoint.HEIGHT)
	self._frame_image:animate(anim_deselect, CustomSafehouseMapPoint.FRAME_WIDTH, CustomSafehouseMapPoint.FRAME_HEIGHT)

	return 
end
CustomSafehouseMapPoint.set_tier_image = function (self, tier)
	self._frame_image:set_image(tweak_data.safehouse.map.frame_texture[tier])

	return 
end
CustomSafehouseMapPoint.refresh = function (self)
	self._current_tier = managers.custom_safehouse:get_room_current_tier(self._id)

	self.set_tier_image(self, self._current_tier)

	if self._selected == self._image or self.is_selected(self) then
		self._image:animate(anim_select, CustomSafehouseMapPoint.WIDTH, CustomSafehouseMapPoint.HEIGHT)
		self._frame_image:animate(anim_select, CustomSafehouseMapPoint.FRAME_WIDTH, CustomSafehouseMapPoint.FRAME_HEIGHT)
	else
		self._image:animate(anim_deselect, CustomSafehouseMapPoint.WIDTH, CustomSafehouseMapPoint.HEIGHT)
		self._frame_image:animate(anim_deselect, CustomSafehouseMapPoint.FRAME_WIDTH, CustomSafehouseMapPoint.FRAME_HEIGHT)
	end

	return 
end
CustomSafehouseMapPoint.update = function (self)
	self._update_position(self)

	return 
end
CustomSafehouseMapPoint.hide = function (self)
	self._panel:hide()

	return 
end
CustomSafehouseMapPoint.show = function (self)
	self._panel:show()

	return 
end
CustomSafehouseMapPoint._update_position = function (self)
	local diff_width = self._map_width/self._map_panel:width() - 1
	local diff_height = self._map_height/self._map_panel:height() - 1

	self._panel:set_width(self._width*(self._current_size_mod or 1))
	self._panel:set_height(self._height*(self._current_size_mod or 1))
	self._panel:set_center(self._map_panel:w()*self._x, self._map_panel:h()*self._y)

	return 
end
CustomSafehouseMapPoint.mouse_moved = function (self, o, x, y)
	local used, pointer, new_selected = nil

	if self._image:inside(x, y) then
		new_selected = self._image
		pointer = "link"
		used = true
	end

	if self._selected ~= new_selected then
		if self._selected == self._image then
			self.set_selected(self, false)
		elseif new_selected == self._image then
			self.set_selected(self, true)
			managers.menu_component:post_event("highlight")
		end
	end

	self._selected = new_selected

	return used, pointer
end
CustomSafehouseMapPoint.mouse_pressed = function (self, button, x, y)
	if button ~= Idstring("0") then
		return 
	end

	local ret = nil

	if self._selected == self._image then
		self.attempt_purchase(self)

		ret = true
	end

	return ret
end
CustomSafehouseMapPoint.set_selected = function (self, selected)
	self._is_selected = selected

	if selected then
		self._image:animate(anim_select, CustomSafehouseMapPoint.WIDTH, CustomSafehouseMapPoint.HEIGHT)
		self._frame_image:animate(anim_select, CustomSafehouseMapPoint.FRAME_WIDTH, CustomSafehouseMapPoint.FRAME_HEIGHT)
		self.update_help_text(self, self._current_tier)

		if managers.custom_safehouse:get_room_current_tier(self._id) == #tweak_data.safehouse.prices.rooms then
			self._parent:set_top_help_text("menu_cs_rooms_help_hover_no_upgrade")
		else
			self._parent:set_top_help_text("menu_cs_rooms_help_hover")
		end
	else
		self._image:animate(anim_deselect, CustomSafehouseMapPoint.WIDTH, CustomSafehouseMapPoint.HEIGHT)
		self._frame_image:animate(anim_deselect, CustomSafehouseMapPoint.FRAME_WIDTH, CustomSafehouseMapPoint.FRAME_HEIGHT)
	end

	return 
end
CustomSafehouseMapPoint.is_selected = function (self)
	return self._is_selected
end
CustomSafehouseMapPoint.select_tier = function (self, id)
	managers.custom_safehouse:set_room_tier(self._id, id)
	managers.menu_component:custom_safehouse_gui():call_refresh()

	return 
end
CustomSafehouseMapPoint.attempt_purchase = function (self, step)
	step = step or 0
	local next_tier = managers.custom_safehouse:get_next_tier_unlocked(self._id) + step

	if next_tier <= managers.custom_safehouse:get_room_max_tier(self._id) then
		local can_afford = managers.custom_safehouse:can_afford_room_tier(self._id, next_tier)

		if can_afford then
			local dialog_macros = {
				character = managers.localization:text(self._name_id),
				cost = managers.custom_safehouse:get_upgrade_cost(self._id, next_tier)
			}
			local dialog_data = {
				title = managers.localization:text("dialog_bm_weapon_buy_title"),
				text = managers.localization:text("dialog_upgrade_safehouse", dialog_macros),
				focus_button = 2
			}
			local room_data = table.find_value(tweak_data.safehouse.rooms, function (v)
				return v.room_id == self._id
			end)
			dialog_data.texture = room_data and room_data.images[next_tier]

			if dialog_data.texture then
				dialog_data.w = 620
				dialog_data.h = 532
				dialog_data.image_w = 560
				dialog_data.image_h = 300
				dialog_data.image_valign = "top"
				dialog_data.image_halign = "center"
			end

			dialog_data.text = managers.localization:text(room_data.help_id .. "_" .. next_tier) .. "\n\n" .. dialog_data.text
			local yes_button = {
				text = managers.localization:text("dialog_yes"),
				callback_func = callback(self, self, "_confirm_purchase", {
					tier = next_tier
				})
			}
			local no_button = {
				text = managers.localization:text("dialog_no"),
				cancel_button = true
			}
			dialog_data.button_list = {
				yes_button,
				no_button
			}

			if next_tier ~= managers.custom_safehouse:get_room_max_tier(self._id) then
				local next_button = {
					text = managers.localization:text("dialog_next_tier"),
					callback_func = callback(self, self, "attempt_purchase", step + 1)
				}

				table.insert(dialog_data.button_list, next_button)
			end

			if 0 < step then
				local previous_button = {
					text = managers.localization:text("dialog_previous_tier"),
					callback_func = callback(self, self, "attempt_purchase", step - 1)
				}

				table.insert(dialog_data.button_list, previous_button)
			end

			managers.system_menu:show_new_unlock(dialog_data)
		else
			self.invalid_purchase(self, true)
		end
	end

	return 
end
CustomSafehouseMapPoint._confirm_purchase = function (self, data)
	local can_afford = managers.custom_safehouse:can_afford_tier(managers.custom_safehouse:get_next_tier_unlocked(self._id))

	if can_afford then
		managers.custom_safehouse:purchase_room_tier(self._id, data.tier)
		managers.custom_safehouse:set_room_tier(self._id, data.tier)
		managers.menu_component:custom_safehouse_gui():call_refresh()
		managers.menu_component:post_event("chill_upgrade_stinger")

		local effect_panel = self._panel:panel({
			layer = 100
		})

		SimpleGUIEffectSpewer.infamous_up(self._image:center_x(), self._image:center_y(), effect_panel)
		self.update_help_text(self, data.tier)
	end

	return 
end
CustomSafehouseMapPoint.invalid_purchase = function (self, sound)
	if sound then
		managers.menu_component:post_event("menu_error")
	end

	self._image:animate(anim_invalid)

	return 
end
CustomSafehouseMapPoint.update_help_text = function (self, tier_id)
	local next_tier = math.min(tier_id + 1, managers.custom_safehouse:get_room_max_tier(self._id))

	self._parent:set_title_text(utf8.to_upper(managers.localization:text(self._room_data.title_id)))
	self._parent:set_room_image(self._room_data.images[tier_id])

	local help_text = managers.localization:text(self._room_data.help_id .. "_" .. tier_id)

	if managers.custom_safehouse:is_room_tier_unlocked(self._id, next_tier) then
		if next_tier == #tweak_data.safehouse.prices.rooms then
			help_text = help_text .. "\n\n" .. managers.localization:text("menu_cs_upgrade_max")
		else
			help_text = help_text .. "\n\n" .. ((next_tier == 1 and "") or managers.localization:text("menu_cs_upgrade_owned"))
		end

		self._parent:set_help_text(help_text)
		self._parent:set_warning_text(nil)
	else
		local macros = {
			cost = tweak_data.safehouse.prices.rooms[next_tier]
		}
		help_text = help_text .. "\n\n" .. managers.localization:text("menu_cs_upgrade_cost", macros)

		self._parent:set_help_text(help_text)
		self._parent:set_warning_text(nil)

		if managers.custom_safehouse:get_next_tier_unlocked(self._id) < next_tier then
			self._parent:set_warning_text(utf8.to_upper(managers.localization:text("menu_cs_unlock_prev_tier")))
		elseif not managers.custom_safehouse:can_afford_tier(next_tier) then
			self._parent:set_warning_text(utf8.to_upper(managers.localization:text("menu_cs_cant_afford")))
		end
	end

	return 
end
CustomSafehouseMapPoint.set_zoom_value = function (self, zoom, alpha_limit)
	self._alpha = math.clamp((zoom - alpha_limit)*10, 0, 1)

	self._title:set_alpha(self._alpha)

	return 
end
CustomSafehouseGuiRaidButton = CustomSafehouseGuiRaidButton or class(CustomSafehouseGuiItem)
CustomSafehouseGuiRaidButton.init = function (self, panel, layer, y, callback)
	CustomSafehouseGuiRaidButton.super.init(self)

	self._color = tweak_data.screen_colors.button_stage_3
	self._selected_color = tweak_data.screen_colors.button_stage_2
	self._callback = callback
	self._panel = panel.panel(panel, {
		h = medium_font_size + 8,
		y = y,
		layer = layer
	})
	self._background_panel = self._panel:rect({
		alpha = 0.4,
		color = Color.black
	})
	self._text = self._panel:text({
		name = "defend_text",
		align = "center",
		y = 4,
		layer = 10,
		text = managers.localization:to_upper_text("menu_cn_chill_combat_defend"),
		w = panel.w(panel),
		h = medium_font_size,
		font = medium_font,
		font_size = medium_font_size,
		color = self._color
	})

	return 
end
CustomSafehouseGuiRaidButton.set_text = function (self, text)
	self._text:set_text(utf8.to_upper(text))

	local _, _, w, h = self._text:text_rect()

	self._text:set_h(h)
	self._background_panel:set_h(h)

	return 
end
CustomSafehouseGuiRaidButton.text = function (self)
	return self._text:text()
end
CustomSafehouseGuiRaidButton.inside = function (self, x, y)
	return self._background_panel:inside(x, y)
end
CustomSafehouseGuiRaidButton.show = function (self)
	self.set_selected(self, true, true)

	return 
end
CustomSafehouseGuiRaidButton.hide = function (self)
	self.set_selected(self, false)

	return 
end
CustomSafehouseGuiRaidButton.visible = function (self)
	return self._panel:visible()
end
CustomSafehouseGuiRaidButton.refresh = function (self)
	if self._selected then
		self.show(self)
	else
		self.hide(self)
	end

	return 
end
CustomSafehouseGuiRaidButton.trigger = function (self)
	CustomSafehouseGuiRaidButton.super.trigger(self)
	self._callback()

	return 
end
CustomSafehouseGuiRaidButton.set_color = function (self, color, selected_color)
	self._color = color or self._color
	self._selected_color = selected_color or color or self._selected_color

	self.set_selected(self, self._selected, false)

	return 
end
CustomSafehouseGuiRaidButton.set_selected = function (self, selected, play_sound)
	CustomSafehouseGuiRaidButton.super.set_selected(self, selected, play_sound)

	if selected then
		self._text:set_color(self._selected_color)
	else
		self._text:set_color(self._color)
	end

	return 
end

return 
