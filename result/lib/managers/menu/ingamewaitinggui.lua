IngameWaitingGui = IngameWaitingGui or class()
local PADDING = 10
local text_font = tweak_data.menu.pd2_medium_font
local text_font_size = tweak_data.menu.pd2_medium_font_size
local font_color_highlighted = tweak_data.screen_colors.button_stage_2
local font_color_rest = tweak_data.screen_colors.button_stage_3

local function make_fine_text(text)
	local x, y, w, h = text.text_rect(text)

	text.set_size(text, w, h)
	text.set_position(text, math.round(text.x(text)), math.round(text.y(text)))

	return 
end

IngameWaitingGui.init = function (self, ws)
	self._panel = ws.panel(ws)
	self._current_btn = {}
	self._buttons = {}
	self._saved_names = {}
	self._text_panel = self._panel:panel()
	self._header_text = self._text_panel:text({
		align = "right",
		text = managers.localization:text("menu_waiting_peer_info"),
		font = text_font,
		font_size = text_font_size
	})

	make_fine_text(self._header_text)
	self._text_panel:set_w(self._header_text:w())
	self._text_panel:set_h(self._header_text:h() + 2)

	self._content_panel = self._panel:panel()
	self._background = self._content_panel:rect({
		alpha = 0.25,
		layer = -1,
		color = Color(255, 0, 170, 255)/255
	})
	local color = tweak_data.screen_colors.text
	self._info_panel = self._content_panel:panel({
		name = "self._info_panel",
		h = 42,
		x = PADDING,
		y = PADDING
	})
	local detection = self._info_panel:panel({
		w = 42,
		name = "detection",
		h = 42,
		visible = false
	})
	local detection_ring_left_bg = detection.bitmap(detection, {
		blend_mode = "add",
		name = "detection_left_bg",
		alpha = 0.2,
		texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
		w = detection.w(detection),
		h = detection.h(detection)
	})
	local detection_ring_right_bg = detection.bitmap(detection, {
		blend_mode = "add",
		name = "detection_right_bg",
		alpha = 0.2,
		texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
		w = detection.w(detection),
		h = detection.h(detection)
	})

	detection_ring_right_bg.set_texture_rect(detection_ring_right_bg, detection_ring_right_bg.texture_width(detection_ring_right_bg), 0, -detection_ring_right_bg.texture_width(detection_ring_right_bg), detection_ring_right_bg.texture_height(detection_ring_right_bg))

	local detection_ring_left = detection.bitmap(detection, {
		blend_mode = "add",
		name = "detection_left",
		texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
		render_template = "VertexColorTexturedRadial",
		layer = 1,
		w = detection.w(detection),
		h = detection.h(detection)
	})
	local detection_ring_right = detection.bitmap(detection, {
		blend_mode = "add",
		name = "detection_right",
		texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
		render_template = "VertexColorTexturedRadial",
		layer = 1,
		w = detection.w(detection),
		h = detection.h(detection)
	})

	detection_ring_right.set_texture_rect(detection_ring_right, detection_ring_right.texture_width(detection_ring_right), 0, -detection_ring_right.texture_width(detection_ring_right), detection_ring_right.texture_height(detection_ring_right))

	local detection_value = self._info_panel:text({
		text = "",
		name = "detection_value",
		align = "center",
		blend_mode = "add",
		vertical = "center",
		font_size = text_font_size,
		font = text_font,
		color = color
	})

	detection_value.set_center_x(detection_value, detection.left(detection) + detection.w(detection)/2)
	detection_value.set_center_y(detection_value, detection.top(detection) + detection.h(detection)/2 + 2)
	detection_value.set_visible(detection_value, detection.visible(detection))

	self._detection = detection
	self._detection_value = detection_value
	self._name_text = self._info_panel:text({
		text = "",
		name = "peer_name",
		align = "left",
		vertical = "center",
		font_size = text_font_size,
		font = text_font,
		color = color
	})

	self._name_text:set_left(detection.right(detection) + 5)
	self._name_text:set_center_y(detection_value.center_y(detection_value))

	self._loadout_bottom = self._info_panel:bottom() + 128 + PADDING
	self._loadout_width = 448
	self._button_panel = self._content_panel:panel({
		x = self._info_panel:x(),
		y = self._loadout_bottom + PADDING,
		h = text_font_size
	})
	self._peer_btns_panel = self._button_panel:panel({
		w = 0
	})
	self._peer_btns = {
		self.add_button(self, self._peer_btns_panel, "hud_waiting_accept", "drop_in_accept", "spawn", 30),
		self.add_button(self, self._peer_btns_panel, "hud_waiting_return", "drop_in_return", "return_back", 30),
		self.add_button(self, self._peer_btns_panel, "hud_waiting_kick", "drop_in_kick", "kick")
	}
	self._left_btns_panel = self._button_panel:panel({
		w = 0
	})
	self._left_btns = {
		self.add_button(self, self._left_btns_panel, "hud_waiting_ok", "drop_in_accept", "left_ok")
	}

	self._content_panel:set_h(self._button_panel:bottom() + PADDING)
	self._content_panel:set_w(self._loadout_width + PADDING*2)
	BoxGuiObject:new(self._content_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	self._prev_panel = self._content_panel:panel()
	self._next_panel = self._content_panel:panel()
	self._prev_arrow = IngameWaitingButton:new(self._prev_panel, "<", nil, callback(self, self, "next_page"))
	self._next_arrow = IngameWaitingButton:new(self._next_panel, ">", nil, callback(self, self, "prev_page"))

	BoxGuiObject:new(self._prev_panel, {
		sides = {
			2,
			0,
			0,
			0
		}
	})
	BoxGuiObject:new(self._next_panel, {
		sides = {
			0,
			2,
			0,
			0
		}
	})
	self._prev_arrow:panel():set_top(self._button_panel:top())
	self._next_arrow:panel():set_top(self._button_panel:top())
	self._prev_arrow:panel():set_left(PADDING)
	self._next_arrow:panel():set_right(self._content_panel:w() - PADDING)
	self._button_panel:set_w(self._next_arrow:panel():left() - self._prev_arrow:panel():right())
	self._button_panel:set_left(self._prev_arrow:panel():right())
	self._peer_btns_panel:set_center_x(self._button_panel:w()/2)
	self._left_btns_panel:set_center_x(self._button_panel:w()/2)
	self._content_panel:set_right(self._panel:w())

	if managers.menu_component._ingame_contract_gui then
		self._content_panel:set_world_bottom(managers.menu_component._ingame_contract_gui._panel:world_bottom())
	else
		self._content_panel:set_bottom(self._panel:h() - PADDING*3)
	end

	self._text_panel:set_rightbottom(self._content_panel:righttop())
	self.try_get_dummy(self)

	return 
end
IngameWaitingGui.close = function (self)
	if self._panel and alive(self._panel) then
		self._panel:remove(self._content_panel)
		self._panel:remove(self._text_panel)
	end

	return 
end
IngameWaitingGui.calc_next = function (self, current)
	current = current or self._current_slot

	if not current then
		return nil
	end

	current = current + 1

	for id = current, CriminalsManager.MAX_NR_CRIMINALS, 1 do
		local peer = managers.network:session():peer(id)

		if peer and managers.wait:is_waiting(id) then
			self._saved_names[id] = peer.name(peer)

			return id
		elseif self._saved_names[id] then
			return id
		end
	end

	return nil
end
IngameWaitingGui.calc_prev = function (self, current)
	current = current or self._current_slot

	if not current then
		return nil
	end

	current = current - 1

	for id = current, 1, -1 do
		local peer = managers.network:session():peer(id)

		if peer and managers.wait:is_waiting(id) then
			self._saved_names[id] = peer.name(peer)

			return id
		elseif self._saved_names[id] then
			return id
		end
	end

	return nil
end
IngameWaitingGui.left_ok = function (self)
	if not self._current_slot then
		return 
	end

	self.check_remove_panel(self, self._current_slot)

	return 
end
IngameWaitingGui.kick = function (self)
	if not self._peer then
		return 
	end

	managers.vote:message_host_kick(self._peer)

	return 
end
IngameWaitingGui.return_back = function (self)
	if not self._peer then
		return 
	end

	managers.wait:kick_to_briefing(self._peer:id())
	self.check_remove_panel(self, self._peer:id())

	return 
end
IngameWaitingGui.spawn = function (self)
	if not self._peer then
		return 
	end

	managers.wait:spawn_waiting(self._peer:id())
	self.check_remove_panel(self, self._peer:id())

	return 
end
IngameWaitingGui.check_remove_panel = function (self, slot_id)
	self._saved_names[slot_id] = nil

	if slot_id == self._current_slot then
		local peer = managers.network:session():peer(self._current_slot)

		if self._peer ~= peer and managers.wait:is_waiting(peer.id(peer)) then
			self.set_panel_for(self, self._current_slot, true)
		else
			self.set_panel_for(self, self.calc_next(self) or self.calc_prev(self), true)
		end
	end

	return 
end
IngameWaitingGui.add_button = function (self, panel, text, binding, func_name, padding)
	local params = {
		MY_BTN = managers.localization:btn_macro(binding, true, true) or ""
	}
	text = managers.localization:text(text, params)
	local btn, btn_panel = IngameWaitingButton:new(panel, text, binding, callback(self, self, func_name))

	btn_panel.set_x(btn_panel, panel.w(panel))
	panel.set_w(panel, btn_panel.right(btn_panel) + (padding or 0))

	return btn
end
IngameWaitingGui.set_layer = function (self, layer)
	self._content_panel:set_layer(layer)
	self._text_panel:set_layer(layer)

	return 
end
IngameWaitingGui.try_get_dummy = function (self)
	local active_node_gui = managers.menu:active_menu().renderer:active_node_gui()

	if active_node_gui then
		self._dummy_item = active_node_gui.row_item_by_name(active_node_gui, "drop_in_dummy")

		self._dummy_item.item:set_actual_item(self)

		local id = self.calc_next(self, 1)

		if not id then
			self.set_enabled(self, false)
		else
			self.set_enabled(self, true)
			self.set_panel_for(self, id)
		end
	end

	return self._dummy_item
end
IngameWaitingGui.game_state_blacklist = {
	victoryscreen = true,
	gameoverscreen = true
}
IngameWaitingGui.update = function (self, t, dt)
	if not self._dummy_item then
		self.try_get_dummy(self)
	end

	if self.game_state_blacklist[game_state_machine:last_queued_state_name()] then
		self.set_enabled(self, false)

		return 
	end

	if not self._content_panel:visible() or not self._current_slot then
		local valid_slot = self.calc_next(self, 1)

		if valid_slot then
			self.set_enabled(self, true)
			self.set_panel_for(self, valid_slot)
		else
			return 
		end
	end

	if self._peer and self._peer ~= managers.network:session():peer(self._current_slot) then
		self.set_panel_for(self, self._current_slot)
	end

	self._next_panel:set_visible(self.calc_next(self))
	self._prev_panel:set_visible(self.calc_prev(self))

	return 
end
IngameWaitingGui.set_enabled = function (self, enabled)
	enabled = not not enabled

	self._content_panel:set_visible(enabled)
	self._text_panel:set_visible(enabled)

	self._peer = (enabled and self._peer) or nil

	if self._dummy_item then
		self._dummy_item.item.no_select = not enabled
	end

	return 
end
IngameWaitingGui.set_panel_for = function (self, peer_id)
	self._current_slot = peer_id
	self._peer = managers.network:session():peer(peer_id)

	if peer_id == nil then
		self.set_enabled(self, false)

		return 
	end

	if self._loadout_panel then
		self._content_panel:remove(self._loadout_panel)
	end

	self._buttons = {}

	if not self._peer or (not managers.wait:is_waiting(self._peer:id()) and not peer_id == 1) then
		local name = self._saved_names[peer_id] or "N/A"
		local str = managers.localization:text("menu_waiting_peer_left", {
			name = name
		})
		self._loadout_panel = self._content_panel:panel()

		self._detection:set_visible(false)
		self._detection_value:set_visible(false)
		self._name_text:set_visible(false)

		local text = self._loadout_panel:text({
			vertical = "center",
			align = "left",
			text = str,
			font = text_font,
			font_size = text_font_size
		})

		make_fine_text(text)
		text.set_world_left(text, self._detection:world_left())
		text.set_world_center_y(text, self._name_text:world_center_y())

		self._buttons = self._left_btns

		self._button_panel:set_visible(true)
		self._left_btns_panel:set_visible(true)
		self._peer_btns_panel:set_visible(false)
		self.set_current_button(self, math.clamp(1, self._current_btn.index, #self._buttons))

		return 
	end

	self._saved_names[peer_id] = self._peer:name()
	local kit_menu = managers.menu:get_menu("kit_menu")

	if not kit_menu or not kit_menu.renderer then
		return 
	end

	local kit_slot = kit_menu.renderer:get_player_slot_by_peer_id(peer_id)
	local outfit = kit_slot and kit_slot.outfit

	if not outfit then
		return 
	end

	self._buttons = self._peer_btns

	self._left_btns_panel:set_visible(false)
	self._peer_btns_panel:set_visible(true)
	self.set_current_button(self, math.clamp(1, self._current_btn.index, #self._buttons))

	local current, reached = managers.blackmarket:get_suspicion_offset_of_peer(self._peer, tweak_data.player.SUSPICION_OFFSET_LERP or 0.75)

	self._detection:child("detection_left"):set_color(Color(current*0.5 + 0.5, 1, 1))
	self._detection:child("detection_right"):set_color(Color(current*0.5 + 0.5, 1, 1))
	self._detection:set_visible(true)
	self._detection_value:set_visible(true)
	self._detection_value:set_text(math.round(current*100))

	if reached then
		self._detection_value:set_color(Color(255, 255, 42, 0)/255)
	else
		self._detection_value:set_color(tweak_data.screen_colors.text)
	end

	self._name_text:set_text(self._peer:name() .. "  " .. ((0 < self._peer:rank() and managers.experience:rank_string(self._peer:rank()) .. "-") or "") .. (self._peer:level() or "") .. "")
	self._name_text:set_visible(true)

	self._loadout_panel = self._content_panel:panel()
	local primary_panel = self._loadout_panel:panel({
		w = 128,
		h = 128
	})
	local secondary_panel = self._loadout_panel:panel({
		w = 128,
		h = 128
	})

	self.create_weapon(self, outfit.primary, primary_panel)
	self.create_weapon(self, outfit.secondary, secondary_panel)
	secondary_panel.set_lefttop(secondary_panel, primary_panel.righttop(primary_panel))
	self._loadout_panel:set_left(self._info_panel:left())
	self._loadout_panel:set_top(self._info_panel:bottom() + PADDING)

	local background = self._loadout_panel:rect({
		alpha = 0.2,
		layer = -1,
		color = Color(255, 0, 0, 0)/255
	})
	local melee_panel = self._loadout_panel:panel({
		w = 128,
		h = 64
	})
	local throw_panel = self._loadout_panel:panel({
		w = 128,
		h = 64
	})
	local deploy_panel = self._loadout_panel:panel({
		w = 64,
		h = 64
	})
	local armor_panel = self._loadout_panel:panel({
		w = 64,
		h = 64
	})

	self.create_melee(self, melee_panel, outfit, tweak_data.blackmarket.melee_weapons[outfit.melee_weapon], "textures/pd2/blackmarket/icons/melee_weapons/")
	self.create_item(self, throw_panel, outfit.grenade, tweak_data.blackmarket.projectiles[outfit.grenade], "textures/pd2/blackmarket/icons/grenades/")
	self.create_item(self, deploy_panel, outfit.deployable, tweak_data.blackmarket.deployables[outfit.deployable], "textures/pd2/blackmarket/icons/deployables/")
	self.create_item(self, armor_panel, outfit.armor, tweak_data.blackmarket.armors[outfit.armor], "textures/pd2/blackmarket/icons/armors/")
	melee_panel.set_lefttop(melee_panel, secondary_panel.righttop(secondary_panel))
	armor_panel.set_lefttop(armor_panel, melee_panel.righttop(melee_panel))
	throw_panel.set_leftbottom(throw_panel, secondary_panel.rightbottom(secondary_panel))
	deploy_panel.set_lefttop(deploy_panel, throw_panel.righttop(throw_panel))
	self._loadout_panel:set_w(deploy_panel.right(deploy_panel))
	self._loadout_panel:set_h(primary_panel.bottom(primary_panel))
	BoxGuiObject:new(self._loadout_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	return true
end
IngameWaitingGui.create_item = function (self, panel, outfit_item, tweak_data, folder)
	local w = panel.w(panel)
	local h = panel.h(panel)

	if outfit_item and outfit_item ~= "nil" then
		local guis_catalog = "guis/"
		local bundle_folder = tweak_data and tweak_data.texture_bundle_folder

		if bundle_folder then
			guis_catalog = guis_catalog .. "dlcs/" .. tostring(bundle_folder) .. "/"
		end

		slot9 = panel.bitmap(panel, {
			alpha = 0.8,
			texture = guis_catalog .. folder .. outfit_item,
			w = w,
			h = h
		})
	else
		self.create_empty(self, panel)
	end

	return 
end
IngameWaitingGui.create_melee = function (self, panel, outfit, ...)
	return self.create_item(self, panel, outfit.melee_weapon, ...)
end
IngameWaitingGui.create_empty = function (self, panel)
	local empty_bitmap = panel.bitmap(panel, {
		texture = "guis/textures/pd2/none_icon",
		alpha = 0.8,
		w = panel.w(panel),
		h = panel.h(panel)
	})
	local aspect = empty_bitmap.texture_width(empty_bitmap)/math.max(1, empty_bitmap.texture_height(empty_bitmap))

	empty_bitmap.set_w(empty_bitmap, empty_bitmap.h(empty_bitmap)*aspect)
	empty_bitmap.set_center_x(empty_bitmap, panel.center_x(panel))

	return 
end
IngameWaitingGui.create_weapon = function (self, weapon, panel)
	local w = panel.w(panel)
	local h = panel.h(panel)

	if weapon.factory_id then
		local primary_id = managers.weapon_factory:get_weapon_id_by_factory_id(weapon.factory_id)
		local texture, rarity = managers.blackmarket:get_weapon_icon_path(primary_id, weapon.cosmetics)
		local primary_bitmap = panel.bitmap(panel, {
			alpha = 0.8,
			layer = 1,
			texture = texture,
			w = w,
			h = h
		})
		local aspect = primary_bitmap.texture_height(primary_bitmap)/math.max(1, primary_bitmap.texture_width(primary_bitmap))

		primary_bitmap.set_h(primary_bitmap, primary_bitmap.w(primary_bitmap)*aspect)
		primary_bitmap.set_center_y(primary_bitmap, panel.center_y(panel))

		if rarity then
			local rarity_bitmap = panel.bitmap(panel, {
				blend_mode = "add",
				rotation = 360,
				texture = rarity
			})
			local texture_width = rarity_bitmap.texture_width(rarity_bitmap)
			local texture_height = rarity_bitmap.texture_height(rarity_bitmap)
			local panel_width = primary_bitmap.w(primary_bitmap)
			local panel_height = primary_bitmap.h(primary_bitmap)
			local tw = texture_width
			local th = texture_height
			local pw = panel_width
			local ph = panel_height

			if tw == 0 or th == 0 then
				Application:error("[TeamLoadoutItem] BG Texture size error!:", "width", tw, "height", th)

				tw = 1
				th = 1
			end

			local sw = math.min(pw, ph*tw/th)
			local sh = math.min(ph, pw/tw/th)

			rarity_bitmap.set_size(rarity_bitmap, math.round(sw), math.round(sh))
			rarity_bitmap.set_center(rarity_bitmap, primary_bitmap.center(primary_bitmap))
		end

		local perk_index = 0
		local perks = managers.blackmarket:get_perks_from_weapon_blueprint(weapon.factory_id, weapon.blueprint)

		if 0 < table.size(perks) then
			for perk in pairs(perks) do
				if perk ~= "bonus" then
					local texture = "guis/textures/pd2/blackmarket/inv_mod_" .. perk

					if DB:has(Idstring("texture"), texture) then
						local perk_object = panel.bitmap(panel, {
							w = 16,
							h = 16,
							alpha = 0.8,
							layer = 2,
							texture = texture
						})

						perk_object.set_rightbottom(perk_object, math.round(panel.right(panel) - perk_index*16), math.round(panel.bottom(panel) - 5))

						perk_index = perk_index + 1
					end
				end
			end
		end

		local factory = tweak_data.weapon.factory.parts
		local parts = managers.weapon_factory:get_parts_from_weapon_by_type_or_perk("bonus", weapon.factory_id, weapon.blueprint) or {}
		local stats, custom_stats, has_stat, has_team = nil
		local textures = {}

		for _, part_id in ipairs(parts) do
			stats = (factory[part_id] and factory[part_id].stats) or false
			custom_stats = (factory[part_id] and factory[part_id].custom_stats) or false

			if stats and 1 < table.size(stats) then
				has_stat = true
			else
				has_stat = false
			end

			if custom_stats and (custom_stats.exp_multiplier or custom_stats.money_multiplier) then
				has_team = true
			else
				has_team = false
			end

			if has_stat then
				table.insert(textures, "guis/textures/pd2/blackmarket/inv_mod_bonus_stats")
			end

			if has_team then
				table.insert(textures, "guis/textures/pd2/blackmarket/inv_mod_bonus_team")
			end
		end

		if #textures == 0 and weapon.cosmetics and weapon.cosmetics.bonus and not managers.job:is_current_job_competitive() then
			local bonus_data = tweak_data.economy.bonuses[tweak_data.blackmarket.weapon_skins[weapon.cosmetics.id].bonus]

			if bonus_data and bonus_data.stats then
				has_stat = true
			else
				has_stat = false
			end

			if bonus_data and (bonus_data.exp_multiplier or bonus_data.money_multiplier) then
				has_team = true
			else
				has_team = false
			end

			if has_stat then
				table.insert(textures, "guis/textures/pd2/blackmarket/inv_mod_bonus_stats")
			end

			if has_team then
				table.insert(textures, "guis/textures/pd2/blackmarket/inv_mod_bonus_team")
			end
		end

		for _, texture in ipairs(table.list_union(textures)) do
			if DB:has(Idstring("texture"), texture) then
				local perk_object = panel.bitmap(panel, {
					w = 16,
					h = 16,
					alpha = 0.8,
					layer = 2,
					texture = texture
				})

				perk_object.set_rightbottom(perk_object, math.round(panel.right(panel) - perk_index*16), math.round(panel.bottom(panel) - 5))

				perk_index = perk_index + 1
			end
		end
	end

	return 
end
IngameWaitingGui.dummy_trigger = function (self)
	local btn = self._current_btn.button
	local _ = btn and btn.trigger(btn)

	return 
end
IngameWaitingGui.dummy_set_highlight = function (self, highlight)
	if self._highlighted == highlight then
		return 
	end

	self._highlighted = highlight

	self._background:set_visible(highlight)

	local btn = self._current_btn.button

	if not btn then
		return 
	end

	btn.set_highlighted(btn, highlight)

	return 
end
IngameWaitingGui.move_left = function (self)
	if not self._highlighted then
		return 
	end

	if self._current_btn.index == 1 then
		self.set_current_button(self, (self.prev_page(self) and #self._buttons) or 1)
	else
		self.set_current_button(self, self._current_btn.index - 1)
	end

	return 
end
IngameWaitingGui.move_right = function (self)
	if not self._highlighted then
		return 
	end

	if self._current_btn.index == #self._buttons then
		self.set_current_button(self, (self.next_page(self) and 1) or #self._buttons)
	else
		self.set_current_button(self, self._current_btn.index + 1)
	end

	return 
end
IngameWaitingGui.prev_page = function (self)
	local page = self.calc_prev(self)

	if page then
		self.set_panel_for(self, page)

		return true
	end

	return false
end
IngameWaitingGui.previous_page = IngameWaitingGui.prev_page
IngameWaitingGui.next_page = function (self)
	local page = self.calc_next(self)

	if page then
		self.set_panel_for(self, page)

		return true
	end

	return false
end
IngameWaitingGui.mouse_moved = function (self, o, x, y)
	if not self._content_panel:visible() then
		return 
	end

	if self._content_panel:inside(x, y) then
		if not self._highlighted then
			self.dummy_set_highlight(self, true)
			managers.menu:active_menu().logic:mouse_over_select_item(self._dummy_item.name, false)
		end
	else
		return 
	end

	for k, btn in pairs(self._buttons) do
		if btn.panel(btn):inside(x, y) then
			self.set_current_button(self, k)

			return true, "link"
		end
	end

	self._next_arrow:set_highlighted(self._next_arrow:panel():inside(x, y))
	self._prev_arrow:set_highlighted(self._prev_arrow:panel():inside(x, y))

	if self._next_arrow._highlighted or self._prev_arrow._highlighted then
		return true, "link"
	end

	return 
end
IngameWaitingGui.special_btn_pressed = function (self, button)
	print(button)

	for k, btn in pairs(self._buttons) do
		if btn.binding == button then
			btn.trigger(btn)
		end
	end

	return 
end
IngameWaitingGui.mouse_pressed = function (self, button, x, y)
	if not self._content_panel:visible() or button ~= Idstring("0") then
		return 
	end

	if not self._content_panel:inside(x, y) then
		return 
	end

	for k, btn in pairs(self._buttons) do
		if btn.panel(btn):inside(x, y) then
			btn.trigger(btn)

			return 
		end
	end

	local t = self._next_arrow:panel():inside(x, y) and self.next_page(self)
	local t = self._prev_arrow:panel():inside(x, y) and self.prev_page(self)

	return 
end
IngameWaitingGui.set_current_button = function (self, index)
	index = math.clamp(1, index or 1, #self._buttons)
	local current = self._current_btn.button
	local target = self._buttons[index]
	self._current_btn = {
		button = target,
		index = index
	}
	local t1 = current and current.set_highlighted(current, false)
	local t0 = target and target.set_highlighted(target, self._highlighted)

	return 
end
IngameWaitingButton = IngameWaitingButton or class()
IngameWaitingButton.init = function (self, parent_panel, text, binding, callback)
	self._panel = parent_panel.panel(parent_panel)
	self._callback = callback
	self.binding = binding and Idstring(binding)
	self._text = self._panel:text({
		text = text,
		font = text_font,
		font_size = text_font_size,
		color = font_color_rest
	})

	make_fine_text(self._text)
	self._panel:set_size(self._text:size())

	return self._panel
end
IngameWaitingButton.set_highlighted = function (self, highlighted)
	self._highlighted = highlighted

	self._text:set_color((highlighted and font_color_highlighted) or font_color_rest)

	return 
end
IngameWaitingButton.panel = function (self)
	return self._panel
end
IngameWaitingButton.trigger = function (self)
	if self._callback then
		self._callback()
	end

	return 
end

return 
