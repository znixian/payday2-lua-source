WalletGuiObject = WalletGuiObject or class()
WalletGuiObject.init = function (self, panel)
	WalletGuiObject.set_wallet(panel)

	return 
end
WalletGuiObject.set_wallet = function (panel, layer)
	WalletGuiObject.remove_wallet()

	Global.wallet_panel = panel.panel(panel, {
		name = "WalletGuiObject",
		layer = layer or 0
	})
	local money_icon = Global.wallet_panel:bitmap({
		texture = "guis/textures/pd2/shared_wallet_symbol",
		name = "wallet_money_icon"
	})
	local level_icon = Global.wallet_panel:bitmap({
		texture = "guis/textures/pd2/shared_level_symbol",
		name = "wallet_level_icon"
	})
	local skillpoint_icon = Global.wallet_panel:bitmap({
		texture = "guis/textures/pd2/shared_skillpoint_symbol",
		name = "wallet_skillpoint_icon"
	})
	local money_text = Global.wallet_panel:text({
		name = "wallet_money_text",
		text = managers.money:total_string_no_currency(),
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.text
	})
	local level_text = Global.wallet_panel:text({
		name = "wallet_level_text",
		text = tostring(managers.experience:current_level()),
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.text
	})
	local skillpoint_text = Global.wallet_panel:text({
		name = "wallet_skillpoint_text",
		text = tostring(managers.skilltree:points()),
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.text
	})
	local mw, mh = WalletGuiObject.make_fine_text(money_text)
	local lw, lh = WalletGuiObject.make_fine_text(level_text)
	local sw, sh = WalletGuiObject.make_fine_text(skillpoint_text)

	money_icon.set_leftbottom(money_icon, 2, Global.wallet_panel:h() - 2)
	money_text.set_left(money_text, money_icon.right(money_icon) + 2)
	money_text.set_center_y(money_text, money_icon.center_y(money_icon))
	money_text.set_y(money_text, math.round(money_text.y(money_text)))
	level_icon.set_leftbottom(level_icon, money_text.right(money_text) + 10, Global.wallet_panel:h() - 2)
	level_text.set_left(level_text, level_icon.right(level_icon) + 2)
	level_text.set_center_y(level_text, level_icon.center_y(level_icon))
	level_text.set_y(level_text, math.round(level_text.y(level_text)))
	skillpoint_icon.set_leftbottom(skillpoint_icon, level_text.right(level_text) + 10, Global.wallet_panel:h() - 2)
	skillpoint_text.set_left(skillpoint_text, skillpoint_icon.right(skillpoint_icon) + 2)
	skillpoint_text.set_center_y(skillpoint_text, skillpoint_icon.center_y(skillpoint_icon))
	skillpoint_text.set_y(skillpoint_text, math.round(skillpoint_text.y(skillpoint_text)))

	local coins_icon = Global.wallet_panel:bitmap({
		texture = "guis/dlcs/chill/textures/pd2/safehouse/continental_coins_symbol",
		name = "wallet_coins_icon"
	})
	local coins_text = Global.wallet_panel:text({
		name = "wallet_coins_text",
		text = managers.experience:cash_string(math.floor(managers.custom_safehouse:coins()), ""),
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.text
	})
	local cw, ch = WalletGuiObject.make_fine_text(coins_text)
	local align = (0 < managers.skilltree:points() and skillpoint_text) or level_text

	coins_icon.set_leftbottom(coins_icon, align.right(align) + 10, Global.wallet_panel:h() - 2)
	coins_text.set_left(coins_text, coins_icon.right(coins_icon) + 2)
	coins_text.set_center_y(coins_text, coins_icon.center_y(coins_icon))
	coins_text.set_y(coins_text, math.round(coins_text.y(coins_text)))

	local max_w = coins_text.right(coins_text)
	local bg_blur = Global.wallet_panel:bitmap({
		texture = "guis/textures/test_blur_df",
		name = "bg_blur",
		h = 0,
		w = 0,
		render_template = "VertexColorTexturedBlur3D",
		layer = -1
	})

	bg_blur.set_leftbottom(bg_blur, 0, Global.wallet_panel:h())
	bg_blur.set_w(bg_blur, max_w + 2)
	bg_blur.set_h(bg_blur, Global.wallet_panel:h() - money_icon.top(money_icon))
	WalletGuiObject.set_object_visible("wallet_skillpoint_icon", 0 < managers.skilltree:points())
	WalletGuiObject.set_object_visible("wallet_skillpoint_text", 0 < managers.skilltree:points())
	WalletGuiObject.set_object_visible("wallet_coins_icon", managers.custom_safehouse:unlocked() and 0 < managers.custom_safehouse:coins())
	WalletGuiObject.set_object_visible("wallet_coins_text", managers.custom_safehouse:unlocked() and 0 < managers.custom_safehouse:coins())

	return 
end
WalletGuiObject.refresh = function ()
	if Global.wallet_panel then
		local money_icon = Global.wallet_panel:child("wallet_money_icon")
		local level_icon = Global.wallet_panel:child("wallet_level_icon")
		local skillpoint_icon = Global.wallet_panel:child("wallet_skillpoint_icon")
		local money_text = Global.wallet_panel:child("wallet_money_text")
		local level_text = Global.wallet_panel:child("wallet_level_text")
		local skillpoint_text = Global.wallet_panel:child("wallet_skillpoint_text")

		money_text.set_text(money_text, managers.money:total_string_no_currency())
		WalletGuiObject.make_fine_text(money_text)
		money_icon.set_leftbottom(money_icon, 2, Global.wallet_panel:h() - 2)
		money_text.set_left(money_text, money_icon.right(money_icon) + 2)
		money_text.set_center_y(money_text, money_icon.center_y(money_icon))
		money_text.set_y(money_text, math.round(money_text.y(money_text)))
		level_text.set_text(level_text, tostring(managers.experience:current_level()))
		WalletGuiObject.make_fine_text(level_text)
		level_icon.set_leftbottom(level_icon, money_text.right(money_text) + 10, Global.wallet_panel:h() - 2)
		level_text.set_left(level_text, level_icon.right(level_icon) + 2)
		level_text.set_center_y(level_text, level_icon.center_y(level_icon))
		level_text.set_y(level_text, math.round(level_text.y(level_text)))
		skillpoint_text.set_text(skillpoint_text, tostring(managers.skilltree:points()))
		WalletGuiObject.make_fine_text(skillpoint_text)
		skillpoint_icon.set_leftbottom(skillpoint_icon, level_text.right(level_text) + 10, Global.wallet_panel:h() - 2)
		skillpoint_text.set_left(skillpoint_text, skillpoint_icon.right(skillpoint_icon) + 2)
		skillpoint_text.set_center_y(skillpoint_text, skillpoint_icon.center_y(skillpoint_icon))
		skillpoint_text.set_y(skillpoint_text, math.round(skillpoint_text.y(skillpoint_text)))
		WalletGuiObject.set_object_visible("wallet_skillpoint_icon", 0 < managers.skilltree:points())
		WalletGuiObject.set_object_visible("wallet_skillpoint_text", 0 < managers.skilltree:points())

		if managers.custom_safehouse:unlocked() then
			local coins_icon = Global.wallet_panel:child("wallet_coins_icon")
			local coins_text = Global.wallet_panel:child("wallet_coins_text")

			coins_text.set_text(coins_text, managers.experience:cash_string(math.floor(managers.custom_safehouse:coins()), ""))
			WalletGuiObject.make_fine_text(coins_text)

			local align = (0 < managers.skilltree:points() and skillpoint_text) or level_text

			coins_icon.set_leftbottom(coins_icon, align.right(align) + 10, Global.wallet_panel:h() - 2)
			coins_text.set_left(coins_text, coins_icon.right(coins_icon) + 2)
			coins_text.set_center_y(coins_text, coins_icon.center_y(coins_icon))
			coins_text.set_y(coins_text, math.round(coins_text.y(coins_text)))
		end
	end

	return 
end
WalletGuiObject.make_fine_text = function (text)
	local x, y, w, h = text.text_rect(text)

	text.set_size(text, w, h)
	text.set_position(text, math.round(text.x(text)), math.round(text.y(text)))

	return w, h
end
WalletGuiObject.set_layer = function (layer)
	if not alive(Global.wallet_panel) then
		return 
	end

	Global.wallet_panel:set_layer(layer)

	return 
end
WalletGuiObject.move_wallet = function (mx, my)
	if not alive(Global.wallet_panel) then
		return 
	end

	Global.wallet_panel:move(mx, my)

	return 
end
WalletGuiObject.set_wallet_pos = function (mx, my)
	if not alive(Global.wallet_panel) then
		return 
	end

	if mx then
		Global.wallet_panel:set_x(mx)
	end

	if my then
		Global.wallet_panel:set_y(my)
	end

	return 
end
WalletGuiObject.set_object_visible = function (object, visible)
	if not alive(Global.wallet_panel) then
		return 
	end

	Global.wallet_panel:child(object):set_visible(visible)

	local bg_blur = Global.wallet_panel:child("bg_blur")

	if Global.wallet_panel:child("wallet_skillpoint_icon"):visible() then
		bg_blur.set_w(bg_blur, Global.wallet_panel:child("wallet_skillpoint_text"):right())
	elseif Global.wallet_panel:child("wallet_level_icon"):visible() then
		bg_blur.set_w(bg_blur, Global.wallet_panel:child("wallet_level_text"):right())
	elseif Global.wallet_panel:child("wallet_money_icon"):visible() then
		bg_blur.set_w(bg_blur, Global.wallet_panel:child("wallet_money_text"):right())
	elseif Global.wallet_panel:child("wallet_coins_icon"):visible() then
		bg_blur.set_w(bg_blur, Global.wallet_panel:child("wallet_coins_text"):right())

		if not managers.custom_safehouse:unlocked() then
			Global.wallet_panel:child("wallet_coins_icon"):set_visible(false)
		end
	else
		bg_blur.set_w(bg_blur, 0)
	end

	bg_blur.set_leftbottom(bg_blur, Global.wallet_panel:child("wallet_money_icon"):leftbottom())

	return 
end
WalletGuiObject.remove_wallet = function ()
	if not alive(Global.wallet_panel) or not alive(Global.wallet_panel:parent()) then
		Global.wallet_panel = nil

		return 
	end

	Global.wallet_panel:parent():remove(Global.wallet_panel)

	Global.wallet_panel = nil

	return 
end
WalletGuiObject.close_wallet = function (panel)
	if not alive(Global.wallet_panel) or not alive(Global.wallet_panel:parent()) then
		Global.wallet_panel = nil

		return 
	end

	if panel ~= Global.wallet_panel:parent() then
		return 
	end

	panel.remove(panel, Global.wallet_panel)

	Global.wallet_panel = nil

	return 
end

return 
