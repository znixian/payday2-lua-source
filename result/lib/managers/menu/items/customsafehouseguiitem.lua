local is_win32 = SystemInfo:platform() == Idstring("WIN32")
local NOT_WIN_32 = not is_win32
local medium_font = tweak_data.menu.pd2_medium_font
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font = tweak_data.menu.pd2_small_font
local small_font_size = tweak_data.menu.pd2_small_font_size
CustomSafehouseGuiItem = CustomSafehouseGuiItem or class()
CustomSafehouseGuiItem.init = function (self)
	self._selected = false

	return 
end
CustomSafehouseGuiItem.refresh = function (self)
	return 
end
CustomSafehouseGuiItem.inside = function (self)
	return 
end
CustomSafehouseGuiItem.is_selected = function (self)
	return self._selected
end
CustomSafehouseGuiItem.set_selected = function (self, selected, play_sound)
	if self._selected ~= selected then
		self._selected = selected

		self.refresh(self)

		if self._selected and play_sound ~= false then
			managers.menu_component:post_event("highlight")
		end
	end

	return 
end
CustomSafehouseGuiItem.is_active = function (self)
	return self._active
end
CustomSafehouseGuiItem.set_active = function (self, active, play_sound)
	if self._active ~= active then
		self._active = active

		self.refresh(self)
	end

	return 
end
CustomSafehouseGuiItem.trigger = function (self)
	managers.menu_component:post_event("menu_enter")
	self.refresh(self)

	return 
end
CustomSafehouseGuiItem.flash = function (self)
	return 
end
CustomSafehouseGuiTabItem = CustomSafehouseGuiTabItem or class(CustomSafehouseGuiItem)
CustomSafehouseGuiTabItem.init = function (self, index, title_id, page_item, gui, tab_x, tab_panel)
	CustomSafehouseGuiTabItem.super.init(self)

	self._index = index
	self._name_id = title_id
	self._active = false
	self._selected = false
	self._gui = gui
	self._page_item = page_item
	local page_panel = tab_panel.panel(tab_panel, {
		name = "Page" .. string.capitalize(tostring(title_id)),
		x = tab_x
	})
	local page_text = page_panel.text(page_panel, {
		name = "PageText",
		vertical = "center",
		align = "center",
		layer = 1,
		text = managers.localization:to_upper_text(title_id),
		font = medium_font,
		font_size = medium_font_size,
		color = Color.black
	})
	local _, _, tw, th = page_text.text_rect(page_text)

	page_panel.set_size(page_panel, tw + 15, th + 10)
	page_text.set_size(page_text, page_panel.size(page_panel))

	local page_tab_bg = page_panel.bitmap(page_panel, {
		texture = "guis/textures/pd2/shared_tab_box",
		name = "PageTabBG",
		w = page_panel.w(page_panel),
		h = page_panel.h(page_panel),
		color = tweak_data.screen_colors.text
	})
	self._page_panel = page_panel

	self.refresh(self)

	return 
end
CustomSafehouseGuiTabItem.index = function (self)
	return self._index
end
CustomSafehouseGuiTabItem.page = function (self)
	return self._page_item
end
CustomSafehouseGuiTabItem.prev_page_position = function (self)
	return self._page_panel:left() - 15
end
CustomSafehouseGuiTabItem.next_page_position = function (self)
	return self._page_panel:right() + 15
end
CustomSafehouseGuiTabItem.set_active = function (self, active)
	self._active = active

	self.refresh(self)

	return 
end
CustomSafehouseGuiTabItem.is_active = function (self)
	return self._active
end
CustomSafehouseGuiTabItem.inside = function (self, x, y)
	return self._page_panel:inside(x, y)
end
CustomSafehouseGuiTabItem.refresh = function (self)
	if alive(self._page_panel) then
		self._page_panel:child("PageText"):set_blend_mode((self._active and "normal") or "add")
		self._page_panel:child("PageText"):set_color((self._active and Color.black) or (self._selected and tweak_data.screen_colors.button_stage_2) or tweak_data.screen_colors.button_stage_3)
		self._page_panel:child("PageTabBG"):set_visible(self._active)
	end

	return 
end
CustomSafehouseGuiPage = CustomSafehouseGuiPage or class(CustomSafehouseGuiItem)
CustomSafehouseGuiPage.init = function (self, page_id, page_panel, fullscreen_panel, gui)
	CustomSafehouseGuiPage.super.init(self)

	self._gui = gui
	self._active = false
	self._selected = 0
	self._page_name = page_id
	self._panel = page_panel.panel(page_panel, {})
	self._info_panel = gui.info_panel(gui):panel({})
	self._event_listener = gui.event_listener(gui)

	self._event_listener:add(page_id, {
		"refresh"
	}, callback(self, self, "refresh"))
	self.refresh(self)

	return 
end
CustomSafehouseGuiPage.update = function (self, t, dt)
	return 
end
CustomSafehouseGuiPage.event_listener = function (self)
	return self._event_listener
end
CustomSafehouseGuiPage.refresh = function (self)
	self.panel(self):set_visible(self._active)
	self.info_panel(self):set_visible(self._active)

	return 
end
CustomSafehouseGuiPage.set_active = function (self, active)
	self._active = active

	self.refresh(self)

	return active
end
CustomSafehouseGuiPage.on_notify = function (self, tree, msg)
	return 
end
CustomSafehouseGuiPage.name = function (self)
	return self._page_name
end
CustomSafehouseGuiPage.panel = function (self)
	return self._panel
end
CustomSafehouseGuiPage.info_panel = function (self)
	return self._info_panel
end
CustomSafehouseGuiPage.stack_panels = function (self, padding, panels)
	for idx, panel in ipairs(panels) do
		panel.set_left(panel, 0)
		panel.set_top(panel, (1 < idx and panels[idx - 1]:bottom() + padding) or 0)
	end

	return 
end
CustomSafehouseGuiPage.mouse_clicked = function (self, o, button, x, y)
	return 
end
CustomSafehouseGuiPage.mouse_pressed = function (self, button, x, y)
	return 
end
CustomSafehouseGuiPage.mouse_released = function (self, button, x, y)
	return 
end
CustomSafehouseGuiPage.mouse_moved = function (self, button, x, y)
	return 
end
CustomSafehouseGuiPage.mouse_wheel_up = function (self, x, y)
	return 
end
CustomSafehouseGuiPage.mouse_wheel_down = function (self, x, y)
	return 
end
CustomSafehouseGuiPage.move_up = function (self)
	return 
end
CustomSafehouseGuiPage.move_down = function (self)
	return 
end
CustomSafehouseGuiPage.move_left = function (self)
	return 
end
CustomSafehouseGuiPage.move_right = function (self)
	return 
end
CustomSafehouseGuiPage.confirm_pressed = function (self)
	return 
end
CustomSafehouseGuiPage.special_btn_pressed = function (self, button)
	if not self.is_active(self) or not self._controllers_mapping then
		return 
	end

	local btn = self._controllers_mapping and self._controllers_mapping[button.key(button)]

	if btn and btn._callback and (not self._button_press_delay or self._button_press_delay < TimerManager:main():time()) then
		managers.menu_component:post_event("menu_enter")
		btn._callback()

		self._button_press_delay = TimerManager:main():time() + 0.2

		return 
	end

	return 
end
CustomSafehouseGuiPage.get_legend = function (self)
	return {
		"move",
		"back"
	}
end
CustomSafehouseGuiButtonItem = CustomSafehouseGuiButtonItem or class(CustomSafehouseGuiItem)
CustomSafehouseGuiButtonItem.init = function (self, panel, data, x, priority)
	CustomSafehouseGuiButtonItem.super.init(self, panel, data)

	self._btn_data = data
	self._callback = data.callback
	local up_font_size = (NOT_WIN_32 and RenderSettings.resolution.y < 720 and data.btn == "BTN_STICK_R" and 2) or 0
	self._color = data.color or tweak_data.screen_colors.button_stage_3
	self._selected_color = data.selected_color or tweak_data.screen_colors.button_stage_2
	self._custom_data = data.custom
	self._hidden = false
	self._panel = panel.panel(panel, {
		x = x,
		y = data.y or x + (priority - 1)*small_font_size,
		w = panel.w(panel) - x*2,
		h = medium_font_size
	})
	self._btn_text = self._panel:text({
		name = "text",
		blend_mode = "add",
		text = "",
		x = 0,
		layer = 1,
		align = data.align or "right",
		w = self._panel:w(),
		font_size = small_font_size + up_font_size,
		font = small_font,
		color = self._color
	})
	local text = data.name_id

	if data.localize == nil or data.localize then
		text = managers.localization:text(data.name_id)
	end

	self.set_text(self, text)

	self._select_rect = self._panel:rect({
		blend_mode = "add",
		name = "select_rect",
		halign = "scale",
		alpha = 0.3,
		valign = "scale",
		color = self._color
	})

	self._select_rect:set_visible(false)

	if not managers.menu:is_pc_controller() then
		self._btn_text:set_color(data.color or tweak_data.screen_colors.text)
	end

	return 
end
CustomSafehouseGuiButtonItem.button_data = function (self)
	return self._btn_data
end
CustomSafehouseGuiButtonItem.get_custom_data = function (self)
	return self._custom_data
end
CustomSafehouseGuiButtonItem.reorder = function (self, new_prio)
	self._panel:set_y(self._panel:x() + (new_prio - 1)*small_font_size)

	return 
end
CustomSafehouseGuiButtonItem.set_text = function (self, text)
	local prefix = (not managers.menu:is_pc_controller() and self._btn_data.btn and managers.localization:get_default_macro(self._btn_data.btn)) or ""

	self._btn_text:set_text(prefix .. utf8.to_upper(text))

	local _, _, w, h = self._btn_text:text_rect()
	h = math.max(h, small_font_size)

	self._panel:set_h(h)
	self._btn_text:set_h(h)

	return 
end
CustomSafehouseGuiButtonItem.text = function (self)
	return self._btn_text:text()
end
CustomSafehouseGuiButtonItem.inside = function (self, x, y)
	if self._hidden then
		return false
	end

	return self._panel:inside(x, y)
end
CustomSafehouseGuiButtonItem.show = function (self)
	self._select_rect:set_visible(true)

	return 
end
CustomSafehouseGuiButtonItem.hide = function (self)
	self._select_rect:set_visible(false)

	return 
end
CustomSafehouseGuiButtonItem.visible = function (self)
	if self._hidden then
		return false
	end

	return self._select_rect:visible()
end
CustomSafehouseGuiButtonItem.refresh = function (self)
	if self._selected then
		self.show(self)
	else
		self.hide(self)
	end

	return 
end
CustomSafehouseGuiButtonItem.trigger = function (self)
	CustomSafehouseGuiButtonItem.super.trigger(self)
	self._callback()

	return 
end
CustomSafehouseGuiButtonItem.set_color = function (self, color, selected_color)
	self._color = color or self._color
	self._selected_color = selected_color or color or self._selected_color

	self.set_selected(self, self._selected, false)

	return 
end
CustomSafehouseGuiButtonItem.set_selected = function (self, selected, play_sound)
	CustomSafehouseGuiButtonItem.super.set_selected(self, selected, play_sound)

	if selected then
		self._btn_text:set_color(self._selected_color)
		self._select_rect:set_color(self._selected_color)
	else
		self._btn_text:set_color(self._color)
		self._select_rect:set_color(self._color)
	end

	return 
end
CustomSafehouseGuiButtonItem.hidden = function (self)
	return self._hidden
end
CustomSafehouseGuiButtonItem.set_hidden = function (self, hidden)
	self._hidden = hidden

	self._panel:set_visible(not hidden)

	return 
end
CustomSafehouseGuiButtonItemWithIcon = CustomSafehouseGuiButtonItemWithIcon or class(CustomSafehouseGuiButtonItem)
CustomSafehouseGuiButtonItemWithIcon.init = function (self, panel, data, x, priority)
	CustomSafehouseGuiButtonItemWithIcon.super.init(self, panel, data, x, priority)

	if data.icon then
		self._create_icon(self, data.icon)
	end

	self._btn_text:set_left(self._panel:h())

	return 
end
CustomSafehouseGuiButtonItemWithIcon._create_icon = function (self, icon)
	self._icon = self._panel:bitmap({
		x = 3,
		texture = icon,
		w = self._panel:h() - 6,
		h = self._panel:h() - 6,
		color = self._color
	})

	self._icon:set_center_y(self._panel:h()/2)

	return 
end
CustomSafehouseGuiButtonItemWithIcon.set_icon = function (self, icon)
	if alive(self._icon) then
		if icon then
			self._icon:set_image(icon)
			self._icon:show()
		else
			self._icon:hide()
		end
	elseif icon then
		self._create_icon(self, icon)
	end

	return 
end
CustomSafehouseGuiButtonItemWithIcon.icon = function (self)
	return self._icon
end
CustomSafehouseGuiButtonItemWithIcon.set_selected = function (self, selected, play_sound)
	CustomSafehouseGuiButtonItemWithIcon.super.set_selected(self, selected, play_sound)

	if alive(self._icon) then
		if selected then
			self._icon:set_color(self._selected_color)
		else
			self._icon:set_color(self._color)
		end
	end

	return 
end

return 
