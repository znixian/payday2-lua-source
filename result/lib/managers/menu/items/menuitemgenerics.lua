local is_win32 = SystemInfo:platform() == Idstring("WIN32")
local NOT_WIN_32 = not is_win32
local medium_font = tweak_data.menu.pd2_medium_font
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font = tweak_data.menu.pd2_small_font
local small_font_size = tweak_data.menu.pd2_small_font_size
MenuGuiItem = MenuGuiItem or class()
MenuGuiItem.init = function (self)
	self._selected = false

	return 
end
MenuGuiItem.refresh = function (self)
	return 
end
MenuGuiItem.inside = function (self)
	return 
end
MenuGuiItem.is_selected = function (self)
	return self._selected
end
MenuGuiItem.set_selected = function (self, selected, play_sound)
	if self._selected ~= selected then
		self._selected = selected

		self.refresh(self)

		if self._selected and play_sound ~= false then
			managers.menu_component:post_event("highlight")
		end
	end

	return 
end
MenuGuiItem.is_active = function (self)
	return self._active
end
MenuGuiItem.set_active = function (self, active, play_sound)
	if self._active ~= active then
		self._active = active

		self.refresh(self)
	end

	return 
end
MenuGuiItem.trigger = function (self)
	managers.menu_component:post_event("menu_enter")
	self.refresh(self)

	return 
end
MenuGuiItem.flash = function (self)
	return 
end
MenuGuiTabItem = MenuGuiTabItem or class(MenuGuiItem)
MenuGuiTabItem.FONT = medium_font
MenuGuiTabItem.FONT_SIZE = medium_font_size
MenuGuiTabItem.PAGE_PADDING = 15
MenuGuiTabItem.TEXT_PADDING_W = 15
MenuGuiTabItem.TEXT_PADDING_H = 10
MenuGuiTabItem.init = function (self, index, title_id, page_item, gui, tab_x, tab_panel)
	MenuGuiTabItem.super.init(self)

	self._index = index
	self._name_id = title_id
	self._active = false
	self._selected = false
	self._gui = gui
	self._page_item = page_item
	local page_panel = tab_panel.panel(tab_panel, {
		name = "Page" .. string.capitalize(tostring(title_id)),
		x = tab_x
	})
	local page_text = page_panel.text(page_panel, {
		name = "PageText",
		vertical = "center",
		align = "center",
		layer = 1,
		text = managers.localization:to_upper_text(title_id),
		font = self.FONT,
		font_size = self.FONT_SIZE,
		color = Color.black
	})
	local _, _, tw, th = page_text.text_rect(page_text)

	page_panel.set_size(page_panel, tw + self.TEXT_PADDING_W, th + self.TEXT_PADDING_H)
	page_text.set_size(page_text, page_panel.size(page_panel))

	local page_tab_bg = page_panel.bitmap(page_panel, {
		texture = "guis/textures/pd2/shared_tab_box",
		name = "PageTabBG",
		w = page_panel.w(page_panel),
		h = page_panel.h(page_panel),
		color = tweak_data.screen_colors.text
	})
	self._page_panel = page_panel

	self.refresh(self)

	return 
end
MenuGuiTabItem.index = function (self)
	return self._index
end
MenuGuiTabItem.page = function (self)
	return self._page_item
end
MenuGuiTabItem.prev_page_position = function (self)
	return self._page_panel:left() - self.PAGE_PADDING
end
MenuGuiTabItem.next_page_position = function (self)
	return self._page_panel:right() + self.PAGE_PADDING
end
MenuGuiTabItem.set_active = function (self, active)
	self._active = active

	self.refresh(self)

	return 
end
MenuGuiTabItem.is_active = function (self)
	return self._active
end
MenuGuiTabItem.inside = function (self, x, y)
	return self._page_panel:inside(x, y)
end
MenuGuiTabItem.refresh = function (self)
	if alive(self._page_panel) then
		self._page_panel:child("PageText"):set_blend_mode((self._active and "normal") or "add")
		self._page_panel:child("PageText"):set_color((self._active and Color.black) or (self._selected and tweak_data.screen_colors.button_stage_2) or tweak_data.screen_colors.button_stage_3)
		self._page_panel:child("PageTabBG"):set_visible(self._active)
	end

	return 
end
MenuGuiSmallTabItem = MenuGuiSmallTabItem or class(MenuGuiTabItem)
MenuGuiSmallTabItem.FONT = small_font
MenuGuiSmallTabItem.FONT_SIZE = small_font_size
MenuGuiSmallTabItem.PAGE_PADDING = 8
MenuGuiSmallTabItem.TEXT_PADDING_W = 15
MenuGuiSmallTabItem.TEXT_PADDING_H = 4
MenuGuiTabPage = MenuGuiTabPage or class(MenuGuiItem)
MenuGuiTabPage.init = function (self, page_id, page_panel, fullscreen_panel, gui)
	MenuGuiTabPage.super.init(self)

	self._gui = gui
	self._active = false
	self._selected = 0
	self._page_name = page_id
	self._panel = page_panel.panel(page_panel, {})
	self._info_panel = gui.info_panel(gui):panel({})

	if gui.event_listener then
		self._event_listener = gui.event_listener(gui)

		self._event_listener:add(page_id, {
			"refresh"
		}, callback(self, self, "refresh"))
	end

	self.refresh(self)

	return 
end
MenuGuiTabPage.update = function (self, t, dt)
	return 
end
MenuGuiTabPage.event_listener = function (self)
	return self._event_listener
end
MenuGuiTabPage.refresh = function (self)
	self.panel(self):set_visible(self._active)
	self.info_panel(self):set_visible(self._active)

	return 
end
MenuGuiTabPage.set_active = function (self, active)
	self._active = active

	self.refresh(self)

	return active
end
MenuGuiTabPage.on_notify = function (self, tree, msg)
	return 
end
MenuGuiTabPage.name = function (self)
	return self._page_name
end
MenuGuiTabPage.panel = function (self)
	return self._panel
end
MenuGuiTabPage.info_panel = function (self)
	return self._info_panel
end
MenuGuiTabPage.stack_panels = function (self, padding, panels)
	for idx, panel in ipairs(panels) do
		panel.set_left(panel, 0)
		panel.set_top(panel, (1 < idx and panels[idx - 1]:bottom() + padding) or 0)
	end

	return 
end
MenuGuiTabPage.mouse_clicked = function (self, o, button, x, y)
	return 
end
MenuGuiTabPage.mouse_pressed = function (self, button, x, y)
	return 
end
MenuGuiTabPage.mouse_released = function (self, button, x, y)
	return 
end
MenuGuiTabPage.mouse_moved = function (self, button, x, y)
	return 
end
MenuGuiTabPage.mouse_wheel_up = function (self, x, y)
	return 
end
MenuGuiTabPage.mouse_wheel_down = function (self, x, y)
	return 
end
MenuGuiTabPage.move_up = function (self)
	return 
end
MenuGuiTabPage.move_down = function (self)
	return 
end
MenuGuiTabPage.move_left = function (self)
	return 
end
MenuGuiTabPage.move_right = function (self)
	return 
end
MenuGuiTabPage.confirm_pressed = function (self)
	return 
end
MenuGuiTabPage.special_btn_pressed = function (self, button)
	if not self.is_active(self) or not self._controllers_mapping then
		return 
	end

	local btn = self._controllers_mapping and self._controllers_mapping[button.key(button)]

	if btn and btn._callback and (not self._button_press_delay or self._button_press_delay < TimerManager:main():time()) then
		managers.menu_component:post_event("menu_enter")
		btn._callback()

		self._button_press_delay = TimerManager:main():time() + 0.2

		return 
	end

	return 
end
MenuGuiTabPage.get_legend = function (self)
	return {
		"move",
		"back"
	}
end
MenuGuiButtonItem = MenuGuiButtonItem or class(MenuGuiItem)
MenuGuiButtonItem.init = function (self, panel, data, x, priority)
	MenuGuiButtonItem.super.init(self, panel, data)

	self._callback = data.callback
	local prefix = (not managers.menu:is_pc_controller() and data.btn and managers.localization:get_default_macro(data.btn)) or ""
	local up_font_size = (NOT_WIN_32 and RenderSettings.resolution.y < 720 and data.btn == "BTN_STICK_R" and 2) or 0
	self._panel = panel.panel(panel, {
		x = x,
		y = x + (priority - 1)*small_font_size,
		w = panel.w(panel) - x*2,
		h = medium_font_size
	})
	self._btn_text = self._panel:text({
		name = "text",
		blend_mode = "add",
		text = "",
		x = 0,
		layer = 1,
		align = data.align or "right",
		w = self._panel:w(),
		font_size = small_font_size + up_font_size,
		font = small_font,
		color = tweak_data.screen_colors.button_stage_3
	})

	self.set_text(self, prefix .. managers.localization:text(data.name_id))

	self._select_rect = self._panel:rect({
		blend_mode = "add",
		name = "select_rect",
		halign = "scale",
		alpha = 0.3,
		valign = "scale",
		color = tweak_data.screen_colors.button_stage_3
	})

	self._select_rect:set_visible(false)

	if not managers.menu:is_pc_controller() then
		self._btn_text:set_color(tweak_data.screen_colors.text)
	end

	return 
end
MenuGuiButtonItem.set_text = function (self, text)
	self._btn_text:set_text(utf8.to_upper(text))

	local _, _, w, h = self._btn_text:text_rect()

	self._panel:set_h(h)
	self._btn_text:set_h(h)

	return 
end
MenuGuiButtonItem.inside = function (self, x, y)
	return self._panel:inside(x, y)
end
MenuGuiButtonItem.show = function (self)
	self._select_rect:set_visible(true)

	return 
end
MenuGuiButtonItem.hide = function (self)
	self._select_rect:set_visible(false)

	return 
end
MenuGuiButtonItem.visible = function (self)
	return self._select_rect:visible()
end
MenuGuiButtonItem.refresh = function (self)
	if self._selected then
		self.show(self)
	else
		self.hide(self)
	end

	return 
end
MenuGuiButtonItem.trigger = function (self)
	MenuGuiButtonItem.super.trigger(self)
	self._callback()

	return 
end

return 
