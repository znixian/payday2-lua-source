core:import("CoreMenuItem")

ItemColumn = ItemColumn or class(CoreMenuItem.Item)
ItemColumn.TYPE = "column"
ItemColumn.init = function (self, data_node, parameters)
	CoreMenuItem.Item.init(self, data_node, parameters)

	self._type = ItemColumn.TYPE

	return 
end
ItemServerColumn = ItemServerColumn or class(ItemColumn)
ItemServerColumn.TYPE = "server_column"
ItemServerColumn.init = function (self, data_node, parameters)
	ItemServerColumn.super.init(self, data_node, parameters)

	self._type = ItemServerColumn.TYPE

	return 
end

return 
