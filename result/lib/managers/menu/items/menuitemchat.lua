core:import("CoreMenuItem")

MenuItemChat = MenuItemChat or class(CoreMenuItem.Item)
MenuItemChat.TYPE = "chat"
MenuItemChat.init = function (self, data_node, parameters)
	CoreMenuItem.Item.init(self, data_node, parameters)

	self._type = MenuItemChat.TYPE

	return 
end

return 
