core:import("CoreMenuItem")

MenuItemFriend = MenuItemFriend or class(CoreMenuItem.Item)
MenuItemFriend.TYPE = "friend"
MenuItemFriend.init = function (self, data_node, parameters)
	CoreMenuItem.Item.init(self, data_node, parameters)

	self._type = MenuItemFriend.TYPE

	return 
end

return 
