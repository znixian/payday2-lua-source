core:import("CoreMenuItem")

MenuItemInput = MenuItemInput or class(CoreMenuItem.Item)
MenuItemInput.TYPE = "input"
MenuItemInput.init = function (self, data_node, parameters)
	MenuItemInput.super.init(self, data_node, parameters)

	self._esc_released_callback = 0
	self._enter_callback = 0
	self._typing_callback = 0
	self._type = MenuItemInput.TYPE
	self._input_text = ""
	self._input_limit = self._parameters.input_limit or 30
	self._empty_gui_input_limit = self._parameters.empty_gui_input_limit or self._input_limit/2

	return 
end
MenuItemInput.input_text = function (self)
	return self._input_text
end
MenuItemInput.set_input_text = function (self, input_text)
	self._input_text = input_text

	return 
end
MenuItemInput.add_input_text = function (self, input_text)
	self._input_text = self._input_text .. input_text

	return 
end
MenuItemInput.set_value = function (self, value)
	self.set_input_text(self, value)

	return 
end
MenuItemInput.value = function (self)
	return self.input_text(self)
end
MenuItemInput.setup_gui = function (self, node, row_item)
	local right_align = node._right_align(node)
	row_item.gui_panel = node.item_panel:panel({
		alpha = 0.9,
		w = node.item_panel:w()
	})
	row_item.gui_text = node._text_item_part(node, row_item, row_item.gui_panel, right_align, row_item.align)
	row_item.empty_gui_text = node._text_item_part(node, row_item, row_item.gui_panel, right_align, row_item.align)
	row_item.input_bg = row_item.gui_panel:rect({
		alpha = 0,
		vertical = "scale",
		blend_mdoe = "add",
		halign = "scale",
		align = "scale",
		valign = "scale",
		color = Color(0.5, 0.5, 0.5),
		layer = node.layers.items - 1
	})

	row_item.empty_gui_text:set_alpha(1)
	row_item.gui_text:set_text("")

	row_item.caret = row_item.gui_panel:rect({
		rotation = 360,
		name = "caret",
		h = 0,
		w = 0,
		blend_mode = "add",
		y = 0,
		x = 0,
		color = Color(0.1, 1, 1, 1),
		layer = node.layers.items + 2
	})

	self._layout_gui(self, node, row_item)

	return true
end
MenuItemInput._layout_gui = function (self, node, row_item)
	local safe_rect = managers.gui_data:scaled_size()
	local right_align = node._right_align(node)
	local left_align = node._left_align(node)

	row_item.gui_text:set_text(self._input_text or "")

	local n = utf8.len(row_item.gui_text:text())

	row_item.gui_text:set_selection(n, n)

	local _, _, w, h = row_item.empty_gui_text:text_rect()

	row_item.gui_panel:set_height(h)
	row_item.gui_panel:set_width(safe_rect.width - node._mid_align(node))
	row_item.gui_panel:set_x(node._mid_align(node))

	self._align_right = row_item.gui_panel:w()
	self._align_left = node._right_align(node) - row_item.gui_panel:x()

	self._layout(self, row_item)

	return 
end
MenuItemInput._layout = function (self, row_item)
	local _, _, w = row_item.gui_text:text_rect()

	row_item.gui_text:set_width(w + 5)

	local _, _, w, h = row_item.empty_gui_text:text_rect()

	row_item.gui_text:set_h(h)
	row_item.empty_gui_text:set_h(h)
	row_item.empty_gui_text:set_width(w + 5)

	if row_item.align == "right" then
		row_item.gui_text:set_left(self._align_left)
		row_item.empty_gui_text:set_right(self._align_right)
	else
		row_item.gui_text:set_right(self._align_right)
		row_item.empty_gui_text:set_left(self._align_left)
	end

	row_item.gui_text:set_color(row_item.color)
	row_item.empty_gui_text:set_color(row_item.color)
	row_item.empty_gui_text:set_visible(utf8.len(row_item.gui_text:text()) < (self._empty_gui_input_limit or 1))
	self._update_caret(self, row_item)
	self._update_input_bg(self, row_item)

	return true
end
MenuItemInput._update_input_bg = function (self, row_item)
	if not row_item or not alive(row_item.gui_text) then
		return 
	end

	row_item.input_bg:set_alpha((self._input_text ~= row_item.gui_text:text() and 0.6) or 0)

	return 
end
MenuItemInput.reload = function (self, row_item, node)
	if not row_item or not alive(row_item.gui_text) then
		return 
	end

	self._layout_gui(self, node, row_item)

	return true
end
MenuItemInput.highlight_row_item = function (self, node, row_item, mouse_over)
	row_item.gui_text:set_color(row_item.color)
	row_item.empty_gui_text:set_color(row_item.color)
	self._on_focus(self, row_item)

	return true
end
MenuItemInput.fade_row_item = function (self, node, row_item, mouse_over)
	row_item.gui_text:set_color(row_item.color)
	row_item.empty_gui_text:set_color(row_item.color)
	self._loose_focus(self, row_item)

	return true
end
MenuItemInput.esc_key_callback = function (self, row_item)
	if not row_item or not alive(row_item.gui_text) then
		return 
	end

	self._set_enabled(self, false)
	self._loose_focus(self, row_item)

	return 
end
MenuItemInput.enter_key_callback = function (self, row_item)
	if not row_item or not alive(row_item.gui_text) then
		return 
	end

	if not self._editing then
		self._set_enabled(self, true)

		return 
	end

	self._set_enabled(self, false)

	local text = row_item.gui_text
	local message = text.text(text)

	self.set_input_text(self, message)
	self._layout(self, row_item)

	return 
end
MenuItemInput._set_enabled = function (self, enabled)
	if not self.enabled(self) then
		return 
	end

	if enabled then
		self._editing = true

		managers.menu:active_menu().input:set_back_enabled(false)
		managers.menu:active_menu().input:accept_input(false)
		managers.menu:active_menu().input:deactivate_mouse()
	else
		self._editing = false

		managers.menu:active_menu().input:activate_mouse()
		managers.menu:active_menu().input:accept_input(true)
		managers.menu:active_menu().input:set_back_enabled(true)
	end

	return 
end
MenuItemInput._animate_show_input = function (self, input_panel)
	local TOTAL_T = 0.2
	local start_alpha = input_panel.alpha(input_panel)
	local end_alpha = 1

	over(TOTAL_T, function (p)
		input_panel:set_alpha(math.lerp(start_alpha, end_alpha, p))

		return 
	end)

	return 
end
MenuItemInput._animate_hide_input = function (self, input_panel)
	local TOTAL_T = 0.2
	local start_alpha = input_panel.alpha(input_panel)
	local end_alpha = 0.95

	over(TOTAL_T, function (p)
		input_panel:set_alpha(math.lerp(start_alpha, end_alpha, p))

		return 
	end)

	return 
end
MenuItemInput._animate_input_bg = function (self, input_bg)
	local t = 0

	while true do
		local dt = coroutine.yield()
		t = t + dt
		local a = (math.sin(t*200) + 1)/8 + 0.75

		input_bg.set_alpha(input_bg, a)
	end

	return 
end
MenuItemInput.trigger = function (self)
	if type(self._enter_callback) ~= "number" then
		self._enter_callback()
	end

	slot1 = pairs
	slot2 = ((self.enabled(self) or self.parameters(self).ignore_disabled) and self.parameters(self).callback) or self.parameters(self).callback_disabled

	for _, callback in slot1(slot2) do
		callback(self)
	end

	return 
end
MenuItemInput._on_focus = function (self, row_item)
	if self._focus or not self.enabled(self) then
		return 
	end

	self._focus = true

	row_item.gui_panel:stop()
	row_item.gui_panel:animate(callback(self, self, "_animate_show_input"))
	row_item.node_gui.ws:connect_keyboard(Input:keyboard())
	row_item.gui_panel:key_press(callback(self, self, "key_press", row_item))
	row_item.gui_panel:enter_text(callback(self, self, "enter_text", row_item))
	row_item.gui_panel:key_release(callback(self, self, "key_release", row_item))

	self._esc_released_callback = callback(self, self, "esc_key_callback", row_item)
	self._enter_callback = callback(self, self, "enter_key_callback", row_item)
	self._typing_callback = 0
	self._enter_text_set = false

	self._layout(self, row_item)

	return 
end
MenuItemInput.focus = function (self)
	return self._focus
end
MenuItemInput._loose_focus = function (self, row_item)
	if not self._focus then
		return false
	end

	self._focus = false
	self._one_scroll_up_delay = nil
	self._one_scroll_dn_delay = nil
	local text = row_item.gui_text

	text.set_text(text, self._input_text or "")

	local n = utf8.len(text.text(text))

	text.set_selection(text, n, n)
	row_item.node_gui.ws:disconnect_keyboard()
	row_item.gui_panel:key_press(nil)
	row_item.gui_panel:enter_text(nil)
	row_item.gui_panel:key_release(nil)

	self._esc_released_callback = 0
	self._enter_callback = 0
	self._typing_callback = 0

	row_item.gui_panel:stop()
	row_item.gui_panel:animate(callback(self, self, "_animate_hide_input"))

	local text = row_item.gui_text

	text.stop(text)
	self._layout(self, row_item)

	return true
end
MenuItemInput._shift = function (self)
	local k = Input:keyboard()

	return k.down(k, Idstring("left shift")) or k.down(k, Idstring("right shift")) or (k.has_button(k, Idstring("shift")) and k.down(k, Idstring("shift")))
end
MenuItemInput.blink = function (o)
	while true do
		o.set_color(o, Color(0.05, 1, 1, 1))
		wait(0.4)
		o.set_color(o, Color(0.9, 1, 1, 1))
		wait(0.4)
	end

	return 
end
MenuItemInput.set_blinking = function (self, b, row_item)
	local caret = row_item.caret

	if b == self._blinking then
		return 
	end

	if b then
		caret.animate(caret, self.blink)
	else
		caret.stop(caret)
	end

	self._blinking = b

	if not self._blinking then
		caret.set_color(caret, Color(0.9, 1, 1, 1))
	end

	return 
end
MenuItemInput._update_caret = function (self, row_item)
	local text = row_item.gui_text
	local caret = row_item.caret
	local s, e = text.selection(text)
	local x, y, w, h = text.selection_rect(text)

	if s == 0 and e == 0 then
		if text.align(text) == "center" then
			x = text.world_x(text) + text.w(text)/2
		elseif row_item.align == "right" then
			x = text.world_right(text)
		else
			x = text.world_left(text)
		end

		y = text.world_y(text)
	end

	h = text.h(text)

	if w < 3 then
		w = 3
	end

	if not self._editing then
		w = 0
		h = 0
	end

	caret.set_world_shape(caret, x, y + 2, w, h - 4)

	if row_item.align == "right" then
		caret.set_world_right(caret, x)
	end

	self.set_blinking(self, s == e and self._editing, row_item)

	return 
end
MenuItemInput.enter_text = function (self, row_item, o, s)
	if not row_item or not alive(row_item.gui_text) or not self._editing then
		return 
	end

	local text = row_item.gui_text
	local m = self._input_limit
	local n = utf8.len(text.text(text))
	s = utf8.sub(s, 1, m - n)

	if type(self._typing_callback) ~= "number" then
		self._typing_callback()
	end

	text.replace_text(text, s)
	self._layout(self, row_item)

	return 
end
MenuItemInput.update_key_down = function (self, row_item, o, k)
	if not row_item or not alive(row_item.gui_text) then
		return 
	end

	wait(0.6)

	local text = row_item.gui_text

	while self._key_pressed == k do
		local s, e = text.selection(text)
		local n = utf8.len(text.text(text))
		local d = math.abs(e - s)

		if self._key_pressed == Idstring("backspace") then
			if s == e and 0 < s then
				text.set_selection(text, s - 1, e)
			end

			text.replace_text(text, "")

			if utf8.len(text.text(text)) < 1 and type(self._esc_released_callback) ~= "number" then
			end
		elseif self._key_pressed == Idstring("delete") then
			if s == e and s < n then
				text.set_selection(text, s, e + 1)
			end

			text.replace_text(text, "")

			if utf8.len(text.text(text)) < 1 and type(self._esc_released_callback) ~= "number" then
			end
		elseif self._key_pressed == Idstring("insert") then
			local clipboard = Application:get_clipboard() or ""

			text.replace_text(text, clipboard)

			local lbs = text.line_breaks(text)

			if 1 < #lbs then
				local s = lbs[2]
				local e = utf8.len(text.text(text))

				text.set_selection(text, s, e)
				text.replace_text(text, "")
			end
		elseif self._key_pressed == Idstring("left") then
			if s < e then
				text.set_selection(text, s, s)
			elseif 0 < s then
				text.set_selection(text, s - 1, s - 1)
			end
		elseif self._key_pressed == Idstring("right") then
			if s < e then
				text.set_selection(text, e, e)
			elseif s < n then
				text.set_selection(text, s + 1, s + 1)
			end
		else
			self._key_pressed = false
		end

		self._layout(self, row_item)
		wait(0.03)
	end

	return 
end
MenuItemInput.key_release = function (self, row_item, o, k)
	if not row_item or not alive(row_item.gui_text) then
		return 
	end

	if self._key_pressed == k then
		self._key_pressed = false
	end

	if k == Idstring("esc") then
		if type(self._esc_released_callback) ~= "number" then
			self._esc_released_callback()
		end
	elseif k == Idstring("enter") and self._should_disable then
		self._should_disable = false

		self.trigger(self)
	end

	return 
end
MenuItemInput.key_press = function (self, row_item, o, k)
	if not row_item or not alive(row_item.gui_text) or not self._editing then
		return 
	end

	local text = row_item.gui_text
	local s, e = text.selection(text)
	local n = utf8.len(text.text(text))
	local d = math.abs(e - s)
	self._key_pressed = k

	text.stop(text)
	text.animate(text, callback(self, self, "update_key_down", row_item), k)

	if k == Idstring("backspace") then
		if s == e and 0 < s then
			text.set_selection(text, s - 1, e)
		end

		text.replace_text(text, "")

		if utf8.len(text.text(text)) < 1 and type(self._esc_released_callback) ~= "number" then
		end
	elseif k == Idstring("delete") then
		if s == e and s < n then
			text.set_selection(text, s, e + 1)
		end

		text.replace_text(text, "")

		if utf8.len(text.text(text)) < 1 and type(self._esc_released_callback) ~= "number" then
		end
	elseif k == Idstring("insert") then
		local clipboard = Application:get_clipboard() or ""

		text.replace_text(text, clipboard)

		local lbs = text.line_breaks(text)

		if 1 < #lbs then
			local s = lbs[2]
			local e = utf8.len(text.text(text))

			text.set_selection(text, s, e)
			text.replace_text(text, "")
		end
	elseif k == Idstring("left") then
		if s < e then
			text.set_selection(text, s, s)
		elseif 0 < s then
			text.set_selection(text, s - 1, s - 1)
		end
	elseif k == Idstring("right") then
		if s < e then
			text.set_selection(text, e, e)
		elseif s < n then
			text.set_selection(text, s + 1, s + 1)
		end
	elseif self._key_pressed == Idstring("end") then
		text.set_selection(text, n, n)
	elseif self._key_pressed == Idstring("home") then
		text.set_selection(text, 0, 0)
	elseif k == Idstring("enter") then
		self._should_disable = true
	end

	self._layout(self, row_item)

	return 
end

return 
