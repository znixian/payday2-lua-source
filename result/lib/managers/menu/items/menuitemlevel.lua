core:import("CoreMenuItem")

MenuItemLevel = MenuItemLevel or class(CoreMenuItem.Item)
MenuItemLevel.TYPE = "level"
MenuItemLevel.init = function (self, data_node, parameters)
	CoreMenuItem.Item.init(self, data_node, parameters)

	self._type = MenuItemLevel.TYPE

	return 
end

return 
