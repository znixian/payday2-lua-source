require("lib/managers/menu/items/ContractBrokerHeistItem")

local padding = 10

local function make_fine_text(text)
	local x, y, w, h = text.text_rect(text)

	text.set_size(text, w, h)
	text.set_position(text, math.round(text.x(text)), math.round(text.y(text)))

	return 
end

ContractBrokerGui = ContractBrokerGui or class(MenuGuiComponent)
ContractBrokerGui.tabs = {
	{
		"menu_filter_contractor",
		"_setup_filter_contact"
	},
	{
		"menu_filter_time",
		"_setup_filter_time"
	},
	{
		"menu_filter_tactic",
		"_setup_filter_tactic"
	},
	{
		"menu_filter_most_played",
		"_setup_filter_most_played"
	},
	{
		"menu_filter_favourite",
		"_setup_filter_favourite"
	}
}
ContractBrokerGui.MAX_SEARCH_LENGTH = 20
ContractBrokerGui.init = function (self, ws, fullscreen_ws, node)
	self._fullscreen_ws = managers.gui_data:create_fullscreen_16_9_workspace()
	self._ws = managers.gui_data:create_saferect_workspace()
	self._node = node
	self._fullscreen_panel = self._fullscreen_ws:panel():panel({
		layer = 1000
	})
	self._panel = self._ws:panel():panel({
		layer = 1100
	})
	self.make_fine_text = BlackMarketGui.make_fine_text
	self._enabled = true
	self._current_filter = (Global.contract_broker and Global.contract_broker.filter) or 1
	self._highlighted_filter = (Global.contract_broker and Global.contract_broker.filter) or 1
	self._current_tab = (Global.contract_broker and Global.contract_broker.tab) or 1
	self._highlighted_tab = (Global.contract_broker and Global.contract_broker.tab) or 1
	self._current_selection = 1
	self._active_filters = {}
	self._buttons = {}
	self._tab_buttons = {}
	self._filter_buttons = {}
	self._heist_items = {}

	managers.menu_component:disable_crimenet()
	managers.menu:active_menu().input:deactivate_controller_mouse()
	self.setup(self)

	if Global.contract_broker and Global.contract_broker.job_id then
		for idx, item in ipairs(self._heist_items) do
			if item._job_data and item._job_data.job_id == Global.contract_broker.job_id then
				self._panels.scroll:scroll_to(item._panel:y())
				self._set_selection(self, idx)

				break
			end
		end
	end

	Global.contract_broker = nil

	return 
end
ContractBrokerGui.close = function (self)
	self.disconnect_search_input(self)

	self._enabled = false

	managers.menu:active_menu().input:activate_controller_mouse()
	managers.menu_component:enable_crimenet()

	if alive(self._ws) then
		managers.gui_data:destroy_workspace(self._ws)

		self._ws = nil
	end

	if alive(self._fullscreen_ws) then
		managers.gui_data:destroy_workspace(self._fullscreen_ws)

		self._fullscreen_ws = nil
	end

	return 
end
ContractBrokerGui.enabled = function (self)
	return self._enabled
end
ContractBrokerGui.setup = function (self)
	self._create_background(self)
	self._create_title(self)
	self._create_panels(self)
	self._create_back_button(self)
	self._create_legend(self)
	self._setup_tabs(self)
	self._setup_filters(self)
	self._setup_jobs(self)
	self._set_selection(self, 1)
	self.refresh(self)

	return 
end
ContractBrokerGui._setup_change_tab = function (self)
	self.clear_filters(self)
	self._setup_filters(self)
	self._setup_jobs(self)
	self._set_selection(self, 1)
	self.refresh(self)

	return 
end
ContractBrokerGui._setup_change_filter = function (self)
	self._setup_jobs(self)
	self._set_selection(self, 1)
	self.refresh(self)

	return 
end
ContractBrokerGui._setup_change_search = function (self)
	self.clear_filters(self)
	self._setup_filters(self)
	self._setup_jobs(self)
	self._set_selection(self, 1)
	self.refresh(self)

	return 
end
ContractBrokerGui._create_background = function (self)
	self._fullscreen_panel:rect({
		alpha = 0.6,
		layer = 10,
		color = Color.black
	})
	self._fullscreen_panel:bitmap({
		texture = "guis/textures/test_blur_df",
		layer = -2,
		halign = "scale",
		alpha = 10,
		render_template = "VertexColorTexturedBlur3D",
		valign = "scale",
		w = self._fullscreen_panel:w(),
		h = self._fullscreen_panel:h()
	})

	return 
end
ContractBrokerGui._create_title = function (self)
	local title = self._panel:text({
		vertical = "top",
		align = "left",
		layer = 100,
		text = managers.localization:to_upper_text("menu_contract_broker"),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		color = tweak_data.screen_colors.text
	})

	make_fine_text(title)
	title.set_top(title, padding)
	title.set_left(title, padding)

	return 
end
ContractBrokerGui._create_panels = function (self)
	local main_panel = self._panel:panel({
		w = self._panel:w()*0.55,
		h = self._panel:h()*0.7
	})

	main_panel.set_center_x(main_panel, self._panel:w()*0.5)
	main_panel.set_top(main_panel, padding*2 + tweak_data.menu.pd2_large_font_size*2)
	BoxGuiObject:new(main_panel.panel(main_panel, {
		layer = 100
	}), {
		sides = {
			1,
			1,
			2,
			2
		}
	})

	local jobs_scroll = ScrollablePanel:new(main_panel, "jobs_scroll", {
		ignore_up_indicator = true,
		y_padding = 0,
		ignore_down_indicator = true
	})
	local filters_panel = self._panel:panel({
		w = self._panel:w()*0.15,
		h = main_panel.h(main_panel)
	})

	filters_panel.set_top(filters_panel, main_panel.top(main_panel))
	filters_panel.set_right(filters_panel, main_panel.left(main_panel) - padding)

	local tabs_panel = self._panel:panel({
		w = main_panel.w(main_panel),
		h = tweak_data.menu.pd2_large_font_size
	})

	tabs_panel.set_left(tabs_panel, main_panel.left(main_panel))
	tabs_panel.set_bottom(tabs_panel, main_panel.top(main_panel))

	self._panels = {
		main = main_panel,
		scroll = jobs_scroll,
		canvas = jobs_scroll.canvas(jobs_scroll),
		filters = filters_panel,
		tabs = tabs_panel
	}

	return 
end
ContractBrokerGui._create_back_button = function (self)
	local back_panel = self._panel:panel({
		w = self._panels.main:w(),
		h = tweak_data.menu.pd2_small_font_size
	})

	back_panel.set_left(back_panel, self._panels.main:left())
	back_panel.set_top(back_panel, self._panels.main:bottom() + 5)

	if managers.menu:is_pc_controller() then
		self._back_button = back_panel.text(back_panel, {
			vertical = "top",
			align = "right",
			layer = 1,
			text = managers.localization:to_upper_text("menu_back"),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			color = tweak_data.screen_colors.button_stage_3
		})

		make_fine_text(self._back_button)
		self._back_button:set_right(back_panel.w(back_panel))
	end

	return 
end
ContractBrokerGui._create_legend = function (self)
	if not managers.menu:is_pc_controller() then
		local legend_items = {
			"move",
			"scroll",
			"select",
			"favourite",
			"back"
		}
		local legends = {}

		if table.contains(legend_items, "move") then
			legends[#legends + 1] = {
				string_id = "menu_legend_preview_move"
			}
		end

		if table.contains(legend_items, "scroll") then
			legends[#legends + 1] = {
				string_id = "menu_legend_scroll"
			}
		end

		if table.contains(legend_items, "select") then
			legends[#legends + 1] = {
				string_id = "menu_legend_select"
			}
		end

		if table.contains(legend_items, "favourite") then
			legends[#legends + 1] = {
				string_id = "menu_legend_broker_favorite"
			}
		end

		if table.contains(legend_items, "back") then
			legends[#legends + 1] = {
				string_id = "menu_legend_back"
			}
		end

		if table.contains(legend_items, "zoom") then
			legends[#legends + 1] = {
				string_id = "menu_legend_preview_zoom"
			}
		end

		local legend_text = ""

		for i, legend in ipairs(legends) do
			local spacing = (1 < i and "  |  ") or ""
			legend_text = legend_text .. spacing .. managers.localization:to_upper_text(legend.string_id)
		end

		self._legends_panel = self._panel:panel({
			w = self._panel:w()*0.75,
			h = tweak_data.menu.pd2_medium_font_size
		})

		self._legends_panel:set_rightbottom(self._panel:w(), self._panel:h())
		self._legends_panel:text({
			name = "text",
			vertical = "bottom",
			align = "right",
			blend_mode = "add",
			halign = "right",
			valign = "bottom",
			text = legend_text,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			color = tweak_data.screen_colors.text
		})
	end

	return 
end
ContractBrokerGui._setup_tabs = function (self)
	local last_x = 0

	if not managers.menu:is_pc_controller() then
		local icon_text = self._panels.tabs:text({
			vertical = "top",
			alpha = 1,
			align = "center",
			layer = 1,
			text = managers.localization:get_default_macro("BTN_BOTTOM_L"),
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size,
			color = tweak_data.screen_colors.text,
			x = padding,
			y = padding
		})

		make_fine_text(icon_text)
		icon_text.set_x(icon_text, padding)

		last_x = icon_text.right(icon_text)
	end

	for _, data in ipairs(self.tabs) do
		local tab_panel = self._panels.tabs:panel({})
		local text = tab_panel.text(tab_panel, {
			vertical = "top",
			alpha = 0.8,
			align = "left",
			layer = 1,
			text = managers.localization:to_upper_text(data[1]),
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size,
			color = tweak_data.screen_colors.text,
			x = padding,
			y = padding
		})

		make_fine_text(text)
		tab_panel.set_w(tab_panel, padding*2 + text.w(text))
		tab_panel.set_x(tab_panel, last_x)

		last_x = tab_panel.right(tab_panel)

		if data[3] then
			tab_panel.set_visible(tab_panel, data[3](self))
		end

		table.insert(self._buttons, {
			type = "tab",
			element = text
		})
		table.insert(self._tab_buttons, text)
	end

	if not managers.menu:is_pc_controller() then
		local icon_text = self._panels.tabs:text({
			vertical = "top",
			alpha = 1,
			align = "center",
			layer = 1,
			text = managers.localization:get_default_macro("BTN_BOTTOM_R"),
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size,
			color = tweak_data.screen_colors.text,
			x = padding,
			y = padding
		})

		make_fine_text(icon_text)
		icon_text.set_x(icon_text, last_x)
	end

	return 
end
ContractBrokerGui._setup_filters = function (self)
	for _, btn in ipairs(self._filter_buttons) do
		self._panels.filters:remove(btn)
	end

	self._filter_buttons = {}
	local tab_data = self.tabs[self._current_tab]
	local filters_clbk = callback(self, self, tab_data[2])

	filters_clbk()

	self._search_filter_visible = false

	if self._search and alive(self._search.text) and 0 < #self._search.text:text() then
		local last_y = 0
		local num_filters = #self._filter_buttons

		if 0 < num_filters then
			last_y = self._filter_buttons[num_filters]:bottom() + 1
		end

		self._add_filter_button(self, "menu_filter_search_results", last_y)

		self._current_filter = num_filters + 1
		self._search_filter_visible = true
	end

	if not alive(self._filter_selection_bg) then
		self._filter_selection_bg = self._panels.filters:rect({
			blend_mode = "add",
			alpha = 0.4,
			layer = 1,
			color = tweak_data.screen_colors.button_stage_3,
			w = self._panels.filters:w(),
			h = tweak_data.menu.pd2_small_font_size
		})
	end

	if not managers.menu:is_pc_controller() then
		if 1 < table.size(self._filter_buttons) then
			for _, btn in ipairs(self._filter_buttons) do
				btn.set_y(btn, btn.y(btn) + tweak_data.menu.pd2_small_font_size)
			end

			if not self._filter_up then
				self._filter_up = self._panels.filters:text({
					vertical = "top",
					alpha = 1,
					align = "right",
					layer = 1,
					text = managers.localization:get_default_macro("BTN_TOP_L"),
					font = tweak_data.menu.pd2_small_font,
					font_size = tweak_data.menu.pd2_small_font_size,
					color = tweak_data.screen_colors.text,
					x = padding,
					y = padding
				})

				make_fine_text(self._filter_up)
			end

			self._filter_up:set_right(self._panels.filters:w() - 4)
			self._filter_up:set_bottom(self._filter_buttons[1]:top())
			self._filter_up:set_visible(true)

			if not self._filter_down then
				self._filter_down = self._panels.filters:text({
					vertical = "top",
					alpha = 1,
					align = "right",
					layer = 1,
					text = managers.localization:get_default_macro("BTN_TOP_R"),
					font = tweak_data.menu.pd2_small_font,
					font_size = tweak_data.menu.pd2_small_font_size,
					color = tweak_data.screen_colors.text,
					x = padding,
					y = padding
				})

				make_fine_text(self._filter_down)
			end

			self._filter_down:set_right(self._panels.filters:w() - 4)
			self._filter_down:set_top(self._filter_buttons[#self._filter_buttons]:bottom())
			self._filter_down:set_visible(true)
		else
			if self._filter_up then
				self._filter_up:set_visible(false)
			end

			if self._filter_down then
				self._filter_down:set_visible(false)
			end
		end
	end

	if managers.menu:is_pc_controller() and not self._search then
		local search_panel = self._panels.filters:panel({
			alpha = 1,
			layer = 2,
			h = tweak_data.menu.pd2_small_font_size
		})

		search_panel.set_w(search_panel, self._panels.filters:w())
		search_panel.set_h(search_panel, tweak_data.menu.pd2_small_font_size)
		search_panel.set_bottom(search_panel, self._panels.filters:h())
		search_panel.set_right(search_panel, self._panels.filters:w() - 4)

		local search_placeholder = search_panel.text(search_panel, {
			vertical = "top",
			align = "right",
			alpha = 0.6,
			layer = 2,
			text = managers.localization:to_upper_text("menu_filter_search"),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			color = tweak_data.screen_colors.text
		})
		local search_text = search_panel.text(search_panel, {
			vertical = "top",
			alpha = 1,
			align = "right",
			text = "",
			layer = 2,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			color = tweak_data.screen_colors.text,
			w = search_panel.w(search_panel) - 3
		})
		local caret = search_panel.rect(search_panel, {
			name = "caret",
			h = 0,
			y = 0,
			w = 0,
			x = 0,
			layer = 200,
			color = Color(0.05, 1, 1, 1)
		})

		caret.set_right(caret, search_panel.w(search_panel)*0.5)

		self._search = {
			panel = search_panel,
			placeholder = search_placeholder,
			text = search_text,
			caret = caret
		}
	end

	return 
end
ContractBrokerGui._add_filter_button = function (self, text_id, y, params)
	local text = self._panels.filters:text({
		vertical = "top",
		align = "right",
		alpha = 1,
		layer = 2,
		text = managers.localization:to_upper_text(text_id),
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		color = tweak_data.screen_colors.button_stage_3
	})

	make_fine_text(text)
	text.set_w(text, self._panels.filters:w())
	text.set_h(text, tweak_data.menu.pd2_small_font_size + ((params and params.extra_h) or 0))
	text.set_y(text, y)
	text.set_right(text, self._panels.filters:w() - 4)
	table.insert(self._buttons, {
		type = "filter",
		element = text
	})
	table.insert(self._filter_buttons, text)

	return text
end
ContractBrokerGui._setup_filter_none = function (self)
	return 
end
ContractBrokerGui._setup_filter_contact = function (self)
	local contacts = {}
	local filters = {}

	for index, job_id in ipairs(tweak_data.narrative:get_jobs_index()) do
		local job_tweak = tweak_data.narrative:job_data(job_id)
		local contact = job_tweak.contact
		local contact_tweak = tweak_data.narrative.contacts[contact]

		if contact then
			local allow_contact = true
			allow_contact = not table.contains(contacts, contact) and (not contact_tweak or not contact_tweak.hidden)

			if allow_contact then
				table.insert(contacts, contact)
				table.insert(filters, {
					id = contact,
					data = contact_tweak
				})
			end
		end
	end

	table.sort(filters, function (a, b)
		return managers.localization:to_upper_text(a.data.name_id) < managers.localization:to_upper_text(b.data.name_id)
	end)

	local last_y = 0

	for _, contact in ipairs(filters) do
		local text = self._add_filter_button(self, contact.data.name_id, last_y)
		last_y = text.bottom(text) + 1
	end

	self._contact_filter_list = filters

	self.add_filter(self, "contact", ContractBrokerGui.perform_filter_contact)
	self.set_sorting_function(self, ContractBrokerGui.perform_standard_sort)

	return 
end
ContractBrokerGui._setup_filter_time = function (self)
	local times = {
		{
			"menu_filter_heist_short"
		},
		{
			"menu_filter_heist_medium"
		},
		{
			"menu_filter_heist_long"
		}
	}
	local last_y = 0

	for _, filter in ipairs(times) do
		local text = self._add_filter_button(self, filter[1], last_y, {
			extra_h = 4
		})
		last_y = text.bottom(text) + 1
	end

	self.add_filter(self, "job_id", ContractBrokerGui.perform_filter_time)
	self.set_sorting_function(self, ContractBrokerGui.perform_standard_sort)

	return 
end
ContractBrokerGui._setup_filter_tactic = function (self)
	local tactics = {
		{
			"menu_filter_tactic_loud_only"
		},
		{
			"menu_filter_tactic_stealth_only"
		},
		{
			"menu_filter_tactic_stealthable"
		}
	}
	local last_y = 0

	for _, filter in ipairs(tactics) do
		local text = self._add_filter_button(self, filter[1], last_y)
		last_y = text.bottom(text) + 1
	end

	self.add_filter(self, "job", ContractBrokerGui.perform_filter_tactic)
	self.set_sorting_function(self, ContractBrokerGui.perform_standard_sort)

	return 
end
ContractBrokerGui._setup_filter_most_played = function (self)
	local played = {
		{
			"menu_filter_most_played"
		},
		{
			"menu_filter_least_played"
		}
	}
	local last_y = 0

	for _, filter in ipairs(played) do
		local text = self._add_filter_button(self, filter[1], last_y)
		last_y = text.bottom(text) + 1
	end

	self.set_sorting_function(self, ContractBrokerGui.perform_most_played_sort)

	return 
end
ContractBrokerGui._setup_filter_favourite = function (self)
	self._favourite_jobs = managers.crimenet:get_favourite_jobs()

	self.add_filter(self, "job_id", ContractBrokerGui.perform_filter_favourites)
	self.set_sorting_function(self, ContractBrokerGui.perform_standard_sort)

	return 
end
ContractBrokerGui.perform_filter_contact = function (self, value)
	return value == self._contact_filter_list[self._current_filter] or self._contact_filter_list[1] or {}.id
end
ContractBrokerGui.perform_filter_time = function (self, value)
	local job_tweak = tweak_data.narrative:job_data(value)

	if job_tweak.job_wrapper then
		job_tweak = tweak_data.narrative.jobs[job_tweak.job_wrapper[1]]

		return ((job_tweak and job_tweak.chain and table.size(job_tweak.chain)) or 1) == self._current_filter
	else
		return table.size(job_tweak.chain) == self._current_filter
	end

	return 
end
ContractBrokerGui.perform_filter_tactic = function (self, job_tweak, wrapped_tweak)
	local allow = false
	local chain = (wrapped_tweak and wrapped_tweak.chain) or job_tweak.chain

	for _, data in ipairs(chain) do
		local level_data = tweak_data.levels[data.level_id]

		if level_data then
			if self._current_filter == 1 then
				allow = allow or level_data.ghost_bonus == nil
			elseif self._current_filter == 2 then
				allow = allow or level_data.ghost_required or level_data.ghost_required_visual
			elseif self._current_filter == 3 then
				allow = allow or level_data.ghost_bonus ~= nil
			end
		end
	end

	return allow
end
ContractBrokerGui.perform_filter_favourites = function (self, value)
	return self._favourite_jobs[value] == true
end
ContractBrokerGui.perform_filter_search = function (self, job_tweak)
	if self._search and alive(self._search.text) then
		local search_text = string.lower(self._search.text:text())

		if 0 < #search_text then
			local job_name = string.lower(managers.localization:text(job_tweak.name_id or "no name_id"))

			if string.find(job_name, search_text, nil, true) then
				return true
			end

			local contact_tweak = tweak_data.narrative.contacts[job_tweak.contact]

			if contact_tweak then
				local contact_name = string.lower(managers.localization:text(contact_tweak.name_id or "no name_id"))

				if string.find(contact_name, search_text, nil, true) then
					return true
				end
			end

			return false
		end
	end

	return true
end
ContractBrokerGui.perform_standard_sort = function (x, y)
	if x.enabled ~= y.enabled then
		return x.enabled
	end

	if x.is_new and y.is_new then
		return x.date_value < y.date_value
	elseif x.is_new and not y.is_new then
		return true
	elseif not x.is_new and y.is_new then
		return false
	end

	local job_tweak_x = tweak_data.narrative:job_data(x.job_id)
	local job_tweak_y = tweak_data.narrative:job_data(y.job_id)
	local string_x = managers.localization:to_upper_text(job_tweak_x.name_id)
	local string_y = managers.localization:to_upper_text(job_tweak_y.name_id)
	local ids_x = Idstring(string_x)
	local ids_y = Idstring(string_y)

	if ids_x ~= ids_y then
		return string_x < string_y
	end

	if job_tweak_x.jc ~= job_tweak_y.jc then
		return job_tweak_x.jc <= job_tweak_y.jc
	end

	return false
end
ContractBrokerGui.perform_most_played_sort = function (x, y)
	local x_played = managers.crimenet:get_job_times_played(x.job_id)
	local y_played = managers.crimenet:get_job_times_played(y.job_id)

	if x_played ~= y_played then
		if not managers.menu_component:contract_broker_gui() or managers.menu_component:contract_broker_gui()._current_filter == 1 then
			return y_played < x_played
		else
			return x_played < y_played
		end
	else
		return ContractBrokerGui.perform_standard_sort(x, y)
	end

	return 
end
ContractBrokerGui.clear_filters = function (self)
	self._active_filters = {}
	self._current_filter = 1

	return 
end
ContractBrokerGui.add_filter = function (self, key, pass_func)
	self._active_filters[key] = pass_func

	return 
end
ContractBrokerGui.remove_filter = function (self, key)
	self._active_filters[key] = nil

	return 
end
ContractBrokerGui.set_sorting_function = function (self, sort_func)
	self._sorting_func = sort_func

	return 
end
ContractBrokerGui._setup_jobs = function (self)
	local date_value = {
		tonumber(os.date("%Y")),
		tonumber(os.date("%m")),
		tonumber(os.date("%d"))
	}
	date_value = date_value[1]*30*12 + date_value[2]*30 + date_value[3]
	local release_window = 7

	if alive(self._jobs_spacer) then
		self._panels.canvas:remove(self._jobs_spacer)
	end

	for _, item in ipairs(self._heist_items) do
		item.destroy(item)
	end

	self._heist_items = {}

	self._panels.scroll:scroll_to(0)

	local jobs = {}
	local max_jc = managers.job:get_max_jc_for_player()

	for index, job_id in ipairs(tweak_data.narrative:get_jobs_index()) do
		local job_tweak = tweak_data.narrative:job_data(job_id)
		local wrapped_tweak = nil

		if job_tweak.job_wrapper then
			wrapped_tweak = tweak_data.narrative.jobs[job_tweak.job_wrapper[1]]
		end

		local filter_pass = true
		local contact_pass = true
		local contact = job_tweak.contact
		local contact_tweak = tweak_data.narrative.contacts[contact]

		if contact then
			contact_pass = true
			contact_pass = not contact_tweak or not contact_tweak.hidden
		end

		if self._search_filter_visible and self._current_filter == #self._filter_buttons then
			if not self.perform_filter_search(self, job_tweak) then
				filter_pass = false
			end
		else
			for key, pass_func in pairs(self._active_filters) do
				if key == "search" then
					if not pass_func(self, job_tweak) then
						filter_pass = false

						break
					end
				elseif key == "job" then
					if not pass_func(self, job_tweak, wrapped_tweak) then
						filter_pass = false

						break
					end
				elseif key == "job_id" then
					if not pass_func(self, job_id) then
						filter_pass = false

						break
					end
				elseif job_tweak[key] then
					if not pass_func(self, job_tweak[key]) then
						filter_pass = false

						break
					end
				else
					filter_pass = false

					break
				end
			end
		end

		if filter_pass and contact_pass and not tweak_data.narrative:is_wrapped_to_job(job_id) then
			local dlc = job_tweak.dlc
			dlc = not dlc or managers.dlc:is_dlc_unlocked(dlc)
			dlc = dlc and not tweak_data.narrative:is_job_locked(job_id)
			local date_value = (job_tweak.date_added and (job_tweak.date_added[1]*30*12 + job_tweak.date_added[2]*30 + job_tweak.date_added[3]) - date_value) or false

			table.insert(jobs, {
				job_id = job_id,
				enabled = dlc and (job_tweak.jc or 0) + ((job_tweak.professional and 10) or 0) <= max_jc,
				date_value = date_value,
				is_new = date_value ~= false and -release_window <= date_value
			})
		end
	end

	table.sort(jobs, self._sorting_func)

	for idx, job in ipairs(jobs) do
		local item = ContractBrokerHeistItem:new(self._panels.canvas, job, idx)

		table.insert(self._heist_items, item)
	end

	if 0 < #self._heist_items then
		self._jobs_spacer = self._panels.canvas:panel({
			y = self._heist_items[#self._heist_items]._panel:bottom(),
			h = padding
		})
	end

	self._panels.scroll:update_canvas_size()
	self._panels.scroll:update_canvas_size()

	return 
end
ContractBrokerGui.update = function (self, t, dt)
	local cx, cy = managers.menu_component:get_right_controller_axis()

	if cy ~= 0 and self._panels.scroll then
		self._panels.scroll:perform_scroll(math.abs(cy*500*dt), math.sign(cy))
	end

	return 
end
ContractBrokerGui.refresh = function (self)
	if self._filter_buttons[self._current_filter] then
		self._filter_selection_bg:set_y(self._filter_buttons[self._current_filter]:y())
		self._filter_selection_bg:set_visible(true)
	else
		self._filter_selection_bg:set_visible(false)
	end

	for idx, btn in ipairs(self._filter_buttons) do
		if idx == self._current_filter then
			btn.set_color(btn, tweak_data.screen_colors.button_stage_2)
			btn.set_blend_mode(btn, "add")
		else
			btn.set_color(btn, tweak_data.screen_colors.button_stage_3)
			btn.set_blend_mode(btn, "normal")
		end
	end

	for idx, btn in ipairs(self._tab_buttons) do
		if idx == self._current_tab then
			btn.set_alpha(btn, 1)
		else
			btn.set_alpha(btn, 0.7)
		end
	end

	return 
end
ContractBrokerGui._move_selection = function (self, dir)
	self._set_selection(self, (self._current_selection or 0) + dir)

	return 
end
ContractBrokerGui._set_selection = function (self, idx)
	local last_selection = self._current_selection
	self._current_selection = math.clamp(idx, 1, #self._heist_items)
	local last_heist = last_selection and self._heist_items[last_selection]

	if last_heist then
		last_heist.deselect(last_heist)
	end

	local new_heist = self._current_selection and self._heist_items[self._current_selection]

	if new_heist then
		new_heist.select(new_heist)

		local scroll_panel = self._panels.scroll:scroll_panel()
		local y = self._panels.scroll:canvas():y() + new_heist.bottom(new_heist)

		if scroll_panel.h(scroll_panel) < y then
			self._panels.scroll:perform_scroll(y - scroll_panel.h(scroll_panel), -1)
		else
			y = self._panels.scroll:canvas():y() + new_heist.top(new_heist)

			if y < 0 then
				self._panels.scroll:perform_scroll(math.abs(y), 1)
			end
		end
	end

	return 
end
ContractBrokerGui.mouse_moved = function (self, button, x, y)
	local used, pointer = nil

	if not used then
		local u, p = self._panels.scroll:mouse_moved(button, x, y)

		if u then
			used = u
			pointer = p or pointer
		end
	end

	if self._filter_buttons[self._current_filter] then
		self._filter_selection_bg:set_y(self._filter_buttons[self._current_filter]:y())
		self._filter_selection_bg:set_visible(true)
	else
		self._filter_selection_bg:set_visible(false)
	end

	for idx, btn in ipairs(self._filter_buttons) do
		if not used and self._current_filter ~= idx and btn.inside(btn, x, y) then
			pointer = "link"
			used = true

			btn.set_color(btn, tweak_data.screen_colors.button_stage_2)
			btn.set_blend_mode(btn, "add")
			self._filter_selection_bg:set_y(btn.y(btn))

			if self._highlighted_filter ~= idx then
				self._highlighted_filter = idx

				managers.menu:post_event("highlight")
			end
		elseif idx == self._current_filter then
			btn.set_color(btn, tweak_data.screen_colors.button_stage_2)
			btn.set_blend_mode(btn, "add")
		else
			btn.set_color(btn, tweak_data.screen_colors.button_stage_3)
			btn.set_blend_mode(btn, "normal")
		end
	end

	for idx, btn in ipairs(self._tab_buttons) do
		if not used and self._current_tab ~= idx and btn.inside(btn, x, y) then
			pointer = "link"
			used = true

			btn.set_alpha(btn, 1)

			if self._highlighted_tab ~= idx then
				self._highlighted_tab = idx

				managers.menu:post_event("highlight")
			end
		elseif idx == self._current_tab then
			btn.set_alpha(btn, 1)
		else
			btn.set_alpha(btn, 0.7)
		end
	end

	if self._panels.main:inside(x, y) then
		for _, item in ipairs(self._heist_items) do
			local u, p = item.mouse_moved(item, button, x, y, used)

			if u then
				used = u
				pointer = p or pointer
			end
		end
	else
		for _, item in ipairs(self._heist_items) do
			item.deselect(item)
		end
	end

	if self._search and alive(self._search.panel) and not used and self._search.panel:inside(x, y) then
		used = true
		pointer = "link"
	end

	if alive(self._back_button) then
		if not used and self._back_button:inside(x, y) then
			pointer = "link"
			used = true

			if self._back_button:color() ~= tweak_data.screen_colors.button_stage_2 then
				managers.menu:post_event("highlight")
				self._back_button:set_color(tweak_data.screen_colors.button_stage_2)
				self._back_button:set_blend_mode("add")
			end
		else
			self._back_button:set_color(tweak_data.screen_colors.button_stage_3)
			self._back_button:set_blend_mode("normal")
		end
	end

	return used, pointer
end
ContractBrokerGui.mouse_clicked = function (self, o, button, x, y)
	self.disconnect_search_input(self)

	if self._scroll_event then
		self._scroll_event = nil

		return 
	end

	if self._panels.scroll:mouse_clicked(o, button, x, y) then
		self._scroll_event = true

		return true
	end

	for idx, btn in ipairs(self._filter_buttons) do
		if self._current_filter ~= idx and btn.inside(btn, x, y) then
			self._current_filter = idx

			self._setup_change_filter(self)
			managers.menu:post_event("menu_enter")

			return true
		end
	end

	for idx, btn in ipairs(self._tab_buttons) do
		if self._current_tab ~= idx and btn.inside(btn, x, y) then
			self._current_tab = idx

			self._setup_change_tab(self)
			managers.menu:post_event("menu_enter")

			return true
		end
	end

	if self._panels.main:inside(x, y) then
		for _, item in ipairs(self._heist_items) do
			if item.mouse_clicked(item, o, button, x, y) then
				return true
			end
		end
	end

	if self._search and alive(self._search.panel) and self._search.panel:inside(x, y) then
		self.connect_search_input(self)

		return true
	end

	if alive(self._back_button) and self._back_button:inside(x, y) then
		managers.menu:back()

		return true
	end

	return 
end
ContractBrokerGui.mouse_pressed = function (self, button, x, y)
	if self._panels.scroll:mouse_pressed(button, x, y) then
		self._scroll_event = true

		return true
	end

	return 
end
ContractBrokerGui.mouse_released = function (self, button, x, y)
	if self._panels.scroll:mouse_released(button, x, y) then
		self._scroll_event = true

		return true
	end

	return 
end
ContractBrokerGui.confirm_pressed = function (self)
	if self._search_focus then
		return 
	end

	local item = self._current_selection and self._heist_items[self._current_selection]

	if item then
		item.trigger(item)
	end

	return 
end
ContractBrokerGui.back_pressed = function (self)
	if self._search_focus then
		return 
	end

	managers.menu:back(true)

	return 
end
local ids_cancel = Idstring("cancel")
local ids_favourite = Idstring("menu_toggle_legends")
local ids_trigger_left = Idstring("trigger_left")
local ids_trigger_right = Idstring("trigger_right")
ContractBrokerGui.special_btn_pressed = function (self, button)
	if self._search_focus then
		return 
	end

	if button == ids_cancel then
		managers.menu:back(true)
	elseif button == ids_favourite then
		local item = self._current_selection and self._heist_items[self._current_selection]

		if item then
			item.toggle_favourite(item)
		end
	elseif button == ids_trigger_right then
		local num_filters = #self._filter_buttons

		if 1 < num_filters then
			self._current_filter = self._current_filter + 1

			if num_filters < self._current_filter then
				self._current_filter = 1
			end

			self._setup_change_filter(self)
			managers.menu:post_event("menu_enter")
		end
	elseif button == ids_trigger_left then
		local num_filters = #self._filter_buttons

		if 1 < num_filters then
			self._current_filter = self._current_filter - 1

			if self._current_filter < 1 then
				self._current_filter = num_filters
			end

			self._setup_change_filter(self)
			managers.menu:post_event("menu_enter")
		end
	end

	return 
end
ContractBrokerGui.move_up = function (self)
	self._move_selection(self, -1)

	return 
end
ContractBrokerGui.move_down = function (self)
	self._move_selection(self, 1)

	return 
end
ContractBrokerGui.next_page = function (self)
	self._current_tab = self._current_tab + 1

	if #self._tab_buttons < self._current_tab then
		self._current_tab = 1
	end

	self._setup_change_tab(self)
	managers.menu:post_event("menu_enter")

	return 
end
ContractBrokerGui.previous_page = function (self)
	self._current_tab = self._current_tab - 1

	if self._current_tab < 1 then
		self._current_tab = #self._tab_buttons
	end

	self._setup_change_tab(self)
	managers.menu:post_event("menu_enter")

	return 
end
ContractBrokerGui.mouse_wheel_up = function (self, x, y)
	self._panels.scroll:scroll(x, y, 1)

	return 
end
ContractBrokerGui.mouse_wheel_down = function (self, x, y)
	self._panels.scroll:scroll(x, y, -1)

	return 
end
ContractBrokerGui.input_focus = function (self)
	return 1
end
ContractBrokerGui.save_temporary_data = function (self, job_id)
	Global.contract_broker = {
		filter = self._current_filter,
		tab = self._current_tab,
		job_id = job_id
	}

	return 
end
ContractBrokerGui.connect_search_input = function (self)
	if not self._search_focus then
		self._ws:connect_keyboard(Input:keyboard())
		self._search.panel:key_press(callback(self, self, "search_key_press"))
		self._search.panel:key_release(callback(self, self, "search_key_release"))

		self._enter_callback = callback(self, self, "enter_key_callback")
		self._esc_callback = callback(self, self, "esc_key_callback")
		self._search_focus = true
		self._focus = true

		self.update_caret(self)
	end

	return 
end
ContractBrokerGui.disconnect_search_input = function (self)
	if self._search_focus then
		self._ws:disconnect_keyboard()
		self._search.panel:key_press(nil)
		self._search.panel:key_release(nil)

		self._search_focus = nil
		self._focus = nil

		self.update_caret(self)
	end

	return 
end
ContractBrokerGui.search_key_press = function (self, o, k)
	if self._skip_first then
		self._skip_first = false

		return 
	end

	if not self._enter_text_set then
		self._search.panel:enter_text(callback(self, self, "enter_text"))

		self._enter_text_set = true
	end

	local text = self._search.text
	local s, e = text.selection(text)
	local n = utf8.len(text.text(text))
	local d = math.abs(e - s)
	self._key_pressed = k

	text.stop(text)
	text.animate(text, callback(self, self, "update_key_down"), k)

	local len = utf8.len(text.text(text))

	text.clear_range_color(text, 0, len)

	if k == Idstring("backspace") then
		if s == e and 0 < s then
			text.set_selection(text, s - 1, e)
		end

		text.replace_text(text, "")
	elseif k == Idstring("delete") then
		if s == e and s < n then
			text.set_selection(text, s, e + 1)
		end

		text.replace_text(text, "")
	elseif k == Idstring("insert") then
		local clipboard = Application:get_clipboard() or ""

		text.replace_text(text, clipboard)

		local lbs = text.line_breaks(text)

		if ContractBrokerGui.MAX_SEARCH_LENGTH < #text.text(text) then
			text.set_text(text, string.sub(text.text(text), 1, ContractBrokerGui.MAX_SEARCH_LENGTH))
		end

		if 1 < #lbs then
			local s = lbs[2]
			local e = utf8.len(text.text(text))

			text.set_selection(text, s, e)
			text.replace_text(text, "")
		end
	elseif k == Idstring("left") then
		if s < e then
			text.set_selection(text, s, s)
		elseif 0 < s then
			text.set_selection(text, s - 1, s - 1)
		end
	elseif k == Idstring("right") then
		if s < e then
			text.set_selection(text, e, e)
		elseif s < n then
			text.set_selection(text, s + 1, s + 1)
		end
	elseif self._key_pressed == Idstring("end") then
		text.set_selection(text, n, n)
	elseif self._key_pressed == Idstring("home") then
		text.set_selection(text, 0, 0)
	elseif k == Idstring("enter") then
		if type(self._enter_callback) ~= "number" then
			self._enter_callback()
		end
	elseif k == Idstring("esc") and type(self._esc_callback) ~= "number" then
		text.set_text(text, "")
		text.set_selection(text, 0, 0)
		self._esc_callback()
	end

	self.update_caret(self)

	return 
end
ContractBrokerGui.search_key_release = function (self, o, k)
	if self._key_pressed == k then
		self._key_pressed = false
	end

	return 
end
ContractBrokerGui.update_key_down = function (self, o, k)
	wait(0.6)

	local text = self._search.text

	while self._key_pressed == k do
		local s, e = text.selection(text)
		local n = utf8.len(text.text(text))
		local d = math.abs(e - s)

		if self._key_pressed == Idstring("backspace") then
			if s == e and 0 < s then
				text.set_selection(text, s - 1, e)
			end

			text.replace_text(text, "")
		elseif self._key_pressed == Idstring("delete") then
			if s == e and s < n then
				text.set_selection(text, s, e + 1)
			end

			text.replace_text(text, "")
		elseif self._key_pressed == Idstring("insert") then
			local clipboard = Application:get_clipboard() or ""

			text.replace_text(text, clipboard)

			if ContractBrokerGui.MAX_SEARCH_LENGTH < #text.text(text) then
				text.set_text(text, string.sub(text.text(text), 1, ContractBrokerGui.MAX_SEARCH_LENGTH))
			end

			local lbs = text.line_breaks(text)

			if 1 < #lbs then
				local s = lbs[2]
				local e = utf8.len(text.text(text))

				text.set_selection(text, s, e)
				text.replace_text(text, "")
			end
		elseif self._key_pressed == Idstring("left") then
			if s < e then
				text.set_selection(text, s, s)
			elseif 0 < s then
				text.set_selection(text, s - 1, s - 1)
			end
		elseif self._key_pressed == Idstring("right") then
			if s < e then
				text.set_selection(text, e, e)
			elseif s < n then
				text.set_selection(text, s + 1, s + 1)
			end
		else
			self._key_pressed = false
		end

		self.update_caret(self)
		wait(0.03)
	end

	return 
end
ContractBrokerGui.enter_text = function (self, o, s)
	if self._skip_first then
		self._skip_first = false

		return 
	end

	local text = self._search.text

	if #text.text(text) < ContractBrokerGui.MAX_SEARCH_LENGTH then
		text.replace_text(text, s)
	end

	local lbs = text.line_breaks(text)

	if 1 < #lbs then
		local s = lbs[2]
		local e = utf8.len(text.text(text))

		text.set_selection(text, s, e)
		text.replace_text(text, "")
	end

	self.update_caret(self)
	self._setup_change_search(self)

	return 
end
ContractBrokerGui.enter_key_callback = function (self)
	self._setup_change_search(self)

	return 
end
ContractBrokerGui.esc_key_callback = function (self)
	self.disconnect_search_input(self)

	return 
end
ContractBrokerGui.blink = function (o)
	while true do
		o.set_color(o, Color(0, 1, 1, 1))
		wait(0.3)
		o.set_color(o, Color.white)
		wait(0.3)
	end

	return 
end
ContractBrokerGui.set_blinking = function (self, b)
	local caret = self._search.caret

	if b == self._blinking then
		return 
	end

	if b then
		caret.animate(caret, self.blink)
	else
		caret.stop(caret)
	end

	self._blinking = b

	if not self._blinking then
		caret.set_color(caret, Color.white)
	end

	return 
end
ContractBrokerGui.update_caret = function (self)
	local text = self._search.text
	local caret = self._search.caret
	local s, e = text.selection(text)
	local x, y, w, h = text.selection_rect(text)

	if s == 0 and e == 0 then
		if text.align(text) == "center" then
			x = text.world_x(text) + text.w(text)/2
		else
			x = text.world_x(text) + text.w(text)
		end

		y = text.world_y(text)
	end

	h = text.h(text)

	if w < 3 then
		w = 3
	end

	if not self._focus then
		w = 0
		h = 0
	end

	caret.set_world_shape(caret, x, y + 2, w, h - 4)
	self.set_blinking(self, s == e and self._focus)

	local text_s = text.text(text)

	self._search.placeholder:set_visible(#text_s == 0)

	return 
end

return 
