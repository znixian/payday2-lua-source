RaidMenuGui = RaidMenuGui or class(PromotionalMenuGui)
RaidMenuGui.RaidAppId = "414740"
RaidMenuGui.RaidBetaAppId = "704580"
RaidMenuGui.init = function (self, ws, fullscreen_ws, node, promo_menu_id)
	RaidMenuGui.super.init(self, ws, fullscreen_ws, node)

	self._promo_menu = tweak_data.promos.menus[promo_menu_id]
	self._theme = tweak_data.promos.themes.raid

	self.setup(self, self._promo_menu, self._theme)

	return 
end
RaidMenuGui.launch_raid = function (self)
	local dialog_data = {
		title = managers.localization:text("dialog_play_raid_beta"),
		text = managers.localization:text("dialog_play_raid_beta_text")
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_launch_raid_dialog_yes")
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = callback(self, self, "_launch_raid_dialog_no"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
RaidMenuGui._launch_raid_dialog_yes = function (self)
	managers.savefile:save_progress("local_hdd")
	setup:quit()
	os.execute("cmd /c start steam://run/" .. tostring(RaidMenuGui.RaidBetaAppId))

	return 
end
RaidMenuGui._launch_raid_dialog_no = function (self)
	return 
end
RaidMenuGui.open_raid_weapons_menu = function (self)
	managers.menu:open_node("raid_beta_weapons", {})
	managers.menu:post_event("menu_enter")

	return 
end
RaidMenuGui.open_raid_masks_menu = function (self)
	managers.menu:open_node("raid_beta_masks", {})
	managers.menu:post_event("menu_enter")

	return 
end
RaidMenuGui.open_raid_trailer = function (self)
	Steam:overlay_activate("url", "https://www.youtube.com/embed/XARRgLUzSiA")

	return 
end
RaidMenuGui.open_dev_diary_trailer = function (self)
	Steam:overlay_activate("url", "https://www.youtube.com/embed/cm98FnsSKvY")

	return 
end
RaidMenuGui.open_gameplay_trailer = function (self)
	Steam:overlay_activate("url", "https://www.youtube.com/embed/Kl2qT-UJVJ4")

	return 
end
RaidMenuGui.open_raid_gang = function (self)
	Steam:overlay_activate("url", "http://www.raidworldwar2.com/#characters")

	return 
end
RaidMenuGui.open_raid_feedback = function (self)
	Steam:overlay_activate("url", "https://steamcommunity.com/games/" .. RaidMenuGui.RaidAppId .. "/")

	return 
end
RaidMenuGui.open_raid_special_edition_menu = function (self)
	managers.menu:open_node("raid_beta_special", {})
	managers.menu:post_event("menu_enter")

	return 
end
RaidMenuGui.open_raid_special_edition = function (self)
	Steam:overlay_activate("url", "https://store.steampowered.com/app/" .. RaidMenuGui.RaidAppId .. "/")

	return 
end
RaidMenuGui.open_raid_twitch = function (self)
	Steam:overlay_activate("url", "https://www.twitch.tv/liongamelion")

	return 
end
RaidMenuGui.open_raid_preorder_menu = function (self)
	managers.menu:open_node("raid_beta_preorder", {})
	managers.menu:post_event("menu_enter")

	return 
end
RaidMenuGui.open_raid_preorder = function (self)
	Steam:overlay_activate("url", "https://store.steampowered.com/app/" .. RaidMenuGui.RaidAppId .. "/")

	return 
end
RaidMenuGui.preview_breech = function (self)
	managers.blackmarket:view_weapon_platform("breech", callback(self, self, "_open_preview_node", {
		id = "breech",
		category = "secondaries"
	}))

	return 
end
RaidMenuGui.preview_ching = function (self)
	managers.blackmarket:view_weapon_platform("ching", callback(self, self, "_open_preview_node", {
		id = "ching",
		category = "primaries"
	}))

	return 
end
RaidMenuGui.preview_erma = function (self)
	managers.blackmarket:view_weapon_platform("erma", callback(self, self, "_open_preview_node", {
		id = "erma",
		category = "secondaries"
	}))

	return 
end
RaidMenuGui.preview_push = function (self)
	managers.menu:open_node("raid_weapon_preview_node", {
		{
			category = "melee",
			item_id = "push"
		}
	})
	managers.blackmarket:preview_melee_weapon("push")

	return 
end
RaidMenuGui.preview_grip = function (self)
	managers.menu:open_node("raid_weapon_preview_node", {
		{
			category = "melee",
			item_id = "grip"
		}
	})
	managers.blackmarket:preview_melee_weapon("grip")

	return 
end
RaidMenuGui._open_preview_node = function (self, data)
	managers.menu:open_node("raid_weapon_preview_node", {
		{
			item_id = data.id,
			category = data.category
		}
	})

	return 
end
RaidMenuGui.preview_jfr_01 = function (self)
	self._preview_mask(self, "jfr_01")

	return 
end
RaidMenuGui.preview_jfr_02 = function (self)
	self._preview_mask(self, "jfr_02")

	return 
end
RaidMenuGui.preview_jfr_03 = function (self)
	self._preview_mask(self, "jfr_03")

	return 
end
RaidMenuGui.preview_jfr_04 = function (self)
	self._preview_mask(self, "jfr_04")

	return 
end
RaidMenuGui._preview_mask = function (self, mask_id)
	managers.blackmarket:view_mask_with_mask_id(mask_id)
	managers.menu:open_node("raid_weapon_preview_node", {
		{
			category = "mask",
			item_id = mask_id
		}
	})

	return 
end

return 
