CircleGuiObject = CircleGuiObject or class()
CircleGuiObject.init = function (self, panel, config)
	self._panel = panel
	self._radius = config.radius or 20
	self._sides = config.sides or 10
	self._total = config.total or 1
	config.triangles = self._create_triangles(self, config)
	config.w = self._radius*2
	config.h = self._radius*2
	self._circle = self._panel:polygon(config)

	return 
end
CircleGuiObject._create_triangles = function (self, config)
	local amount = ((config.current or 1)*360)/(config.total or 1)
	local s = self._radius
	local triangles = {}
	local step = self._sides/360

	for i = step, amount, step do
		local mid = Vector3(self._radius, self._radius, 0)

		table.insert(triangles, mid)
		table.insert(triangles, mid + Vector3(math.sin(i)*self._radius, -math.cos(i)*self._radius, 0))
		table.insert(triangles, mid + Vector3(math.sin(i - step)*self._radius, -math.cos(i - step)*self._radius, 0))
	end

	return triangles
end
CircleGuiObject.set_current = function (self, current)
	local triangles = self._create_triangles(self, {
		current = current,
		total = self._total
	})

	self._circle:clear()
	self._circle:add_triangles(triangles)

	return 
end
CircleGuiObject.set_position = function (self, x, y)
	self._circle:set_position(x, y)

	return 
end
CircleGuiObject.set_layer = function (self, layer)
	self._circle:set_layer(layer)

	return 
end
CircleGuiObject.layer = function (self)
	return self._circle:layer()
end
CircleGuiObject.remove = function (self)
	self._panel:remove(self._circle)

	return 
end
CircleBitmapGuiObject = CircleBitmapGuiObject or class()
CircleBitmapGuiObject.init = function (self, panel, config)
	self._panel = panel
	self._radius = config.radius or 20
	self._sides = config.sides or 64
	self._total = config.total or 1
	self._size = 128
	config.texture_rect = nil
	config.texture = config.image or "guis/textures/pd2/hud_progress_active"
	config.w = self._radius*2
	config.h = self._radius*2
	self._circle = self._panel:bitmap(config)

	self._circle:set_render_template(Idstring("VertexColorTexturedRadial"))

	self._alpha = self._circle:color().alpha

	self._circle:set_color(self._circle:color():with_red(0))

	if config.use_bg then
		local bg_config = deep_clone(config)
		bg_config.texture = config.bg or "guis/textures/pd2/hud_progress_bg"
		bg_config.layer = bg_config.layer - 1
		bg_config.blend_mode = "normal"
		self._bg_circle = self._panel:bitmap(bg_config)
	end

	return 
end
CircleBitmapGuiObject.radius = function (self)
	return self._radius
end
CircleBitmapGuiObject.set_current = function (self, current)
	local j = math.mod(math.floor(current), 8)
	local i = math.floor(current/8)

	self._circle:set_color(Color(self._alpha, current, self._circle:color().blue, self._circle:color().green))

	return 
end
CircleBitmapGuiObject.position = function (self)
	return self._circle:position()
end
CircleBitmapGuiObject.set_position = function (self, x, y)
	self._circle:set_position(x, y)

	if self._bg_circle then
		self._bg_circle:set_position(x, y)
	end

	return 
end
CircleBitmapGuiObject.set_visible = function (self, visible)
	self._circle:set_visible(visible)

	if self._bg_circle then
		self._bg_circle:set_visible(visible)
	end

	return 
end
CircleBitmapGuiObject.visible = function (self)
	return self._circle:visible()
end
CircleBitmapGuiObject.set_alpha = function (self, alpha)
	self._circle:set_alpha(alpha)

	return 
end
CircleBitmapGuiObject.alpha = function (self)
	self._circle:alpha()

	return 
end
CircleBitmapGuiObject.set_color = function (self, color)
	self._circle:set_color(color)

	return 
end
CircleBitmapGuiObject.color = function (self)
	return self._circle:color()
end
CircleBitmapGuiObject.size = function (self)
	return self._circle:size()
end
CircleBitmapGuiObject.set_image = function (self, texture)
	self._circle:set_image(texture)

	return 
end
CircleBitmapGuiObject.set_layer = function (self, layer)
	self._circle:set_layer(layer)

	if self._bg_circle then
		self._bg_circle:set_layer(layer - 1)
	end

	return 
end
CircleBitmapGuiObject.layer = function (self)
	return self._circle:layer()
end
CircleBitmapGuiObject.remove = function (self)
	self._panel:remove(self._circle)

	if self._bg_circle then
		self._panel:remove(self._bg_circle)
	end

	self._panel = nil

	return 
end

return 
