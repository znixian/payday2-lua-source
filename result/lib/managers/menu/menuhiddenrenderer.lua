MenuHiddenRenderer = MenuHiddenRenderer or class(MenuRenderer)
MenuHiddenRenderer.init = function (self, ...)
	MenuHiddenRenderer.super.init(self, ...)

	self._disable_blackborder = true

	return 
end
MenuHiddenRenderer.open = function (self, ...)
	MenuHiddenRenderer.super.open(self, ...)
	self._main_panel:root():hide()

	return 
end
MenuHiddenRenderer.show = function (self)
	MenuHiddenRenderer.super.show(self)
	self._main_panel:root():hide()

	return 
end
MenuHiddenRenderer.hide = function (self)
	MenuHiddenRenderer.super.hide(self)
	self._main_panel:root():hide()

	return 
end

return 
