CrimeSpreeContractBoxGui = CrimeSpreeContractBoxGui or class()
CrimeSpreeContractBoxGui.init = function (self, ws, fullscreen_ws)
	self._ws = ws
	self._fullscreen_ws = fullscreen_ws
	self._panel = self._ws:panel():panel()
	self._fullscreen_panel = self._fullscreen_ws:panel():panel()
	self._peer_panels = {}

	if not self._can_update(self) then
		for i = 1, tweak_data.max_players, 1 do
			self._check_create_peer_panel(self, i)
		end
	end

	self._enabled = true

	return 
end
CrimeSpreeContractBoxGui.set_enabled = function (self, enabled)
	self._enabled = enabled

	return 
end
CrimeSpreeContractBoxGui.close = function (self)
	self._ws:panel():remove(self._panel)
	self._fullscreen_ws:panel():remove(self._fullscreen_panel)

	return 
end
CrimeSpreeContractBoxGui._can_update = function (self)
	return not Global.game_settings.single_player
end
CrimeSpreeContractBoxGui._check_create_peer_panel = function (self, peer_id)
	if not self._peer_panels[peer_id] then
		local peer = managers.network:session():peer(peer_id)

		if peer then
			local char_data = LobbyCharacterData:new(self._panel, peer)
			self._peer_panels[peer_id] = char_data
		end
	end

	return self._peer_panels[peer_id]
end
CrimeSpreeContractBoxGui.update_character_menu_state = function (self, peer_id, state)
	if not self._can_update(self) then
		return 
	end

	local panel = self._check_create_peer_panel(self, peer_id)

	if panel then
		panel.update_peer_id(panel, peer_id)
		panel.update_character_menu_state(panel, state)
	end

	return 
end
CrimeSpreeContractBoxGui.update_character = function (self, peer_id)
	if not self._can_update(self) then
		return 
	end

	local panel = self._check_create_peer_panel(self, peer_id)

	if panel then
		panel.update_peer_id(panel, peer_id)
		panel.update_character(panel)
	end

	return 
end
CrimeSpreeContractBoxGui.update_bg_state = function (self, peer_id, state)
	return 
end
CrimeSpreeContractBoxGui.set_character_panel_alpha = function (self, peer_id, alpha)
	if not self._can_update(self) then
		return 
	end

	local panel = self._check_create_peer_panel(self, peer_id)

	if panel then
		panel.update_peer_id(panel, peer_id)
		panel.set_alpha(panel, alpha)
	end

	return 
end
CrimeSpreeContractBoxGui.refresh = function (self)
	return 
end
CrimeSpreeContractBoxGui.update = function (self, t, dt)
	if not self._can_update(self) then
		return 
	end

	for i = 1, 4, 1 do
		self.update_character(self, i)
	end

	return 
end
CrimeSpreeContractBoxGui.mouse_pressed = function (self, button, x, y)
	if not self.can_take_input(self) or not self._can_update(self) then
		return 
	end

	if button == Idstring("0") and self._peer_panels and SystemInfo:platform() == Idstring("WIN32") and MenuCallbackHandler:is_overlay_enabled() then
		for peer_id, object in pairs(self._peer_panels) do
			if alive(object.panel(object)) and object.panel(object):inside(x, y) then
				local peer = managers.network:session() and managers.network:session():peer(peer_id)

				if peer then
					Steam:overlay_activate("url", tweak_data.gui.fbi_files_webpage .. "/suspect/" .. peer.user_id(peer) .. "/")

					return 
				end
			end
		end
	end

	return 
end
CrimeSpreeContractBoxGui.mouse_moved = function (self, x, y)
	if not self.can_take_input(self) or not self._can_update(self) then
		return 
	end

	local used = false
	local pointer = "arrow"

	if self._peer_panels and SystemInfo:platform() == Idstring("WIN32") and MenuCallbackHandler:is_overlay_enabled() then
		for peer_id, object in pairs(self._peer_panels) do
			if alive(object.panel(object)) and object.panel(object):inside(x, y) then
				used = true
				pointer = "link"
			end
		end
	end

	if used then
		return used, pointer
	end

	return 
end
CrimeSpreeContractBoxGui.can_take_input = function (self)
	if managers.menu_component and managers.menu_component:crime_spree_modifiers() then
		return false
	end

	return true
end
CrimeSpreeContractBoxGui.check_minimize = function (self)
	return false
end
CrimeSpreeContractBoxGui.moved_scroll_bar = function (self)
	return 
end
CrimeSpreeContractBoxGui.mouse_wheel_down = function (self)
	return 
end
CrimeSpreeContractBoxGui.mouse_wheel_up = function (self)
	return 
end
CrimeSpreeContractBoxGui.check_grab_scroll_bar = function (self)
	return false
end
CrimeSpreeContractBoxGui.release_scroll_bar = function (self)
	return false
end

return 
