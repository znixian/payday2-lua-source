CrimeSpreeRewardsDetailsPage = CrimeSpreeRewardsDetailsPage or class(CrimeSpreeDetailsPage)
local padding = 10
CrimeSpreeRewardsDetailsPage.init = function (self, ...)
	CrimeSpreeRewardsDetailsPage.super.init(self, ...)
	self.panel(self):bitmap({
		texture = "guis/textures/test_blur_df",
		layer = -1,
		halign = "scale",
		alpha = 1,
		render_template = "VertexColorTexturedBlur3D",
		valign = "scale",
		w = self.panel(self):w(),
		h = self.panel(self):h()
	})
	self.panel(self):rect({
		alpha = 0.5,
		valign = "scale",
		halign = "scale",
		color = Color.black
	})

	local outline_panel = self.panel(self):panel({})

	BoxGuiObject:new(outline_panel, {
		sides = {
			4,
			4,
			2,
			1
		}
	})

	local w = (self.panel(self):w() - padding)/#tweak_data.crime_spree.rewards
	local count = 0

	for i, data in ipairs(tweak_data.crime_spree.rewards) do
		local amount = math.max(math.floor(data.amount*managers.crime_spree:reward_level()), 0)

		if 0 < amount or data.always_show then
			local panel = self.panel(self):panel({
				w = w,
				x = count*w
			})
			local card_layer = 50
			local num_cards = math.clamp(math.floor(amount/(data.card_inc or 1)), 1, data.max_cards)
			local upcard = self.create_card(self, panel, data.icon, card_layer, 10, 0, 1 < num_cards)

			for i = 1, num_cards - 1, 1 do
				self.create_card(self, panel, data.icon, card_layer - i*2, 10, 6, true)
			end

			local reward_name = panel.text(panel, {
				vertical = "center",
				h = 32,
				wrap = true,
				align = "center",
				word_wrap = true,
				blend_mode = "add",
				layer = 11,
				name = "reward" .. tostring(i),
				text = managers.localization:to_upper_text(data.name_id or ""),
				w = panel.w(panel),
				font_size = tweak_data.menu.pd2_small_font_size*0.8,
				font = tweak_data.menu.pd2_small_font,
				color = Color.white:with_alpha(0.4)
			})

			reward_name.set_top(reward_name, upcard.bottom(upcard) + padding*2)

			local x, y, w, h = reward_name.text_rect(reward_name)

			reward_name.set_h(reward_name, h)

			local reward_amount = panel.text(panel, {
				vertical = "center",
				h = 32,
				wrap = true,
				align = "center",
				word_wrap = true,
				blend_mode = "add",
				layer = 11,
				name = "reward" .. tostring(i),
				text = managers.experience:cash_string(amount, data.cash_string or ""),
				w = panel.w(panel),
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_small_font,
				color = Color.white
			})

			reward_amount.set_top(reward_amount, reward_name.bottom(reward_name))

			local x, y, w, h = reward_amount.text_rect(reward_amount)

			reward_amount.set_h(reward_amount, h)

			count = count + 1
		end
	end

	local warning_title, warning_text = nil

	if managers.crime_spree:server_spree_level() < managers.crime_spree:spree_level() then
		warning_title = "menu_cs_rewards_suspended"
		warning_text = "menu_cs_rewards_suspended_desc"
	elseif not managers.crime_spree:in_progress() then
		warning_title = "menu_cs_rewards_not_in_progress"
		warning_text = "menu_cs_rewards_not_in_progress_desc"
	elseif managers.crime_spree:has_failed() then
		warning_title = "menu_cs_rewards_has_failed"
		warning_text = "menu_cs_rewards_has_failed_desc"
	end

	if warning_title then
		local level_layer = 50
		local level_panel = self.panel(self):panel({
			layer = level_layer
		})

		level_panel.bitmap(level_panel, {
			texture = "guis/textures/pd2/cs_warning_background",
			name = "background",
			h = 128,
			layer = 10,
			color = Color.white,
			w = level_panel.w(level_panel)
		})

		local suspend_text = level_panel.text(level_panel, {
			word_wrap = true,
			vertical = "left",
			wrap = true,
			align = "left",
			layer = 20,
			text = (warning_title and managers.localization:to_upper_text(warning_title)) or "",
			font_size = tweak_data.menu.pd2_medium_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = Color.white
		})

		self.make_fine_text(self, suspend_text)
		suspend_text.set_top(suspend_text, padding*2)
		suspend_text.set_left(suspend_text, padding*4)

		local w_multi = 0.75
		local suspend_desc_text = level_panel.text(level_panel, {
			vertical = "top",
			word_wrap = true,
			wrap = true,
			align = "left",
			layer = 20,
			text = (warning_text and managers.localization:text(warning_text)) or "",
			x = self.panel(self):w()*(w_multi - 1)*0.5,
			w = self.panel(self):w()*w_multi,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font,
			color = Color.white
		})

		self.make_fine_text(self, suspend_desc_text)
		suspend_desc_text.set_top(suspend_desc_text, suspend_text.bottom(suspend_text))
		suspend_desc_text.set_left(suspend_desc_text, padding*4)
		level_panel.rect(level_panel, {
			alpha = 0.75,
			color = Color.black
		})
		outline_panel.set_layer(outline_panel, level_layer + 10)
	end

	return 
end
CrimeSpreeRewardsDetailsPage.make_fine_text = function (self, text)
	local x, y, w, h = text.text_rect(text)

	text.set_size(text, w, h)
	text.set_position(text, math.round(text.x(text)), math.round(text.y(text)))

	return x, y, w, h
end
CrimeSpreeRewardsDetailsPage.create_card = function (self, panel, icon, layer, rotation_amt, wiggle_amt, outline)
	local rotation = math.rand(-rotation_amt, rotation_amt)
	local wiggle_x = math.rand(-wiggle_amt, wiggle_amt)
	local wiggle_y = math.rand(-wiggle_amt, wiggle_amt)
	local scale = 0.35
	local upcard = self._create_card(self, panel, icon, scale, layer, rotation, wiggle_x, wiggle_y)

	if outline then
		local outline_card = nil
		local color = Color.black:with_alpha(0.4)
		outline_card = self._create_card(self, panel, icon, scale, layer - 1, rotation, wiggle_x + 1, wiggle_y + 1)

		outline_card.set_color(outline_card, color)

		outline_card = self._create_card(self, panel, icon, scale, layer - 1, rotation, wiggle_x - 1, wiggle_y - 1)

		outline_card.set_color(outline_card, color)
	end

	return upcard
end
CrimeSpreeRewardsDetailsPage._create_card = function (self, panel, icon, scale, layer, rotation, wiggle_x, wiggle_y)
	local texture, rect, coords = tweak_data.hud_icons:get_icon_data(icon or "downcard_overkill_deck")
	local upcard = panel.bitmap(panel, {
		name = "upcard",
		halign = "scale",
		valign = "scale",
		texture = texture,
		w = math.round(panel.h(panel)*0.7111111111111111*scale),
		h = panel.h(panel)*scale,
		layer = layer or 20
	})

	upcard.set_center_x(upcard, panel.w(panel)*0.5 + wiggle_x)
	upcard.set_center_y(upcard, panel.h(panel)*0.3 + wiggle_y)
	upcard.set_rotation(upcard, rotation)

	if coords then
		local tl = Vector3(coords[1][1], coords[1][2], 0)
		local tr = Vector3(coords[2][1], coords[2][2], 0)
		local bl = Vector3(coords[3][1], coords[3][2], 0)
		local br = Vector3(coords[4][1], coords[4][2], 0)

		upcard.set_texture_coordinates(upcard, tl, tr, bl, br)
	else
		upcard.set_texture_rect(upcard, unpack(rect))
	end

	return upcard
end

return 
