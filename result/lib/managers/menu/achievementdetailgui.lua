local massive_font = tweak_data.menu.pd2_massive_font
local large_font = tweak_data.menu.pd2_large_font
local medium_font = tweak_data.menu.pd2_medium_font
local small_font = tweak_data.menu.pd2_small_font
local tiny_font = tweak_data.menu.pd2_tiny_font
local massive_font_size = tweak_data.menu.pd2_massive_font_size
local large_font_size = tweak_data.menu.pd2_large_font_size
local medium_font_size = tweak_data.menu.pd2_medium_font_size
local small_font_size = tweak_data.menu.pd2_small_font_size
local tiny_font_size = tweak_data.menu.pd2_tiny_font_size
HalfCircleProgressBar = HalfCircleProgressBar or class(ExtendedPanel)
HalfCircleProgressBar.init = function (self, parent, config, progress)
	HalfCircleProgressBar.super.init(self, parent, config)

	local half_w = math.floor(self.w(self)/2)

	if not config.color then
		slot5 = Color(config.alpha or 1, 1, 1, 1)
	end

	self._color = slot5

	if not config.back_color then
		slot5 = self._color:with_alpha(config.back_alpha or self._color.alpha*0.5)
	end

	self._back_color = slot5
	self._back = self.fit_bitmap(self, {
		blend_mode = "add",
		texture = config.texture,
		color = self._back_color
	})
	self._right = self.fit_bitmap(self, {
		render_template = "VertexColorTexturedRadial",
		blend_mode = "add",
		texture = config.texture,
		color = self._color
	})
	self._left = self.fit_bitmap(self, {
		render_template = "VertexColorTexturedRadial",
		blend_mode = "add",
		texture = config.texture,
		color = self._color
	})

	self._right:set_w(half_w)
	self._left:set_w(half_w)
	self._right:set_left(self._left:right())

	local tw = self._left:texture_width()
	local th = self._left:texture_height()
	local half_tw = math.floor(tw/2)

	self._left:set_texture_rect(0, th, half_tw, -th)
	self._right:set_texture_rect(half_tw, 0, half_tw, th)

	if progress then
		self.set_progress(self, progress)
	end

	return 
end
HalfCircleProgressBar.set_progress = function (self, value)
	self._right:set_color(self._color:with_red(value*0.5))
	self._left:set_color(self._color:with_red(value*0.5 + 0.5))

	return 
end

local function create_tag_text(str)
	return "menu_achievements_" .. str
end

local function get_tag_category(tag)
	return string.split(tag, "_", nil, 1)[1]
end

local difficulty_translate = {
	difficulty_death_wish = "menu_difficulty_apocalypse",
	difficulty_one_down = "menu_difficulty_sm_wish",
	difficulty_normal = "menu_difficulty_normal",
	difficulty_hard = "menu_difficulty_hard",
	difficulty_very_hard = "menu_difficulty_very_hard",
	difficulty_overkill = "menu_difficulty_overkill",
	difficulty_mayhem = "menu_difficulty_easy_wish"
}

local function create_difficulty_text(str)
	return difficulty_translate[str]
end

local function create_contract_text(str)
	local id = string.sub(str, 11)
	local data = tweak_data.narrative.contacts[id]

	if data then
		return data.name_id
	end

	return create_tag_text(str)
end

AchievementDetailGui = AchievementDetailGui or class(GrowPanel)
AchievementDetailGui.init = function (self, parent, achievement_data_or_id, back_callback)
	AchievementDetailGui.super.init(self, parent, {
		padding = 10,
		layer = 50,
		border = 10,
		fixed_w = 650,
		input = true
	})

	if type(achievement_data_or_id) == "table" then
		local data = achievement_data_or_id
		self._id = data.id
		self._info = data.info
		self._visual = data.visual
	else
		self._id = achievement_data_or_id
	end

	print(self._id)

	if not managers.menu:is_pc_controller() then
		self._legends = TextLegendsBar:new(parent, nil, {
			layer = self.layer(self)
		})

		self._legends:add_items({
			"menu_legend_back",
			"menu_legend_scroll_left_right"
		})
		self._legends:set_rightbottom(parent.w(parent), parent.h(parent))
	end

	self._back_callback = back_callback
	self._info = self._info or managers.achievment:get_info(self._id) or {}
	self._visual = self._visual or tweak_data.achievement.visual[self._id]
	local grey_color = tweak_data.screen_colors.achievement_grey
	local placer = self.placer(self)

	placer.push_right(placer)

	local texture, texture_rect = tweak_data.hud_icons:get_icon_or(self._visual.icon_id, "guis/dlcs/unfinished/textures/placeholder")
	local bitmap = placer.add_right(placer, self.bitmap(self, {
		w = 85,
		h = 85,
		texture = texture,
		texture_rect = texture_rect
	}))

	if not self._info.awarded then
		bitmap.set_color(bitmap, Color.white:with_alpha(0.1))

		local lock = self.bitmap(self, {
			texture = "guis/dlcs/trk/textures/pd2/lock"
		})
		local cx, cy = bitmap.center(bitmap)

		lock.set_center(lock, math.round((cx + bitmap.w(bitmap)*0.5) - 10), math.round((cy + bitmap.h(bitmap)*0.5) - 10))
	end

	placer.add_right(placer, nil)

	local title = placer.add_bottom(placer, self.fine_text(self, {
		text_id = self._visual.name_id,
		font = medium_font,
		font_size = medium_font_size
	}))
	local unlock_date = self._info.unlock_time

	if unlock_date then
		placer.add_bottom(placer, self.fine_text(self, {
			text = managers.localization:text("menu_achievement_unlock_date", {
				DATE = os.date("%d %b %Y %H:%M", unlock_date)
			}),
			font = small_font,
			font_size = small_font_size,
			color = grey_color
		}), 5)
	elseif self._info.forced then
		placer.add_bottom(placer, self.fine_text(self, {
			text = "TRACKED",
			font = small_font,
			font_size = small_font_size,
			color = grey_color
		}), 5)
	end

	placer.pop(placer)

	local detail = placer.add_bottom(placer, ScrollableList:new(self, {
		w = 380,
		scrollbar_padding = 5,
		h = 300,
		input = true
	}, {
		padding = 10
	}))

	detail.add_lines_and_static_down_indicator(detail)

	local detail_canvas = detail.canvas(detail)
	local detail_placer = detail.canvas(detail):placer()

	add_achievement_detail_text(detail, detail_placer, self._visual, grey_color)

	local tag_str = nil

	for _, tag in pairs(self._visual.tags) do
		local category = get_tag_category(tag)
		local id = nil

		if category == "contracts" then
			id = create_contract_text(tag)
		elseif category == "difficulty" then
			id = create_difficulty_text(tag)
		else
			id = create_tag_text(tag)
		end

		local str = managers.localization:text("menu_achievements_" .. category) .. ": " .. managers.localization:text(id)

		if not tag_str then
			tag_str = managers.localization:text("menu_achievements_tags_intro") .. str
		else
			tag_str = tag_str .. ", " .. str
		end
	end

	if tag_str then
		slot15 = detail_placer.add_row(detail_placer, detail_canvas.fine_text(detail_canvas, {
			wrap = true,
			word_wrap = true,
			text = tag_str,
			font = tiny_font,
			font_size = tiny_font_size,
			color = grey_color,
			w = detail_canvas.row_w(detail_canvas)
		}), nil, 10)
	end

	self._detail = detail
	local friend_list = placer.add_right(placer, ScrollableList:new(self, {
		scrollbar_padding = 5,
		input = true,
		w = self.w(self) - self._detail:right() - 5,
		h = self._detail:h()
	}, {
		padding = 0
	}), 0)

	friend_list.add_lines_and_static_down_indicator(friend_list)

	self._friend_list = friend_list

	placer.set_at(placer, friend_list.left(friend_list) + friend_list.canvas(friend_list):w(), nil)

	if managers.menu:is_pc_controller() then
		placer.set_keep_current(placer, 1)
		placer.add_bottom_ralign(placer, TextButton:new(self, {
			input = true,
			text_id = "menu_back",
			font = medium_font,
			font_size = medium_font_size
		}, function ()
			self._back_callback()

			return 
		end))
	else
		self.grow(self, 0, 10)
	end

	self._num_friend_text = placer.add_top_ralign(placer, self.fine_text(self, {
		keep_w = true,
		align = "right",
		text = managers.localization:text("menu_achievement_friends_unlocked", {
			COUNT = ""
		}),
		font = small_font,
		font_size = small_font_size,
		color = grey_color
	}))
	local canvas = friend_list.canvas(friend_list)
	local f_placer = ResizingPlacer:new(canvas, {
		padding = 4,
		y = 10,
		x = canvas.row_w(canvas) - 10
	})

	self._num_friend_text:set_text(managers.localization:text("menu_achievement_fetching_data"))

	local friend_p = 0
	local global_p = managers.achievment:get_global_achieved_percent(self._id)
	global_p = (0 <= global_p and global_p/100) or 0

	placer.push(placer)

	local f_text = managers.localization:text("menu_achievement_friends_unlocked_percent", {
		COUNT = string.format("%.1f", friend_p*100)
	})
	local g_text = managers.localization:text("menu_achievement_global_unlocked_percent", {
		COUNT = string.format("%.1f", global_p*100)
	})
	local friend_p_text = placer.add_top_ralign(placer, self.fine_text(self, {
		text = f_text,
		font = small_font,
		font_size = small_font_size,
		color = grey_color
	}))

	placer.add_top_ralign(placer, self.fine_text(self, {
		text = g_text,
		font = small_font,
		font_size = small_font_size,
		color = Color(255, 30, 70, 100)/255
	}), 0)
	placer.pop(placer)

	local _, cy = placer.current_center(placer)
	local circle_size = 42
	local friend_circle = placer.add_left(placer, HalfCircleProgressBar:new(self, {
		texture = "guis/dlcs/trk/textures/pd2/circle_inside",
		alpha = 0.6,
		back_alpha = 0.1,
		w = circle_size,
		h = circle_size
	}, friend_p), 15)
	local global_circle = HalfCircleProgressBar:new(self, {
		texture = "guis/dlcs/trk/textures/pd2/circle_outside",
		alpha = 0.8,
		back_alpha = 0.1,
		w = circle_size,
		h = circle_size
	}, global_p)

	friend_circle.set_center_y(friend_circle, cy)
	global_circle.set_lefttop(global_circle, friend_circle.lefttop(friend_circle))

	if friend_circle.left(friend_circle) < title.right(title) then
		title.set_w(title, title.w(title) - title.right(title) + friend_circle.left(friend_circle))
	end

	if managers.network.account:signin_state() == "signed in" then
		local total_friends = Steam:friends()
		local unlocked_friends = {}

		managers.achievment:get_friends_with_achievement(self._id, function (friend)
			print("[Ach]", "GET FRIEND WITH ACHIEVEMENT", friend)

			if not alive(friend_list) or not alive(self._panel) then
				print("[Ach]", "\tQuit!")

				return 
			end

			if friend == true then
				print("[Ach]", "\tDONE!")
				self._num_friend_text:set_text(managers.localization:text("menu_achievement_friends_unlocked", {
					COUNT = #unlocked_friends
				}))

				return 
			elseif friend == false then
				print("[Ach]", "\tFAILED!")
				self._num_friend_text:set_text("ERROR")

				return 
			end

			table.insert(unlocked_friends, friend)

			local friend_p = #unlocked_friends/#total_friends

			friend_circle:set_progress(friend_p)
			friend_p_text:set_text(managers.localization:text("menu_achievement_friends_unlocked_percent", {
				COUNT = string.format("%.1f", friend_p*100)
			}))
			self.make_fine_text(friend_p_text)
			Steam:friend_avatar(Steam.SMALL_AVATAR, friend.id(friend), function (texture)
				if alive(friend_list) then
					local avatar = f_placer:add_left(canvas:fit_bitmap({
						w = 32,
						h = 32,
						texture = texture
					}), 10)
					local name = f_placer:add_left(canvas:fine_text({
						text = friend:name(),
						font = small_font,
						font_size = small_font_size,
						color = grey_color
					}))

					name.set_center_y(name, avatar.center_y(avatar))
					f_placer:new_row()
				end

				return 
			end)

			return 
		end)
	end

	local back_panel = self.panel(self, {
		layer = -1
	})

	back_panel.rect(back_panel, {
		color = Color(255, 15, 18, 24)/255
	})
	BoxGuiObject:new(back_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	self._back = back_panel

	self.set_center(self, parent.w(parent)/2, parent.h(parent)/2)

	return 
end
AchievementDetailGui.close = function (self)
	self.remove_self(self)

	if self._legends then
		self._legends:remove_self()
	end

	return 
end
AchievementDetailGui.update = function (self, ...)
	if not managers.menu:is_pc_controller() and self.allow_input(self) and (not managers.system_menu or not managers.system_menu:is_active() or not not managers.system_menu:is_closing()) then
		local axis_x, axis_y = managers.menu_component:get_right_controller_axis()

		if axis_y ~= 0 and self._friend_list then
			self._friend_list:perform_scroll(axis_y)
		end

		local axis_x, axis_y = managers.menu_component:get_left_controller_axis()

		if axis_y ~= 0 and self._detail then
			self._detail:perform_scroll(axis_y)
		end
	end

	return 
end
AchievementDetailGui.back_pressed = function (self)
	self._back_callback()

	return true
end

return 
