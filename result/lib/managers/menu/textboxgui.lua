TextBoxGui = TextBoxGui or class()
TextBoxGui.PRESETS = {
	system_menu = {
		w = 540,
		h = 270
	},
	weapon_stats = {
		w = 700,
		x = 60,
		h = 270,
		bottom = 620
	},
	help_dialog = {
		w = 540,
		h = 270,
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size
	}
}
TextBoxGui.init = function (self, ...)
	self._target_alpha = {}
	self._visible = true
	self._enabled = true
	self._minimized = false

	self._create_text_box(self, ...)
	self._check_scroll_indicator_states(self)

	self._thread = self._panel:animate(self._update, self)

	return 
end
TextBoxGui._update = function (o, self)
	while true do
		local dt = coroutine.yield()

		if self._up_alpha then
			self._up_alpha.current = math.step(self._up_alpha.current, self._up_alpha.target, dt*5)

			self._text_box:child("scroll_up_indicator_shade"):set_alpha(self._up_alpha.current)
			self._text_box:child("scroll_up_indicator_arrow"):set_color(self._text_box:child("scroll_up_indicator_arrow"):color():with_alpha(self._up_alpha.current))
		end

		if self._down_alpha then
			self._down_alpha.current = math.step(self._down_alpha.current, self._down_alpha.target, dt*5)

			self._text_box:child("scroll_down_indicator_shade"):set_alpha(self._down_alpha.current)
			self._text_box:child("scroll_down_indicator_arrow"):set_color(self._text_box:child("scroll_down_indicator_arrow"):color():with_alpha(self._down_alpha.current))
		end
	end

	return 
end
TextBoxGui.set_layer = function (self, layer)
	self._panel:set_layer(self._init_layer + layer)

	if self._background then
		self._background:set_layer(self._panel:layer() - 1)
	end

	if self._background2 then
		self._background2:set_layer(self._panel:layer() - 1)
	end

	return 
end
TextBoxGui.layer = function (self)
	return self._panel:layer()
end
TextBoxGui.add_background = function (self)
	if alive(self._fullscreen_ws) then
		managers.gui_data:destroy_workspace(self._fullscreen_ws)

		self._fullscreen_ws = nil
	end

	self._fullscreen_ws = managers.gui_data:create_fullscreen_workspace()
	self._background = self._fullscreen_ws:panel():bitmap({
		texture = "guis/textures/test_blur_df",
		name = "bg",
		alpha = 0,
		valign = "grow",
		render_template = "VertexColorTexturedBlur3D",
		layer = 0,
		color = Color.white,
		w = self._fullscreen_ws:panel():w(),
		h = self._fullscreen_ws:panel():h()
	})
	self._background2 = self._fullscreen_ws:panel():rect({
		blend_mode = "normal",
		name = "bg",
		halign = "grow",
		alpha = 0,
		valign = "grow",
		layer = 0,
		color = Color.black
	})

	return 
end
TextBoxGui.set_centered = function (self)
	self._panel:set_center(self._ws:panel():center())

	return 
end
TextBoxGui.recreate_text_box = function (self, ...)
	self.close(self)
	self._create_text_box(self, ...)
	self._check_scroll_indicator_states(self)

	self._thread = self._panel:animate(self._update, self)

	return 
end
TextBoxGui._create_text_box = function (self, ws, title, text, content_data, config)
	self._ws = ws
	self._init_layer = self._ws:panel():layer()

	if alive(self._text_box) then
		ws.panel(ws):remove(self._text_box)

		self._text_box = nil
	end

	if self._info_box then
		self._info_box:close()

		self._info_box = nil
	end

	self._text_box_focus_button = nil
	local scaled_size = managers.gui_data:scaled_size()
	local type = config and config.type
	local preset = type and self.PRESETS[type]
	local stats_list = content_data and content_data.stats_list
	local stats_text = content_data and content_data.stats_text
	local button_list = content_data and content_data.button_list
	local focus_button = content_data and content_data.focus_button
	local use_indicator = (config and config.use_indicator) or false
	local no_close_legend = config and config.no_close_legend
	local no_scroll_legend = config and config.no_scroll_legend
	self._no_scroll_legend = true
	local only_buttons = config and config.only_buttons
	local use_minimize_legend = (config and config.use_minimize_legend) or false
	local w = (preset and preset.w) or (config and config.w) or scaled_size.width/2.25
	local h = (preset and preset.h) or (config and config.h) or scaled_size.height/2
	local x = (preset and preset.x) or (config and config.x) or 0
	local y = (preset and preset.y) or (config and config.y) or 0
	local bottom = (preset and preset.bottom) or (config and config.bottom)
	local forced_h = (preset and preset.forced_h) or (config and config.forced_h) or false
	local title_font = (preset and preset.title_font) or (config and config.title_font) or tweak_data.menu.pd2_large_font
	local title_font_size = (preset and preset.title_font_size) or (config and config.title_font_size) or 28
	local font = (preset and preset.font) or (config and config.font) or tweak_data.menu.pd2_medium_font
	local font_size = (preset and preset.font_size) or (config and config.font_size) or tweak_data.menu.pd2_medium_font_size
	local use_text_formating = (preset and preset.use_text_formating) or (config and config.use_text_formating) or false
	local text_formating_color = (preset and preset.text_formating_color) or (config and config.text_formating_color) or Color.white
	local text_formating_color_table = (preset and preset.text_formating_color_table) or (config and config.text_formating_color_table) or nil
	local is_title_outside = (preset and preset.is_title_outside) or (config and config.is_title_outside) or false
	local text_blend_mode = (preset and preset.text_blend_mode) or (config and config.text_blend_mode) or "normal"
	self._allow_moving = (config and config.allow_moving) or false
	local preset_or_config_y = y ~= 0
	title = title and utf8.to_upper(title)
	text = text and text
	local main = ws.panel(ws):panel({
		valign = "center",
		visible = self._visible,
		x = x,
		y = y,
		w = w,
		h = h,
		layer = self._init_layer
	})
	self._panel = main
	self._panel_h = self._panel:h()
	self._panel_w = self._panel:w()
	local title_text = main.text(main, {
		word_wrap = false,
		name = "title",
		halign = "left",
		wrap = false,
		align = "left",
		vertical = "top",
		valign = "top",
		y = 10,
		rotation = 360,
		x = 10,
		layer = 1,
		text = title or "none",
		visible = (title and true) or false,
		font = title_font,
		font_size = title_font_size
	})
	local _, _, tw, th = title_text.text_rect(title_text)

	title_text.set_size(title_text, tw, th)

	th = th + 10

	if is_title_outside then
		th = 0
	end

	self._indicator = main.bitmap(main, {
		texture = "guis/textures/icon_loading",
		name = "indicator",
		layer = 1,
		visible = use_indicator
	})

	self._indicator:set_right(main.w(main))

	local top_line = main.bitmap(main, {
		texture = "guis/textures/headershadow",
		name = "top_line",
		y = 0,
		layer = 0,
		color = Color.white,
		w = main.w(main)
	})

	top_line.set_bottom(top_line, th)

	local bottom_line = main.bitmap(main, {
		texture = "guis/textures/headershadow",
		name = "bottom_line",
		y = 100,
		rotation = 180,
		layer = 0,
		color = Color.white,
		w = main.w(main)
	})

	bottom_line.set_top(bottom_line, main.h(main) - th)
	top_line.hide(top_line)
	bottom_line.hide(bottom_line)

	local lower_static_panel = main.panel(main, {
		name = "lower_static_panel",
		h = 0,
		y = 0,
		x = 0,
		layer = 0,
		w = main.w(main)
	})

	self._create_lower_static_panel(self, lower_static_panel)

	local info_area = main.panel(main, {
		name = "info_area",
		y = 0,
		x = 0,
		layer = 0,
		w = main.w(main),
		h = main.h(main) - th*2
	})
	local info_bg = info_area.rect(info_area, {
		name = "info_bg",
		valign = "grow",
		halign = "grow",
		alpha = 0.6,
		layer = 0,
		color = Color(0, 0, 0)
	})
	local buttons_panel = self._setup_buttons_panel(self, info_area, button_list, focus_button, only_buttons)
	local scroll_panel = info_area.panel(info_area, {
		name = "scroll_panel",
		x = 10,
		layer = 1,
		y = math.round(th + 5),
		w = info_area.w(info_area) - 20,
		h = info_area.h(info_area)
	})
	local has_stats = stats_list and 0 < #stats_list
	local stats_panel = self._setup_stats_panel(self, scroll_panel, stats_list, stats_text)
	local text = scroll_panel.text(scroll_panel, {
		word_wrap = true,
		name = "text",
		wrap = true,
		align = "left",
		halign = "left",
		vertical = "top",
		valign = "top",
		layer = 1,
		text = text or "none",
		visible = (text and true) or false,
		w = scroll_panel.w(scroll_panel) - math.round(stats_panel.w(stats_panel)) - ((has_stats and 20) or 0),
		x = math.round(stats_panel.w(stats_panel)) + ((has_stats and 20) or 0),
		font = font,
		font_size = font_size,
		blend_mode = text_blend_mode
	})

	if use_text_formating then
		local text_string = text.text(text)
		local text_dissected = utf8.characters(text_string)
		local idsp = Idstring("#")
		local start_ci = {}
		local end_ci = {}
		local first_ci = true

		for i, c in ipairs(text_dissected) do
			if Idstring(c) == idsp then
				local next_c = text_dissected[i + 1]

				if next_c and Idstring(next_c) == idsp then
					if first_ci then
						table.insert(start_ci, i)
					else
						table.insert(end_ci, i)
					end

					first_ci = not first_ci
				end
			end
		end

		if #start_ci == #end_ci or false then
			for i = 1, #start_ci, 1 do
				start_ci[i] = start_ci[i] - ((i - 1)*4 + 1)
				end_ci[i] = end_ci[i] - i*4 - 1
			end
		end

		text_string = string.gsub(text_string, "##", "")

		text.set_text(text, text_string)
		text.clear_range_color(text, 1, utf8.len(text_string))

		if #start_ci ~= #end_ci then
			Application:error("TextBoxGui: Not even amount of ##'s in skill description string!", #start_ci, #end_ci)
		else
			for i = 1, #start_ci, 1 do
				text.set_range_color(text, start_ci[i], end_ci[i], (text_formating_color_table and text_formating_color_table[i]) or text_formating_color)
			end
		end
	end

	local scroll_up_indicator_shade = main.panel(main, {
		halign = "right",
		name = "scroll_up_indicator_shade",
		valign = "top",
		y = 100,
		layer = 5,
		x = scroll_panel.x(scroll_panel),
		w = main.w(main) - buttons_panel.w(buttons_panel)
	})
	local scroll_down_indicator_shade = main.panel(main, {
		halign = "right",
		name = "scroll_down_indicator_shade",
		valign = "bottom",
		y = 100,
		layer = 5,
		x = scroll_panel.x(scroll_panel),
		w = main.w(main) - buttons_panel.w(buttons_panel)
	})
	local texture, rect = tweak_data.hud_icons:get_icon_data("scrollbar_arrow")

	scroll_panel.grow(scroll_panel, -rect[3], 0)
	scroll_up_indicator_shade.set_w(scroll_up_indicator_shade, scroll_panel.w(scroll_panel))
	scroll_down_indicator_shade.set_w(scroll_down_indicator_shade, scroll_panel.w(scroll_panel))
	text.set_w(text, scroll_panel.w(scroll_panel))

	local _, _, ttw, tth = text.text_rect(text)

	text.set_h(text, tth)
	scroll_panel.set_h(scroll_panel, forced_h or math.min(h - th, tth))

	if self._override_info_area_size then
		self._override_info_area_size(self, info_area, scroll_panel, buttons_panel)
	else
		info_area.set_h(info_area, scroll_panel.bottom(scroll_panel) + buttons_panel.h(buttons_panel) + 10 + 5)
	end

	buttons_panel.set_bottom(buttons_panel, info_area.h(info_area) - 10)

	if not preset_or_config_y then
		main.set_h(main, info_area.h(info_area))

		if content_data.clamp_to_screen then
			main.set_h(main, main.parent(main):h()*0.9)
			main.set_center_y(main, main.parent(main):h()/2)
		end

		if bottom then
			main.set_bottom(main, bottom)
		elseif y == 0 then
			main.set_center_y(main, main.parent(main):h()/2)
		end
	end

	top_line.set_world_bottom(top_line, scroll_panel.world_top(scroll_panel))
	bottom_line.set_world_top(bottom_line, scroll_panel.world_bottom(scroll_panel))
	scroll_up_indicator_shade.set_top(scroll_up_indicator_shade, top_line.bottom(top_line))
	scroll_down_indicator_shade.set_bottom(scroll_down_indicator_shade, bottom_line.top(bottom_line))

	local scroll_up_indicator_arrow = main.bitmap(main, {
		name = "scroll_up_indicator_arrow",
		layer = 3,
		halign = "right",
		valign = "top",
		texture = texture,
		texture_rect = rect,
		color = Color.white
	})

	scroll_up_indicator_arrow.set_lefttop(scroll_up_indicator_arrow, scroll_panel.right(scroll_panel) + 2, scroll_up_indicator_shade.top(scroll_up_indicator_shade) + 8)

	local scroll_down_indicator_arrow = main.bitmap(main, {
		name = "scroll_down_indicator_arrow",
		layer = 3,
		halign = "right",
		valign = "bottom",
		rotation = 180,
		texture = texture,
		texture_rect = rect,
		color = Color.white
	})

	scroll_down_indicator_arrow.set_leftbottom(scroll_down_indicator_arrow, scroll_panel.right(scroll_panel) + 2, scroll_down_indicator_shade.bottom(scroll_down_indicator_shade) - 8)
	BoxGuiObject:new(scroll_up_indicator_shade, {
		sides = {
			0,
			0,
			2,
			0
		}
	}):set_aligns("scale", "scale")
	BoxGuiObject:new(scroll_down_indicator_shade, {
		sides = {
			0,
			0,
			0,
			2
		}
	}):set_aligns("scale", "scale")
	lower_static_panel.set_bottom(lower_static_panel, main.h(main) - h*2)

	self._info_box = BoxGuiObject:new(info_area, {
		sides = {
			1,
			1,
			1,
			1
		}
	})
	local bar_h = scroll_down_indicator_arrow.top(scroll_down_indicator_arrow) - scroll_up_indicator_arrow.bottom(scroll_up_indicator_arrow)
	local scroll_bar = main.panel(main, {
		name = "scroll_bar",
		halign = "right",
		w = 4,
		layer = 4,
		h = bar_h
	})
	self._scroll_bar_box_class = BoxGuiObject:new(scroll_bar, {
		sides = {
			2,
			2,
			0,
			0
		}
	})

	self._scroll_bar_box_class:set_aligns("scale", "scale")
	scroll_bar.set_w(scroll_bar, 8)
	scroll_bar.set_bottom(scroll_bar, scroll_down_indicator_arrow.top(scroll_down_indicator_arrow))
	scroll_bar.set_center_x(scroll_bar, scroll_down_indicator_arrow.center_x(scroll_down_indicator_arrow))

	local legend_minimize = main.text(main, {
		text = "MINIMIZE",
		name = "legend_minimize",
		halign = "left",
		valign = "top",
		layer = 1,
		visible = use_minimize_legend,
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size
	})
	local _, _, lw, lh = legend_minimize.text_rect(legend_minimize)

	legend_minimize.set_size(legend_minimize, lw, lh)
	legend_minimize.set_bottom(legend_minimize, top_line.bottom(top_line) - 4)
	legend_minimize.set_right(legend_minimize, top_line.right(top_line))

	local legend_close = main.text(main, {
		text = "CLOSE",
		name = "legend_close",
		halign = "left",
		valign = "top",
		layer = 1,
		visible = not no_close_legend,
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size
	})
	local _, _, lw, lh = legend_close.text_rect(legend_close)

	legend_close.set_size(legend_close, lw, lh)
	legend_close.set_top(legend_close, bottom_line.top(bottom_line) + 4)

	local legend_scroll = main.text(main, {
		text = "SCROLL WITH",
		name = "legend_scroll",
		halign = "right",
		valign = "top",
		layer = 1,
		visible = not no_scroll_legend,
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size
	})
	local _, _, lw, lh = legend_scroll.text_rect(legend_scroll)

	legend_scroll.set_size(legend_scroll, lw, lh)
	legend_scroll.set_righttop(legend_scroll, scroll_panel.right(scroll_panel), bottom_line.top(bottom_line) + 4)

	self._scroll_panel = scroll_panel
	self._text_box = main

	self._set_scroll_indicator(self)

	if is_title_outside then
		title_text.set_bottom(title_text, 0)
		title_text.set_rotation(title_text, 360)

		self._is_title_outside = is_title_outside
	end

	return main
end
TextBoxGui._setup_stats_panel = function (self, scroll_panel, stats_list, stats_text)
	local has_stats = stats_list and 0 < #stats_list
	local stats_panel = scroll_panel.panel(scroll_panel, {
		name = "stats_panel",
		x = 10,
		layer = 1,
		w = (has_stats and scroll_panel.w(scroll_panel)/3) or 0,
		h = scroll_panel.h(scroll_panel)
	})
	local total_h = 0

	if has_stats then
		for _, stats in ipairs(stats_list) do
			if stats.type == "bar" then
				local panel = stats_panel.panel(stats_panel, {
					layer = -1,
					h = 20,
					w = stats_panel.w(stats_panel),
					y = total_h
				})
				local bg = panel.rect(panel, {
					layer = -1,
					color = Color.black:with_alpha(0.5)
				})
				local w = (bg.w(bg) - 4)*stats.current/stats.total
				local progress_bar = panel.rect(panel, {
					y = 1,
					x = 1,
					layer = 0,
					w = w,
					h = bg.h(bg) - 2,
					color = tweak_data.hud.prime_color:with_alpha(0.5)
				})
				local text = panel.text(panel, {
					vertical = "center",
					halign = "left",
					align = "left",
					valign = "center",
					blend_mode = "normal",
					kern = 0,
					y = -1,
					x = 4,
					layer = 1,
					text = stats.text,
					w = panel.w(panel),
					h = panel.h(panel),
					font = tweak_data.menu.pd2_small_font,
					font_size = tweak_data.menu.pd2_small_font_size
				})
				total_h = total_h + panel.h(panel)
			elseif stats.type == "condition" then
				local panel = stats_panel.panel(stats_panel, {
					h = 22,
					w = stats_panel.w(stats_panel),
					y = total_h
				})
				local texture, rect = tweak_data.hud_icons:get_icon_data("icon_repair")
				local icon = panel.bitmap(panel, {
					name = "icon",
					layer = 0,
					texture = texture,
					texture_rect = rect,
					color = Color.white
				})

				icon.set_center_y(icon, panel.h(panel)/2)

				local text = panel.text(panel, {
					vertical = "center",
					valign = "center",
					align = "left",
					blend_mode = "normal",
					kern = 0,
					halign = "left",
					layer = 0,
					text = "CONDITION: " .. stats.value .. "%",
					w = panel.w(panel),
					h = panel.h(panel),
					x = icon.right(icon),
					font = tweak_data.menu.pd2_small_font,
					font_size = tweak_data.menu.pd2_small_font_size
				})

				text.set_center_y(text, panel.h(panel)/2)

				total_h = total_h + panel.h(panel)
			elseif stats.type == "empty" then
				local panel = stats_panel.panel(stats_panel, {
					w = stats_panel.w(stats_panel),
					h = stats.h,
					y = total_h
				})
				total_h = total_h + panel.h(panel)
			elseif stats.type == "mods" then
				local panel = stats_panel.panel(stats_panel, {
					h = 22,
					w = stats_panel.w(stats_panel),
					y = total_h
				})
				local texture, rect = tweak_data.hud_icons:get_icon_data("icon_addon")
				local icon = panel.bitmap(panel, {
					name = "icon",
					layer = 0,
					texture = texture,
					texture_rect = rect,
					color = Color.white
				})

				icon.set_center_y(icon, panel.h(panel)/2)

				local text = panel.text(panel, {
					vertical = "center",
					valign = "center",
					align = "left",
					blend_mode = "normal",
					kern = 0,
					text = "ACTIVE MODS:",
					halign = "left",
					layer = 0,
					w = panel.w(panel),
					h = panel.h(panel),
					x = icon.right(icon),
					font = tweak_data.menu.pd2_small_font,
					font_size = tweak_data.menu.pd2_small_font_size
				})

				text.set_center_y(text, panel.h(panel)/2)

				local s = ""

				for _, mod in ipairs(stats.list) do
					s = s .. mod .. "\n"
				end

				local mods_text = panel.text(panel, {
					vertical = "top",
					halign = "left",
					align = "left",
					valign = "top",
					blend_mode = "normal",
					kern = 0,
					layer = 0,
					text = s,
					w = panel.w(panel),
					h = panel.h(panel),
					y = text.bottom(text),
					x = icon.right(icon),
					font = tweak_data.menu.pd2_small_font,
					font_size = tweak_data.menu.pd2_small_font_size
				})
				local _, _, w, h = mods_text.text_rect(mods_text)

				mods_text.set_h(mods_text, h)
				panel.set_h(panel, text.h(text) + mods_text.h(mods_text))

				total_h = total_h + panel.h(panel)
			end
		end

		local stats_text = stats_panel.text(stats_panel, {
			vertical = "top",
			halign = "left",
			wrap = true,
			align = "left",
			word_wrap = true,
			valign = "top",
			blend_mode = "normal",
			kern = 0,
			layer = 0,
			text = stats_text or "Nunc vel diam vel neque sodales gravida et ac quam. Phasellus egestas, arcu in tristique mattis, velit nisi tincidunt lorem, bibendum molestie nunc purus id turpis. Donec sagittis nibh in eros ultrices aliquam. Vestibulum ante mauris, mattis quis commodo a, dictum eget sapien. Maecenas eu diam lorem. Nunc dolor metus, varius sit amet rhoncus vel, iaculis sed massa. Morbi tempus mi quis dolor posuere eu commodo magna eleifend. Pellentesque sit amet mattis nunc. Nunc lectus quam, pretium sit amet consequat sed, vestibulum vitae lorem. Sed bibendum egestas turpis, sit amet viverra risus viverra in. Suspendisse aliquam dapibus urna, posuere fermentum tellus vulputate vitae.",
			w = stats_panel.w(stats_panel),
			y = total_h,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size
		})
		local _, _, w, h = stats_text.text_rect(stats_text)

		stats_text.set_h(stats_text, h)

		total_h = total_h + h
	end

	stats_panel.set_h(stats_panel, total_h)

	return stats_panel
end
TextBoxGui._setup_buttons_panel = function (self, info_area, button_list, focus_button, only_buttons)
	local has_buttons = button_list and 0 < #button_list
	local buttons_panel = info_area.panel(info_area, {
		name = "buttons_panel",
		x = 10,
		layer = 1,
		w = (has_buttons and 200) or 0,
		h = info_area.h(info_area)
	})

	buttons_panel.set_right(buttons_panel, info_area.w(info_area))

	self._text_box_buttons_panel = buttons_panel

	if has_buttons then
		local button_text_config = {
			name = "button_text",
			vertical = "center",
			word_wrap = "true",
			wrap = "true",
			blend_mode = "add",
			halign = "right",
			x = 10,
			layer = 2,
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size,
			color = tweak_data.screen_colors.button_stage_3
		}
		local max_w = 0
		local max_h = 0

		if button_list then
			for i, button in ipairs(button_list) do
				local button_panel = buttons_panel.panel(buttons_panel, {
					halign = "grow",
					h = 20,
					y = 100,
					name = button.id_name
				})
				button_text_config.text = utf8.to_upper(button.text or "")
				local text = button_panel.text(button_panel, button_text_config)
				local _, _, w, h = text.text_rect(text)
				max_w = math.max(max_w, w)
				max_h = math.max(max_h, h)

				text.set_size(text, w, h)
				button_panel.set_h(button_panel, h)
				text.set_right(text, button_panel.w(button_panel))
				button_panel.set_bottom(button_panel, i*h)
			end

			buttons_panel.set_h(buttons_panel, #button_list*max_h)
			buttons_panel.set_bottom(buttons_panel, info_area.h(info_area) - 10)
		end

		buttons_panel.set_w(buttons_panel, (only_buttons and info_area.w(info_area)) or math.max(max_w, 120) + 40)
		buttons_panel.set_right(buttons_panel, info_area.w(info_area) - 10)

		local selected = buttons_panel.rect(buttons_panel, {
			blend_mode = "add",
			name = "selected",
			alpha = 0.3,
			color = tweak_data.screen_colors.button_stage_3
		})

		self.set_focus_button(self, focus_button)
	end

	return buttons_panel
end
TextBoxGui._create_lower_static_panel = function (self)
	return 
end
TextBoxGui.check_focus_button = function (self, x, y)
	for i, panel in ipairs(self._text_box_buttons_panel:children()) do
		if panel.child and panel.inside(panel, x, y) then
			self.set_focus_button(self, i)

			return true
		end
	end

	return false
end
TextBoxGui.set_focus_button = function (self, focus_button)
	if focus_button ~= self._text_box_focus_button then
		managers.menu:post_event("highlight")

		if self._text_box_focus_button then
			self._set_button_selected(self, self._text_box_focus_button, false)
		end

		self._set_button_selected(self, focus_button, true)

		self._text_box_focus_button = focus_button
	end

	return 
end
TextBoxGui._set_button_selected = function (self, index, is_selected)
	local button_panel = self._text_box_buttons_panel:child(index - 1)

	if button_panel then
		local button_text = button_panel.child(button_panel, "button_text")
		local selected = self._text_box_buttons_panel:child("selected")

		if is_selected then
			button_text.set_color(button_text, tweak_data.screen_colors.button_stage_2)
			selected.set_shape(selected, button_panel.shape(button_panel))
			selected.move(selected, 2, -1)
		else
			button_text.set_color(button_text, tweak_data.screen_colors.button_stage_3)
		end
	end

	return 
end
TextBoxGui.change_focus_button = function (self, change)
	local button_count = self._text_box_buttons_panel:num_children() - 1
	local focus_button = (self._text_box_focus_button + change)%button_count

	if focus_button == 0 then
		focus_button = button_count
	end

	self.set_focus_button(self, focus_button)

	return 
end
TextBoxGui.get_focus_button = function (self)
	return self._text_box_focus_button
end
TextBoxGui.get_focus_button_id = function (self)
	return self._text_box_buttons_panel:child(self._text_box_focus_button - 1):name()
end
TextBoxGui._set_scroll_indicator = function (self)
	local info_area = self._text_box:child("info_area")
	local scroll_panel = info_area.child(info_area, "scroll_panel")
	local scroll_text = scroll_panel.child(scroll_panel, "text")
	local scroll_bar = self._text_box:child("scroll_bar")
	local legend_scroll = self._text_box:child("legend_scroll")
	local bar_h = self._text_box:child("scroll_down_indicator_arrow"):top() - self._text_box:child("scroll_up_indicator_arrow"):bottom()
	local is_visible = scroll_panel.h(scroll_panel) < scroll_text.h(scroll_text)

	if scroll_text.h(scroll_text) ~= 0 then
		scroll_bar.set_h(scroll_bar, (bar_h*scroll_panel.h(scroll_panel))/scroll_text.h(scroll_text))
	end

	scroll_bar.set_visible(scroll_bar, is_visible)
	legend_scroll.set_visible(legend_scroll, not self._no_scroll_legend and is_visible)
	self._text_box:child("scroll_up_indicator_shade"):set_visible(is_visible)
	self._text_box:child("scroll_up_indicator_arrow"):set_visible(is_visible)
	self._text_box:child("scroll_down_indicator_shade"):set_visible(is_visible)
	self._text_box:child("scroll_down_indicator_arrow"):set_visible(is_visible)

	return 
end
TextBoxGui._check_scroll_indicator_states = function (self)
	local info_area = self._text_box:child("info_area")
	local scroll_panel = info_area.child(info_area, "scroll_panel")
	local scroll_text = scroll_panel.child(scroll_panel, "text")

	if not self._up_alpha then
		self._up_alpha = {
			current = 0
		}

		self._text_box:child("scroll_up_indicator_shade"):set_alpha(self._up_alpha.current)
		self._text_box:child("scroll_up_indicator_arrow"):set_color(self._text_box:child("scroll_up_indicator_arrow"):color():with_alpha(self._up_alpha.current))
	end

	if not self._down_alpha then
		self._down_alpha = {
			current = 1
		}

		self._text_box:child("scroll_down_indicator_shade"):set_alpha(self._down_alpha.current)
		self._text_box:child("scroll_down_indicator_arrow"):set_color(self._text_box:child("scroll_down_indicator_arrow"):color():with_alpha(self._down_alpha.current))
	end

	self._up_alpha.target = (scroll_text.top(scroll_text) < 0 and 1) or 0
	self._down_alpha.target = (scroll_panel.h(scroll_panel) < scroll_text.bottom(scroll_text) and 1) or 0
	local up_arrow = self._text_box:child("scroll_up_indicator_arrow")
	local scroll_bar = self._text_box:child("scroll_bar")
	local sh = (scroll_text.h(scroll_text) ~= 0 and scroll_text.h(scroll_text)) or 1

	scroll_bar.set_top(scroll_bar, up_arrow.bottom(up_arrow) - (scroll_text.top(scroll_text)*(scroll_panel.h(scroll_panel) - up_arrow.h(up_arrow)*2 - 16))/sh)

	return 
end
TextBoxGui.input_focus = function (self)
	return false
end
TextBoxGui.mouse_pressed = function (self, button, x, y)
	return false
end
TextBoxGui.check_close = function (self, x, y)
	if not self.can_take_input(self) then
		return false
	end

	local legend_close = self._panel:child("legend_close")

	if not legend_close.visible(legend_close) then
		return false
	end

	if legend_close.inside(legend_close, x, y) then
		return true
	end

	return false
end
TextBoxGui.check_minimize = function (self, x, y)
	if not self.can_take_input(self) then
		return false
	end

	local legend_minimize = self._panel:child("legend_minimize")

	if not legend_minimize.visible(legend_minimize) then
		return false
	end

	if legend_minimize.inside(legend_minimize, x, y) then
		return true
	end

	return false
end
TextBoxGui.check_grab_scroll_bar = function (self, x, y)
	if not self.can_take_input(self) or not alive(self._text_box) or not self._text_box:child("scroll_bar"):visible() then
		return false
	end

	if self._allow_moving and self._text_box:inside(x, y) and self._text_box:child("top_line"):inside(x, y) then
		self._grabbed_title = true
		self._grabbed_offset_x = self.x(self) - x
		self._grabbed_offset_y = self.y(self) - y

		return true
	end

	if self._text_box:child("scroll_bar") and self._text_box:child("scroll_bar"):inside(x, y) then
		self._grabbed_scroll_bar = true
		self._current_y = y

		return true
	end

	if self._text_box:child("scroll_up_indicator_arrow"):inside(x, y) then
		self._pressing_arrow_up = true

		return true
	end

	if self._text_box:child("scroll_down_indicator_arrow"):inside(x, y) then
		self._pressing_arrow_down = true

		return true
	end

	return false
end
TextBoxGui.release_scroll_bar = function (self)
	self._pressing_arrow_up = false
	self._pressing_arrow_down = false

	if self._grabbed_scroll_bar then
		self._grabbed_scroll_bar = false

		return true
	end

	if self._grabbed_title then
		self._grabbed_title = false

		return true
	end

	return false
end
TextBoxGui.mouse_moved = function (self, x, y)
	if not self.can_take_input(self) or not alive(self._panel) then
		return false, "arrow"
	end

	if self._grabbed_scroll_bar then
		self.scroll_with_bar(self, y, self._current_y)

		self._current_y = y

		return true, "grab"
	elseif self._grabbed_title then
		local _x = x + self._grabbed_offset_x
		local _y = y + self._grabbed_offset_y

		if self._ws:panel():w() < _x + self.w(self) then
			self._grabbed_offset_x = self.x(self) - x
			_x = self._ws:panel():w() - self.w(self)
		elseif _x < self._ws:panel():x() then
			self._grabbed_offset_x = self.x(self) - x
			_x = self._ws:panel():x()
		end

		if self._ws:panel():h() < _y + self.h(self) then
			self._grabbed_offset_y = self.y(self) - y
			_y = self._ws:panel():h() - self.h(self)
		elseif _y < self._ws:panel():y() then
			self._grabbed_offset_y = self.y(self) - y
			_y = self._ws:panel():y()
		end

		self.set_position(self, _x, _y)

		return true, "grab"
	elseif self._text_box:child("scroll_bar") and self._text_box:child("scroll_bar"):visible() and self._text_box:child("scroll_bar"):inside(x, y) then
		return true, "hand"
	elseif self._text_box:child("scroll_up_indicator_arrow"):visible() and self._text_box:child("scroll_up_indicator_arrow"):inside(x, y) then
		if self._pressing_arrow_up then
			self.scroll_up(self)
		end

		return true, "link"
	elseif self._text_box:child("scroll_down_indicator_arrow"):visible() and self._text_box:child("scroll_down_indicator_arrow"):inside(x, y) then
		if self._pressing_arrow_down then
			self.scroll_down(self)
		end

		return true, "link"
	else
		for i, panel in ipairs(self._text_box_buttons_panel:children()) do
			if panel.child and panel.inside(panel, x, y) then
				return true, "link"
			end
		end
	end

	return false, "arrow"
end
TextBoxGui.moved_scroll_bar = function (self, x, y)
	self._x = x
	self._y = y

	return 

	if not self.can_take_input(self) then
		return false, "arrow"
	end

	if self._pressing_arrow_up and self._text_box:child("scroll_up_indicator_arrow"):inside(x, y) then
		self.scroll_up(self)
	end

	if self._pressing_arrow_down and self._text_box:child("scroll_down_indicator_arrow"):inside(x, y) then
		self.scroll_down(self)
	end

	if self._grabbed_scroll_bar then
		self.scroll_with_bar(self, y, self._current_y)

		self._current_y = y

		return true, "grab"
	end

	if self._grabbed_title then
		local _x = x + self._grabbed_offset_x
		local _y = y + self._grabbed_offset_y

		if self._ws:panel():w() < _x + self.w(self) then
			self._grabbed_offset_x = self.x(self) - x
			_x = self._ws:panel():w() - self.w(self)
		elseif _x < self._ws:panel():x() then
			self._grabbed_offset_x = self.x(self) - x
			_x = self._ws:panel():x()
		end

		if self._ws:panel():h() < _y + self.h(self) then
			self._grabbed_offset_y = self.y(self) - y
			_y = self._ws:panel():h() - self.h(self)
		elseif _y < self._ws:panel():y() then
			self._grabbed_offset_y = self.y(self) - y
			_y = self._ws:panel():y()
		end

		self.set_position(self, _x, _y)

		return true, "grab"
	end

	return false, "hand"
end
TextBoxGui.mouse_wheel_up = function (self, x, y)
	if not self._visible then
		return false
	end

	local scroll_panel = self._text_box:child("info_area"):child("scroll_panel")
	local info_area = self._text_box:child("info_area")

	if info_area.inside(info_area, x, y) and scroll_panel.inside(scroll_panel, x, y) then
		self.scroll_up(self, 28)

		return true
	end

	return 
end
TextBoxGui.mouse_wheel_down = function (self, x, y)
	if not self._visible then
		return false
	end

	local scroll_panel = self._text_box:child("info_area"):child("scroll_panel")
	local info_area = self._text_box:child("info_area")

	if info_area.inside(info_area, x, y) and scroll_panel.inside(scroll_panel, x, y) then
		self.scroll_down(self, 28)

		return true
	end

	return 
end
TextBoxGui.scroll_up = function (self, y)
	if not alive(self._text_box) then
		return 
	end

	local info_area = self._text_box:child("info_area")
	local scroll_panel = info_area.child(info_area, "scroll_panel")
	local scroll_text = scroll_panel.child(scroll_panel, "text")

	if scroll_text.top(scroll_text) < 0 then
		if scroll_text.top(scroll_text) < 0 then
			scroll_text.set_y(scroll_text, scroll_text.y(scroll_text) + (y or TimerManager:main():delta_time()*200))
		end

		if 0 < scroll_text.top(scroll_text) then
			scroll_text.set_top(scroll_text, 0)
		end
	end

	self._check_scroll_indicator_states(self)

	return 
end
TextBoxGui.scroll_down = function (self, y)
	if not alive(self._text_box) then
		return 
	end

	local info_area = self._text_box:child("info_area")
	local scroll_panel = info_area.child(info_area, "scroll_panel")
	local scroll_text = scroll_panel.child(scroll_panel, "text")

	if scroll_panel.h(scroll_panel) < scroll_text.bottom(scroll_text) then
		if scroll_panel.h(scroll_panel) < scroll_text.bottom(scroll_text) then
			scroll_text.set_y(scroll_text, scroll_text.y(scroll_text) - (y or TimerManager:main():delta_time()*200))
		end

		if scroll_text.bottom(scroll_text) < scroll_panel.h(scroll_panel) then
			scroll_text.set_bottom(scroll_text, scroll_panel.h(scroll_panel))
		end
	end

	self._check_scroll_indicator_states(self)

	return 
end
TextBoxGui.scroll_with_bar = function (self, target_y, current_y)
	local arrow_size = self._text_box:child("scroll_up_indicator_arrow"):size()
	local info_area = self._text_box:child("info_area")
	local scroll_panel = info_area.child(info_area, "scroll_panel")
	local scroll_text = scroll_panel.child(scroll_panel, "text")

	if target_y < current_y then
		if target_y < scroll_panel.world_bottom(scroll_panel) - arrow_size then
			local mul = (scroll_panel.h(scroll_panel) - arrow_size*2)/scroll_text.h(scroll_text)

			self.scroll_up(self, (current_y - target_y)/mul)
		end

		current_y = target_y
	elseif current_y < target_y then
		if scroll_panel.world_y(scroll_panel) + arrow_size < target_y then
			local mul = (scroll_panel.h(scroll_panel) - arrow_size*2)/scroll_text.h(scroll_text)

			self.scroll_down(self, (target_y - current_y)/mul)
		end

		current_y = target_y
	end

	return 
end
TextBoxGui.update_indicator = function (self, t, dt)
	if alive(self._indicator) then
		self._indicator:rotate(dt*180)
	end

	if self._x and self._y then
		local used, pointer = self.mouse_moved(self, self._x, self._y)

		managers.mouse_pointer:set_pointer_image(pointer)
	end

	return 
end
TextBoxGui.set_fade = function (self, fade)
	self._set_alpha(self, fade)

	if alive(self._background) then
		self._background:set_alpha(fade*0.9)
	end

	if alive(self._background2) then
		self._background2:set_alpha(fade*0.3)
	end

	return 
end
TextBoxGui._set_alpha = function (self, alpha)
	self._panel:set_alpha(alpha)
	self._panel:set_visible(alpha ~= 0)

	return 
end
TextBoxGui._set_alpha_recursive = function (self, obj, alpha)
	if obj.set_color then
		local a = (self._target_alpha[obj.key(obj)] and self._target_alpha[obj.key(obj)]*alpha) or alpha

		obj.set_color(obj, obj.color(obj):with_alpha(a))
	end

	if obj.children then
		for _, child in ipairs(obj.children(obj)) do
			self._set_alpha_recursive(self, child, alpha)
		end
	end

	return 
end
TextBoxGui.set_title = function (self, title)
	local title_text = self._panel:child("title")

	title_text.set_text(title_text, title or "none")

	local _, _, w, h = title_text.text_rect(title_text)

	title_text.set_size(title_text, w, h)
	title_text.set_visible(title_text, (title and true) or false)

	return 
end
TextBoxGui.set_text = function (self, txt, no_upper)
	local text = self._panel:child("info_area"):child("scroll_panel"):child("text")
	slot5 = text
	slot4 = text.set_text

	if not no_upper or not txt then
		slot6 = utf8.to_upper(txt or "")
	end

	slot4(slot5, slot6)

	return 
end
TextBoxGui.set_use_minimize_legend = function (self, use)
	self._panel:child("legend_minimize"):set_visible(use)

	return 
end
TextBoxGui.in_focus = function (self, x, y)
	return self._panel:inside(x, y)
end
TextBoxGui.in_info_area_focus = function (self, x, y)
	return self._panel:child("info_area"):inside(x, y)
end
TextBoxGui._set_visible_state = function (self)
	local visible = self._visible and self._enabled

	self._panel:set_visible(visible)

	if alive(self._background) then
		self._background:set_visible(visible)
	end

	if alive(self._background2) then
		self._background2:set_visible(visible)
	end

	return 
end
TextBoxGui.can_take_input = function (self)
	return self._visible and self._enabled
end
TextBoxGui.set_visible = function (self, visible)
	self._visible = visible

	self._set_visible_state(self)

	return 
end
TextBoxGui.set_enabled = function (self, enabled)
	if self._enabled == enabled then
		return 
	end

	self._enabled = enabled

	self._set_visible_state(self)

	if self._minimized then
		if not self._enabled then
			self._remove_minimized(self, self._minimized_id)
		else
			self._add_minimized(self)
		end
	end

	return 
end
TextBoxGui.enabled = function (self)
	return self._enabled
end
TextBoxGui._maximize = function (self, data)
	self.set_visible(self, true)
	self.set_minimized(self, false, nil)
	self._remove_minimized(self, data.id)

	return 
end
TextBoxGui.set_minimized = function (self, minimized, minimized_data)
	self._minimized = minimized
	self._minimized_data = minimized_data

	if self._minimized then
		self._add_minimized(self)
	end

	return 
end
TextBoxGui._add_minimized = function (self)
	self._minimized_data.callback = callback(self, self, "_maximize")

	self.set_visible(self, false)

	self._minimized_id = managers.menu_component:add_minimized(self._minimized_data)

	return 
end
TextBoxGui._remove_minimized = function (self, id)
	self._minimized_id = nil

	managers.menu_component:remove_minimized(id)

	return 
end
TextBoxGui.minimized = function (self)
	return self._minimized
end
TextBoxGui.set_position = function (self, x, y)
	self._panel:set_position(x, y)

	return 
end
TextBoxGui.position = function (self)
	return self._panel:position()
end
TextBoxGui.set_size = function (self, x, y)
	self._panel:set_size(x, y)

	local _, _, w, h = self._panel:child("title"):text_rect()
	local lower_static_panel = self._panel:child("lower_static_panel")

	lower_static_panel.set_w(lower_static_panel, self._panel:w())
	lower_static_panel.set_bottom(lower_static_panel, self._panel:h() - h)

	local lsp_h = lower_static_panel.h(lower_static_panel)
	local info_area = self._panel:child("info_area")

	info_area.set_size(info_area, self._panel:w(), self._panel:h() - h*2 - lsp_h)

	if self._info_box then
		self._info_box:create_sides(info_area, {
			sides = {
				1,
				1,
				1,
				1
			}
		})
	end

	local buttons_panel = info_area.child(info_area, "buttons_panel")
	local scroll_panel = info_area.child(info_area, "scroll_panel")

	scroll_panel.set_w(scroll_panel, info_area.w(info_area) - buttons_panel.w(buttons_panel) - 30)
	scroll_panel.set_y(scroll_panel, scroll_panel.y(scroll_panel) - 1)
	scroll_panel.set_y(scroll_panel, scroll_panel.y(scroll_panel) + 1)

	local scroll_up_indicator_shade = self._panel:child("scroll_up_indicator_shade")

	scroll_up_indicator_shade.set_w(scroll_up_indicator_shade, scroll_panel.w(scroll_panel))

	local scroll_up_indicator_arrow = self._panel:child("scroll_up_indicator_arrow")

	scroll_up_indicator_arrow.set_lefttop(scroll_up_indicator_arrow, scroll_panel.right(scroll_panel) + 2, scroll_up_indicator_shade.top(scroll_up_indicator_shade) + 8)

	local top_line = self._panel:child("top_line")

	top_line.set_w(top_line, self._panel:w())
	top_line.set_bottom(top_line, h)

	local bottom_line = self._panel:child("bottom_line")

	bottom_line.set_w(bottom_line, self._panel:w())
	bottom_line.set_top(bottom_line, self._panel:h() - h)

	local scroll_down_indicator_shade = self._panel:child("scroll_down_indicator_shade")

	scroll_down_indicator_shade.set_w(scroll_down_indicator_shade, scroll_panel.w(scroll_panel))
	scroll_down_indicator_shade.set_bottom(scroll_down_indicator_shade, bottom_line.top(bottom_line))

	local scroll_down_indicator_arrow = self._panel:child("scroll_down_indicator_arrow")

	scroll_down_indicator_arrow.set_leftbottom(scroll_down_indicator_arrow, scroll_panel.right(scroll_panel) + 2, scroll_down_indicator_shade.bottom(scroll_down_indicator_shade) - 8)

	local scroll_bar = self._panel:child("scroll_bar")

	scroll_bar.set_center_x(scroll_bar, scroll_down_indicator_arrow.center_x(scroll_down_indicator_arrow))

	local legend_close = self._panel:child("legend_close")

	legend_close.set_top(legend_close, bottom_line.top(bottom_line) + 4)

	local legend_minimize = self._panel:child("legend_minimize")

	legend_minimize.set_bottom(legend_minimize, top_line.bottom(top_line) - 4)
	legend_minimize.set_right(legend_minimize, top_line.right(top_line))

	local legend_scroll = self._panel:child("legend_scroll")

	legend_scroll.set_righttop(legend_scroll, scroll_panel.right(scroll_panel), bottom_line.top(bottom_line) + 4)
	self._set_scroll_indicator(self)
	self._check_scroll_indicator_states(self)

	return 
end
TextBoxGui.size = function (self)
	return self._panel:size()
end
TextBoxGui.open_page = function (self)
	return 
end
TextBoxGui.close_page = function (self)
	return 
end
TextBoxGui.x = function (self)
	return self._panel:x()
end
TextBoxGui.y = function (self)
	return self._panel:y()
end
TextBoxGui.h = function (self)
	return self._panel:h()
end
TextBoxGui.w = function (self)
	return self._panel:w()
end
TextBoxGui.h = function (self)
	return self._panel:h()
end
TextBoxGui.visible = function (self)
	return self._visible
end
TextBoxGui.close = function (self)
	if self._minimized then
		self._remove_minimized(self, self._minimized_id)
	end

	if self._thread then
		self._panel:stop(self._thread)

		self._thread = nil
	end

	if self._info_box then
		self._info_box:close()

		self._info_box = nil
	end

	self._ws:panel():remove(self._panel)

	if alive(self._background) then
		self._fullscreen_ws:panel():remove(self._background)
	end

	if alive(self._background2) then
		self._fullscreen_ws:panel():remove(self._background2)
	end

	if alive(self._fullscreen_ws) then
		managers.gui_data:destroy_workspace(self._fullscreen_ws)

		self._fullscreen_ws = nil
	end

	return 
end

return 
