UiPlacer = UiPlacer or class()
UiPlacer.init = function (self, x, y, padding_x, padding_y)
	self._padding_x = padding_x or 0
	self._padding_y = padding_y or 0
	x = x or 0
	y = y or 0
	self._start_x = x
	self._start_y = y
	self._left = x
	self._right = x
	self._top = y
	self._bottom = y
	self._most = {
		left = x + self._padding_x,
		right = x,
		top = y + self._padding_y,
		bottom = y
	}
	self._stack = {}
	self._first = true

	return 
end
UiPlacer.create_branch = function (self, use_root_level, use_stack)
	local rtn = BranchPlacer:new(self)
	rtn._padding_x = self._padding_x
	rtn._padding_y = self._padding_y
	rtn._start_x = self._start_x
	rtn._start_y = self._start_y
	rtn._left = self._left
	rtn._right = self._right
	rtn._top = self._top
	rtn._bottom = self._bottom
	rtn._first = self._first
	rtn._keep = self._keep
	rtn._most = (use_root_level and self._stack[1]) or self._most
	rtn._stack = (use_stack and self._stack) or {}

	return 
end
UiPlacer.set_padding = function (self, x, y)
	self._padding_x = x or self._padding_x
	self._padding_y = y or self._padding_y

	return 
end
UiPlacer.set_keep_current = function (self, val)
	self._keep = val

	return 
end
UiPlacer.stop_keeping = function (self)
	self._keep = nil

	return 
end
UiPlacer.set_at_from = function (self, item)
	self._update_most(self, item.left(item), item.top(item), item.rightbottom(item))

	if self._keep then
		if type(self._keep) == "number" then
			self._keep = self._keep - 1
			self._keep = (0 < self._keep and self._keep) or nil
		end

		return 
	end

	self._left, self._top = item.lefttop(item)
	self._right, self._bottom = item.rightbottom(item)
	self._first = nil

	return 
end
UiPlacer._update_most = function (self, l, t, r, b, from_branch)
	if from_branch then
		return 
	end

	self._most.left = math.min(self._most.left, l)
	self._most.top = math.min(self._most.top, t)
	self._most.right = math.max(self._most.right, r)
	self._most.bottom = math.max(self._most.bottom, b)

	return 
end
UiPlacer.set_start = function (self, x, y, dont_set_at)
	self._start_x = x or self._start_x
	self._start_y = y or self._start_y

	if dont_set_at then
		return 
	end

	return self.set_at(self, x, y)
end
UiPlacer.set_at = function (self, x, y)
	self._left = x or self._left
	self._right = x or self._right
	self._top = y or self._top
	self._bottom = y or self._bottom

	return x, y
end
UiPlacer._padd_x = function (self, val)
	if self._first then
		return val or 0
	end

	return val or self._padding_x
end
UiPlacer._padd_y = function (self, val)
	if self._first then
		return val or 0
	end

	return val or self._padding_y
end
UiPlacer.add_right = function (self, item, padding)
	padding = self._padd_x(self, padding)

	if item then
		item.set_lefttop(item, self._right + padding, self._top)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._right + padding, self._top)
	end

	return item
end
UiPlacer.add_right_center = function (self, item, padding)
	padding = self._padd_x(self, padding)

	if item then
		item.set_left(item, self._right + padding)
		item.set_center_y(item, (self._top + self._bottom)/2)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._right + padding, nil)
	end

	return item
end
UiPlacer.add_left = function (self, item, padding)
	padding = self._padd_x(self, padding)

	if item then
		item.set_righttop(item, self._left - padding, self._top)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._left - padding, self._top)
	end

	return item
end
UiPlacer.add_left_center = function (self, item, padding)
	padding = self._padd_x(self, padding)

	if item then
		item.set_right(item, self._left - padding)
		item.set_center_y(item, (self._top + self._bottom)/2)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._left - padding, nil)
	end

	return item
end
UiPlacer.add_top = function (self, item, padding)
	padding = self._padd_y(self, padding)

	if item then
		item.set_leftbottom(item, self._left, self._top - padding)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._left, self._top - padding)
	end

	return item
end
UiPlacer.add_top_ralign = function (self, item, padding)
	padding = self._padd_y(self, padding)

	if item then
		item.set_rightbottom(item, self._right, self._top - padding)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._left, self._top - padding)
	end

	return item
end
UiPlacer.add_bottom = function (self, item, padding)
	padding = self._padd_y(self, padding)

	if item then
		item.set_lefttop(item, self._left, self._bottom + padding)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._left, self._bottom + padding)
	end

	return item
end
UiPlacer.add_bottom_ralign = function (self, item, padding)
	padding = self._padd_y(self, padding)

	if item then
		item.set_righttop(item, self._right, self._bottom + padding)
		self.set_at_from(self, item)
	else
		return self.set_at(self, self._right, self._bottom + padding)
	end

	return item
end
UiPlacer.new_row = function (self, padding_x, padding_y)
	padding_x = padding_x or 0
	padding_y = self._padd_y(self, padding_y)

	self.set_at(self, self._start_x + padding_x, math.max(self._most.bottom, self._bottom) + padding_y)

	self._first = true

	return 
end
UiPlacer.add_row = function (self, item, padding_x, padding_y)
	self.new_row(self, padding_x, padding_y)

	return self.add_right(self, item)
end
UiPlacer.is_first_in_row = function (self)
	return self._first
end
UiPlacer._push = function (self, x, y)
	table.insert(self._stack, {
		self._start_x,
		self._start_y,
		self._most
	})

	self._start_x = x
	self._start_y = y
	self._most = {
		left = x + self._padding_x,
		top = y + self._padding_y,
		right = x,
		bottom = y
	}

	return 
end
UiPlacer.push = function (self)
	table.insert(self._stack, {
		self._start_x,
		self._start_y,
		self._most
	})

	self._most = {
		bottom = -999,
		right = -999,
		left = self._most.right*999,
		top = self._most.bottom*999
	}

	return 
end
UiPlacer.push_right = function (self)
	self._push(self, self._right, self._top)

	return 
end
UiPlacer.push_left = function (self)
	self._push(self, self._left, self._top)

	return 
end
UiPlacer.push_top = function (self)
	self._push(self, self._left, self._top)

	return 
end
UiPlacer.push_bottom = function (self)
	self._push(self, self._left, self._bottom)

	return 
end
UiPlacer.pop = function (self)
	self._left = self._most.left
	self._top = self._most.top
	self._right = self._most.right
	self._bottom = self._most.bottom
	local most = nil
	self._start_x, self._start_y, most = unpack(table.remove(self._stack))
	self._most.left = math.min(self._most.left, most.left)
	self._most.top = math.min(self._most.top, most.top)
	self._most.right = math.max(self._most.right, most.right)
	self._most.bottom = math.max(self._most.bottom, most.bottom)

	return 
end
UiPlacer.corners = function (self)
	return self._left, self._top, self._right, self._bottom
end
UiPlacer.current_right = function (self)
	return self._right
end
UiPlacer.current_left = function (self)
	return self._left
end
UiPlacer.current_top = function (self)
	return self._top
end
UiPlacer.current_bottom = function (self)
	return self._bottom
end
UiPlacer.current_center = function (self)
	return self._left + (self._right - self._left)/2, self._top + (self._bottom - self._top)/2
end
UiPlacer.most = function (self)
	return self._most
end
UiPlacer.most_corners = function (self)
	return self._most.left, self._most.top, self._most.right, self._most.bottom
end
UiPlacer.most_rightbottom = function (self)
	return self._most.right, self._most.bottom
end
UiPlacer.most_leftbottom = function (self)
	return self._most.left, self._most.bottom
end
BranchPlacer = BranchPlacer or class(UiPlacer)
BranchPlacer.init = function (self, placer)
	self._from = placer

	return 
end
BranchPlacer._update_most = function (self, a, b, c, d, branch)
	self._from:_update_most(a, b, c, d, true)
	BranchPlacer.super._update_most(a, b, c, d, branch)

	return 
end
ResizingPlacer = ResizingPlacer or class(UiPlacer)
ResizingPlacer.init = function (self, panel, config)
	self._panel = panel
	self._border_padding_x = config.border_x or config.border or 0
	self._border_padding_y = config.border_y or config.border or 0

	ResizingPlacer.super.init(self, config.x or self._border_padding_x, config.y or self._border_padding_y, config.padding_x or config.padding, config.padding_y or config.padding)

	return 
end
ResizingPlacer.clear = function (self, keep_stack)
	if not keep_stack and 0 < #self._stack then
		self._start_x, self._start_y = unpack(self._stack[1])
		self._stack = {}
	end

	local x, y = self.set_at(self, self._start_x, self._start_y)
	self._most = {
		left = x + self._padding_x,
		right = x,
		top = y + self._padding_y,
		bottom = y
	}
	self._first = true

	return 
end
ResizingPlacer._update_most = function (self, ...)
	ResizingPlacer.super._update_most(self, ...)

	if self._panel._ensure_size then
		self._panel:_ensure_size(self._most.right + self._border_padding_x, self._most.bottom + self._border_padding_y)
	else
		self._panel:set_size(self._most.right + self._border_padding_x, self._most.bottom + self._border_padding_y)
	end

	return 
end

return 
