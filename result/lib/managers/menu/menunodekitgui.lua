core:import("CoreMenuNodeGui")

MenuNodeKitGui = MenuNodeKitGui or class(MenuNodeGui)
MenuNodeKitGui.init = function (self, node, layer, parameters)
	MenuNodeKitGui.super.init(self, node, layer, parameters)

	return 
end
MenuNodeKitGui._setup_item_panel_parent = function (self, safe_rect, shape)
	MenuNodeKitGui.super._setup_item_panel_parent(self, safe_rect, shape)

	return 
end
MenuNodeKitGui._update_scaled_values = function (self)
	MenuNodeKitGui.super._update_scaled_values(self)

	self.font_size = tweak_data.menu.kit_default_font_size

	return 
end
MenuNodeKitGui.resolution_changed = function (self)
	self._update_scaled_values(self)
	MenuNodeKitGui.super.resolution_changed(self)

	return 
end

return 
