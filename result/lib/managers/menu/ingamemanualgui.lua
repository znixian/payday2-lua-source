IngameManualGui = IngameManualGui or class()
IngameManualGui.PAGES = 8
IngameManualGui.init = function (self, ws, fullscreen_ws)
	self._ws = ws
	self._fullscreen_ws = fullscreen_ws
	self._manual_panel = self._ws:panel():panel()
	self._fullscreen_panel = self._fullscreen_ws:panel():panel()
	self._active = true
	local black_bg = self._fullscreen_panel:rect({
		valign = "scale",
		alpha = 0,
		halign = "scale",
		layer = 0,
		color = Color.black
	})

	local function fade_in_anim(o)
		over(0.35, function (p)
			o:set_alpha(p)

			return 
		end)

		return 
	end

	black_bg.animate(black_bg, fade_in_anim)

	local width = math.round(self._manual_panel:w())
	local height = math.round((self._manual_panel:h() - self._manual_panel:w()/2)*0.5)

	self._manual_panel:rect({
		h = 1,
		x = 0,
		layer = 4,
		w = width,
		y = height
	})
	self._manual_panel:rect({
		h = 1,
		x = 0,
		layer = 4,
		w = width,
		y = self._manual_panel:h() - height - 1
	})
	self._manual_panel:rect({
		w = 1,
		x = 0,
		layer = 4,
		h = self._manual_panel:h() - height*2 - 2,
		y = height + 1
	})
	self._manual_panel:rect({
		w = 1,
		layer = 4,
		h = self._manual_panel:h() - height*2 - 2,
		x = self._manual_panel:w() - 1,
		y = height + 1
	})

	self._manual_y = height
	self._page_counter = self._manual_panel:text({
		align = "center",
		layer = 4,
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		text = "1/" .. tostring(self.PAGES)
	})
	self._zoom = 1

	self.open_manual_page(self, 1)

	return 
end
IngameManualGui._setup_controller_input = function (self)
	self._left_axis_vector = Vector3()
	self._right_axis_vector = Vector3()

	self._fullscreen_ws:connect_controller(managers.menu:active_menu().input:get_controller(), true)
	self._fullscreen_panel:axis_move(callback(self, self, "_axis_move"))

	return 
end
IngameManualGui._destroy_controller_input = function (self)
	self._fullscreen_ws:disconnect_all_controllers()

	if alive(self._fullscreen_panel) then
		self._fullscreen_panel:axis_move(nil)
	end

	return 
end
IngameManualGui._axis_move = function (self, o, axis_name, axis_vector, controller)
	if axis_name == Idstring("left") then
		mvector3.set(self._left_axis_vector, axis_vector)
	elseif axis_name == Idstring("right") then
		mvector3.set(self._right_axis_vector, axis_vector)
	end

	return 
end
IngameManualGui.update = function (self, t, dt)
	if managers.menu:is_pc_controller() then
		return 
	end

	local left_x, left_y = managers.menu_component:get_left_controller_axis()
	local right_x, right_y = managers.menu_component:get_right_controller_axis()

	self.controller_move(self, -left_x*dt, left_y*dt)
	self.controller_zoom(self, right_y*dt)

	return 
end
IngameManualGui.correct_position = function (self)
	if 0 < self._page:left() then
		self._page:set_left(0)
	elseif self._page:right() < self._page_panel:w() then
		self._page:set_right(self._page_panel:w())
	end

	if 0 < self._page:top() then
		self._page:set_top(0)
	elseif self._page:bottom() < self._page_panel:h() then
		self._page:set_bottom(self._page_panel:h())
	end

	return 
end
IngameManualGui.controller_move = function (self, x, y)
	if self._loading then
		return 
	end

	if not alive(self._page) then
		return 
	end

	local speed = 512

	self._page:move(x*speed, y*speed)
	self.correct_position(self)

	return 
end
IngameManualGui.controller_zoom = function (self, y)
	if self._loading then
		return 
	end

	if not alive(self._page) then
		return 
	end

	self._zoom = math.clamp(self._zoom + y*2, 1, 2)
	local w, h = self._manual_panel:size()
	local px, py = self._page:position()
	local x = w/2 - px
	local y = h/2 - py
	local sx = x/self._page:w()
	local sy = y/self._page:h()
	local aspect = self._page:texture_height()/math.max(self._page:texture_width(), 1)
	local width = self._page_panel:w()
	local height = self._page_panel:h()

	self._page:set_size(width*self._zoom, width*aspect*self._zoom)
	self._page:set_position(w/2 - sx*self._page:w(), h/2 - sy*self._page:h())
	self.correct_position(self)

	return 
end
IngameManualGui.next_page = function (self)
	self.open_manual_page(self, self._current_page + 1)

	return 
end
IngameManualGui.previous_page = function (self)
	self.open_manual_page(self, self._current_page - 1)

	return 
end
IngameManualGui.input_focus = function (self)
	return 1
end
IngameManualGui.open_manual_page = function (self, page)
	local new_page = math.clamp(page, 1, self.PAGES)

	if new_page == self._current_page then
		return 
	end

	local path = "guis/textures/pd2/ingame_manual/page_"
	local lang_key = SystemInfo:language():key()
	local files = {
		[Idstring("french"):key()] = "_fr",
		[Idstring("spanish"):key()] = "_es"
	}
	self._zoom = 1
	self._current_page = new_page

	self._page_counter:set_text(tostring(self._current_page) .. "/" .. tostring(self.PAGES))

	local new_page = path .. tostring(page) .. (files[lang_key] or "")

	self.create_page(self, new_page)

	return 
end
IngameManualGui.remove_page = function (self, unretrieve_texture)
	if alive(self._page_panel) then
		self._page_panel:parent():remove(self._page_panel)

		self._page_panel = nil
		self._page = nil
	end

	if alive(self._page) then
		self._page:parent():remove(self._page)

		self._page = nil
	end

	if unretrieve_texture then
		self.unretrieve_texture(self)
	end

	return 
end
IngameManualGui.create_page = function (self, texture_path)
	self.remove_page(self, true)

	self._page = self._manual_panel:panel()
	local loading_text = self._page:text({
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		text = managers.localization:to_upper_text("debug_loading_level")
	})
	local x, y, w, h = loading_text.text_rect(loading_text)

	loading_text.set_size(loading_text, w, h)

	local spinning_item = self._page:bitmap({
		w = 32,
		texture = "guis/textures/icon_loading",
		h = 32
	})

	loading_text.set_position(loading_text, 10, self._manual_y + 10)
	spinning_item.set_left(spinning_item, loading_text.right(loading_text))
	spinning_item.set_center_y(spinning_item, loading_text.center_y(loading_text))

	local function spin_anim(o)
		local dt = nil

		while true do
			dt = coroutine.yield()

			o.rotate(o, dt*360)
		end

		return 
	end

	spinning_item.animate(spinning_item, spin_anim)

	self._loading = true

	if not managers.menu_component then
		return 
	end

	if DB:has(Idstring("texture"), texture_path) then
		local texture_count = managers.menu_component:request_texture(texture_path, callback(self, self, "texture_done_clbk"))
		self._requested_texture = {
			texture_count = texture_count,
			texture = texture_path
		}
	end

	return 
end
IngameManualGui.unretrieve_texture = function (self)
	if self._requested_texture then
		managers.menu_component:unretrieve_texture(self._requested_texture.texture, self._requested_texture.texture_count)

		self._requested_texture = nil
	end

	return 
end
IngameManualGui.texture_done_clbk = function (self, texture_ids)
	local new_page_panel = self._manual_panel:panel({
		visible = false
	})
	local texture = new_page_panel.bitmap(new_page_panel, {
		name = "texture",
		layer = 1,
		texture = texture_ids
	})

	new_page_panel.show(new_page_panel)
	self.remove_page(self, false)

	self._page_panel = new_page_panel
	self._page = texture
	local aspect = self._page:texture_height()/math.max(self._page:texture_width(), 1)
	local width = self._manual_panel:w()
	local height = self._manual_panel:h()

	new_page_panel.set_h(new_page_panel, width*aspect)
	new_page_panel.set_w(new_page_panel, width)
	new_page_panel.set_center(new_page_panel, self._manual_panel:w()/2, self._manual_panel:h()/2)
	self._page:set_w(new_page_panel.w(new_page_panel))
	self._page:set_h(new_page_panel.h(new_page_panel))
	texture.set_size(texture, self._page:w(), self._page:h())

	self._loading = nil

	return 
end
IngameManualGui.set_layer = function (self, layer)
	return 
end
IngameManualGui.close = function (self)
	self.remove_page(self, true)
	self._ws:panel():remove(self._manual_panel)
	self._fullscreen_ws:panel():remove(self._fullscreen_panel)

	return 
end

return 
