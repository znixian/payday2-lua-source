PlayerProfileGuiObject = PlayerProfileGuiObject or class()
PlayerProfileGuiObject.init = function (self, ws)
	local panel = ws.panel(ws):panel()
	local next_level_data = managers.experience:next_level_data() or {}
	local max_left_len = 0
	local max_right_len = 0
	local font = tweak_data.menu.pd2_small_font
	local font_size = tweak_data.menu.pd2_small_font_size
	local bg_ring = panel.bitmap(panel, {
		texture = "guis/textures/pd2/level_ring_small",
		y = 10,
		alpha = 0.4,
		x = 10,
		w = font_size*4,
		h = font_size*4,
		color = Color.black
	})
	local exp_ring = panel.bitmap(panel, {
		texture = "guis/textures/pd2/level_ring_small",
		render_template = "VertexColorTexturedRadial",
		blend_mode = "add",
		y = 10,
		x = 10,
		layer = 1,
		w = font_size*4,
		h = font_size*4,
		color = Color((next_level_data.current_points or 1)/(next_level_data.points or 1), 1, 1)
	})
	local player_level = managers.experience:current_level()
	local player_rank = managers.experience:current_rank()
	local is_infamous = 0 < player_rank
	local level_string = ((is_infamous and managers.experience:rank_string(player_rank) .. "-") or "") .. tostring(player_level)
	local level_text = panel.text(panel, {
		vertical = "center",
		align = "center",
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size + ((is_infamous and -5) or 0),
		text = level_string,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, level_text)
	level_text.set_font_size(level_text, level_text.font_size(level_text)*math.min((font_size*2)/level_text.w(level_text), 1))
	level_text.set_center(level_text, exp_ring.center(exp_ring))

	max_left_len = math.max(max_left_len, level_text.w(level_text))
	local player_text = panel.text(panel, {
		y = 10,
		font = font,
		font_size = font_size,
		text = tostring(managers.network.account:username() or managers.blackmarket:get_preferred_character_real_name()),
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, player_text)
	player_text.set_left(player_text, math.round(exp_ring.right(exp_ring)))

	max_left_len = math.max(max_left_len, player_text.w(player_text))
	local money_text = panel.text(panel, {
		text = self.get_text(self, "menu_cash", {
			money = managers.money:total_string()
		}),
		font_size = font_size,
		font = font,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, money_text)
	money_text.set_left(money_text, math.round(exp_ring.right(exp_ring)))
	money_text.set_top(money_text, math.round(player_text.bottom(player_text)))

	max_left_len = math.max(max_left_len, money_text.w(money_text))
	local total_money_text = panel.text(panel, {
		text = self.get_text(self, "hud_offshore_account") .. ": " .. managers.experience:cash_string(managers.money:offshore()),
		font_size = font_size,
		font = font,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, total_money_text)
	total_money_text.set_left(total_money_text, math.round(exp_ring.right(exp_ring)))
	total_money_text.set_top(total_money_text, math.round(money_text.bottom(money_text)))

	max_left_len = math.max(max_left_len, total_money_text.w(total_money_text))
	local skillpoints = managers.skilltree:points()
	local skill_text, skill_glow = nil

	if 0 < skillpoints then
		skill_text = panel.text(panel, {
			layer = 1,
			text = self.get_text(self, "menu_spendable_skill_points", {
				points = tostring(skillpoints)
			}),
			font_size = font_size,
			font = font,
			color = tweak_data.screen_colors.text
		})

		self._make_fine_text(self, skill_text)
		skill_text.set_left(skill_text, math.round(exp_ring.right(exp_ring)))
		skill_text.set_top(skill_text, math.round(total_money_text.bottom(total_money_text)))

		max_left_len = math.max(max_left_len, skill_text.w(skill_text))
		local skill_icon = panel.bitmap(panel, {
			w = 16,
			texture = "guis/textures/pd2/shared_skillpoint_symbol",
			h = 16,
			layer = 1
		})

		skill_icon.set_right(skill_icon, skill_text.left(skill_text))
		skill_icon.set_center_y(skill_icon, skill_text.center_y(skill_text) + 1)

		skill_glow = panel.bitmap(panel, {
			texture = "guis/textures/pd2/crimenet_marker_glow",
			blend_mode = "add",
			layer = 0,
			w = panel.w(panel),
			h = skill_text.h(skill_text)*2,
			color = tweak_data.screen_colors.button_stage_3
		})

		skill_glow.set_center_y(skill_glow, skill_icon.center_y(skill_icon))
	end

	local font_scale = 1
	local mastermind_ponts, num_skills = managers.skilltree:get_tree_progress_2("mastermind")
	mastermind_ponts = string.format("%02d", mastermind_ponts)
	local mastermind_text = panel.text(panel, {
		y = 10,
		text = self.get_text(self, "menu_profession_progress", {
			profession = self.get_text(self, "st_menu_mastermind"),
			progress = mastermind_ponts,
			num_skills = num_skills
		}),
		font_size = font_size*font_scale,
		font = font,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, mastermind_text)

	max_right_len = math.max(max_right_len, mastermind_text.w(mastermind_text))
	local enforcer_ponts, num_skills = managers.skilltree:get_tree_progress_2("enforcer")
	enforcer_ponts = string.format("%02d", enforcer_ponts)
	local enforcer_text = panel.text(panel, {
		text = self.get_text(self, "menu_profession_progress", {
			profession = self.get_text(self, "st_menu_enforcer"),
			progress = enforcer_ponts,
			num_skills = num_skills
		}),
		font_size = font_size*font_scale,
		font = font,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, enforcer_text)
	enforcer_text.set_top(enforcer_text, math.round(mastermind_text.bottom(mastermind_text)))

	max_right_len = math.max(max_right_len, enforcer_text.w(enforcer_text))
	local technician_ponts, num_skills = managers.skilltree:get_tree_progress_2("technician")
	technician_ponts = string.format("%02d", technician_ponts)
	local technician_text = panel.text(panel, {
		text = self.get_text(self, "menu_profession_progress", {
			profession = self.get_text(self, "st_menu_technician"),
			progress = technician_ponts,
			num_skills = num_skills
		}),
		font_size = font_size*font_scale,
		font = font,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, technician_text)
	technician_text.set_top(technician_text, math.round(enforcer_text.bottom(enforcer_text)))

	max_right_len = math.max(max_right_len, technician_text.w(technician_text))
	local ghost_ponts, num_skills = managers.skilltree:get_tree_progress_2("ghost")
	ghost_ponts = string.format("%02d", ghost_ponts)
	local ghost_text = panel.text(panel, {
		text = self.get_text(self, "menu_profession_progress", {
			profession = self.get_text(self, "st_menu_ghost"),
			progress = ghost_ponts,
			num_skills = num_skills
		}),
		font_size = font_size*font_scale,
		font = font,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, ghost_text)
	ghost_text.set_top(ghost_text, math.round(technician_text.bottom(technician_text)))

	max_right_len = math.max(max_right_len, ghost_text.w(ghost_text))
	local hoxton_ponts, num_skills = managers.skilltree:get_tree_progress_2("hoxton")
	hoxton_ponts = string.format("%02d", hoxton_ponts)
	local hoxton_text = panel.text(panel, {
		text = self.get_text(self, "menu_profession_progress", {
			profession = self.get_text(self, "st_menu_hoxton_pack"),
			progress = hoxton_ponts,
			num_skills = num_skills
		}),
		font_size = font_size*font_scale,
		font = font,
		color = tweak_data.screen_colors.text
	})

	self._make_fine_text(self, hoxton_text)
	hoxton_text.set_top(hoxton_text, math.round(ghost_text.bottom(ghost_text)))

	max_right_len = math.max(max_right_len, hoxton_text.w(hoxton_text))
	self._panel = panel

	self._panel:set_size(exp_ring.w(exp_ring) + max_left_len + 15 + max_right_len + 10, math.max((skill_text and skill_text.bottom(skill_text)) or total_money_text.bottom(total_money_text), hoxton_text.bottom(hoxton_text)) + 8)
	self._panel:set_bottom(self._panel:parent():h() - 60)
	BoxGuiObject:new(self._panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})
	mastermind_text.set_right(mastermind_text, self._panel:w() - 10)
	enforcer_text.set_right(enforcer_text, self._panel:w() - 10)
	technician_text.set_right(technician_text, self._panel:w() - 10)
	ghost_text.set_right(ghost_text, self._panel:w() - 10)
	hoxton_text.set_right(hoxton_text, self._panel:w() - 10)
	bg_ring.move(bg_ring, -5, 0)
	exp_ring.move(exp_ring, -5, 0)
	level_text.set_center(level_text, exp_ring.center(exp_ring))

	if skill_glow then
		local function animate_new_skillpoints(o)
			while true do
				over(1, function (p)
					o:set_alpha(math.lerp(0.4, 0.85, math.sin(p*180)))

					return 
				end)
			end

			return 
		end

		skill_glow.set_w(skill_glow, self._panel:w())
		skill_glow.set_center_x(skill_glow, (skill_text and skill_text.center_x(skill_text)) or 0)
		skill_glow.animate(skill_glow, animate_new_skillpoints)
	end

	self._rec_round_object(self, panel)

	return 
end
PlayerProfileGuiObject._rec_round_object = function (self, object)
	local x, y, w, h = object.shape(object)

	object.set_shape(object, math.round(x), math.round(y), math.round(w), math.round(h))

	if object.children then
		for i, d in ipairs(object.children(object)) do
			self._rec_round_object(self, d)
		end
	end

	return 
end
PlayerProfileGuiObject.get_text = function (self, text, macros)
	return utf8.to_upper(managers.localization:text(text, macros))
end
PlayerProfileGuiObject._make_fine_text = function (self, text)
	local x, y, w, h = text.text_rect(text)

	text.set_size(text, w, h)
	text.set_position(text, math.round(text.x(text)), math.round(text.y(text)))

	return 
end
PlayerProfileGuiObject.close = function (self)
	if self._panel and alive(self._panel) then
		self._panel:parent():remove(self._panel)

		self._panel = nil
	end

	return 
end

return 
