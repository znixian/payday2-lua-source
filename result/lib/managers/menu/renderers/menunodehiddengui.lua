MenuNodeHiddenGui = MenuNodeHiddenGui or class(MenuNodeGui)
MenuNodeHiddenGui._create_menu_item = function (self, row_item)
	MenuNodeHiddenGui.super._create_menu_item(self, row_item)
	row_item.gui_panel:hide()

	return 
end

return 
