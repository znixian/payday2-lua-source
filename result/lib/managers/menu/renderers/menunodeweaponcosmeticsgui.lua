MenuNodeSkinEditorGui = MenuNodeSkinEditorGui or class(MenuNodeGui)
MenuNodeSkinEditorGui.init = function (self, node, layer, parameters)
	parameters.font = tweak_data.menu.pd2_small_font
	parameters.font_size = tweak_data.menu.pd2_small_font_size
	parameters.row_item_blend_mode = "add"
	parameters.row_item_color = tweak_data.screen_colors.button_stage_3
	parameters.row_item_hightlight_color = tweak_data.screen_colors.button_stage_2
	parameters.marker_alpha = 1
	parameters.to_upper = true

	MenuNodeSkinEditorGui.super.init(self, node, layer, parameters)

	return 
end
MenuNodeSkinEditorGui.mouse_pressed = function (self, button, x, y)
	if button == Idstring("1") then
		local row_item = self._highlighted_item and self.row_item(self, self._highlighted_item)

		if row_item and row_item.gui_panel and row_item.gui_panel:inside(x, y) then
			local key = self._highlighted_item:parameters().key or self._highlighted_item:name()
			local vector = self._highlighted_item:parameters().vector

			if key == "pattern_tweak" then
				self._highlighted_item:set_value((vector ~= 2 or 0) and 1)
			elseif key == "pattern_pos" then
				self._highlighted_item:set_value(0)
			elseif key == "wear_and_tear" then
				self._highlighted_item:set_value(1)
			elseif key == "uv_scale" then
				self._highlighted_item:set_value(19)
			elseif key == "uv_offset_rot" then
				self._highlighted_item:set_value(0)
			elseif key == "cubemap_pattern_control" then
				self._highlighted_item:set_value(0)
			else
				return 
			end

			MenuCallbackHandler:weapon_skin_changed(self._highlighted_item)
		end
	end

	return 
end
MenuNodeArmorSkinEditorGui = MenuNodeArmorSkinEditorGui or class(MenuNodeGui)
MenuNodeArmorSkinEditorGui.init = function (self, node, layer, parameters)
	parameters.font = tweak_data.menu.pd2_small_font
	parameters.font_size = tweak_data.menu.pd2_small_font_size
	parameters.row_item_blend_mode = "add"
	parameters.row_item_color = tweak_data.screen_colors.button_stage_3
	parameters.row_item_hightlight_color = tweak_data.screen_colors.button_stage_2
	parameters.marker_alpha = 1
	parameters.to_upper = true

	MenuNodeArmorSkinEditorGui.super.init(self, node, layer, parameters)

	if node.parameters(node).create_background ~= false then
		local panel = self.ws:panel():panel({})

		panel.set_w(panel, self.ws:panel():w()*((node.parameters(node).align_line_proportions or 0.65) - 1))
		panel.set_right(panel, self.ws:panel():w())
		panel.set_h(panel, self.ws:panel():h() - tweak_data.menu.pd2_large_font_size - 10)
		panel.rect(panel, {
			alpha = 0.4,
			color = Color.black
		})
		BoxGuiObject:new(panel, {
			sides = {
				1,
				1,
				1,
				1
			}
		})
	end

	return 
end
MenuNodeArmorSkinEditorGui.mouse_pressed = function (self, button, x, y)
	if button == Idstring("1") then
		local row_item = self._highlighted_item and self.row_item(self, self._highlighted_item)

		if row_item and row_item.gui_panel and row_item.gui_panel:inside(x, y) then
			local key = self._highlighted_item:parameters().key or self._highlighted_item:name()
			local vector = self._highlighted_item:parameters().vector

			if key == "pattern_tweak" then
				self._highlighted_item:set_value((vector ~= 2 or 0) and 1)
			elseif key == "pattern_pos" then
				self._highlighted_item:set_value(0)
			elseif key == "wear_and_tear" then
				self._highlighted_item:set_value(1)
			elseif key == "uv_scale" then
				self._highlighted_item:set_value(19)
			elseif key == "uv_offset_rot" then
				self._highlighted_item:set_value(0)
			elseif key == "cubemap_pattern_control" then
				self._highlighted_item:set_value(0)
			else
				return 
			end

			MenuCallbackHandler:armor_skin_changed(self._highlighted_item)
		end
	end

	return 
end

return 
