require("lib/managers/menu/renderers/MenuNodeBaseGui")

MenuNodeJukeboxGui = MenuNodeJukeboxGui or class(MenuNodeBaseGui)
MenuNodeJukeboxGui.init = function (self, node, layer, parameters)
	parameters.align = "left"
	parameters._align_line_proportions = 0.5

	MenuNodeJukeboxGui.super.init(self, node, layer, parameters)
	self.item_panel:set_y(self.item_panel:parent():y() + 165)
	self._set_topic_position(self)

	node.parameters(node).block_back = true
	node.parameters(node).allow_pause_menu = true

	return 
end
MenuNodeJukeboxGui.close = function (self)
	MenuNodeJukeboxGui.super.close(self)

	return 
end

return 
