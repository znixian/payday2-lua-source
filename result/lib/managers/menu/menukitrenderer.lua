core:import("CoreMenuNodeGui")
require("lib/managers/menu/MenuNodeKitGui")

MenuKitRenderer = MenuKitRenderer or class(MenuLobbyRenderer)
MenuKitRenderer.init = function (self, logic)
	local parameters = {
		layer = 200
	}

	MenuRenderer.init(self, logic, parameters)

	return 
end
MenuKitRenderer._setup_bg = function (self)
	return 
end
MenuKitRenderer.show_node = function (self, node)
	local gui_class = MenuNodeKitGui

	if node.parameters(node).gui_class then
		gui_class = CoreSerialize.string_to_classtable(node.parameters(node).gui_class)
	end

	local parameters = {
		marker_alpha = 0.6,
		align = "right",
		row_item_blend_mode = "add",
		to_upper = true,
		row_item_color = tweak_data.screen_colors.button_stage_3,
		row_item_hightlight_color = tweak_data.screen_colors.button_stage_2,
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		node_gui_class = gui_class,
		spacing = node.parameters(node).spacing,
		marker_color = tweak_data.screen_colors.button_stage_3:with_alpha(0.2)
	}

	MenuKitRenderer.super.super.show_node(self, node, parameters)
	self._update_slots_info(self)

	return 
end
MenuKitRenderer.open = function (self, ...)
	self._all_items_enabled = true
	self._no_stencil = true
	self._server_state_string_id = "menu_lobby_server_state_in_game"

	MenuKitRenderer.super.open(self, ...)

	if self._player_slots then
		for slot4, slot5 in ipairs(self._player_slots) do
		end
	end

	return 
end
MenuKitRenderer._update_slots_info = function (self)
	print("MenuKitRenderer:_update_slots_info")

	local kit_menu = managers.menu:get_menu("kit_menu")

	if kit_menu then
		local id = managers.network:session():local_peer():id()
		local criminal_name = managers.network:session():local_peer():character()

		kit_menu.renderer:set_slot_outfit(id, criminal_name, managers.blackmarket:outfit_string())
	end

	for peer_id, peer in pairs(managers.network:session():peers()) do
		if not peer.loading(peer) and peer.is_streaming_complete(peer) then
			if peer.waiting_for_player_ready(peer) then
				self.set_slot_ready(self, peer, peer_id)
			else
				self.set_slot_not_ready(self, peer, peer_id)
			end
		else
			self.set_slot_joining(self, peer, peer_id)
		end
	end

	return 
end
MenuKitRenderer._entered_menu = function (self)
	self.on_request_lobby_slot_reply(self)

	local kit_menu = managers.menu:get_menu("kit_menu")

	if kit_menu then
		local id = managers.network:session():local_peer():id()
		local criminal_name = managers.network:session():local_peer():character()

		kit_menu.renderer:set_slot_outfit(id, criminal_name, managers.blackmarket:outfit_string())
	end

	for peer_id, peer in pairs(managers.network:session():peers()) do
		self.set_slot_joining(self, peer, peer_id)
	end

	return 
end
MenuKitRenderer._set_player_slot = function (self, nr, params)
	local peer = managers.network:session():peer(nr)
	local ready = peer.waiting_for_player_ready(peer)
	params.status = string.upper(managers.localization:text((ready and "menu_waiting_is_ready") or "menu_waiting_is_not_ready"))
	params.kit_panel_visible = true

	MenuKitRenderer.super._set_player_slot(self, nr, params)

	return 
end
MenuKitRenderer.highlight_item = function (self, item, ...)
	MenuKitRenderer.super.highlight_item(self, item, ...)
	self.post_event(self, "highlight")

	return 
end
MenuKitRenderer.trigger_item = function (self, item)
	MenuKitRenderer.super.trigger_item(self, item)

	local node_gui = self.active_node_gui(self)

	if node_gui and node_gui.trigger_item then
		node_gui.trigger_item(node_gui, item)
	end

	return 
end
MenuKitRenderer.sync_chat_message = function (self, message, id)
	for _, node_gui in ipairs(self._node_gui_stack) do
		local row_item_chat = node_gui.row_item_by_name(node_gui, "chat")

		if row_item_chat then
			node_gui.sync_say(node_gui, message, row_item_chat, id)

			return true
		end
	end

	return false
end
MenuKitRenderer.set_all_items_enabled = function (self, enabled)
	self._all_items_enabled = enabled

	for _, node in ipairs(self._logic._node_stack) do
		for _, item in ipairs(node.items(node)) do
			if item.type(item) == "kitslot" or item.type(item) == "toggle" then
				item.set_enabled(item, enabled)
			end
		end
	end

	return 
end
MenuKitRenderer.set_ready_items_enabled = function (self, enabled)
	if not self._all_items_enabled then
		return 
	end

	for _, node in ipairs(self._logic._node_stack) do
		for _, item in ipairs(node.items(node)) do
			if item.type(item) == "kitslot" then
				item.set_enabled(item, enabled)
			end
		end
	end

	return 
end
MenuKitRenderer.set_bg_visible = function (self, visible)
	if self._menu_bg then
		self._menu_bg:set_visible(visible)
	end

	return 
end
MenuKitRenderer.set_bg_area = function (self, area)
	if self._menu_bg then
		if area == "full" then
			self._menu_bg:set_size(self._menu_bg:parent():size())
			self._menu_bg:set_position(0, 0)
		elseif area == "half" then
			self._menu_bg:set_size(self._menu_bg:parent():w()*0.5, self._menu_bg:parent():h())
			self._menu_bg:set_top(0)
			self._menu_bg:set_right(self._menu_bg:parent():w())
		else
			self._menu_bg:set_size(self._menu_bg:parent():size())
			self._menu_bg:set_position(0, 0)
		end
	end

	return 
end
MenuKitRenderer.set_slot_joining = function (self, peer, peer_id)
	MenuKitRenderer.super.set_slot_joining(self, peer, peer_id)
	managers.preplanning:on_peer_added(peer_id)

	return 
end
MenuKitRenderer.remove_player_slot_by_peer_id = function (self, peer, reason)
	MenuKitRenderer.super.remove_player_slot_by_peer_id(self, peer, reason)

	local peer_id = peer.id(peer)

	managers.preplanning:on_peer_removed(peer_id)

	return 
end
MenuKitRenderer.close = function (self, ...)
	MenuKitRenderer.super.close(self, ...)

	return 
end

return 
