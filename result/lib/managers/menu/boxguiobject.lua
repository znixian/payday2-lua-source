BoxGuiObject = BoxGuiObject or class()
local mvector_tl = Vector3()
local mvector_tr = Vector3()
local mvector_bl = Vector3()
local mvector_br = Vector3()
BoxGuiObject.init = function (self, panel, config)
	self.create_sides(self, panel, config)

	return 
end
BoxGuiObject.create_sides = function (self, panel, config)
	if not alive(panel) then
		Application:error("[BoxGuiObject:create_sides] Failed creating BoxGui. Parent panel not alive!")
		Application:stack_dump()

		return 
	end

	if alive(self._panel) then
		self._panel:parent():remove(self._panel)

		self._panel = nil
	end

	self._panel = panel.panel(panel, {
		layer = 1,
		halign = "scale",
		valign = "scale",
		name = config.name,
		w = config.w,
		h = config.h
	})
	self._color = config.color or self._color or Color.white
	local left_side = (config.sides and config.sides[1]) or config.left or 0
	local right_side = (config.sides and config.sides[2]) or config.right or 0
	local top_side = (config.sides and config.sides[3]) or config.top or 0
	local bottom_side = (config.sides and config.sides[4]) or config.bottom or 0

	self._create_side(self, self._panel, "left", left_side, config.texture)
	self._create_side(self, self._panel, "right", right_side, config.texture)
	self._create_side(self, self._panel, "top", top_side, config.texture)
	self._create_side(self, self._panel, "bottom", bottom_side, config.texture)

	return 
end
BoxGuiObject._create_side = function (self, panel, side, type, texture)
	local ids_side = Idstring(side)
	local ids_left = Idstring("left")
	local ids_right = Idstring("right")
	local ids_top = Idstring("top")
	local ids_bottom = Idstring("bottom")
	local left_or_right = false
	local w, h = nil

	if ids_side == ids_left or ids_side == ids_right then
		left_or_right = true
		w = 2
		h = panel.h(panel)
	else
		w = panel.w(panel)
		h = 2
	end

	local side_panel = panel.panel(panel, {
		name = side,
		w = w,
		h = h,
		halign = (left_or_right and side) or "scale",
		valign = (left_or_right and "scale") or side
	})

	if type == 0 then
		return 
	elseif type == 1 or type == 3 or type == 4 then
		local one = side_panel.bitmap(side_panel, {
			texture = texture or "guis/textures/pd2/shared_lines"
		})
		local two = side_panel.bitmap(side_panel, {
			texture = texture or "guis/textures/pd2/shared_lines"
		})
		local x = math.random(1, 255)
		local y = math.random(0, one.texture_height(one)/2 - 1)*2
		local tw = math.min(10, w)
		local th = math.min(10, h)

		if left_or_right then
			one.set_halign(one, side)
			two.set_halign(two, side)
			one.set_valign(one, "scale")
			two.set_valign(two, "scale")
			mvector3.set_static(mvector_tl, x, y + tw, 0)
			mvector3.set_static(mvector_tr, x, y, 0)
			mvector3.set_static(mvector_bl, x + th, y + tw, 0)
			mvector3.set_static(mvector_br, x + th, y, 0)
			one.set_texture_coordinates(one, mvector_tl, mvector_tr, mvector_bl, mvector_br)

			x = math.random(1, 255)
			y = math.random(0, math.round(one.texture_height(one)/2 - 1))*2

			mvector3.set_static(mvector_tl, x, y + tw, 0)
			mvector3.set_static(mvector_tr, x, y, 0)
			mvector3.set_static(mvector_bl, x + th, y + tw, 0)
			mvector3.set_static(mvector_br, x + th, y, 0)
			two.set_texture_coordinates(two, mvector_tl, mvector_tr, mvector_bl, mvector_br)
			one.set_size(one, 2, th)
			two.set_size(two, 2, th)
			two.set_bottom(two, h)
		else
			one.set_halign(one, "scale")
			two.set_halign(two, "scale")
			one.set_valign(one, side)
			two.set_valign(two, side)
			mvector3.set_static(mvector_tl, x, y, 0)
			mvector3.set_static(mvector_tr, x + tw, y, 0)
			mvector3.set_static(mvector_bl, x, y + th, 0)
			mvector3.set_static(mvector_br, x + tw, y + th, 0)
			one.set_texture_coordinates(one, mvector_tl, mvector_tr, mvector_bl, mvector_br)

			x = math.random(1, 255)
			y = math.random(0, math.round(one.texture_height(one)/2 - 1))*2

			mvector3.set_static(mvector_tl, x, y, 0)
			mvector3.set_static(mvector_tr, x + tw, y, 0)
			mvector3.set_static(mvector_bl, x, y + th, 0)
			mvector3.set_static(mvector_br, x + tw, y + th, 0)
			two.set_texture_coordinates(two, mvector_tl, mvector_tr, mvector_bl, mvector_br)
			one.set_size(one, tw, 2)
			two.set_size(two, tw, 2)
			two.set_right(two, w)
		end

		one.set_visible(one, type == 1 or type == 3)
		two.set_visible(two, type == 1 or type == 4)
	elseif type == 2 then
		local full = side_panel.bitmap(side_panel, {
			texture = texture or "guis/textures/pd2/shared_lines",
			w = side_panel.w(side_panel),
			h = side_panel.h(side_panel)
		})
		local x = math.random(1, 255)
		local y = math.random(0, math.round(full.texture_height(full)/2 - 1))*2

		if left_or_right then
			full.set_halign(full, side)
			full.set_valign(full, "scale")
			mvector3.set_static(mvector_tl, x, y + w, 0)
			mvector3.set_static(mvector_tr, x, y, 0)
			mvector3.set_static(mvector_bl, x + h, y + w, 0)
			mvector3.set_static(mvector_br, x + h, y, 0)
			full.set_texture_coordinates(full, mvector_tl, mvector_tr, mvector_bl, mvector_br)
		else
			full.set_halign(full, "scale")
			full.set_valign(full, side)
			mvector3.set_static(mvector_tl, x, y, 0)
			mvector3.set_static(mvector_tr, x + w, y, 0)
			mvector3.set_static(mvector_bl, x, y + h, 0)
			mvector3.set_static(mvector_br, x + w, y + h, 0)
			full.set_texture_coordinates(full, mvector_tl, mvector_tr, mvector_bl, mvector_br)
		end
	else
		Application:error("[BoxGuiObject] Type", type, "is not supported")
		Application:stack_dump()

		return 
	end

	side_panel.set_position(side_panel, 0, 0)

	if ids_side == ids_right then
		side_panel.set_right(side_panel, panel.w(panel))
	elseif ids_side == ids_bottom then
		side_panel.set_bottom(side_panel, panel.h(panel))
	end

	return side_panel
end
BoxGuiObject.hide = function (self)
	self._panel:hide()

	return 
end
BoxGuiObject.show = function (self)
	self._panel:show()

	return 
end
BoxGuiObject.set_visible = function (self, visible)
	self._panel:set_visible(visible)

	return 
end
BoxGuiObject.visible = function (self)
	return self._panel:visible()
end
BoxGuiObject.set_layer = function (self, layer)
	self._panel:set_layer(layer)

	return 
end
BoxGuiObject.size = function (self)
	return self._panel:size()
end
BoxGuiObject.alive = function (self)
	return alive(self._panel)
end
BoxGuiObject.inside = function (self, x, y, side)
	if not self.alive(self) then
		return false
	end

	if side then
		return self._panel:child(side) and self._panel:child(side):inside(x, y)
	else
		return self._panel:inside(x, y)
	end

	return 
end
BoxGuiObject.set_aligns = function (self, halign, valign)
	for i, d in pairs(self._panel:children()) do
		d.set_valign(d, valign)
		d.set_halign(d, halign)
	end

	return 
end
BoxGuiObject.set_clipping = function (self, clip, rec_panel)
	slot3 = pairs
	slot4 = (rec_panel and rec_panel.children(rec_panel)) or self._panel:children()

	for i, d in slot3(slot4) do
		if d.set_rotation then
			d.set_rotation(d, (not clip or 0) and 360)
		else
			self.set_clipping(self, clip, d)
		end
	end

	return 
end
BoxGuiObject.color = function (self)
	return self._color
end
BoxGuiObject.set_color = function (self, color, rec_panel)
	self._color = color
	slot3 = pairs
	slot4 = (rec_panel and rec_panel.children(rec_panel)) or self._panel:children()

	for i, d in slot3(slot4) do
		if d.set_color then
			d.set_color(d, color)
		else
			self.set_color(self, color, d)
		end
	end

	return 
end
BoxGuiObject.blend_mode = function (self)
	return self._blend_mode
end
BoxGuiObject.set_blend_mode = function (self, blend_mode, rec_panel)
	self._blend_mode = blend_mode
	slot3 = pairs
	slot4 = (rec_panel and rec_panel.children(rec_panel)) or self._panel:children()

	for i, d in slot3(slot4) do
		if d.set_blend_mode then
			d.set_blend_mode(d, blend_mode)
		else
			self.set_blend_mode(self, blend_mode, d)
		end
	end

	return 
end
BoxGuiObject.close = function (self)
	if alive(self._panel) and alive(self._panel:parent()) then
		self._panel:parent():remove(self._panel)
	end

	return 
end

return 
