IngameContractGuiCrimeSpree = IngameContractGuiCrimeSpree or class()
IngameContractGuiCrimeSpree.init = function (self, ws, node)
	local padding = (SystemInfo:platform() == Idstring("WIN32") and 10) or 5
	self._ws = ws
	self._panel = ws.panel(ws):panel({
		w = math.round(ws.panel(ws):w()*0.6),
		h = math.round(ws.panel(ws):h()*1)
	})

	self._panel:set_y((CoreMenuRenderer.Renderer.border_height + tweak_data.menu.pd2_large_font_size) - 5)
	self._panel:grow(0, -(self._panel:y() + tweak_data.menu.pd2_medium_font_size))

	self._node = node
	local job_data = managers.job:current_job_data()
	local job_chain = managers.job:current_job_chain_data()
	local mission = managers.crime_spree:get_mission(managers.crime_spree:current_played_mission())

	if not mission then
		return 
	end

	local contract_text = self._panel:text({
		text = "",
		vertical = "bottom",
		rotation = 360,
		layer = 1,
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		color = tweak_data.screen_colors.text
	})

	contract_text.set_text(contract_text, self.get_text(self, "cn_crime_spree_level", {
		level = managers.experience:cash_string(managers.crime_spree:server_spree_level(), "")
	}))

	local range = {
		from = utf8.len(managers.localization:text("cn_crime_spree_level_no_num")),
		to = utf8.len(managers.localization:text(contract_text.text(contract_text))),
		color = tweak_data.screen_colors.crime_spree_risk
	}

	contract_text.set_range_color(contract_text, range.from, range.to, range.color)
	contract_text.set_bottom(contract_text, 5)

	local text_panel = self._panel:panel({
		layer = 1,
		w = self._panel:w() - padding*2,
		h = self._panel:h() - padding*2
	})

	text_panel.set_left(text_panel, padding)
	text_panel.set_top(text_panel, padding)

	local level_title = text_panel.text(text_panel, {
		text = "",
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		color = tweak_data.screen_colors.text
	})
	local level_id = managers.localization:to_upper_text(tweak_data.levels[mission.level.level_id].name_id) or ""

	level_title.set_text(level_title, level_id)
	managers.hud:make_fine_text(level_title)

	local briefing_title = text_panel.text(text_panel, {
		text = "",
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		color = tweak_data.screen_colors.text
	})

	briefing_title.set_text(briefing_title, self.get_text(self, "menu_briefing"))
	managers.hud:make_fine_text(briefing_title)
	briefing_title.set_top(briefing_title, level_title.bottom(level_title))

	local font_size = tweak_data.menu.pd2_small_font_size
	local text = managers.localization:text(tweak_data.levels[mission.level.level_id].briefing_id) or ""
	local briefing_description = text_panel.text(text_panel, {
		name = "briefing_description",
		vertical = "top",
		h = 128,
		wrap = true,
		align = "left",
		word_wrap = true,
		text = text,
		font = tweak_data.menu.pd2_small_font,
		font_size = font_size,
		color = tweak_data.screen_colors.text
	})
	local _, _, _, h = briefing_description.text_rect(briefing_description)

	briefing_description.set_h(briefing_description, h)
	briefing_description.set_top(briefing_description, briefing_title.bottom(briefing_title))

	local modifiers_title = text_panel.text(text_panel, {
		text = "",
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		color = tweak_data.screen_colors.text
	})

	modifiers_title.set_text(modifiers_title, self.get_text(self, "cn_crime_spree_modifiers"))
	managers.hud:make_fine_text(modifiers_title)
	modifiers_title.set_top(modifiers_title, briefing_description.bottom(briefing_description) + padding)

	self._modifiers_panel = self._panel:panel({
		w = self._panel:w() - padding*2,
		h = self._panel:h() - padding*2 - modifiers_title.bottom(modifiers_title),
		x = padding,
		y = modifiers_title.bottom(modifiers_title) + padding
	})

	CrimeSpreeModifierDetailsPage.add_modifiers_panel(self, self._modifiers_panel, managers.crime_spree:server_active_modifiers(), false)

	self._text_panel = text_panel

	self._rec_round_object(self, self._panel)

	self._sides = BoxGuiObject:new(self._panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	if not managers.menu:is_pc_controller() then
		self._setup_controller_input(self)
	end

	return 
end
IngameContractGuiCrimeSpree._rec_round_object = function (self, object)
	if object.children then
		for i, d in ipairs(object.children(object)) do
			self._rec_round_object(self, d)
		end
	end

	local x, y = object.position(object)

	object.set_position(object, math.round(x), math.round(y))

	return 
end
IngameContractGuiCrimeSpree.set_layer = function (self, layer)
	if self._panel and alive(self._panel) then
		self._panel:set_layer(layer)
	end

	return 
end
IngameContractGuiCrimeSpree.get_text = function (self, text, macros)
	return utf8.to_upper(managers.localization:text(text, macros))
end
IngameContractGuiCrimeSpree._make_fine_text = function (self, text)
	local x, y, w, h = text.text_rect(text)

	text.set_size(text, w, h)

	return 
end
IngameContractGuiCrimeSpree.mouse_moved = function (self, o, x, y)
	return 
end
IngameContractGuiCrimeSpree.mouse_pressed = function (self, button, x, y)
	return 
end
IngameContractGuiCrimeSpree.mouse_wheel_up = function (self, x, y)
	self._scroll:scroll(x, y, 1)

	return 
end
IngameContractGuiCrimeSpree.mouse_wheel_down = function (self, x, y)
	self._scroll:scroll(x, y, -1)

	return 
end
IngameContractGuiCrimeSpree.special_btn_pressed = function (self, button)
	if button == Idstring("menu_modify_item") then
		self._toggle_potential_rewards(self)
	end

	return false
end
IngameContractGuiCrimeSpree._setup_controller_input = function (self)
	CrimeSpreeContractMenuComponent._setup_controller_input(self)

	return 
end
IngameContractGuiCrimeSpree._axis_move = function (self, o, axis_name, axis_vector, controller)
	CrimeSpreeContractMenuComponent._axis_move(self, o, axis_name, axis_vector, controller)

	return 
end
IngameContractGuiCrimeSpree.update = function (self, t, dt)
	CrimeSpreeContractMenuComponent.update(self, t, dt)

	return 
end
IngameContractGuiCrimeSpree.close = function (self)
	if self._panel and alive(self._panel) then
		self._panel:parent():remove(self._panel)

		self._panel = nil
	end

	return 
end

return 
