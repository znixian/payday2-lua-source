require("lib/managers/HUDManager")
require("lib/managers/HUDManagerVR")

local function make_fine_text(text)
	local x, y, w, h = text.text_rect(text)

	text.set_size(text, w, h)
	text.set_position(text, math.round(text.x(text)), math.round(text.y(text)))

	return 
end

local PADDING = 30
VRGuiObject = VRGuiObject or class()
VRGuiObject.init = function (self, panel, id, params)
	self._id = id
	self._panel = panel.panel(panel, {
		w = params.w,
		h = params.h,
		x = params.x,
		y = params.y
	})
	self._parent_menu = params.parent_menu
	self._enabled = true

	return 
end
VRGuiObject.id = function (self)
	return self._id
end
VRGuiObject.parent_menu = function (self)
	return self._parent_menu
end
VRGuiObject.set_enabled = function (self, enabled)
	self._enabled = enabled

	self._panel:set_visible(enabled)

	return 
end
VRGuiObject.enabled = function (self)
	return self._enabled
end
VRGuiObject.set_selected = function (self, selected)
	if self._selected == selected then
		return 
	end

	self._selected = selected

	if selected then
		managers.menu:post_event("highlight")
	end

	return true
end
VRGuiObject.moved = function (self, x, y)
	return 
end
VRGuiObject.pressed = function (self, x, y)
	return 
end
VRGuiObject.released = function (self, x, y)
	return 
end
local overrides = {
	"inside",
	"x",
	"y",
	"w",
	"h",
	"left",
	"right",
	"top",
	"bottom",
	"set_x",
	"set_y",
	"set_w",
	"set_h",
	"set_left",
	"set_right",
	"set_top",
	"set_bottom",
	"set_visible"
}

for _, func in ipairs(overrides) do
	VRGuiObject[func] = function (self, ...)
		return self._panel[func](self._panel, ...)
	end
end

local unselected_color = Color.black:with_alpha(0.5)
local selected_color = Color.black:with_alpha(0.7)
VRButton = VRButton or class(VRGuiObject)
VRButton.init = function (self, panel, id, params)
	params.w = params.w or 200
	params.h = params.h or 75

	VRButton.super.init(self, panel, id, params)

	self._bg = self._panel:rect({
		name = "bg",
		color = unselected_color
	})
	self._text = self._panel:text({
		font_size = 50,
		text = managers.localization:to_upper_text(params.text_id),
		font = tweak_data.menu.pd2_massive_font
	})

	make_fine_text(self._text)
	self._text:set_center(self._panel:w()/2, self._panel:h()/2)
	BoxGuiObject:new(self._panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	return 
end
VRButton.set_selected = function (self, selected)
	if VRButton.super.set_selected(self, selected) then
		self._bg:set_color((selected and selected_color) or unselected_color)
	end

	return 
end
VRButton.set_text = function (self, text_id)
	self._text:set_text(managers.localization:to_upper_text(text_id))
	make_fine_text(self._text)
	self._text:set_center_x(self._panel:w()/2)

	return 
end
VRSlider = VRSlider or class(VRGuiObject)
VRSlider.init = function (self, panel, id, params)
	params.w = params.w or 400
	params.h = params.h or 75

	VRSlider.super.init(self, panel, id, params)

	self._value = params.value or 0
	self._max = params.max or 1
	self._min = params.min or 0
	self._snap = params.snap or 1
	self._value_clbk = params.value_clbk
	self._line = self._panel:rect({
		h = 4,
		name = "line"
	})

	self._line:set_center_y(self._panel:h()/2)

	self._mid_piece = self._panel:panel({
		w = 100,
		name = "mid_piece",
		layer = 1
	})

	self._mid_piece:set_center_x(self._panel:w()/2)

	self._bg = self._mid_piece:rect({
		name = "bg",
		color = unselected_color
	})
	self._text = self._mid_piece:text({
		font_size = 50,
		text = tostring(math.floor(self._value)),
		font = tweak_data.menu.pd2_massive_font
	})

	make_fine_text(self._text)
	self._text:set_center(self._mid_piece:w()/2, self._mid_piece:h()/2)
	BoxGuiObject:new(self._mid_piece, {
		sides = {
			1,
			1,
			1,
			1
		}
	})
	self._update_position(self)

	return 
end
VRSlider.value = function (self)
	return self._value
end
VRSlider.value_ratio = function (self)
	return (self._value - self._min)/(self._max - self._min)
end
VRSlider.value_from_ratio = function (self, ratio)
	return math.clamp((self._max - self._min)*ratio + self._min, self._min, self._max)
end
VRSlider.set_value = function (self, value)
	self._value = math.clamp(value, self._min, self._max)

	self.set_text(self, math.floor(self._value))
	self._update_position(self)

	return 
end
VRSlider._update_position = function (self)
	local value_ratio = self.value_ratio(self)
	local w = self._panel:w() - self._mid_piece:w()

	self._mid_piece:set_center_x(value_ratio*w + self._mid_piece:w()/2)

	return 
end
VRSlider.set_selected = function (self, selected)
	if VRButton.super.set_selected(self, selected) then
		self._bg:set_color((selected and selected_color) or unselected_color)
	end

	return 
end
VRSlider.set_text = function (self, text)
	self._text:set_text(tostring(text))
	make_fine_text(self._text)
	self._text:set_center_x(self._mid_piece:w()/2)

	return 
end
VRSlider.pressed = function (self, x, y)
	if not self._selected then
		return 
	end

	self._start_x = x
	self._start_ratio = self.value_ratio(self)
	self._pressed = true

	return 
end
VRSlider.released = function (self, x, y)
	if self._pressed and self._value_clbk then
		self._value_clbk(self._value)
	end

	self._pressed = nil

	return 
end
VRSlider.moved = function (self, x, y)
	if self._pressed then
		local diff = x - self._start_x
		local diff_ratio = diff/(self._panel:w() - self._mid_piece:w())

		if self._snap <= math.abs(diff_ratio)*self._max then
			self.set_value(self, math.floor(self.value_from_ratio(self, diff_ratio + self._start_ratio)/self._snap)*self._snap)

			self._start_ratio = self.value_ratio(self)
			self._start_x = self._panel:world_x() + self._mid_piece:w()/2 + (self._panel:w() - self._mid_piece:w())*self._start_ratio
		end
	end

	return 
end
VRSettingButton = VRSettingButton or class(VRButton)
VRSettingButton.init = function (self, panel, id, params)
	if not params.setting then
		Application:error("Tried to add a setting button without a setting!")
	end

	params.text_id = self._get_setting_text(self, managers.vr:get_setting(params.setting))

	VRSettingButton.super.init(self, panel, id, params)

	self._setting = params.setting

	return 
end
VRSettingButton._get_setting_text = function (self, value)
	if type(value) == "boolean" then
		return (value and "menu_vr_on") or "menu_vr_off"
	else
		return "menu_vr_" .. tostring(value)
	end

	return 
end
VRSettingButton.setting_changed = function (self)
	local new_value = managers.vr:get_setting(self._setting)

	self.set_text(self, self._get_setting_text(self, new_value))

	return 
end
VRSettingSlider = VRSettingSlider or class(VRSlider)
VRSettingSlider.init = function (self, panel, id, params)
	if not params.setting then
		Application:error("Tried to add a setting slider without a setting!")
	end

	params.value = managers.vr:get_setting(params.setting)
	params.value_clbk = params.value_clbk or function (value)
		managers.vr:set_setting(params.setting, value)

		return 
	end
	params.min, params.max = managers.vr:setting_limits(params.setting)

	if not params.max then
		Application:error("Tried to add a setting slider without limits: " .. params.setting)
	end

	VRSettingSlider.super.init(self, panel, id, params)

	self._setting = params.setting

	return 
end
VRSettingSlider.setting_changed = function (self)
	local new_value = managers.vr:get_setting(self._setting)

	self.set_value(self, new_value)

	return 
end
VRSettingTrigger = VRSettingTrigger or class(VRButton)
VRSettingTrigger.init = function (self, panel, id, params)
	VRSettingTrigger.super.init(self, panel, id, params)

	self._setting = params.setting
	self._change_clbk = params.change_clbk

	return 
end
VRSettingTrigger.setting_changed = function (self)
	if self._change_clbk then
		self._change_clbk(self, managers.vr:get_setting(self._setting))
	end

	return 
end
VRMenu = VRMenu or class()
VRMenu.init = function (self)
	self._buttons = {}
	self._sub_menus = {}
	self._objects = {}

	return 
end
VRMenu.set_selected = function (self, index)
	if self._selected and self._selected ~= index then
		self._buttons[self._selected].button:set_selected(false)
	end

	if index then
		self._buttons[index].button:set_selected(true)
	end

	self._selected = index

	return 
end
VRMenu.selected = function (self)
	return self._selected and self._buttons[self._selected]
end
VRMenu.mouse_moved = function (self, o, x, y)
	local selected = nil

	for i, button in ipairs(self._buttons) do
		if button.button:inside(x, y) and button.button:enabled() then
			selected = i
		end

		button.button:moved(x, y)
	end

	self.set_selected(self, selected)

	if self._open_menu then
		self._open_menu:mouse_moved(o, x, y)
	end

	return 
end
VRMenu.mouse_pressed = function (self, o, button, x, y)
	if button ~= Idstring("0") then
		return 
	end

	for _, button in ipairs(self._buttons) do
		button.button:pressed(x, y)
	end

	if self._open_menu then
		self._open_menu:mouse_pressed(o, button, x, y)
	end

	return 
end
VRMenu.mouse_released = function (self, o, button, x, y)
	if button ~= Idstring("0") then
		return 
	end

	for _, button in ipairs(self._buttons) do
		button.button:released(x, y)
	end

	if self._open_menu then
		self._open_menu:mouse_released(o, button, x, y)
	end

	return 
end
VRMenu.mouse_clicked = function (self, o, button, x, y)
	if self.selected(self) and self.selected(self).clbk then
		self.selected(self).clbk(self.selected(self).button)
		managers.menu:post_event("menu_enter")
	end

	if self._open_menu then
		self._open_menu:mouse_clicked(o, button, x, y)
	end

	return 
end
VRMenu.add_object = function (self, id, obj)
	self._objects[id] = obj

	return 
end
VRMenu.remove_object = function (self, id)
	if self._objects[id].destroy then
		self._objects[id]:destroy()
	end

	self._objects[id] = nil

	return 
end
VRMenu.object = function (self, id)
	return self._objects[id]
end
VRMenu.clear_objects = function (self)
	for id in pairs(self._objects) do
		self.remove_object(self, id)
	end

	return 
end
VRMenu.update = function (self, t, dt)
	for id, obj in pairs(self._objects) do
		obj.update(obj, t, dt)
	end

	return 
end
VRSubMenu = VRSubMenu or class(VRMenu)
VRSubMenu.init = function (self, panel, id)
	VRSubMenu.super.init(self)

	self._id = id
	self._enabled = false
	self._panel = panel.panel(panel, {
		visible = false,
		w = panel.w(panel)*0.8 - PADDING*2,
		h = panel.h(panel) - PADDING*2,
		x = panel.w(panel)*0.2 + PADDING,
		y = PADDING
	})

	return 
end
VRSubMenu.add_desc = function (self, desc)
	self._desc = self._panel:text({
		word_wrap = true,
		wrap = true,
		text = managers.localization:text(desc),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size
	})

	make_fine_text(self._desc)

	return 
end
VRSubMenu.setting = function (self, id)
	return self._settings and self._settings[id]
end
VRSubMenu.add_setting = function (self, type, text_id, setting, params)
	local y_offset = 0

	if self._desc then
		y_offset = self._desc:h()
	end

	self._settings = self._settings or {}
	local num_settings = table.size(self._settings)
	params = params or {}
	local setting_text = self._panel:text({
		word_wrap = true,
		wrap = true,
		text = managers.localization:text(text_id),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		y = num_settings*100 + y_offset
	})
	local setting_item, clbk = nil

	if type == "button" then
		setting_item = VRSettingButton:new(self._panel, setting, table.map_append({
			setting = setting,
			parent_menu = self
		}, params))

		function clbk(btn)
			local new_value = not managers.vr:get_setting(setting)

			managers.vr:set_setting(setting, new_value)
			btn.setting_changed(btn)

			if params.clbk then
				params.clbk(new_value)
			end

			return 
		end
	elseif type == "slider" then
		local function clbk(value)
			managers.vr:set_setting(setting, value)

			return 
		end

		setting_item = VRSettingSlider:new(self._panel, setting, table.map_append({
			setting = setting,
			parent_menu = self
		}, params))
	elseif type == "multi_button" then
		if not params.options then
			Application:error("Tried to add a multi_button setting without options: " .. setting)

			params.options = {
				"error"
			}
		end

		local option_count = #params.options
		setting_item = VRSettingButton:new(self._panel, setting, table.map_append({
			setting = setting,
			parent_menu = self
		}, params))

		function clbk(btn)
			local current_index = table.index_of(params.options, managers.vr:get_setting(setting))
			local new_index = current_index%option_count + 1
			local new_value = params.options[new_index]

			managers.vr:set_setting(setting, new_value)
			btn.setting_changed(btn)

			if params.clbk then
				params.clbk(new_value)
			end

			return 
		end
	elseif type == "trigger" then
		params.text_id = params.trigger_text_id
		setting_item = VRSettingTrigger:new(self._panel, setting, table.map_append({
			setting = setting,
			parent_menu = self
		}, params))

		function clbk(btn)
			local value = params.value_clbk(btn)

			managers.vr:set_setting(setting, value)

			return 
		end
	end

	setting_item.set_y(setting_item, num_settings*100 + y_offset)
	setting_item.set_right(setting_item, self._panel:w() - PADDING)
	setting_text.set_w(setting_text, self._panel:w() - setting_item.w(setting_item) - PADDING*2)
	make_fine_text(setting_text)
	table.insert(self._buttons, {
		button = setting_item,
		clbk = clbk,
		custom_params = {
			x = setting_item.x(setting_item),
			y = setting_item.y(setting_item)
		}
	})

	self._settings[setting] = {
		text = setting_text,
		button = setting_item
	}

	return 
end
VRSubMenu.set_setting_enabled = function (self, setting, enabled)
	local item = self.setting(self, setting)

	if item then
		item.text:set_visible(enabled)
		item.button:set_visible(enabled)
		item.button:set_enabled(enabled)
	end

	return 
end
VRSubMenu.add_button = function (self, id, text, clbk, custom_params)
	custom_params = custom_params or {}
	local button = VRButton:new(self._panel, id, {
		text_id = text,
		parent_menu = self
	})
	slot7 = button
	slot6 = button.set_x

	if not custom_params.x then
		slot8 = (self._buttons[#self._buttons] and self._buttons[#self._buttons].button:left()) or self._panel:w()
		slot8 = slot8 - button.w(button) - PADDING
	end

	slot6(slot7, slot8)
	button.set_y(button, custom_params.y or self._panel:h() - button.h(button) - PADDING)
	table.insert(self._buttons, {
		button = button,
		clbk = clbk,
		custom_params = custom_params
	})

	return button
end
VRSubMenu.set_button_enabled = function (self, id, enabled)
	for _, button in ipairs(self._buttons) do
		if button.button:id() == id then
			button.button:set_enabled(enabled)
		end
	end

	self.layout_buttons(self)

	return 
end
VRSubMenu.layout_buttons = function (self)
	local last_x = self._panel:w()

	for _, button in ipairs(self._buttons) do
		if button.button:enabled() and not button.custom_params.x then
			button.button:set_x(last_x - button.button:w() - PADDING)

			last_x = button.button:x()
		end
	end

	return 
end
VRSubMenu.add_image = function (self, params)
	if not params or not params.texture then
		Application:error("[VRSubMenu:add_image] tried to add missing image!")

		return 
	end

	local image = self._panel:bitmap({
		texture = params.texture,
		x = params.x,
		y = params.y,
		w = params.w,
		h = params.h
	})

	if params.fit then
		if params.fit == "height" then
			local h = self._panel:h()
			local dh = h/image.texture_height(image)

			image.set_size(image, image.texture_width(image)*dh, h)
		elseif params.fit == "width" then
			local w = self._panel:w()
			local dw = w/image.texture_width(image)

			image.set_size(image, w, image.texture_height(image)*dw)
		else
			image.set_size(image, self._panel:w(), self._panel:h())
		end
	end

	return 
end
VRSubMenu.set_temp_text = function (self, text_id, color)
	self.clear_temp_text(self)

	self._temp_text = self._panel:text({
		word_wrap = true,
		wrap = true,
		text = managers.localization:text(text_id),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		color = color or Color.white,
		y = self._desc:bottom() + PADDING
	})

	make_fine_text(self._temp_text)

	return 
end
VRSubMenu.clear_temp_text = function (self)
	if alive(self._temp_text) then
		self._panel:remove(self._temp_text)

		self._temp_text = nil
	end

	return 
end
VRSubMenu.id = function (self)
	return self._id
end
VRSubMenu.set_enabled_clbk = function (self, clbk)
	self._enabled_clbk = clbk

	return 
end
VRSubMenu.set_enabled = function (self, enabled)
	if self._enabled_clbk then
		self._enabled_clbk(self, enabled)
	end

	self._enabled = enabled

	self._panel:set_visible(enabled)

	return 
end
VRSubMenu.enabled = function (self)
	return self._enabled
end
VRCustomizationGui = VRCustomizationGui or class(VRMenu)
VRCustomizationGui.init = function (self, is_start_menu)
	VRCustomizationGui.super.init(self)

	self._is_start_menu = is_start_menu
	self._ws = managers.gui_data:create_fullscreen_workspace("left")

	managers.menu:player():register_workspace({
		ws = self._ws,
		activate = callback(self, self, "activate"),
		deactivate = callback(self, self, "deactivate")
	})

	self._id = "vr_customization"

	if not is_start_menu then
		self.initialize(self)
	else
		self._ws:hide()
	end

	return 
end
VRCustomizationGui.initialize = function (self)
	if not self._initialized then
		self._setup_gui(self)
		managers.vr:show_savefile_dialog()

		if not managers.vr:has_set_height() then
			managers.menu:show_vr_settings_dialog()
			self.open_sub_menu(self, "height")
		end

		self._ws:show()

		self._initialized = true
	end

	return 
end
VRCustomizationGui._setup_gui = function (self)
	if alive(self._panel) then
		self._panel:clear()
	end

	self._panel = self._ws:panel():panel({})
	self._buttons = {}
	self._bg = self._panel:bitmap({
		texture = "guis/dlcs/vr/textures/pd2/bg",
		name = "bg",
		layer = -2
	})
	local h = self._panel:h()
	local dh = h/self._bg:texture_height()

	self._bg:set_size(self._bg:texture_width()*dh, h)
	self._setup_sub_menus(self)
	self.add_back_button(self)

	self._controls_image = self._panel:bitmap({
		texture = "guis/dlcs/vr/textures/pd2/menu_controls",
		x = self._panel:w()*0.2 + PADDING,
		y = PADDING
	})
	local h = self._panel:h() - PADDING*2
	local dh = h/self._controls_image:texture_height()

	self._controls_image:set_size(self._controls_image:texture_width()*dh, h)

	return 
end
VRCustomizationGui._setup_sub_menus = function (self)
	self._sub_menus = {}
	self._open_menu = nil
	local is_start_menu = self._is_start_menu

	self.add_sub_menu(self, "height", "menu_vr_height_desc", {
		{
			text = "menu_vr_calibrate",
			id = "calibrate",
			clbk = function (btn)
				local hmd_pos = VRManager:hmd_position()

				managers.vr:set_setting("height", hmd_pos.z)
				managers.system_menu:close("vr_settings")
				btn.parent_menu(btn):set_temp_text("menu_vr_height_success", Color.green)

				return 
			end
		},
		{
			text = "menu_vr_reset",
			id = "reset",
			clbk = function (btn)
				managers.vr:reset_setting("height")

				return 
			end
		}
	})
	self.add_settings_menu(self, "belt", {
		{
			setting = "belt_snap",
			type = "slider",
			text_id = "menu_vr_belt_snap",
			params = {
				snap = 15
			}
		},
		{
			setting = "belt_height_ratio",
			type = "trigger",
			text_id = "menu_vr_belt_height",
			params = {
				trigger_text_id = "menu_vr_set_height",
				value_clbk = function (btn)
					local belt = btn.parent_menu(btn):object("belt")

					if belt then
						return belt.height(belt)/managers.vr:get_setting("height")
					end

					return 
				end,
				change_clbk = function (btn, value)
					local belt = btn.parent_menu(btn):object("belt")

					if belt then
						belt.reset(belt, value*managers.vr:get_setting("height"))
					end

					return 
				end
			}
		}
	}, function (menu, enabled)
		if enabled then
			if not menu.object(menu, "belt") then
				menu.add_object(menu, "belt", VRBeltCustomization:new(is_start_menu))
			end
		elseif menu.object(menu, "belt") then
			menu.remove_object(menu, "belt")
		end

		return 
	end)
	self.add_settings_menu(self, "gameplay", {
		{
			setting = "auto_reload",
			type = "button",
			text_id = "menu_vr_auto_reload_text"
		},
		{
			setting = "default_weapon_hand",
			type = "multi_button",
			text_id = "menu_vr_default_weapon_hand",
			params = {
				options = {
					"right",
					"left"
				},
				clbk = function (value)
					managers.menu:set_primary_hand(value)

					return 
				end
			}
		},
		{
			setting = "default_tablet_hand",
			type = "multi_button",
			text_id = "menu_vr_default_tablet_hand",
			params = {
				options = {
					"left",
					"right"
				}
			}
		}
	})
	self.add_settings_menu(self, "advanced", {
		{
			setting = "weapon_switch_button",
			type = "button",
			text_id = "menu_vr_weapon_switch_button"
		},
		{
			setting = "autowarp_length",
			type = "multi_button",
			text_id = "menu_vr_autowarp_length",
			params = {
				options = {
					"off",
					"long",
					"short"
				}
			}
		},
		{
			setting = "zipline_screen",
			type = "button",
			text_id = "menu_vr_zipline_screen"
		}
	})

	return 
end
VRCustomizationGui.sub_menu = function (self, id)
	return self._sub_menus[id]
end
VRCustomizationGui.add_sub_menu = function (self, id, desc, buttons, clbk)
	local menu = VRSubMenu:new(self._panel, id)

	menu.add_desc(menu, desc)
	menu.set_enabled_clbk(menu, clbk)

	for _, button in ipairs(buttons) do
		local btn = menu.add_button(menu, button.id, button.text, button.clbk)

		if button.enabled ~= nil then
			btn.set_enabled(btn, button.enabled)
		end
	end

	menu.layout_buttons(menu)

	self._sub_menus[id] = menu

	self.add_menu_button(self, id)

	return 
end
VRCustomizationGui.add_settings_menu = function (self, id, settings, clbk)
	local menu = VRSubMenu:new(self._panel, id)

	menu.set_enabled_clbk(menu, clbk)
	menu.add_button(menu, "reset_" .. id, "menu_vr_reset", function ()
		for setting, item in pairs(menu._settings) do
			managers.vr:reset_setting(setting)
			item.button:setting_changed()
		end

		return 
	end)

	for _, setting in ipairs(settings) do
		menu.add_setting(menu, setting.type, setting.text_id, setting.setting, setting.params)
	end

	self._sub_menus[id] = menu

	self.add_menu_button(self, id)

	return 
end
VRCustomizationGui.add_image_menu = function (self, id, params, clbk)
	local menu = VRSubMenu:new(self._panel, id)

	menu.set_enabled_clbk(menu, clbk)
	menu.add_image(menu, params)

	self._sub_menus[id] = menu

	self.add_menu_button(self, id)

	return 
end
VRCustomizationGui.open_sub_menu = function (self, id)
	self.close_sub_menu(self)
	self._controls_image:set_visible(false)

	self._open_menu = self._sub_menus[id]

	self._open_menu:set_enabled(true)

	return 
end
VRCustomizationGui.close_sub_menu = function (self)
	if self._open_menu then
		self._open_menu:set_enabled(false)

		self._open_menu = nil

		self._controls_image:set_visible(true)
	end

	return 
end
VRCustomizationGui.add_menu_button = function (self, id)
	local x = PADDING
	local y = PADDING + ((self._buttons[#self._buttons] and self._buttons[#self._buttons].button:bottom()) or 0)
	local button = VRButton:new(self._panel, id, {
		text_id = "menu_vr_open_" .. id,
		x = x,
		y = y
	})

	table.insert(self._buttons, {
		button = button,
		clbk = callback(self, self, "open_sub_menu", id)
	})

	return 
end
VRCustomizationGui.add_back_button = function (self)
	local x = PADDING
	local y = self._panel:h() - 75 - PADDING
	local button = VRButton:new(self._panel, "back", {
		text_id = "menu_vr_back",
		x = x,
		y = y
	})

	table.insert(self._buttons, {
		button = button,
		clbk = callback(self, self, "close_sub_menu")
	})

	return 
end
VRCustomizationGui.update = function (self, t, dt)
	if self._open_menu then
		self._open_menu:update(t, dt)
	end

	return 
end
VRCustomizationGui.activate = function (self)
	local clbks = {
		mouse_move = callback(self, self, "mouse_moved"),
		mouse_click = callback(self, self, "mouse_clicked"),
		mouse_press = callback(self, self, "mouse_pressed"),
		mouse_release = callback(self, self, "mouse_released"),
		id = self._id
	}

	managers.mouse_pointer:use_mouse(clbks)

	self._active = true

	return 
end
VRCustomizationGui.deactivate = function (self)
	managers.mouse_pointer:remove_mouse(self._id)

	self._active = false

	return 
end
VRCustomizationGui.exit_menu = function (self)
	for _, menu in pairs(self._sub_menus) do
		menu.clear_objects(menu)
	end

	if self._active then
		self.deactivate(self)
	end

	self._setup_gui(self)

	return 
end
VRBeltCustomization = VRBeltCustomization or class()
VRBeltCustomization.init = function (self, is_start_menu)
	local scene = (is_start_menu and World) or MenuRoom
	local player = managers.menu:player()
	self._belt_unit = World:spawn_unit(Idstring("units/pd2_dlc_vr/player/vr_hud_belt"), Vector3(0, 0, 0), Rotation())

	self._belt_unit:set_visible(false)

	self._ws = scene.gui(scene):create_world_workspace(1280, 680, Vector3(), math.X, math.Y)
	self._belt = HUDBelt:new(self._ws)

	HUDManagerVR.link_belt(self._ws, self._belt_unit)

	self._help_ws = scene.gui(scene):create_linked_workspace(256, 300, self._belt_unit:orientation_object(), Vector3(-10, 10, 24.5), math.X*20, -math.Z*24.5)
	self._help_panel = self._help_ws:panel()

	self._setup_help_panel(self, self._help_panel)

	self._height = managers.vr:get_setting("height")*managers.vr:get_setting("belt_height_ratio")
	self._start_height = self._height

	self._belt_unit:set_position((is_start_menu and Vector3(-320, 10, self._height)) or player.position(player):with_z(self._height) - Vector3(20, 0, 0))
	self._belt_unit:set_rotation(Rotation(90))
	self._belt:set_alpha(0.4)
	player._hand_state_machine:enter_hand_state(player.primary_hand_index(player), "customization")
	managers.menu:active_menu().input:focus(false)
	managers.menu:active_menu().input:focus(true)

	return 
end
VRBeltCustomization.reset = function (self, reset_value)
	if reset_value then
		self._start_height = reset_value
	end

	self._height = self._start_height

	return 
end
VRBeltCustomization._setup_help_panel = function (self, panel)
	local up_arrow = panel.bitmap(panel, {
		texture = "guis/dlcs/vr/textures/pd2/icon_belt_arrow",
		name = "up_arrow",
		texture_rect = {
			128,
			0,
			128,
			128
		}
	})

	up_arrow.set_center_x(up_arrow, panel.w(panel)/2)

	local down_arrow = panel.bitmap(panel, {
		texture = "guis/dlcs/vr/textures/pd2/icon_belt_arrow",
		name = "down_arrow",
		rotation = 180,
		texture_rect = {
			128,
			0,
			128,
			128
		}
	})

	down_arrow.set_center_x(down_arrow, panel.w(panel)/2)
	down_arrow.set_bottom(down_arrow, panel.h(panel))

	local text = panel.text(panel, {
		name = "text",
		text = managers.localization:to_upper_text("menu_vr_belt_grip"),
		font = tweak_data.hud.medium_font_noshadow,
		font_size = tweak_data.hud.default_font_size
	})

	make_fine_text(text)
	text.set_center(text, panel.w(panel)/2, panel.h(panel)/2)

	self._state = "active"

	return 
end
VRBeltCustomization._set_help_state = function (self, state)
	if state == self._state then
		return 
	end

	self._state = state
	local grip = state == "grip"
	local inactive = state == "inactive"
	local x = (not grip or 0) and 128
	local up_arrow = self._help_panel:child("up_arrow")

	up_arrow.set_texture_rect(up_arrow, x, 0, 128, 128)
	up_arrow.set_alpha(up_arrow, (inactive and 0.2) or 1)

	local down_arrow = self._help_panel:child("down_arrow")

	down_arrow.set_texture_rect(down_arrow, x, 0, 128, 128)
	down_arrow.set_alpha(down_arrow, (inactive and 0.2) or 1)

	local text = self._help_panel:child("text")

	text.set_alpha(text, (inactive and 0.5) or 1)
	text.set_text(text, managers.localization:to_upper_text((grip and "menu_vr_belt_release") or "menu_vr_belt_grip"))
	make_fine_text(text)
	text.set_center_x(text, self._help_panel:w()/2)

	return 
end
VRBeltCustomization.destroy = function (self)
	self._ws:gui():destroy_workspace(self._ws)
	self._help_ws:gui():destroy_workspace(self._help_ws)

	local player = managers.menu:player()

	player._hand_state_machine:enter_hand_state(player.primary_hand_index(player), "laser")

	if managers.menu:active_menu() then
		managers.menu:active_menu().input:focus(false)
		managers.menu:active_menu().input:focus(true)
	end

	self._belt:destroy()
	World:delete_unit(self._belt_unit)

	return 
end
VRBeltCustomization.height = function (self)
	return self._height
end
VRBeltCustomization.update = function (self, t, dt)
	local player = managers.menu:player()
	local hand = player.hand(player, player.primary_hand_index(player))
	local height_diff = hand.position(hand).z - self._belt_unit:position().z

	if mvector3.distance_sq(hand.position(hand), self._belt_unit:position()) < 1600 and math.abs(height_diff) < 15 then
		self._belt:set_alpha(0.9)

		if managers.menu:get_controller():get_input_pressed("interact") then
			self._grip_offset = height_diff
		end

		if managers.menu:get_controller():get_input_bool("interact") and self._grip_offset then
			local height = managers.vr:get_setting("height")
			local z = hand.position(hand).z - self._grip_offset
			local min, max = managers.vr:setting_limits("belt_height_ratio")

			if min and max then
				z = math.clamp(z, height*min, height*max)
			end

			self._height = z

			self._set_help_state(self, "grip")
		else
			self._set_help_state(self, "active")
		end
	else
		self._belt:set_alpha(0.4)
		self._set_help_state(self, "inactive")
	end

	self._belt_unit:set_position(player.position(player):with_z(self._height) + math.Y:rotate_with(self._belt_unit:rotation())*20)

	local hmd_rot = VRManager:hmd_rotation()
	local snap_angle = managers.vr:get_setting("belt_snap")
	local yaw_rot = Rotation(hmd_rot.yaw(hmd_rot))
	local angle = Rotation:rotation_difference(Rotation(self._belt_unit:rotation():yaw()), yaw_rot):yaw()
	angle = math.abs(angle)

	if snap_angle < angle then
		self._belt_unit:set_rotation(yaw_rot)
	end

	return 
end

return 
