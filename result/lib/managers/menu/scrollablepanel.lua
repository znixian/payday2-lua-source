ScrollablePanel = ScrollablePanel or class()
local PANEL_PADDING = 10
local FADEOUT_SPEED = 5
local SCROLL_SPEED = 28
ScrollablePanel.SCROLL_SPEED = SCROLL_SPEED
ScrollablePanel.init = function (self, parent_panel, name, data)
	data = data or {}
	self._alphas = {}
	self._x_padding = (data.x_padding ~= nil and data.x_padding) or (data.padding ~= nil and data.padding) or PANEL_PADDING
	self._y_padding = (data.y_padding ~= nil and data.y_padding) or (data.padding ~= nil and data.padding) or PANEL_PADDING
	self._force_scroll_indicators = data.force_scroll_indicators
	local layer = (data.layer ~= nil and data.layer) or 50
	data.name = data.name or (name and name .. "Base")
	self._panel = parent_panel.panel(parent_panel, data)
	self._scroll_panel = self._panel:panel({
		name = name and name .. "Scroll",
		x = self.x_padding(self),
		y = self.y_padding(self),
		w = self._panel:w() - self.x_padding(self)*2,
		h = self._panel:h() - self.y_padding(self)*2
	})
	self._canvas = self._scroll_panel:panel({
		name = name and name .. "Canvas",
		w = self._scroll_panel:w(),
		h = self._scroll_panel:h()
	})

	if data.ignore_up_indicator == nil or not data.ignore_up_indicator then
		local scroll_up_indicator_shade = self.panel(self):panel({
			halign = "right",
			name = "scroll_up_indicator_shade",
			valign = "top",
			alpha = 0,
			layer = layer,
			x = self.x_padding(self),
			y = self.y_padding(self),
			w = self.canvas(self):w()
		})

		BoxGuiObject:new(scroll_up_indicator_shade, {
			sides = {
				0,
				0,
				2,
				0
			}
		}):set_aligns("scale", "scale")
	end

	if data.ignore_down_indicator == nil or not data.ignore_down_indicator then
		local scroll_down_indicator_shade = self.panel(self):panel({
			valign = "bottom",
			name = "scroll_down_indicator_shade",
			halign = "right",
			alpha = 0,
			layer = layer,
			x = self.x_padding(self),
			y = self.y_padding(self),
			w = self.canvas(self):w(),
			h = self.panel(self):h() - self.y_padding(self)*2
		})

		BoxGuiObject:new(scroll_down_indicator_shade, {
			sides = {
				0,
				0,
				0,
				2
			}
		}):set_aligns("scale", "scale")
	end

	local texture, rect = tweak_data.hud_icons:get_icon_data("scrollbar_arrow")
	local scroll_up_indicator_arrow = self.panel(self):bitmap({
		name = "scroll_up_indicator_arrow",
		halign = "right",
		valign = "top",
		alpha = 0,
		texture = texture,
		texture_rect = rect,
		layer = layer,
		color = Color.white
	})

	scroll_up_indicator_arrow.set_top(scroll_up_indicator_arrow, self.y_padding(self) + 6)
	scroll_up_indicator_arrow.set_right(scroll_up_indicator_arrow, self.panel(self):w() - self.scrollbar_x_padding(self))

	local scroll_down_indicator_arrow = self.panel(self):bitmap({
		name = "scroll_down_indicator_arrow",
		valign = "bottom",
		alpha = 0,
		halign = "right",
		rotation = 180,
		texture = texture,
		texture_rect = rect,
		layer = layer,
		color = Color.white
	})

	scroll_down_indicator_arrow.set_bottom(scroll_down_indicator_arrow, self.panel(self):h() - self.y_padding(self) - 6)
	scroll_down_indicator_arrow.set_right(scroll_down_indicator_arrow, self.panel(self):w() - self.scrollbar_x_padding(self))

	if data.left_scrollbar then
		scroll_up_indicator_arrow.set_left(scroll_up_indicator_arrow, 2)
		scroll_down_indicator_arrow.set_left(scroll_down_indicator_arrow, 2)
	end

	local bar_h = scroll_down_indicator_arrow.top(scroll_down_indicator_arrow) - scroll_up_indicator_arrow.bottom(scroll_up_indicator_arrow)
	self._scroll_bar = self.panel(self):panel({
		name = "scroll_bar",
		halign = "right",
		w = 4,
		layer = layer - 1,
		h = bar_h
	})
	self._scroll_bar_box_class = BoxGuiObject:new(self._scroll_bar, {
		sides = {
			2,
			2,
			0,
			0
		}
	})

	self._scroll_bar_box_class:set_aligns("scale", "scale")
	self._scroll_bar:set_w(8)
	self._scroll_bar:set_bottom(scroll_down_indicator_arrow.top(scroll_down_indicator_arrow))
	self._scroll_bar:set_center_x(scroll_down_indicator_arrow.center_x(scroll_down_indicator_arrow))

	self._bar_minimum_size = data.bar_minimum_size or 5
	self._thread = self._panel:animate(self._update, self)

	return 
end
ScrollablePanel.alive = function (self)
	return alive(self.panel(self))
end
ScrollablePanel.panel = function (self)
	return self._panel
end
ScrollablePanel.scroll_panel = function (self)
	return self._scroll_panel
end
ScrollablePanel.canvas = function (self)
	return self._canvas
end
ScrollablePanel.x_padding = function (self)
	return self._x_padding
end
ScrollablePanel.y_padding = function (self)
	return self._y_padding
end
ScrollablePanel.scrollbar_x_padding = function (self)
	if self._x_padding == 0 then
		return PANEL_PADDING
	else
		return self._x_padding
	end

	return 
end
ScrollablePanel.scrollbar_y_padding = function (self)
	if self._y_padding == 0 then
		return PANEL_PADDING
	else
		return self._y_padding
	end

	return 
end
ScrollablePanel.set_pos = function (self, x, y)
	if x ~= nil then
		self.panel(self):set_x(x)
	end

	if y ~= nil then
		self.panel(self):set_y(y)
	end

	return 
end
ScrollablePanel.set_size = function (self, w, h)
	self.panel(self):set_size(w, h)
	self.scroll_panel(self):set_size(w - self.x_padding(self)*2, h - self.y_padding(self)*2)

	local scroll_up_indicator_arrow = self.panel(self):child("scroll_up_indicator_arrow")

	scroll_up_indicator_arrow.set_top(scroll_up_indicator_arrow, self.y_padding(self) + 6)
	scroll_up_indicator_arrow.set_right(scroll_up_indicator_arrow, self.panel(self):w() - self.scrollbar_x_padding(self))

	local scroll_down_indicator_arrow = self.panel(self):child("scroll_down_indicator_arrow")

	scroll_down_indicator_arrow.set_bottom(scroll_down_indicator_arrow, self.panel(self):h() - self.y_padding(self) - 6)
	scroll_down_indicator_arrow.set_right(scroll_down_indicator_arrow, self.panel(self):w() - self.scrollbar_x_padding(self))
	self._scroll_bar:set_bottom(scroll_down_indicator_arrow.top(scroll_down_indicator_arrow))
	self._scroll_bar:set_center_x(scroll_down_indicator_arrow.center_x(scroll_down_indicator_arrow))

	return 
end
ScrollablePanel.on_canvas_updated_callback = function (self, callback)
	self._on_canvas_updated = callback

	return 
end
ScrollablePanel.canvas_max_width = function (self)
	return self.scroll_panel(self):w()
end
ScrollablePanel.canvas_scroll_width = function (self)
	return self.scroll_panel(self):w() - self.x_padding(self) - 5
end
ScrollablePanel.canvas_scroll_height = function (self)
	return self.scroll_panel(self):h()
end
ScrollablePanel.update_canvas_size = function (self)
	local orig_w = self.canvas(self):w()
	local max_h = 0

	for i, panel in ipairs(self.canvas(self):children()) do
		local h = panel.y(panel) + panel.h(panel)

		if max_h < h then
			max_h = h
		end
	end

	local show_scrollbar = self.canvas_scroll_height(self) < max_h
	local max_w = (show_scrollbar and self.canvas_scroll_width(self)) or self.canvas_max_width(self)

	self.canvas(self):grow(max_w - self.canvas(self):w(), max_h - self.canvas(self):h())

	if self._on_canvas_updated then
		self._on_canvas_updated(max_w)
	end

	max_h = 0

	for i, panel in ipairs(self.canvas(self):children()) do
		local h = panel.y(panel) + panel.h(panel)

		if max_h < h then
			max_h = h
		end
	end

	if max_h <= self.scroll_panel(self):h() then
		max_h = self.scroll_panel(self):h()
	end

	self.set_canvas_size(self, nil, max_h)

	return 
end
ScrollablePanel.set_canvas_size = function (self, w, h)
	if w == nil then
		w = self.canvas(self):w()
	end

	if h == nil then
		h = self.canvas(self):h()
	end

	if h <= self.scroll_panel(self):h() then
		h = self.scroll_panel(self):h()

		self.canvas(self):set_y(0)
	end

	self.canvas(self):set_size(w, h)

	local show_scrollbar = self.scroll_panel(self):h() < h

	if not show_scrollbar then
		self._scroll_bar:set_alpha(0)
		self._scroll_bar:set_visible(false)
		self._scroll_bar_box_class:hide()
		self.set_element_alpha_target(self, "scroll_up_indicator_arrow", 0, 100)
		self.set_element_alpha_target(self, "scroll_down_indicator_arrow", 0, 100)
		self.set_element_alpha_target(self, "scroll_up_indicator_shade", 0, 100)
		self.set_element_alpha_target(self, "scroll_down_indicator_shade", 0, 100)
	else
		self._scroll_bar:set_alpha(1)
		self._scroll_bar:set_visible(true)
		self._scroll_bar_box_class:show()
		self._set_scroll_indicator(self)
		self._check_scroll_indicator_states(self)
	end

	return 
end
ScrollablePanel.set_element_alpha_target = function (self, element, target, speed)
	local element_name = (type(element) == "string" and element) or element.name(element)
	self._alphas[element_name] = {
		current = (self._alphas[element_name] and self._alphas[element_name].current) or (element.alpha and element.alpha(element)) or 1,
		target = target,
		speed = speed or (self._alphas[element_name] and self._alphas[element_name].speed) or 5
	}

	return 
end
ScrollablePanel.is_scrollable = function (self)
	return self.scroll_panel(self):h() < self.canvas(self):h()
end
ScrollablePanel.scroll = function (self, x, y, direction)
	if self.panel(self):inside(x, y) then
		self.perform_scroll(self, SCROLL_SPEED*TimerManager:main():delta_time()*200, direction)

		return true
	end

	return 
end
ScrollablePanel.perform_scroll = function (self, speed, direction)
	if self.canvas(self):h() <= self.scroll_panel(self):h() then
		return 
	end

	local scroll_amount = speed*direction
	local max_h = self.canvas(self):h() - self.scroll_panel(self):h()
	max_h = max_h*-1
	local new_y = math.clamp(self.canvas(self):y() + scroll_amount, max_h, 0)

	self.canvas(self):set_y(new_y)
	self._set_scroll_indicator(self)
	self._check_scroll_indicator_states(self)

	return 
end
ScrollablePanel.scroll_to = function (self, y)
	if self.canvas(self):h() <= self.scroll_panel(self):h() then
		return 
	end

	local scroll_amount = -y
	local max_h = self.canvas(self):h() - self.scroll_panel(self):h()
	max_h = max_h*-1
	local new_y = math.clamp(scroll_amount, max_h, 0)

	self.canvas(self):set_y(new_y)
	self._set_scroll_indicator(self)
	self._check_scroll_indicator_states(self)

	return 
end
ScrollablePanel.scroll_with_bar = function (self, target_y, current_y)
	local arrow_size = self.panel(self):child("scroll_up_indicator_arrow"):size()
	local scroll_panel = self.scroll_panel(self)
	local canvas = self.canvas(self)

	if target_y < current_y then
		if target_y < scroll_panel.world_bottom(scroll_panel) - arrow_size then
			local mul = (scroll_panel.h(scroll_panel) - arrow_size*2)/canvas.h(canvas)

			self.perform_scroll(self, (current_y - target_y)/mul, 1)
		end

		current_y = target_y
	elseif current_y < target_y then
		if scroll_panel.world_y(scroll_panel) + arrow_size < target_y then
			local mul = (scroll_panel.h(scroll_panel) - arrow_size*2)/canvas.h(canvas)

			self.perform_scroll(self, (target_y - current_y)/mul, -1)
		end

		current_y = target_y
	end

	return 
end
ScrollablePanel.release_scroll_bar = function (self)
	self._pressing_arrow_up = false
	self._pressing_arrow_down = false

	if self._grabbed_scroll_bar then
		self._grabbed_scroll_bar = false

		return true
	end

	return 
end
ScrollablePanel._set_scroll_indicator = function (self)
	local bar_h = self.panel(self):child("scroll_down_indicator_arrow"):top() - self.panel(self):child("scroll_up_indicator_arrow"):bottom()

	if self.canvas(self):h() ~= 0 then
		self._scroll_bar:set_h(math.max((bar_h*self.scroll_panel(self):h())/self.canvas(self):h(), self._bar_minimum_size))
	end

	return 
end
ScrollablePanel._check_scroll_indicator_states = function (self)
	local up_alpha = (self.canvas(self):top() < 0 and 1) or 0
	local down_alpha = (self.scroll_panel(self):h() < self.canvas(self):bottom() and 1) or 0

	self.set_element_alpha_target(self, "scroll_up_indicator_arrow", up_alpha, FADEOUT_SPEED)
	self.set_element_alpha_target(self, "scroll_down_indicator_arrow", down_alpha, FADEOUT_SPEED)

	if 0 < self.y_padding(self) or self._force_scroll_indicators then
		self.set_element_alpha_target(self, "scroll_up_indicator_shade", up_alpha, FADEOUT_SPEED)
		self.set_element_alpha_target(self, "scroll_down_indicator_shade", down_alpha, FADEOUT_SPEED)
	end

	local up_arrow = self.panel(self):child("scroll_up_indicator_arrow")
	local down_arrow = self.panel(self):child("scroll_down_indicator_arrow")
	local canvas_h = (self.canvas(self):h() ~= 0 and self.canvas(self):h()) or 1
	local at = self.canvas(self):top()/(self.scroll_panel(self):h() - canvas_h)
	local max = down_arrow.top(down_arrow) - up_arrow.bottom(up_arrow) - self._scroll_bar:h()

	self._scroll_bar:set_top(up_arrow.bottom(up_arrow) + max*at)

	return 
end
ScrollablePanel._update = function (o, self)
	while true do
		local dt = coroutine.yield()

		for element_name, data in pairs(self._alphas) do
			data.current = math.step(data.current, data.target, dt*data.speed)
			local element = self.panel(self):child(element_name)

			if alive(element) then
				element.set_alpha(element, data.current)
			end
		end
	end

	return 
end
ScrollablePanel.mouse_moved = function (self, button, x, y)
	if self._grabbed_scroll_bar then
		self.scroll_with_bar(self, y, self._current_y)

		self._current_y = y

		return true, "grab"
	elseif alive(self._scroll_bar) and self._scroll_bar:visible() and self._scroll_bar:inside(x, y) then
		return true, "hand"
	elseif self.panel(self):child("scroll_up_indicator_arrow"):inside(x, y) then
		if self._pressing_arrow_up then
			self.perform_scroll(self, SCROLL_SPEED*0.1, 1)
		end

		return true, "link"
	elseif self.panel(self):child("scroll_down_indicator_arrow"):inside(x, y) then
		if self._pressing_arrow_down then
			self.perform_scroll(self, SCROLL_SPEED*0.1, -1)
		end

		return true, "link"
	end

	return 
end
ScrollablePanel.mouse_clicked = function (self, o, button, x, y)
	if alive(self._scroll_bar) and self._scroll_bar:visible() and self._scroll_bar:inside(x, y) then
		return true
	end

	return 
end
ScrollablePanel.mouse_pressed = function (self, button, x, y)
	if alive(self._scroll_bar) and self._scroll_bar:visible() and self._scroll_bar:inside(x, y) then
		self._grabbed_scroll_bar = true
		self._current_y = y

		return true
	elseif self.panel(self):child("scroll_up_indicator_arrow"):inside(x, y) then
		self._pressing_arrow_up = true

		return true
	elseif self.panel(self):child("scroll_down_indicator_arrow"):inside(x, y) then
		self._pressing_arrow_down = true

		return true
	end

	return 
end
ScrollablePanel.mouse_released = function (self, button, x, y)
	return self.release_scroll_bar(self)
end

return 
