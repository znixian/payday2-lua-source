core:import("CoreMenuNode")
core:import("CoreSerialize")
core:import("CoreMenuItem")
core:import("CoreMenuItemToggle")

MenuNodeServerList = MenuNodeServerList or class(MenuNodeTable)
MenuNodeServerList.init = function (self, data_node)
	MenuNodeServerList.super.init(self, data_node)

	return 
end
MenuNodeServerList.update = function (self, t, dt)
	MenuNodeServerList.super.update(self, t, dt)

	return 
end
MenuNodeServerList._setup_columns = function (self)
	self._add_column(self, {
		proportions = 1.9,
		align = "left",
		text = string.upper("")
	})
	self._add_column(self, {
		proportions = 1.7,
		align = "right",
		text = string.upper("")
	})
	self._add_column(self, {
		proportions = 1,
		align = "right",
		text = string.upper("")
	})
	self._add_column(self, {
		proportions = 0.225,
		align = "right",
		text = string.upper("")
	})

	return 
end

return 
