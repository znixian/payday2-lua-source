core:import("CoreMenuNode")
core:import("CoreSerialize")
core:import("CoreMenuItem")
core:import("CoreMenuItemToggle")

MenuNodeTable = MenuNodeTable or class(CoreMenuNode.MenuNode)
MenuNodeTable.init = function (self, data_node)
	MenuNodeTable.super.init(self, data_node)

	self._columns = {}

	self._setup_columns(self)

	self._parameters.total_proportions = 0

	for _, data in ipairs(self._columns) do
		self._parameters.total_proportions = self._parameters.total_proportions + data.proportions
	end

	return 
end
MenuNodeTable._setup_columns = function (self)
	return 
end
MenuNodeTable._add_column = function (self, params)
	table.insert(self._columns, params)

	return 
end
MenuNodeTable.columns = function (self)
	return self._columns
end

return 
