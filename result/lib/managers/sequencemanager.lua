core:module("SequenceManager")
core:import("CoreSequenceManager")
core:import("CoreClass")

SequenceManager = SequenceManager or class(CoreSequenceManager.SequenceManager)
SequenceManager.init = function (self)
	SequenceManager.super.init(self, managers.slot:get_mask("body_area_damage"), managers.slot:get_mask("area_damage_blocker"), managers.slot:get_mask("unit_area_damage"))
	self.register_event_element_class(self, InteractionElement)

	self._proximity_masks.players = managers.slot:get_mask("players")

	return 
end
InteractionElement = InteractionElement or class(CoreSequenceManager.BaseElement)
InteractionElement.NAME = "interaction"
InteractionElement.init = function (self, node, unit_element)
	InteractionElement.super.init(self, node, unit_element)

	self._enabled = self.get(self, "enabled")

	return 
end
InteractionElement.activate_callback = function (self, env)
	local enabled = self.run_parsed_func(self, env, self._enabled)

	if env.dest_unit:interaction() then
		env.dest_unit:interaction():set_active(enabled)
	else
		Application:error("Unit " .. tostring(env.dest_unit:name()) .. " doesn't have the interaction extension.")
	end

	return 
end

CoreClass.override_class(CoreSequenceManager.SequenceManager, SequenceManager)

return 
