core:module("ControllerManager")
core:import("CoreControllerManager")
core:import("CoreClass")

ControllerManager = ControllerManager or class(CoreControllerManager.ControllerManager)
ControllerManager.init = function (self, path, default_settings_path)
	default_settings_path = "settings/controller_settings"
	path = default_settings_path
	self._menu_mode_enabled = 0

	ControllerManager.super.init(self, path, default_settings_path)

	return 
end
ControllerManager.update = function (self, t, dt)
	ControllerManager.super.update(self, t, dt)
	self._poll_reconnected_controller(self)

	return 
end
ControllerManager._poll_reconnected_controller = function (self)
	if SystemInfo:platform() == Idstring("XB1") and Global.controller_manager.connect_controller_dialog_visible then
		local active_xuid = XboxLive:current_user()
		local nr_controllers = Input:num_controllers()

		for i_controller = 0, nr_controllers - 1, 1 do
			local controller = Input:controller(i_controller)

			if controller.type(controller) == "xb1_controller" and (controller.down(controller, 12) or controller.pressed(controller, 12)) and controller.user_xuid(controller) == active_xuid then
				self._close_controller_changed_dialog(self)
				self.replace_active_controller(self, i_controller, controller)
			end
		end
	end

	return 
end
ControllerManager.controller_mod_changed = function (self)
	if not Global.controller_manager.user_mod then
		Global.controller_manager.user_mod = managers.user:get_setting("controller_mod")

		self.load_user_mod(self)
	end

	return 
end
ControllerManager.set_user_mod = function (self, connection_name, params)
	Global.controller_manager.user_mod = Global.controller_manager.user_mod or {}

	if params.axis then
		Global.controller_manager.user_mod[connection_name] = Global.controller_manager.user_mod[connection_name] or {
			axis = params.axis
		}
		Global.controller_manager.user_mod[connection_name][params.button] = params
	else
		Global.controller_manager.user_mod[connection_name] = params
	end

	managers.user:set_setting("controller_mod_type", managers.controller:get_default_wrapper_type())
	managers.user:set_setting("controller_mod", Global.controller_manager.user_mod, true)

	return 
end
ControllerManager.clear_user_mod = function (self, category, CONTROLS_INFO)
	Global.controller_manager.user_mod = Global.controller_manager.user_mod or {}
	local names = table.map_keys(Global.controller_manager.user_mod)

	for _, name in ipairs(names) do
		if CONTROLS_INFO[name] and CONTROLS_INFO[name].category == category then
			Global.controller_manager.user_mod[name] = nil
		end
	end

	managers.user:set_setting("controller_mod_type", managers.controller:get_default_wrapper_type())
	managers.user:set_setting("controller_mod", Global.controller_manager.user_mod, true)
	self.load_user_mod(self)

	return 
end
ControllerManager.load_user_mod = function (self)
	if Global.controller_manager.user_mod then
		local connections = managers.controller:get_settings(managers.user:get_setting("controller_mod_type")):get_connection_map()

		for connection_name, params in pairs(Global.controller_manager.user_mod) do
			if params.axis and connections[params.axis] then
				for button, button_params in pairs(params) do
					if type(button_params) == "table" and button_params.button and connections[params.axis]._btn_connections[button_params.button] then
						connections[params.axis]._btn_connections[button_params.button].name = button_params.connection
					end
				end
			elseif params.button and connections[params.button] then
				connections[params.button]:set_controller_id(params.controller_id)
				connections[params.button]:set_input_name_list({
					params.connection
				})
			end
		end

		self.rebind_connections(self)
	end

	return 
end
ControllerManager.init_finalize = function (self)
	managers.user:add_setting_changed_callback("controller_mod", callback(self, self, "controller_mod_changed"), true)

	if Global.controller_manager.user_mod then
		self.load_user_mod(self)
	end

	self._check_dialog(self)

	return 
end
ControllerManager.default_controller_connect_change = function (self, connected)
	ControllerManager.super.default_controller_connect_change(self, connected)

	if Global.controller_manager.default_wrapper_index and not connected and not self._controller_changed_dialog_active(self) then
		self._show_controller_changed_dialog(self)
	end

	return 
end
ControllerManager._check_dialog = function (self)
	if Global.controller_manager.connect_controller_dialog_visible and not self._controller_changed_dialog_active(self) then
		self._show_controller_changed_dialog(self)
	end

	return 
end
ControllerManager._controller_changed_dialog_active = function (self)
	return (managers.system_menu:is_active_by_id("connect_controller_dialog") and true) or false
end
ControllerManager._show_controller_changed_dialog = function (self)
	if self._controller_changed_dialog_active(self) then
		return 
	end

	Global.controller_manager.connect_controller_dialog_visible = true
	local data = {
		callback_func = callback(self, self, "connect_controller_dialog_callback"),
		title = managers.localization:text("dialog_connect_controller_title"),
		text = managers.localization:text("dialog_connect_controller_text", {
			NR = Global.controller_manager.default_wrapper_index or 1
		})
	}

	if SystemInfo:platform() == Idstring("XB1") then
		data.no_buttons = true
	else
		data.button_list = {
			{
				text = managers.localization:text("dialog_ok")
			}
		}
	end

	data.id = "connect_controller_dialog"
	data.force = true

	managers.system_menu:show(data)

	return 
end
ControllerManager._change_mode = function (self, mode)
	self.change_default_wrapper_mode(self, mode)

	return 
end
ControllerManager.set_menu_mode_enabled = function (self, enabled)
	if SystemInfo:platform() == Idstring("WIN32") then
		self._menu_mode_enabled = self._menu_mode_enabled or 0
		self._menu_mode_enabled = self._menu_mode_enabled + ((enabled and 1) or -1)

		if self.get_menu_mode_enabled(self) then
			self._change_mode(self, "menu")
		else
			self.set_ingame_mode(self)
		end

		if self._menu_mode_enabled < 0 then
			Application:error("[ControllerManager:set_menu_mode_enabled] Controller menu mode counter reached negative refs!")
		end
	end

	return 
end
ControllerManager.get_menu_mode_enabled = function (self)
	return self._menu_mode_enabled and 0 < self._menu_mode_enabled
end
ControllerManager.set_ingame_mode = function (self, mode)
	if SystemInfo:platform() == Idstring("WIN32") then
		if mode then
			self._ingame_mode = mode
		end

		if not self.get_menu_mode_enabled(self) then
			self._change_mode(self, self._ingame_mode)
		end
	end

	return 
end
ControllerManager._close_controller_changed_dialog = function (self, hard)
	if Global.controller_manager.connect_controller_dialog_visible or self._controller_changed_dialog_active(self) then
		print("[ControllerManager:_close_controller_changed_dialog] closing")
		managers.system_menu:close("connect_controller_dialog", hard)
		self.connect_controller_dialog_callback(self)
	end

	return 
end
ControllerManager.connect_controller_dialog_callback = function (self)
	Global.controller_manager.connect_controller_dialog_visible = nil

	return 
end
ControllerManager.get_mouse_controller = function (self)
	local index = Global.controller_manager.default_wrapper_index or self.get_preferred_default_wrapper_index(self)
	local wrapper_class = self._wrapper_class_map and self._wrapper_class_map[index]

	if wrapper_class and wrapper_class.TYPE == "steam" then
		local controller_index = self._wrapper_to_controller_list[index][1]
		local controller = Input:controller(controller_index)
		local index = Global.controller_manager.default_wrapper_index or self.get_preferred_default_wrapper_index(self)
		local wrapper_class = self._wrapper_class_map[index]

		return controller
	end

	return Input:mouse()
end
ControllerManager.get_vr_wrapper_index = function (self)
	for index = 1, self._wrapper_count, 1 do
		local wrapper_class = self._wrapper_class_map and self._wrapper_class_map[index]

		if wrapper_class and wrapper_class.TYPE == "vr" then
			return index
		end
	end

	return 
end
ControllerManager.get_vr_controller = function (self)
	for index = 1, self._wrapper_count, 1 do
		local wrapper_class = self._wrapper_class_map and self._wrapper_class_map[index]

		if wrapper_class and wrapper_class.TYPE == "vr" then
			local controller_index = self._wrapper_to_controller_list[index][1]
			local controller = Input:controller(controller_index)

			return controller
		end
	end

	return 
end

CoreClass.override_class(CoreControllerManager.ControllerManager, ControllerManager)

return 
