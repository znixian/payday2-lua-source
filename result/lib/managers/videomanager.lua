VideoManager = VideoManager or class()
VideoManager.init = function (self)
	self._videos = {}

	return 
end
VideoManager.add_video = function (self, video)
	local volume = managers.user:get_setting("sfx_volume")
	local percentage = (volume - tweak_data.menu.MIN_SFX_VOLUME)/(tweak_data.menu.MAX_SFX_VOLUME - tweak_data.menu.MIN_SFX_VOLUME)

	video.set_volume_gain(video, percentage)
	table.insert(self._videos, video)

	return 
end
VideoManager.remove_video = function (self, video)
	table.delete(self._videos, video)

	return 
end
VideoManager.volume_changed = function (self, volume)
	for _, video in ipairs(self._videos) do
		video.set_volume_gain(video, volume)
	end

	return 
end

return 
