core:import("CoreEvent")

HUDManager._animate_test_circle = function (self)
	local t = 2

	while 0 < t do
		local dt = coroutine.yield()
		t = t - dt
	end

	print("done")

	return 
end
HUDManager._animate_ammo_test = function (self, panel)
	local t = 3

	panel.set_alpha(panel, 1)

	while 0 < t do
		local dt = coroutine.yield()
		t = t - dt

		if t < 2 then
			panel.set_alpha(panel, t/2)
		end
	end

	panel.set_alpha(panel, 0)

	return 
end

return 
