UpgradesManager = UpgradesManager or class()
UpgradesManager.AQUIRE_STRINGS = {
	"Default",
	"SkillTree",
	"SpecializationTree",
	"LevelTree",
	"DLC"
}
UpgradesManager.init = function (self)
	self._setup(self)

	return 
end
UpgradesManager._setup = function (self)
	if not Global.upgrades_manager then
		Global.upgrades_manager = {
			aquired = {},
			automanage = false,
			progress = {
				0,
				0,
				0,
				0
			},
			target_tree = self._autochange_tree(self),
			disabled_visual_upgrades = {}
		}
	end

	self._global = Global.upgrades_manager

	return 
end
UpgradesManager.setup_current_weapon = function (self)
	return 
end
UpgradesManager.visual_weapon_upgrade_active = function (self, upgrade)
	return not self._global.disabled_visual_upgrades[upgrade]
end
UpgradesManager.toggle_visual_weapon_upgrade = function (self, upgrade)
	if self._global.disabled_visual_upgrades[upgrade] then
		self._global.disabled_visual_upgrades[upgrade] = nil
	else
		self._global.disabled_visual_upgrades[upgrade] = true
	end

	return 
end
UpgradesManager.set_target_tree = function (self, tree)
	local level = managers.experience:current_level()
	local step = self._global.progress[tree]
	local cap = tweak_data.upgrades.tree_caps[self._global.progress[tree] + 1]

	if cap and level < cap then
		return 
	end

	self._set_target_tree(self, tree)

	return 
end
UpgradesManager._set_target_tree = function (self, tree)
	local i = self._global.progress[tree] + 1
	local upgrade = tweak_data.upgrades.definitions[tweak_data.upgrades.progress[tree][i]]
	self._global.target_tree = tree

	return 
end
UpgradesManager.current_tree_name = function (self)
	return self.tree_name(self, self._global.target_tree)
end
UpgradesManager.tree_name = function (self, tree)
	return managers.localization:text(tweak_data.upgrades.trees[tree].name_id)
end
UpgradesManager.tree_allowed = function (self, tree, level)
	level = level or managers.experience:current_level()
	local cap = tweak_data.upgrades.tree_caps[self._global.progress[tree] + 1]

	return not cap or level >= cap, cap
end
UpgradesManager.current_tree = function (self)
	return self._global.target_tree
end
UpgradesManager.next_upgrade = function (self, tree)
	return 
end
UpgradesManager.level_up = function (self)
	local level = managers.experience:current_level()

	print("UpgradesManager:level_up()", level)
	self.aquire_from_level_tree(self, level, false)

	return 
end
UpgradesManager.aquire_from_level_tree = function (self, level, loading)
	local tree_data = tweak_data.upgrades.level_tree[level]

	if not tree_data then
		return 
	end

	local identifier = UpgradesManager.AQUIRE_STRINGS[4] .. tostring(level)

	for _, upgrade in ipairs(tree_data.upgrades) do
		if not self.aquired(self, upgrade, identifier) then
			self.aquire(self, upgrade, loading, identifier)
		end
	end

	return 
end
UpgradesManager.verify_level_tree = function (self, level, loading)
	local tree_data = tweak_data.upgrades.level_tree[level]

	if not tree_data then
		return 
	end

	local identifier = UpgradesManager.AQUIRE_STRINGS[4] .. tostring(level)
	local upgrade = nil

	for _, id in ipairs(tree_data.upgrades) do
		upgrade = tweak_data.upgrades.definitions[id]

		if upgrade and upgrade.dlc and not managers.dlc:is_dlc_unlocked(upgrade.dlc) and self.aquired(self, id, identifier) then
			self.unaquire(self, id, identifier)
		end
	end

	return 
end
UpgradesManager._next_tree = function (self)
	local tree = nil

	if self._global.automanage then
		tree = self._autochange_tree(self)
	end

	local level = managers.experience:current_level() + 1
	local cap = tweak_data.upgrades.tree_caps[self._global.progress[self._global.target_tree] + 1]

	if cap and level < cap then
		tree = self._autochange_tree(self, self._global.target_tree)
	end

	return tree or self._global.target_tree
end
UpgradesManager.num_trees = function (self)
	return (managers.dlc:is_dlc_unlocked("preorder") and 4) or 3
end
UpgradesManager._autochange_tree = function (self, exlude_tree)
	local progress = clone(Global.upgrades_manager.progress)

	if exlude_tree then
		progress[exlude_tree] = nil
	end

	if not managers.dlc:is_dlc_unlocked("preorder") then
		progress[4] = nil
	end

	local n_tree = 0
	local n_v = 100

	for tree, v in pairs(progress) do
		if v < n_v then
			n_tree = tree
			n_v = v
		end
	end

	return n_tree
end
UpgradesManager.aquired = function (self, id, identifier)
	if identifier then
		local identify_key = Idstring(identifier):key()

		return self._global.aquired[id] and not not self._global.aquired[id][identify_key]
	else
		local count = 0
		slot4 = pairs
		slot5 = self._global.aquired[id] or {}

		for key, aquired in slot4(slot5) do
			if aquired then
				count = count + 1
			end
		end

		return 0 < count
	end

	return 
end
UpgradesManager.aquire_default = function (self, id, identifier)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to aquire an upgrade that doesn't exist: " .. (id or "nil") .. "")

		return 
	end

	local upgrade = tweak_data.upgrades.definitions[id]

	if upgrade.dlc and not managers.dlc:is_dlc_unlocked(upgrade.dlc) then
		Application:error("Tried to aquire an upgrade locked to a dlc you do not have: " .. id .. " DLC: ", upgrade.dlc)

		return 
	end

	if not identifier then
		debug_pause(identifier, "[UpgradesManager:aquire_default] No identifier for upgrade aquire", "id", id)

		identifier = UpgradesManager.AQUIRE_STRINGS[1]
	end

	local identify_key = Idstring(identifier):key()

	if self._global.aquired[id] and self._global.aquired[id][identify_key] then
		Application:error("Tried to aquire an upgrade that has already been aquired: " .. id, "identifier", identifier, "id_key", identify_key)
		Application:stack_dump()

		return 
	end

	self._global.aquired[id] = self._global.aquired[id] or {}
	self._global.aquired[id][identify_key] = identifier
	local upgrade = tweak_data.upgrades.definitions[id]

	self._aquire_upgrade(self, upgrade, id, true)

	return 
end
UpgradesManager.enable_weapon = function (self, id, identifier)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to aquire an upgrade that doesn't exist: " .. (id or "nil") .. "")

		return 
	end

	local upgrade = tweak_data.upgrades.definitions[id]

	if upgrade.dlc and not managers.dlc:is_dlc_unlocked(upgrade.dlc) then
		Application:error("Tried to aquire an upgrade locked to a dlc you do not have: " .. id .. " DLC: ", upgrade.dlc)

		return 
	end

	if not identifier then
		debug_pause(identifier, "[UpgradesManager:aquire_default] No identifier for upgrade aquire", "id", id)

		identifier = UpgradesManager.AQUIRE_STRINGS[1]
	end

	local identify_key = Idstring(identifier):key()

	if self._global.aquired[id] and self._global.aquired[id][identify_key] then
		Application:error("Tried to aquire an upgrade that has already been aquired: " .. id, "identifier", identifier, "id_key", identify_key)
		Application:stack_dump()

		return 
	end

	self._global.aquired[id] = self._global.aquired[id] or {}
	self._global.aquired[id][identify_key] = identifier

	managers.player:aquire_weapon(upgrade, id, UpgradesManager.AQUIRE_STRINGS[1])

	return 
end
UpgradesManager.aquire = function (self, id, loading, identifier)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to aquire an upgrade that doesn't exist: " .. (id or "nil") .. "")

		return 
	end

	local upgrade = tweak_data.upgrades.definitions[id]

	if upgrade.dlc and not managers.dlc:is_dlc_unlocked(upgrade.dlc) then
		Application:error("Tried to aquire an upgrade locked to a dlc you do not have: " .. id .. " DLC: ", upgrade.dlc)

		return 
	end

	if not identifier then
		debug_pause(identifier, "[UpgradesManager:aquire] No identifier for upgrade aquire", "id", id, "loading", loading)

		identifier = UpgradesManager.AQUIRE_STRINGS[1]
	end

	local identify_key = Idstring(identifier):key()

	if self._global.aquired[id] and self._global.aquired[id][identify_key] then
		Application:error("Tried to aquire an upgrade that has already been aquired: " .. id, "identifier", identifier, "id_key", identify_key)
		Application:stack_dump()

		return 
	end

	self._global.aquired[id] = self._global.aquired[id] or {}
	self._global.aquired[id][identify_key] = identifier

	self._aquire_upgrade(self, upgrade, id, loading)
	self.setup_current_weapon(self)

	return 
end
UpgradesManager.unaquire = function (self, id, identifier)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to unaquire an upgrade that doesn't exist: " .. (id or "nil") .. "")

		return 
	end

	if not identifier then
		debug_pause(identifier, "[UpgradesManager:unaquire] No identifier for upgrade aquire", "id", id)

		identifier = UpgradesManager.AQUIRE_STRINGS[1]
	end

	local identify_key = Idstring(identifier):key()

	if not self._global.aquired[id] or not self._global.aquired[id][identify_key] then
		Application:error("Tried to unaquire an upgrade that hasn't benn aquired: " .. id, "identifier", identifier)

		return 
	end

	self._global.aquired[id][identify_key] = nil
	local count = 0

	for key, aquired in pairs(self._global.aquired[id]) do
		count = count + 1
	end

	if count == 0 then
		self._global.aquired[id] = nil
		local upgrade = tweak_data.upgrades.definitions[id]

		self._unaquire_upgrade(self, upgrade, id)
	end

	return 
end
UpgradesManager._aquire_upgrade = function (self, upgrade, id, loading)
	if upgrade.category == "weapon" then
		self._aquire_weapon(self, upgrade, id, loading)
	elseif upgrade.category == "feature" then
		self._aquire_feature(self, upgrade, id, loading)
	elseif upgrade.category == "equipment" then
		self._aquire_equipment(self, upgrade, id, loading)
	elseif upgrade.category == "equipment_upgrade" then
		self._aquire_equipment_upgrade(self, upgrade, id, loading)
	elseif upgrade.category == "temporary" then
		self._aquire_temporary(self, upgrade, id, loading)
	elseif upgrade.category == "cooldown" then
		self._aquire_cooldown(self, upgrade, id, loading)
	elseif upgrade.category == "team" then
		self._aquire_team(self, upgrade, id, loading)
	elseif upgrade.category == "armor" then
		self._aquire_armor(self, upgrade, id, loading)
	elseif upgrade.category == "rep_upgrade" then
		self._aquire_rep_upgrade(self, upgrade, id, loading)
	elseif upgrade.category == "melee_weapon" then
		self._aquire_melee_weapon(self, upgrade, id, loading)
	elseif upgrade.category == "grenade" then
		self._aquire_grenade(self, upgrade, id, loading)
	end

	return 
end
UpgradesManager._unaquire_upgrade = function (self, upgrade, id)
	if upgrade.category == "weapon" then
		self._unaquire_weapon(self, upgrade, id)
	elseif upgrade.category == "feature" then
		self._unaquire_feature(self, upgrade, id)
	elseif upgrade.category == "equipment" then
		self._unaquire_equipment(self, upgrade, id)
	elseif upgrade.category == "equipment_upgrade" then
		self._unaquire_equipment_upgrade(self, upgrade, id)
	elseif upgrade.category == "temporary" then
		self._unaquire_temporary(self, upgrade, id)
	elseif upgrade.category == "cooldown" then
		self._unaquire_cooldown(self, upgrade, id)
	elseif upgrade.category == "team" then
		self._unaquire_team(self, upgrade, id)
	elseif upgrade.category == "armor" then
		self._unaquire_armor(self, upgrade, id)
	elseif upgrade.category == "melee_weapon" then
		self._unaquire_melee_weapon(self, upgrade, id)
	elseif upgrade.category == "grenade" then
		self._unaquire_grenade(self, upgrade, id)
	end

	return 
end
UpgradesManager._aquire_weapon = function (self, upgrade, id, loading)
	managers.player:aquire_weapon(upgrade, id)
	managers.blackmarket:on_aquired_weapon_platform(upgrade, id, loading)

	return 
end
UpgradesManager._unaquire_weapon = function (self, upgrade, id)
	managers.player:unaquire_weapon(upgrade, id)
	managers.blackmarket:on_unaquired_weapon_platform(upgrade, id)

	return 
end
UpgradesManager._aquire_melee_weapon = function (self, upgrade, id, loading)
	managers.player:aquire_melee_weapon(upgrade, id)
	managers.blackmarket:on_aquired_melee_weapon(upgrade, id, loading)

	return 
end
UpgradesManager._unaquire_melee_weapon = function (self, upgrade, id)
	managers.player:unaquire_melee_weapon(upgrade, id)
	managers.blackmarket:on_unaquired_melee_weapon(upgrade, id)

	return 
end
UpgradesManager._aquire_grenade = function (self, upgrade, id, loading)
	managers.player:aquire_grenade(upgrade, id)
	managers.blackmarket:on_aquired_grenade(upgrade, id, loading)

	return 
end
UpgradesManager._unaquire_grenade = function (self, upgrade, id)
	managers.player:unaquire_grenade(upgrade, id)
	managers.blackmarket:on_unaquired_grenade(upgrade, id)

	return 
end
UpgradesManager._aquire_feature = function (self, feature)
	if feature.incremental then
		managers.player:aquire_incremental_upgrade(feature.upgrade)
	else
		managers.player:aquire_upgrade(feature.upgrade)
	end

	return 
end
UpgradesManager._unaquire_feature = function (self, feature)
	if feature.incremental then
		managers.player:unaquire_incremental_upgrade(feature.upgrade)
	else
		managers.player:unaquire_upgrade(feature.upgrade)
	end

	return 
end
UpgradesManager._aquire_equipment = function (self, equipment, id, loading)
	managers.player:aquire_equipment(equipment, id, loading)

	return 
end
UpgradesManager._unaquire_equipment = function (self, equipment, id)
	managers.player:unaquire_equipment(equipment, id)

	return 
end
UpgradesManager._aquire_equipment_upgrade = function (self, equipment_upgrade)
	if equipment_upgrade.incremental then
		managers.player:aquire_incremental_upgrade(equipment_upgrade.upgrade)
	else
		managers.player:aquire_upgrade(equipment_upgrade.upgrade)
	end

	return 
end
UpgradesManager._unaquire_equipment_upgrade = function (self, equipment_upgrade)
	if equipment_upgrade.incremental then
		managers.player:unaquire_incremental_upgrade(equipment_upgrade.upgrade)
	else
		managers.player:unaquire_upgrade(equipment_upgrade.upgrade)
	end

	return 
end
UpgradesManager._aquire_temporary = function (self, temporary, id)
	if temporary.incremental then
		managers.player:aquire_incremental_upgrade(temporary.upgrade)
	else
		managers.player:aquire_upgrade(temporary.upgrade, id)
	end

	return 
end
UpgradesManager._unaquire_temporary = function (self, temporary, id)
	if temporary.incremental then
		managers.player:unaquire_incremental_upgrade(temporary.upgrade)
	else
		managers.player:unaquire_upgrade(temporary.upgrade)
	end

	return 
end
UpgradesManager._aquire_cooldown = function (self, cooldown, id)
	managers.player:aquire_cooldown_upgrade(cooldown.upgrade, id)

	return 
end
UpgradesManager._unaquire_cooldown = function (self, cooldown, id)
	managers.player:unaquire_cooldown_upgrade(cooldown.upgrade)

	return 
end
UpgradesManager._aquire_team = function (self, team, id)
	managers.player:aquire_team_upgrade(team.upgrade, id)

	return 
end
UpgradesManager._unaquire_team = function (self, team, id)
	managers.player:unaquire_team_upgrade(team.upgrade, id)

	return 
end
UpgradesManager._aquire_armor = function (self, upgrade, id, loading)
	managers.blackmarket:on_aquired_armor(upgrade, id, loading)

	return 
end
UpgradesManager._unaquire_armor = function (self, upgrade, id)
	managers.blackmarket:on_unaquired_armor(upgrade, id)

	return 
end
UpgradesManager._aquire_rep_upgrade = function (self, upgrade, id)
	managers.skilltree:rep_upgrade(upgrade, id)

	return 
end
UpgradesManager.get_value = function (self, upgrade_id, ...)
	local upgrade = tweak_data.upgrades.definitions[upgrade_id]

	if not upgrade then
		Application:error("[UpgradesManager:get_value] Missing Upgrade ID: ", upgrade_id)
	end

	local u = upgrade.upgrade

	if upgrade.category == "feature" then
		return tweak_data.upgrades.values[u.category][u.upgrade][u.value]
	elseif upgrade.category == "equipment" then
		return upgrade.equipment_id
	elseif upgrade.category == "equipment_upgrade" then
		return tweak_data.upgrades.values[u.category][u.upgrade][u.value]
	elseif upgrade.category == "temporary" then
		local temporary = tweak_data.upgrades.values[u.category][u.upgrade][u.value]

		return "Value: " .. tostring(temporary[1]) .. " Time: " .. temporary[2]
	elseif upgrade.category == "cooldown" then
		local cooldown = tweak_data.upgrades.values[u.category][u.upgrade][u.value]

		return "Value: " .. tostring(cooldown[1]) .. " Time: " .. cooldown[2]
	elseif upgrade.category == "team" then
		local value = tweak_data.upgrades.values.team[u.category][u.upgrade][u.value]

		return value
	elseif upgrade.category == "weapon" then
		local default_weapons = {
			"glock_17",
			"amcar"
		}
		local weapon_id = upgrade.weapon_id
		local is_default_weapon = (table.contains(default_weapons, weapon_id) and true) or false
		local weapon_level = 0
		local new_weapon_id = (tweak_data.weapon[weapon_id] and tweak_data.weapon[weapon_id].parent_weapon_id) or weapon_id

		for level, data in pairs(tweak_data.upgrades.level_tree) do
			local upgrades = data.upgrades

			if upgrades and table.contains(upgrades, new_weapon_id) then
				weapon_level = level

				break
			end
		end

		return is_default_weapon, weapon_level, weapon_id ~= new_weapon_id
	elseif upgrade.category == "melee_weapon" then
		local params = {
			...
		}
		local default_id = params[1] or (managers.blackmarket and managers.blackmarket:get_category_default("melee_weapon")) or "weapon"
		local melee_weapon_id = upgrade_id
		local is_default_weapon = melee_weapon_id == default_id
		local melee_weapon_level = 0

		for level, data in pairs(tweak_data.upgrades.level_tree) do
			local upgrades = data.upgrades

			if upgrades and table.contains(upgrades, melee_weapon_id) then
				melee_weapon_level = level

				break
			end
		end

		return is_default_weapon, melee_weapon_level
	elseif upgrade.category == "grenade" then
		local params = {
			...
		}
		local default_id = params[1] or (managers.blackmarket and managers.blackmarket:get_category_default("grenade")) or "weapon"
		local grenade_id = upgrade_id
		local is_default_weapon = grenade_id == default_id
		local grenade_level = 0

		for level, data in pairs(tweak_data.upgrades.level_tree) do
			local upgrades = data.upgrades

			if upgrades and table.contains(upgrades, grenade_id) then
				grenade_level = level

				break
			end
		end

		return is_default_weapon, grenade_level
	end

	print("no value for", upgrade_id, upgrade.category)

	return 
end
UpgradesManager.get_category = function (self, upgrade_id)
	local upgrade = tweak_data.upgrades.definitions[upgrade_id]

	return upgrade.category
end
UpgradesManager.get_upgrade_upgrade = function (self, upgrade_id)
	local upgrade = tweak_data.upgrades.definitions[upgrade_id]

	return upgrade.upgrade
end
UpgradesManager.get_upgrade_locks = function (self, upgrade_id)
	local upgrade = tweak_data.upgrades.definitions[upgrade_id]

	return {
		dlc = upgrade.dlc
	}
end
UpgradesManager.is_upgrade_locked = function (self, upgrade_id)
	local locks = self.get_upgrade_locks(self, upgrade_id)

	for category, id in pairs(locks) do
		if category == "dlc" and not managers.dlc:is_dlc_unlocked(id) then
			return true
		end
	end

	return false
end
UpgradesManager.is_locked = function (self, step)
	local level = managers.experience:current_level()

	for i, d in ipairs(tweak_data.upgrades.itree_caps) do
		if level < d.level then
			return d.step <= step
		end
	end

	return false
end
UpgradesManager.get_level_from_step = function (self, step)
	for i, d in ipairs(tweak_data.upgrades.itree_caps) do
		if step == d.step then
			return d.level
		end
	end

	return 0
end
UpgradesManager.progress = function (self)
	if managers.dlc:is_dlc_unlocked("preorder") then
		return {
			self._global.progress[1],
			self._global.progress[2],
			self._global.progress[3],
			self._global.progress[4]
		}
	end

	return {
		self._global.progress[1],
		self._global.progress[2],
		self._global.progress[3]
	}
end
UpgradesManager.progress_by_tree = function (self, tree)
	return self._global.progress[tree]
end
UpgradesManager.name = function (self, id)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to get name from an upgrade that doesn't exist: " .. id .. "")

		return 
	end

	local upgrade = tweak_data.upgrades.definitions[id]

	return managers.localization:text(upgrade.name_id)
end
UpgradesManager.title = function (self, id)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to get title from an upgrade that doesn't exist: " .. id .. "")

		return 
	end

	local upgrade = tweak_data.upgrades.definitions[id]

	return (upgrade.title_id and managers.localization:text(upgrade.title_id)) or nil
end
UpgradesManager.subtitle = function (self, id)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to get subtitle from an upgrade that doesn't exist: " .. id .. "")

		return 
	end

	local upgrade = tweak_data.upgrades.definitions[id]

	return (upgrade.subtitle_id and managers.localization:text(upgrade.subtitle_id)) or nil
end
UpgradesManager.complete_title = function (self, id, type)
	local title = self.title(self, id)

	if not title then
		return nil
	end

	local subtitle = self.subtitle(self, id)

	if not subtitle then
		return title
	end

	if type then
		if type == "single" then
			return title .. " " .. subtitle
		else
			return title .. type .. subtitle
		end
	end

	return title .. "\n" .. subtitle
end
UpgradesManager.description = function (self, id)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to get description from an upgrade that doesn't exist: " .. id .. "")

		return 
	end

	local upgrade = tweak_data.upgrades.definitions[id]

	return (upgrade.subtitle_id and managers.localization:text(upgrade.description_text_id or id)) or nil
end
UpgradesManager.image = function (self, id)
	local image = tweak_data.upgrades.definitions[id].image

	if not image then
		return nil, nil
	end

	return tweak_data.hud_icons:get_icon_data(image)
end
UpgradesManager.image_slice = function (self, id)
	local image_slice = tweak_data.upgrades.definitions[id].image_slice

	if not image_slice then
		return nil, nil
	end

	return tweak_data.hud_icons:get_icon_data(image_slice)
end
UpgradesManager.icon = function (self, id)
	if not tweak_data.upgrades.definitions[id] then
		Application:error("Tried to aquire an upgrade that doesn't exist: " .. id .. "")

		return 
	end

	return tweak_data.upgrades.definitions[id].icon
end
UpgradesManager.aquired_by_category = function (self, category)
	local t = {}

	for name, _ in pairs(self._global.aquired) do
		if tweak_data.upgrades.definitions[name].category == category and self.aquired(self, name) then
			table.insert(t, name)
		end
	end

	return t
end
UpgradesManager.aquired_features = function (self)
	return self.aquired_by_category(self, "feature")
end
UpgradesManager.aquired_weapons = function (self)
	return self.aquired_by_category(self, "weapon")
end
UpgradesManager.list_level_rewards = function (self, dlcs)
	local t = {}
	local tree_data = tweak_data.upgrades.level_tree
	local def = nil

	for level, data in pairs(tree_data) do
		if data.upgrades then
			for _, upgrade in ipairs(data.upgrades) do
				def = tweak_data.upgrades.definitions[upgrade]

				if def and (not dlcs or def.dlc) and (not dlcs or (dlcs == true and def.dlc) or dlcs[def.dlc] or table.contains(dlcs, def.dlc)) then
					table.insert(t, {
						upgrade,
						level,
						def.dlc
					})
				end
			end
		end
	end

	return t
end
UpgradesManager.all_weapon_upgrades = function (self)
	for id, data in pairs(tweak_data.upgrades.definitions) do
		if data.category == "weapon" then
			print(id)
		end
	end

	return 
end
UpgradesManager.weapon_upgrade_by_weapon_id = function (self, weapon_id)
	for id, data in pairs(tweak_data.upgrades.definitions) do
		if data.category == "weapon" and data.weapon_id == weapon_id then
			return data
		end
	end

	return 
end
UpgradesManager.weapon_upgrade_by_factory_id = function (self, factory_id)
	for id, data in pairs(tweak_data.upgrades.definitions) do
		if data.category == "weapon" and data.factory_id == factory_id then
			return data
		end
	end

	return 
end
UpgradesManager.print_aquired_tree = function (self)
	local tree = {}

	for name, data in pairs(self._global.aquired) do
		tree[data.level] = {
			name = name
		}
	end

	for i, data in pairs(tree) do
		print(self.name(self, data.name))
	end

	return 
end
UpgradesManager.analyze = function (self)
	local not_placed = {}
	local placed = {}
	local features = {}
	local amount = 0

	for lvl, upgrades in pairs(tweak_data.upgrades.levels) do
		print("Upgrades at level " .. lvl .. ":")

		for _, upgrade in ipairs(upgrades) do
			print("\t" .. upgrade)
		end
	end

	for name, data in pairs(tweak_data.upgrades.definitions) do
		amount = amount + 1

		for lvl, upgrades in pairs(tweak_data.upgrades.levels) do
			for _, upgrade in ipairs(upgrades) do
				if upgrade == name then
					if placed[name] then
						print("ERROR: Upgrade " .. name .. " is already placed in level " .. placed[name] .. "!")
					else
						placed[name] = lvl
					end

					if data.category == "feature" then
						features[data.upgrade.category] = features[data.upgrade.category] or {}

						table.insert(features[data.upgrade.category], {
							level = lvl,
							name = name
						})
					end
				end
			end
		end

		if not placed[name] then
			not_placed[name] = true
		end
	end

	for name, lvl in pairs(placed) do
		print("Upgrade " .. name .. " is placed in level\t\t " .. lvl .. ".")
	end

	for name, _ in pairs(not_placed) do
		print("Upgrade " .. name .. " is not placed any level!")
	end

	print("")

	for category, upgrades in pairs(features) do
		print("Upgrades for category " .. category .. " is recieved at:")

		for _, upgrade in ipairs(upgrades) do
			print("  Level: " .. upgrade.level .. ", " .. upgrade.name .. "")
		end
	end

	print("\nTotal upgrades " .. amount .. ".")

	return 
end
UpgradesManager.tree_stats = function (self)
	local t = {
		{
			a = 0,
			u = {}
		},
		{
			a = 0,
			u = {}
		},
		{
			a = 0,
			u = {}
		}
	}

	for name, d in pairs(tweak_data.upgrades.definitions) do
		if d.tree then
			t[d.tree].a = t[d.tree].a + 1

			table.insert(t[d.tree].u, name)
		end
	end

	for i, d in ipairs(t) do
		print(inspect(d.u))
		print(d.a)
	end

	return 
end
UpgradesManager.save = function (self, data)
	local state = {
		automanage = self._global.automanage,
		progress = self._global.progress,
		target_tree = self._global.target_tree,
		disabled_visual_upgrades = self._global.disabled_visual_upgrades
	}

	if self._global.incompatible_data_loaded and self._global.incompatible_data_loaded.progress then
		state.progress = clone(self._global.progress)

		for i, k in pairs(self._global.incompatible_data_loaded.progress) do
			print("saving incompatible data", i, k)

			state.progress[i] = math.max(state.progress[i], k)
		end
	end

	data.UpgradesManager = state

	return 
end
UpgradesManager.load = function (self, data)
	local state = data.UpgradesManager
	self._global.automanage = state.automanage
	self._global.progress = state.progress
	self._global.target_tree = state.target_tree
	self._global.disabled_visual_upgrades = state.disabled_visual_upgrades

	self._verify_loaded_data(self)

	return 
end
UpgradesManager._verify_loaded_data = function (self)
	return 
end
UpgradesManager.reset = function (self)
	Global.upgrades_manager = nil

	self._setup(self)

	return 
end

return 
