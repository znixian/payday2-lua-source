ActionMessagingManager = ActionMessagingManager or class()
ActionMessagingManager.PATH = "gamedata/action_messages"
ActionMessagingManager.FILE_EXTENSION = "action_message"
ActionMessagingManager.FULL_PATH = ActionMessagingManager.PATH .. "." .. ActionMessagingManager.FILE_EXTENSION
ActionMessagingManager.init = function (self)
	self._messages = {}

	self._parse_messages(self)

	return 
end
ActionMessagingManager._parse_messages = function (self)
	local list = PackageManager:script_data(self.FILE_EXTENSION:id(), self.PATH:id())

	for _, data in ipairs(list) do
		if data._meta == "message" then
			self._parse_message(self, data)
		else
			Application:error("Unknown node \"" .. tostring(data._meta) .. "\" in \"" .. self.FULL_PATH .. "\". Expected \"message\" node.")
		end
	end

	return 
end
ActionMessagingManager._parse_message = function (self, data)
	local id = data.id
	local text_id = data.text_id
	local event = data.event
	local dialog_id = data.dialog_id
	local equipment_id = data.equipment_id
	self._messages[id] = {
		text_id = text_id,
		event = event,
		dialog_id = dialog_id,
		equipment_id = equipment_id
	}

	return 
end
ActionMessagingManager.ids = function (self)
	local t = {}

	for id, _ in pairs(self._messages) do
		table.insert(t, id)
	end

	table.sort(t)

	return t
end
ActionMessagingManager.messages = function (self)
	return self._messages
end
ActionMessagingManager.message = function (self, id)
	return self._messages[id]
end
ActionMessagingManager.show_message = function (self, id, instigator)
	if not id or not self.message(self, id) then
		Application:stack_dump_error("Bad id to show message, " .. tostring(id) .. ".")

		return 
	end

	self._show_message(self, id, instigator)

	return 
end
ActionMessagingManager._show_message = function (self, id, instigator)
	local msg_data = self.message(self, id)
	local title = instigator.base(instigator):nick_name()
	local icon = nil
	local msg = ""

	if msg_data.equipment_id then
		title = title .. " " .. managers.localization:text("message_obtained_equipment")
		local equipment = tweak_data.equipments.specials[msg_data.equipment_id]
		icon = equipment.icon
		msg = managers.localization:text(equipment.text_id)
	else
		title = title .. ":"
		msg = managers.localization:text(self.message(self, id).text_id)
	end

	managers.hud:present_mid_text({
		time = 4,
		title = utf8.to_upper(title),
		text = utf8.to_upper(msg),
		icon = icon,
		event = self.message(self, id).event
	})

	if self.message(self, id).dialog_id then
		managers.dialog:queue_dialog(self.message(self, id).dialog_id, {})
	end

	return 
end
ActionMessagingManager.sync_show_message = function (self, id, instigator)
	if alive(instigator) and managers.network:session():peer_by_unit(instigator) then
		self._show_message(self, id, instigator)
	end

	return 
end
ActionMessagingManager.save = function (self, data)
	return 
end
ActionMessagingManager.load = function (self, data)
	return 
end

return 
