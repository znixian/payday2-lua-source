LootManager = LootManager or class()
LootManager.init = function (self)
	self._setup(self)

	return 
end
LootManager._setup = function (self)
	Global.loot_manager = Global.loot_manager or {
		secured = {},
		mandatory_bags = {},
		postponed_small_loot = {}
	}
	self._global = Global.loot_manager
	self._initial_loot = deep_clone(self._global.secured)
	self._distribution_loot = {}
	local level_id = Global.level_data and Global.level_data.level_id
	local level_tweak = tweak_data.levels[level_id]

	if level_tweak and level_tweak.repossess_bags then
		self._repossess_bags_for_distribution(self)
	end

	self._triggers = {}
	self._respawns = {}

	return 
end
LootManager.clear = function (self)
	Global.loot_manager = nil

	return 
end
LootManager.reset = function (self)
	Global.loot_manager = nil

	self._setup(self)

	return 
end
LootManager.on_simulation_ended = function (self)
	self._respawns = {}
	self._global.mandatory_bags = {}

	return 
end
LootManager.add_trigger = function (self, id, type, amount, callback)
	self._triggers[type] = self._triggers[type] or {}
	self._triggers[type][id] = {
		amount = amount,
		callback = callback
	}

	return 
end
LootManager._check_triggers = function (self, type)
	if not self._triggers[type] then
		return 
	end

	if type == "amount" then
		local bag_total_value = self.get_real_total_loot_value(self)

		for id, cb_data in pairs(self._triggers[type]) do
			if type ~= "amount" or cb_data.amount <= bag_total_value then
				cb_data.callback()
			end
		end
	elseif type == "total_amount" then
		local total_value = self.get_real_total_value(self)

		for id, cb_data in pairs(self._triggers[type]) do
			if cb_data.amount <= total_value then
				cb_data.callback()
			end
		end
	elseif type == "report_only" then
		for id, cb_data in pairs(self._triggers[type]) do
			cb_data.callback()
		end
	end

	return 
end
LootManager.on_retry_job_stage = function (self)
	self._global.secured = self._initial_loot

	return 
end
LootManager.get_secured = function (self)
	return table.remove(self._global.secured, 1)
end
LootManager.get_secured_random = function (self)
	local entry = math.random(#self._global.secured)

	return table.remove(self._global.secured, entry)
end
LootManager.get_distribute = function (self)
	return table.remove(self._distribution_loot, 1)
end
LootManager.get_respawn = function (self)
	return table.remove(self._respawns, 1)
end
LootManager.add_to_respawn = function (self, carry_id, multiplier)
	table.insert(self._respawns, {
		carry_id = carry_id,
		multiplier = multiplier
	})

	return 
end
LootManager._repossess_bags_for_distribution = function (self)
	local small_loot = {}
	local distribute = self._distribution_loot

	for _, data in ipairs(self._global.secured) do
		local is_small = tweak_data.carry.small_loot[data.carry_id] ~= nil

		table.insert((is_small and small_loot) or distribute, data)
	end

	self._global.secured = small_loot

	return 
end
LootManager.on_job_deactivated = function (self)
	self.clear(self)

	return 
end
LootManager.secure = function (self, carry_id, multiplier_level, silent, peer_id)
	if Network:is_server() then
		self.server_secure_loot(self, carry_id, multiplier_level, silent, peer_id)
	else
		managers.network:session():send_to_host("server_secure_loot", carry_id, multiplier_level, peer_id)
	end

	return 
end
LootManager.server_secure_loot = function (self, carry_id, multiplier_level, silent, peer_id)
	managers.network:session():send_to_peers_synched("sync_secure_loot", carry_id, multiplier_level, silent, peer_id)
	self.sync_secure_loot(self, carry_id, multiplier_level, silent, peer_id)

	return 
end
LootManager.sync_secure_loot = function (self, carry_id, multiplier_level, silent, peer_id)
	if peer_id == 0 then
		peer_id = nil
	end

	local multiplier = (tweak_data.carry.small_loot[carry_id] and managers.player:upgrade_value_by_level("player", "small_loot_multiplier", multiplier_level, 1)) or 1

	table.insert(self._global.secured, {
		carry_id = carry_id,
		multiplier = multiplier,
		peer_id = peer_id
	})
	managers.hud:loot_value_updated()
	self._check_triggers(self, "amount")
	self._check_triggers(self, "total_amount")

	if not tweak_data.carry.small_loot[carry_id] then
		self._check_triggers(self, "report_only")

		if not silent then
			self._present(self, carry_id, multiplier)
		end
	end

	self.check_achievements(self, carry_id, multiplier)

	return 
end
LootManager._count_achievement_secured = function (self, achievement, secured_data)
	local amount = 0
	local total_amount = 0
	local value = 0

	for _, data in ipairs(self._global.secured) do
		local found = false
		local carry_id = nil

		if type(secured_data.carry_id) == "table" then
			for _, id in ipairs(secured_data.carry_id) do
				if id == data.carry_id then
					found = true
					carry_id = id

					break
				end
			end
		elseif data.carry_id == secured_data.carry_id then
			found = true
			carry_id = data.carry_id
		end

		if found then
			if not data[achievement] then
				amount = amount + 1
				data[achievement] = true
			end

			total_amount = total_amount + 1
			local is_small_loot = not not tweak_data.carry.small_loot[carry_id]

			if is_small_loot then
				value = value + self.get_real_value(self, carry_id, data.multiplier)
			else
				value = value + managers.money:get_secured_bonus_bag_value(carry_id, data.multiplier)
			end
		end
	end

	return total_amount, amount, value
end
LootManager._check_secured = function (self, achievement, secured_data)
	local total_amount, amount, value = self._count_achievement_secured(self, achievement, secured_data)

	return (secured_data.total_amount and secured_data.total_amount <= total_amount) or (secured_data.amount and secured_data.amount <= amount) or (secured_data.value and secured_data.value <= value)
end
LootManager.check_achievements = function (self, carry_id, multiplier)
	local real_total_value = self.get_real_total_value(self)
	local memory, total_memory_value, all_pass, total_value_pass, jobs_pass, levels_pass, difficulties_pass, total_time_pass, no_assets_pass, no_deployable_pass, secured_pass, is_dropin_pass = nil
	slot16 = pairs
	slot17 = tweak_data.achievement.loot_cash_achievements or {}

	for achievement, achievement_data in slot16(slot17) do
		jobs_pass = not achievement_data.jobs or table.contains(achievement_data.jobs, managers.job:current_real_job_id())
		levels_pass = not achievement_data.levels or table.contains(achievement_data.levels, managers.job:current_level_id())
		difficulties_pass = not achievement_data.difficulties or table.contains(achievement_data.difficulties, Global.game_settings.difficulty)
		total_time_pass = not achievement_data.total_time or (managers.game_play_central and managers.game_play_central:get_heist_timer() <= achievement_data.total_time)
		no_assets_pass = not achievement_data.no_assets or #managers.assets:get_unlocked_asset_ids(true) == 0
		no_deployable_pass = not achievement_data.no_deployable or not managers.player:has_deployable_been_used()
		is_dropin_pass = achievement_data.is_dropin == nil or achievement_data.is_dropin == managers.statistics:is_dropin()
		secured_pass = not achievement_data.secured

		if achievement_data.secured then
			if achievement_data.secured[1] ~= nil then
				for i, secured_data in ipairs(achievement_data.secured) do
					secured_pass = self._check_secured(self, achievement, secured_data)

					if not secured_pass then
						break
					end
				end
			else
				secured_pass = self._check_secured(self, achievement, achievement_data.secured)
			end
		end

		if not achievement_data.timer then
			total_value_pass = not achievement_data.total_value or achievement_data.total_value <= real_total_value
		else
			memory = managers.job:get_memory(achievement, achievement_data.is_shortterm)
			local t = Application:time()
			local new_memory = {
				time = t,
				value = self.get_real_value(self, carry_id, multiplier)
			}

			if memory then
				table.insert(memory, new_memory)

				for i = #memory, 1, -1 do
					if achievement_data.timer <= t - memory[i].time then
						table.remove(memory, i)
					end
				end

				managers.job:set_memory(achievement, memory, achievement_data.is_shortterm)
			else
				memory = {
					new_memory
				}

				managers.job:set_memory(achievement, memory, achievement_data.is_shortterm)
			end

			total_memory_value = 0

			for _, m_data in ipairs(memory) do
				total_memory_value = total_memory_value + m_data.value
			end

			total_value_pass = not achievement_data.total_value or achievement_data.total_value <= total_memory_value
		end

		all_pass = total_value_pass and jobs_pass and levels_pass and difficulties_pass and total_time_pass and no_assets_pass and no_deployable_pass and secured_pass and is_dropin_pass

		if all_pass and not managers.achievment:award_data(achievement_data) then
			Application:debug("[LootManager] loot_cash_achievements:", achievement)
		end
	end

	return 
end
LootManager.secure_small_loot = function (self, type, multiplier_level, peer_id)
	self.secure(self, type, multiplier_level, nil, peer_id)

	return 
end
LootManager.show_small_loot_taken_hint = function (self, type, multiplier)
	managers.hint:show_hint("grabbed_small_loot", 2, nil, {
		MONEY = managers.experience:cash_string(self.get_real_value(self, type, multiplier))
	})

	return 
end
LootManager.set_mandatory_bags_data = function (self, carry_id, amount)
	self._global.mandatory_bags.carry_id = carry_id
	self._global.mandatory_bags.amount = amount

	return 
end
LootManager.get_mandatory_bags_data = function (self)
	return self._global.mandatory_bags
end
LootManager.check_secured_mandatory_bags = function (self)
	if not self._global.mandatory_bags.amount or self._global.mandatory_bags.amount == 0 then
		return true
	end

	local amount = self.get_secured_mandatory_bags_amount(self)

	return self._global.mandatory_bags.amount <= amount
end
LootManager.get_secured_mandatory_bags_amount = function (self, is_vehicle)
	local mandatory_bags_amount = self._global.mandatory_bags.amount or 0

	if mandatory_bags_amount == 0 then
		return 0
	end

	local amount = 0

	for _, data in ipairs(self._global.secured) do
		if not tweak_data.carry.small_loot[data.carry_id] and not tweak_data.carry[data.carry_id].is_vehicle == not is_vehicle and 0 < mandatory_bags_amount and (self._global.mandatory_bags.carry_id == "none" or self._global.mandatory_bags.carry_id == data.carry_id) then
			amount = amount + 1
			mandatory_bags_amount = mandatory_bags_amount - 1
		end
	end

	return amount
end
LootManager.get_secured_bonus_bags_amount = function (self, is_vehicle)
	local mandatory_bags_amount = self._global.mandatory_bags.amount or 0
	local secured_mandatory_bags_amount = self.get_secured_mandatory_bags_amount(self)
	local amount = 0

	for _, data in ipairs(self._global.secured) do
		if not tweak_data.carry.small_loot[data.carry_id] and not tweak_data.carry[data.carry_id].is_vehicle == not is_vehicle then
			if 0 < mandatory_bags_amount and (self._global.mandatory_bags.carry_id == "none" or self._global.mandatory_bags.carry_id == data.carry_id) then
				mandatory_bags_amount = mandatory_bags_amount - 1
			else
				amount = amount + 1
			end
		end
	end

	return amount
end
LootManager.get_secured_bonus_bags_value = function (self, level_id, is_vehicle)
	local mandatory_bags_amount = self._global.mandatory_bags.amount or 0
	local amount_bags = (tweak_data.levels[level_id] and tweak_data.levels[level_id].max_bags) or 20
	local value = 0

	for _, data in ipairs(self._global.secured) do
		if not tweak_data.carry.small_loot[data.carry_id] and not tweak_data.carry[data.carry_id].is_vehicle == not is_vehicle then
			if 0 < mandatory_bags_amount and (self._global.mandatory_bags.carry_id == "none" or self._global.mandatory_bags.carry_id == data.carry_id) then
				mandatory_bags_amount = mandatory_bags_amount - 1
			elseif 0 < amount_bags then
				value = value + managers.money:get_bag_value(data.carry_id, data.multiplier)
			end

			amount_bags = amount_bags - 1
		end
	end

	return value
end
LootManager.get_secured_mandatory_bags_value = function (self, is_vehicle)
	local mandatory_bags_amount = self._global.mandatory_bags.amount or 0
	local value = 0

	for _, data in ipairs(self._global.secured) do
		if not tweak_data.carry.small_loot[data.carry_id] and not tweak_data.carry[data.carry_id].is_vehicle == not is_vehicle and 0 < mandatory_bags_amount and (self._global.mandatory_bags.carry_id == "none" or self._global.mandatory_bags.carry_id == data.carry_id) then
			mandatory_bags_amount = mandatory_bags_amount - 1
			value = value + managers.money:get_bag_value(data.carry_id, data.multiplier)
		end
	end

	return value
end
LootManager.is_bonus_bag = function (self, carry_id, is_vehicle)
	if self._global.mandatory_bags.carry_id ~= "none" and carry_id and carry_id ~= self._global.mandatory_bags.carry_id then
		return true
	end

	local mandatory_bags_amount = self._global.mandatory_bags.amount or 0

	for _, data in ipairs(self._global.secured) do
		if not tweak_data.carry.small_loot[data.carry_id] and not tweak_data.carry[data.carry_id].is_vehicle == not is_vehicle then
			if 0 < mandatory_bags_amount and (self._global.mandatory_bags.carry_id == "none" or self._global.mandatory_bags.carry_id == data.carry_id) then
				mandatory_bags_amount = mandatory_bags_amount - 1
			elseif mandatory_bags_amount == 0 then
				return true
			end
		end
	end

	return false
end
LootManager.get_real_value = function (self, carry_id, multiplier)
	local mul_value = 1

	if not tweak_data.carry.small_loot[carry_id] then
		local has_active_job = managers.job:has_active_job()
		local job_stars = (has_active_job and managers.job:current_job_stars()) or 1
		mul_value = tweak_data:get_value("carry", "value_multiplier", job_stars)
		mul_value = mul_value or 1
	end

	return managers.money:get_bag_value(carry_id, multiplier)*mul_value
end
LootManager.get_real_total_value = function (self)
	local value = 0

	for _, data in ipairs(self._global.secured) do
		value = value + self.get_real_value(self, data.carry_id, data.multiplier)
	end

	return value
end
LootManager.get_real_total_loot_value = function (self)
	local value = 0
	local loot_value = nil

	for _, data in ipairs(self._global.secured) do
		if not tweak_data.carry.small_loot[data.carry_id] then
			loot_value = self.get_real_value(self, data.carry_id, data.multiplier)

			if value + loot_value < tweak_data:get_value("money_manager", "max_small_loot_value") then
				value = value + loot_value
			else
				value = tweak_data:get_value("money_manager", "max_small_loot_value")

				break
			end
		end
	end

	return value
end
LootManager.get_real_total_small_loot_value = function (self)
	local value = 0
	local loot_value = nil

	for _, data in ipairs(self._global.secured) do
		if tweak_data.carry.small_loot[data.carry_id] then
			loot_value = self.get_real_value(self, data.carry_id, data.multiplier)

			if value + loot_value < tweak_data:get_value("money_manager", "max_small_loot_value") then
				value = value + loot_value
			else
				value = tweak_data:get_value("money_manager", "max_small_loot_value")

				break
			end
		end
	end

	return value
end
LootManager.set_postponed_small_loot = function (self)
	self._global.postponed_small_loot = {}

	for _, data in ipairs(self._global.secured) do
		if tweak_data.carry.small_loot[data.carry_id] then
			table.insert(self._global.postponed_small_loot, deep_clone(data))
		end
	end

	return 
end
LootManager.get_real_total_postponed_small_loot_value = function (self)
	if not self._global.postponed_small_loot then
		return 0
	end

	local value = 0

	for _, data in ipairs(self._global.postponed_small_loot) do
		value = value + self.get_real_value(self, data.carry_id, data.multiplier)
	end

	return value
end
LootManager.clear_postponed_small_loot = function (self)
	self._global.postponed_small_loot = nil

	return 
end
LootManager.total_value_by_carry_id = function (self, carry_id)
	local value = 0

	for _, data in ipairs(self._global.secured) do
		if data.carry_id == carry_id then
			value = value + data.value
		end
	end

	return value
end
LootManager.total_small_loot_value = function (self)
	local value = 0

	for _, data in ipairs(self._global.secured) do
		if tweak_data.carry.small_loot[data.carry_id] then
			value = value + data.value
		end
	end

	return value
end
LootManager.total_value_by_type = function (self, type)
	if not tweak_data.carry.types[type] then
		Application:error("Carry type", type, "doesn't exists!")

		return 
	end

	local value = 0

	for _, data in ipairs(self._global.secured) do
		if tweak_data.carry[data.carry_id].type == type then
			value = value + data.value
		end
	end

	return value
end
LootManager.get_loot_stinger = function (self)
	local job_tweak = tweak_data.narrative.jobs[managers.job:current_real_job_id()]

	if job_tweak and job_tweak.objective_stinger then
		return job_tweak.objective_stinger
	end

	return "stinger_objectivecomplete"
end
LootManager._present = function (self, carry_id, multiplier)
	local real_value = 0
	local is_small_loot = not not tweak_data.carry.small_loot[carry_id]

	if is_small_loot then
		real_value = self.get_real_value(self, carry_id, multiplier)
	else
		real_value = managers.money:get_secured_bonus_bag_value(carry_id, multiplier)
	end

	local carry_data = tweak_data.carry[carry_id]
	local title = managers.localization:text("hud_loot_secured_title")
	local type_text = carry_data.name_id and managers.localization:text(carry_data.name_id)
	local text = managers.localization:text("hud_loot_secured", {
		CARRY_TYPE = type_text,
		AMOUNT = managers.experience:cash_string(real_value)
	})
	local icon = nil

	managers.hud:present_mid_text({
		time = 4,
		text = text,
		title = title,
		icon = icon,
		event = self.get_loot_stinger(self)
	})

	return 
end
LootManager.sync_save = function (self, data)
	data.LootManager = clone(self._global)

	return 
end
LootManager.sync_load = function (self, data)
	self._global = data.LootManager
	Global.loot_manager = self._global

	for _, data in ipairs(self._global.secured) do
		if data.multiplier and 2 < data.multiplier then
			data.multiplier = 2
		end
	end

	return 
end

return 
