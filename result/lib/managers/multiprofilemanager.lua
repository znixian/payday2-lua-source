MultiProfileManager = MultiProfileManager or class()
MultiProfileManager.init = function (self)
	if not Global.multi_profile then
		Global.multi_profile = {}
	end

	self._global = self._global or Global.multi_profile
	self._global._profiles = self._global._profiles or {}
	self._global._current_profile = self._global._current_profile or 1

	self._check_amount(self)

	return 
end
MultiProfileManager.save_current = function (self)
	print("[MultiProfileManager:save_current] current profile:", self._global._current_profile)

	local profile = self.current_profile(self) or {}
	local blm = managers.blackmarket
	local skt = managers.skilltree._global
	profile.primary = blm.equipped_weapon_slot(blm, "primaries")
	profile.secondary = blm.equipped_weapon_slot(blm, "secondaries")
	profile.melee = blm.equipped_melee_weapon(blm)
	profile.throwable = blm.equipped_grenade(blm)
	profile.deployable = blm.equipped_deployable(blm)
	profile.deployable_secondary = blm.equipped_deployable(blm, 2)
	profile.armor = blm.equipped_armor(blm)
	profile.armor_skin = blm.equipped_armor_skin(blm)
	profile.skillset = skt.selected_skill_switch
	profile.perk_deck = Application:digest_value(skt.specializations.current_specialization, false)
	profile.mask = blm.equipped_mask_slot(blm)
	self._global._profiles[self._global._current_profile] = profile

	print("[MultiProfileManager:save_current] done")

	return 
end
MultiProfileManager.load_current = function (self)
	local profile = self.current_profile(self)
	local blm = managers.blackmarket
	local skt = managers.skilltree

	skt.switch_skills(skt, profile.skillset)
	managers.player:check_skills()
	skt.set_current_specialization(skt, profile.perk_deck)
	blm.equip_weapon(blm, "primaries", profile.primary)
	blm.equip_weapon(blm, "secondaries", profile.secondary)
	blm.equip_melee_weapon(blm, profile.melee)
	blm.equip_grenade(blm, profile.throwable)
	blm.equip_deployable(blm, {
		target_slot = 1,
		name = profile.deployable
	})
	blm.equip_deployable(blm, {
		target_slot = 2,
		name = profile.deployable_secondary
	})
	blm.equip_armor(blm, profile.armor)
	blm.set_equipped_armor_skin(blm, profile.armor_skin)
	blm.equip_mask(blm, profile.mask)

	local mcm = managers.menu_component

	if mcm._player_inventory_gui then
		local node = mcm._player_inventory_gui._node

		mcm.close_inventory_gui(mcm)
		mcm.create_inventory_gui(mcm, node)
	elseif mcm._mission_briefing_gui then
		managers.assets:reload_locks()

		local node = mcm._mission_briefing_gui._node

		mcm.close_mission_briefing_gui(mcm)
		mcm.create_mission_briefing_gui(mcm, node)
	end

	return 
end
MultiProfileManager.current_profile_name = function (self)
	if not self.current_profile(self) then
		return "Error"
	end

	return self.current_profile(self).name or "Profile " .. self._global._current_profile
end
MultiProfileManager.profile_count = function (self)
	return math.max(#self._global._profiles, 1)
end
MultiProfileManager.set_current_profile = function (self, index)
	if index < 0 or self.profile_count(self) < index then
		return 
	end

	if index == self._global._current_profile then
		return 
	end

	self.save_current(self)

	self._global._current_profile = index

	self.load_current(self)
	print("[MultiProfileManager:set_current_profile] current profile:", self._global._current_profile)

	return 
end
MultiProfileManager.current_profile = function (self)
	return self.profile(self, self._global._current_profile)
end
MultiProfileManager.profile = function (self, index)
	return self._global._profiles[index]
end
MultiProfileManager._add_profile = function (self, profile, index)
	index = index or #self._global._profiles + 1
	self._global._profiles[index] = profile

	return 
end
MultiProfileManager.next_profile = function (self)
	self.set_current_profile(self, self._global._current_profile + 1)

	return 
end
MultiProfileManager.previous_profile = function (self)
	self.set_current_profile(self, self._global._current_profile - 1)

	return 
end
MultiProfileManager.has_next = function (self)
	return self._global._current_profile < self.profile_count(self)
end
MultiProfileManager.has_previous = function (self)
	return 1 < self._global._current_profile
end
MultiProfileManager.open_quick_select = function (self)
	local dialog_data = {
		title = "",
		text = "",
		button_list = {}
	}

	for idx, profile in pairs(self._global._profiles) do
		local text = profile.name or "Profile " .. idx

		table.insert(dialog_data.button_list, {
			text = text,
			callback_func = function ()
				self:set_current_profile(idx)

				return 
			end,
			focus_callback_func = function ()
				return 
			end
		})
	end

	local no_button = {
		text = managers.localization:text("dialog_cancel"),
		focus_callback_func = function ()
			return 
		end,
		cancel_button = true
	}

	table.insert(dialog_data.button_list, no_button)

	dialog_data.image_blend_mode = "normal"
	dialog_data.text_blend_mode = "add"
	dialog_data.use_text_formating = true
	dialog_data.w = 480
	dialog_data.h = 532
	dialog_data.title_font = tweak_data.menu.pd2_medium_font
	dialog_data.title_font_size = tweak_data.menu.pd2_medium_font_size
	dialog_data.font = tweak_data.menu.pd2_small_font
	dialog_data.font_size = tweak_data.menu.pd2_small_font_size
	dialog_data.text_formating_color = Color.white
	dialog_data.text_formating_color_table = {}
	dialog_data.clamp_to_screen = true

	managers.system_menu:show_buttons(dialog_data)

	return 
end
MultiProfileManager.save = function (self, data)
	local save_data = deep_clone(self._global._profiles)
	save_data.current_profile = self._global._current_profile
	data.multi_profile = save_data

	return 
end
MultiProfileManager.load = function (self, data)
	if data.multi_profile then
		for i, profile in ipairs(data.multi_profile) do
			self._add_profile(self, profile, i)
		end

		self._global._current_profile = data.multi_profile.current_profile
	end

	self._check_amount(self)

	return 
end
MultiProfileManager._check_amount = function (self)
	local wanted_amount = 15

	if not self.current_profile(self) then
		self.save_current(self)
	end

	if wanted_amount < self.profile_count(self) then
		table.crop(self._global._profiles, wanted_amount)

		self._global._current_profile = math.min(self._global._current_profile, wanted_amount)
	elseif self.profile_count(self) < wanted_amount then
		local prev_current = self._global._current_profile
		self._global._current_profile = self.profile_count(self)

		while self._global._current_profile < wanted_amount do
			self._global._current_profile = self._global._current_profile + 1

			self.save_current(self)
		end

		self._global._current_profile = prev_current
	end

	return 
end

return 
