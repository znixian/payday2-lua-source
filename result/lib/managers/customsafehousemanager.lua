local one_day_seconds = 86400
CustomSafehouseManager = CustomSafehouseManager or class()
CustomSafehouseManager.SAVE_DATA_VERSION = 2
CustomSafehouseManager.SPAWN_COOLDOWN = one_day_seconds*3
CustomSafehouseManager.IGNORE_SPAWN_COOLDOWN = one_day_seconds*0.5
CustomSafehouseManager.SERVER_TICK = 256
CustomSafehouseManager.init = function (self)
	self._setup(self)

	return 
end
CustomSafehouseManager._setup = function (self)
	self._server_tick = 0
	self._highest_tier = #tweak_data.safehouse.prices.rooms

	if not Global.custom_safehouse_manager then
		Global.custom_safehouse_manager = {
			total = Application:digest_value(0, true),
			total_collected = Application:digest_value(0, true),
			prev_total = Application:digest_value(0, true),
			rooms = {}
		}

		for index, data in ipairs(tweak_data.safehouse.rooms) do
			Global.custom_safehouse_manager.rooms[data.room_id] = {
				tier_start = 1,
				tier_current = 1,
				tier_max = data.tier_max or self._highest_tier,
				unlocked_tiers = {
					1
				}
			}
		end

		Global.custom_safehouse_manager.trophies = {}

		for idx, trophy in ipairs(tweak_data.safehouse.trophies) do
			table.insert(Global.custom_safehouse_manager.trophies, deep_clone(trophy))
		end

		Global.custom_safehouse_manager.completed_trophies = {}

		self.generate_daily(self)
	end

	self._global = Global.custom_safehouse_manager
	self._global.prev_total = self._global.total
	self._synced_room_tiers = {}

	self.attempt_give_initial_coins(self)
	self.check_if_new_daily_available(self)
	managers.mission:add_global_event_listener("custom_safehouse_enter_safehouse", {
		Message.OnEnterSafeHouse
	}, callback(self, self, "_on_enter_safe_house"))
	managers.mission:add_global_event_listener("custom_safehouse_heist_complete", {
		Message.OnHeistComplete
	}, callback(self, self, "_on_heist_completed"))

	self._trophy_unlocked_callbacks = {}

	if not self.unlocked(self) then
		self._global._new_player = true
	end

	return 
end
CustomSafehouseManager.save = function (self, data)
	local save_rooms = {}

	for room_id, data in pairs(self._global.rooms) do
		table.insert(save_rooms, {
			room_id = room_id,
			unlocked = data.unlocked_tiers,
			current = data.tier_current
		})
	end

	local trophies = {}

	for idx, trophy in ipairs(self._global.trophies) do
		local trophy_data = {
			id = trophy.id,
			objectives = {},
			completed = trophy.completed,
			displayed = trophy.displayed
		}

		for _, objective in ipairs(trophy.objectives) do
			local objective_data = {}

			for _, save_value in ipairs(objective.save_values) do
				objective_data[save_value] = objective[save_value]
			end

			table.insert(trophy_data.objectives, objective_data)
		end

		table.insert(trophies, trophy_data)
	end

	local state = {
		version = CustomSafehouseManager.SAVE_DATA_VERSION,
		total = self._global.total,
		total_collected = self._global.total_collected,
		rooms = save_rooms,
		trophies = trophies,
		daily = self._global.daily,
		has_entered_safehouse = self._global._has_entered_safehouse or false,
		spawn_cooldown = self._global._spawn_cooldown or 0,
		new_player = self._global._new_player or false
	}
	data.CustomSafehouseManager = state

	return 
end
CustomSafehouseManager.load = function (self, data, version)
	local state = data.CustomSafehouseManager

	if state and state.version == CustomSafehouseManager.SAVE_DATA_VERSION then
		self._global._new_player = state.new_player or false
		self._global._has_entered_safehouse = state.has_entered_safehouse or false
		self._global._spawn_cooldown = state.spawn_cooldown or 0
		self._global.total = (state.total and Application:digest_value(math.max(0, Application:digest_value(state.total, false)), true)) or Application:digest_value(0, true)
		self._global.total_collected = (state.total_collected and Application:digest_value(math.max(0, Application:digest_value(state.total_collected, false)), true)) or Application:digest_value(0, true)
		slot4 = ipairs
		slot5 = state.rooms or {}

		for index, room_data in slot4(slot5) do
			local room = self._global.rooms[room_data.room_id]

			if room then
				room.tier_current = math.clamp(room_data.current, 1, self._highest_tier)
				room.unlocked_tiers = room_data.unlocked
			else
				print("couldn't find room for room_id: ", room_data.room_id)
			end
		end

		slot4 = ipairs
		slot5 = state.trophies or {}

		for idx, saved_trophy in slot4(slot5) do
			local trophy = self.get_trophy(self, saved_trophy.id)

			if trophy then
				local objectives_complete = true

				if not saved_trophy.completed then
					for _, objective in ipairs(trophy.objectives) do
						for _, saved_objective in ipairs(saved_trophy.objectives) do
							if (objective.achievement_id ~= nil and objective.achievement_id == saved_objective.achievement_id) or (objective.progress_id ~= nil and objective.progress_id == saved_objective.progress_id) then
								for _, save_value in ipairs(objective.save_values) do
									objective[save_value] = saved_objective[save_value] or objective[save_value]
								end

								if not saved_objective.completed then
									objectives_complete = false
								end
							end
						end
					end
				end

				trophy.completed = objectives_complete

				if saved_trophy.displayed ~= nil then
					trophy.displayed = saved_trophy.displayed
				else
					trophy.displayed = true
				end
			end
		end

		if state.daily then
			self._global.daily = state.daily
		end

		self.check_if_new_daily_available(self)
		self.attempt_give_initial_coins(self)
	end

	return 
end
CustomSafehouseManager.reset = function (self)
	managers.mission:remove_global_event_listener("custom_safehouse_enter_safehouse")
	managers.mission:remove_global_event_listener("custom_safehouse_heist_complete")

	Global.custom_safehouse_manager = nil
	self._global = nil

	self._setup(self)

	return 
end
CustomSafehouseManager.unlocked = function (self)
	return Global.mission_manager.has_played_tutorial and (tweak_data.safehouse.level_limit <= managers.experience:current_level() or 0 < managers.experience:current_rank())
end
CustomSafehouseManager.coins = function (self)
	return Application:digest_value(self._global.total, false)
end
CustomSafehouseManager.previous_coins = function (self)
	return Application:digest_value(self._global.prev_total, false)
end
CustomSafehouseManager.total_coins_earned = function (self)
	return Application:digest_value(self._global.total_collected, false)
end
CustomSafehouseManager.coins_spent = function (self)
	return self.total_coins_earned(self) - self.coins(self)
end
CustomSafehouseManager.add_coins = function (self, amount)
	if not self.unlocked(self) then
		return 
	end

	local new_total = self.total_coins_earned(self) + amount
	local new_current = self.coins(self) + amount
	Global.custom_safehouse_manager.total = Application:digest_value(new_current, true)
	Global.custom_safehouse_manager.total_collected = Application:digest_value(new_total, true)

	print("[Safehouse] adding coins to safehouse: ", amount, Application:digest_value(self._global.total, false))

	if managers.statistics then
		managers.statistics:aquired_coins(amount)
	end

	return 
end
CustomSafehouseManager.deduct_coins = function (self, amount)
	amount = math.clamp(amount, 0, self.coins(self))
	Global.custom_safehouse_manager.total = Application:digest_value(self.coins(self) - amount, true)

	return 
end
CustomSafehouseManager.attempt_give_initial_coins = function (self)
	if not self.unlocked(self) then
		return 
	end

	if self.total_coins_earned(self) == 0 then
		print("[Safehouse] adding initial coins! ", tweak_data.safehouse.rewards.initial)
		self.add_coins(self, tweak_data.safehouse.rewards.initial)
	end

	return 
end
CustomSafehouseManager.get_host_room_tier = function (self, room_id)
	if Network:is_server() then
		return self.get_room_current_tier(self, room_id)
	else
		return self._synced_room_tiers[room_id] or self.get_room_current_tier(self, room_id)
	end

	return 
end
CustomSafehouseManager.set_host_room_tier = function (self, room_id, room_tier)
	if Network:is_server() then
		debug_pause("Trying to set host room tier on host! This should only happen for the client!")
	else
		self._synced_room_tiers[room_id] = room_tier
	end

	return 
end
CustomSafehouseManager.send_room_tiers = function (self, peer)
	local send_func = nil

	if peer then
		function send_func(room_name, room_tier)
			managers.network:session():send_to_peer(peer, "sync_safehouse_room_tier", room_name, room_tier)

			return 
		end
	else
		function send_func(room_name, room_tier)
			managers.network:session():send_to_peers("sync_safehouse_room_tier", room_name, room_tier)

			return 
		end
	end

	for name, v in pairs(self._global.rooms) do
		send_func(name, v.tier_current)
	end

	return 
end
CustomSafehouseManager.get_room_current_tier = function (self, room_id)
	if self._global.rooms[room_id] then
		return self._global.rooms[room_id].tier_current
	end

	return false
end
CustomSafehouseManager.get_room_start_tier = function (self, room_id)
	if self._global.rooms[room_id] then
		return self._global.rooms[room_id].tier_start
	end

	return false
end
CustomSafehouseManager.get_room_max_tier = function (self, room_id)
	if self._global.rooms[room_id] then
		return self._global.rooms[room_id].tier_max
	end

	return false
end
CustomSafehouseManager.set_room_tier = function (self, room_id, tier)
	if self.is_room_tier_unlocked(self, room_id, tier) then
		local room = self._global.rooms[room_id]
		room.tier_current = math.clamp(tier, 1, room.tier_max)

		return true
	end

	return false
end
CustomSafehouseManager.is_room_tier_unlocked = function (self, room_id, tier)
	if self._global.rooms[room_id] then
		for idx, unlocked_tier in ipairs(self._global.rooms[room_id].unlocked_tiers) do
			if tier == unlocked_tier then
				return true
			end
		end
	end

	return false
end
CustomSafehouseManager.purchase_room_tier = function (self, room_id, tier)
	if not self.is_room_tier_unlocked(self, room_id, tier) then
		local current_tier = self.get_room_current_tier(self, room_id)

		while current_tier < tier do
			current_tier = current_tier + 1

			table.insert(self._global.rooms[room_id].unlocked_tiers, current_tier)
			self.deduct_coins(self, tweak_data.safehouse.prices.rooms[current_tier])
		end

		managers.mission:call_global_event(Message.OnSafeHouseUpgrade)
	end

	return false
end
CustomSafehouseManager.can_afford_room_tier = function (self, room_id, tier)
	local current_tier = self.get_room_current_tier(self, room_id)

	if tier <= current_tier then
		return true
	else
		local cost = 0

		while current_tier < tier do
			current_tier = current_tier + 1
			cost = cost + tweak_data.safehouse.prices.rooms[current_tier]
		end

		return cost <= self.coins(self)
	end

	return 
end
CustomSafehouseManager.can_afford_tier = function (self, tier)
	if #tweak_data.safehouse.prices.rooms < tier then
		return false
	else
		return tweak_data.safehouse.prices.rooms[tier] <= self.coins(self)
	end

	return 
end
CustomSafehouseManager.get_highest_tier_unlocked = function (self, room_id)
	if self._global.rooms[room_id] then
		local highest_tier = 0

		for idx, tier in ipairs(self._global.rooms[room_id].unlocked_tiers) do
			if highest_tier < tier then
				highest_tier = tier
			end
		end

		return highest_tier
	end

	return false
end
CustomSafehouseManager.get_next_tier_unlocked = function (self, room_id)
	if self._global.rooms[room_id] then
		return self.get_highest_tier_unlocked(self, room_id) + 1
	end

	return false
end
CustomSafehouseManager.get_next_upgrade_cost = function (self, room_id)
	if self._global.rooms[room_id] then
		local next_tier = self.get_next_tier_unlocked(self, room_id)

		if next_tier then
			return tweak_data.safehouse.prices.rooms[next_tier]
		end
	end

	return false
end
CustomSafehouseManager.get_upgrade_cost = function (self, room_id, tier)
	if self._global.rooms[room_id] then
		local current_tier = self.get_room_current_tier(self, room_id)
		local cost = 0

		while current_tier < tier do
			current_tier = current_tier + 1
			cost = cost + tweak_data.safehouse.prices.rooms[current_tier]
		end

		return cost
	end

	return false
end
CustomSafehouseManager.can_afford_any_upgrade = function (self)
	local prices = tweak_data.safehouse.prices.rooms
	local cheapest_upgrade = prices[#prices]

	for room_id, data in pairs(self._global.rooms) do
		local tier = self.get_next_tier_unlocked(self, room_id)

		if tier and prices[tier] ~= nil and prices[tier] < cheapest_upgrade then
			cheapest_upgrade = prices[tier] or cheapest_upgrade
		end
	end

	return cheapest_upgrade <= self.coins(self)
end
CustomSafehouseManager.total_room_unlocks = function (self)
	local total = 0

	for id, data in pairs(self._global.rooms) do
		total = total + data.tier_max - data.tier_start
	end

	return total
end
CustomSafehouseManager.total_room_unlocks_purchased = function (self)
	local total = 0

	for character, data in pairs(self._global.rooms) do
		total = total + #data.unlocked_tiers - 1
	end

	return total
end
CustomSafehouseManager.avarage_level = function (self)
	local unlocked = self.total_room_unlocks_purchased(self)
	local total = self.total_room_unlocks(self)
	local level = math.clamp(math.ceil(unlocked/total/#tweak_data.safehouse.prices.rooms), 1, #tweak_data.safehouse.prices.rooms)

	return level
end
CustomSafehouseManager.get_coins_income = function (self)
	return math.floor(Application:digest_value(self._global.total, false)) - math.floor(Application:digest_value(self._global.prev_total, false))
end
CustomSafehouseManager.give_upgrade_points = function (self, exp)
	self.add_coins(self, exp/tweak_data.safehouse.rewards.experience_ratio)

	return 
end
CustomSafehouseManager.trophies = function (self)
	return self._global.trophies
end
CustomSafehouseManager.get_trophy = function (self, id)
	for idx, trophy in pairs(self._global.trophies) do
		if trophy.id == id then
			return trophy
		end
	end

	return 
end
CustomSafehouseManager.is_trophy_unlocked = function (self, id)
	local trophy = self.get_trophy(self, id)

	return (trophy and trophy.completed) or false
end
CustomSafehouseManager.is_trophy_displayed = function (self, id)
	local trophy = self.get_trophy(self, id)

	return (trophy and trophy.completed and trophy.displayed) or false
end
CustomSafehouseManager.set_trophy_displayed = function (self, id, displayed)
	if self.is_trophy_unlocked(self, id) then
		if displayed == nil then
			displayed = true
		end

		local trophy = self.get_trophy(self, id)
		trophy.displayed = displayed
	end

	return 
end
CustomSafehouseManager.get_daily = function (self, id)
	for idx, daily in pairs(tweak_data.safehouse.dailies) do
		if daily.id == id then
			return daily
		end
	end

	return 
end
CustomSafehouseManager.register_trophy_unlocked_callback = function (self, callback, id)
	self._trophy_unlocked_callbacks[id or callback] = callback

	return 
end
CustomSafehouseManager.unregister_trophy_unlocked_callback = function (self, id_or_function)
	self._trophy_unlocked_callbacks[id_or_function] = nil

	return 
end
CustomSafehouseManager.run_trophy_unlocked_callbacks = function (self, ...)
	for _, callback in pairs(self._trophy_unlocked_callbacks) do
		callback(...)
	end

	return 
end
CustomSafehouseManager._mutator_achievement_categories = {
	"complete_heist_achievements",
	"grenade_achievements",
	"enemy_kill_achievements",
	"enemy_melee_kill_achievements"
}
CustomSafehouseManager.can_progress_trophies = function (self, id)
	if not self.unlocked(self) then
		return false
	end

	if managers.mutators:are_trophies_disabled() then
		for i, achivement_category in ipairs(self._mutator_achievement_categories) do
			local achievements = tweak_data.achievement[achivement_category]

			if achievements then
				for _, achievement_data in pairs(achievements) do
					if achievement_data.trophy_stat == id then
						return achievement_data.mutators
					end
				end
			end
		end

		return false
	end

	return true
end
CustomSafehouseManager.award = function (self, id)
	self.on_achievement_awarded(self, id)
	self.on_achievement_progressed(self, id, 1)

	return 
end
CustomSafehouseManager.award_progress = function (self, id, amount)
	amount = amount or 1

	self.on_achievement_progressed(self, id, amount)

	return 
end
CustomSafehouseManager.on_achievement_awarded = function (self, id)
	if self.can_progress_trophies(self, id) then
		self.update_progress(self, "achievement_id", id)
	end

	return 
end
CustomSafehouseManager.on_achievement_progressed = function (self, progress_id, amount)
	if self.can_progress_trophies(self, progress_id) then
		self.update_progress(self, "progress_id", progress_id, amount)
	end

	return 
end
CustomSafehouseManager.update_progress = function (self, key, id, amount)
	if self.can_progress_trophies(self, id) then
		amount = amount or 1

		for idx, trophy in pairs(self._global.trophies) do
			self._update_trophy_progress(self, trophy, key, id, amount, self.complete_trophy)
		end

		self._update_trophy_progress(self, self._global.daily.trophy, key, id, amount, self.complete_daily)
	end

	return 
end
CustomSafehouseManager._update_trophy_progress = function (self, trophy, key, id, amount, complete_func)
	for obj_idx, objective in ipairs(trophy.objectives) do
		if not objective.completed and objective[key] == id then
			local pass = true

			if objective.verify then
				pass = tweak_data.safehouse[objective.verify](tweak_data.safehouse, objective)
			end

			if pass then
				objective.progress = math.floor(math.min((objective.progress or 0) + amount, objective.max_progress))
				objective.completed = objective.max_progress <= objective.progress

				for _, objective in ipairs(trophy.objectives) do
					if not objective.completed then
						pass = false

						break
					end
				end

				if pass then
					complete_func(self, trophy)

					if managers.hud then
						managers.hud:post_event("Achievement_challenge")
					end
				end

				break
			end
		end
	end

	return 
end
CustomSafehouseManager.complete_trophy = function (self, trophy_or_id)
	local trophy = (type(trophy_or_id) == "table" and trophy_or_id) or self.get_trophy(self, trophy_or_id)

	print(inspect(trophy))

	if trophy and not trophy.completed then
		trophy.completed = true

		self.add_completed_trophy(self, trophy, "trophy")

		if trophy.gives_reward == nil or trophy.gives_reward then
			self.add_coins(self, tweak_data.safehouse.rewards.challenge)
		end

		self.run_trophy_unlocked_callbacks(self, trophy.id, "trophy")
	end

	return 
end
CustomSafehouseManager.add_completed_trophy = function (self, trophy, trophy_type)
	if trophy.hidden_in_list then
		return 
	end

	local reward = 0

	if trophy_type == "trophy" then
		reward = tweak_data.safehouse.rewards.challenge
	elseif trophy_type == "daily" then
		reward = tweak_data.safehouse.rewards.daily_complete
	end

	local completed_data = {
		name = trophy.name_id,
		type = trophy_type,
		reward = reward
	}

	table.insert(self._global.completed_trophies, completed_data)

	return 
end
CustomSafehouseManager.completed_any_trophies = function (self)
	self._completed_trophies = self._completed_trophies or {}

	return 0 < #self._global.completed_trophies or 0 < #self._completed_trophies
end
CustomSafehouseManager.completed_trophies = function (self)
	self._completed_trophies = self._completed_trophies or {}

	for idx, trophy_data in ipairs(self._global.completed_trophies) do
		if not table.contains(self._completed_trophies, trophy_data) then
			table.insert(self._completed_trophies, trophy_data)
		end
	end

	return self._completed_trophies
end
CustomSafehouseManager.flush_completed_trophies = function (self)
	self._global.completed_trophies = {}

	return 
end
CustomSafehouseManager.DAILY_STATES = {
	"unstarted",
	"seen",
	"accepted",
	"completed",
	"rewarded"
}
CustomSafehouseManager.get_timestamp = ChallengeManager.get_timestamp
CustomSafehouseManager.get_daily_challenge = function (self)
	return self._global.daily
end
CustomSafehouseManager._get_daily_state = function (self)
	return self._global.daily.state
end
CustomSafehouseManager._set_daily_state = function (self, new_state)
	if table.contains(CustomSafehouseManager.DAILY_STATES, new_state) then
		self._global.daily.state = new_state
	else
		print("CustomSafehouseManager:_set_daily_state attempted to set an invalid state! ", new_state)
	end

	return 
end
CustomSafehouseManager.is_daily_new = function (self)
	return self._get_daily_state(self) == "unstarted"
end
CustomSafehouseManager.has_daily_been_accepted_from_heister = function (self)
	return self._get_daily_state(self) ~= "unstarted" and self._get_daily_state(self) ~= "seen"
end
CustomSafehouseManager.has_completed_daily = function (self)
	local complete = self._get_daily_state(self) == "completed"

	if not complete then
		complete = true

		for idx, objective in ipairs(self._global.daily.trophy.objectives) do
			objective.completed = (objective.max_progress or 1) <= (objective.progress or 0)

			if not objective.completed then
				complete = false

				break
			end
		end

		self._global.daily.trophy.completed = complete
	end

	return complete
end
CustomSafehouseManager.has_rewarded_daily = function (self)
	local is_just_completed = false

	for i, trophy in ipairs(self._global.completed_trophies) do
		if trophy.type == "daily" then
			is_just_completed = true
		end
	end

	return self._get_daily_state(self) == "rewarded" and not is_just_completed
end
CustomSafehouseManager.mark_daily_as_seen = function (self)
	if not self.has_daily_been_accepted_from_heister(self) then
		print("CustomSafehouseManager:mark_daily_as_seen()")
		self._set_daily_state(self, "seen")
	end

	return 
end
CustomSafehouseManager.accept_daily = function (self)
	if not self.has_daily_been_accepted_from_heister(self) then
		print("CustomSafehouseManager:accept_daily()")
		self._set_daily_state(self, "accepted")
	end

	return 
end
CustomSafehouseManager.complete_daily = function (self)
	if not self.unlocked(self) then
		return 
	end

	print("CustomSafehouseManager:complete_daily()")

	if not self._global.daily.trophy.completed then
		self._set_daily_state(self, "completed")

		self._global.daily.trophy.completed = true

		for _, objective in ipairs(self._global.daily.trophy.objectives) do
			objective.completed = true
			objective.progress = objective.max_progress
		end

		self.run_trophy_unlocked_callbacks(self, self._global.daily.trophy.id, "daily")

		if managers.mission then
			managers.mission:call_global_event(Message.OnDailyCompleted)
		end
	end

	return 
end
CustomSafehouseManager.reward_daily = function (self)
	if self._global.daily.trophy.completed and not self._global.daily.trophy.rewarded then
		self.add_completed_trophy(self, self._global.daily.trophy, "daily")
		self.add_coins(self, tweak_data.safehouse.rewards.daily_complete)
		self._set_daily_state(self, "rewarded")

		self._global.daily.trophy.rewarded = true

		if managers.mission then
			managers.mission:call_global_event(Message.OnDailyRewardCollected)
		end
	end

	return 
end
CustomSafehouseManager.complete_and_reward_daily = function (self)
	if not self._global.daily.trophy.completed then
		self.complete_daily(self)
		self.reward_daily(self)
	end

	return 
end
CustomSafehouseManager._get_random_daily = function (self)
	local selector = WeightedSelector:new()

	for idx, data in pairs(tweak_data.safehouse.contractors) do
		selector.add(selector, data, data.weighting)
	end

	local contractor, daily_id = nil

	while not daily_id do
		contractor = selector.select(selector)

		if 0 < #contractor.dailies then
			daily_id = contractor.dailies[#contractor.dailies]
		end
	end

	for _, daily in ipairs(tweak_data.safehouse.dailies) do
		if daily.id == daily_id then
			return daily, contractor
		end
	end

	return nil, contractor
end
CustomSafehouseManager.set_active_daily = function (self, id)
	if tweak_data.safehouse.daily_redirects[id] then
		id = tweak_data.safehouse.daily_redirects[id]
	end

	local daily = self.get_daily_challenge(self)

	if daily and daily.id ~= id and daily.tag ~= "debug" then
		self.generate_daily(self, id)
	end

	return 
end
CustomSafehouseManager.generate_daily = function (self, id, tag)
	local daily, contractor = self._get_random_daily(self)

	if id then
		for _, daily_data in ipairs(tweak_data.safehouse.dailies) do
			if daily_data.id == id then
				daily = daily_data

				for _, contractor_data in pairs(tweak_data.safehouse.contractors) do
					if table.contains(contractor_data.dailies, daily_data.id) then
						contractor = contractor_data

						break
					end
				end

				break
			end
		end
	end

	Global.custom_safehouse_manager.daily = {
		reward_id = "menu_challenge_safehouse_daily_reward",
		state = "unstarted",
		id = daily.id,
		tag = tag or nil,
		contractor = contractor.character,
		timestamp = self.get_timestamp(self),
		rewards = {
			{
				"safehouse_coins",
				tweak_data.safehouse.rewards.daily_complete
			}
		},
		trophy = deep_clone(daily)
	}

	print("CustomSafehouseManager generated new daily")
	print(inspect(Global.custom_safehouse_manager.daily))

	if managers.mission then
		managers.mission:call_global_event(Message.OnDailyGenerated, Global.custom_safehouse_manager.daily)
	end

	return 
end
CustomSafehouseManager.check_if_new_daily_available = function (self)
	local generate_new = self.interval_til_new_daily(self) < self.get_timestamp(self) - Global.custom_safehouse_manager.daily.timestamp

	if generate_new then
		self.generate_daily(self)
	end

	return generate_new
end
CustomSafehouseManager.daily_challenge_interval = function (self)
	return 23
end
CustomSafehouseManager.interval_til_new_daily = function (self)
	return (Global.custom_safehouse_manager.daily.state == "rewarded" and 16) or self.daily_challenge_interval(self)
end
CustomSafehouseManager.enable_in_game_menu = function (self, skip_safehouse_menu)
	self._should_enable_hud = not Global.hud_disabled

	managers.hud:set_disabled()
	managers.menu:open_menu("custom_safehouse_menu")

	if not skip_safehouse_menu then
		managers.menu:open_node("custom_safehouse")
	end

	return 
end
CustomSafehouseManager.disable_in_game_menu = function (self)
	if self._should_enable_hud then
		managers.hud:set_enabled()
	end

	managers.menu:close_menu("custom_safehouse_menu")

	if self._equip_data then
		managers.blackmarket:equip_weapon_in_game(self._equip_data.category, self._equip_data.slot)

		self._equip_data = nil
	end

	return 
end
CustomSafehouseManager.open_in_game_loadout = function (self, category)
	if not managers.menu:active_menu() or managers.menu:active_menu().name ~= "custom_safehouse_menu" then
		self.enable_in_game_menu(self, true)
	end

	category = category or "primaries"

	managers.menu:open_node("loadout_" .. category)

	return 
end
CustomSafehouseManager.register_equipped_weapon = function (self, data)
	self._equip_data = data

	return 
end
CustomSafehouseManager._on_enter_safe_house = function (self)
	self._global._has_entered_safehouse = true

	return 
end
CustomSafehouseManager._on_heist_completed = function (self, job_id)
	if job_id == "chill_combat" and (Network:is_server() or Global.game_settings.single_player) then
		self._set_safehouse_cooldown(self)
	end

	return 
end
CustomSafehouseManager.is_being_raided = function (self)
	if not self.unlocked(self) or not self.has_entered_safehouse(self) then
		return false
	end

	local server_time = self._get_server_time(self) or 0

	return self.SPAWN_COOLDOWN <= server_time - (self._global._spawn_cooldown or 0)
end
CustomSafehouseManager.tick_safehouse_spawn = function (self)
	if not self.unlocked(self) then
		return 
	end

	self.spawn_safehouse_contract(self)

	if not self._global._spawn_cooldown then
		if self._global._has_entered_safehouse then
			self._global._spawn_cooldown = 1
		end
	elseif self._global._spawn_cooldown == 0 then
		self._set_safehouse_cooldown(self)
	elseif self.is_being_raided(self) then
		self.spawn_safehouse_combat_contract(self)
	end

	return 
end
CustomSafehouseManager.on_exit_crimenet = function (self)
	self._has_spawned_safehouse_contract = false

	return 
end
CustomSafehouseManager._set_safehouse_cooldown = function (self)
	self._global._spawn_cooldown = Steam:server_time()

	return 
end
CustomSafehouseManager.ignore_raid = function (self)
	self.remove_combat_contract(self)
	self.spawn_safehouse_contract(self)

	self._global._spawn_cooldown = Steam:server_time() - self.SPAWN_COOLDOWN - self.IGNORE_SPAWN_COOLDOWN

	return 
end
CustomSafehouseManager._get_server_time = function (self)
	self._tick = (self._tick and self._tick + 1) or 0

	if self._tick%self.SERVER_TICK == 0 then
		self._server_time_cache = Steam:server_time()
	end

	return self._server_time_cache or 0
end
CustomSafehouseManager.spawn_safehouse_contract = function (self)
	self._has_spawned_safehouse_contract = true

	return 
end
CustomSafehouseManager.spawn_safehouse_combat_contract = function (self)
	return 
end
CustomSafehouseManager.remove_combat_contract = function (self)
	if managers.menu_component._crimenet_gui then
		managers.menu_component._crimenet_gui:remove_job("safehouse_combat", true)

		self._has_spawned_safehouse_contract = false
	end

	return 
end
CustomSafehouseManager.has_entered_safehouse = function (self)
	return self._global._has_entered_safehouse
end
CustomSafehouseManager.is_new_player = function (self)
	return self._global._new_player
end

return 
