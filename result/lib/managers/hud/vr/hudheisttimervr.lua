HUDHeistTimerVR = HUDHeistTimer
HUDHeistTimerVR.old_init = HUDHeistTimer.init
HUDHeistTimerVR.init = function (self, hud, tweak_hud)
	hud.old_panel = hud.panel
	hud.panel = managers.hud:watch_panel()

	self.old_init(self, hud, tweak_hud)

	hud.panel = hud.old_panel
	hud.old_panel = nil

	self._heist_timer_panel:set_valign("center")
	self._heist_timer_panel:set_center_y(self._hud_panel:center_y())
	self._timer_text:set_font_size(26)
	self._timer_text:set_vertical("center")

	return 
end
HUDHeistTimerVR.hide = function (self)
	self._heist_timer_panel:hide()

	return 
end
HUDHeistTimerVR.show = function (self)
	self._heist_timer_panel:show()

	return 
end

return 
