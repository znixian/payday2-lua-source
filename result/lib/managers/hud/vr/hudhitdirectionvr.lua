HUDHitDirectionVR = HUDHitDirection or class("HUDHitDirectionVR needs HUDHitDirection!")
local __init = HUDHitDirection.init
HUDHitDirectionVR.init = function (self, hud)
	local old_panel = hud.panel
	hud.panel = managers.hud:full_hmd_panel()

	__init(self, hud)

	hud.panel = old_panel
	local w, h = self._hud_panel:size()
	local size = (h < w and w) or h

	self._hit_direction_panel:set_size(size, size)
	self._hit_direction_panel:set_position(0, 0)

	return 
end
HUDHitDirectionVR._add_hit_indicator = function (self, damage_origin, damage_type, fixed_angle)
	damage_type = damage_type or HUDHitDirection.DAMAGE_TYPES.HEALTH
	local hit = self._hit_direction_panel:gradient({
		rotation = 0,
		visible = true
	})

	hit.set_center(hit, self._hud_panel:w()*0.5, self._hud_panel:h()*0.5)

	local data = {
		duration = 0.8,
		origin = damage_origin,
		damage_type = damage_type,
		fixed_angle = fixed_angle
	}

	hit.animate(hit, callback(self, self, "_animate"), data, callback(self, self, "_remove"))

	return 
end
HUDHitDirectionVR._get_indicator_intensity_multiplier = function (self, damage_type)
	if damage_type == HUDHitDirection.DAMAGE_TYPES.HEALTH and alive(managers.player:player_unit()) then
		return managers.player:player_unit():character_damage():health_ratio() - 1 + 1
	end

	return 1
end
HUDHitDirectionVR._animate = function (self, indicator, data, remove_func)
	data.t = data.duration
	data.col_start = 0.7
	data.col = 0.4
	local intensity_multiplier = self._get_indicator_intensity_multiplier(self, data.damage_type)

	while 0 < data.t do
		local dt = coroutine.yield()
		data.t = data.t - dt
		data.col = math.clamp(data.col - dt, 0, 1)

		if alive(indicator) then
			local color = self._get_indicator_color(self, data.damage_type, data.col/data.col_start):with_alpha(intensity_multiplier*0.5)

			indicator.set_gradient_points(indicator, {
				0,
				color,
				0.5,
				color.with_alpha(color, 0),
				1,
				color.with_alpha(color, 0)
			})
			indicator.set_alpha(indicator, data.t/data.duration)

			if managers.player:player_unit() then
				local ply_camera = managers.player:player_unit():camera()

				if ply_camera then
					local target_vec = ply_camera.position(ply_camera) - data.origin
					local angle = data.fixed_angle or target_vec.to_polar_with_reference(target_vec, ply_camera.forward(ply_camera), math.UP).spin
					local rounded_angle = math.round(angle/90)*90

					indicator.set_rotation(indicator, rounded_angle - 270)
				end
			end
		end
	end

	remove_func(indicator, data)

	return 
end

return 
