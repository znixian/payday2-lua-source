HUDHintVR = HUDHint
HUDHintVR.old_init = HUDHint.init
HUDHintVR.init = function (self, hud)
	hud.old_panel = hud.panel
	hud.panel = managers.hud:prompt_panel()

	self.old_init(self, hud)

	hud.panel = hud.old_panel
	hud.old_panel = nil

	return 
end

return 
