HUDChatVR = HUDChat
HUDChatVR.old_init = HUDChat.init
HUDChatVR.init = function (self, ws, hud)
	local old_panel = hud.panel
	hud.panel = managers.hud:tablet_page("left_page")

	self.old_init(self, ws, hud)
	self._panel:set_bottom(self._hud_panel:h())

	hud.panel = old_panel

	return 
end
HUDChatVR.default_layout_output_panel = HUDChat._layout_output_panel
HUDChatVR._layout_output_panel = function (self)
	self.default_layout_output_panel(self)
	self._panel:child("output_panel"):set_bottom(self._panel:h())

	return 
end
HUDChatVR._animate_fade_output = function (self)
	return 
end
HUDChatVR._animate_show_component = function (self, o)
	o.set_alpha(o, 1)

	return 
end

return 
