HUDObjectivesVR = HUDObjectives
HUDObjectivesVR.old_init = HUDObjectives.init
HUDObjectivesVR.init = function (self, hud)
	hud.old_panel = hud.panel
	hud.panel = managers.hud:tablet_page()

	self.old_init(self, hud)
	self._hud_panel:child("objectives_panel"):set_y(45)

	hud.panel = hud.old_panel
	hud.old_panel = nil

	return 
end

return 
