HUDStatsScreen = HUDStatsScreen or class()
local padding = 10
HUDStatsScreen.init = function (self)
	self._full_hud_panel = managers.hud:script(managers.hud.STATS_SCREEN_FULLSCREEN).panel

	self._full_hud_panel:clear()

	local x_margine = 10
	local y_margine = 10
	local left_panel = self._full_hud_panel:panel({
		name = "left_panel",
		valign = "scale",
		w = self._full_hud_panel:w()/3
	})

	left_panel.set_x(left_panel, -left_panel.w(left_panel))

	local blur_bg = left_panel.bitmap(left_panel, {
		texture = "guis/textures/test_blur_df",
		name = "blur_bg",
		layer = -1,
		render_template = "VertexColorTexturedBlur3D",
		valign = "scale",
		w = left_panel.w(left_panel),
		h = left_panel.h(left_panel),
		x = x_margine,
		y = y_margine,
		w = left_panel.w(left_panel) - x_margine,
		h = left_panel.h(left_panel) - y_margine*2
	})
	local leftbox = HUDBGBox_create(left_panel, {
		valign = "scale",
		x = x_margine,
		y = y_margine,
		w = left_panel.w(left_panel) - x_margine,
		h = left_panel.h(left_panel) - y_margine*2
	}, {
		blend_mode = "normal",
		color = Color.white
	})

	leftbox.child(leftbox, "bg"):set_color(Color(0, 0, 0):with_alpha(0.75))
	leftbox.child(leftbox, "bg"):set_alpha(1)

	local objectives_title = left_panel.text(left_panel, {
		name = "objectives_title",
		vertical = "top",
		h = 32,
		w = 512,
		align = "left",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.objectives_title_size,
		font = tweak_data.hud_stats.objectives_font,
		text = utf8.to_upper(managers.localization:text("hud_objective"))
	})
	local x, y = managers.gui_data:corner_safe_to_full(0, 0)

	objectives_title.set_position(objectives_title, math.round(x), math.round(y))
	objectives_title.set_valign(objectives_title, {
		math.round(y)/managers.gui_data:full_scaled_size().h,
		0
	})
	managers.hud:make_fine_text(objectives_title)

	local pad = 8
	local objectives_panel = left_panel.panel(left_panel, {
		name = "objectives_panel",
		layer = 1,
		x = math.round(objectives_title.x(objectives_title) + pad),
		y = math.round(objectives_title.bottom(objectives_title)),
		w = left_panel.w(left_panel) - (objectives_title.x(objectives_title) + pad)
	})

	objectives_panel.set_valign(objectives_panel, {
		math.round(y)/managers.gui_data:full_scaled_size().h,
		0
	})

	local loot_wrapper_panel = left_panel.panel(left_panel, {
		name = "loot_wrapper_panel",
		visible = true,
		x = 0,
		layer = 1,
		y = math.round(managers.gui_data:full_scaled_size().height/2) + 0,
		h = math.round(managers.gui_data:full_scaled_size().height/2),
		w = left_panel.w(left_panel)
	})

	loot_wrapper_panel.set_valign(loot_wrapper_panel, "center")

	local secured_loot_title = loot_wrapper_panel.text(loot_wrapper_panel, {
		valign = "center",
		name = "secured_loot_title",
		h = 32,
		w = 512,
		align = "left",
		vertical = "top",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_title_size,
		font = tweak_data.hud_stats.objectives_font,
		text = utf8.to_upper(managers.localization:text("hud_secured_loot"))
	})

	secured_loot_title.set_position(secured_loot_title, math.round(x), 0)
	managers.hud:make_fine_text(secured_loot_title)

	local mission_bags_title = loot_wrapper_panel.text(loot_wrapper_panel, {
		valign = "center",
		name = "mission_bags_title",
		h = 32,
		w = 512,
		align = "left",
		vertical = "top",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font,
		text = utf8.to_upper(managers.localization:text("hud_mission_bags"))
	})

	mission_bags_title.set_position(mission_bags_title, math.round(x + pad), secured_loot_title.bottom(secured_loot_title))
	managers.hud:make_fine_text(mission_bags_title)

	local mission_bags_panel = loot_wrapper_panel.panel(loot_wrapper_panel, {
		y = 0,
		name = "mission_bags_panel",
		h = 44,
		visible = true,
		x = 0,
		w = left_panel.w(left_panel)
	})

	mission_bags_panel.set_lefttop(mission_bags_panel, mission_bags_title.leftbottom(mission_bags_title))
	mission_bags_panel.set_position(mission_bags_panel, mission_bags_panel.x(mission_bags_panel), mission_bags_panel.y(mission_bags_panel))

	local mission_bags_payout = loot_wrapper_panel.text(loot_wrapper_panel, {
		valign = "center",
		name = "mission_bags_payout",
		h = 32,
		w = 512,
		align = "left",
		vertical = "top",
		text = "",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font
	})

	mission_bags_payout.set_text(mission_bags_payout, utf8.to_upper(managers.localization:text("hud_bonus_bags_payout", {
		MONEY = managers.experience:cash_string(0)
	})))
	mission_bags_payout.set_position(mission_bags_payout, mission_bags_title.left(mission_bags_title), mission_bags_panel.bottom(mission_bags_panel))
	managers.hud:make_fine_text(mission_bags_payout)
	mission_bags_payout.set_w(mission_bags_payout, loot_wrapper_panel.w(loot_wrapper_panel))

	local bonus_bags_title = loot_wrapper_panel.text(loot_wrapper_panel, {
		valign = "center",
		name = "bonus_bags_title",
		h = 32,
		w = 512,
		align = "left",
		vertical = "top",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font,
		text = utf8.to_upper(managers.localization:text("hud_bonus_bags"))
	})

	bonus_bags_title.set_position(bonus_bags_title, math.round(x + pad), mission_bags_payout.bottom(mission_bags_payout) + 4)
	managers.hud:make_fine_text(bonus_bags_title)

	local bonus_bags_panel = loot_wrapper_panel.panel(loot_wrapper_panel, {
		y = 0,
		name = "bonus_bags_panel",
		h = 44,
		visible = true,
		x = 0,
		w = left_panel.w(left_panel)
	})

	bonus_bags_panel.set_lefttop(bonus_bags_panel, bonus_bags_title.leftbottom(bonus_bags_title))
	bonus_bags_panel.set_position(bonus_bags_panel, bonus_bags_panel.x(bonus_bags_panel), bonus_bags_panel.y(bonus_bags_panel))
	bonus_bags_panel.grow(bonus_bags_panel, -bonus_bags_panel.x(bonus_bags_panel), 0)

	local bonus_bags_payout = loot_wrapper_panel.text(loot_wrapper_panel, {
		valign = "center",
		name = "bonus_bags_payout",
		h = 32,
		w = 512,
		align = "left",
		vertical = "top",
		text = "",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font
	})

	bonus_bags_payout.set_text(bonus_bags_payout, utf8.to_upper(managers.localization:text("hud_bonus_bags_payout", {
		MONEY = managers.experience:cash_string(0)
	})))
	bonus_bags_payout.set_position(bonus_bags_payout, bonus_bags_title.left(bonus_bags_title), bonus_bags_panel.bottom(bonus_bags_panel))
	managers.hud:make_fine_text(bonus_bags_payout)
	bonus_bags_payout.set_w(bonus_bags_payout, loot_wrapper_panel.w(loot_wrapper_panel))

	local instant_cash_title = loot_wrapper_panel.text(loot_wrapper_panel, {
		valign = "center",
		name = "instant_cash_title",
		h = 32,
		w = 512,
		align = "left",
		vertical = "top",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font,
		text = utf8.to_upper(managers.localization:text("hud_instant_cash"))
	})

	instant_cash_title.set_position(instant_cash_title, math.round(x + pad), bonus_bags_payout.bottom(bonus_bags_payout) + 4)
	managers.hud:make_fine_text(instant_cash_title)

	local instant_cash_text = loot_wrapper_panel.text(loot_wrapper_panel, {
		valign = "center",
		name = "instant_cash_text",
		h = 32,
		w = 512,
		align = "left",
		vertical = "top",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font,
		text = managers.experience:cash_string(0)
	})

	instant_cash_text.set_position(instant_cash_text, instant_cash_title.left(instant_cash_title), instant_cash_title.bottom(instant_cash_title))
	managers.hud:make_fine_text(instant_cash_text)
	instant_cash_text.set_w(instant_cash_text, loot_wrapper_panel.w(loot_wrapper_panel))

	local challenges_wrapper_panel = left_panel.panel(left_panel, {
		layer = 1,
		name = "challenges_wrapper_panel",
		visible = false,
		x = 0,
		valign = {
			0.5,
			0.5
		},
		y = y + math.round(managers.gui_data:scaled_size().height/2),
		h = math.round(managers.gui_data:scaled_size().height/2),
		w = left_panel.w(left_panel)
	})
	local _, by = managers.gui_data:corner_safe_to_full(0, managers.gui_data:corner_scaled_size().height)

	challenges_wrapper_panel.set_bottom(challenges_wrapper_panel, by)
	challenges_wrapper_panel.set_valign(challenges_wrapper_panel, {
		by/managers.gui_data:full_scaled_size().h,
		0
	})

	local last_completed_challenge_title = challenges_wrapper_panel.text(challenges_wrapper_panel, {
		valign = "center",
		name = "last_completed_challenge_title",
		h = 32,
		text_id = "victory_last_completed_challenge",
		align = "left",
		w = 512,
		vertical = "top",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.objectives_title_size,
		font = tweak_data.hud_stats.objectives_font
	})

	last_completed_challenge_title.set_position(last_completed_challenge_title, math.round(x), 0)
	managers.hud:make_fine_text(last_completed_challenge_title)

	local challenges_panel = challenges_wrapper_panel.panel(challenges_wrapper_panel, {
		valign = "center",
		name = "challenges_panel",
		layer = 1,
		x = math.round(objectives_title.x(objectives_title) + pad),
		y = last_completed_challenge_title.bottom(last_completed_challenge_title),
		w = left_panel.w(left_panel) - (last_completed_challenge_title.x(last_completed_challenge_title) + pad)
	})
	local near_completion_title = challenges_wrapper_panel.text(challenges_wrapper_panel, {
		valign = "center",
		name = "near_completion_title",
		h = 32,
		text_id = "menu_near_completion_challenges",
		align = "left",
		w = 512,
		vertical = "top",
		layer = 1,
		color = Color.white,
		font_size = tweak_data.hud_stats.objectives_title_size,
		font = tweak_data.hud_stats.objectives_font
	})

	near_completion_title.set_position(near_completion_title, math.round(x), math.round(challenges_wrapper_panel.h(challenges_wrapper_panel)/3))
	managers.hud:make_fine_text(near_completion_title)

	local near_completion_panel = challenges_wrapper_panel.panel(challenges_wrapper_panel, {
		valign = "center",
		name = "near_completion_panel",
		layer = 1,
		x = math.round(objectives_title.x(objectives_title) + pad),
		y = near_completion_title.bottom(near_completion_title),
		w = left_panel.w(left_panel) - (near_completion_title.x(near_completion_title) + pad)
	})
	local bottom_panel = self._full_hud_panel:panel({
		name = "bottom_panel",
		h = y + 90 + 24,
		w = self._full_hud_panel:w()/3 - x_margine*2
	})

	bottom_panel.set_y(bottom_panel, self._full_hud_panel:h())
	bottom_panel.set_x(bottom_panel, self._full_hud_panel:w()/3 + x_margine)

	local blur_bg = bottom_panel.bitmap(bottom_panel, {
		texture = "guis/textures/test_blur_df",
		name = "blur_bg",
		layer = -1,
		render_template = "VertexColorTexturedBlur3D",
		valign = "scale",
		w = bottom_panel.w(bottom_panel),
		h = bottom_panel.h(bottom_panel),
		h = bottom_panel.h(bottom_panel) - y_margine
	})
	local bottombox = HUDBGBox_create(bottom_panel, {
		valign = "scale",
		h = bottom_panel.h(bottom_panel) - y_margine
	}, {
		blend_mode = "normal",
		color = Color.white
	})

	bottombox.child(bottombox, "bg"):set_color(Color(0, 0, 0):with_alpha(0.6))
	bottombox.child(bottombox, "bg"):set_alpha(1)

	local right_panel = self._full_hud_panel:panel({
		name = "right_panel",
		valign = "scale",
		w = self._full_hud_panel:w()/3
	})

	right_panel.set_x(right_panel, self._full_hud_panel:w())

	local blur_bg = right_panel.bitmap(right_panel, {
		texture = "guis/textures/test_blur_df",
		name = "blur_bg",
		layer = -1,
		render_template = "VertexColorTexturedBlur3D",
		valign = "scale",
		w = right_panel.w(right_panel),
		h = right_panel.h(right_panel),
		y = y_margine,
		h = right_panel.h(right_panel) - y_margine*2,
		w = right_panel.w(right_panel) - x_margine
	})
	local rightbox = HUDBGBox_create(right_panel, {
		valign = "scale",
		y = y_margine,
		h = right_panel.h(right_panel) - y_margine*2,
		w = right_panel.w(right_panel) - x_margine
	}, {
		blend_mode = "normal",
		color = Color.white
	})

	rightbox.child(rightbox, "bg"):set_color(Color(0, 0, 0):with_alpha(0.75))
	rightbox.child(rightbox, "bg"):set_alpha(1)

	local days_title = right_panel.text(right_panel, {
		vertical = "top",
		name = "days_title",
		h = 32,
		w = 512,
		align = "left",
		text = "DAY 1 OF 3",
		x = 20,
		layer = 1,
		y = y,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font
	})

	managers.hud:make_fine_text(days_title)
	days_title.set_w(days_title, right_panel.w(right_panel))

	local day_wrapper_panel = right_panel.panel(right_panel, {
		name = "day_wrapper_panel",
		visible = true,
		x = 0,
		layer = 1,
		y = y + math.round(managers.gui_data:scaled_size().height/2),
		h = math.round(managers.gui_data:scaled_size().height),
		w = right_panel.w(right_panel)
	})

	day_wrapper_panel.set_position(day_wrapper_panel, days_title.x(days_title) + pad, days_title.bottom(days_title))
	day_wrapper_panel.set_w(day_wrapper_panel, right_panel.w(right_panel) - x - day_wrapper_panel.x(day_wrapper_panel))

	local day_title = day_wrapper_panel.text(day_wrapper_panel, {
		vertical = "top",
		name = "day_title",
		h = 32,
		w = 512,
		align = "left",
		text = "BLUH!",
		y = 0,
		x = 0,
		layer = 0,
		color = Color.white,
		font_size = tweak_data.hud_stats.objectives_title_size,
		font = tweak_data.hud_stats.objectives_font
	})

	managers.hud:make_fine_text(day_title)
	day_title.set_w(day_title, day_wrapper_panel.w(day_wrapper_panel))

	local paygrade_title = day_wrapper_panel.text(day_wrapper_panel, {
		name = "paygrade_title",
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.hud_stats.loot_size,
		text = managers.localization:to_upper_text("menu_lobby_difficulty_title"),
		color = tweak_data.screen_colors.text
	})

	managers.hud:make_fine_text(paygrade_title)
	paygrade_title.set_top(paygrade_title, math.round(day_title.bottom(day_title)))

	local job_data = managers.job:current_job_data()

	if job_data then
		local job_stars = managers.job:current_job_stars()
		local job_and_difficulty_stars = managers.job:current_job_and_difficulty_stars()
		local difficulty_stars = managers.job:current_difficulty_stars()
		local difficulty = tweak_data.difficulties[difficulty_stars + 2] or 1
		local difficulty_string_id = tweak_data.difficulty_name_ids[difficulty]
		local difficulty_string = managers.localization:to_upper_text(difficulty_string_id)
		local difficulty_color = tweak_data.screen_colors.risk

		if managers.crime_spree:is_active() then
			difficulty_string = managers.localization:text("menu_cs_level", {
				level = managers.experience:cash_string(managers.crime_spree:server_spree_level(), "")
			})
			difficulty_color = tweak_data.screen_colors.crime_spree_risk
		end

		local risk_text = day_wrapper_panel.text(day_wrapper_panel, {
			name = "risk_text",
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.hud_stats.loot_size,
			text = difficulty_string,
			color = tweak_data.screen_colors.text
		})

		managers.hud:make_fine_text(risk_text)
		risk_text.set_top(risk_text, math.round(paygrade_title.top(paygrade_title)))
		risk_text.set_left(risk_text, paygrade_title.right(paygrade_title) + 8)

		if 0 < difficulty_stars then
			risk_text.set_color(risk_text, difficulty_color)
		end
	end

	local day_payout = day_wrapper_panel.text(day_wrapper_panel, {
		vertical = "top",
		name = "day_payout",
		h = 32,
		w = 512,
		align = "left",
		text = "BLUH!",
		y = 0,
		x = 0,
		layer = 0,
		color = Color.white,
		font_size = tweak_data.hud_stats.loot_size,
		font = tweak_data.hud_stats.objectives_font
	})

	day_payout.set_text(day_payout, utf8.to_upper(managers.localization:text("hud_day_payout", {
		MONEY = managers.experience:cash_string(0)
	})))
	managers.hud:make_fine_text(day_payout)
	day_payout.set_w(day_payout, day_wrapper_panel.w(day_wrapper_panel))
	day_payout.set_y(day_payout, math.round(paygrade_title.bottom(paygrade_title)))

	local bains_plan = day_wrapper_panel.text(day_wrapper_panel, {
		name = "bains_plan",
		vertical = "top",
		h = 128,
		wrap = true,
		align = "left",
		word_wrap = true,
		text = managers.localization:to_upper_text("menu_description"),
		font = tweak_data.hud_stats.objective_desc_font,
		font_size = tweak_data.hud_stats.day_description_size + 2,
		color = Color(1, 1, 1, 1)
	})

	managers.hud:make_fine_text(bains_plan)
	bains_plan.set_y(bains_plan, math.round(day_payout.bottom(day_payout) + 20))

	local day_description = day_wrapper_panel.text(day_wrapper_panel, {
		name = "day_description",
		vertical = "top",
		h = 128,
		wrap = true,
		align = "left",
		word_wrap = true,
		text = "sdsd",
		font = tweak_data.hud_stats.objective_desc_font,
		font_size = tweak_data.hud_stats.day_description_size,
		color = Color(1, 1, 1, 1)
	})

	day_description.set_y(day_description, math.round(bains_plan.bottom(bains_plan)))
	day_description.set_h(day_description, day_wrapper_panel.h(day_wrapper_panel))

	local is_level_ghostable = managers.job:is_level_ghostable(managers.job:current_level_id())
	local is_whisper_mode = managers.groupai and managers.groupai:state():whisper_mode()
	local ghost_icon = right_panel.bitmap(right_panel, {
		texture = "guis/textures/pd2/cn_minighost",
		name = "ghost_icon",
		h = 16,
		blend_mode = "add",
		w = 16
	})
	local ghost_string = managers.localization:text("menu_ghostable_stage")

	if managers.job:is_level_ghostable_required(managers.job:current_level_id()) then
		ghost_string = managers.localization:text("menu_ghostable_stage_required")
	end

	local ghostable_text = day_wrapper_panel.text(day_wrapper_panel, {
		vertical = "top",
		name = "ghostable_text",
		blend_mode = "add",
		align = "left",
		text = ghost_string,
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.text
	})
	local x, y, w, h = ghostable_text.text_rect(ghostable_text)

	ghostable_text.set_size(ghostable_text, w, h)
	ghost_icon.set_left(ghost_icon, days_title.right(days_title))
	ghost_icon.set_center_y(ghost_icon, days_title.center_y(days_title))
	ghostable_text.set_y(ghostable_text, day_description.top(day_description) + 10)
	ghostable_text.set_left(ghostable_text, day_description.left(day_description))
	ghost_icon.set_visible(ghost_icon, is_level_ghostable)
	ghost_icon.set_color(ghost_icon, (is_whisper_mode and Color.white) or tweak_data.screen_colors.important_1)
	ghostable_text.set_visible(ghostable_text, is_level_ghostable and is_whisper_mode)

	local ext_inventory_panel = right_panel.panel(right_panel, {
		name = "ext_inventory_panel",
		x = 20,
		layer = 1,
		valign = {
			0.5,
			0.5
		},
		y = y + math.round(managers.gui_data:scaled_size().height/2),
		h = math.round(managers.gui_data:scaled_size().height/2),
		w = left_panel.w(left_panel)
	})

	ext_inventory_panel.set_w(ext_inventory_panel, right_panel.w(right_panel) - x - ext_inventory_panel.x(ext_inventory_panel))

	local _, by = managers.gui_data:corner_safe_to_full(0, managers.gui_data:corner_scaled_size().height)

	ext_inventory_panel.set_bottom(ext_inventory_panel, by)
	ext_inventory_panel.set_valign(ext_inventory_panel, {
		by/managers.gui_data:full_scaled_size().h,
		0
	})

	local mutators_panel = right_panel.panel(right_panel, {
		name = "mutators_panel",
		h = 0,
		x = 20,
		layer = 1,
		valign = {
			0.5,
			0.5
		},
		y = y + math.round(managers.gui_data:scaled_size().height/2),
		w = left_panel.w(left_panel)
	})

	mutators_panel.set_w(mutators_panel, right_panel.w(right_panel) - x - mutators_panel.x(mutators_panel))
	mutators_panel.set_bottom(mutators_panel, ext_inventory_panel.top(ext_inventory_panel))
	mutators_panel.set_valign(mutators_panel, {
		by/managers.gui_data:full_scaled_size().h,
		0
	})

	local profile_wrapper_panel = bottom_panel.panel(bottom_panel, {
		name = "profile_wrapper_panel",
		layer = 1,
		valign = {
			0.5,
			0.5
		},
		x = x_margine,
		y = y_margine,
		h = math.round(bottom_panel.h(bottom_panel) - y - y_margine),
		w = bottom_panel.w(bottom_panel) - x_margine*2
	})

	self._rec_round_object(self, left_panel)
	self._rec_round_object(self, right_panel)
	self._rec_round_object(self, bottom_panel)

	return 
end
HUDStatsScreen._rec_round_object = function (self, object)
	if object.children then
		for i, d in ipairs(object.children(object)) do
			self._rec_round_object(self, d)
		end
	end

	local x, y = object.position(object)

	object.set_position(object, math.round(x), math.round(y))

	return 
end
HUDStatsScreen.show = function (self)
	local safe = managers.hud.STATS_SCREEN_SAFERECT
	local full = managers.hud.STATS_SCREEN_FULLSCREEN

	managers.hud:show(full)

	local left_panel = self._full_hud_panel:child("left_panel")
	local right_panel = self._full_hud_panel:child("right_panel")
	local bottom_panel = self._full_hud_panel:child("bottom_panel")

	left_panel.stop(left_panel)
	self._create_stats_screen_profile(self, bottom_panel.child(bottom_panel, "profile_wrapper_panel"))
	self._create_stats_screen_objectives(self, left_panel.child(left_panel, "objectives_panel"))
	self._create_stats_ext_inventory(self, right_panel.child(right_panel, "ext_inventory_panel"))
	self._create_mutators_list(self, right_panel.child(right_panel, "mutators_panel"))
	self._update_stats_screen_loot(self, left_panel.child(left_panel, "loot_wrapper_panel"))
	self._update_stats_screen_day(self, right_panel)

	local teammates_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("teammates_panel")
	local objectives_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("objectives_panel")
	local chat_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("chat_panel")

	left_panel.animate(left_panel, callback(self, self, "_animate_show_stats_left_panel"), right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)

	self._showing_stats_screen = true

	if managers.groupai:state() and not self._whisper_listener then
		self._whisper_listener = "HUDStatsScreen_whisper_mode"

		managers.groupai:state():add_listener(self._whisper_listener, {
			"whisper_mode"
		}, callback(self, self, "on_whisper_mode_changed"))
	end

	return 
end
HUDStatsScreen.hide = function (self)
	self._showing_stats_screen = false
	local safe = managers.hud.STATS_SCREEN_SAFERECT
	local full = managers.hud.STATS_SCREEN_FULLSCREEN

	if not managers.hud:exists(safe) then
		return 
	end

	managers.hud:hide(safe)

	local left_panel = self._full_hud_panel:child("left_panel")
	local right_panel = self._full_hud_panel:child("right_panel")
	local bottom_panel = self._full_hud_panel:child("bottom_panel")

	left_panel.stop(left_panel)

	local teammates_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("teammates_panel")
	local objectives_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("objectives_panel")
	local chat_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel:child("chat_panel")

	left_panel.animate(left_panel, callback(self, self, "_animate_hide_stats_left_panel"), right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)

	if managers.groupai:state() and self._whisper_listener then
		managers.groupai:state():remove_listener(self._whisper_listener)

		self._whisper_listener = nil
	end

	return 
end
HUDStatsScreen._create_stats_screen_objectives = function (self, panel)
	panel.clear(panel)

	local x = 0
	local y = 0
	local panel_w = panel.w(panel) - x

	for i, data in pairs(managers.objectives:get_active_objectives()) do
		local obj_panel = panel.panel(panel, {
			name = "obj_panel",
			x = x,
			y = y,
			w = panel_w
		})
		local obj_title = obj_panel.text(obj_panel, {
			name = "title",
			vertical = "top",
			wrap = true,
			align = "left",
			word_wrap = true,
			text = utf8.to_upper(data.text),
			font = tweak_data.hud.medium_font,
			font_size = tweak_data.hud.active_objective_title_font_size,
			color = Color.white,
			h = tweak_data.hud.active_objective_title_font_size
		})

		managers.hud:make_fine_text(obj_title)
		obj_title.set_w(obj_title, obj_title.w(obj_title) + 1)

		local obj_description = obj_panel.text(obj_panel, {
			name = "description",
			vertical = "top",
			h = 128,
			wrap = true,
			font_size = 24,
			align = "left",
			word_wrap = true,
			text = data.description,
			font = tweak_data.hud_stats.objective_desc_font,
			color = Color(1, 1, 1, 1)
		})

		managers.hud:make_fine_text(obj_description)
		obj_description.set_h(obj_description, obj_description.h(obj_description) + 10)
		obj_panel.set_h(obj_panel, obj_title.h(obj_title) + obj_description.h(obj_description))
		obj_description.set_lefttop(obj_description, obj_title.leftbottom(obj_title))

		y = math.ceil(y + obj_panel.h(obj_panel))
	end

	return 
end
HUDStatsScreen._create_stats_screen_profile = function (self, profile_wrapper_panel)
	profile_wrapper_panel.stop(profile_wrapper_panel)
	profile_wrapper_panel.clear(profile_wrapper_panel)

	local next_level_data = managers.experience:next_level_data() or {}
	local bg_ring = profile_wrapper_panel.bitmap(profile_wrapper_panel, {
		texture = "guis/textures/pd2/level_ring_small",
		w = 64,
		h = 64,
		alpha = 0.4,
		color = Color.black
	})
	local exp_ring = profile_wrapper_panel.bitmap(profile_wrapper_panel, {
		texture = "guis/textures/pd2/level_ring_small",
		h = 64,
		render_template = "VertexColorTexturedRadial",
		w = 64,
		blend_mode = "add",
		rotation = 360,
		layer = 1,
		color = Color((next_level_data.current_points or 1)/(next_level_data.points or 1), 1, 1)
	})

	bg_ring.set_bottom(bg_ring, profile_wrapper_panel.h(profile_wrapper_panel))
	exp_ring.set_bottom(exp_ring, profile_wrapper_panel.h(profile_wrapper_panel))

	local gain_xp = managers.experience:get_xp_dissected(true, 0, true)
	local at_max_level = managers.experience:current_level() == managers.experience:level_cap()
	local can_lvl_up = not at_max_level and next_level_data.points - next_level_data.current_points <= gain_xp
	local progress = (next_level_data.current_points or 1)/(next_level_data.points or 1)
	local gain_progress = (gain_xp or 1)/(next_level_data.points or 1)
	local exp_gain_ring = profile_wrapper_panel.bitmap(profile_wrapper_panel, {
		texture = "guis/textures/pd2/level_ring_potential_small",
		h = 64,
		render_template = "VertexColorTexturedRadial",
		w = 64,
		blend_mode = "normal",
		rotation = 360,
		layer = 2,
		color = Color(gain_progress, 1, 0)
	})

	exp_gain_ring.rotate(exp_gain_ring, progress*360)
	exp_gain_ring.set_center(exp_gain_ring, exp_ring.center(exp_ring))

	local level_text = profile_wrapper_panel.text(profile_wrapper_panel, {
		name = "level_text",
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.hud_stats.day_description_size,
		text = tostring(managers.experience:current_level()),
		color = tweak_data.screen_colors.text
	})

	managers.hud:make_fine_text(level_text)
	level_text.set_center(level_text, exp_ring.center(exp_ring))

	if at_max_level then
		local text = managers.localization:to_upper_text("hud_at_max_level")
		local at_max_level_text = profile_wrapper_panel.text(profile_wrapper_panel, {
			name = "at_max_level_text",
			text = text,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font,
			color = tweak_data.hud_stats.potential_xp_color
		})

		managers.hud:make_fine_text(at_max_level_text)
		at_max_level_text.set_left(at_max_level_text, math.round(exp_ring.right(exp_ring) + 4))
		at_max_level_text.set_center_y(at_max_level_text, math.round(exp_ring.center_y(exp_ring)) + 0)
	else
		local next_level_in = profile_wrapper_panel.text(profile_wrapper_panel, {
			text = "",
			name = "next_level_in",
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font,
			color = tweak_data.screen_colors.text
		})
		local points = next_level_data.points - next_level_data.current_points

		next_level_in.set_text(next_level_in, utf8.to_upper(managers.localization:text("menu_es_next_level") .. " " .. managers.money:add_decimal_marks_to_string(tostring(points))))
		managers.hud:make_fine_text(next_level_in)
		next_level_in.set_left(next_level_in, math.round(exp_ring.right(exp_ring) + 4))
		next_level_in.set_center_y(next_level_in, math.round(exp_ring.center_y(exp_ring)) - 20)

		local text = managers.localization:to_upper_text("hud_potential_xp", {
			XP = managers.money:add_decimal_marks_to_string(tostring(gain_xp))
		})
		local gain_xp_text = profile_wrapper_panel.text(profile_wrapper_panel, {
			name = "gain_xp_text",
			text = text,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font,
			color = tweak_data.hud_stats.potential_xp_color
		})

		managers.hud:make_fine_text(gain_xp_text)
		gain_xp_text.set_left(gain_xp_text, math.round(exp_ring.right(exp_ring) + 4))
		gain_xp_text.set_center_y(gain_xp_text, math.round(exp_ring.center_y(exp_ring)) + 0)

		if can_lvl_up then
			local text = managers.localization:to_upper_text("hud_potential_level_up")
			local potential_level_up_text = profile_wrapper_panel.text(profile_wrapper_panel, {
				vertical = "center",
				name = "potential_level_up_text",
				blend_mode = "normal",
				align = "left",
				layer = 3,
				visible = can_lvl_up,
				text = text,
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_small_font,
				color = tweak_data.hud_stats.potential_xp_color
			})

			managers.hud:make_fine_text(potential_level_up_text)
			potential_level_up_text.set_left(potential_level_up_text, math.round(exp_ring.right(exp_ring) + 4))
			potential_level_up_text.set_center_y(potential_level_up_text, math.round(exp_ring.center_y(exp_ring)) + 20)
			potential_level_up_text.animate(potential_level_up_text, callback(self, self, "_animate_text_pulse"), exp_gain_ring, exp_ring)
		end
	end

	local track_text = profile_wrapper_panel.text(profile_wrapper_panel, {
		name = "track_text",
		y = 4,
		x = 4,
		text = utf8.to_upper(managers.localization:text("menu_es_playing_track")) .. " " .. managers.music:current_track_string(),
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.text
	})

	managers.hud:make_fine_text(track_text)

	return 
end
HUDStatsScreen.on_whisper_mode_changed = function (self)
	local is_level_ghostable = managers.job:is_level_ghostable(managers.job:current_level_id()) and managers.groupai and managers.groupai:state():whisper_mode()
	local right_panel = self._full_hud_panel:child("right_panel")
	local day_wrapper_panel = right_panel.child(right_panel, "day_wrapper_panel")
	local ghost_icon = right_panel.child(right_panel, "ghost_icon")
	local ghostable_text = day_wrapper_panel.child(day_wrapper_panel, "ghostable_text")

	if alive(ghost_icon) and alive(ghostable_text) then
		ghost_icon.set_color(ghost_icon, tweak_data.screen_colors.important_1)
		ghostable_text.set_visible(ghostable_text, is_level_ghostable)
	end

	return 
end
HUDStatsScreen.on_ext_inventory_changed = function (self)
	local right_panel = self._full_hud_panel:child("right_panel")

	if not alive(right_panel) then
		return 
	end

	local ext_inventory_panel = right_panel.child(right_panel, "ext_inventory_panel")

	if not alive(ext_inventory_panel) then
		return 
	end

	self._create_stats_ext_inventory(self, ext_inventory_panel)

	return 
end
HUDStatsScreen._create_stats_ext_inventory = function (self, ext_inventory_panel)
	ext_inventory_panel.clear(ext_inventory_panel)

	local eq_h = ((PlayerBase.USE_GRENADES and 3) or 2)/64
	local eq_w = 48
	local equipment = {
		{
			icon = "equipment_body_bag",
			text = managers.localization:to_upper_text("hud_body_bags"),
			amount = managers.player:get_body_bags_amount()
		}
	}

	ext_inventory_panel.set_h(ext_inventory_panel, #equipment*eq_h + tweak_data.hud_stats.loot_title_size + 4)

	local y = nil

	for i, eq in ipairs(equipment) do
		y = ext_inventory_panel.h(ext_inventory_panel) - eq_h*i - (i - 1)*2
		local panel = ext_inventory_panel.panel(ext_inventory_panel, {
			layer = 1,
			name = "panel" .. i,
			w = eq_w,
			h = eq_h,
			x = ext_inventory_panel.w(ext_inventory_panel) - eq_w,
			y = y
		})
		local icon, texture_rect = tweak_data.hud_icons:get_icon_data(eq.icon)
		local image = panel.bitmap(panel, {
			name = "image",
			layer = 1,
			visible = true,
			texture = icon,
			texture_rect = texture_rect,
			color = Color.white,
			w = panel.h(panel),
			h = panel.h(panel),
			x = -(panel.h(panel) - panel.h(panel))/2,
			y = -(panel.h(panel) - panel.h(panel))/2
		})
		local amount = panel.text(panel, {
			name = "amount",
			vertical = "center",
			font_size = 22,
			align = "right",
			y = 2,
			font = "fonts/font_medium_mf",
			visible = true,
			x = -2,
			layer = 2,
			text = tostring(13),
			color = Color.white,
			w = panel.w(panel),
			h = panel.h(panel)
		})

		self._set_amount_string(self, amount, eq.amount)

		local text = ext_inventory_panel.text(ext_inventory_panel, {
			vertical = "center",
			font_size = 22,
			align = "right",
			y = 2,
			font = "fonts/font_medium_mf",
			visible = true,
			x = -2,
			layer = 2,
			name = "text" .. i,
			text = eq.text,
			color = Color.white,
			w = panel.w(panel),
			h = panel.h(panel)
		})

		managers.hud:make_fine_text(text)
		text.set_y(text, math.round(panel.center_y(panel) - text.h(text)/2) + 2)
		text.set_right(text, math.round(panel.left(panel) - 8))
	end

	local title = ext_inventory_panel.text(ext_inventory_panel, {
		name = "title",
		vertical = "center",
		align = "right",
		y = 2,
		visible = true,
		x = -2,
		layer = 2,
		text = managers.localization:to_upper_text("hud_extended_inventory"),
		font_size = tweak_data.hud_stats.loot_title_size,
		font = tweak_data.hud_stats.objectives_font,
		color = Color.white,
		w = ext_inventory_panel.w(ext_inventory_panel),
		h = ext_inventory_panel.h(ext_inventory_panel)
	})

	managers.hud:make_fine_text(title)
	title.set_y(title, y - title.h(title) - 4)
	title.set_right(title, math.round(ext_inventory_panel.w(ext_inventory_panel)))

	local _, by = managers.gui_data:corner_safe_to_full(0, managers.gui_data:corner_scaled_size().height)

	ext_inventory_panel.set_bottom(ext_inventory_panel, by)
	ext_inventory_panel.set_valign(ext_inventory_panel, {
		by/managers.gui_data:full_scaled_size().h,
		0
	})

	return 
end
HUDStatsScreen._set_amount_string = function (self, text, amount)
	local zero = (amount < 10 and "0") or ""

	text.set_text(text, zero .. amount)
	text.set_range_color(text, 0, string.len((amount == 0 and text.text(text)) or zero), Color.white:with_alpha(0.5))

	return 
end
HUDStatsScreen._create_mutators_list = function (self, mutators_panel)
	mutators_panel.clear(mutators_panel)

	if not managers.mutators:are_mutators_active() then
		return 
	end

	local y = 2
	local title = mutators_panel.text(mutators_panel, {
		name = "title",
		vertical = "center",
		align = "right",
		visible = true,
		x = -2,
		layer = 2,
		text = managers.localization:to_upper_text("menu_mutators"),
		font_size = tweak_data.hud_stats.loot_title_size,
		font = tweak_data.hud_stats.objectives_font,
		color = Color.white,
		y = y,
		w = mutators_panel.w(mutators_panel),
		h = tweak_data.hud_stats.loot_title_size
	})

	managers.hud:make_fine_text(title)
	title.set_right(title, math.round(mutators_panel.w(mutators_panel)))

	y = y + title.h(title)

	for i, active_mutator in ipairs(managers.mutators:active_mutators()) do
		local mutator_text = mutators_panel.text(mutators_panel, {
			vertical = "top",
			align = "right",
			x = 0,
			layer = 1,
			name = "mutator_" .. tostring(i),
			color = Color.white,
			font_size = tweak_data.hud_stats.day_description_size,
			font = tweak_data.hud_stats.objectives_font,
			text = active_mutator.mutator:name(),
			w = mutators_panel.w(mutators_panel),
			h = tweak_data.hud_stats.day_description_size,
			y = y
		})
		y = y + mutator_text.h(mutator_text) + 2
	end

	mutators_panel.set_h(mutators_panel, y)

	local right_panel = self._full_hud_panel:child("right_panel")

	if alive(right_panel) then
		local ext_inventory_panel = right_panel.child(right_panel, "ext_inventory_panel")

		if alive(ext_inventory_panel) then
			mutators_panel.set_bottom(mutators_panel, ext_inventory_panel.top(ext_inventory_panel) - 10)
		end
	end

	return 
end
HUDStatsScreen._animate_text_pulse = function (self, text, exp_gain_ring, exp_ring)
	local t = 0
	local c = text.color(text)
	local w, h = text.size(text)
	local cx, cy = text.center(text)
	local ecx, ecy = exp_gain_ring.center(exp_gain_ring)

	while true do
		local dt = coroutine.yield()
		t = t + dt
		local alpha = math.abs(math.sin(t*180*1))

		text.set_size(text, math.lerp(w*2, w, alpha), math.lerp(h*2, h, alpha))
		text.set_font_size(text, math.lerp(25, tweak_data.menu.pd2_small_font_size, alpha*alpha))
		text.set_center_y(text, cy)
		exp_gain_ring.set_size(exp_gain_ring, math.lerp(72, 64, alpha*alpha), math.lerp(72, 64, alpha*alpha))
		exp_gain_ring.set_center(exp_gain_ring, ecx, ecy)
		exp_ring.set_size(exp_ring, exp_gain_ring.size(exp_gain_ring))
		exp_ring.set_center(exp_ring, exp_gain_ring.center(exp_gain_ring))
	end

	return 
end
HUDStatsScreen._update_stats_screen_loot = function (self, loot_wrapper_panel)
	local mandatory_bags_data = managers.loot:get_mandatory_bags_data()
	local secured_amount = managers.loot:get_secured_mandatory_bags_amount()
	local x = nil
	local bag_texture, bag_rect = tweak_data.hud_icons:get_icon_data("bag_icon")
	local mission_amount = managers.loot:get_secured_mandatory_bags_amount()
	local mission_vis = 0 < mission_amount or 0 < secured_amount
	local mission_bags_panel = loot_wrapper_panel.child(loot_wrapper_panel, "mission_bags_panel")

	mission_bags_panel.clear(mission_bags_panel)

	if mandatory_bags_data and mandatory_bags_data.amount then
		if 18 < mandatory_bags_data.amount then
			local x = 0
			local bag = mission_bags_panel.bitmap(mission_bags_panel, {
				name = "bag1",
				alpha = 0.25,
				texture = bag_texture,
				texture_rect = bag_rect,
				x = x
			})
			local bag_text = mission_bags_panel.text(mission_bags_panel, {
				name = "bag_amount",
				text = " x" .. tostring(mandatory_bags_data.amount - mission_amount),
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_small_font
			})

			managers.hud:make_fine_text(bag_text)
			bag_text.set_left(bag_text, bag.right(bag))
			bag_text.set_center_y(bag_text, math.round(bag.center_y(bag)))

			local bag_gotten = mission_bags_panel.bitmap(mission_bags_panel, {
				name = "bag1",
				texture = bag_texture,
				texture_rect = bag_rect,
				x = x
			})
			local bag_text_gotten = mission_bags_panel.text(mission_bags_panel, {
				name = "bag_amount",
				text = " x" .. tostring(mission_amount),
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_small_font
			})

			managers.hud:make_fine_text(bag_text_gotten)
			bag_gotten.set_left(bag_gotten, bag_text.right(bag_text) + 10)
			bag_text_gotten.set_left(bag_text_gotten, bag_gotten.right(bag_gotten))
			bag_text_gotten.set_center_y(bag_text_gotten, math.round(bag_gotten.center_y(bag_gotten)))
		else
			local x = 0
			local y = 0

			for i = 1, mandatory_bags_data.amount, 1 do
				local alpha = (i <= secured_amount and 1) or 0.25

				mission_bags_panel.bitmap(mission_bags_panel, {
					name = "bag" .. i,
					texture = bag_texture,
					texture_rect = bag_rect,
					x = x,
					y = y,
					alpha = alpha
				})

				x = x + 32

				if 288 <= x then
					x = 0
					y = 22
				end
			end
		end
	end

	local bonus_amount = managers.loot:get_secured_bonus_bags_amount()
	local bonus_vis = 0 < bonus_amount or 0 < secured_amount or 0 < managers.loot:get_secured_bonus_bags_amount(true)
	local bonus_bags_title = loot_wrapper_panel.child(loot_wrapper_panel, "bonus_bags_title")

	bonus_bags_title.set_alpha(bonus_bags_title, (bonus_vis and 1) or 0.5)

	local bonus_bags_panel = loot_wrapper_panel.child(loot_wrapper_panel, "bonus_bags_panel")

	bonus_bags_panel.clear(bonus_bags_panel)

	if 10 <= bonus_amount then
		local x = 0
		local bag = bonus_bags_panel.bitmap(bonus_bags_panel, {
			name = "bag1",
			texture = bag_texture,
			texture_rect = bag_rect,
			x = x
		})
		local bag_text = bonus_bags_panel.text(bonus_bags_panel, {
			name = "bag_amount",
			text = " x" .. tostring(bonus_amount),
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font
		})

		managers.hud:make_fine_text(bag_text)
		bag_text.set_left(bag_text, bag.right(bag))
		bag_text.set_center_y(bag_text, math.round(bag.center_y(bag)))
	else
		for i = 1, bonus_amount, 1 do
			local x = (i - 1)*32

			bonus_bags_panel.bitmap(bonus_bags_panel, {
				name = "bag" .. i,
				texture = bag_texture,
				texture_rect = bag_rect,
				x = x
			})
		end
	end

	local mandatory_cash = managers.money:get_secured_mandatory_bags_money()
	local mission_bags_payout = loot_wrapper_panel.child(loot_wrapper_panel, "mission_bags_payout")

	mission_bags_payout.set_visible(mission_bags_payout, mission_vis)
	mission_bags_payout.set_text(mission_bags_payout, utf8.to_upper(managers.localization:text("hud_bonus_bags_payout", {
		MONEY = managers.experience:cash_string(mandatory_cash)
	})))

	local bonus_cash = managers.money:get_secured_bonus_bags_money()
	local bonus_bags_payout = loot_wrapper_panel.child(loot_wrapper_panel, "bonus_bags_payout")

	bonus_bags_payout.set_visible(bonus_bags_payout, bonus_vis)
	bonus_bags_payout.set_text(bonus_bags_payout, utf8.to_upper(managers.localization:text("hud_bonus_bags_payout", {
		MONEY = managers.experience:cash_string(bonus_cash)
	})))

	local instant_cash = managers.loot:get_real_total_small_loot_value()
	local instant_vis = 0 < instant_cash
	local instant_cash_title = loot_wrapper_panel.child(loot_wrapper_panel, "instant_cash_title")

	instant_cash_title.set_alpha(instant_cash_title, (instant_vis and 1) or 0.5)

	local instant_cash_text = loot_wrapper_panel.child(loot_wrapper_panel, "instant_cash_text")

	instant_cash_text.set_text(instant_cash_text, utf8.to_upper(managers.experience:cash_string(instant_cash)))
	instant_cash_text.set_alpha(instant_cash_text, (instant_vis and 1) or 0.5)

	return 
end
HUDStatsScreen._update_stats_screen_day = function (self, right_panel)
	local job_data = managers.job:current_job_data()
	local stage_data = managers.job:current_stage_data()
	local has_stage_data = (stage_data and true) or false
	local days_title = right_panel.child(right_panel, "days_title")

	days_title.set_visible(days_title, has_stage_data)

	local day_wrapper_panel = right_panel.child(right_panel, "day_wrapper_panel")

	day_wrapper_panel.set_visible(day_wrapper_panel, has_stage_data)

	if job_data and managers.job:current_job_id() == "safehouse" and Global.mission_manager.saved_job_values.playedSafeHouseBefore then
		right_panel.set_visible(right_panel, false)

		return 
	end

	if has_stage_data then
		local num_stages = (self._current_job_chain and #self._current_job_chain) or 0
		local job_chain = managers.job:current_job_chain_data()
		local day = managers.job:current_stage()
		local days = (job_chain and #job_chain) or 0

		days_title.set_text(days_title, utf8.to_upper(managers.localization:text("hud_days_title", {
			DAY = day,
			DAYS = days
		})))

		local payout = managers.money:get_potential_payout_from_current_stage()
		local day_payout = day_wrapper_panel.child(day_wrapper_panel, "day_payout")

		day_payout.set_text(day_payout, utf8.to_upper(managers.localization:text("hud_day_payout", {
			MONEY = managers.experience:cash_string(payout)
		})))

		local level_data = managers.job:current_level_data()

		if level_data then
			local day_title = day_wrapper_panel.child(day_wrapper_panel, "day_title")

			day_title.set_text(day_title, utf8.to_upper(managers.localization:text(level_data.name_id)))

			local briefing_id = stage_data.briefing_id or level_data.briefing_id
			local day_description = day_wrapper_panel.child(day_wrapper_panel, "day_description")

			day_description.set_text(day_description, managers.localization:text(briefing_id))

			local _, _, _, h = day_description.text_rect(day_description)

			day_description.set_h(day_description, h)
			managers.hud:make_fine_text(days_title)
			day_wrapper_panel.set_h(day_wrapper_panel, day_title.top(day_title) + day_description.bottom(day_description))

			if managers.mutators:are_mutators_active() then
				local mutators_panel = right_panel.child(right_panel, "mutators_panel")

				if mutators_panel then
					local diff = day_wrapper_panel.bottom(day_wrapper_panel) - mutators_panel.top(mutators_panel)

					if 0 < diff then
						day_description.set_h(day_description, day_description.h(day_description) - diff)
						day_wrapper_panel.set_h(day_wrapper_panel, day_wrapper_panel.h(day_wrapper_panel) - diff)
					end
				end

				if self._box_gui then
					self._box_gui:close()

					self._box_gui = nil
				end

				self._box_gui = BoxGuiObject:new(day_wrapper_panel, {
					sides = {
						0,
						0,
						0,
						2
					}
				})
			end

			local _, _, _, h = day_description.text_rect(day_description)
			local is_level_ghostable = managers.job:is_level_ghostable(managers.job:current_level_id())
			local is_whisper_mode = managers.groupai and managers.groupai:state():whisper_mode()
			local ghost_icon = right_panel.child(right_panel, "ghost_icon")
			local ghostable_text = day_wrapper_panel.child(day_wrapper_panel, "ghostable_text")

			ghost_icon.set_left(ghost_icon, days_title.right(days_title))
			ghost_icon.set_center_y(ghost_icon, days_title.center_y(days_title))
			ghostable_text.set_y(ghostable_text, day_description.bottom(day_description) + padding)
			ghostable_text.set_left(ghostable_text, day_description.left(day_description))
			ghost_icon.set_visible(ghost_icon, is_level_ghostable)
			ghost_icon.set_color(ghost_icon, (is_whisper_mode and Color.white) or tweak_data.screen_colors.important_1)
			ghostable_text.set_visible(ghostable_text, is_level_ghostable and is_whisper_mode)
		end
	end

	if managers.crime_spree:is_active() then
		local day_payout = day_wrapper_panel.child(day_wrapper_panel, "day_payout")
		local day_description = day_wrapper_panel.child(day_wrapper_panel, "day_description")
		local bains_plan = day_wrapper_panel.child(day_wrapper_panel, "bains_plan")
		local ghost_icon = right_panel.child(right_panel, "ghost_icon")
		local ghostable_text = day_wrapper_panel.child(day_wrapper_panel, "ghostable_text")

		days_title.set_visible(days_title, false)
		day_payout.set_visible(day_payout, false)
		ghost_icon.set_visible(ghost_icon, false)
		ghostable_text.set_visible(ghostable_text, false)
		bains_plan.set_top(bains_plan, day_payout.top(day_payout) + padding)
		day_description.set_top(day_description, day_payout.bottom(day_payout) + padding)
	end

	return 
end
HUDStatsScreen.loot_value_updated = function (self)
	local right_panel = self._full_hud_panel:child("right_panel")
	local left_panel = self._full_hud_panel:child("left_panel")

	self._update_stats_screen_loot(self, left_panel.child(left_panel, "loot_wrapper_panel"))

	return 
end
HUDStatsScreen._animate_show_stats_left_panel = function (self, left_panel, right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)
	local start_x = left_panel.x(left_panel)
	local start_a = start_x/-left_panel.w(left_panel) - 1
	local TOTAL_T = start_x/-left_panel.w(left_panel)*0.33
	local t = 0

	while t < TOTAL_T do
		local dt = coroutine.yield()*TimerManager:game():multiplier()/1
		t = t + dt
		local a = math.lerp(start_a, 1, t/TOTAL_T)

		left_panel.set_alpha(left_panel, a)
		left_panel.set_x(left_panel, math.lerp(start_x, 0, t/TOTAL_T))
		right_panel.set_alpha(right_panel, a)
		right_panel.set_x(right_panel, right_panel.parent(right_panel):w() - (left_panel.x(left_panel) + right_panel.w(right_panel)))
		bottom_panel.set_alpha(bottom_panel, a)
		bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h() - (left_panel.x(left_panel) + bottom_panel.h(bottom_panel)))

		local a_half = (a - 1)*0.5 + 0.5

		teammates_panel.set_alpha(teammates_panel, a_half)
		objectives_panel.set_alpha(objectives_panel, a - 1)
		chat_panel.set_alpha(chat_panel, a_half)
	end

	left_panel.set_x(left_panel, 0)
	left_panel.set_alpha(left_panel, 1)
	teammates_panel.set_alpha(teammates_panel, 0.5)
	objectives_panel.set_alpha(objectives_panel, 0)
	chat_panel.set_alpha(chat_panel, 0.5)
	right_panel.set_alpha(right_panel, 1)
	right_panel.set_x(right_panel, right_panel.parent(right_panel):w() - right_panel.w(right_panel))
	bottom_panel.set_alpha(bottom_panel, 1)
	bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h() - bottom_panel.h(bottom_panel))
	self._rec_round_object(self, left_panel)
	self._rec_round_object(self, right_panel)
	self._rec_round_object(self, bottom_panel)

	return 
end
HUDStatsScreen._animate_hide_stats_left_panel = function (self, left_panel, right_panel, bottom_panel, teammates_panel, objectives_panel, chat_panel)
	local start_x = left_panel.x(left_panel)
	local start_a = start_x/-left_panel.w(left_panel) - 1
	local TOTAL_T = (start_x/-left_panel.w(left_panel) - 1)*0.33
	local t = 0

	while t < TOTAL_T do
		local dt = coroutine.yield()*TimerManager:game():multiplier()/1
		t = t + dt
		local a = math.lerp(start_a, 0, t/TOTAL_T)

		left_panel.set_alpha(left_panel, a)
		left_panel.set_x(left_panel, math.lerp(start_x, -left_panel.w(left_panel), t/TOTAL_T))
		right_panel.set_alpha(right_panel, a)
		right_panel.set_x(right_panel, right_panel.parent(right_panel):w() - (left_panel.x(left_panel) + right_panel.w(right_panel)))
		bottom_panel.set_alpha(bottom_panel, a)
		bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h() - (left_panel.x(left_panel) + bottom_panel.h(bottom_panel)))

		local a_half = (a - 1)*0.5 + 0.5

		teammates_panel.set_alpha(teammates_panel, a_half)
		objectives_panel.set_alpha(objectives_panel, a - 1)
		chat_panel.set_alpha(chat_panel, a_half)
	end

	left_panel.set_x(left_panel, -left_panel.w(left_panel))
	left_panel.set_alpha(left_panel, 0)
	teammates_panel.set_alpha(teammates_panel, 1)
	objectives_panel.set_alpha(objectives_panel, 1)
	chat_panel.set_alpha(chat_panel, 1)
	right_panel.set_alpha(right_panel, 0)
	right_panel.set_x(right_panel, right_panel.parent(right_panel):w())
	bottom_panel.set_alpha(bottom_panel, 0)
	bottom_panel.set_y(bottom_panel, bottom_panel.parent(bottom_panel):h())

	return 
end

return 
