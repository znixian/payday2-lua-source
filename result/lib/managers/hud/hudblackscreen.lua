HUDBlackScreen = HUDBlackScreen or class()
HUDBlackScreen.init = function (self, hud)
	self._hud_panel = hud.panel

	if self._hud_panel:child("blackscreen_panel") then
		self._hud_panel:remove(self._hud_panel:child("blackscreen_panel"))
	end

	self._blackscreen_panel = self._hud_panel:panel({
		y = 0,
		name = "blackscreen_panel",
		halign = "grow",
		visible = true,
		layer = 200,
		valign = "grow"
	})
	local mid_text = self._blackscreen_panel:text({
		name = "mid_text",
		y = 0,
		vertical = "center",
		align = "center",
		text = "000",
		visible = true,
		layer = 1,
		color = Color.white,
		valign = {
			0.4,
			0
		},
		font_size = tweak_data.hud.default_font_size,
		font = tweak_data.hud.medium_font,
		w = self._blackscreen_panel:w()
	})
	local _, _, _, h = mid_text.text_rect(mid_text)

	mid_text.set_h(mid_text, h)
	mid_text.set_center_x(mid_text, self._blackscreen_panel:center_x())
	mid_text.set_center_y(mid_text, self._blackscreen_panel:h()/2.5)

	local is_server = Network:is_server()
	local continue_button = (managers.menu:is_pc_controller() and "[ENTER]") or nil

	if _G.IS_VR then
		continue_button = managers.localization:btn_macro("laser_primary", true)
	end

	local text = utf8.to_upper(managers.localization:text("hud_skip_blackscreen", {
		BTN_ACCEPT = continue_button
	}))
	local skip_text = self._blackscreen_panel:text({
		y = 0,
		vertical = "bottom",
		name = "skip_text",
		align = "right",
		layer = 1,
		visible = is_server,
		text = text,
		color = Color.white,
		font = tweak_data.hud.medium_font_noshadow
	})
	local loading_text = utf8.to_upper(managers.localization:text("menu_loading_progress", {
		prog = 0
	}))
	local loading_text_object = self._blackscreen_panel:text({
		y = 0,
		vertical = "bottom",
		name = "loading_text",
		align = "right",
		visible = false,
		layer = 1,
		text = loading_text,
		color = Color.white,
		font = tweak_data.hud.medium_font_noshadow
	})
	self._circle_radius = 16
	self._sides = 64

	skip_text.set_x(skip_text, skip_text.x(skip_text) - self._circle_radius*3)
	skip_text.set_y(skip_text, skip_text.y(skip_text) - self._circle_radius)
	loading_text_object.set_x(loading_text_object, loading_text_object.x(loading_text_object) - self._circle_radius*3)
	loading_text_object.set_y(loading_text_object, loading_text_object.y(loading_text_object) - self._circle_radius)

	self._skip_circle = CircleBitmapGuiObject:new(self._blackscreen_panel, {
		blend_mode = "normal",
		image = "guis/textures/pd2/hud_progress_32px",
		layer = 2,
		radius = self._circle_radius,
		sides = self._sides,
		current = self._sides,
		total = self._sides,
		color = Color.white
	})

	self._skip_circle:set_position(self._blackscreen_panel:w() - self._circle_radius*3, self._blackscreen_panel:h() - self._circle_radius*3)

	return 
end
HUDBlackScreen.set_skip_circle = function (self, current, total)
	self._skip_circle:set_current(current/total)

	return 
end
HUDBlackScreen.set_loading_text_status = function (self, status)
	if status then
		self._blackscreen_panel:child("skip_text"):set_visible(false)
		self._blackscreen_panel:child("loading_text"):set_visible(true)

		if status == "allow_skip" then
			self._blackscreen_panel:child("loading_text"):set_visible(false)

			if Network:is_server() then
				self._blackscreen_panel:child("skip_text"):set_visible(true)
			end
		elseif status == "wait_for_peers" then
			local peer_name, peer_status = managers.network:session():peer_streaming_status()

			print("[HUDBlackScreen:set_loading_text_status] wait_for_peers", peer_name, peer_status)

			local loading_text = utf8.to_upper(managers.localization:text("menu_waiting_for_players_progress", {
				player_name = peer_name,
				prog = peer_status
			}))

			self._blackscreen_panel:child("loading_text"):set_text(loading_text)
		else
			local loading_text = utf8.to_upper(managers.localization:text("menu_loading_progress", {
				prog = status
			}))

			self._blackscreen_panel:child("loading_text"):set_text(loading_text)
		end
	else
		self._blackscreen_panel:child("loading_text"):set_visible(false)

		if Network:is_server() then
			self._blackscreen_panel:child("skip_text"):set_visible(true)
		end
	end

	return 
end
HUDBlackScreen.skip_circle_done = function (self)
	self._blackscreen_panel:child("skip_text"):set_visible(false)

	local bitmap = self._blackscreen_panel:bitmap({
		blend_mode = "add",
		texture = "guis/textures/pd2/hud_progress_32px",
		layer = 2,
		align = "center",
		valign = "center",
		w = self._circle_radius*2,
		h = self._circle_radius*2
	})

	bitmap.set_position(bitmap, self._skip_circle:position())

	local circle = CircleBitmapGuiObject:new(self._blackscreen_panel, {
		sides = 64,
		total = 64,
		current = 64,
		image = "guis/textures/pd2/hud_progress_32px",
		blend_mode = "normal",
		layer = 3,
		radius = self._circle_radius,
		color = Color.white:with_alpha(1)
	})

	circle.set_position(circle, self._skip_circle:position())
	bitmap.animate(bitmap, callback(self, HUDInteraction, "_animate_interaction_complete"), circle)

	return 
end
HUDBlackScreen.set_job_data = function (self)
	if managers.crime_spree:is_active() then
		self._set_job_data_crime_spree(self)
	else
		self._set_job_data(self)
	end

	return 
end
HUDBlackScreen._set_job_data = function (self)
	if not managers.job:has_active_job() then
		return 
	end

	local job_panel = self._blackscreen_panel:panel({
		y = 0,
		name = "job_panel",
		halign = "grow",
		visible = true,
		layer = 1,
		valign = "grow"
	})
	local risk_panel = job_panel.panel(job_panel, {})
	local last_risk_level = nil
	local blackscreen_risk_textures = tweak_data.gui.blackscreen_risk_textures

	for i = 1, managers.job:current_difficulty_stars(), 1 do
		local difficulty_name = tweak_data.difficulties[i + 2]
		local texture = blackscreen_risk_textures[difficulty_name] or "guis/textures/pd2/risklevel_blackscreen"
		last_risk_level = risk_panel.bitmap(risk_panel, {
			texture = texture,
			color = tweak_data.screen_colors.risk
		})

		last_risk_level.move(last_risk_level, (i - 1)*last_risk_level.w(last_risk_level), 0)
	end

	if last_risk_level then
		risk_panel.set_size(risk_panel, last_risk_level.right(last_risk_level), last_risk_level.bottom(last_risk_level))
		risk_panel.set_center(risk_panel, job_panel.w(job_panel)/2, job_panel.h(job_panel)/2)
		risk_panel.set_position(risk_panel, math.round(risk_panel.x(risk_panel)), math.round(risk_panel.y(risk_panel)))
	else
		risk_panel.set_size(risk_panel, 64, 64)
		risk_panel.set_center_x(risk_panel, job_panel.w(job_panel)/2)
		risk_panel.set_bottom(risk_panel, job_panel.h(job_panel)/2)
		risk_panel.set_position(risk_panel, math.round(risk_panel.x(risk_panel)), math.round(risk_panel.y(risk_panel)))
	end

	local risk_text = job_panel.text(job_panel, {
		vertical = "bottom",
		align = "center",
		text = managers.localization:to_upper_text(tweak_data.difficulty_name_id),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_small_large_size,
		color = tweak_data.screen_colors.risk
	})

	risk_text.set_bottom(risk_text, risk_panel.top(risk_panel))
	risk_text.set_center_x(risk_text, risk_panel.center_x(risk_panel))

	return 
end
HUDBlackScreen._set_job_data_crime_spree = function (self)
	local job_panel = self._blackscreen_panel:panel({
		y = 0,
		name = "job_panel",
		halign = "grow",
		visible = true,
		layer = 1,
		valign = "grow"
	})
	local job_text = job_panel.text(job_panel, {
		vertical = "bottom",
		align = "center",
		text = managers.localization:to_upper_text("cn_crime_spree"),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		color = tweak_data.screen_colors.crime_spree_risk
	})

	job_text.set_bottom(job_text, job_panel.h(job_panel)*0.5)
	job_text.set_center_x(job_text, job_panel.center_x(job_panel))

	local risk_text = job_panel.text(job_panel, {
		vertical = "top",
		align = "center",
		text = managers.localization:to_upper_text("menu_cs_level", {
			level = managers.experience:cash_string(managers.crime_spree:server_spree_level(), "")
		}),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		color = tweak_data.screen_colors.crime_spree_risk
	})

	risk_text.set_top(risk_text, job_panel.h(job_panel)*0.5)
	risk_text.set_center_x(risk_text, job_panel.center_x(job_panel))

	return 
end
HUDBlackScreen._create_stages = function (self)
	local job_chain = managers.job:current_job_chain_data()
	local job_panel = self._blackscreen_panel:child("job_panel")
	local stages_panel = job_panel.panel(job_panel, {
		name = "stages_panel",
		h = 256,
		visible = true,
		x = 320,
		y = job_panel.child(job_panel, "contact_name"):bottom()
	})
	local types = {
		a = {
			256,
			0,
			64,
			64
		},
		b = {
			192,
			0,
			64,
			64
		},
		c = {
			128,
			0,
			64,
			64
		},
		d = {
			64,
			0,
			64,
			64
		},
		e = {
			0,
			0,
			64,
			64
		}
	}
	local level_rects = {
		{
			0,
			0,
			256,
			256
		},
		{
			768,
			0,
			256,
			256
		},
		{
			512,
			0,
			256,
			256
		},
		{
			256,
			0,
			256,
			256
		}
	}
	local x = 0

	for i, heist in ipairs(job_chain) do
		local is_current_stage = managers.job:current_stage() == i
		local is_completed = i < managers.job:current_stage()
		local panel = stages_panel.panel(stages_panel, {
			y = 0,
			name = "panel",
			visible = true,
			x = x,
			w = (is_current_stage and 256) or 80
		})

		if not is_completed and not is_current_stage then
			local image = panel.bitmap(panel, {
				texture = "guis/textures/pd2/icon_mission_overview_unknown",
				blend_mode = "normal",
				layer = 1
			})

			image.set_center(image, panel.w(panel)/2, panel.h(panel)/2)
		else
			local image = panel.bitmap(panel, {
				texture = "guis/textures/pd2/icon_mission_overview",
				blend_mode = "normal",
				layer = 1,
				texture_rect = level_rects[i]
			})

			image.set_center(image, panel.w(panel)/2, panel.h(panel)/2)
		end

		local badge = panel.bitmap(panel, {
			texture = "guis/textures/pd2/gui_grade_badges",
			layer = 4,
			texture_rect = types[heist.type]
		})

		badge.set_right(badge, panel.w(panel) - 8)
		badge.set_bottom(badge, panel.h(panel) - 8)

		if (not is_completed or not {
			0,
			Color(120, 255, 120)/255:with_alpha(0.25),
			1,
			Color(120, 255, 120)/255:with_alpha(0)
		}) and (not is_current_stage or not {
			0,
			Color(230, 200, 150)/255:with_alpha(0.5),
			1,
			Color(230, 200, 150)/255:with_alpha(0)
		}) then
			local gradient_points = {
				0,
				Color.black:with_alpha(0),
				1,
				Color.black:with_alpha(0)
			}
		end

		panel.gradient(panel, {
			orientation = "vertical",
			layer = 3,
			gradient_points = gradient_points,
			h = panel.h(panel)/2
		})

		x = x + panel.w(panel) + 10
		local level_data = tweak_data.levels[heist.level_id]

		if is_current_stage then
			local pad = 8

			panel.text(panel, {
				vertical = "top",
				h = 24,
				name = "stage_name",
				align = "left",
				layer = 4,
				text = utf8.to_upper(managers.localization:text(level_data.name_id)),
				font_size = tweak_data.hud.small_font_size,
				font = tweak_data.hud.small_font,
				w = panel.w(panel),
				x = pad,
				y = pad
			})
			panel.text(panel, {
				vertical = "top",
				h = 24,
				name = "type",
				align = "left",
				layer = 4,
				text = utf8.to_upper(managers.localization:text(heist.type_id)),
				font_size = tweak_data.hud.small_font_size,
				font = tweak_data.hud.small_font,
				w = panel.w(panel),
				x = pad,
				y = pad + 24
			})
		end

		stages_panel.set_w(stages_panel, panel.right(panel))
	end

	stages_panel.set_center_x(stages_panel, math.round(job_panel.child(job_panel, "portrait"):w() + (job_panel.w(job_panel) - job_panel.child(job_panel, "portrait"):w())/2))

	return 
end
HUDBlackScreen.set_mid_text = function (self, text)
	local mid_text = self._blackscreen_panel:child("mid_text")

	mid_text.set_alpha(mid_text, 0)
	mid_text.set_text(mid_text, utf8.to_upper(text))

	return 
end
HUDBlackScreen.fade_in_mid_text = function (self)
	self._blackscreen_panel:child("mid_text"):animate(callback(self, self, "_animate_fade_in"))

	return 
end
HUDBlackScreen.fade_out_mid_text = function (self)
	self._blackscreen_panel:child("mid_text"):animate(callback(self, self, "_animate_fade_out"))

	return 
end
HUDBlackScreen._animate_fade_in = function (self, mid_text)
	local job_panel = self._blackscreen_panel:child("job_panel")
	local t = 1
	local d = t

	while 0 < t do
		local dt = coroutine.yield()
		t = t - dt
		local a = (d - t)/d

		mid_text.set_alpha(mid_text, a)

		if job_panel then
			job_panel.set_alpha(job_panel, a)
		end

		self._blackscreen_panel:set_alpha(a)
	end

	mid_text.set_alpha(mid_text, 1)

	if job_panel then
		job_panel.set_alpha(job_panel, 1)
	end

	self._blackscreen_panel:set_alpha(1)

	return 
end
HUDBlackScreen._animate_fade_out = function (self, mid_text)
	local job_panel = self._blackscreen_panel:child("job_panel")
	local t = 1
	local d = t

	while 0 < t do
		local dt = coroutine.yield()
		t = t - dt
		local a = t/d

		mid_text.set_alpha(mid_text, a)

		if job_panel then
			job_panel.set_alpha(job_panel, a)
		end

		self._blackscreen_panel:set_alpha(a)
	end

	mid_text.set_alpha(mid_text, 0)

	if job_panel then
		job_panel.set_alpha(job_panel, 0)
	end

	self._blackscreen_panel:set_alpha(0)

	return 
end

return 
