HUDChat = HUDChat or class()
HUDChat.line_height = 21
HUDChat.init = function (self, ws, hud)
	self._ws = ws
	self._hud_panel = hud.panel

	self.set_channel_id(self, ChatManager.GAME)

	self._output_width = 300
	self._panel_width = 500
	self._lines = {}
	self._esc_callback = callback(self, self, "esc_key_callback")
	self._enter_callback = callback(self, self, "enter_key_callback")
	self._typing_callback = 0
	self._skip_first = false
	self._panel = self._hud_panel:panel({
		name = "chat_panel",
		h = 500,
		halign = "left",
		x = 0,
		valign = "bottom",
		w = self._panel_width
	})

	self._panel:set_bottom(self._panel:parent():h() - 112)

	local output_panel = self._panel:panel({
		name = "output_panel",
		h = 10,
		x = 0,
		layer = 1,
		w = self._output_width
	})

	output_panel.gradient(output_panel, {
		blend_mode = "sub",
		name = "output_bg",
		valign = "grow",
		layer = -1,
		gradient_points = {
			0,
			Color.white:with_alpha(0),
			0.2,
			Color.white:with_alpha(0.25),
			1,
			Color.white:with_alpha(0)
		}
	})
	self._create_input_panel(self)
	self._layout_input_panel(self)
	self._layout_output_panel(self)

	return 
end
HUDChat.set_layer = function (self, layer)
	self._panel:set_layer(layer)

	return 
end
HUDChat.set_channel_id = function (self, channel_id)
	managers.chat:unregister_receiver(self._channel_id, self)

	self._channel_id = channel_id

	managers.chat:register_receiver(self._channel_id, self)

	return 
end
HUDChat.esc_key_callback = function (self)
	managers.hud:set_chat_focus(false)

	return 
end
HUDChat.enter_key_callback = function (self)
	local text = self._input_panel:child("input_text")
	local message = text.text(text)

	if 0 < string.len(message) then
		local u_name = managers.network.account:username()

		managers.chat:send_message(self._channel_id, u_name or "Offline", message)
	end

	text.set_text(text, "")
	text.set_selection(text, 0, 0)
	managers.hud:set_chat_focus(false)

	return 
end
HUDChat._create_input_panel = function (self)
	self._input_panel = self._panel:panel({
		name = "input_panel",
		h = 24,
		alpha = 0,
		x = 0,
		layer = 1,
		w = self._panel_width
	})

	self._input_panel:rect({
		name = "focus_indicator",
		layer = 0,
		visible = false,
		color = Color.white:with_alpha(0.2)
	})

	local say = self._input_panel:text({
		y = 0,
		name = "say",
		vertical = "center",
		hvertical = "center",
		align = "left",
		blend_mode = "normal",
		halign = "left",
		x = 0,
		layer = 1,
		text = utf8.to_upper(managers.localization:text("debug_chat_say")),
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		color = Color.white
	})
	local _, _, w, h = say.text_rect(say)

	say.set_size(say, w, self._input_panel:h())

	local input_text = self._input_panel:text({
		y = 0,
		name = "input_text",
		vertical = "center",
		wrap = true,
		align = "left",
		blend_mode = "normal",
		hvertical = "center",
		text = "",
		word_wrap = false,
		halign = "left",
		x = 0,
		layer = 1,
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		color = Color.white
	})
	local caret = self._input_panel:rect({
		name = "caret",
		h = 0,
		y = 0,
		w = 0,
		x = 0,
		layer = 2,
		color = Color(0.05, 1, 1, 1)
	})

	self._input_panel:gradient({
		blend_mode = "sub",
		name = "input_bg",
		valign = "grow",
		layer = -1,
		gradient_points = {
			0,
			Color.white:with_alpha(0),
			0.2,
			Color.white:with_alpha(0.25),
			1,
			Color.white:with_alpha(0)
		},
		h = self._input_panel:h()
	})

	return 
end
HUDChat._layout_output_panel = function (self)
	local output_panel = self._panel:child("output_panel")

	output_panel.set_w(output_panel, self._output_width)

	local line_height = HUDChat.line_height
	local lines = 0

	for i = #self._lines, 1, -1 do
		local line = self._lines[i][1]
		local icon = self._lines[i][2]

		line.set_w(line, output_panel.w(output_panel) - line.left(line))

		local _, _, w, h = line.text_rect(line)

		line.set_h(line, h)

		lines = lines + line.number_of_lines(line)
	end

	output_panel.set_h(output_panel, line_height*math.min(10, lines))

	local y = 0

	for i = #self._lines, 1, -1 do
		local line = self._lines[i][1]
		local icon = self._lines[i][2]
		local _, _, w, h = line.text_rect(line)

		line.set_bottom(line, output_panel.h(output_panel) - y)

		if icon then
			icon.set_top(icon, line.top(line) + 1)
		end

		y = y + h
	end

	output_panel.set_bottom(output_panel, self._input_panel:top())

	return 
end
HUDChat._layout_input_panel = function (self)
	self._input_panel:set_w(self._panel_width)

	local say = self._input_panel:child("say")
	local input_text = self._input_panel:child("input_text")

	input_text.set_left(input_text, say.right(say) + 4)
	input_text.set_w(input_text, self._input_panel:w() - input_text.left(input_text))

	local focus_indicator = self._input_panel:child("focus_indicator")

	focus_indicator.set_shape(focus_indicator, input_text.shape(input_text))
	self._input_panel:set_y(self._input_panel:parent():h() - self._input_panel:h())

	return 
end
HUDChat.input_focus = function (self)
	return self._focus
end
HUDChat.set_skip_first = function (self, skip_first)
	self._skip_first = skip_first

	return 
end
HUDChat._on_focus = function (self)
	if self._focus then
		return 
	end

	local output_panel = self._panel:child("output_panel")

	output_panel.stop(output_panel)
	output_panel.animate(output_panel, callback(self, self, "_animate_show_component"), output_panel.alpha(output_panel))
	self._input_panel:stop()
	self._input_panel:animate(callback(self, self, "_animate_show_component"))

	self._focus = true

	self._input_panel:child("focus_indicator"):set_color(Color(0.8, 1, 0.8):with_alpha(0.2))
	self._ws:connect_keyboard(Input:keyboard())
	self._input_panel:key_press(callback(self, self, "key_press"))
	self._input_panel:key_release(callback(self, self, "key_release"))

	self._enter_text_set = false

	self._input_panel:child("input_bg"):animate(callback(self, self, "_animate_input_bg"))
	self.set_layer(self, 1100)
	self.update_caret(self)

	return 
end
HUDChat._loose_focus = function (self)
	if not self._focus then
		return 
	end

	self._focus = false

	self._input_panel:child("focus_indicator"):set_color(Color.white:with_alpha(0.2))
	self._ws:disconnect_keyboard()
	self._input_panel:key_press(nil)
	self._input_panel:enter_text(nil)
	self._input_panel:key_release(nil)
	self._panel:child("output_panel"):stop()
	self._panel:child("output_panel"):animate(callback(self, self, "_animate_fade_output"))
	self._input_panel:stop()
	self._input_panel:animate(callback(self, self, "_animate_hide_input"))

	local text = self._input_panel:child("input_text")

	text.stop(text)
	self._input_panel:child("input_bg"):stop()
	self.set_layer(self, 1)
	self.update_caret(self)

	return 
end
HUDChat.clear = function (self)
	local text = self._input_panel:child("input_text")

	text.set_text(text, "")
	text.set_selection(text, 0, 0)
	self._loose_focus(self)
	managers.hud:set_chat_focus(false)

	return 
end
HUDChat._shift = function (self)
	local k = Input:keyboard()

	return k.down(k, "left shift") or k.down(k, "right shift") or (k.has_button(k, "shift") and k.down(k, "shift"))
end
HUDChat.blink = function (o)
	while true do
		o.set_color(o, Color(0, 1, 1, 1))
		wait(0.3)
		o.set_color(o, Color.white)
		wait(0.3)
	end

	return 
end
HUDChat.set_blinking = function (self, b)
	local caret = self._input_panel:child("caret")

	if b == self._blinking then
		return 
	end

	if b then
		caret.animate(caret, self.blink)
	else
		caret.stop(caret)
	end

	self._blinking = b

	if not self._blinking then
		caret.set_color(caret, Color.white)
	end

	return 
end
HUDChat.update_caret = function (self)
	local text = self._input_panel:child("input_text")
	local caret = self._input_panel:child("caret")
	local s, e = text.selection(text)
	local x, y, w, h = text.selection_rect(text)

	if s == 0 and e == 0 then
		if text.align(text) == "center" then
			x = text.world_x(text) + text.w(text)/2
		else
			x = text.world_x(text)
		end

		y = text.world_y(text)
	end

	h = text.h(text)

	if w < 3 then
		w = 3
	end

	if not self._focus then
		w = 0
		h = 0
	end

	caret.set_world_shape(caret, x, y + 2, w, h - 4)
	self.set_blinking(self, s == e and self._focus)

	local mid = x/self._input_panel:child("input_bg"):w()

	self._input_panel:child("input_bg"):set_gradient_points({
		0,
		Color.white:with_alpha(0),
		mid,
		Color.white:with_alpha(0.25),
		1,
		Color.white:with_alpha(0)
	})

	return 
end
HUDChat.enter_text = function (self, o, s)
	if managers.hud and managers.hud:showing_stats_screen() then
		return 
	end

	if self._skip_first then
		self._skip_first = false

		return 
	end

	local text = self._input_panel:child("input_text")

	if type(self._typing_callback) ~= "number" then
		self._typing_callback()
	end

	text.replace_text(text, s)

	local lbs = text.line_breaks(text)

	if 1 < #lbs then
		local s = lbs[2]
		local e = utf8.len(text.text(text))

		text.set_selection(text, s, e)
		text.replace_text(text, "")
	end

	self.update_caret(self)

	return 
end
HUDChat.update_key_down = function (self, o, k)
	wait(0.6)

	local text = self._input_panel:child("input_text")

	while self._key_pressed == k do
		local s, e = text.selection(text)
		local n = utf8.len(text.text(text))
		local d = math.abs(e - s)

		if self._key_pressed == Idstring("backspace") then
			if s == e and 0 < s then
				text.set_selection(text, s - 1, e)
			end

			text.replace_text(text, "")

			if utf8.len(text.text(text)) < 1 and type(self._esc_callback) ~= "number" then
			end
		elseif self._key_pressed == Idstring("delete") then
			if s == e and s < n then
				text.set_selection(text, s, e + 1)
			end

			text.replace_text(text, "")

			if utf8.len(text.text(text)) < 1 and type(self._esc_callback) ~= "number" then
			end
		elseif self._key_pressed == Idstring("insert") then
			local clipboard = Application:get_clipboard() or ""

			text.replace_text(text, clipboard)

			local lbs = text.line_breaks(text)

			if 1 < #lbs then
				local s = lbs[2]
				local e = utf8.len(text.text(text))

				text.set_selection(text, s, e)
				text.replace_text(text, "")
			end
		elseif self._key_pressed == Idstring("left") then
			if s < e then
				text.set_selection(text, s, s)
			elseif 0 < s then
				text.set_selection(text, s - 1, s - 1)
			end
		elseif self._key_pressed == Idstring("right") then
			if s < e then
				text.set_selection(text, e, e)
			elseif s < n then
				text.set_selection(text, s + 1, s + 1)
			end
		else
			self._key_pressed = false
		end

		self.update_caret(self)
		wait(0.03)
	end

	return 
end
HUDChat.key_release = function (self, o, k)
	if self._key_pressed == k then
		self._key_pressed = false
	end

	return 
end
HUDChat.key_press = function (self, o, k)
	if self._skip_first then
		self._skip_first = false

		return 
	end

	if not self._enter_text_set then
		self._input_panel:enter_text(callback(self, self, "enter_text"))

		self._enter_text_set = true
	end

	local text = self._input_panel:child("input_text")
	local s, e = text.selection(text)
	local n = utf8.len(text.text(text))
	local d = math.abs(e - s)
	self._key_pressed = k

	text.stop(text)
	text.animate(text, callback(self, self, "update_key_down"), k)

	if k == Idstring("backspace") then
		if s == e and 0 < s then
			text.set_selection(text, s - 1, e)
		end

		text.replace_text(text, "")

		if utf8.len(text.text(text)) < 1 and type(self._esc_callback) ~= "number" then
		end
	elseif k == Idstring("delete") then
		if s == e and s < n then
			text.set_selection(text, s, e + 1)
		end

		text.replace_text(text, "")

		if utf8.len(text.text(text)) < 1 and type(self._esc_callback) ~= "number" then
		end
	elseif k == Idstring("insert") then
		local clipboard = Application:get_clipboard() or ""

		text.replace_text(text, clipboard)

		local lbs = text.line_breaks(text)

		if 1 < #lbs then
			local s = lbs[2]
			local e = utf8.len(text.text(text))

			text.set_selection(text, s, e)
			text.replace_text(text, "")
		end
	elseif k == Idstring("left") then
		if s < e then
			text.set_selection(text, s, s)
		elseif 0 < s then
			text.set_selection(text, s - 1, s - 1)
		end
	elseif k == Idstring("right") then
		if s < e then
			text.set_selection(text, e, e)
		elseif s < n then
			text.set_selection(text, s + 1, s + 1)
		end
	elseif self._key_pressed == Idstring("end") then
		text.set_selection(text, n, n)
	elseif self._key_pressed == Idstring("home") then
		text.set_selection(text, 0, 0)
	elseif k == Idstring("enter") then
		if type(self._enter_callback) ~= "number" then
			self._enter_callback()
		end
	elseif k == Idstring("esc") and type(self._esc_callback) ~= "number" then
		text.set_text(text, "")
		text.set_selection(text, 0, 0)
		self._esc_callback()
	end

	self.update_caret(self)

	return 
end
HUDChat.send_message = function (self, name, message)
	return 
end
HUDChat.receive_message = function (self, name, message, color, icon)
	local output_panel = self._panel:child("output_panel")
	local len = utf8.len(name) + 1
	local x = 0
	local icon_bitmap = nil

	if icon then
		local icon_texture, icon_texture_rect = tweak_data.hud_icons:get_icon_data(icon)
		icon_bitmap = output_panel.bitmap(output_panel, {
			y = 1,
			texture = icon_texture,
			texture_rect = icon_texture_rect,
			color = color
		})
		x = icon_bitmap.right(icon_bitmap)
	end

	local line = output_panel.text(output_panel, {
		halign = "left",
		vertical = "top",
		hvertical = "top",
		wrap = true,
		align = "left",
		blend_mode = "normal",
		word_wrap = true,
		y = 0,
		layer = 0,
		text = name .. ": " .. message,
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		x = x,
		color = color
	})
	local total_len = utf8.len(line.text(line))

	line.set_range_color(line, 0, len, color)
	line.set_range_color(line, len, total_len, Color.white)

	local _, _, w, h = line.text_rect(line)

	line.set_h(line, h)
	table.insert(self._lines, {
		line,
		icon_bitmap
	})
	line.set_kern(line, line.kern(line))
	self._layout_output_panel(self)

	if not self._focus then
		local output_panel = self._panel:child("output_panel")

		output_panel.stop(output_panel)
		output_panel.animate(output_panel, callback(self, self, "_animate_show_component"), output_panel.alpha(output_panel))
		output_panel.animate(output_panel, callback(self, self, "_animate_fade_output"))
	end

	return 
end
HUDChat._animate_fade_output = function (self)
	local wait_t = 10
	local fade_t = 1
	local t = 0

	while t < wait_t do
		local dt = coroutine.yield()
		t = t + dt
	end

	local t = 0

	while t < fade_t do
		local dt = coroutine.yield()
		t = t + dt

		self.set_output_alpha(self, t/fade_t - 1)
	end

	self.set_output_alpha(self, 0)

	return 
end
HUDChat._animate_show_component = function (self, input_panel, start_alpha)
	local TOTAL_T = 0.25
	local t = 0
	start_alpha = start_alpha or 0

	while t < TOTAL_T do
		local dt = coroutine.yield()
		t = t + dt

		input_panel.set_alpha(input_panel, start_alpha + t/TOTAL_T*(start_alpha - 1))
	end

	input_panel.set_alpha(input_panel, 1)

	return 
end
HUDChat._animate_hide_input = function (self, input_panel)
	local TOTAL_T = 0.25
	local t = 0

	while t < TOTAL_T do
		local dt = coroutine.yield()
		t = t + dt

		input_panel.set_alpha(input_panel, t/TOTAL_T - 1)
	end

	input_panel.set_alpha(input_panel, 0)

	return 
end
HUDChat._animate_input_bg = function (self, input_bg)
	local t = 0

	while true do
		local dt = coroutine.yield()
		t = t + dt
		local a = (math.sin(t*200) + 1)/8 + 0.75

		input_bg.set_alpha(input_bg, a)
	end

	return 
end
HUDChat.set_output_alpha = function (self, alpha)
	self._panel:child("output_panel"):set_alpha(alpha)

	return 
end
HUDChat.remove = function (self)
	self._panel:child("output_panel"):stop()
	self._input_panel:stop()
	self._hud_panel:remove(self._panel)
	managers.chat:unregister_receiver(self._channel_id, self)

	return 
end

if _G.IS_VR then
	require("lib/managers/hud/vr/HUDChatVR")
end

return 
