HUDHitConfirm = HUDHitConfirm or class()
HUDHitConfirm.init = function (self, hud)
	self._hud_panel = hud.panel

	if self._hud_panel:child("hit_confirm") then
		self._hud_panel:remove(self._hud_panel:child("hit_confirm"))
	end

	if self._hud_panel:child("headshot_confirm") then
		self._hud_panel:remove(self._hud_panel:child("headshot_confirm"))
	end

	if self._hud_panel:child("crit_confirm") then
		self._hud_panel:remove(self._hud_panel:child("crit_confirm"))
	end

	self._hit_confirm = self._hud_panel:bitmap({
		texture = "guis/textures/pd2/hitconfirm",
		name = "hit_confirm",
		halign = "center",
		visible = false,
		layer = 0,
		blend_mode = "add",
		valign = "center",
		color = Color.white
	})

	self._hit_confirm:set_center(self._hud_panel:w()/2, self._hud_panel:h()/2)

	self._crit_confirm = self._hud_panel:bitmap({
		texture = "guis/textures/pd2/hitconfirm_crit",
		name = "crit_confirm",
		halign = "center",
		visible = false,
		layer = 0,
		blend_mode = "add",
		valign = "center",
		color = Color.white
	})

	self._crit_confirm:set_center(self._hud_panel:w()/2, self._hud_panel:h()/2)

	return 
end
HUDHitConfirm.on_hit_confirmed = function (self)
	self._hit_confirm:stop()
	self._hit_confirm:animate(callback(self, self, "_animate_show"), callback(self, self, "show_done"), 0.25)

	return 
end
HUDHitConfirm.on_headshot_confirmed = function (self)
	self._hit_confirm:stop()
	self._hit_confirm:animate(callback(self, self, "_animate_show"), callback(self, self, "show_done"), 0.25)

	return 
end
HUDHitConfirm.on_crit_confirmed = function (self)
	self._crit_confirm:stop()
	self._crit_confirm:animate(callback(self, self, "_animate_show"), callback(self, self, "show_done"), 0.25)

	return 
end
HUDHitConfirm._animate_show = function (self, hint_confirm, done_cb, seconds)
	hint_confirm.set_visible(hint_confirm, true)
	hint_confirm.set_alpha(hint_confirm, 1)

	local t = seconds

	while 0 < t do
		local dt = coroutine.yield()
		t = t - dt

		hint_confirm.set_alpha(hint_confirm, t/seconds)
	end

	hint_confirm.set_visible(hint_confirm, false)
	done_cb()

	return 
end
HUDHitConfirm.show_done = function (self)
	return 
end

if _G.IS_VR then
	require("lib/managers/hud/vr/HUDHitConfirmVR")
end

return 
