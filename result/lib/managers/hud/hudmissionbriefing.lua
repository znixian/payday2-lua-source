require("lib/managers/menu/MenuBackdropGUI")

HUDMissionBriefing = HUDMissionBriefing or class()
HUDMissionBriefing.init = function (self, hud, workspace)
	self._backdrop = MenuBackdropGUI:new(workspace)

	if not _G.IS_VR then
		self._backdrop:create_black_borders()
	end

	self._hud = hud
	self._workspace = workspace
	self._singleplayer = Global.game_settings.single_player
	local bg_font = tweak_data.menu.pd2_massive_font
	local title_font = tweak_data.menu.pd2_large_font
	local content_font = tweak_data.menu.pd2_medium_font
	local text_font = tweak_data.menu.pd2_small_font
	local bg_font_size = tweak_data.menu.pd2_massive_font_size
	local title_font_size = tweak_data.menu.pd2_large_font_size
	local content_font_size = tweak_data.menu.pd2_medium_font_size
	local text_font_size = tweak_data.menu.pd2_small_font_size
	local interupt_stage = managers.job:interupt_stage()
	self._background_layer_one = self._backdrop:get_new_background_layer()
	self._background_layer_two = self._backdrop:get_new_background_layer()
	self._background_layer_three = self._backdrop:get_new_background_layer()
	self._foreground_layer_one = self._backdrop:get_new_foreground_layer()

	self._backdrop:set_panel_to_saferect(self._background_layer_one)
	self._backdrop:set_panel_to_saferect(self._foreground_layer_one)

	self._ready_slot_panel = self._foreground_layer_one:panel({
		name = "player_slot_panel",
		w = self._foreground_layer_one:w()/2,
		h = text_font_size*4 + 20
	})

	self._ready_slot_panel:set_bottom(self._foreground_layer_one:h() - 70)
	self._ready_slot_panel:set_right(self._foreground_layer_one:w())

	if not self._singleplayer then
		local voice_icon, voice_texture_rect = tweak_data.hud_icons:get_icon_data("mugshot_talk")
		local infamy_icon, infamy_rect = tweak_data.hud_icons:get_icon_data("infamy_icon")

		for i = 1, tweak_data.max_players, 1 do
			local color_id = i
			local color = tweak_data.chat_colors[color_id] or tweak_data.chat_colors[#tweak_data.chat_colors]
			local slot_panel = self._ready_slot_panel:panel({
				x = 10,
				name = "slot_" .. tostring(i),
				h = text_font_size,
				y = (i - 1)*text_font_size + 10,
				w = self._ready_slot_panel:w() - 20
			})
			local criminal = slot_panel.text(slot_panel, {
				name = "criminal",
				align = "left",
				blend_mode = "add",
				vertical = "center",
				font_size = text_font_size,
				font = text_font,
				color = color,
				text = tweak_data.gui.LONGEST_CHAR_NAME
			})
			local voice = slot_panel.bitmap(slot_panel, {
				name = "voice",
				visible = false,
				x = 10,
				layer = 2,
				texture = voice_icon,
				texture_rect = voice_texture_rect,
				w = voice_texture_rect[3],
				h = voice_texture_rect[4],
				color = color
			})
			local name = slot_panel.text(slot_panel, {
				vertical = "center",
				name = "name",
				w = 256,
				align = "left",
				blend_mode = "add",
				layer = 1,
				text = managers.localization:text("menu_lobby_player_slot_available") .. "  ",
				font = text_font,
				font_size = text_font_size,
				color = color.with_alpha(color, 0.5),
				h = text_font_size
			})
			local status = slot_panel.text(slot_panel, {
				vertical = "center",
				name = "status",
				w = 256,
				align = "right",
				blend_mode = "add",
				text = "  ",
				visible = false,
				layer = 1,
				font = text_font,
				font_size = text_font_size,
				h = text_font_size,
				color = tweak_data.screen_colors.text:with_alpha(0.5)
			})
			local infamy = slot_panel.bitmap(slot_panel, {
				name = "infamy",
				layer = 2,
				visible = false,
				y = 1,
				texture = infamy_icon,
				texture_rect = infamy_rect,
				color = color
			})
			local detection = slot_panel.panel(slot_panel, {
				name = "detection",
				visible = false,
				layer = 2,
				w = slot_panel.h(slot_panel),
				h = slot_panel.h(slot_panel)
			})
			local detection_ring_left_bg = detection.bitmap(detection, {
				blend_mode = "add",
				name = "detection_left_bg",
				alpha = 0.2,
				texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
				w = detection.w(detection),
				h = detection.h(detection)
			})
			local detection_ring_right_bg = detection.bitmap(detection, {
				blend_mode = "add",
				name = "detection_right_bg",
				alpha = 0.2,
				texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
				w = detection.w(detection),
				h = detection.h(detection)
			})

			detection_ring_right_bg.set_texture_rect(detection_ring_right_bg, detection_ring_right_bg.texture_width(detection_ring_right_bg), 0, -detection_ring_right_bg.texture_width(detection_ring_right_bg), detection_ring_right_bg.texture_height(detection_ring_right_bg))

			local detection_ring_left = detection.bitmap(detection, {
				blend_mode = "add",
				name = "detection_left",
				texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
				render_template = "VertexColorTexturedRadial",
				layer = 1,
				w = detection.w(detection),
				h = detection.h(detection)
			})
			local detection_ring_right = detection.bitmap(detection, {
				blend_mode = "add",
				name = "detection_right",
				texture = "guis/textures/pd2/mission_briefing/inv_detection_meter",
				render_template = "VertexColorTexturedRadial",
				layer = 1,
				w = detection.w(detection),
				h = detection.h(detection)
			})

			detection_ring_right.set_texture_rect(detection_ring_right, detection_ring_right.texture_width(detection_ring_right), 0, -detection_ring_right.texture_width(detection_ring_right), detection_ring_right.texture_height(detection_ring_right))

			local detection_value = slot_panel.text(slot_panel, {
				text = " ",
				name = "detection_value",
				align = "left",
				blend_mode = "add",
				vertical = "center",
				font_size = text_font_size,
				font = text_font,
				color = color
			})

			detection.set_left(detection, slot_panel.w(slot_panel)*0.65)
			detection_value.set_left(detection_value, detection.right(detection) + 2)
			detection_value.set_visible(detection_value, detection.visible(detection))

			local _, _, w, _ = criminal.text_rect(criminal)

			voice.set_left(voice, w + 2)
			criminal.set_w(criminal, w)
			criminal.set_align(criminal, "right")
			criminal.set_text(criminal, "")
			name.set_left(name, voice.right(voice) + 2)
			status.set_right(status, slot_panel.w(slot_panel))
			infamy.set_left(infamy, name.x(name))
		end

		BoxGuiObject:new(self._ready_slot_panel, {
			sides = {
				1,
				1,
				1,
				1
			}
		})
	end

	if not managers.job:has_active_job() then
		return 
	end

	self._current_contact_data = managers.job:current_contact_data()
	self._current_level_data = managers.job:current_level_data()
	self._current_stage_data = managers.job:current_stage_data()
	self._current_job_data = managers.job:current_job_data()
	self._current_job_chain = managers.job:current_job_chain_data()
	self._job_class = (self._current_job_data and self._current_job_data.jc) or 0
	local show_contact_gui = true

	if managers.crime_spree:is_active() then
		self._backdrop:set_pattern("guis/textures/pd2/mission_briefing/bain/bd_pattern", 0.1, "add")

		show_contact_gui = false
	end

	if show_contact_gui then
		local contact_gui = self._background_layer_two:gui(self._current_contact_data.assets_gui, {})
		local contact_pattern = contact_gui.has_script(contact_gui) and contact_gui.script(contact_gui).pattern

		if contact_pattern then
			self._backdrop:set_pattern(contact_pattern, 0.1, "add")
		end
	end

	local padding_y = 60
	self._paygrade_panel = self._background_layer_one:panel({
		w = 210,
		h = 70,
		y = padding_y
	})
	local pg_text = self._foreground_layer_one:text({
		name = "pg_text",
		vertical = "center",
		h = 32,
		align = "right",
		text = utf8.to_upper(managers.localization:text("menu_risk")),
		y = padding_y,
		font_size = content_font_size,
		font = content_font,
		color = tweak_data.screen_colors.text
	})
	local _, _, w, h = pg_text.text_rect(pg_text)

	pg_text.set_size(pg_text, w, h)

	self._paygrade_text = pg_text
	local job_stars = managers.job:current_job_stars()
	local job_and_difficulty_stars = managers.job:current_job_and_difficulty_stars()
	local difficulty_stars = managers.job:current_difficulty_stars()
	local filled_star_rect = {
		0,
		32,
		32,
		32
	}
	local empty_star_rect = {
		32,
		32,
		32,
		32
	}
	local num_stars = 0
	local x = 0
	local y = 0
	local star_size = 18
	local panel_w = 0
	local panel_h = 0
	local risk_color = tweak_data.screen_colors.risk
	local risks = {
		"risk_swat",
		"risk_fbi",
		"risk_death_squad",
		"risk_easy_wish"
	}

	if not Global.SKIP_OVERKILL_290 then
		table.insert(risks, "risk_murder_squad")
		table.insert(risks, "risk_sm_wish")
	end

	for i, name in ipairs(risks) do
		local texture, rect = tweak_data.hud_icons:get_icon_data(name)
		local active = i <= difficulty_stars
		local color = (active and risk_color) or tweak_data.screen_colors.text
		local alpha = (active and 1) or 0.25
		local risk = self._paygrade_panel:bitmap({
			y = 0,
			x = 0,
			name = name,
			texture = texture,
			texture_rect = rect,
			alpha = alpha,
			color = color
		})

		risk.set_position(risk, x, y)

		x = x + risk.w(risk) + 0
		panel_w = math.max(panel_w, risk.right(risk))
		panel_h = math.max(panel_h, risk.h(risk))
	end

	pg_text.set_color(pg_text, risk_color)
	self._paygrade_panel:set_h(panel_h)
	self._paygrade_panel:set_w(panel_w)
	self._paygrade_panel:set_right(self._background_layer_one:w())
	pg_text.set_right(pg_text, self._paygrade_panel:left())

	self._job_schedule_panel = self._background_layer_one:panel({
		h = 70,
		w = self._background_layer_one:w()/2
	})

	self._job_schedule_panel:set_right(self._foreground_layer_one:w())
	self._job_schedule_panel:set_top(padding_y + content_font_size + 15)

	if interupt_stage then
		self._job_schedule_panel:set_alpha(0.2)

		if not tweak_data.levels[interupt_stage].bonus_escape then
			self._interupt_panel = self._background_layer_one:panel({
				h = 125,
				w = self._background_layer_one:w()/2
			})
			local interupt_text = self._interupt_panel:text({
				name = "job_text",
				vertical = "top",
				h = 80,
				font_size = 70,
				align = "left",
				layer = 5,
				text = utf8.to_upper(managers.localization:text("menu_escape")),
				font = bg_font,
				color = tweak_data.screen_colors.important_1
			})
			local _, _, w, h = interupt_text.text_rect(interupt_text)

			interupt_text.set_size(interupt_text, w, h)
			interupt_text.rotate(interupt_text, -15)
			interupt_text.set_center(interupt_text, self._interupt_panel:w()/2, self._interupt_panel:h()/2)
			self._interupt_panel:set_shape(self._job_schedule_panel:shape())
		end
	end

	num_stages = (self._current_job_chain and #self._current_job_chain) or 0
	local day_color = tweak_data.screen_colors.item_stage_1
	local chain = (self._current_job_chain and self._current_job_chain) or {}
	local js_w = self._job_schedule_panel:w()/7
	local js_h = self._job_schedule_panel:h()

	for i = 1, 7, 1 do
		local day_font = text_font
		local day_font_size = text_font_size
		day_color = tweak_data.screen_colors.item_stage_1

		if num_stages < i then
			day_color = tweak_data.screen_colors.item_stage_3
		elseif i == managers.job:current_stage() then
			day_font = content_font
			day_font_size = content_font_size
		end

		local day_text = self._job_schedule_panel:text({
			vertical = "center",
			align = "center",
			blend_mode = "add",
			name = "day_" .. tostring(i),
			text = utf8.to_upper(managers.localization:text("menu_day_short", {
				day = tostring(i)
			})),
			font_size = day_font_size,
			font = day_font,
			w = js_w,
			h = js_h,
			color = day_color
		})

		day_text.set_left(day_text, (i ~= 1 or 0) and self._job_schedule_panel:child("day_" .. tostring(i - 1)):right())

		local ghost = self._job_schedule_panel:bitmap({
			texture = "guis/textures/pd2/cn_minighost",
			h = 16,
			blend_mode = "add",
			w = 16,
			name = "ghost_" .. tostring(i),
			color = tweak_data.screen_colors.ghost_color
		})

		ghost.set_center(ghost, day_text.center_x(day_text), day_text.center_y(day_text) + day_text.h(day_text)*0.25)

		local ghost_visible = i <= num_stages and managers.job:is_job_stage_ghostable(managers.job:current_real_job_id(), i)

		ghost.set_visible(ghost, ghost_visible)

		if ghost_visible then
			self._apply_ghost_color(self, ghost, i, not Network:is_server())
		end
	end

	slot37 = 1
	slot38 = managers.job:current_stage() or 0

	for i = slot37, slot38, 1 do
		local stage_marker = self._job_schedule_panel:bitmap({
			texture = "guis/textures/pd2/mission_briefing/calendar_xo",
			h = 64,
			layer = 1,
			w = 80,
			name = "stage_done_" .. tostring(i),
			texture_rect = {
				(i == managers.job:current_stage() and 80) or 0,
				0,
				80,
				64
			},
			rotation = math.rand(-10, 10)
		})

		stage_marker.set_center(stage_marker, self._job_schedule_panel:child("day_" .. tostring(i)):center())
		stage_marker.move(stage_marker, math.random(4) - 2, math.random(4) - 2)
	end

	if managers.job:has_active_job() then
		local payday_stamp = self._job_schedule_panel:bitmap({
			texture = "guis/textures/pd2/mission_briefing/calendar_xo",
			name = "payday_stamp",
			h = 64,
			layer = 2,
			w = 96,
			texture_rect = {
				160,
				0,
				96,
				64
			},
			rotation = math.rand(-5, 5)
		})

		payday_stamp.set_center(payday_stamp, self._job_schedule_panel:child("day_" .. tostring(num_stages)):center())
		payday_stamp.move(payday_stamp, math.random(4) - 2 - 7, math.random(4) - 2 + 8)

		if payday_stamp.rotation(payday_stamp) == 0 then
			payday_stamp.set_rotation(payday_stamp, 1)
		end
	end

	local job_overview_text = self._foreground_layer_one:text({
		name = "job_overview_text",
		vertical = "bpttom",
		align = "left",
		text = utf8.to_upper(managers.localization:text("menu_job_overview")),
		h = content_font_size,
		font_size = content_font_size,
		font = content_font,
		color = tweak_data.screen_colors.text
	})
	local _, _, w, h = job_overview_text.text_rect(job_overview_text)

	job_overview_text.set_size(job_overview_text, w, h)
	job_overview_text.set_leftbottom(job_overview_text, self._job_schedule_panel:left(), pg_text.bottom(pg_text))
	job_overview_text.set_y(job_overview_text, math.round(job_overview_text.y(job_overview_text)))

	self._job_overview_text = job_overview_text

	self._paygrade_panel:set_center_y(job_overview_text.center_y(job_overview_text))
	pg_text.set_center_y(pg_text, job_overview_text.center_y(job_overview_text))
	pg_text.set_y(pg_text, math.round(pg_text.y(pg_text)))

	if pg_text.left(pg_text) <= job_overview_text.right(job_overview_text) + 15 then
		pg_text.move(pg_text, 0, -pg_text.h(pg_text))
		self._paygrade_panel:move(0, -pg_text.h(pg_text))
	end

	local text = utf8.to_upper(managers.localization:text(self._current_contact_data.name_id) .. ": " .. managers.localization:text(self._current_job_data.name_id))
	local text_align, text_len = nil

	if managers.crime_spree:is_active() then
		local level_id = Global.game_settings.level_id
		local name_id = level_id and tweak_data.levels[level_id] and tweak_data.levels[level_id].name_id
		local mission = managers.crime_spree:get_mission()
		text = managers.localization:to_upper_text(name_id) .. ": "
		text_len = utf8.len(text)
		text = text .. "+" .. managers.localization:text("menu_cs_level", {
			level = (mission and mission.add) or 0
		})
		text_align = "right"
	end

	local job_text = self._foreground_layer_one:text({
		vertical = "top",
		name = "job_text",
		text = text,
		align = text_align or "left",
		font_size = title_font_size,
		font = title_font,
		color = tweak_data.screen_colors.text
	})

	if managers.crime_spree:is_active() then
		job_text.set_range_color(job_text, text_len, utf8.len(text), tweak_data.screen_colors.crime_spree_risk)
	end

	if not text_align then
		local big_text = self._background_layer_three:text({
			vertical = "top",
			name = "job_text",
			alpha = 0.4,
			text = text,
			align = text_align or "left",
			font_size = bg_font_size,
			font = bg_font,
			color = tweak_data.screen_colors.button_stage_1
		})

		big_text.set_world_center_y(big_text, self._foreground_layer_one:child("job_text"):world_center_y())
		big_text.set_world_x(big_text, self._foreground_layer_one:child("job_text"):world_x())
		big_text.move(big_text, -13, 9)
		self._backdrop:animate_bg_text(big_text)
	end

	if managers.crime_spree:is_active() then
		self._paygrade_panel:set_visible(false)
		self._job_schedule_panel:set_visible(false)
		self._paygrade_text:set_visible(false)
		self._job_overview_text:set_visible(false)
	end

	return 
end
HUDMissionBriefing._apply_ghost_color = function (self, ghost, i, is_unknown)
	local accumulated_ghost_bonus = managers.job:get_accumulated_ghost_bonus()
	local agb = accumulated_ghost_bonus and accumulated_ghost_bonus[i]

	if is_unknown then
		ghost.set_color(ghost, Color(64, 255, 255, 255)/255)
	elseif i == managers.job:current_stage() then
		if not managers.groupai or not managers.groupai:state():whisper_mode() then
			ghost.set_color(ghost, Color(255, 255, 51, 51)/255)
		else
			ghost.set_color(ghost, Color(128, 255, 255, 255)/255)
		end
	elseif agb and agb.ghost_success then
		ghost.set_color(ghost, tweak_data.screen_colors.ghost_color)
	elseif i < managers.job:current_stage() then
		ghost.set_color(ghost, Color(255, 255, 51, 51)/255)
	else
		ghost.set_color(ghost, Color(128, 255, 255, 255)/255)
	end

	return 
end
HUDMissionBriefing.on_whisper_mode_changed = function (self)
	if alive(self._job_schedule_panel) then
		local i = managers.job:current_stage() or 1
		local ghost_icon = self._job_schedule_panel:child("ghost_" .. tostring(i))

		if alive(ghost_icon) then
			self._apply_ghost_color(self, ghost_icon, i)
		end
	end

	return 
end
HUDMissionBriefing.hide = function (self)
	self._backdrop:hide()

	if alive(self._background_layer_two) then
		self._background_layer_two:clear()
	end

	return 
end
HUDMissionBriefing.show = function (self)
	print("SHOW")
	self._backdrop:show()

	return 
end
HUDMissionBriefing.inside_slot = function (self, peer_id, child, x, y)
	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer_id))

	if not slot or not alive(slot) then
		return nil
	end

	local object = slot.child(slot, child)

	if not object or not alive(object) then
		return nil
	end

	if not slot.child(slot, "status") or not alive(slot.child(slot, "status")) or not slot.child(slot, "status"):visible() then
		return 
	end

	return object.inside(object, x, y)
end
HUDMissionBriefing.set_player_slot = function (self, nr, params)
	print("set_player_slot( nr, params )", nr, params)

	local slot = self._ready_slot_panel:child("slot_" .. tostring(nr))

	if not slot or not alive(slot) then
		return 
	end

	slot.child(slot, "status"):stop()
	slot.child(slot, "status"):set_alpha(1)
	slot.child(slot, "status"):set_color(slot.child(slot, "status"):color():with_alpha(1))
	slot.child(slot, "status"):set_font_size(tweak_data.menu.pd2_small_font_size)
	slot.child(slot, "name"):set_color(slot.child(slot, "name"):color():with_alpha(1))
	slot.child(slot, "name"):set_text(params.name)
	slot.child(slot, "criminal"):set_color(slot.child(slot, "criminal"):color():with_alpha(1))
	slot.child(slot, "criminal"):set_text(managers.localization:to_upper_text("menu_" .. tostring(params.character)))

	local name_len = utf8.len(slot.child(slot, "name"):text())
	local experience = ((0 < params.rank and managers.experience:rank_string(params.rank) .. "-") or "") .. tostring(params.level)

	slot.child(slot, "name"):set_text(slot.child(slot, "name"):text() .. " (" .. experience .. ")  ")

	if 0 < params.rank then
		slot.child(slot, "infamy"):set_visible(true)
		slot.child(slot, "name"):set_x(slot.child(slot, "infamy"):right())
	else
		slot.child(slot, "infamy"):set_visible(false)
	end

	if params.status then
		slot.child(slot, "status"):set_text(params.status)
	end

	return 
end
HUDMissionBriefing.set_slot_joining = function (self, peer, peer_id)
	print("set_slot_joining( peer, peer_id )", peer, peer_id)

	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer_id))

	if not slot or not alive(slot) then
		return 
	end

	slot.child(slot, "voice"):set_visible(false)
	slot.child(slot, "infamy"):set_visible(false)
	slot.child(slot, "status"):stop()
	slot.child(slot, "status"):set_alpha(1)
	slot.child(slot, "status"):set_color(slot.child(slot, "status"):color():with_alpha(1))
	slot.child(slot, "criminal"):set_color(slot.child(slot, "criminal"):color():with_alpha(1))
	slot.child(slot, "criminal"):set_text(managers.localization:to_upper_text("menu_" .. tostring(peer.character(peer))))
	slot.child(slot, "name"):set_text(peer.name(peer) .. "  ")
	slot.child(slot, "status"):set_visible(true)
	slot.child(slot, "status"):set_text(managers.localization:text("menu_waiting_is_joining"))
	slot.child(slot, "status"):set_font_size(tweak_data.menu.pd2_small_font_size)

	local function animate_joining(o)
		local t = 0

		while true do
			t = (t + coroutine.yield())%1

			o.set_alpha(o, math.sin(t*180)*0.7 + 0.3)
		end

		return 
	end

	slot.child(slot, "status"):animate(animate_joining)

	return 
end
HUDMissionBriefing.set_slot_ready = function (self, peer, peer_id)
	print("set_slot_ready( peer, peer_id )", peer, peer_id)

	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer_id))

	if not slot or not alive(slot) then
		return 
	end

	slot.child(slot, "status"):stop()
	slot.child(slot, "status"):set_blend_mode("add")
	slot.child(slot, "status"):set_visible(true)
	slot.child(slot, "status"):set_alpha(1)
	slot.child(slot, "status"):set_color(slot.child(slot, "status"):color():with_alpha(1))
	slot.child(slot, "status"):set_text(managers.localization:text("menu_waiting_is_ready"))
	slot.child(slot, "status"):set_font_size(tweak_data.menu.pd2_small_font_size)
	managers.menu_component:flash_ready_mission_briefing_gui()

	return 
end
HUDMissionBriefing.set_slot_not_ready = function (self, peer, peer_id)
	print("set_slot_not_ready( peer, peer_id )", peer, peer_id)

	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer_id))

	if not slot or not alive(slot) then
		return 
	end

	slot.child(slot, "status"):stop()
	slot.child(slot, "status"):set_visible(true)
	slot.child(slot, "status"):set_alpha(1)
	slot.child(slot, "status"):set_color(slot.child(slot, "status"):color():with_alpha(1))
	slot.child(slot, "status"):set_text(managers.localization:text("menu_waiting_is_not_ready"))
	slot.child(slot, "status"):set_font_size(tweak_data.menu.pd2_small_font_size)

	return 
end
HUDMissionBriefing.set_dropin_progress = function (self, peer_id, progress_percentage, mode)
	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer_id))

	if not slot or not alive(slot) then
		return 
	end

	slot.child(slot, "status"):stop()
	slot.child(slot, "status"):set_visible(true)
	slot.child(slot, "status"):set_alpha(1)

	local status_text = (mode == "join" and "menu_waiting_is_joining") or "debug_loading_level"

	slot.child(slot, "status"):set_text(utf8.to_upper(managers.localization:text(status_text) .. " " .. tostring(progress_percentage) .. "%"))
	slot.child(slot, "status"):set_font_size(tweak_data.menu.pd2_small_font_size)

	return 
end
HUDMissionBriefing.set_kit_selection = function (self, peer_id, category, id, slot)
	print("set_kit_selection( peer_id, category, id, slot )", peer_id, category, id, slot)

	return 
end
HUDMissionBriefing.set_slot_outfit = function (self, peer_id, criminal_name, outfit)
	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer_id))

	if not slot or not alive(slot) then
		return 
	end

	if managers.network:session() and not managers.network:session():peer(peer_id) then
		return 
	end

	local detection, reached = managers.blackmarket:get_suspicion_offset_of_outfit_string(outfit, tweak_data.player.SUSPICION_OFFSET_LERP or 0.75)
	local detection_panel = slot.child(slot, "detection")

	detection_panel.child(detection_panel, "detection_left"):set_color(Color(detection*0.5 + 0.5, 1, 1))
	detection_panel.child(detection_panel, "detection_right"):set_color(Color(detection*0.5 + 0.5, 1, 1))
	detection_panel.set_visible(detection_panel, true)
	slot.child(slot, "detection_value"):set_visible(detection_panel.visible(detection_panel))
	slot.child(slot, "detection_value"):set_text(math.round(detection*100))

	if reached then
		slot.child(slot, "detection_value"):set_color(Color(255, 255, 42, 0)/255)
	else
		slot.child(slot, "detection_value"):set_color(tweak_data.screen_colors.text)
	end

	return 
end
HUDMissionBriefing.set_slot_voice = function (self, peer, peer_id, active)
	print("set_slot_voice( peer, peer_id, active )", peer, peer_id, active)

	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer_id))

	if not slot or not alive(slot) then
		return 
	end

	slot.child(slot, "voice"):set_visible(active)

	return 
end
HUDMissionBriefing.remove_player_slot_by_peer_id = function (self, peer, reason)
	print("remove_player_slot_by_peer_id( peer, reason )", peer, reason)

	local slot = self._ready_slot_panel:child("slot_" .. tostring(peer.id(peer)))

	if not slot or not alive(slot) then
		return 
	end

	slot.child(slot, "status"):stop()
	slot.child(slot, "status"):set_alpha(1)
	slot.child(slot, "criminal"):set_text("")
	slot.child(slot, "name"):set_text(utf8.to_upper(managers.localization:text("menu_lobby_player_slot_available")))
	slot.child(slot, "status"):set_text("")
	slot.child(slot, "status"):set_visible(false)
	slot.child(slot, "voice"):set_visible(false)
	slot.child(slot, "status"):set_font_size(tweak_data.menu.pd2_small_font_size)
	slot.child(slot, "name"):set_x(slot.child(slot, "infamy"):x())
	slot.child(slot, "infamy"):set_visible(false)
	slot.child(slot, "detection"):set_visible(false)
	slot.child(slot, "detection_value"):set_visible(slot.child(slot, "detection"):visible())

	return 
end
HUDMissionBriefing.update_layout = function (self)
	self._backdrop:_set_black_borders()

	return 
end
HUDMissionBriefing.reload = function (self)
	self._backdrop:close()

	self._backdrop = nil

	HUDMissionBriefing.init(self, self._hud, self._workspace)

	return 
end

return 
