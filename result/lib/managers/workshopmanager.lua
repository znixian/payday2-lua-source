require("lib/managers/workshop/UGCItem")

WorkshopManager = WorkshopManager or class()
WorkshopManager.PATH = "workshop/"
WorkshopManager.FULL_PATH = Application:base_path() .. WorkshopManager.PATH
WorkshopManager.STAGING_NAME = "temporary_staging"
local UGC = Steam:ugc_handler()
WorkshopManager.init = function (self)
	if self._initialized then
		return 
	end

	self._initialized = false

	self._setup(self)

	if managers.user then
		self.set_enabled(self, managers.user:get_setting("workshop"))
	end

	return 
end
WorkshopManager.items = function (self)
	return self._items
end
WorkshopManager.create_item = function (self, type)
	local path = self._new_item_path(self)

	if SystemFS:make_dir(path) then
		local item = UGCItem:new(path)

		if type == "weapon_skin" then
			item.add_tag(item, "Weapons")
		end

		if type == "armor_skin" then
			item.add_tag(item, "Armors")
		end

		item.save(item)
		table.insert_sorted(self._items, item, UGCItem.SortByTimestamp)

		return item
	end

	return nil
end
WorkshopManager.delete_item = function (self, item)
	local path = item.path(item)

	if SystemFS:exists(path) then
		SystemFS:delete_file(Application:nice_path(path, false))
		table.delete(self._items, item)
	end

	return 
end
WorkshopManager.is_initialized = function (self)
	return self._initialized
end
WorkshopManager.set_enabled = function (self, enabled)
	self._enabled = enabled

	return 
end
WorkshopManager.enabled = function (self)
	return self._enabled
end
WorkshopManager.create_staging_directory = function (self)
	local path = Application:create_temporary_folder()

	if not path or path == "" then
		local basepath = WorkshopManager.FULL_PATH .. WorkshopManager.STAGING_NAME
		path = basepath
		local count = 1

		while SystemFS:exists(path) do
			path = basepath .. count
			count = count + 1
		end

		if not SystemFS:make_dir(path) then
			return nil
		end
	end

	return path .. "/"
end
WorkshopManager._setup = function (self)
	if not SystemFS:exists(WorkshopManager.FULL_PATH) and not SystemFS:make_dir(WorkshopManager.FULL_PATH) then
		return 
	end

	self._init_items(self)

	self._initialized = true

	return 
end
WorkshopManager._new_item_path = function (self)
	local date = Application:date()
	date = date.gsub(date, ":", "")
	date = date.gsub(date, " ", "-")
	local date_path = WorkshopManager.FULL_PATH .. date
	local next_path = date_path
	local counter = 2

	while SystemFS:exists(next_path) do
		next_path = date_path .. "_" .. counter
		counter = counter + 1
	end

	return next_path .. "/"
end
WorkshopManager._init_items = function (self)
	self._items = {}
	local directories = SystemFS:list(WorkshopManager.FULL_PATH, true)

	for _, dir in ipairs(directories) do
		local item_path = WorkshopManager.FULL_PATH .. dir .. "/"
		local item = UGCItem:new(item_path)

		if item.load(item) then
			table.insert_sorted(self._items, item, UGCItem.SortByTimestamp)
		end
	end

	return 
end

return 
