BanListManager = BanListManager or class()
BanListManager.init = function (self)
	if not Global.ban_list then
		Global.ban_list = {}
	end

	self._global = self._global or Global.ban_list
	self._global.banned = self._global.banned or {}

	return 
end
BanListManager.ban = function (self, identifier, name)
	table.insert(self._global.banned, {
		name = name,
		identifier = identifier
	})

	return 
end
BanListManager.unban = function (self, identifier)
	local user_index = nil

	for index, user in ipairs(self._global.banned) do
		if user.identifier == identifier then
			user_index = index

			break
		end
	end

	if user_index then
		table.remove(self._global.banned, user_index)
	end

	return 
end
BanListManager.banned = function (self, identifier)
	for _, user in ipairs(self._global.banned) do
		if user.identifier == identifier then
			return true
		end
	end

	return false
end
BanListManager.ban_list = function (self)
	return self._global.banned
end
BanListManager.save = function (self, data)
	data.ban_list = self._global

	return 
end
BanListManager.load = function (self, data)
	if data.ban_list then
		Global.ban_list = data.ban_list
		self._global = Global.ban_list
	end

	return 
end

return 
