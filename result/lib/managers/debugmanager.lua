core:module("DebugManager")
core:import("CoreDebugManager")
core:import("CoreClass")

DebugManager = DebugManager or class(CoreDebugManager.DebugManager)
DebugManager.qa_debug = function (self, username)
	self.set_qa_debug_enabled(self, username, true)

	return 
end
DebugManager.get_qa_debug_enabled = function (self)
	return self._qa_debug_enabled
end
DebugManager.set_qa_debug_enabled = function (self, username, enabled)
	enabled = not not enabled
	local cat_print_list = {
		"qa"
	}

	for _, cat in ipairs(cat_print_list) do
		Global.category_print[cat] = enabled
	end

	self._qa_debug_enabled = enabled

	return 
end

CoreClass.override_class(CoreDebugManager.DebugManager, DebugManager)

return 
