_OcclusionManager = _OcclusionManager or class()
_OcclusionManager.init = function (self)
	self._model_ids = Idstring("model")
	self._skip_occlusion = {}

	return 
end
_OcclusionManager.is_occluded = function (self, unit)
	if self._skip_occlusion[unit.key(unit)] then
		return false
	end

	return unit.occluded(unit)
end
_OcclusionManager.remove_occlusion = function (self, unit)
	if alive(unit) then
		local objects = unit.get_objects_by_type(unit, self._model_ids)

		for _, obj in pairs(objects) do
			obj.set_skip_occlusion(obj, true)
		end
	end

	self._skip_occlusion[unit.key(unit)] = true

	return 
end
_OcclusionManager.add_occlusion = function (self, unit)
	if alive(unit) then
		local objects = unit.get_objects_by_type(unit, self._model_ids)

		for _, obj in pairs(objects) do
			obj.set_skip_occlusion(obj, false)
		end
	end

	self._skip_occlusion[unit.key(unit)] = nil

	return 
end

return 
