KillzoneManager = KillzoneManager or class()
KillzoneManager.init = function (self)
	self._units = {}

	return 
end
KillzoneManager.update = function (self, t, dt)
	for _, data in pairs(self._units) do
		if alive(data.unit) then
			if data.type == "sniper" then
				data.timer = data.timer + dt

				if data.next_shot < data.timer then
					local warning_time = 4
					data.next_shot = data.timer + math.rand((warning_time < data.timer and 0.5) or 1)
					local warning_shot = math.max(warning_time - data.timer, 1)
					warning_shot = 0.75 < math.rand(warning_shot)

					if warning_shot then
						self._warning_shot(self, data.unit)
					else
						self._deal_damage(self, data.unit)
					end
				end
			elseif data.type == "gas" then
				data.timer = data.timer + dt

				if data.next_gas < data.timer then
					data.next_gas = data.timer + 0.25

					self._deal_gas_damage(self, data.unit)
				end
			elseif data.type == "fire" then
				data.timer = data.timer + dt

				if data.next_fire < data.timer then
					data.next_fire = data.timer + 0.25

					self._deal_fire_damage(self, data.unit)
				end
			end
		end
	end

	return 
end
KillzoneManager.set_unit = function (self, unit, type)
	if self._units[unit.key(unit)] then
		self._remove_unit(self, unit)
	else
		self._add_unit(self, unit, type)
	end

	return 
end
KillzoneManager._warning_shot = function (self, unit)
	local rot = unit.camera(unit):rotation()
	rot = Rotation(rot.yaw(rot), 0, 0)
	local pos = unit.position(unit) + rot.y(rot)*(math.random(200) + 100)
	local dir = Rotation(math.rand(360), 0, 0):y()
	dir = dir.with_z(dir, -0.4):normalized()
	local from_pos = pos + dir*-100
	local to_pos = pos + dir*100
	local col_ray = World:raycast("ray", from_pos, to_pos, "slot_mask", managers.slot:get_mask("bullet_impact_targets"), "ignore_unit", unit)

	if col_ray and col_ray.unit then
		managers.game_play_central:play_impact_sound_and_effects({
			col_ray = col_ray
		})
	end

	return 
end
KillzoneManager._deal_damage = function (self, unit)
	if unit.character_damage(unit):need_revive() then
		return 
	end

	local col_ray = {}
	local ray = Rotation(math.rand(360), 0, 0):y()
	ray = ray.with_z(ray, -0.4):normalized()
	col_ray.ray = ray
	local attack_data = {
		damage = 1,
		col_ray = col_ray
	}

	unit.character_damage(unit):damage_killzone(attack_data)

	return 
end
KillzoneManager._deal_gas_damage = function (self, unit)
	local attack_data = {
		damage = 0.75,
		col_ray = {
			ray = math.UP
		}
	}

	unit.character_damage(unit):damage_killzone(attack_data)

	return 
end
KillzoneManager._deal_fire_damage = function (self, unit)
	local attack_data = {
		damage = 0.5,
		col_ray = {
			ray = math.UP
		}
	}

	unit.character_damage(unit):damage_killzone(attack_data)

	return 
end
KillzoneManager._add_unit = function (self, unit, type)
	if type == "sniper" then
		local next_shot = math.rand(1)
		self._units[unit.key(unit)] = {
			timer = 0,
			type = type,
			next_shot = next_shot,
			unit = unit
		}
	elseif type == "gas" then
		local next_gas = math.rand(1)
		self._units[unit.key(unit)] = {
			timer = 0,
			type = type,
			next_gas = next_gas,
			unit = unit
		}
	elseif type == "fire" then
		local next_fire = math.rand(1)
		self._units[unit.key(unit)] = {
			timer = 0,
			type = type,
			next_fire = next_fire,
			unit = unit
		}
	end

	return 
end
KillzoneManager._remove_unit = function (self, unit)
	self._units[unit.key(unit)] = nil

	return 
end

return 
