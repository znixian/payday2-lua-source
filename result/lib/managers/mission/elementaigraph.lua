core:import("CoreMissionScriptElement")

ElementAIGraph = ElementAIGraph or class(CoreMissionScriptElement.MissionScriptElement)
ElementAIGraph.init = function (self, ...)
	ElementAIGraph.super.init(self, ...)

	return 
end
ElementAIGraph.on_script_activated = function (self)
	return 
end
ElementAIGraph.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementAIGraph.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.graph_ids) do
		managers.navigation:set_nav_segment_state(id, self._values.operation)
	end

	ElementAIGraph.super.on_executed(self, instigator)

	return 
end

return 
