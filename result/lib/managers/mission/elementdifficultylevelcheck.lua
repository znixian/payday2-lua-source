core:import("CoreMissionScriptElement")

ElementDifficultyLevelCheck = ElementDifficultyLevelCheck or class(CoreMissionScriptElement.MissionScriptElement)
ElementDifficultyLevelCheck.init = function (self, ...)
	ElementDifficultyLevelCheck.super.init(self, ...)

	return 
end
ElementDifficultyLevelCheck.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementDifficultyLevelCheck.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local diff = (Global.game_settings and Global.game_settings.difficulty) or "hard"

	if (self._values.difficulty ~= "overkill" or diff ~= "overkill_145" or false) and self._values.difficulty ~= diff then
		return 
	end

	ElementDifficultyLevelCheck.super.on_executed(self, instigator)

	return 
end

return 
