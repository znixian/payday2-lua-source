core:import("CoreMissionScriptElement")

ElementCarry = ElementCarry or class(CoreMissionScriptElement.MissionScriptElement)
ElementCarry.init = function (self, ...)
	ElementCarry.super.init(self, ...)

	return 
end
ElementCarry.on_executed = function (self, instigator)
	if not self._values.enabled or not alive(instigator) then
		return 
	end

	local execute = true

	if self._values.type_filter and self._values.type_filter ~= "none" then
		local carry_ext = instigator.carry_data(instigator)

		if not carry_ext then
			debug_pause_unit(instigator, "[ElementCarry:on_executed] instigator missing carry_data extension", instigator)

			return 
		end

		local carry_id = carry_ext.carry_id(carry_ext)

		if carry_id ~= self._values.type_filter then
			return 
		end
	end

	if self._values.operation == "remove" then
		if Network:is_server() then
			instigator.set_slot(instigator, 0)
		end
	elseif self._values.operation == "add_to_respawn" then
		if Network:is_server() then
			local carry_ext = instigator.carry_data(instigator)
			local carry_id = carry_ext.carry_id(carry_ext)
			local multiplier = carry_ext.multiplier(carry_ext)

			managers.loot:add_to_respawn(carry_id, multiplier)
			instigator.set_slot(instigator, 0)
		end
	elseif self._values.operation == "freeze" then
		if instigator.damage(instigator):has_sequence("freeze") then
			instigator.damage(instigator):run_sequence_simple("freeze")
		else
			debug_pause("[ElementCarry:on_executed] instigator missing freeze sequence", instigator)
		end
	elseif self._values.operation == "secure" or self._values.operation == "secure_silent" then
		local carry_ext = instigator.carry_data(instigator)

		if carry_ext then
			if 0 < carry_ext.value(carry_ext) then
				carry_ext.disarm(carry_ext)

				if Network:is_server() then
					local silent = self._values.operation == "secure_silent"
					local carry_id = carry_ext.carry_id(carry_ext)
					local multiplier = carry_ext.multiplier(carry_ext)
					local peer_id = carry_ext.latest_peer_id(carry_ext)

					managers.loot:secure(carry_id, multiplier, silent, peer_id)
				end

				carry_ext.set_value(carry_ext, 0)

				if instigator.damage(instigator):has_sequence("secured") then
					instigator.damage(instigator):run_sequence_simple("secured")
				else
					debug_pause("[ElementCarry:on_executed] instigator missing secured sequence", instigator)
				end
			else
				execute = false
			end
		else
			debug_pause("[ElementCarry:on_executed] instigator missing carry_data extension", instigator)
		end
	elseif self._values.operation == "poof" then
		local carry_ext = instigator.carry_data(instigator)

		if carry_ext and carry_ext.can_poof(carry_ext) then
			carry_ext.poof(carry_ext)
		end
	end

	if execute then
		ElementCarry.super.on_executed(self, instigator)
	end

	return 
end
ElementCarry.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end

return 
