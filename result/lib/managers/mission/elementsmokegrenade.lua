core:import("CoreMissionScriptElement")

ElementSmokeGrenade = ElementSmokeGrenade or class(CoreMissionScriptElement.MissionScriptElement)
ElementSmokeGrenade.init = function (self, ...)
	ElementSmokeGrenade.super.init(self, ...)

	return 
end
ElementSmokeGrenade.client_on_executed = function (self, ...)
	return 
end
ElementSmokeGrenade.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local is_flashbang = self._values.effect_type == "flash"

	managers.groupai:state():queue_smoke_grenade(self.id(self), self._values.position, self._values.duration, self._values.ignore_control, is_flashbang)

	if self._values.immediate and (managers.groupai:state():get_assault_mode() or self._values.ignore_control) then
		managers.groupai:state():detonate_world_smoke_grenade(self.id(self))
	end

	ElementSmokeGrenade.super.on_executed(self, instigator)

	return 
end

return 
