core:import("CoreMissionScriptElement")

ElementLoadDelayed = ElementLoadDelayed or class(CoreMissionScriptElement.MissionScriptElement)
ElementLoadDelayed.init = function (self, ...)
	ElementLoadDelayed.super.init(self, ...)

	return 
end
ElementLoadDelayed.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementLoadDelayed.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if not Application:editor() then
		for _, unit_id in ipairs(self._values.unit_ids) do
			managers.worlddefinition:create_delayed_unit(unit_id)
		end
	end

	ElementLoadDelayed.super.on_executed(self, instigator)

	return 
end

return 
