ElementVehicleTrigger = ElementVehicleTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementVehicleTrigger.init = function (self, ...)
	ElementVehicleTrigger.super.init(self, ...)

	return 
end
ElementVehicleTrigger.on_script_activated = function (self)
	if Network:is_server() then
		managers.vehicle:add_listener(self._id, {
			self._values.event
		}, callback(self, self, "on_executed"))
	end

	return 
end
ElementVehicleTrigger.on_enter = function (self, instigator)
	self.on_executed(self, instigator)

	return 
end
ElementVehicleTrigger.on_exit = function (self, instigator)
	self.on_executed(self, instigator)

	return 
end
ElementVehicleTrigger.on_all_inside = function (self, instigator)
	self.on_executed(self, instigator)

	return 
end
ElementVehicleTrigger.send_to_host = function (self, instigator)
	if instigator then
		managers.network:session():send_to_host("to_server_mission_element_trigger", self._id, nil)
	end

	return 
end
ElementVehicleTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementVehicleTrigger.super.on_executed(self, self._unit or instigator)

	return 
end

return 
