core:import("CoreMissionScriptElement")

ElementTeleportPlayer = ElementTeleportPlayer or class(CoreMissionScriptElement.MissionScriptElement)
ElementTeleportPlayer.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementTeleportPlayer.on_executed = function (self, instigator)
	ElementTeleportPlayer.super.on_executed(self, instigator)

	if not self._values.enabled then
		return 
	end

	local player = instigator or managers.player:player_unit()

	if player ~= managers.player:player_unit() then
		return 
	end

	if alive(player) then
		player.movement(player):trigger_teleport(self._values)

		if self._values.refill then
			player.character_damage(player):replenish()

			slot3 = pairs
			slot4 = player.inventory(player):available_selections() or {}

			for id, weapon in slot3(slot4) do
				if alive(weapon.unit) then
					weapon.unit:base():replenish()
					managers.hud:set_ammo_amount(id, weapon.unit:base():ammo_info())
				end
			end
		end
	end

	return 
end

return 
