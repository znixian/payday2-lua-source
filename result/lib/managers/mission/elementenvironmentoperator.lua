ElementEnvironmentOperator = ElementEnvironmentOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementEnvironmentOperator.init = function (self, ...)
	ElementEnvironmentOperator.super.init(self, ...)

	return 
end
ElementEnvironmentOperator.stop_simulation = function (self, ...)
	if self._old_default_environment then
		managers.viewport:set_default_environment(self._old_default_environment, nil, nil)
	end

	ElementEnvironmentOperator.super.destroy(self, ...)

	return 
end
ElementEnvironmentOperator.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementEnvironmentOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self._old_default_environment = managers.viewport:default_environment()
	local operation = self.value(self, "operation")

	if not operation or operation == "set" then
		managers.viewport:set_default_environment(self._values.environment, self.value(self, "blend_time"), nil)
	elseif operation == "enable_global_override" then
		managers.viewport:set_override_environment(self._values.environment, self.value(self, "blend_time"), nil)
	elseif operation == "disable_global_override" then
		managers.viewport:set_override_environment(nil, self.value(self, "blend_time"), nil)
	end

	ElementEnvironmentOperator.super.on_executed(self, instigator)

	return 
end

return 
