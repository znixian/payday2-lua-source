core:import("CoreMissionScriptElement")

ElementAccessCamera = ElementAccessCamera or class(CoreMissionScriptElement.MissionScriptElement)
ElementAccessCamera.init = function (self, ...)
	ElementAccessCamera.super.init(self, ...)

	self._camera_unit = nil
	self._triggers = {}

	return 
end
ElementAccessCamera.on_script_activated = function (self)
	if self._values.camera_u_id then
		local id = self._values.camera_u_id
		local unit = nil

		if Global.running_simulation then
			unit = managers.editor:unit_with_id(id)
		else
			unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "_load_unit"))
		end

		if unit then
			unit.base(unit):set_access_camera_mission_element(self)

			self._camera_unit = unit
		end
	end

	self._has_fetched_units = true

	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementAccessCamera._load_unit = function (self, unit)
	unit.base(unit):set_access_camera_mission_element(self)

	self._camera_unit = unit

	return 
end
ElementAccessCamera.client_on_executed = function (self, ...)
	return 
end
ElementAccessCamera.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementAccessCamera.super.on_executed(self, instigator)

	return 
end
ElementAccessCamera.access_camera_operation_destroy = function (self)
	self._values.destroyed = true

	self.check_triggers(self, "destroyed")

	return 
end
ElementAccessCamera.add_trigger = function (self, id, type, callback)
	self._triggers[type] = self._triggers[type] or {}
	self._triggers[type][id] = {
		callback = callback
	}

	return 
end
ElementAccessCamera.remove_trigger = function (self, id, type)
	if self._triggers[type] then
		self._triggers[type][id] = nil
	end

	return 
end
ElementAccessCamera.trigger_accessed = function (self, instigator)
	if Network:is_client() then
		managers.network:session():send_to_host("to_server_access_camera_trigger", self._id, "accessed", instigator)
	else
		self.check_triggers(self, "accessed", instigator)
	end

	return 
end
ElementAccessCamera.check_triggers = function (self, type, instigator)
	if not self._triggers[type] then
		return 
	end

	for id, cb_data in pairs(self._triggers[type]) do
		cb_data.callback(instigator)
	end

	return 
end
ElementAccessCamera.enabled = function (self, ...)
	if alive(self._camera_unit) then
		if self._camera_unit:base() and self._camera_unit:base().is_security_camera and self._camera_unit:base().access_enabled then
			return self._camera_unit:base():access_enabled()
		end

		return self._camera_unit:enabled()
	end

	return ElementAccessCamera.super.enabled(self, ...)
end
ElementAccessCamera.has_camera_unit = function (self)
	return (alive(self._camera_unit) and true) or false
end
ElementAccessCamera.camera_unit = function (self)
	if alive(self._camera_unit) then
		return self._camera_unit
	end

	return nil
end
ElementAccessCamera.camera_position = function (self)
	if alive(self._camera_unit) then
		return self._camera_unit:get_object(Idstring("CameraLens")):position()
	end

	return self.value(self, "position")
end
ElementAccessCamera.camera_rotation = function (self)
	return self.value(self, "rotation")
end
ElementAccessCamera.is_moving = function (self)
	return alive(self._camera_unit) and self._camera_unit:base() and self._camera_unit:base().update_position
end
ElementAccessCamera.m_camera_rotation = function (self, m_rot)
	if not m_rot then
		return self.camera_rotation(self)
	end

	if alive(self._camera_unit) then
		self._camera_unit:get_object(Idstring("CameraLens")):m_rotation(m_rot)

		return m_rot
	end

	mrotation.set_zero(m_rot)
	mrotation.multiply(m_rot, self.value(self, "rotation"))

	return m_rot
end
ElementAccessCamera.m_camera_position = function (self, m_vec)
	if not m_vec then
		return self.camera_position(self)
	end

	if alive(self._camera_unit) then
		self._camera_unit:get_object(Idstring("CameraLens")):m_position(m_vec)

		return m_vec
	end

	mvector3.set(m_vec, self.value(self, "position"))

	return m_vec
end
ElementAccessCamera.save = function (self, data)
	data.enabled = self._values.enabled
	data.destroyed = self._values.destroyed

	return 
end
ElementAccessCamera.load = function (self, data)
	self.set_enabled(self, data.enabled)

	self._values.destroyed = data.destroyed

	if not self._has_fetched_units then
		self.on_script_activated(self)
	end

	return 
end
ElementAccessCameraOperator = ElementAccessCameraOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementAccessCameraOperator.init = function (self, ...)
	ElementAccessCameraOperator.super.init(self, ...)

	return 
end
ElementAccessCameraOperator.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementAccessCameraOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element and self._values.operation == "destroy" then
			element.access_camera_operation_destroy(element)
		end
	end

	ElementAccessCameraOperator.super.on_executed(self, instigator)

	return 
end
ElementAccessCameraTrigger = ElementAccessCameraTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementAccessCameraTrigger.init = function (self, ...)
	ElementAccessCameraTrigger.super.init(self, ...)

	return 
end
ElementAccessCameraTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_trigger(element, self._id, self._values.trigger_type, callback(self, self, "on_executed"))
	end

	return 
end
ElementAccessCameraTrigger.client_on_executed = function (self, ...)
	return 
end
ElementAccessCameraTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementAccessCameraTrigger.super.on_executed(self, instigator)

	return 
end

return 
