core:import("CoreMissionScriptElement")

ElementLaserTrigger = ElementLaserTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementLaserTrigger.COLORS = {
	red = {
		1,
		0,
		0
	},
	green = {
		0,
		1,
		0
	},
	blue = {
		0,
		0,
		1
	}
}
ElementLaserTrigger.init = function (self, ...)
	ElementLaserTrigger.super.init(self, ...)

	self._brush = Draw:brush(Color(0.15, unpack(self.COLORS[self._values.color])))

	self._brush:set_blend_mode("opacity_add")

	self._last_project_amount_all = 0
	self._inside = {}
	self._connections = {}
	self._is_cycled = self._values.cycle_interval ~= 0
	self._next_cycle_t = 0
	self._cycle_index = 1
	self._cycle_order = {}
	self._slotmask = managers.slot:get_mask("persons")

	if self._values.instigator == "enemies" then
		self._slotmask = managers.slot:get_mask("enemies")
	elseif self._values.instigator == "civilians" then
		self._slotmask = managers.slot:get_mask("civilians")
	elseif self._values.instigator == "loot" then
		self._slotmask = World:make_slot_mask(14)
	end

	if not self._values.skip_dummies then
		self._dummy_units = {}
		self._dummies_visible = true

		for _, point in pairs(self._values.points) do
			local unit = safe_spawn_unit(Idstring("units/payday2/props/gen_prop_lazer_blaster_dome/gen_prop_lazer_blaster_dome"), point.pos, point.rot)
			local materials = unit.get_objects_by_type(unit, Idstring("material"))

			for _, m in ipairs(materials) do
				m.set_variable(m, Idstring("contour_opacity"), 0)
			end

			table.insert(self._dummy_units, unit)

			point.pos = point.pos + point.rot:y()*3
		end
	end

	for i, connection in ipairs(self._values.connections) do
		table.insert(self._cycle_order, i)
		table.insert(self._connections, {
			enabled = not self._is_cycled,
			from = self._values.points[connection.from],
			to = self._values.points[connection.to]
		})
	end

	if self._values.cycle_random then
		local cycle_order = clone(self._cycle_order)
		self._cycle_order = {}

		while 0 < #cycle_order do
			table.insert(self._cycle_order, table.remove(cycle_order, math.random(#cycle_order)))
		end
	end

	return 
end
ElementLaserTrigger.on_script_activated = function (self, ...)
	ElementLaserTrigger.super.on_script_activated(self, ...)
	self._mission_script:add_save_state_cb(self._id)

	if self._values.enabled then
		self.add_callback(self)
	end

	return 
end
ElementLaserTrigger.set_enabled = function (self, enabled)
	ElementLaserTrigger.super.set_enabled(self, enabled)

	if enabled then
		self._delayed_remove = nil

		self.add_callback(self)
	else
		self.remove_callback(self)
	end

	return 
end
ElementLaserTrigger.add_callback = function (self)
	if not self._callback then
		if not self._values.visual_only then
			self._callback = self._mission_script:add(callback(self, self, "update_laser"), self._values.interval)
		end

		self._mission_script:add_updator(self._id, callback(self, self, "update_laser_draw"))
	end

	return 
end
ElementLaserTrigger.remove_callback = function (self)
	if self._callback then
		self._mission_script:remove(self._callback)

		self._callback = nil
	end

	if self._values.flicker_remove then
		self._delayed_remove = 7
		self._delayed_remove_t = 0
		self._delayed_remove_state_on = true
	else
		self._mission_script:remove_updator(self._id)
	end

	return 
end
ElementLaserTrigger.client_on_executed = function (self, ...)
	return 
end
ElementLaserTrigger.on_executed = function (self, instigator, alternative)
	if not self._values.enabled then
		return 
	end

	ElementLaserTrigger.super.on_executed(self, instigator, alternative)

	if not self._values.enabled then
		self.remove_callback(self)
	end

	return 
end
ElementLaserTrigger.instigators = function (self)
	return ElementAreaTrigger.project_instigators(self)
end
ElementLaserTrigger._check_delayed_remove = function (self, t, dt)
	if not self._delayed_remove then
		return false
	end

	if self._delayed_remove_t <= 0 then
		self._delayed_remove_state_on = not self._delayed_remove_state_on
		self._delayed_remove_t = math.rand(0.05) + 0.05
		self._delayed_remove = self._delayed_remove - 1

		if self._delayed_remove <= 0 then
			self._mission_script:remove_updator(self._id)
		end
	end

	self._delayed_remove_t = self._delayed_remove_t - dt

	if not self._delayed_remove_state_on then
		return true
	end

	return false
end
ElementLaserTrigger.update_laser_draw = function (self, t, dt)
	if #self._connections == 0 then
		return 
	end

	if self._check_delayed_remove(self, t, dt) then
		return 
	end

	for _, connection in ipairs(self._connections) do
		if connection.enabled then
			self._brush:cylinder(connection.from.pos, connection.to.pos, 0.5)
		end
	end

	if self._is_cycled then
		self._next_cycle_t = self._next_cycle_t - dt

		if self._next_cycle_t <= 0 then
			self._next_cycle_t = self._values.cycle_interval

			for i, connection in ipairs(self._connections) do
				connection.enabled = false
			end

			local index = self._cycle_index - 1

			for j = 1, self._values.cycle_active_amount, 1 do
				index = index + 1

				if #self._cycle_order < index then
					index = 1
				end

				self._connections[self._cycle_order[index]].enabled = true
			end

			self._cycle_index = ((self._values.cycle_type == "pop" and index) or self._cycle_index) + 1

			if #self._cycle_order < self._cycle_index then
				self._cycle_index = 1
			end
		end
	end

	return 
end
ElementLaserTrigger.project_amount_all = function (self)
	return ElementAreaTrigger.project_amount_all(self)
end
ElementLaserTrigger.update_laser = function (self)
	if not self._values.enabled then
		return 
	end

	local instigators = self.instigators(self)

	if #instigators == 0 and Network:is_server() then
		self._check_state(self, nil)
	else
		for _, unit in ipairs(instigators) do
			if alive(unit) then
				if Network:is_client() then
					self._client_check_state(self, unit)
				else
					self._check_state(self, unit)
				end
			end
		end
	end

	return 
end
ElementLaserTrigger.sync_enter_area = function (self, unit)
	self._add_inside(self, unit)

	return 
end
ElementLaserTrigger.sync_exit_area = function (self, unit)
	self._remove_inside(self, unit)

	return 
end
ElementLaserTrigger.sync_while_in_area = function (self, unit)
	self._while_inside(self, unit)

	return 
end
ElementLaserTrigger._check_state = function (self, unit)
	if alive(unit) then
		local rule_ok = self._check_instigator_rules(self, unit)
		local inside = nil
		local mover = unit.mover(unit)

		if mover then
			for i, connection in ipairs(self._connections) do
				if connection.enabled then
					inside = mover.line_intersection(mover, connection.from.pos, connection.to.pos)

					if inside then
						break
					end
				end
			end
		else
			local oobb = unit.oobb(unit)

			for i, connection in ipairs(self._connections) do
				if connection.enabled then
					local hit_oobb = oobb.raycast(oobb, connection.from.pos, connection.to.pos)

					if hit_oobb then
						inside = World:raycast("ray", connection.from.pos, connection.to.pos, "target_unit", unit, "slot_mask", self._slotmask, "report")
					end

					if inside then
						break
					end
				end
			end
		end

		if table.contains(self._inside, unit) then
			if not inside or not rule_ok then
				self._remove_inside(self, unit)
			end
		elseif inside and rule_ok then
			self._add_inside(self, unit)
		end

		if inside and rule_ok then
			self._while_inside(self, unit)
		end
	end

	local project_amount_all = self.project_amount_all(self)

	if project_amount_all ~= self._last_project_amount_all then
		self._last_project_amount_all = project_amount_all

		self._clean_destroyed_units(self)

		return true
	end

	return false
end
ElementLaserTrigger._add_inside = function (self, unit)
	table.insert(self._inside, unit)
	self.on_executed(self, unit, "enter")

	return 
end
ElementLaserTrigger._while_inside = function (self, unit)
	self.on_executed(self, unit, "while_inside")

	return 
end
ElementLaserTrigger._remove_inside = function (self, unit)
	table.delete(self._inside, unit)
	self.on_executed(self, unit, "leave")

	if #self._inside == 0 then
		self.on_executed(self, unit, "empty")
	end

	return 
end
ElementLaserTrigger._remove_inside_by_index = function (self, index)
	table.remove(self._inside, index)
	self.on_executed(self, nil, "leave")

	if #self._inside == 0 then
		self.on_executed(self, nil, "empty")
	end

	return 
end
ElementLaserTrigger._check_instigator_rules = function (self, unit)
	if not self._rules_elements then
		return true
	end

	for _, element in ipairs(self._rules_elements) do
		if not element.check_rules(element, self._values.instigator, unit) then
			return false
		end
	end

	return true
end
ElementLaserTrigger._clean_destroyed_units = function (self)
	local i = 1

	while next(self._inside) and i <= #self._inside do
		if alive(self._inside[i]) then
			i = i + 1
		else
			self._remove_inside_by_index(self, i)
		end
	end

	return 
end
ElementLaserTrigger._client_check_state = function (self, unit)
	local rule_ok = self._check_instigator_rules(self, unit)
	local inside = nil
	local mover = unit.mover(unit)

	if mover then
		for i, connection in ipairs(self._connections) do
			if connection.enabled then
				inside = mover.line_intersection(mover, connection.from.pos, connection.to.pos)

				if inside then
					break
				end
			end
		end
	end

	if table.contains(self._inside, unit) then
		if not inside or not rule_ok then
			table.delete(self._inside, unit)
			managers.network:session():send_to_host("to_server_area_event", 2, self._id, unit)
		end
	elseif inside and rule_ok then
		table.insert(self._inside, unit)
		managers.network:session():send_to_host("to_server_area_event", 1, self._id, unit)
	end

	if inside and rule_ok then
		managers.network:session():send_to_host("to_server_area_event", 3, self._id, unit)
	end

	return 
end
ElementLaserTrigger.operation_add = function (self)
	self._set_dummies_visible(self, true)

	return 
end
ElementLaserTrigger.operation_remove = function (self)
	self._set_dummies_visible(self, false)

	return 
end
ElementLaserTrigger._set_dummies_visible = function (self, visible)
	if not self._dummy_units then
		return 
	end

	self._dummies_visible = visible

	for _, unit in ipairs(self._dummy_units) do
		unit.set_enabled(unit, self._dummies_visible)
	end

	return 
end
ElementLaserTrigger.save = function (self, data)
	data.enabled = self._values.enabled
	data.cycle_order = self._cycle_order
	data.cycle_index = self._cycle_index
	data.next_cycle_t = self._next_cycle_t
	data.dummies_visible = self._dummies_visible

	return 
end
ElementLaserTrigger.load = function (self, data)
	self.set_enabled(self, data.enabled)

	self._cycle_order = data.cycle_order
	self._cycle_index = data.cycle_index
	self._next_cycle_t = data.next_cycle_t

	self._set_dummies_visible(self, data.dummies_visible)

	return 
end

return 
