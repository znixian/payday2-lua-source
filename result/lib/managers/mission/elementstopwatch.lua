core:import("CoreMissionScriptElement")

ElementStopwatch = ElementStopwatch or class(CoreMissionScriptElement.MissionScriptElement)
ElementStopwatch.init = function (self, ...)
	ElementStopwatch.super.init(self, ...)

	self._digital_gui_units = {}
	self._triggers = {}

	return 
end
ElementStopwatch.on_script_activated = function (self)
	self._timer = 0

	if not Network:is_server() then
		return 
	end

	if self._values.digital_gui_unit_ids then
		for _, id in ipairs(self._values.digital_gui_unit_ids) do
			if Global.running_simulation then
				local unit = managers.editor:unit_with_id(id)

				table.insert(self._digital_gui_units, unit)
				unit.digital_gui(unit):timer_set(self._timer)
			else
				local unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "_load_unit"))

				if unit then
					table.insert(self._digital_gui_units, unit)
					unit.digital_gui(unit):timer_set(self._timer)
				end
			end
		end
	end

	return 
end
ElementStopwatch._load_unit = function (self, unit)
	table.insert(self._digital_gui_units, unit)
	unit.digital_gui(unit):timer_set(self._timer)

	return 
end
ElementStopwatch.set_enabled = function (self, enabled)
	ElementStopwatch.super.set_enabled(self, enabled)

	return 
end
ElementStopwatch.add_updator = function (self)
	if not Network:is_server() then
		return 
	end

	if not self._updator then
		self._updator = true

		self._mission_script:add_updator(self._id, callback(self, self, "update_timer"))
	end

	return 
end
ElementStopwatch.remove_updator = function (self)
	if self._updator then
		self._mission_script:remove_updator(self._id)

		self._updator = nil
	end

	return 
end
ElementStopwatch.update_timer = function (self, t, dt)
	self._timer = self._timer + dt

	for id, cb_data in pairs(self._triggers) do
		if cb_data.time <= self._timer and not cb_data.disabled then
			cb_data.callback()
			self.remove_trigger(self, id)
		end
	end

	return 
end
ElementStopwatch.client_on_executed = function (self, ...)
	return 
end
ElementStopwatch.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementStopwatch.super.on_executed(self, instigator)

	return 
end
ElementStopwatch.get_time = function (self)
	return self._timer
end
ElementStopwatch.stopwatch_operation_pause = function (self)
	self.remove_updator(self)
	self._pause_digital_guis(self)

	return 
end
ElementStopwatch.stopwatch_operation_start = function (self)
	self._update_digital_guis_timer(self)
	self.add_updator(self)
	self._start_digital_guis_count_up(self)

	return 
end
ElementStopwatch.stopwatch_operation_add_time = function (self, time)
	self._timer = self._timer + time

	self._update_digital_guis_timer(self)

	return 
end
ElementStopwatch.stopwatch_operation_subtract_time = function (self, time)
	self._timer = self._timer - time

	self._update_digital_guis_timer(self)

	return 
end
ElementStopwatch.stopwatch_operation_reset = function (self)
	self._timer = 0

	self._update_digital_guis_timer(self)

	return 
end
ElementStopwatch.stopwatch_operation_set_time = function (self, time)
	self._timer = time

	self._update_digital_guis_timer(self)

	return 
end
ElementStopwatch._update_digital_guis_timer = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):timer_set(self._timer, true)
		end
	end

	return 
end
ElementStopwatch._start_digital_guis_count_up = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):timer_start_count_up(true)
		end
	end

	return 
end
ElementStopwatch._pause_digital_guis = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):timer_pause(true)
		end
	end

	return 
end
ElementStopwatch.add_trigger = function (self, id, time, callback, disabled)
	self._triggers[id] = {
		time = time,
		callback = callback,
		disabled = disabled
	}

	return 
end
ElementStopwatch.remove_trigger = function (self, id)
	if not self._triggers[id].disabled then
		self._triggers[id] = nil
	end

	return 
end
ElementStopwatch.enable_trigger = function (self, id)
	if self._triggers[id] then
		self._triggers[id].disabled = false
	end

	return 
end
ElementStopwatchOperator = ElementStopwatchOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementStopwatchOperator.init = function (self, ...)
	ElementStopwatchOperator.super.init(self, ...)

	return 
end
ElementStopwatchOperator.client_on_executed = function (self, ...)
	return 
end
ElementStopwatchOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local time = self.get_random_table_value_float(self, self.value(self, "time"))

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self._values.operation == "pause" then
				element.stopwatch_operation_pause(element)
			elseif self._values.operation == "start" then
				element.stopwatch_operation_start(element)
			elseif self._values.operation == "add_time" then
				element.stopwatch_operation_add_time(element, time)
			elseif self._values.operation == "subtract_time" then
				element.stopwatch_operation_subtract_time(element, time)
			elseif self._values.operation == "reset" then
				element.stopwatch_operation_reset(element)
			elseif self._values.operation == "set_time" then
				element.stopwatch_operation_set_time(element, time)
			elseif self._values.operation == "save_time" then
				local time = element.get_time(element) or 0
				local saved_time = managers.mission:get_saved_job_value(self.value(self, "save_key"))

				print("[stopwatch] saving stopwatch time", self.value(self, "save_key"), time, saved_time, self._save_value_ok(self, time, saved_time))

				if self._save_value_ok(self, time, saved_time) then
					Global.mission_manager.saved_job_values[self.value(self, "save_key")] = time
				end
			elseif self._values.operation == "load_time" then
				print("[stopwatch] loading stopwatch time", self.value(self, "save_key"), Global.mission_manager.saved_job_values[self.value(self, "save_key")])

				local saved_time = Global.mission_manager.saved_job_values[self.value(self, "save_key")]

				if saved_time ~= nil then
					if type(saved_time) ~= "number" then
						saved_time = tonumber(saved_time)
					end

					if saved_time ~= nil then
						element.stopwatch_operation_set_time(element, saved_time)
					end
				end
			end
		end
	end

	ElementStopwatchOperator.super.on_executed(self, instigator)

	return 
end
ElementStopwatchOperator._save_value_ok = function (self, new_time, saved_time)
	local condition = self.value(self, "condition")

	if condition == "always" or saved_time == nil then
		return true
	elseif condition == "equal" then
		return new_time == saved_time
	elseif condition == "less_than" then
		return new_time < saved_time
	elseif condition == "greater_than" then
		return saved_time < new_time
	elseif condition == "less_or_equal" then
		return new_time <= saved_time
	elseif condition == "greater_or_equal" then
		return saved_time <= new_time
	end

	return false
end
ElementStopwatchTrigger = ElementStopwatchTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementStopwatchTrigger.init = function (self, ...)
	ElementStopwatchTrigger.super.init(self, ...)

	return 
end
ElementStopwatchTrigger.on_script_activated = function (self)
	self.activate_trigger(self)

	return 
end
ElementStopwatchTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementStopwatchTrigger.super.on_executed(self, instigator)

	return 
end
ElementStopwatchTrigger.client_on_executed = function (self, ...)
	return 
end
ElementStopwatchTrigger.activate_trigger = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			element.add_trigger(element, self._id, self._values.time, callback(self, self, "on_executed"), not self.enabled(self))
		end
	end

	return 
end
ElementStopwatchTrigger.operation_add = function (self)
	return 
end
ElementStopwatchTrigger.set_enabled = function (self, enabled)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			element.enable_trigger(element, self._id)
		end
	end

	ElementStopwatchTrigger.super.set_enabled(self, enabled)

	return 
end
ElementStopwatchFilter = ElementStopwatchFilter or class(CoreMissionScriptElement.MissionScriptElement)
ElementStopwatchFilter.init = function (self, ...)
	ElementStopwatchFilter.super.init(self, ...)

	return 
end
ElementStopwatchFilter.on_script_activated = function (self)
	return 
end
ElementStopwatchFilter.client_on_executed = function (self, ...)
	return 
end
ElementStopwatchFilter.on_executed = function (self, instigator)
	if self._values.enabled and self._values_ok(self) then
		ElementStopwatchFilter.super.on_executed(self, instigator)
	end

	return 
end
ElementStopwatchFilter._values_ok = function (self)
	if self._values.needed_to_execute == "all" then
		return self._all_elements_ok(self)
	end

	if self._values.needed_to_execute == "any" then
		return self._any_elements_ok(self)
	end

	return 
end
ElementStopwatchFilter._all_elements_ok = function (self)
	for _, id in ipairs(self._values.elements) do
		if not self._check_time(self, self.get_mission_element(self, id), self._get_time(self)) then
			return false
		end
	end

	return true
end
ElementStopwatchFilter._any_elements_ok = function (self)
	for _, id in ipairs(self._values.elements) do
		if self._check_time(self, self.get_mission_element(self, id), self._get_time(self)) then
			return true
		end
	end

	return false
end
ElementStopwatchFilter._get_time = function (self)
	local time = self._values.value

	if self._values.stopwatch_value_ids and 0 < #self._values.stopwatch_value_ids then
		local element = self.get_mission_element(self, self._values.stopwatch_value_ids[1])

		if element then
			time = element.get_time(element)
		end
	end

	return time
end
ElementStopwatchFilter._check_time = function (self, element, value)
	if not self._values.check_type or self._values.check_type == "equal" then
		return element.get_time(element) == value
	elseif self._values.check_type == "less_or_equal" then
		return element.get_time(element) <= value
	elseif self._values.check_type == "greater_or_equal" then
		return value <= element.get_time(element)
	elseif self._values.check_type == "less_than" then
		return element.get_time(element) < value
	elseif self._values.check_type == "greater_than" then
		return value < element.get_time(element)
	end

	return false
end

return 
