core:import("CoreMissionScriptElement")

ElementSpawnCivilianGroup = ElementSpawnCivilianGroup or class(CoreMissionScriptElement.MissionScriptElement)
ElementSpawnCivilianGroup.init = function (self, ...)
	ElementSpawnCivilianGroup.super.init(self, ...)

	self._group_data = {
		amount = self._values.amount,
		random = self._values.random,
		ignore_disabled = self._values.ignore_disabled,
		spawn_points = {}
	}
	self._unused_randoms = {}

	self._finalize_values(self)

	return 
end
ElementSpawnCivilianGroup._finalize_values = function (self)
	local values = self._values

	if values.team == "default" then
		values.team = nil
	end

	return 
end
ElementSpawnCivilianGroup.on_script_activated = function (self)
	for i, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		table.insert(self._unused_randoms, i)
		table.insert(self._group_data.spawn_points, element)
	end

	return 
end
ElementSpawnCivilianGroup.add_event_callback = function (self, name, callback)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_event_callback(element, name, callback)
	end

	return 
end
ElementSpawnCivilianGroup._check_spawn_points = function (self)
	self._spawn_points = {}

	if not self._group_data.ignore_disabled then
		self._spawn_points = self._group_data.spawn_points

		return 
	end

	self._unused_randoms = {}
	local i = 1

	for _, element in pairs(self._group_data.spawn_points) do
		if element.enabled(element) then
			table.insert(self._unused_randoms, i)
			table.insert(self._spawn_points, element)

			i = i + 1
		end
	end

	return 
end
ElementSpawnCivilianGroup.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self._check_spawn_points(self)

	if 0 < #self._spawn_points then
		for i = 1, self._group_data.amount, 1 do
			local element = self._spawn_points[self._get_spawn_point(self, i)]

			element.produce(element, self._values.team and {
				team = self._values.team
			})
		end
	end

	ElementSpawnCivilianGroup.super.on_executed(self, instigator)

	return 
end
ElementSpawnCivilianGroup._get_spawn_point = function (self, i)
	if self._group_data.random then
		if #self._unused_randoms == 0 then
			for i = 1, #self._spawn_points, 1 do
				table.insert(self._unused_randoms, i)
			end
		end

		local rand = math.random(#self._unused_randoms)

		return table.remove(self._unused_randoms, rand)
	end

	return math.mod(i, #self._spawn_points) + 1
end
ElementSpawnCivilianGroup.unspawn_all_units = function (self)
	ElementSpawnEnemyGroup.unspawn_all_units(self)

	return 
end
ElementSpawnCivilianGroup.kill_all_units = function (self)
	ElementSpawnEnemyGroup.kill_all_units(self)

	return 
end
ElementSpawnCivilianGroup.execute_on_all_units = function (self, func)
	ElementSpawnEnemyGroup.execute_on_all_units(self, func)

	return 
end
ElementSpawnCivilianGroup.units = function (self)
	local all_units = {}

	for _, element in ipairs(self._group_data.spawn_points) do
		local element_units = element.units(element)

		for _, unit in ipairs(element_units) do
			table.insert(all_units, unit)
		end
	end

	return all_units
end

return 
