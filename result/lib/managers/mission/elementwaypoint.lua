core:import("CoreMissionScriptElement")

ElementWaypoint = ElementWaypoint or class(CoreMissionScriptElement.MissionScriptElement)
ElementWaypoint.init = function (self, ...)
	ElementWaypoint.super.init(self, ...)

	self._network_execute = true

	if self._values.icon == "guis/textures/waypoint2" or self._values.icon == "guis/textures/waypoint" then
		self._values.icon = "wp_standard"
	end

	return 
end
ElementWaypoint.on_script_activated = function (self)
	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementWaypoint.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementWaypoint.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.only_on_instigator and instigator ~= managers.player:player_unit() then
		ElementWaypoint.super.on_executed(self, instigator)

		return 
	end

	if not self._values.only_in_civilian or managers.player:current_state() == "civilian" then
		local text = managers.localization:text(self._values.text_id)

		managers.hud:add_waypoint(self._id, {
			distance = true,
			state = "sneak_present",
			present_timer = 0,
			text = text,
			icon = self._values.icon,
			position = self._values.position
		})
	elseif managers.hud:get_waypoint_data(self._id) then
		managers.hud:remove_waypoint(self._id)
	end

	ElementWaypoint.super.on_executed(self, instigator)

	return 
end
ElementWaypoint.operation_remove = function (self)
	managers.hud:remove_waypoint(self._id)

	return 
end
ElementWaypoint.save = function (self, data)
	data.enabled = self._values.enabled

	return 
end
ElementWaypoint.load = function (self, data)
	self.set_enabled(self, data.enabled)

	return 
end

return 
