core:import("CoreMissionScriptElement")

ElementFlashlight = ElementFlashlight or class(CoreMissionScriptElement.MissionScriptElement)
ElementFlashlight.init = function (self, ...)
	ElementFlashlight.super.init(self, ...)

	return 
end
ElementFlashlight.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementFlashlight.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.on_player then
		managers.game_play_central:set_flashlights_on_player_on(self._values.state)
	end

	managers.game_play_central:set_flashlights_on(self._values.state)
	ElementFlashlight.super.on_executed(self, instigator)

	return 
end

return 
