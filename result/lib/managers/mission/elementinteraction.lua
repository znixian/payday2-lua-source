core:import("CoreMissionScriptElement")

ElementInteraction = ElementInteraction or class(CoreMissionScriptElement.MissionScriptElement)
ElementInteraction.init = function (self, ...)
	ElementInteraction.super.init(self, ...)

	if Network:is_server() then
		local host_only = self.value(self, "host_only")

		if host_only then
			self._unit = CoreUnit.safe_spawn_unit("units/dev_tools/mission_elements/point_interaction/interaction_dummy_nosync", self._values.position, self._values.rotation)
		else
			self._unit = CoreUnit.safe_spawn_unit("units/dev_tools/mission_elements/point_interaction/interaction_dummy", self._values.position, self._values.rotation)
		end

		if self._unit then
			self._unit:interaction():set_host_only(host_only)
			self._unit:interaction():set_active(false)
			self._unit:interaction():set_mission_element(self)
			self._unit:interaction():set_tweak_data(self._values.tweak_data_id)

			if self._values.override_timer ~= -1 then
				self._unit:interaction():set_override_timer_value(self._values.override_timer)
			end
		end
	end

	return 
end
ElementInteraction.on_script_activated = function (self)
	if alive(self._unit) and self._values.enabled then
		self._unit:interaction():set_active(self._values.enabled, true)
	end

	return 
end
ElementInteraction.set_enabled = function (self, enabled)
	ElementInteraction.super.set_enabled(self, enabled)

	if alive(self._unit) then
		self._unit:interaction():set_active(enabled, true)
	end

	return 
end
ElementInteraction.on_executed = function (self, instigator, ...)
	if not self._values.enabled then
		return 
	end

	ElementInteraction.super.on_executed(self, instigator, ...)

	return 
end
ElementInteraction.on_interacted = function (self, instigator)
	self.on_executed(self, instigator, "interacted")

	return 
end
ElementInteraction.on_interact_start = function (self, instigator)
	self.on_executed(self, instigator, "start")

	return 
end
ElementInteraction.on_interact_interupt = function (self, instigator)
	self.on_executed(self, instigator, "interupt")

	return 
end
ElementInteraction.stop_simulation = function (self, ...)
	ElementInteraction.super.stop_simulation(self, ...)

	if alive(self._unit) then
		World:delete_unit(self._unit)
	end

	return 
end

return 
