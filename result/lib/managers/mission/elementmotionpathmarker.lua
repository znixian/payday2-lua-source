core:import("CoreMissionScriptElement")

ElementMotionpathMarker = ElementMotionpathMarker or class(CoreMissionScriptElement.MissionScriptElement)
ElementMotionpathMarker.init = function (self, ...)
	ElementMotionpathMarker.super.init(self, ...)

	self._network_execute = true

	if self._values.icon == "guis/textures/VehicleMarker" then
		self._values.icon = "wp_standard"
	end

	return 
end
ElementMotionpathMarker.on_script_activated = function (self)
	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementMotionpathMarker.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementMotionpathMarker.on_executed = function (self, instigator)
	ElementMotionpathMarker.super.on_executed(self, instigator)

	return 
end
ElementMotionpathMarker.operation_remove = function (self)
	return 
end
ElementMotionpathMarker.save = function (self, data)
	data.enabled = self._values.enabled
	data.motion_state = self._values.motion_state

	return 
end
ElementMotionpathMarker.load = function (self, data)
	self.set_enabled(self, data.enabled)

	self._values.motion_state = data.motion_state

	return 
end
ElementMotionpathMarker.add_trigger = function (self, trigger_id, outcome, callback)
	managers.motion_path:add_trigger(self._id, self._values.path_id, trigger_id, outcome, callback)

	return 
end
ElementMotionpathMarker.motion_operation_goto_marker = function (self, checkpoint_marker_id, goto_marker_id)
	managers.motion_path:operation_goto_marker(checkpoint_marker_id, goto_marker_id)

	return 
end
ElementMotionpathMarker.motion_operation_teleport_to_marker = function (self, checkpoint_marker_id, teleport_to_marker_id)
	managers.motion_path:operation_teleport_to_marker(checkpoint_marker_id, teleport_to_marker_id)

	return 
end
ElementMotionpathMarker.motion_operation_set_motion_state = function (self, state)
	self._values.motion_state = state

	return 
end
ElementMotionpathMarker.motion_operation_set_rotation = function (self, operator_id)
	managers.motion_path:operation_set_unit_target_rotation(self._id, operator_id)

	return 
end

return 
