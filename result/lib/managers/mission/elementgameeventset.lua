core:import("CoreMissionScriptElement")

ElementGameEventSet = ElementGameEventSet or class(CoreMissionScriptElement.MissionScriptElement)
ElementGameEventSet.init = function (self, ...)
	ElementGameEventSet.super.init(self, ...)

	return 
end
ElementGameEventSet.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementGameEventSet.super.on_executed(self, instigator)

	return 
end
ElementGameEventSet.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end

return 
