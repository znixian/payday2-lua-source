core:import("CoreMissionScriptElement")

ElementGameDirection = ElementGameDirection or class(CoreMissionScriptElement.MissionScriptElement)
ElementGameDirection.init = function (self, ...)
	ElementGameDirection.super.init(self, ...)

	return 
end
ElementGameDirection.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_mission_fwd_vector(self._values.rotation:y(), self._values.position)
	ElementGameDirection.super.on_executed(self, instigator)

	return 
end

return 
