core:import("CoreMissionScriptElement")

ElementLootBag = ElementLootBag or class(CoreMissionScriptElement.MissionScriptElement)
ElementLootBag.init = function (self, ...)
	ElementLootBag.super.init(self, ...)

	self._triggers = {}

	return 
end
ElementLootBag.client_on_executed = function (self, ...)
	return 
end
ElementLootBag.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local unit = nil
	local pos, rot = self.get_orientation(self)

	if self._values.carry_id ~= "none" then
		local dir = (self._values.push_multiplier and self._values.spawn_dir*self._values.push_multiplier) or Vector3(0, 0, 0)
		unit = managers.player:server_drop_carry(self._values.carry_id, 1, true, false, 1, pos, rot, dir, 0, nil, nil)
	elseif self._values.from_respawn then
		local loot = managers.loot:get_respawn()

		if loot then
			local dir = (self._values.push_multiplier and self._values.spawn_dir*self._values.push_multiplier) or Vector3(0, 0, 0)
			unit = managers.player:server_drop_carry(loot.carry_id, loot.multiplier, true, false, 1, pos, rot, dir, 0, nil, nil)
		else
			print("NO MORE LOOT TO RESPAWN")
		end
	else
		local loot = managers.loot:get_distribute()

		if loot then
			local dir = (self._values.push_multiplier and self._values.spawn_dir*self._values.push_multiplier) or Vector3(0, 0, 0)
			unit = managers.player:server_drop_carry(loot.carry_id, loot.multiplier, true, false, 1, pos, rot, dir, 0, nil, nil)
		else
			print("NO MORE LOOT TO DISTRIBUTE")
		end
	end

	if alive(unit) then
		self._check_triggers(self, "spawn", unit)
		unit.carry_data(unit):set_mission_element(self)
	end

	ElementLootBag.super.on_executed(self, instigator)

	return 
end
ElementLootBag.add_trigger = function (self, id, type, callback)
	self._triggers[type] = self._triggers[type] or {}
	self._triggers[type][id] = {
		callback = callback
	}

	return 
end
ElementLootBag._check_triggers = function (self, type, instigator)
	if not self._triggers[type] then
		return 
	end

	for id, cb_data in pairs(self._triggers[type]) do
		cb_data.callback(instigator)
	end

	return 
end
ElementLootBag.trigger = function (self, type, instigator)
	self._check_triggers(self, type, instigator)

	return 
end
ElementLootBagTrigger = ElementLootBagTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementLootBagTrigger.init = function (self, ...)
	ElementLootBagTrigger.super.init(self, ...)

	return 
end
ElementLootBagTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_trigger(element, self._id, self._values.trigger_type, callback(self, self, "on_executed"))
	end

	return 
end
ElementLootBagTrigger.client_on_executed = function (self, ...)
	return 
end
ElementLootBagTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementLootBagTrigger.super.on_executed(self, instigator)

	return 
end

return 
