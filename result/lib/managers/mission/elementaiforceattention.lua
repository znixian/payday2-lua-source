core:import("CoreMissionScriptElement")

ElementAIForceAttention = ElementAIForceAttention or class(CoreMissionScriptElement.MissionScriptElement)
ElementAIForceAttention.init = function (self, ...)
	ElementAIForceAttention.super.init(self, ...)

	return 
end
ElementAIForceAttention._get_unit = function (self, unit_id)
	if Global.running_simulation then
		return managers.editor:unit_with_id(unit_id)
	else
		return managers.worlddefinition:get_unit(unit_id)
	end

	return 
end
ElementAIForceAttention.on_executed = function (self, instigator)
	if not self._values.enabled or Network:is_client() then
		return 
	end

	if self._values.is_spawn then
		local spawn_element = self.get_mission_element(self, self._values.att_unit_id)

		spawn_element.add_event_callback(spawn_element, "spawn", function (unit)
			self:_register_force_attention_unit(unit)

			return 
		end)

		if spawn_element.units(spawn_element) then
			self._register_force_attention_unit(self, spawn_element.units(spawn_element)[#spawn_element.units(spawn_element)])
		end
	else
		self._register_force_attention_unit(self, self._get_unit(self, self._values.att_unit_id))
	end

	for _, included_spawn_id in ipairs(self._values.included_units) do
		local spawn_element = self.get_mission_element(self, included_spawn_id)

		if not spawn_element then
			return 
		end

		spawn_element.add_event_callback(spawn_element, "spawn", function (unit)
			managers.groupai:state():add_affected_force_attention_unit(unit)

			return 
		end)

		slot8 = pairs
		slot9 = spawn_element.units(spawn_element) or {}

		for _, unit in slot8(slot9) do
			managers.groupai:state():add_affected_force_attention_unit(unit)
		end
	end

	for _, excluded_spawn_id in ipairs(self._values.excluded_units) do
		local spawn_element = self.get_mission_element(self, excluded_spawn_id)

		if not spawn_element then
			return 
		end

		spawn_element.add_event_callback(spawn_element, "spawn", function (unit)
			managers.groupai:state():add_excluded_force_attention_unit(unit)

			return 
		end)

		slot8 = pairs
		slot9 = spawn_element.units(spawn_element) or {}

		for _, unit in slot8(slot9) do
			managers.groupai:state():add_excluded_force_attention_unit(unit)
		end
	end

	ElementSpecialObjective.super.on_executed(self, instigator)

	return 
end
ElementAIForceAttention._register_force_attention_unit = function (self, unit)
	managers.groupai:state():set_force_attention({
		unit = unit,
		ignore_vis_blockers = self._values.ignore_vis_blockers,
		include_all_force_spawns = self._values.include_all_force_spawns
	})

	return 
end

return 
