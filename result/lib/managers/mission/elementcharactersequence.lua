core:import("CoreMissionScriptElement")

ElementCharacterSequence = ElementCharacterSequence or class(CoreMissionScriptElement.MissionScriptElement)
ElementCharacterSequence.init = function (self, ...)
	ElementCharacterSequence.super.init(self, ...)

	return 
end
ElementCharacterSequence.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local function f(unit)
		unit.damage(unit):run_sequence_simple(self._values.sequence)

		if Network:is_server() and managers.network:session() then
			managers.network:session():send_to_peers_synched("sync_run_sequence_char", unit, self._values.sequence)
		end

		return 
	end

	if self._values.sequence == "" then
		debug_pause("[ElementCharacterSequence] Empty sequence name. Element ID:", self._id)

		return 
	end

	if self._values.use_instigator then
		if alive(instigator) then
			if instigator.damage(instigator) then
				f(instigator)
			else
				debug_pause_unit(unit, "[ElementCharacterSequence] instigator without damage extension", unit, ". Element ID:", self._id)
			end
		end
	else
		for _, id in ipairs(self._values.elements) do
			local element = self.get_mission_element(self, id)

			element.execute_on_all_units(element, f)
		end
	end

	ElementCharacterSequence.super.on_executed(self, instigator)

	return 
end

return 
