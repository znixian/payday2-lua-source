core:import("CoreMissionScriptElement")

ElementAssetTrigger = ElementAssetTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementAssetTrigger.init = function (self, ...)
	ElementAssetTrigger.super.init(self, ...)

	return 
end
ElementAssetTrigger.client_on_executed = function (self, ...)
	return 
end
ElementAssetTrigger.on_script_activated = function (self)
	managers.assets:add_trigger(self._id, "asset", self._values.id, callback(self, self, "on_executed"))

	return 
end
ElementAssetTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementAssetTrigger.super.on_executed(self, instigator)

	return 
end

return 
