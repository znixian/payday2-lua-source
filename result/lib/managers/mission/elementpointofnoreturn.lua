core:import("CoreMissionScriptElement")

ElementPointOfNoReturn = ElementPointOfNoReturn or class(CoreMissionScriptElement.MissionScriptElement)
ElementPointOfNoReturn.init = function (self, ...)
	ElementPointOfNoReturn.super.init(self, ...)

	return 
end
ElementPointOfNoReturn.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementPointOfNoReturn.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local diff = (Global.game_settings and Global.game_settings.difficulty) or "hard"

	if diff == "easy" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_easy, self._id)
	elseif diff == "normal" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_normal, self._id)
	elseif diff == "hard" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_hard, self._id)
	elseif diff == "overkill" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_overkill, self._id)
	elseif diff == "overkill_145" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_overkill_145, self._id)
	elseif diff == "easy_wish" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_easy_wish, self._id)
	elseif diff == "overkill_290" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_overkill_290, self._id)
	elseif diff == "sm_wish" then
		managers.groupai:state():set_point_of_no_return_timer(self._values.time_sm_wish, self._id)
	end

	ElementPointOfNoReturn.super.on_executed(self, instigator)

	return 
end

return 
