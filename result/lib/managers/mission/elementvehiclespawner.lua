core:import("CoreMissionScriptElement")

ElementVehicleSpawner = ElementVehicleSpawner or class(CoreMissionScriptElement.MissionScriptElement)
ElementVehicleSpawner.init = function (self, ...)
	Application:trace("ElementVehicleSpawner:init")
	ElementVehicleSpawner.super.init(self, ...)

	self._vehicle_units = {}

	Application:trace("ElementVehicleSpawner:init")
	ElementVehicleSpawner.super.init(self, ...)

	self._vehicles = {
		muscle = "units/pd2_dlc_shoutout_raid/vehicles/fps_vehicle_muscle_1/fps_vehicle_muscle_1",
		escape_van = "units/pd2_dlc_drive/vehicles/fps_vehicle_van_1/fps_vehicle_van_1",
		falcogini = "units/pd2_dlc_cage/vehicles/fps_vehicle_falcogini_1/fps_vehicle_falcogini_1"
	}
	self._vehicle_units = {}

	return 
end
ElementVehicleSpawner.value = function (self, name)
	Application:trace("ElementVehicleSpawner:value", name)

	return self._values[name]
end
ElementVehicleSpawner.client_on_executed = function (self, ...)
	Application:trace("ElementVehicleSpawner:client_on_executed")

	if not self._values.enabled then
		return 
	end

	return 
end
ElementVehicleSpawner.on_executed = function (self, instigator)
	Application:trace("ElementVehicleSpawner:on_executed", inspect(self._values))

	if not self._values.enabled then
		return 
	end

	local vehicle = safe_spawn_unit(self._vehicles[self._values.vehicle], self._values.position, self._values.rotation)

	table.insert(self._vehicle_units, vehicle)
	print("[ElementVehicleSpawner] Spawned vehicle", vehicle)
	ElementVehicleSpawner.super.on_executed(self, self._unit or instigator)

	return 
end
ElementVehicleSpawner.unspawn_all_units = function (self)
	for _, vehicle_unit in ipairs(self._vehicle_units) do
		vehicle_unit.set_slot(vehicle_unit, 0)
	end

	return 
end
ElementVehicleSpawner.stop_simulation = function (self, ...)
	self.unspawn_all_units(self)

	return 
end

return 
