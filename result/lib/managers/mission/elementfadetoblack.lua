core:import("CoreMissionScriptElement")

ElementFadeToBlack = ElementFadeToBlack or class(CoreMissionScriptElement.MissionScriptElement)
ElementFadeToBlack.init = function (self, ...)
	ElementFadeToBlack.super.init(self, ...)

	return 
end
ElementFadeToBlack.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementFadeToBlack.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.overlay_effect:play_effect((self._values.state and tweak_data.overlay_effects.element_fade_in) or tweak_data.overlay_effects.element_fade_out)
	ElementFadeToBlack.super.on_executed(self, instigator)

	return 
end

return 
