core:import("CoreMissionScriptElement")

ElementDifficulty = ElementDifficulty or class(CoreMissionScriptElement.MissionScriptElement)
ElementDifficulty.init = function (self, ...)
	ElementDifficulty.super.init(self, ...)

	return 
end
ElementDifficulty.client_on_executed = function (self, ...)
	return 
end
ElementDifficulty.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_difficulty(self._values.difficulty)
	ElementDifficulty.super.on_executed(self, instigator)

	return 
end

return 
