core:import("CoreMissionScriptElement")

ElementPlayerCharacterTrigger = ElementPlayerCharacterTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementPlayerCharacterTrigger.init = function (self, ...)
	ElementPlayerCharacterTrigger.super.init(self, ...)

	return 
end
ElementPlayerCharacterTrigger.on_script_activated = function (self)
	ElementPlayerCharacterTrigger.super.on_script_activated(self)

	local is_host = Network:is_server() or Global.game_settings.single_player

	if is_host then
		managers.criminals:add_listener(string.format("add_criminal_event_%s", tostring(self._id)), {
			"on_criminal_added"
		}, callback(self, self, "on_criminal_added"))
		managers.criminals:add_listener(string.format("rmv_criminal_event_%s", tostring(self._id)), {
			"on_criminal_removed"
		}, callback(self, self, "on_criminal_removed"))
	end

	return 
end
ElementPlayerCharacterTrigger.on_criminal_added = function (self, name, unit, peer_id, ai)
	if not self.value(self, "trigger_on_left") and self.value(self, "character") == name then
		self.on_executed(self)
	end

	return 
end
ElementPlayerCharacterTrigger.on_criminal_removed = function (self, data)
	local name = data.name

	if self.value(self, "trigger_on_left") and self.value(self, "character") == name then
		self.on_executed(self)
	end

	return 
end
ElementPlayerCharacterTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementPlayerStateTrigger.super.on_executed(self, self._unit or instigator)

	return 
end
ElementPlayerCharacterFilter = ElementPlayerCharacterFilter or class(CoreMissionScriptElement.MissionScriptElement)
ElementPlayerCharacterFilter.init = function (self, ...)
	ElementPlayerCharacterFilter.super.init(self, ...)

	return 
end
ElementPlayerCharacterFilter.on_script_activated = function (self)
	ElementPlayerCharacterFilter.super.on_script_activated(self)

	if self.value(self, "execute_on_startup") then
		self.on_executed(self)
	end

	return 
end
ElementPlayerCharacterFilter.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local char_taken = self.is_character_taken(self, instigator)
	local require_char = self.value(self, "require_presence")

	if (char_taken and not require_char) or (require_char and not char_taken) then
		return 
	end

	ElementPlayerStateTrigger.super.on_executed(self, self._unit or instigator)

	return 
end
ElementPlayerCharacterFilter.is_character_taken = function (self, instigator)
	if self.value(self, "check_instigator") then
		return managers.criminals:character_name_by_unit(instigator) == self.value(self, "character")
	else
		return managers.criminals:is_taken(self.value(self, "character"))
	end

	return 
end

return 
