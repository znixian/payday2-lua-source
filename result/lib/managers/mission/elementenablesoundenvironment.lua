core:import("CoreMissionScriptElement")

ElementEnableSoundEnvironment = ElementEnableSoundEnvironment or class(CoreMissionScriptElement.MissionScriptElement)
ElementEnableSoundEnvironment.init = function (self, ...)
	ElementEnableSoundEnvironment.super.init(self, ...)

	return 
end
ElementEnableSoundEnvironment.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementEnableSoundEnvironment.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, name in pairs(self._values.elements) do
		managers.sound_environment:enable_area(name, self._values.enable)
	end

	ElementEnableSoundEnvironment.super.on_executed(self, instigator)

	return 
end

return 
