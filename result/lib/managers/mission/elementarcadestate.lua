core:import("CoreMissionScriptElement")

ElementArcadeState = ElementArcadeState or class(CoreMissionScriptElement.MissionScriptElement)
ElementArcadeState.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementArcadeState.super.on_executed(self, instigator)

	return 
end

return 
