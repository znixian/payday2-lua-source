core:import("CoreElementTimer")

ElementHeistTimer = ElementHeistTimer or class(CoreElementTimer.ElementTimer)
ElementHeistTimer.timer_operation_start = function (self)
	ElementHeistTimer.super.timer_operation_start(self)
	managers.game_play_central:start_inverted_heist_timer(self._timer)

	return 
end
ElementHeistTimer.timer_operation_pause = function (self)
	ElementHeistTimer.super.timer_operation_pause(self)
	managers.game_play_central:stop_heist_timer()

	return 
end
ElementHeistTimer.timer_operation_add_time = function (self, time)
	ElementHeistTimer.super.timer_operation_add_time(self, time)
	managers.game_play_central:modify_heist_timer(time)

	return 
end
ElementHeistTimer.timer_operation_subtract_time = function (self, time)
	ElementHeistTimer.super.timer_operation_subtract_time(self, time)
	managers.game_play_central:modify_heist_timer(-time)

	return 
end

return 
