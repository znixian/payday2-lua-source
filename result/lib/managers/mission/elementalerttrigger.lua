core:import("CoreMissionScriptElement")

ElementAlertTrigger = ElementAlertTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementAlertTrigger.init = function (self, ...)
	ElementAlertTrigger.super.init(self, ...)

	self._values.filter = self._values.filter or "0"
	self._values.alert_types = self._values.alert_types or {}
	self._filter = managers.navigation:convert_access_filter_to_number(self._values.filter)
	self._alert_types_map = {}

	for _, alert_type in ipairs(self._values.alert_types) do
		self._alert_types_map[alert_type] = true
	end

	return 
end
ElementAlertTrigger.client_on_executed = function (self, ...)
	return 
end
ElementAlertTrigger.on_executed = function (self, instigator)
	ElementAlertTrigger.super.on_executed(self, instigator)

	return 
end
ElementAlertTrigger.do_synced_execute = function (self, instigator)
	if Network:is_server() then
		self.on_executed(self, instigator)
	else
		managers.network:session():send_to_host("to_server_mission_element_trigger", self._id, instigator)
	end

	return 
end
ElementAlertTrigger.operation_add = function (self)
	if self._added then
		return 
	end

	self._added = true

	managers.groupai:state():add_alert_listener(self._id, callback(self, self, "on_alert"), self._filter, self._alert_types_map, self._values.position)

	return 
end
ElementAlertTrigger.operation_remove = function (self)
	if not self._added then
		return 
	end

	self._added = nil

	managers.groupai:state():remove_alert_listener(self._id)

	return 
end
ElementAlertTrigger.on_alert = function (self, alert_data)
	local instigator = alert_data[5]

	self.do_synced_execute(self, instigator)

	return 
end

return 
