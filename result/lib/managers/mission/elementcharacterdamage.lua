core:import("CoreMissionScriptElement")

ElementCharacterDamage = ElementCharacterDamage or class(CoreMissionScriptElement.MissionScriptElement)
ElementCharacterDamage.init = function (self, ...)
	ElementCharacterDamage.super.init(self, ...)

	self._units = {}
	local dmg_filter = self.value(self, "damage_types")

	if dmg_filter and dmg_filter ~= "" then
		self._allow_damage_types = {}
		local dmgs = string.split(dmg_filter, " ")

		for _, dmg_type in ipairs(dmgs) do
			table.insert(self._allow_damage_types, dmg_type)
		end
	end

	return 
end
ElementCharacterDamage.destroy = function (self)
	return 
end
ElementCharacterDamage.on_created = function (self)
	return 
end
ElementCharacterDamage.on_script_activated = function (self)
	for _, id in ipairs(self.value(self, "elements")) do
		local element = self.get_mission_element(self, id)

		if element.add_event_callback then
			element.add_event_callback(element, "spawn", callback(self, self, "unit_spawned"))
		end
	end

	return 
end
ElementCharacterDamage.unit_spawned = function (self, unit)
	if alive(unit) and unit.character_damage(unit) then
		unit.character_damage(unit):add_listener("character_damage_" .. tostring(unit.key(unit)), nil, callback(self, self, "clbk_linked_unit_took_damage"))
	end

	return 
end
ElementCharacterDamage.clbk_linked_unit_took_damage = function (self, unit, damage_info)
	if not alive(unit) then
		return 
	end

	local damage = damage_info.damage

	if self.value(self, "percentage") then
		damage = damage/unit.character_damage(unit)._HEALTH_INIT*100
	end

	self.on_executed(self, damage_info.attacker_unit, damage, damage_info.variant)

	return 
end
ElementCharacterDamage.on_executed = function (self, instigator, damage, damage_type)
	if not self._values.enabled then
		return 
	end

	local allow = true

	if self._allow_damage_types and not table.contains(self._allow_damage_types, damage_type) then
		allow = false
	end

	if allow then
		damage = math.floor(damage*tweak_data.gui.stats_present_multiplier)

		self.override_value_on_element_type(self, "ElementCounterOperator", "amount", damage)
		ElementCharacterDamage.super.on_executed(self, instigator)
	end

	return 
end
ElementCharacterDamage.save = function (self, data)
	data.enabled = self._values.enabled

	return 
end
ElementCharacterDamage.load = function (self, data)
	self.set_enabled(self, data.enabled)

	return 
end

return 
