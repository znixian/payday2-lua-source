core:import("CoreMissionScriptElement")

ElementExperience = ElementExperience or class(CoreMissionScriptElement.MissionScriptElement)
ElementExperience.init = function (self, ...)
	ElementExperience.super.init(self, ...)

	return 
end
ElementExperience.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.experience:mission_xp_award(self._values.amount)
	ElementExperience.super.on_executed(self, instigator)

	return 
end
ElementExperience.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end

return 
