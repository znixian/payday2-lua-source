core:import("CoreMissionScriptElement")

ElementLootSecuredTrigger = ElementLootSecuredTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementLootSecuredTrigger.init = function (self, ...)
	ElementLootSecuredTrigger.super.init(self, ...)

	return 
end
ElementLootSecuredTrigger.client_on_executed = function (self, ...)
	return 
end
ElementLootSecuredTrigger.on_script_activated = function (self)
	if self._values.report_only then
		managers.loot:add_trigger(self._id, "report_only", self._values.amount, callback(self, self, "on_executed"))
	else
		managers.loot:add_trigger(self._id, (self._values.include_instant_cash and "total_amount") or "amount", self._values.amount, callback(self, self, "on_executed"))
	end

	return 
end
ElementLootSecuredTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementLootSecuredTrigger.super.on_executed(self, instigator)

	return 
end

return 
