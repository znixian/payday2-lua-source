core:import("CoreMissionScriptElement")

ElementSetOutline = ElementSetOutline or class(CoreMissionScriptElement.MissionScriptElement)
ElementSetOutline.init = function (self, ...)
	ElementSetOutline.super.init(self, ...)

	return 
end
ElementSetOutline.client_on_executed = function (self, ...)
	return 
end
ElementSetOutline.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local function f(unit)
		if unit.contour(unit) then
			if self._values.clear_previous then
				unit.contour(unit):remove("highlight", true)
			end

			if self._values.set_outline then
				unit.contour(unit):add("highlight_character", true)
			else
				unit.contour(unit):remove("highlight_character", true)
			end
		end

		return 
	end

	if self._values.use_instigator then
		f(instigator)
	else
		for _, id in ipairs(self._values.elements) do
			local element = self.get_mission_element(self, id)

			element.execute_on_all_units(element, f)
		end
	end

	ElementSetOutline.super.on_executed(self, instigator)

	return 
end

return 
