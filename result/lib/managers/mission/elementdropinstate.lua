core:import("CoreMissionScriptElement")

ElementDropinState = ElementDropinState or class(CoreMissionScriptElement.MissionScriptElement)
ElementDropinState.init = function (self, ...)
	ElementDropinState.super.init(self, ...)

	return 
end
ElementDropinState.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementDropinState.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_allow_dropin(self._values.state)
	ElementDropinState.super.on_executed(self, instigator)

	return 
end

return 
