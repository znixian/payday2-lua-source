core:import("CoreMissionScriptElement")

ElementWhisperState = ElementWhisperState or class(CoreMissionScriptElement.MissionScriptElement)
ElementWhisperState.init = function (self, ...)
	ElementWhisperState.super.init(self, ...)

	return 
end
ElementWhisperState.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementWhisperState.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_whisper_mode(self._values.state)
	ElementWhisperState.super.on_executed(self, instigator)

	return 
end

return 
