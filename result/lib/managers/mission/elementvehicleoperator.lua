ElementVehicleOperator = ElementVehicleOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementVehicleOperator.init = function (self, ...)
	ElementVehicleOperator.super.init(self, ...)

	return 
end
ElementVehicleOperator.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementVehicleOperator._get_unit = function (self, unit_id)
	if Global.running_simulation then
		return managers.editor:unit_with_id(unit_id)
	else
		return managers.worlddefinition:get_unit(unit_id)
	end

	return 
end
ElementVehicleOperator._apply_opreator = function (self, unit)
	if unit then
		local extension = unit.npc_vehicle_driving(unit) or unit.vehicle_driving(unit)

		if extension then
			local call = extension[self._values.operation]

			if call then
				call(extension, tonumber(self._values.damage))
			else
				Application:error("Vehicle Operator applied to a unit that doesn't support the specified operation - opertion: ", self._values.operation)
			end
		else
			Application:error("Vehicle Operator applied to a unit that isn't a vehicle: ", inspect(unit))
		end
	else
		Application:warn("Vehicle Operator applied to a unit that doesn't exist - operator: ", inspect(self._unit))
	end

	return 
end
ElementVehicleOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.use_instigator then
		if instigator then
			self._apply_opreator(self, instigator)
		end
	else
		for _, id in ipairs(self._values.elements) do
			local unit = self._get_unit(self, id)

			self._apply_opreator(self, unit)
		end
	end

	ElementVehicleOperator.super.on_executed(self, instigator)

	return 
end

return 
