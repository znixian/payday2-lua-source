core:import("CoreMissionScriptElement")

ElementInventoryDummy = ElementInventoryDummy or class(CoreMissionScriptElement.MissionScriptElement)
ElementInventoryDummy.init = function (self, ...)
	ElementInventoryDummy.super.init(self, ...)

	if managers.sync then
		managers.sync:add_managed_unit(self._id, self)
	end

	return 
end
ElementInventoryDummy.client_on_executed = function (self, ...)
	return 
end
ElementInventoryDummy.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.category ~= "none" then
		if self._values.category == "primaries" or self._values.category == "secondaries" then
			self._spawn_weapon(self, self._values.category, self._values.slot, self._values.position, self._values.rotation)
		elseif self._values.category == "masks" then
			self._spawn_mask(self, self._values.category, self._values.slot, self._values.position, self._values.rotation)
		end
	end

	ElementInventoryDummy.super.on_executed(self, instigator)

	return 
end
ElementInventoryDummy._spawn_weapon = function (self, category, slot, position, rotation)
	if not Network:is_server() then
		return 
	end

	local category = managers.blackmarket:get_crafted_category(category)

	if not category then
		return 
	end

	local slot_data = category[slot]

	if not slot_data then
		return 
	end

	self._factory_id = slot_data.factory_id

	self.assemble_weapon(self, slot_data.factory_id, slot_data.blueprint, position, rotation)
	managers.sync:add_synced_weapon_blueprint(self._id, slot_data.factory_id, slot_data.blueprint)

	return 
end
ElementInventoryDummy.assemble_weapon = function (self, factory_id, blueprint, position, rotation)
	position = position or self._values.position
	rotation = rotation or self._values.rotation
	local unit_name = tweak_data.weapon.factory[factory_id].unit

	managers.dyn_resource:load(Idstring("unit"), Idstring(unit_name), DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)

	self._weapon_unit = World:spawn_unit(Idstring(unit_name), position, rotation)
	self._parts, self._blueprint = managers.weapon_factory:assemble_from_blueprint(factory_id, self._weapon_unit, blueprint, true, callback(self, self, "_assemble_completed"))

	self._weapon_unit:set_moving(true)

	return 
end
ElementInventoryDummy._assemble_completed = function (self, parts, blueprint)
	self._parts = parts
	self._blueprint = blueprint

	self._weapon_unit:set_moving(true)

	return 
end
ElementInventoryDummy._spawn_mask = function (self, category, slot, position, rotation)
	if not Network:is_server() then
		return 
	end

	local category = managers.blackmarket:get_crafted_category(category)

	if not category then
		return 
	end

	local slot_data = category[slot]

	if not slot_data then
		return 
	end

	self._mask_id = slot_data.mask_id

	self.assemble_mask(self, slot_data.mask_id, slot_data.blueprint, position, rotation)
	managers.sync:add_synced_mask_blueprint(self._id, slot_data.mask_id, slot_data.blueprint)

	return 
end
ElementInventoryDummy.assemble_mask = function (self, mask_id, blueprint, position, rotation)
	position = position or self._values.position
	rotation = rotation or self._values.rotation
	local mask_unit_name = managers.blackmarket:mask_unit_name_by_mask_id(mask_id)

	managers.dyn_resource:load(Idstring("unit"), Idstring(mask_unit_name), DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)

	self._mask_unit = World:spawn_unit(Idstring(mask_unit_name), position, rotation)

	if not tweak_data.blackmarket.masks[mask_id].type then
		local backside = World:spawn_unit(Idstring("units/payday2/masks/msk_backside/msk_backside"), position, rotation, position, rotation)

		self._mask_unit:link(self._mask_unit:orientation_object():name(), backside, backside.orientation_object(backside):name())
	end

	self._mask_unit:base():apply_blueprint(blueprint)
	self._mask_unit:set_moving(true)

	return 
end
ElementInventoryDummy.pre_destroy = function (self)
	ElementInventoryDummy.super.pre_destroy(self)
	self._destroy_weapon(self)
	self._destroy_mask(self)

	return 
end
ElementInventoryDummy._destroy_weapon = function (self)
	if alive(self._weapon_unit) then
		managers.weapon_factory:disassemble(self._parts)

		local name = self._weapon_unit:name()

		self._weapon_unit:base():set_slot(self._weapon_unit, 0)
		World:delete_unit(self._weapon_unit)
		managers.dyn_resource:unload(Idstring("unit"), name, DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)
	end

	return 
end
ElementInventoryDummy._destroy_mask = function (self)
	if alive(self._mask_unit) then
		for _, linked_unit in ipairs(self._mask_unit:children()) do
			linked_unit.unlink(linked_unit)
			World:delete_unit(linked_unit)
		end

		local name = self._mask_unit:name()

		World:delete_unit(self._mask_unit)
		managers.dyn_resource:unload(Idstring("unit"), name, DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)
	end

	return 
end

return 
