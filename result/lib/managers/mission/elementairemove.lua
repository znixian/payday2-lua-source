core:import("CoreMissionScriptElement")

ElementAIRemove = ElementAIRemove or class(CoreMissionScriptElement.MissionScriptElement)
ElementAIRemove.init = function (self, ...)
	ElementAIRemove.super.init(self, ...)

	return 
end
ElementAIRemove.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.use_instigator then
		if alive(instigator) then
			if self._values.force_ragdoll and instigator.movement(instigator) then
				if instigator.character_damage(instigator).damage_mission then
					local backup_so = self._values.backup_so and self.get_mission_element(self, self._values.backup_so)

					print("backup_so: ", self._values.backup_so, backup_so)
					instigator.character_damage(instigator):damage_mission({
						forced = true,
						damage = 1000,
						col_ray = {},
						backup_so = backup_so
					})
				end

				if instigator.movement(instigator)._active_actions and instigator.movement(instigator)._active_actions[1] and instigator.movement(instigator)._active_actions[1]:type() == "hurt" then
					instigator.movement(instigator)._active_actions[1]:force_ragdoll()
				end
			elseif self._values.true_death then
				if instigator.character_damage(instigator).damage_mission then
					local backup_so = self._values.backup_so and self.get_mission_element(self, self._values.backup_so)

					instigator.character_damage(instigator):damage_mission({
						forced = true,
						damage = 1000,
						col_ray = {},
						backup_so = backup_so
					})
				end
			else
				instigator.brain(instigator):set_active(false)
				instigator.base(instigator):set_slot(instigator, 0)
			end
		end
	else
		for _, id in ipairs(self._values.elements) do
			local element = self.get_mission_element(self, id)

			if self._values.true_death then
				element.kill_all_units(element)
			else
				element.unspawn_all_units(element)
			end
		end
	end

	ElementAIRemove.super.on_executed(self, instigator)

	return 
end

return 
