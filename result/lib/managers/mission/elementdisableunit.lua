core:import("CoreMissionScriptElement")

ElementDisableUnit = ElementDisableUnit or class(CoreMissionScriptElement.MissionScriptElement)
ElementDisableUnit.init = function (self, ...)
	ElementDisableUnit.super.init(self, ...)

	self._units = {}

	return 
end
ElementDisableUnit.on_script_activated = function (self)
	local elementBroken = false

	for _, id in ipairs(self._values.unit_ids) do
		if Global.running_simulation then
			if not managers.editor:unit_with_id(id) then
				print("MISSING UNIT WITH ID:", id)

				elementBroken = true
			else
				table.insert(self._units, managers.editor:unit_with_id(id))
			end
		else
			local unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "_load_unit"))

			if unit then
				table.insert(self._units, unit)
			end
		end
	end

	if elementBroken then
		for _, id in ipairs(self._values.unit_ids) do
			if managers.editor:unit_with_id(id) then
				print(inspect(managers.editor:unit_with_id(id)))
			end
		end
	end

	self._has_fetched_units = true

	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementDisableUnit._load_unit = function (self, unit)
	table.insert(self._units, unit)

	return 
end
ElementDisableUnit.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementDisableUnit.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, unit in ipairs(self._units) do
		managers.game_play_central:mission_disable_unit(unit)
	end

	ElementDisableUnit.super.on_executed(self, instigator)

	return 
end
ElementDisableUnit.save = function (self, data)
	data.save_me = true
	data.enabled = self._values.enabled

	return 
end
ElementDisableUnit.load = function (self, data)
	if not self._has_fetched_units then
		self.on_script_activated(self)
	end

	self.set_enabled(self, data.enabled)

	return 
end

return 
