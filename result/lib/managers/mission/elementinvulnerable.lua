core:import("CoreMissionScriptElement")

ElementInvulnerable = ElementInvulnerable or class(CoreMissionScriptElement.MissionScriptElement)
ElementInvulnerable.init = function (self, ...)
	ElementInvulnerable.super.init(self, ...)

	return 
end
ElementInvulnerable.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self.perform_invulnerable(self, instigator)
	ElementInvulnerable.super.on_executed(self, instigator)

	return 
end
ElementInvulnerable.client_on_executed = function (self, instigator)
	self.make_unit_invulnerable(self, instigator)

	return 
end
ElementInvulnerable.perform_invulnerable = function (self, instigator)
	self.make_unit_invulnerable(self, instigator)

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		for _, unit in ipairs(element.units(element)) do
			self.make_unit_invulnerable(self, unit)
		end
	end

	return 
end
ElementInvulnerable.make_unit_invulnerable = function (self, unit)
	if alive(unit) and unit.character_damage(unit) then
		unit.character_damage(unit):set_invulnerable(self._values.invulnerable)
		unit.character_damage(unit):set_immortal(self._values.immortal)
	end

	return 
end

return 
