core:import("CoreMissionScriptElement")

ElementJobValue = ElementJobValue or class(CoreMissionScriptElement.MissionScriptElement)
ElementJobValue.init = function (self, ...)
	ElementJobValue.super.init(self, ...)

	return 
end
ElementJobValue.client_on_executed = function (self, ...)
	if self._values.save then
		self.on_executed(self, ...)
	end

	return 
end
ElementJobValue.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.key ~= "none" then
		if self._values.save then
			managers.mission:set_saved_job_value(self._values.key, self._values.value)
		else
			managers.mission:set_job_value(self._values.key, self._values.value)
		end
	elseif Application:editor() then
		managers.editor:output_error("Cant set job value with key none.")
	end

	ElementJobValue.super.on_executed(self, instigator)

	return 
end
ElementJobValueFilter = ElementJobValueFilter or class(CoreMissionScriptElement.MissionScriptElement)
ElementJobValueFilter.init = function (self, ...)
	ElementJobValueFilter.super.init(self, ...)

	return 
end
ElementJobValueFilter.client_on_executed = function (self, ...)
	return 
end
ElementJobValueFilter.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local value = nil

	if self._values.save then
		value = managers.mission:get_saved_job_value(self._values.key)
	else
		value = managers.mission:get_job_value(self._values.key)
	end

	if not self._check_value(self, value) then
		return 
	end

	ElementJobValueFilter.super.on_executed(self, instigator)

	return 
end
ElementJobValueFilter._check_value = function (self, value)
	if self._values.check_type == "not_has_key" then
		return not value
	end

	if not value then
		return false
	end

	if self._values.check_type == "has_key" then
		return true
	end

	if not self._values.check_type or self._values.check_type == "equal" then
		return value == self._values.value
	end

	if self._values.check_type == "less_or_equal" then
		return value <= self._values.value
	end

	if self._values.check_type == "greater_or_equal" then
		return self._values.value <= value
	end

	if self._values.check_type == "less_than" then
		return value < self._values.value
	end

	if self._values.check_type == "greater_than" then
		return self._values.value < value
	end

	return 
end
ElementApplyJobValue = ElementApplyJobValue or class(CoreMissionScriptElement.MissionScriptElement)
ElementApplyJobValue.init = function (self, ...)
	ElementApplyJobValue.super.init(self, ...)

	return 
end
ElementApplyJobValue.client_on_executed = function (self, ...)
	return 
end
ElementApplyJobValue.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local value = nil

	if self._values.save then
		value = managers.mission:get_saved_job_value(self._values.key)
	else
		value = managers.mission:get_job_value(self._values.key)
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			element.apply_job_value(element, value)
		end
	end

	ElementApplyJobValue.super.on_executed(self, instigator)

	return 
end

return 
