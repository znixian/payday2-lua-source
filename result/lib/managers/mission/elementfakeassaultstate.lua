core:import("CoreMissionScriptElement")

ElementFakeAssaultState = ElementFakeAssaultState or class(CoreMissionScriptElement.MissionScriptElement)
ElementFakeAssaultState.init = function (self, ...)
	ElementFakeAssaultState.super.init(self, ...)

	return 
end
ElementFakeAssaultState.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementFakeAssaultState.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_fake_assault_mode(self._values.state)
	ElementFakeAssaultState.super.on_executed(self, instigator)

	return 
end

return 
