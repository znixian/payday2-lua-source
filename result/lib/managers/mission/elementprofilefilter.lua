core:import("CoreMissionScriptElement")

ElementProfileFilter = ElementProfileFilter or class(CoreMissionScriptElement.MissionScriptElement)
ElementProfileFilter.init = function (self, ...)
	ElementProfileFilter.super.init(self, ...)

	return 
end
ElementProfileFilter.client_on_executed = function (self, ...)
	return 
end
ElementProfileFilter.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if not self._check_player_lvl(self) then
		return 
	end

	if not self._check_total_money_earned(self) then
		return 
	end

	if not self._check_total_money_offshore(self) then
		return 
	end

	if not self._check_achievement(self) then
		return 
	end

	ElementProfileFilter.super.on_executed(self, instigator)

	return 
end
ElementProfileFilter._check_player_lvl = function (self)
	local pass = self._values.player_lvl <= managers.experience:current_level()

	return pass
end
ElementProfileFilter._check_total_money_earned = function (self)
	local pass = self._values.money_earned*1000 <= managers.money:total_collected()

	return pass
end
ElementProfileFilter._check_total_money_offshore = function (self)
	if not self._values.money_offshore then
		return false
	end

	local pass = self._values.money_offshore*1000 <= managers.money:offshore()

	return pass
end
ElementProfileFilter._check_achievement = function (self)
	if self._values.achievement == "none" then
		return true
	end

	local info = managers.achievment:get_info(self._values.achievement)
	local pass = info and info.awarded

	return pass
end

return 
