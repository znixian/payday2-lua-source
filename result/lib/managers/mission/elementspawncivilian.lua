core:import("CoreMissionScriptElement")

ElementSpawnCivilian = ElementSpawnCivilian or class(CoreMissionScriptElement.MissionScriptElement)
ElementSpawnCivilian.init = function (self, ...)
	ElementSpawnCivilian.super.init(self, ...)

	self._enemy_name = (self._values.enemy and Idstring(self._values.enemy)) or Idstring("units/characters/civilians/dummy_civilian_1/dummy_civilian_1")
	self._units = {}

	self._finalize_values(self)

	return 
end
ElementSpawnCivilian._finalize_values = function (self)
	self._values.state = self.value(self, "state")
	local state_index = table.index_of(CopActionAct._act_redirects.civilian_spawn, self._values.state)
	self._values.state = (state_index ~= -1 and state_index) or nil
	self._values.force_pickup = (self._values.force_pickup ~= "none" and self._values.force_pickup) or nil
	self._values.team = (self._values.team ~= "default" and self._values.team) or nil

	return 
end
ElementSpawnCivilian.enemy_name = function (self)
	return self._enemy_name
end
ElementSpawnCivilian.units = function (self)
	return self._units
end
ElementSpawnCivilian.produce = function (self, params)
	if not managers.groupai:state():is_AI_enabled() then
		return 
	end

	local default_team_id = (params and params.team) or self._values.team or tweak_data.levels:get_default_team_ID("non_combatant")
	local unit = safe_spawn_unit(self._enemy_name, self.get_orientation(self))
	unit.unit_data(unit).mission_element = self

	table.insert(self._units, unit)

	if self._values.state then
		local state = CopActionAct._act_redirects.civilian_spawn[self._values.state]

		if unit.brain(unit) then
			local action_data = {
				align_sync = true,
				body_part = 1,
				type = "act",
				variant = state
			}
			local spawn_ai = {
				init_state = "idle",
				objective = {
					interrupt_health = 1,
					interrupt_dis = -1,
					type = "act",
					action = action_data
				}
			}

			unit.brain(unit):set_spawn_ai(spawn_ai)
		else
			unit.base(unit):play_state(state)
		end
	end

	if self._values.force_pickup then
		unit.character_damage(unit):set_pickup(self._values.force_pickup)
	end

	unit.movement(unit):set_team(managers.groupai:state():team_data(default_team_id))
	self.event(self, "spawn", unit)

	return unit
end
ElementSpawnCivilian.event = function (self, name, unit)
	if self._events and self._events[name] then
		for _, callback in ipairs(self._events[name]) do
			callback(unit)
		end
	end

	return 
end
ElementSpawnCivilian.add_event_callback = function (self, name, callback)
	self._events = self._events or {}
	self._events[name] = self._events[name] or {}

	table.insert(self._events[name], callback)

	return 
end
ElementSpawnCivilian.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if not managers.groupai:state():is_AI_enabled() and not Application:editor() then
		return 
	end

	local unit = self.produce(self)

	ElementSpawnCivilian.super.on_executed(self, unit)

	return 
end
ElementSpawnCivilian.unspawn_all_units = function (self)
	ElementSpawnEnemyDummy.unspawn_all_units(self)

	return 
end
ElementSpawnCivilian.kill_all_units = function (self)
	ElementSpawnEnemyDummy.kill_all_units(self)

	return 
end
ElementSpawnCivilian.execute_on_all_units = function (self, func)
	ElementSpawnEnemyDummy.execute_on_all_units(self, func)

	return 
end

return 
