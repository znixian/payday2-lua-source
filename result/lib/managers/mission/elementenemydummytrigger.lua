core:import("CoreMissionScriptElement")

ElementEnemyDummyTrigger = ElementEnemyDummyTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementEnemyDummyTrigger.init = function (self, ...)
	ElementEnemyDummyTrigger.super.init(self, ...)

	return 
end
ElementEnemyDummyTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_event_callback(element, self._values.event, callback(self, self, "on_executed"))
	end

	return 
end
ElementEnemyDummyTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementEnemyDummyTrigger.super.on_executed(self, instigator)

	return 
end
ElementEnemyDummyTrigger.load = function (self)
	return 
end

return 
