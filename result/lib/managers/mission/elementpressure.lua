core:import("CoreMissionScriptElement")

ElementPressure = ElementPressure or class(CoreMissionScriptElement.MissionScriptElement)
ElementPressure.init = function (self, ...)
	ElementPressure.super.init(self, ...)

	return 
end
ElementPressure.client_on_executed = function (self, ...)
	return 
end
ElementPressure.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.points ~= 0 then
		if 0 < self._values.points then
			managers.groupai:state():add_pressure(self._values.points)
		else
			managers.groupai:state():add_cooldown(math.abs(self._values.points))
		end
	else
		local interval = self._values.interval ~= 0 and self._values.interval

		managers.groupai:state():set_heat_build_period(interval)
	end

	ElementPressure.super.on_executed(self, instigator)

	return 
end

return 
