core:import("CoreMissionScriptElement")

ElementLookAtTrigger = ElementLookAtTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementLookAtTrigger.init = function (self, ...)
	ElementLookAtTrigger.super.init(self, ...)

	return 
end
ElementLookAtTrigger.on_script_activated = function (self)
	self.add_callback(self)

	return 
end
ElementLookAtTrigger.set_enabled = function (self, enabled)
	ElementLookAtTrigger.super.set_enabled(self, enabled)

	if enabled then
		self.add_callback(self)
	end

	return 
end
ElementLookAtTrigger.add_callback = function (self)
	if not self._callback then
		self._callback = self._mission_script:add(callback(self, self, "update_lookat"), self._values.interval)
	end

	return 
end
ElementLookAtTrigger.remove_callback = function (self)
	if self._callback then
		self._mission_script:remove(self._callback)

		self._callback = nil
	end

	return 
end
ElementLookAtTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementLookAtTrigger.super.on_executed(self, instigator)

	if not self._values.enabled then
		self.remove_callback(self)
	end

	return 
end
ElementLookAtTrigger.update_lookat = function (self)
	if not self._values.enabled then
		return 
	end

	local player = managers.player:player_unit()

	if alive(player) then
		local dir = self._values.position - player.camera(player):position()

		if self._values.distance and 0 < self._values.distance then
			local distance = dir.length(dir)

			if self._values.distance < distance then
				return 
			end
		end

		if self._values.in_front then
			local dot = player.camera(player):forward():dot(self._values.rotation:y())

			if 0 < dot then
				return 
			end
		end

		dir = dir.normalized(dir)
		local dot = player.camera(player):forward():dot(dir)

		if self._values.sensitivity <= dot then
			if Network:is_client() then
				managers.network:session():send_to_host("to_server_mission_element_trigger", self._id, player)
			else
				self.on_executed(self, player)
			end
		end
	end

	return 
end

return 
