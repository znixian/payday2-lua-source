core:import("CoreMissionScriptElement")

ElementSpecialObjectiveTrigger = ElementSpecialObjectiveTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementSpecialObjectiveTrigger.init = function (self, ...)
	ElementSpecialObjectiveTrigger.super.init(self, ...)

	return 
end
ElementSpecialObjectiveTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_event_callback(element, self._values.event, callback(self, self, "on_executed"))
	end

	return 
end
ElementSpecialObjectiveTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementSpecialObjectiveTrigger.super.on_executed(self, instigator)

	return 
end

return 
