core:import("CoreMissionScriptElement")

ElementUnloadStatic = ElementUnloadStatic or class(CoreMissionScriptElement.MissionScriptElement)
ElementUnloadStatic.init = function (self, ...)
	ElementUnloadStatic.super.init(self, ...)

	return 
end
ElementUnloadStatic.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementUnloadStatic._get_unit = function (self, unit_id)
	if Application:editor() then
		return managers.editor:unit_with_id(unit_id)
	else
		return managers.worlddefinition:get_unit(unit_id)
	end

	return 
end
ElementUnloadStatic.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if not Application:editor() then
		for _, unit_id in ipairs(self._values.unit_ids) do
			local unit = self._get_unit(self, unit_id)

			if alive(unit) then
				print("[Mission] unload unit ", unit_id, unit)
				World:delete_unit_free_assets(unit)
			end
		end
	end

	ElementUnloadStatic.super.on_executed(self, instigator)

	return 
end

return 
