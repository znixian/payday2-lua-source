core:import("CoreMissionScriptElement")

ElementActionMessage = ElementActionMessage or class(CoreMissionScriptElement.MissionScriptElement)
ElementActionMessage.init = function (self, ...)
	ElementActionMessage.super.init(self, ...)

	return 
end
ElementActionMessage.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementActionMessage.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.message_id ~= "none" then
		if instigator ~= managers.player:player_unit() then
			managers.action_messaging:show_message(self._values.message_id, instigator)
		end
	elseif Application:editor() then
		managers.editor:output_error("Cant show message " .. self._values.message_id .. " in element " .. self._editor_name .. ".")
	end

	ElementActionMessage.super.on_executed(self, instigator)

	return 
end

return 
