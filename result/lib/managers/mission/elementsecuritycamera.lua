core:import("CoreMissionScriptElement")

ElementSecurityCamera = ElementSecurityCamera or class(CoreMissionScriptElement.MissionScriptElement)
ElementSecurityCamera.init = function (self, ...)
	ElementSecurityCamera.super.init(self, ...)

	self._triggers = {}

	return 
end
ElementSecurityCamera.on_executed = function (self, instigator)
	if not self._values.enabled or Network:is_client() then
		return 
	end

	if not self._values.camera_u_id then
		return 
	end

	local camera_unit = self._fetch_unit_by_unit_id(self, self._values.camera_u_id)

	if not camera_unit then
		return 
	end

	local ai_state = (self._values.ai_enabled and true) or false
	local settings = nil

	if ai_state or self._values.apply_settings then
		settings = {
			yaw = self._values.yaw,
			pitch = self._values.pitch,
			fov = self._values.fov,
			detection_range = self._values.detection_range*100,
			suspicion_range = self._values.suspicion_range*100,
			detection_delay = {
				self._values.detection_delay_min,
				self._values.detection_delay_max
			}
		}
	end

	camera_unit.base(camera_unit):set_detection_enabled(ai_state, settings, self)
	ElementSpecialObjective.super.on_executed(self, instigator)

	return 
end
ElementSecurityCamera.client_on_executed = function (self, ...)
	return 
end
ElementSecurityCamera._fetch_unit_by_unit_id = function (self, unit_id)
	local unit = nil

	if Application:editor() then
		unit = managers.editor:unit_with_id(tonumber(unit_id))
	else
		unit = managers.worlddefinition:get_unit_on_load(tonumber(unit_id), callback(self, self, "_load_unit"))
	end

	return unit
end
ElementSecurityCamera._load_unit = function (unit)
	return 
end
ElementSecurityCamera.on_script_activated = function (self)
	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementSecurityCamera.on_destroyed = function (self)
	if not self._values.enabled then
		return 
	end

	self._values.destroyed = true

	self.check_triggers(self, "destroyed")

	return 
end
ElementSecurityCamera.on_alarm = function (self)
	if not self._values.enabled then
		return 
	end

	self._values.alarm = true

	self.check_triggers(self, "alarm")

	return 
end
ElementSecurityCamera.add_trigger = function (self, id, type, callback)
	self._triggers[type] = self._triggers[type] or {}
	self._triggers[type][id] = {
		callback = callback
	}

	return 
end
ElementSecurityCamera.remove_trigger = function (self, id, type)
	if self._triggers[type] then
		self._triggers[type][id] = nil
	end

	return 
end
ElementSecurityCamera.check_triggers = function (self, type, instigator)
	if not self._triggers[type] then
		return 
	end

	for id, cb_data in pairs(self._triggers[type]) do
		cb_data.callback(instigator)
	end

	return 
end
ElementSecurityCamera.save = function (self, data)
	data.enabled = self._values.enabled
	data.destroyed = self._values.destroyed

	return 
end
ElementSecurityCamera.load = function (self, data)
	self.set_enabled(self, data.enabled)

	self._values.destroyed = data.destroyed

	return 
end

return 
