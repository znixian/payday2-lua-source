core:import("CoreMissionScriptElement")

ElementVariableGet = ElementVariableGet or class(CoreMissionScriptElement.MissionScriptElement)
ElementVariableGet.init = function (self, ...)
	ElementVariableGet.super.init(self, ...)

	return 
end
ElementVariableGet.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.activated and not managers.challenge:mission_value(self._values.variable) then
		return 
	elseif not self._values.activated and managers.challenge:mission_value(self._values.variable) then
		return 
	end

	ElementVariableGet.super.on_executed(self, instigator)

	return 
end
ElementVariableGet.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end

return 
