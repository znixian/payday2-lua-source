core:import("CoreMissionScriptElement")

ElementScenarioEvent = ElementScenarioEvent or class(CoreMissionScriptElement.MissionScriptElement)
ElementScenarioEvent.init = function (self, ...)
	ElementScenarioEvent.super.init(self, ...)

	self._network_execute = true

	return 
end
ElementScenarioEvent.client_on_executed = function (self, ...)
	return 
end
ElementScenarioEvent.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementScenarioEvent.super.on_executed(self, instigator)

	return 
end
ElementScenarioEvent.operation_add = function (self)
	local event_decriptor = {
		amount = self._values.amount,
		task_type = self._values.task,
		element = self,
		pos = self._values.position,
		rot = self._values.rotation,
		base_chance = self._values.base_chance or 1,
		chance_inc = self._values.chance_inc or 0
	}

	managers.groupai:state():add_spawn_event(self._id, event_decriptor)

	return 
end
ElementScenarioEvent.operation_remove = function (self)
	managers.groupai:state():remove_spawn_event(self._id)

	return 
end

return 
