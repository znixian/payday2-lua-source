core:import("CoreMissionScriptElement")

ElementFleePoint = ElementFleePoint or class(CoreMissionScriptElement.MissionScriptElement)
ElementFleePoint.init = function (self, ...)
	ElementFleePoint.super.init(self, ...)

	return 
end
ElementFleePoint.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self.operation_add(self)
	ElementFleePoint.super.on_executed(self, instigator)

	return 
end
ElementFleePoint.operation_add = function (self)
	if self._values.functionality == "loot_drop" then
		managers.groupai:state():add_enemy_loot_drop_point(self._id, self._values.position)
	else
		managers.groupai:state():add_flee_point(self._id, self._values.position)
	end

	return 
end
ElementFleePoint.operation_remove = function (self)
	if self._values.functionality == "loot_drop" then
		managers.groupai:state():remove_enemy_loot_drop_point(self._id)
	else
		managers.groupai:state():remove_flee_point(self._id)
	end

	return 
end

return 
