core:import("CoreMissionScriptElement")

ElementDisableShout = ElementDisableShout or class(CoreMissionScriptElement.MissionScriptElement)
ElementDisableShout.init = function (self, ...)
	ElementDisableShout.super.init(self, ...)

	return 
end
ElementDisableShout.client_on_executed = function (self, ...)
	return 
end
ElementDisableShout.sync_function = function (unit, state)
	unit.unit_data(unit).disable_shout = state

	return 
end
ElementDisableShout.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local function f(unit)
		ElementDisableShout.sync_function(unit, self._values.disable_shout)
		managers.network:session():send_to_peers_synched("sync_disable_shout", unit, self._values.disable_shout)

		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.execute_on_all_units(element, f)
	end

	ElementDisableShout.super.on_executed(self, instigator)

	return 
end

return 
