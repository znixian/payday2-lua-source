core:import("CoreMissionScriptElement")

ElementCinematicCamera = ElementCinematicCamera or class(CoreMissionScriptElement.MissionScriptElement)
ElementCinematicCamera.init = function (self, ...)
	ElementCinematicCamera.super.init(self, ...)

	self._camera_unit = nil

	return 
end
ElementCinematicCamera.on_script_activated = function (self)
	self._camera_unit = safe_spawn_unit(Idstring("units/pd2_cinematics/cs_camera/cs_camera"), self._values.position, self._values.rotation)

	self._camera_unit:base():set_mission_element(self)

	return 
end
ElementCinematicCamera.client_on_executed = function (self, ...)
	return 
end
ElementCinematicCamera.on_executed = function (self, instigator, alternative, ...)
	if not self._values.enabled then
		return 
	end

	if not alternative or type_name(alternative) ~= "string" then
		self._camera_unit:base():start()
		self._camera_unit:base():play_state(Idstring(self._values.state))
	end

	ElementCinematicCamera.super.on_executed(self, instigator, alternative or "normal", ...)

	return 
end
ElementCinematicCamera.anim_clbk_done = function (self)
	self._camera_unit:base():stop()
	self.on_executed(self, nil, "camera_done")

	return 
end
ElementCinematicCamera._delete_camera_unit = function (self)
	if not self._camera_unit then
		return 
	end

	World:delete_unit(self._camera_unit)

	self._camera_unit = nil

	return 
end
ElementCinematicCamera.stop_simulation = function (self, ...)
	ElementCinematicCamera.super.stop_simulation(self, ...)
	self._delete_camera_unit(self)

	return 
end
ElementCinematicCamera.pre_destroy = function (self, ...)
	ElementCinematicCamera.super.pre_destroy(self, ...)
	self._delete_camera_unit(self)

	return 
end

return 
