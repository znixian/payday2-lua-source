core:import("CoreMissionScriptElement")

ElementGameEventIsDone = ElementGameEventIsDone or class(CoreMissionScriptElement.MissionScriptElement)
ElementGameEventIsDone.init = function (self, ...)
	ElementGameEventIsDone.super.init(self, ...)

	return 
end
ElementGameEventIsDone.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local ok = false

	if not ok then
		return 
	end

	ElementGameEventIsDone.super.on_executed(self, instigator)

	return 
end
ElementGameEventIsDone.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end

return 
