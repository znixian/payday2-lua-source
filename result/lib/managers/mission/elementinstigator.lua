core:import("CoreMissionScriptElement")

ElementInstigator = ElementInstigator or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstigator.init = function (self, ...)
	ElementInstigator.super.init(self, ...)

	self._instigators = {}
	self._triggers = {}

	return 
end
ElementInstigator.client_on_executed = function (self, ...)
	return 
end
ElementInstigator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self.instigator_operation_set(self, instigator)
	ElementInstigator.super.on_executed(self, instigator)

	return 
end
ElementInstigator.instigator_operation_set = function (self, instigator)
	if not self._is_valid_instigator(self, instigator) then
		return 
	end

	if not table.contains(self._instigators, instigator) then
		self._check_triggers(self, "changed")
	end

	if alive(instigator) and not instigator.character_damage(instigator):dead() then
		self._instigators = {
			instigator
		}

		if instigator.unit_data(instigator).mission_element then
			instigator.unit_data(instigator).mission_element:add_event_callback("death", callback(self, self, "on_instigator_death"))
		end

		self._check_triggers(self, "set")
	end

	return 
end
ElementInstigator.instigator_operation_add_first = function (self, instigator)
	if not self._is_valid_instigator(self, instigator) then
		return 
	end

	if table.contains(self._instigators, instigator) then
		return 
	end

	if alive(instigator) and not instigator.character_damage(instigator):dead() then
		table.insert(self._instigators, 1, instigator)

		if instigator.unit_data(instigator).mission_element then
			instigator.unit_data(instigator).mission_element:add_event_callback("death", callback(self, self, "on_instigator_death"))
		end

		self._check_triggers(self, "add_first")
	end

	return 
end
ElementInstigator.instigator_operation_add_last = function (self, instigator)
	if not self._is_valid_instigator(self, instigator) then
		return 
	end

	if table.contains(self._instigators, instigator) then
		return 
	end

	if alive(instigator) and not instigator.character_damage(instigator):dead() then
		table.insert(self._instigators, instigator)

		if instigator.unit_data(instigator).mission_element then
			instigator.unit_data(instigator).mission_element:add_event_callback("death", callback(self, self, "on_instigator_death"))
		end

		self._check_triggers(self, "add_last")
	end

	return 
end
ElementInstigator._is_valid_instigator = function (self, instigator)
	if type_name(instigator) ~= "Unit" then
		local msg = "[ElementInstigator:_is_valid_instigator] Element can only store units as instigators. Tried to use type " .. type_name(instigator) .. "."

		if Application:editor() then
			managers.editor:output_error(msg)
		else
			Application:error(msg)
		end

		return false
	end

	if not alive(instigator) or instigator.character_damage(instigator):dead() then
		if Application:editor() then
			managers.editor:output_error("Cant set instigator. Reason: " .. ((not alive(instigator) and " Dont exist") or (instigator.character_damage(instigator):dead() and "Dead")) .. ". In element " .. self._editor_name .. ".")
		end

		return false
	end

	return true
end
ElementInstigator.on_instigator_death = function (self, unit)
	if not table.contains(self._instigators, unit) then
		return 
	end

	self._check_triggers(self, "death")

	return 
end
ElementInstigator.instigator_operation_clear = function (self, instigator)
	self._instigators = {}

	self._check_triggers(self, "cleared")

	return 
end
ElementInstigator.instigator_operation_use_first = function (self, keep_on_use)
	return (keep_on_use and self._instigators[1]) or table.remove(self._instigators, 1)
end
ElementInstigator.instigator_operation_use_last = function (self, keep_on_use)
	return (keep_on_use and self._instigators[#self._instigators]) or table.remove(self._instigators)
end
ElementInstigator.instigator_operation_use_random = function (self, keep_on_use)
	local index = math.random(#self._instigators)

	return (keep_on_use and self._instigators[index]) or table.remove(self._instigators, index)
end
ElementInstigator.instigator_operation_use_all = function (self, keep_on_use)
	if keep_on_use then
		return self._instigators
	end

	local instigators = clone(self._instigators)
	self._instigators = {}

	return instigators
end
ElementInstigator.add_trigger = function (self, id, type, callback)
	self._triggers[type] = self._triggers[type] or {}
	self._triggers[type][id] = {
		callback = callback
	}

	return 
end
ElementInstigator._check_triggers = function (self, type)
	if not self._triggers[type] then
		return 
	end

	for id, cb_data in pairs(self._triggers[type]) do
		cb_data.callback()
	end

	return 
end
ElementInstigatorOperator = ElementInstigatorOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstigatorOperator.init = function (self, ...)
	ElementInstigatorOperator.super.init(self, ...)

	return 
end
ElementInstigatorOperator.client_on_executed = function (self, ...)
	return 
end
ElementInstigatorOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.operation ~= "none" then
		for _, id in ipairs(self._values.elements) do
			local element = self.get_mission_element(self, id)

			if element then
				if self._values.operation == "set" then
					element.instigator_operation_set(element, instigator)
				elseif self._values.operation == "add_first" then
					element.instigator_operation_add_first(element, instigator)
				elseif self._values.operation == "add_last" then
					element.instigator_operation_add_last(element, instigator)
				elseif self._values.operation == "clear" then
					element.instigator_operation_clear(element)
				elseif self._values.operation == "use_first" then
					self._check_and_execute(self, element.instigator_operation_use_first(element, self._values.keep_on_use))
				elseif self._values.operation == "use_last" then
					self._check_and_execute(self, element.instigator_operation_use_last(element, self._values.keep_on_use))
				elseif self._values.operation == "use_random" then
					self._check_and_execute(self, element.instigator_operation_use_random(element, self._values.keep_on_use))
				elseif self._values.operation == "use_all" then
					for _, use_instigator in ipairs(element.instigator_operation_use_all(element, self._values.keep_on_use)) do
						self._check_and_execute(self, use_instigator)
					end
				end
			end
		end
	elseif Application:editor() then
		managers.editor:output_error("Cant use operation " .. self._values.operation .. " in element " .. self._editor_name .. ".")
	end

	if self._values.operation == "set" or self._values.operation == "add_first" or self._values.operation == "add_last" or self._values.operation == "clear" then
		ElementInstigatorOperator.super.on_executed(self, instigator)
	end

	return 
end
ElementInstigatorOperator._check_and_execute = function (self, use_instigator)
	if alive(use_instigator) and not use_instigator.character_damage(use_instigator):dead() then
		ElementInstigatorOperator.super.on_executed(self, use_instigator)
	elseif Application:editor() then
		managers.editor:output_warning("Cant use instigator. Reason: " .. ((not alive(use_instigator) and " Dont exist") or (use_instigator.character_damage(use_instigator):dead() and "Dead")) .. ". In element " .. self._editor_name .. ".")
	end

	return 
end
ElementInstigatorTrigger = ElementInstigatorTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstigatorTrigger.init = function (self, ...)
	ElementInstigatorTrigger.super.init(self, ...)

	return 
end
ElementInstigatorTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_trigger(element, self._id, self._values.trigger_type, callback(self, self, "on_executed"))
	end

	return 
end
ElementInstigatorTrigger.client_on_executed = function (self, ...)
	return 
end
ElementInstigatorTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementInstigatorTrigger.super.on_executed(self, instigator)

	return 
end

return 
