core:import("CoreMissionScriptElement")

ElementBlackscreenVariant = ElementBlackscreenVariant or class(CoreMissionScriptElement.MissionScriptElement)
ElementBlackscreenVariant.init = function (self, ...)
	ElementBlackscreenVariant.super.init(self, ...)

	return 
end
ElementBlackscreenVariant.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementBlackscreenVariant.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_blackscreen_variant(tonumber(self._values.variant))
	ElementBlackscreenVariant.super.on_executed(self, instigator)

	return 
end
ElementEndscreenVariant = ElementEndscreenVariant or class(CoreMissionScriptElement.MissionScriptElement)
ElementEndscreenVariant.init = function (self, ...)
	ElementEndscreenVariant.super.init(self, ...)

	return 
end
ElementEndscreenVariant.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementEndscreenVariant.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_endscreen_variant(tonumber(self._values.variant))
	ElementEndscreenVariant.super.on_executed(self, instigator)

	return 
end

return 
