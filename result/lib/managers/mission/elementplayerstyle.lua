core:import("CoreMissionScriptElement")

ElementPlayerStyle = ElementPlayerStyle or class(CoreMissionScriptElement.MissionScriptElement)
ElementPlayerStyle.init = function (self, ...)
	ElementPlayerStyle.super.init(self, ...)

	return 
end
ElementPlayerStyle.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementPlayerStyle.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.player:change_player_look(self._values.style)
	ElementPlayerStyle.super.on_executed(self, instigator)

	return 
end

return 
