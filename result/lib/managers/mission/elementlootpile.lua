core:import("CoreMissionScriptElement")

ElementLootPile = ElementLootPile or class(CoreMissionScriptElement.MissionScriptElement)
ElementLootPile.init = function (self, ...)
	ElementLootPile.super.init(self, ...)

	local max_loot = self.value(self, "max_loot")
	self._remaining_loot = (0 < max_loot and max_loot) or 10000000
	self._steal_SO_data = {}

	return 
end
ElementLootPile.on_script_activated = function (self)
	ElementLootPile.super.on_script_activated(self)

	if not self._updator and Network:is_server() then
		self._updator = true

		self._mission_script:add_updator(self._id, callback(self, self, "update"))
	end

	self.on_set_enabled(self)

	return 
end
ElementLootPile.on_set_enabled = function (self)
	if self.enabled(self) then
		self.register_steal_SO(self)
	else
		self.unregister_steal_SO(self)
	end

	return 
end
ElementLootPile.update = function (self, t, dt)
	if self._next_steal_time ~= nil then
		self._next_steal_time = self._next_steal_time - dt

		if self._next_steal_time <= 0 then
			self._next_steal_time = nil

			if self.enabled(self) then
				self.register_steal_SO(self)
			end
		end
	end

	return 
end
ElementLootPile.register_steal_SO = function (self)
	if self._remaining_loot <= 0 then
		return 
	end

	local loot_index = self._remaining_loot
	local tweak_info = tweak_data.carry[self.value(self, "carry_id")]
	local AI_carry = tweak_info.AI_carry

	if not AI_carry then
		return 
	end

	local pos, rot = self.get_orientation(self)
	local tracker_pickup = managers.navigation:create_nav_tracker(pos, false)
	local pickup_nav_seg = tracker_pickup.nav_segment(tracker_pickup)
	local pickup_pos = tracker_pickup.field_position(tracker_pickup)
	local pickup_area = managers.groupai:state():get_area_from_nav_seg_id(pickup_nav_seg)

	managers.navigation:destroy_nav_tracker(tracker_pickup)

	local drop_pos, drop_nav_seg, drop_area = nil
	local drop_point = managers.groupai:state():get_safe_enemy_loot_drop_point(pickup_nav_seg)

	if drop_point then
		drop_pos = mvector3.copy(drop_point.pos)
		drop_nav_seg = drop_point.nav_seg
		drop_area = drop_point.area
	else
		self._next_steal_time = tonumber(self.value(self, "retry_delay")) or 5

		return 
	end

	local drop_objective = {
		type = "act",
		interrupt_health = 0.5,
		action_duration = 2,
		haste = "walk",
		pose = "crouch",
		interrupt_dis = 400,
		nav_seg = drop_nav_seg,
		pos = drop_pos,
		area = drop_area,
		fail_clbk = callback(self, self, "on_secure_SO_failed", loot_index),
		complete_clbk = callback(self, self, "on_secure_SO_completed", loot_index),
		action = {
			variant = "untie",
			align_sync = true,
			body_part = 1,
			type = "act"
		}
	}
	local pickup_objective = {
		destroy_clbk_key = false,
		type = "act",
		haste = "run",
		interrupt_health = 0.5,
		pose = "crouch",
		interrupt_dis = 100,
		nav_seg = pickup_nav_seg,
		area = pickup_area,
		pos = pickup_pos,
		fail_clbk = callback(self, self, "on_pickup_SO_failed", loot_index),
		complete_clbk = callback(self, self, "on_pickup_SO_completed", loot_index),
		action = {
			variant = "untie",
			align_sync = true,
			body_part = 1,
			type = "act"
		},
		action_duration = math.lerp(1, 2.5, math.random()),
		followup_objective = drop_objective
	}
	local so_descriptor = {
		interval = 0,
		base_chance = 1,
		chance_inc = 0,
		usage_amount = 1,
		objective = pickup_objective,
		search_pos = pickup_objective.pos,
		verification_clbk = callback(self, self, "clbk_pickup_SO_verification", loot_index),
		AI_group = AI_carry.SO_category,
		admin_clbk = callback(self, self, "on_pickup_SO_administered", loot_index)
	}
	local so_id = string.format("carrysteal_%i_pile_%s", loot_index, tostring(self._id))
	self._steal_SO_data[loot_index] = {
		SO_registered = true,
		picked_up = false,
		SO_id = so_id,
		pickup_area = pickup_area,
		pickup_objective = pickup_objective
	}

	managers.groupai:state():add_special_objective(so_id, so_descriptor)

	self._next_steal_time = tonumber(self.value(self, "reissue_delay")) or 30

	return 
end
ElementLootPile.unregister_steal_SO = function (self)
	for i, SO_data in pairs(self._steal_SO_data) do
		if SO_data.SO_registered then
			managers.groupai:state():remove_special_objective(SO_data.SO_id)
			managers.groupai:state():unregister_loot(self._unit:key())
		elseif SO_data.thief then
			local thief = SO_data.thief
			SO_data.thief = nil

			if SO_data.picked_up and SO_data.loot_unit and SO_data.loot_unit:carry_data() then
				SO_data.loot_unit:carry_data():unlink()
			end

			if alive(thief) then
				thief.brain(thief):set_objective(nil)
			end
		end
	end

	self._steal_SO_data = {}

	return 
end
ElementLootPile.on_pickup_SO_completed = function (self, loot_index, thief)
	if not self._steal_SO_data[loot_index] then
		return 
	end

	self._steal_SO_data[loot_index].picked_up = true
	local pos, rot = self.get_orientation(self)
	local unit = managers.player:server_drop_carry(self.value(self, "carry_id"), 1, true, false, 1, pos, rot, Vector3(0, 0, 0), 0, nil, nil)

	if alive(unit) and unit.carry_data(unit) then
		unit.carry_data(unit):link_to(thief)

		self._steal_SO_data[loot_index].loot_unit = unit
	end

	self._remaining_loot = self._remaining_loot - 1

	if 0 < self._remaining_loot then
		self.register_steal_SO(self)
	end

	return 
end
ElementLootPile.on_pickup_SO_failed = function (self, loot_index, thief)
	if not self._steal_SO_data[loot_index] then
		return 
	end

	self._steal_SO_data[loot_index] = nil

	if 0 < self._remaining_loot then
		self.register_steal_SO(self)
	end

	return 
end
ElementLootPile.on_secure_SO_completed = function (self, loot_index, thief)
	if not self._steal_SO_data[loot_index] then
		return 
	end

	local unit = self._steal_SO_data[loot_index].loot_unit

	if alive(unit) and unit.carry_data(unit) then
		unit.carry_data(unit):unlink()
	end

	managers.mission:call_global_event("loot_lost")

	self._steal_SO_data[loot_index] = nil

	return 
end
ElementLootPile.on_secure_SO_failed = function (self, loot_index, thief)
	if not self._steal_SO_data[loot_index] then
		return 
	end

	local unit = self._steal_SO_data[loot_index].loot_unit

	if alive(unit) and unit.carry_data(unit) then
		unit.carry_data(unit):unlink()
		unit.carry_data(unit):_chk_register_steal_SO()
	end

	self._steal_SO_data[loot_index] = nil

	return 
end
ElementLootPile.clbk_pickup_SO_verification = function (self, loot_index, candidate_unit)
	if not self._steal_SO_data[loot_index] or not self._steal_SO_data[loot_index].SO_id then
		return 
	end

	if candidate_unit.movement(candidate_unit):cool() then
		return 
	end

	local nav_seg = candidate_unit.movement(candidate_unit):nav_tracker():nav_segment()

	if not self._steal_SO_data[loot_index].pickup_area.nav_segs[nav_seg] then
		return 
	end

	if not candidate_unit.base(candidate_unit):char_tweak().steal_loot then
		return 
	end

	return true
end
ElementLootPile.on_pickup_SO_administered = function (self, loot_index, thief)
	if loot_index and self._steal_SO_data[loot_index] and not self._steal_SO_data[loot_index].thief then
		self._steal_SO_data[loot_index].SO_registered = false
	end

	return 
end

return 
