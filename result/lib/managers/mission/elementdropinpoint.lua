core:import("CoreMissionScriptElement")

ElementDropInPoint = ElementDropInPoint or class(CoreMissionScriptElement.MissionScriptElement)
ElementDropInPoint.on_script_activated = function (self)
	ElementDropInPoint.super.on_script_activated(self)
	self.on_set_enabled(self)

	return 
end
ElementDropInPoint.on_set_enabled = function (self)
	if self._values.enabled then
		managers.criminals:add_custom_drop_in_point(self.id(self), self._values.position, self._values.rotation)
	else
		managers.criminals:remove_custom_drop_in_point(self.id(self))
	end

	return 
end
ElementDropInPoint.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local peer = alive(instigator) and managers.network:session() and managers.network:session():peer_by_unit(instigator)

	if peer then
		managers.criminals:set_custom_drop_in_point_peer_id(self.id(self), peer.id(peer))
	end

	ElementDropInPoint.super.on_executed(self, instigator)

	return 
end

return 
