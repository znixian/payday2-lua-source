core:import("CoreMissionScriptElement")

ElementPickup = ElementPickup or class(CoreMissionScriptElement.MissionScriptElement)
ElementPickup.init = function (self, ...)
	ElementPickup.super.init(self, ...)

	return 
end
ElementPickup.client_on_executed = function (self, ...)
	return 
end
ElementPickup.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if alive(instigator) and instigator.character_damage(instigator) and not instigator.character_damage(instigator):dead() and instigator.character_damage(instigator).set_pickup then
		if self._values.pickup == "remove" then
			instigator.character_damage(instigator):set_pickup(nil)
		else
			instigator.character_damage(instigator):set_pickup(self._values.pickup)
		end
	else
		local reason = not alive(instigator) and "no instigator"
		reason = reason or (not instigator.character_damage(instigator) and "no character damage extension (wrong type of unit)")
		reason = reason or (instigator.character_damage(instigator):dead() and "instigator dead")
		reason = reason or (not instigator.character_damage(instigator).set_pickup and "No set_pickup function")

		debug_pause("Skipped pickup operation on an instigator!", self.editor_name(self), "Reason: " .. reason)
	end

	ElementPickup.super.on_executed(self, instigator)

	return 
end

return 
