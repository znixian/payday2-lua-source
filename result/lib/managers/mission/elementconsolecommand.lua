core:import("CoreMissionScriptElement")

ElementConsoleCommand = ElementConsoleCommand or class(CoreMissionScriptElement.MissionScriptElement)
ElementConsoleCommand.init = function (self, ...)
	ElementConsoleCommand.super.init(self, ...)

	return 
end
ElementConsoleCommand.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	Application:console_command("s " .. self._values.cmd)
	ElementConsoleCommand.super.on_executed(self, instigator)

	return 
end

return 
