core:import("CoreMissionScriptElement")

ElementPlayerNumberCheck = ElementPlayerNumberCheck or class(CoreMissionScriptElement.MissionScriptElement)
ElementPlayerNumberCheck.init = function (self, ...)
	ElementPlayerNumberCheck.super.init(self, ...)

	return 
end
ElementPlayerNumberCheck.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementPlayerNumberCheck.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local num_plrs = managers.network:session():amount_of_players()

	if not self._values["num" .. num_plrs] then
		return 
	end

	ElementPlayerNumberCheck.super.on_executed(self, instigator)

	return 
end

return 
