core:import("CoreMissionScriptElement")

ElementCheckDLC = ElementCheckDLC or class(CoreMissionScriptElement.MissionScriptElement)
ElementCheckDLC.init = function (self, ...)
	ElementCheckDLC.super.init(self, ...)

	return 
end
ElementCheckDLC.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local can_execute = nil

	if self._values.require_all then
		can_execute = self.check_all_dlcs_owned(self, self._values.dlc_ids)
	else
		can_execute = self.check_any_dlc_owned(self, self._values.dlc_ids)
	end

	if self._values.invert then
		can_execute = not can_execute
	end

	if can_execute then
		ElementCheckDLC.super.on_executed(self, instigator)
	end

	return 
end
ElementCheckDLC.client_on_executed = function (self, ...)
	return 
end
ElementCheckDLC.check_any_dlc_owned = function (self, dlc_list)
	for i, dlc in ipairs(dlc_list) do
		if managers.dlc:is_dlc_unlocked(dlc) then
			return true
		end
	end

	return false
end
ElementCheckDLC.check_all_dlcs_owned = function (self, dlc_list)
	for i, dlc in ipairs(dlc_list) do
		if not managers.dlc:is_dlc_unlocked(dlc) then
			return false
		end
	end

	return true
end

return 
