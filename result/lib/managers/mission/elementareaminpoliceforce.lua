core:import("CoreMissionScriptElement")

ElementAreaMinPoliceForce = ElementAreaMinPoliceForce or class(CoreMissionScriptElement.MissionScriptElement)
ElementAreaMinPoliceForce.init = function (self, ...)
	ElementAreaMinPoliceForce.super.init(self, ...)

	self._network_execute = true

	return 
end
ElementAreaMinPoliceForce.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self.operation_add(self)
	ElementAreaMinPoliceForce.super.on_executed(self, instigator)

	return 
end
ElementAreaMinPoliceForce.operation_add = function (self)
	managers.groupai:state():set_area_min_police_force(self._id, self._values.amount, self._values.position)

	return 
end
ElementAreaMinPoliceForce.operation_remove = function (self)
	managers.groupai:state():set_area_min_police_force(self._id)

	return 
end

return 
