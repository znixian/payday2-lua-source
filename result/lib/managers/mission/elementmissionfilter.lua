core:import("CoreMissionScriptElement")

ElementMissionFilter = ElementMissionFilter or class(CoreMissionScriptElement.MissionScriptElement)
ElementMissionFilter.init = function (self, ...)
	ElementMissionFilter.super.init(self, ...)

	return 
end
ElementMissionFilter.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementMissionFilter.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if not self._check_mission_filters(self) then
		return 
	end

	ElementMissionFilter.super.on_executed(self, instigator)

	return 
end
ElementMissionFilter._check_mission_filters = function (self)
	if self._values[1] and managers.mission:check_mission_filter(1) then
		return true
	end

	if self._values[2] and managers.mission:check_mission_filter(2) then
		return true
	end

	if self._values[3] and managers.mission:check_mission_filter(3) then
		return true
	end

	if self._values[4] and managers.mission:check_mission_filter(4) then
		return true
	end

	if self._values[5] and managers.mission:check_mission_filter(5) then
		return true
	end

	return false
end

return 
