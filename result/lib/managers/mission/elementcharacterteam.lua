core:import("CoreMissionScriptElement")

ElementCharacterTeam = ElementCharacterTeam or class(CoreMissionScriptElement.MissionScriptElement)
ElementCharacterTeam.init = function (self, ...)
	ElementCharacterTeam.super.init(self, ...)
	self._finalize_values(self)

	return 
end
ElementCharacterTeam._finalize_values = function (self)
	local values = self._values

	if values.team == "" then
		values.team = nil
	end

	if values.use_instigator or not next(values.elements) then
		values.elements = nil
	end

	return 
end
ElementCharacterTeam.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local function _set_unit_team_f(unit)
		managers.groupai:state():set_char_team(unit, self._values.team)

		return 
	end

	if self._values.use_instigator then
		if self._values.team and not instigator.character_damage(instigator):dead() then
			_set_unit_team_f(instigator)
		end
	elseif self._values.elements then
		for _, element_id in pairs(self._values.elements) do
			local element = managers.mission:get_element_by_id(element_id)

			if element.enabled(element) or self._values.ignore_disabled then
				local all_units = element.units(element)

				for _, unit in ipairs(all_units) do
					_set_unit_team_f(unit)
				end
			end
		end
	end

	ElementCharacterTeam.super.on_executed(self, instigator)

	return 
end

return 
