core:import("CoreMissionScriptElement")

ElementJobStageAlternative = ElementJobStageAlternative or class(CoreMissionScriptElement.MissionScriptElement)
ElementJobStageAlternative.init = function (self, ...)
	ElementJobStageAlternative.super.init(self, ...)

	return 
end
ElementJobStageAlternative.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementJobStageAlternative.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	print("ElementJobStageAlternative:on_executed", self._values.alternative, self._values.interupt)

	if self._values.interupt and self._values.interupt ~= "none" then
		managers.job:set_next_interupt_stage(self._values.interupt)
	elseif Network:is_server() then
		managers.job:set_next_alternative_stage(self._values.alternative)
	end

	ElementJobStageAlternative.super.on_executed(self, self._unit or instigator)

	return 
end

return 
