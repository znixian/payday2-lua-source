core:import("CoreMissionScriptElement")

ElementSpawnGageAssignment = ElementSpawnGageAssignment or class(CoreMissionScriptElement.MissionScriptElement)
ElementSpawnGageAssignment.init = function (self, ...)
	ElementSpawnGageAssignment.super.init(self, ...)

	return 
end
ElementSpawnGageAssignment.on_script_activated = function (self)
	return 
end
ElementSpawnGageAssignment.on_executed = function (self, instigator)
	if not self._values.enabled or not managers.gage_assignment then
		return 
	end

	local num_executions = tweak_data.gage_assignment:get_num_assignment_units() or 1

	for i = 1, num_executions, 1 do
		managers.gage_assignment:queue_spawn(self.get_orientation(self))
	end

	ElementSpawnGageAssignment.super.on_executed(self, instigator)

	return 
end

return 
