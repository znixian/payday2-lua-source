core:import("CoreMissionScriptElement")

ElementDangerZone = ElementDangerZone or class(CoreMissionScriptElement.MissionScriptElement)
ElementDangerZone.init = function (self, ...)
	ElementDangerZone.super.init(self, ...)

	return 
end
ElementDangerZone.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if alive(instigator) then
		if instigator == managers.player:player_unit() then
			instigator.character_damage(instigator):set_danger_level(self._values.level)
		else
			local rpc_params = {
				"dangerzone_set_level",
				self._values.level
			}

			instigator.network(instigator):send_to_unit(rpc_params)
		end
	end

	ElementDangerZone.super.on_executed(self, self._unit or instigator)

	return 
end

return 
