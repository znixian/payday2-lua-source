core:import("CoreMissionScriptElement")

ElementBlurZone = ElementBlurZone or class(CoreMissionScriptElement.MissionScriptElement)
ElementBlurZone.init = function (self, ...)
	ElementBlurZone.super.init(self, ...)

	return 
end
ElementBlurZone.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementBlurZone.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.mode == 0 then
		managers.environment_controller:set_blurzone(self._id, self._values.mode)
	else
		managers.environment_controller:set_blurzone(self._id, self._values.mode, self._values.position, self._values.radius, self._values.height)
	end

	ElementBlurZone.super.on_executed(self, instigator)

	return 
end
ElementBlurZone.operation_remove = function (self)
	managers.environment_controller:set_blurzone(self._id, -1)

	return 
end

return 
