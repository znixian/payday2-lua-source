core:import("CoreMissionScriptElement")

ElementObjective = ElementObjective or class(CoreMissionScriptElement.MissionScriptElement)
ElementObjective.init = function (self, ...)
	ElementObjective.super.init(self, ...)

	return 
end
ElementObjective.on_script_activated = function (self)
	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementObjective.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementObjective.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local objective = self.value(self, "objective")
	local amount = self.value(self, "amount")
	amount = (amount and 0 < amount and amount) or nil

	if objective ~= "none" then
		if self._values.state == "activate" then
			if self._values.countdown then
				managers.objectives:activate_objective_countdown(objective, nil, {
					amount = amount
				})
			else
				managers.objectives:activate_objective(objective, nil, {
					amount = amount
				})
			end
		elseif self._values.state == "complete_and_activate" then
			managers.objectives:complete_and_activate_objective(objective, nil, {
				amount = amount
			})
		elseif self._values.state == "complete" then
			if self._values.sub_objective and self._values.sub_objective ~= "none" then
				managers.objectives:complete_sub_objective(objective, self._values.sub_objective)
			elseif self._values.countdown then
				managers.objectives:complete_objective_countdown(objective)
			else
				managers.objectives:complete_objective(objective)
			end
		elseif self._values.state == "update" then
			managers.objectives:update_objective(objective)
		elseif self._values.state == "remove" then
			managers.objectives:remove_objective(objective)
		elseif self._values.state == "remove_and_activate" then
			managers.objectives:remove_and_activate_objective(objective, nil, {
				amount = amount
			})
		end
	elseif Application:editor() then
		managers.editor:output_error("Cant operate on objective " .. objective .. " in element " .. self._editor_name .. ".")
	end

	ElementObjective.super.on_executed(self, instigator)

	return 
end
ElementObjective.apply_job_value = function (self, amount)
	local type = CoreClass.type_name(amount)

	if type ~= "number" then
		Application:error("[ElementObjective:apply_job_value] " .. self._id .. "(" .. self._editor_name .. ") Can't apply job value of type " .. type)

		return 
	end

	self._values.amount = amount

	return 
end
ElementObjective.save = function (self, data)
	data.enabled = self._values.enabled

	return 
end
ElementObjective.load = function (self, data)
	self.set_enabled(self, data.enabled)

	return 
end

return 
