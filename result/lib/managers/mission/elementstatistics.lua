core:import("CoreMissionScriptElement")

ElementStatistics = ElementStatistics or class(CoreMissionScriptElement.MissionScriptElement)
ElementStatistics.init = function (self, ...)
	ElementStatistics.super.init(self, ...)

	return 
end
ElementStatistics.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.statistics:mission_stats(self._values.name)
	ElementStatistics.super.on_executed(self, instigator)

	return 
end
ElementStatistics.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end

return 
