core:import("CoreMissionScriptElement")

ElementAIForceAttentionOperator = ElementAIForceAttentionOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementAIForceAttentionOperator.init = function (self, ...)
	ElementAIForceAttentionOperator.super.init(self, ...)

	return 
end
ElementAIForceAttentionOperator.on_executed = function (self, instigator)
	if not self._values.enabled or Network:is_client() then
		return 
	end

	if not self._values.element_id then
		return 
	end

	if self._values.operation == "disable" then
		managers.groupai:state():set_force_attention(nil)
	else
		Application:error("[ElementAIForceAttentionOperator:on_executed] Operation not implemented:", self._values.operation)
	end

	ElementSpecialObjective.super.on_executed(self, instigator)

	return 
end

return 
