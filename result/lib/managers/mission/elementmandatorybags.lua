core:import("CoreMissionScriptElement")

ElementMandatoryBags = ElementMandatoryBags or class(CoreMissionScriptElement.MissionScriptElement)
ElementMandatoryBags.init = function (self, ...)
	ElementMandatoryBags.super.init(self, ...)

	return 
end
ElementMandatoryBags.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementMandatoryBags.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	print("ElementMandatoryBags:on_executed", self._values.carry_id, self._values.amount)
	managers.loot:set_mandatory_bags_data(self._values.carry_id, self._values.amount)
	ElementMandatoryBags.super.on_executed(self, instigator)

	return 
end

return 
