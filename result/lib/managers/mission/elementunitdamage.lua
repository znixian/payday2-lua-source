core:import("CoreMissionScriptElement")

ElementUnitDamage = ElementUnitDamage or class(CoreMissionScriptElement.MissionScriptElement)
ElementUnitDamage.init = function (self, ...)
	ElementUnitDamage.super.init(self, ...)

	self._units = {}
	local dmg_filter = self.value(self, "damage_types")

	if dmg_filter and dmg_filter ~= "" then
		self._allow_damage_types = {}
		local dmgs = string.split(dmg_filter, " ")

		for _, dmg_type in ipairs(dmgs) do
			table.insert(self._allow_damage_types, dmg_type)
		end
	end

	return 
end
ElementUnitDamage.destroy = function (self)
	return 
end
MissionScriptElement.on_created = function (self)
	return 
end
ElementUnitDamage.on_script_activated = function (self)
	local elementBroken = false

	for _, id in ipairs(self._values.unit_ids) do
		if Global.running_simulation then
			if not managers.editor:unit_with_id(id) then
				print("MISSING UNIT WITH ID:", id)

				elementBroken = true
			else
				self._load_unit(self, managers.editor:unit_with_id(id))
			end
		else
			local unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "_load_unit"))

			if unit then
				self._load_unit(self, unit)
			end
		end
	end

	if elementBroken then
		for _, id in ipairs(self._values.unit_ids) do
			if managers.editor:unit_with_id(id) then
				print(inspect(managers.editor:unit_with_id(id)))
			end
		end
	end

	self._has_fetched_units = true

	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementUnitDamage._load_unit = function (self, unit)
	if unit.damage then
		unit.damage(unit):add_listener("element_unit_damage" .. self._id, {
			"on_take_damage"
		}, callback(self, self, "clbk_linked_unit_took_damage"))
		table.insert(self._units, unit)
	end

	return 
end
ElementUnitDamage.clbk_linked_unit_took_damage = function (self, unit, attacker, damage_type, damage)
	self.on_executed(self, attacker, damage_type, damage)

	return 
end
ElementUnitDamage.on_executed = function (self, instigator, damage_type, damage)
	if not self._values.enabled then
		return 
	end

	local allow = true

	if self._allow_damage_types and not table.contains(self._allow_damage_types, damage_type) then
		allow = false
	end

	if allow then
		damage = math.floor(damage*tweak_data.gui.stats_present_multiplier)

		self.override_value_on_element_type(self, "ElementCounterOperator", "amount", damage)
		ElementUnitDamage.super.on_executed(self, instigator)
	end

	return 
end
ElementUnitDamage.client_on_executed = function (self, ...)
	return 
end
ElementUnitDamage.save = function (self, data)
	data.enabled = self._values.enabled

	return 
end
ElementUnitDamage.load = function (self, data)
	self.set_enabled(self, data.enabled)

	return 
end

return 
