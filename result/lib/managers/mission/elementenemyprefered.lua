core:import("CoreMissionScriptElement")

ElementEnemyPreferedAdd = ElementEnemyPreferedAdd or class(CoreMissionScriptElement.MissionScriptElement)
ElementEnemyPreferedAdd.init = function (self, ...)
	ElementEnemyPreferedAdd.super.init(self, ...)

	return 
end
ElementEnemyPreferedAdd.on_script_activated = function (self)
	self._values.spawn_points = self._values.spawn_points or self._values.elements

	if not self._values.spawn_points and not self._values.spawn_groups then
		return 
	end

	self._group_data = {}

	if self._values.spawn_points then
		self._group_data.spawn_points = {}

		for _, id in ipairs(self._values.spawn_points) do
			local element = self.get_mission_element(self, id)

			table.insert(self._group_data.spawn_points, element)
		end
	end

	if self._values.spawn_groups then
		self._group_data.spawn_groups = {}

		for _, id in ipairs(self._values.spawn_groups) do
			local element = self.get_mission_element(self, id)

			table.insert(self._group_data.spawn_groups, element)
		end
	end

	return 
end
ElementEnemyPreferedAdd.add = function (self)
	if not self._group_data then
		return 
	end

	if self._group_data.spawn_points then
		managers.groupai:state():add_preferred_spawn_points(self._id, self._group_data.spawn_points)
	end

	if self._group_data.spawn_groups then
		managers.groupai:state():add_preferred_spawn_groups(self._id, self._group_data.spawn_groups)
	end

	return 
end
ElementEnemyPreferedAdd.remove = function (self)
	managers.groupai:state():remove_preferred_spawn_points(self._id)

	return 
end
ElementEnemyPreferedAdd.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self.add(self)
	ElementEnemyPreferedAdd.super.on_executed(self, instigator)

	return 
end
ElementEnemyPreferedRemove = ElementEnemyPreferedRemove or class(CoreMissionScriptElement.MissionScriptElement)
ElementEnemyPreferedRemove.init = function (self, ...)
	ElementEnemyPreferedRemove.super.init(self, ...)

	return 
end
ElementEnemyPreferedRemove.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			element.remove(element)
		end
	end

	ElementEnemyPreferedRemove.super.on_executed(self, instigator)

	return 
end

return 
