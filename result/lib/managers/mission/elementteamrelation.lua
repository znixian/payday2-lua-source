core:import("CoreMissionScriptElement")

ElementTeamRelation = ElementTeamRelation or class(CoreMissionScriptElement.MissionScriptElement)
ElementTeamRelation.init = function (self, ...)
	ElementCharacterTeam.super.init(self, ...)

	return 
end
ElementTeamRelation.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_team_relation(self._values.team1, self._values.team2, self._values.relation, self._values.mutual)
	ElementTeamRelation.super.on_executed(self, instigator)

	return 
end

return 
