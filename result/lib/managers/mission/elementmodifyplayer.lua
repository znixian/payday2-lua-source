core:import("CoreMissionScriptElement")

ElementModifyPlayer = ElementModifyPlayer or class(CoreMissionScriptElement.MissionScriptElement)
ElementModifyPlayer.init = function (self, ...)
	ElementModifyPlayer.super.init(self, ...)

	return 
end
ElementModifyPlayer.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementModifyPlayer.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if alive(instigator) then
		instigator.character_damage(instigator):set_mission_damage_blockers("damage_fall_disabled", self._values.damage_fall_disabled)
		instigator.character_damage(instigator):set_mission_damage_blockers("invulnerable", self._values.invulnerable)
	end

	ElementModifyPlayer.super.on_executed(self, instigator)

	return 
end

return 
