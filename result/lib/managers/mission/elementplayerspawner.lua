core:import("CoreMissionScriptElement")

ElementPlayerSpawner = ElementPlayerSpawner or class(CoreMissionScriptElement.MissionScriptElement)
ElementPlayerSpawner.init = function (self, ...)
	ElementPlayerSpawner.super.init(self, ...)
	managers.player:preload()

	return 
end
ElementPlayerSpawner.value = function (self, name)
	return self._values[name]
end
ElementPlayerSpawner.client_on_executed = function (self, ...)
	if not self._values.enabled then
		return 
	end

	managers.player:set_player_state(self._values.state or managers.player:default_player_state())

	return 
end
ElementPlayerSpawner.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.player:set_player_state(self._values.state or managers.player:default_player_state())
	managers.groupai:state():on_player_spawn_state_set(self._values.state or managers.player:default_player_state())
	managers.network:register_spawn_point(self._id, {
		position = self._values.position,
		rotation = self._values.rotation
	})
	ElementPlayerSpawner.super.on_executed(self, self._unit or instigator)

	return 
end
ElementPlayerSpawner.execute_on_all_units = function (self, func)
	for _, data in ipairs(managers.criminals:characters()) do
		if alive(data.unit) then
			func(data.unit)
		end
	end

	return 
end

return 
