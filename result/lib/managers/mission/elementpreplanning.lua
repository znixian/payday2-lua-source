core:import("CoreMissionScriptElement")

ElementPrePlanning = ElementPrePlanning or class(CoreMissionScriptElement.MissionScriptElement)
ElementPrePlanning.init = function (self, ...)
	ElementPrePlanning.super.init(self, ...)

	return 
end
ElementPrePlanning.on_script_activated = function (self)
	self._mission_script:add_save_state_cb(self._id)

	if self._values.enabled then
		self._has_registered = true

		managers.preplanning:register_element(self)
	end

	return 
end
ElementPrePlanning.set_enabled = function (self, enabled)
	ElementPrePlanning.super.set_enabled(self, enabled)

	if enabled and not self._has_registered then
		self._has_registered = true

		managers.preplanning:register_element(self)
	elseif not enabled and self._has_registered then
		self._has_registered = nil

		managers.preplanning:unregister_element(self)
	end

	return 
end
ElementPrePlanning.on_executed = function (self, instigator, ...)
	if not self._values.enabled then
		return 
	end

	ElementPrePlanning.super.on_executed(self, instigator, ...)

	return 
end
ElementPrePlanning.save = function (self, data)
	data.enabled = self._values.enabled

	return 
end
ElementPrePlanning.load = function (self, data)
	self.set_enabled(self, data.enabled)

	return 
end

return 
