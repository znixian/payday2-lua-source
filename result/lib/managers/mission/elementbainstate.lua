core:import("CoreMissionScriptElement")

ElementBainState = ElementBainState or class(CoreMissionScriptElement.MissionScriptElement)
ElementBainState.init = function (self, ...)
	ElementBainState.super.init(self, ...)

	return 
end
ElementBainState.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementBainState.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.groupai:state():set_bain_state(self._values.state)
	ElementBainState.super.on_executed(self, instigator)

	return 
end

return 
