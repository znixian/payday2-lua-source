core:import("CoreMissionScriptElement")

ElementVariableSet = ElementVariableSet or class(CoreMissionScriptElement.MissionScriptElement)
ElementVariableSet.init = function (self, ...)
	ElementVariableSet.super.init(self, ...)

	return 
end
ElementVariableSet.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	managers.challenge:mission_set_value(self._values.variable, self._values.activated)
	ElementVariableSet.super.on_executed(self, instigator)

	return 
end
ElementVariableSet.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end

return 
