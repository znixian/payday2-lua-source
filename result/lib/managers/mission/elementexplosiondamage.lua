core:import("CoreMissionScriptElement")

ElementExplosionDamage = ElementExplosionDamage or class(CoreMissionScriptElement.MissionScriptElement)
ElementExplosionDamage.init = function (self, ...)
	ElementExplosionDamage.super.init(self, ...)

	return 
end
ElementExplosionDamage.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementExplosionDamage.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local player = managers.player:player_unit()

	if player then
		player.character_damage(player):damage_explosion({
			position = self._values.position,
			range = self._values.range,
			damage = self._values.damage
		})
	end

	ElementExplosionDamage.super.on_executed(self, instigator)

	return 
end

return 
