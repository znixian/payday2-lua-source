core:import("CoreMissionScriptElement")

ElementSpawnGrenade = ElementSpawnGrenade or class(CoreMissionScriptElement.MissionScriptElement)
ElementSpawnGrenade.init = function (self, ...)
	ElementSpawnGrenade.super.init(self, ...)

	return 
end
ElementSpawnGrenade.client_on_executed = function (self, ...)
	return 
end
ElementSpawnGrenade.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.grenade_type == "frag" then
		ProjectileBase.throw_projectile(1, self._values.position, self._values.spawn_dir*self._values.strength)
	end

	ElementSpawnGrenade.super.on_executed(self, instigator)

	return 
end

return 
