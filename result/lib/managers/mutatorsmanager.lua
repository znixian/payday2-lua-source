require("lib/mutators/BaseMutator")
require("lib/mutators/MutatorEnemyHealth")
require("lib/mutators/MutatorEnemyDamage")
require("lib/mutators/MutatorFriendlyFire")
require("lib/mutators/MutatorShotgunTweak")
require("lib/mutators/MutatorExplodingEnemies")
require("lib/mutators/MutatorHydra")
require("lib/mutators/MutatorEnemyReplacer")
require("lib/mutators/MutatorCloakerEffect")
require("lib/mutators/MutatorShieldDozers")

MutatorsManager = MutatorsManager or class()
MutatorsManager.package = "packages/toxic"
MutatorsManager._atlas_file = "guis/textures/pd2/mutator_icons_atlas"
MutatorsManager._icon_size = 128
MutatorsManager._options_icon_coord = {
	8,
	2
}
MutatorsManager.init = function (self)
	managers.mutators = self
	self._message_system = MessageSystem:new()
	self._lobby_delay = -1

	if not Global.mutators then
		Global.mutators = {
			mutator_values = {},
			active_on_load = {}
		}
	end

	self._mutators = {
		MutatorEnemyHealth:new(self),
		MutatorEnemyDamage:new(self),
		MutatorFriendlyFire:new(self),
		MutatorShotgunTweak:new(self),
		MutatorExplodingEnemies:new(self),
		MutatorHydra:new(self),
		MutatorEnemyReplacer:new(self),
		MutatorMediDozer:new(self),
		MutatorCloakerEffect:new(self),
		MutatorShieldDozers:new(self),
		MutatorTitandozers:new(self)
	}
	self._active_mutators = {}
	local activate = Global.mutators and Global.mutators.active_on_load

	if not self.can_mutators_be_active(self) then
		activate = false
	end

	if activate then
		for id, data in pairs(Global.mutators.active_on_load) do
			cat_print("jamwil", "[Mutators] Attempting to activate mutator: ", id)

			local mutator = self.get_mutator_from_id(self, id)

			if mutator then
				table.insert(self.active_mutators(self), {
					mutator = mutator
				})
				cat_print("jamwil", "[Mutators] Activated mutator: ", id)
			else
				cat_print("jamwil", "[Mutators] No mutator with id: ", id)
			end

			for key, value in pairs(data) do
				if Network:is_client() then
					mutator.set_host_value(mutator, key, value)
				end
			end
		end
	end

	local setup_mutators = {}

	for _, active_mutator in pairs(self.active_mutators(self)) do
		table.insert(setup_mutators, active_mutator.mutator)
	end

	table.sort(setup_mutators, function (a, b)
		return b.load_priority < a.load_priority
	end)

	for _, mutator in pairs(setup_mutators) do
		cat_print("jamwil", "[Mutators] Setting up active mutator: ", mutator.id(mutator))
		mutator.setup(mutator, self)
	end

	return 
end
MutatorsManager.update = function (self, t, dt)
	self.message_system(self):update(t, dt)

	for _, active_mutator in pairs(self.active_mutators(self)) do
		active_mutator.mutator:update(t, dt)
	end

	return 
end
MutatorsManager.save = function (self, data)
	local values = {}

	for i, mutator in ipairs(self.mutators(self)) do
		local mutator_values = {}

		for key, data in pairs(mutator.values(mutator)) do
			mutator_values[key] = data.current
		end

		values[mutator.id(mutator)] = mutator_values
	end

	local state = {
		save_values = values
	}
	data.Mutators = state

	return 
end
MutatorsManager.load = function (self, data, version)
	cat_print("jamwil", "[Mutators] Begin loading...")

	local state = data.Mutators or {}
	slot4 = pairs
	slot5 = state.save_values or {}

	for id, values in slot4(slot5) do
		cat_print("jamwil", "[Mutators] Loading KVs for mutator: ", id)

		local mutator = self.get_mutator_from_id(self, id)

		if mutator then
			for key, value in pairs(values) do
				cat_print("jamwil", "           >: ", key, value)
				mutator.set_value(mutator, key, value)
			end
		else
			cat_print("jamwil", "[Mutators]            > no mutator")
		end
	end

	cat_print("jamwil", "[Mutators] Loading finished!")

	return 
end
MutatorsManager.can_mutators_be_active = function (self)
	if Global.game_settings.gamemode ~= GamemodeStandard.id then
		return false
	end

	return true
end
MutatorsManager.are_mutators_active = function (self)
	if not self.can_mutators_be_active(self) then
		return false
	end

	if Network:is_server() then
		return 0 < #self.active_mutators(self)
	elseif Network:is_client() then
		return self.get_mutators_from_lobby_data(self)
	else
		return false
	end

	return 
end
MutatorsManager.are_mutators_enabled = function (self)
	if not self.can_mutators_be_active(self) then
		return false
	end

	if Network:is_client() then
		return self.are_mutators_active(self)
	else
		for i, mutator in ipairs(self.mutators(self)) do
			if mutator.is_enabled(mutator) then
				return true
			end
		end

		return false
	end

	return 
end
MutatorsManager.mutators = function (self)
	return self._mutators
end
MutatorsManager.active_mutators = function (self)
	return self._active_mutators
end
MutatorsManager.is_mutator_active = function (self, mutator)
	for _, active_mutator in pairs(self.active_mutators(self)) do
		if mutator._type == active_mutator.mutator:id() then
			return true
		end
	end

	return false
end
MutatorsManager.get_mutator = function (self, mutator_class)
	for i, mutator in pairs(self.mutators(self)) do
		if mutator_class._type == mutator.id(mutator) then
			return mutator
		end
	end

	return 
end
MutatorsManager.allow_mutators_in_level = function (self, level_id)
	local level_data = tweak_data.levels[level_id]

	if level_data and level_data.disable_mutators then
		return false
	end

	return true
end
MutatorsManager.globalize_active_mutators = function (self)
	Global.mutators.active_on_load = {}

	if not self.allow_mutators_in_level(self, Global.level_data.level_id) then
		return 
	end

	if Network:is_server() then
		for i, mutator in ipairs(self.mutators(self)) do
			if mutator.is_enabled(mutator) then
				local data = {}

				for key, value in pairs(mutator.values(mutator)) do
					data[key] = value
				end

				Global.mutators.active_on_load[mutator.id(mutator)] = data
			end
		end
	elseif Network:is_client() then
		slot1 = pairs
		slot2 = self.get_mutators_from_lobby_data(self) or {}

		for id, mutator_data in slot1(slot2) do
			local data = {}

			for key, value in pairs(mutator_data) do
				data[key] = value
			end

			Global.mutators.active_on_load[id] = data
		end
	end

	return 
end
MutatorsManager.clear_global_mutators = function (self)
	Global.mutators.active_on_load = {}

	return 
end
MutatorsManager.reset_all_mutators = function (self)
	for _, mutator in ipairs(self.mutators(self)) do
		self.set_enabled(self, mutator, false)
		mutator.clear_values(mutator)
	end

	if not Global.game_settings.single_player then
		self.update_lobby_info(self)
		MenuCallbackHandler:update_matchmake_attributes()
	end

	return 
end
MutatorsManager.get_mutator_from_id = function (self, id)
	for _, mutator in ipairs(self.mutators(self)) do
		if mutator.id(mutator) == id then
			return mutator
		end
	end

	return nil
end
MutatorsManager.can_enable_mutator = function (self, mutator)
	for _, imutator in ipairs(self.mutators(self)) do
		if imutator.id(imutator) ~= mutator.id(mutator) and imutator.is_enabled(imutator) and imutator.is_incompatible_with(imutator, mutator) then
			return false, imutator
		end
	end

	return true
end
MutatorsManager.set_enabled = function (self, mutator, enabled)
	if enabled == nil then
		enabled = true
	end

	if type(mutator) == "string" then
		mutator = self.get_mutator_from_id(self, mutator)
	end

	if not mutator then
		return false
	end

	if enabled and self.can_enable_mutator(self, mutator) then
		mutator.set_enabled(mutator, enabled)
	else
		mutator.set_enabled(mutator, false)
	end

	return 
end
MutatorsManager.categories = function (self)
	return {
		"all",
		"enemies",
		"gameplay"
	}
end
MutatorsManager.message_system = function (self)
	return self._message_system
end
MutatorsManager.notify = function (self, message, ...)
	self._message_system:notify(message, nil, ...)

	return 
end
MutatorsManager.register_message = function (self, message, uid, func)
	self._message_system:register(message, uid, func)

	return 
end
MutatorsManager.unregister_message = function (self, message, uid)
	self._message_system:unregister(message, uid)

	return 
end
MutatorsManager._get_reduction = function (self, func)
	local max_reduction = 0

	for _, mutator in pairs(self.mutators(self)) do
		if mutator.is_enabled(mutator) or mutator.is_active(mutator) then
			max_reduction = math.max(max_reduction, mutator[func](mutator))
		end
	end

	return max_reduction
end
MutatorsManager.get_cash_multiplier = function (self)
	return self._get_reduction(self, "get_cash_reduction") - 1
end
MutatorsManager.get_cash_reduction = function (self)
	return self._get_reduction(self, "get_cash_reduction")
end
MutatorsManager.get_experience_multiplier = function (self)
	return self._get_reduction(self, "get_experience_reduction") - 1
end
MutatorsManager.get_experience_reduction = function (self)
	return self._get_reduction(self, "get_experience_reduction")
end
MutatorsManager.are_achievements_disabled = function (self)
	if game_state_machine:current_state_name() ~= "menu_main" then
		for _, mutator in pairs(self.mutators(self)) do
			if mutator.is_active(mutator) and mutator.disables_achievements then
				return true
			end
		end
	else
		return false
	end

	return 
end
MutatorsManager.are_challenges_disabled = function (self)
	return self.are_achievements_disabled(self)
end
MutatorsManager.are_trophies_disabled = function (self)
	return self.are_achievements_disabled(self)
end
MutatorsManager.should_disable_statistics = function (self)
	return 0 < self.get_cash_reduction(self) or 0 < self.get_experience_reduction(self)
end
MutatorsManager.delay_lobby_time = function (self)
	return 16
end
MutatorsManager.lobby_delay = function (self)
	return self._lobby_delay or 0
end
MutatorsManager.set_lobby_delay = function (self, delay)
	print("[Mutators] Delaying lobby start by ", delay)

	self._lobby_delay = TimerManager:main():time() + delay

	return 
end
MutatorsManager.should_delay_game_start = function (self)
	if BaseNetworkHandler._verify_gamestate(BaseNetworkHandler._gamestate_filter.any_ingame) then
		return false
	end

	local peers_table = managers.network and managers.network:session() and managers.network:session():peers()

	if table.size(peers_table or {}) == 0 then
		return false
	end

	if not self.are_mutators_enabled(self) then
		return false
	end

	if not self._check_all_peers_are_ready(self) then
		return true
	end

	return 0 < self.lobby_delay(self) - TimerManager:main():time()
end
MutatorsManager.use_start_the_game_initial_delay = function (self)
	if not self._used_start_game_delay then
		self.set_lobby_delay(self, self.delay_lobby_time(self))

		self._used_start_game_delay = true
	end

	return 
end
MutatorsManager.start_the_game_countdown_cancelled = function (self)
	self._used_start_game_delay = nil

	return 
end
MutatorsManager._run_func = function (self, func_name, ...)
	for i, active_mutator in pairs(self.active_mutators(self)) do
		local mutator = active_mutator.mutator

		if mutator and mutator[func_name] then
			mutator[func_name](mutator, ...)
		end
	end

	return 
end
MutatorsManager.modify_character_tweak_data = function (self, character_tweak)
	self._run_func(self, "modify_character_tweak_data", character_tweak)

	return 
end
MutatorsManager.modify_tweak_data = function (self, id, value)
	self._run_func(self, "modify_tweak_data", id, value)

	return 
end
MutatorsManager.modify_value = function (self, id, value)
	for i, active_mutator in pairs(self.active_mutators(self)) do
		local mutator = active_mutator.mutator

		if mutator and mutator.modify_value then
			local new_value, override = mutator.modify_value(mutator, id, value)

			if new_value ~= nil or override then
				value = new_value
			end
		end
	end

	return value
end
MutatorsManager.update_lobby_info = function (self)
	print("[Mutators] Updating lobby info...")

	if Network:is_server() and self.are_mutators_enabled(self) and 0 < table.size(managers.network:session():peers()) then
		self.send_mutators_notification_to_clients(self, 0)

		if not self._check_all_peers_are_ready(self) then
			self.set_lobby_delay(self, self.delay_lobby_time(self))
		end
	end

	return 
end
MutatorsManager.apply_matchmake_attributes = function (self, lobby_attributes)
	print("[Mutators] Applying lobby attributes...")

	local count = 0

	for i, mutator in ipairs(self.mutators(self)) do
		if mutator.is_enabled(mutator) then
			count = count + 1
			lobby_attributes["mutator_" .. tostring(count)] = mutator.build_matchmaking_key(mutator)
		end
	end

	lobby_attributes.mutators = count

	return 
end
MutatorsManager.send_mutators_notification_to_clients = function (self, countdown)
	for i, peer in pairs(managers.network:session():peers()) do
		if not self.has_peer_been_notified(self, peer.id(peer)) then
			managers.network:session():send_to_peer(peer, "sync_mutators_launch", countdown or 0)
			self.set_peer_notified(self, peer.id(peer), true)
		end
	end

	return 
end
MutatorsManager.get_mutators_from_lobby_data = function (self)
	if not managers.network or not managers.network.matchmake or not managers.network.matchmake.lobby_handler then
		return false
	end

	local lobby_data = managers.network.matchmake.lobby_handler:get_lobby_data()

	if not lobby_data then
		return false
	end

	local function func(key)
		return lobby_data[key]
	end

	return self._get_mutators_data(self, func)
end
MutatorsManager.get_mutators_from_lobby = function (self, lobby)
	if not lobby then
		return false
	end

	local function func(key)
		return lobby:key_value(key)
	end

	return self._get_mutators_data(self, func)
end
MutatorsManager.set_crimenet_lobby_data = function (self, lobby_data)
	self._crimenet_lobby_data = lobby_data

	return 
end
MutatorsManager.crimenet_lobby_data = function (self)
	return self._crimenet_lobby_data
end
MutatorsManager._get_mutators_data = function (self, get_data_func)
	local num_mutators = 0
	local mutators_kv = get_data_func("mutators")

	if mutators_kv ~= "value_missing" and mutators_kv ~= "value_pending" then
		num_mutators = tonumber(mutators_kv)
	end

	if 0 < num_mutators then
		local mutators_strs = {}

		for i = 1, num_mutators, 1 do
			local mutator_str = get_data_func("mutator_" .. tostring(i))

			if mutator_str then
				table.insert(mutators_strs, mutator_str)
			end
		end

		return self._parse_mutator_strings(self, unpack(mutators_strs))
	else
		return false
	end

	return 
end
MutatorsManager._parse_mutator_strings = function (self, ...)
	local mutators_list = {}

	for i, str in ipairs({
		...
	}) do
		local splits = string.split(str, "[ ]")

		if splits then
			local mutator = self.get_mutator_from_id(self, splits[1])

			if mutator then
				table.remove(splits, 1)

				local data = mutator.get_data_from_attribute_string(mutator, splits)

				if data then
					mutators_list[mutator.id(mutator)] = data
				end
			end
		end
	end

	return mutators_list
end
MutatorsManager.show_mutators_launch_countdown = function (self, countdown)
	if Network:is_server() then
		return 
	end

	if countdown ~= nil and 0 < countdown then
		local dialog_data = {
			title = managers.localization:text("dialog_warning_title"),
			text = managers.localization:text("dialog_mutators_active_text"),
			id = "mutators_warning"
		}
		local yes_button = {
			text = managers.localization:text("dialog_yes"),
			callback_func = callback(self, self, "_dialog_mutators_accept")
		}
		local no_button = {
			text = managers.localization:text("dialog_leave_lobby"),
			callback_func = callback(self, self, "_dialog_mutators_decline"),
			cancel_button = true
		}
		dialog_data.button_list = {
			yes_button,
			no_button
		}

		managers.system_menu:show(dialog_data)
	else
		local dialog_data = {
			title = managers.localization:text("dialog_warning_title"),
			text = managers.localization:text("dialog_mutators_active_text"),
			id = "mutators_warning"
		}
		local yes_button = {
			text = managers.localization:text("dialog_yes"),
			callback_func = callback(self, self, "_dialog_mutators_accept")
		}
		local no_button = {
			text = managers.localization:text("dialog_leave_lobby"),
			callback_func = callback(self, self, "_dialog_mutators_decline"),
			cancel_button = true
		}
		dialog_data.button_list = {
			yes_button,
			no_button
		}

		managers.system_menu:show(dialog_data)
	end

	return 
end
MutatorsManager._dialog_mutators_accept = function (self)
	managers.network:session():send_to_host("sync_mutators_launch_ready", managers.network:session():local_peer():id(), true)

	return 
end
MutatorsManager._dialog_mutators_decline = function (self)
	MenuCallbackHandler:_dialog_leave_lobby_yes()

	return 
end
MutatorsManager.set_peer_notified = function (self, peer_id, is_notified)
	Global.mutators._peers_notified = Global.mutators._peers_notified or {}
	Global.mutators._peers_notified[peer_id] = is_notified

	return 
end
MutatorsManager.has_peer_been_notified = function (self, peer_id)
	if Global.mutators._peers_notified then
		return Global.mutators._peers_notified[peer_id] or false
	end

	return false
end
MutatorsManager.set_peer_is_ready = function (self, peer_id, is_ready, disable_check)
	Global.mutators._peers_ready = Global.mutators._peers_ready or {}
	Global.mutators._peers_ready[peer_id] = is_ready

	if not disable_check and self._check_all_peers_are_ready(self) then
		self.set_lobby_delay(self, math.min(self.lobby_delay(self), 3))
	end

	return 
end
MutatorsManager.is_peer_ready = function (self, peer_id)
	if Global.mutators._peers_ready then
		return Global.mutators._peers_ready[peer_id] or false
	end

	return false
end
MutatorsManager.force_all_ready = function (self)
	for i, peer in pairs(managers.network:session():peers()) do
		self.set_peer_notified(self, peer.id(peer), true)
		self.set_peer_is_ready(self, peer.id(peer), true, true)
	end

	return 
end
MutatorsManager._check_all_peers_are_ready = function (self)
	for i, peer in pairs(managers.network:session():peers()) do
		if not self.has_peer_been_notified(self, peer.id(peer)) or not self.is_peer_ready(self, peer.id(peer)) then
			return false
		end
	end

	return true
end
MutatorsManager.on_peer_added = function (self, peer, peer_id)
	if self.are_mutators_active(self) or self.are_mutators_enabled(self) then
		self._used_start_game_delay = nil

		if game_state_machine:current_state_name() ~= "menu_main" then
			self.set_peer_notified(self, peer_id, true)
			self.set_peer_is_ready(self, peer_id, true)
		end
	end

	return 
end
MutatorsManager.on_peer_removed = function (self, peer, peer_id, reason)
	self.set_peer_notified(self, peer_id, false)
	self.set_peer_is_ready(self, peer_id, false)

	if managers.menu and managers.menu:active_menu() and managers.menu:active_menu().renderer and managers.menu:active_menu().renderer:active_node_gui() and managers.menu:active_menu().renderer:active_node_gui().name == "start_the_game_countdown" then
		managers.menu:back()
		managers.menu:open_node("start_the_game_countdown")
	end

	return 
end
MutatorsManager.on_lobby_left = function (self)
	Global.mutators._peers_notified = nil
	Global.mutators._peers_ready = nil

	return 
end
MutatorsManager.check_achievements = function (self, achievement_data)
	if not achievement_data.mutators then
		return not self.are_achievements_disabled(self)
	end

	if achievement_data.mutators == true then
		return managers.mutators:are_mutators_active()
	elseif type(achievement_data.mutators) == "number" then
		return achievement_data.mutators <= table.size(managers.mutators:active_mutators())
	elseif #achievement_data.mutators == table.size(managers.mutators:active_mutators()) then
		local required_mutators = deep_clone(achievement_data.mutators)

		for _, active_mutator in pairs(managers.mutators:active_mutators()) do
			if table.contains(required_mutators, active_mutator.mutator:id()) then
				table.delete(required_mutators, active_mutator.mutator:id())
			end
		end

		for i = #required_mutators, 1, -1 do
			local mutator_data = required_mutators[i]

			for _, active_mutator in pairs(managers.mutators:active_mutators()) do
				if mutator_data.id == active_mutator.mutator:id() then
					local all_values = true

					for key, value in pairs(mutator_data) do
						if key ~= "id" and active_mutator.mutator:value(key) ~= value then
							all_values = false

							break
						end
					end

					if all_values then
						table.remove(required_mutators, i)
					end

					break
				end
			end
		end

		return #required_mutators == 0
	end

	return 
end

return 
