SyncManager = SyncManager or class()
SyncManager.init = function (self)
	self._units = {}
	self._synced_units = {}

	return 
end
SyncManager.add_managed_unit = function (self, unit_id, script)
	self._units[unit_id] = script

	return 
end
SyncManager.get_managed_unit = function (self, id)
	return self._units[id]
end
SyncManager.add_synced_outfit_unit = function (self, unit_id, type, data_string)
	self._synced_units[unit_id] = {
		type = type,
		data = data_string
	}

	managers.network:session():send_to_peers_synched("sync_synced_unit_outfit", unit_id, type, data_string)

	return 
end
SyncManager.remove_synced_outfit_unit = function (self, unit_id)
	self._synced_units[unit_id] = nil

	return 
end
SyncManager.resync_all = function (self)
	for _, peer in pairs(managers.network:session():peers()) do
		self.send_all_synced_units_to(self, peer)
	end

	return 
end
SyncManager.send_all_synced_units_to = function (self, peer)
	if type(peer) == "number" then
		local find_peer = peer
		peer = nil

		for _, m_peer in pairs(managers.network:session():peers()) do
			if m_peer.id(m_peer) == find_peer then
				peer = m_peer

				break
			end
		end
	end

	if peer then
		for id, data in pairs(self._synced_units) do
			managers.network:session():send_to_peer(peer, "sync_synced_unit_outfit", id, data.type, data.data)
		end
	end

	return 
end
SyncManager.sync_functions = {
	weapon = "handle_synced_weapon_blueprint",
	vault_cash = "handle_synced_vault_cash",
	offshore_gui = "handle_synced_offshore_gui",
	mask = "handle_synced_mask_blueprint"
}
SyncManager.on_received_synced_outfit = function (self, unit_id, type, outfit_string)
	print("[SyncManager] received synced blueprint", type, outfit_string)

	local callback = self[SyncManager.sync_functions[type]]

	if callback then
		callback(self, unit_id, outfit_string)
	else
		Application:error(string.format("[SyncManager] Received invalid outfit type '%s'", tostring(type)))
	end

	return 
end
SyncManager.add_synced_weapon_blueprint = function (self, unit_id, factory_id, blueprint)
	local blueprint_string = managers.weapon_factory:blueprint_to_string(factory_id, blueprint)
	blueprint_string = factory_id .. " " .. blueprint_string

	self.add_synced_outfit_unit(self, unit_id, "weapon", blueprint_string)
	print("[SyncManager] added synced weapon:", unit_id, blueprint_string)

	return 
end
SyncManager.handle_synced_weapon_blueprint = function (self, unit_id, data_string)
	local unit_element = self.get_managed_unit(self, unit_id)

	if unit_element then
		local data = string.split(data_string or "", " ")

		if data then
			local weapon_id = data[1]
			local blueprint_string = string.gsub(data_string, data[1] .. " ", "")
			local blueprint = managers.weapon_factory:unpack_blueprint_from_string(weapon_id, blueprint_string)

			if weapon_id and blueprint then
				unit_element.assemble_weapon(unit_element, weapon_id, blueprint)
			end
		end
	end

	return 
end
SyncManager.add_synced_mask_blueprint = function (self, unit_id, mask_id, blueprint)
	local blueprint_string = string.format("%s %s %s %s", tostring(mask_id), tostring(blueprint.color.id), tostring(blueprint.pattern.id), tostring(blueprint.material.id))

	self.add_synced_outfit_unit(self, unit_id, "mask", blueprint_string)
	print("[SyncManager] added synced mask: ", unit_id, blueprint_string)

	return 
end
SyncManager.handle_synced_mask_blueprint = function (self, unit_id, data_string)
	local unit_element = self.get_managed_unit(self, unit_id)

	if unit_element then
		local mask_id, blueprint = self._get_mask_id_and_blueprint(self, data_string)

		unit_element.assemble_mask(unit_element, mask_id, blueprint)
	end

	return 
end
SyncManager._get_mask_id_and_blueprint = function (self, data_string)
	local data = string.split(data_string or "", " ")
	local mask_id = data[1]
	local blueprint = {
		color = {
			id = data[2] or "nothing"
		},
		pattern = {
			id = data[3] or "no_color_no_material"
		},
		material = {
			id = data[4] or "plastic"
		}
	}

	return mask_id, blueprint
end
SyncManager.add_synced_offshore_gui = function (self, unit_id, visibility, displayed_cash)
	local blueprint = string.format("%s %s", tostring(visibility), tostring(displayed_cash))

	self.add_synced_outfit_unit(self, unit_id, "offshore_gui", blueprint)
	print("[SyncManager] added synced offshore: ", unit_id, blueprint)

	return 
end
SyncManager.handle_synced_offshore_gui = function (self, unit_id, data_string)
	local data = string.split(data_string, " ")
	local unit_element = self.get_managed_unit(self, unit_id)

	if unit_element then
		unit_element.set_visible(unit_element, toboolean(data[1]))
		unit_element.update_offshore(unit_element, tonumber(data[2]))
	end

	return 
end
SyncManager.add_synced_vault_cash = function (self, unit_id, tier)
	local blueprint = string.format("%s", tostring(tier))

	self.add_synced_outfit_unit(self, unit_id, "vault_cash", blueprint)
	print("[SyncManager] added synced vault: ", unit_id, blueprint)

	return 
end
SyncManager.handle_synced_vault_cash = function (self, unit_id, data_string)
	local target_tier = tonumber(data_string)
	local unit_element = self.get_managed_unit(self, unit_id)

	if unit_element and target_tier then
		unit_element.set_active_tier(unit_element, target_tier)
	end

	return 
end

return 
