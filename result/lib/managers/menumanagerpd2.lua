core:import("CoreMenuManager")
core:import("CoreMenuCallbackHandler")
require("lib/managers/menu/MenuSceneManager")
require("lib/managers/menu/MenuComponentManager")
require("lib/managers/menu/items/MenuItemExpand")
require("lib/managers/menu/items/MenuItemDivider")
require("lib/managers/menu/items/MenuItemColoredDivider")
require("lib/managers/menu/MenuInitiatorBase")
core:import("CoreEvent")

MenuManager.update = function (self, t, dt, ...)
	MenuManager.super.update(self, t, dt, ...)

	if managers.menu_scene then
		managers.menu_scene:update(t, dt)
	end

	managers.menu_component:update(t, dt)

	return 
end
MenuManager.on_view_character = function (self, user)
	local outfit = user.rich_presence(user, "outfit")

	if outfit ~= "" then
		if managers.menu:active_menu().logic:selected_node_name() ~= "view_character" then
			managers.menu:active_menu().logic:select_node("view_character", true, {})
		end

		managers.menu_scene:set_main_character_outfit(outfit)
		managers.menu_component:create_view_character_profile_gui(user, 0, 300)
	end

	return 
end
MenuManager.on_enter_lobby = function (self)
	print("function MenuManager:on_enter_lobby()")

	if game_state_machine:gamemode().id == GamemodeCrimeSpree.id then
		managers.menu:active_menu().logic:select_node("crime_spree_lobby", true, {})
	else
		managers.menu:active_menu().logic:select_node("lobby", true, {})
	end

	managers.platform:set_rich_presence("MPLobby")
	managers.menu_component:pre_set_game_chat_leftbottom(0, 50)
	managers.network:session():on_entered_lobby()
	self.setup_local_lobby_character(self)
	managers.tango:attempt_announce_tango_weapon()
	managers.crime_spree:on_entered_lobby()

	if Global.exe_argument_level or self._lobby_autoplay then
		MenuCallbackHandler:start_the_game()
	end

	return 
end
MenuManager.on_leave_active_job = function (self)
	managers.statistics:stop_session({
		quit = true
	})
	managers.savefile:save_progress()
	managers.job:deactivate_current_job()
	managers.gage_assignment:deactivate_assignments()

	if managers.groupai then
		managers.groupai:state():set_AI_enabled(false)
	end

	self._sound_source:post_event("menu_exit")
	managers.menu:close_menu("lobby_menu")
	managers.menu:close_menu("menu_pause")

	return 
end
MenuManager.set_lobby_autoplay = function (self, autoplay)
	self._lobby_autoplay = autoplay

	return 
end
MenuManager.setup_local_lobby_character = function (self)
	local local_peer = managers.network:session():local_peer()
	local level = managers.experience:current_level()
	local rank = managers.experience:current_rank()
	local character = local_peer.character(local_peer)
	local progress = managers.upgrades:progress()

	if managers.menu_scene and not Global.game_settings.single_player then
		managers.menu_scene:set_lobby_character_out_fit(local_peer.id(local_peer), managers.blackmarket:outfit_string(), rank)
	end

	local_peer.set_outfit_string(local_peer, managers.blackmarket:outfit_string())
	managers.network:session():send_to_peers_loaded("sync_profile", level, rank)
	managers.network:session():check_send_outfit()

	return 
end
MenuManager.set_cash_safe_scene_done = function (self, done, silent)
	self._cash_safe_scene_done = done

	if not silent then
		local logic = managers.menu:active_menu().logic

		if logic then
			logic.refresh_node(logic)
		end
	end

	return 
end
MenuManager.cash_safe_scene_done = function (self)
	return self._cash_safe_scene_done
end
MenuManager.http_test = function (self)
	Steam:http_request("http://www.overkillsoftware.com/?feed=rss", callback(self, self, "http_test_result"))

	return 
end
MenuManager.http_test_result = function (self, success, body)
	print("success", success)
	print("body", body)
	print(inspect(self._get_text_block(self, body, "<title>", "</title>")))
	print(inspect(self._get_text_block(self, body, "<link>", "</link>")))

	return 
end
MenuCallbackHandler.continue_to_lobby = function (self)
	return 
end
MenuCallbackHandler.on_view_character_focus = function (self, node, in_focus, data)
	if not in_focus or not data or false then
		managers.menu_scene:set_main_character_outfit(managers.blackmarket:outfit_string())
		managers.menu_component:close_view_character_profile_gui()
	end

	return 
end
MenuCallbackHandler.on_character_customization = function (self)
	managers.menu_component:close_weapon_box()

	return 
end
MenuCallbackHandler.start_job = function (self, job_data)
	if not managers.job:activate_job(job_data.job_id) then
		return 
	end

	Global.game_settings.level_id = managers.job:current_level_id()
	Global.game_settings.mission = managers.job:current_mission()
	Global.game_settings.world_setting = managers.job:current_world_setting()
	Global.game_settings.difficulty = job_data.difficulty

	if managers.platform then
		managers.platform:update_discord_heist()
	end

	local matchmake_attributes = self.get_matchmake_attributes(self)

	if Network:is_server() then
		local job_id_index = tweak_data.narrative:get_index_from_job_id(managers.job:current_job_id())
		local level_id_index = tweak_data.levels:get_index_from_level_id(Global.game_settings.level_id)
		local difficulty_index = tweak_data:difficulty_to_index(Global.game_settings.difficulty)

		managers.network:session():send_to_peers("sync_game_settings", job_id_index, level_id_index, difficulty_index)
		managers.network.matchmake:set_server_attributes(matchmake_attributes)
		managers.mutators:update_lobby_info()
		managers.menu_component:on_job_updated()
		managers.menu:open_node("lobby")
		managers.menu:active_menu().logic:refresh_node("lobby", true)
	else
		managers.network.matchmake:create_lobby(matchmake_attributes)
	end

	return 
end
MenuCallbackHandler.play_single_player_job = function (self, item)
	self.play_single_player(self)
	self.start_single_player_job(self, {
		difficulty = "normal",
		job_id = item.parameter(item, "job_id")
	})

	return 
end
MenuCallbackHandler.play_quick_start_job = function (self, item)
	self.start_job(self, {
		difficulty = "normal",
		job_id = item.parameter(item, "job_id")
	})

	return 
end
MenuCallbackHandler.start_single_player_job = function (self, job_data)
	if not managers.job:activate_job(job_data.job_id) then
		return 
	end

	Global.game_settings.level_id = managers.job:current_level_id()
	Global.game_settings.mission = managers.job:current_mission()
	Global.game_settings.difficulty = job_data.difficulty
	Global.game_settings.world_setting = managers.job:current_world_setting()

	if managers.platform then
		managers.platform:update_discord_heist()
	end

	MenuCallbackHandler:start_the_game()

	return 
end
MenuCallbackHandler.crimenet_focus_changed = function (self, node, in_focus)
	if in_focus then
		if node.parameters(node).no_servers then
			managers.crimenet:start_no_servers()
		else
			managers.crimenet:start()
		end

		managers.menu_component:create_crimenet_gui()
	else
		managers.crimenet:stop()
		managers.menu_component:close_crimenet_gui()
	end

	return 
end
MenuCallbackHandler.can_buy_weapon = function (self, item)
	return not Global.blackmarket_manager.weapons[item.parameter(item, "weapon_id")].owned
end
MenuCallbackHandler.owns_weapon = function (self, item)
	return not self.can_buy_weapon(self, item)
end
MenuCallbackHandler.open_blackmarket_node = function (self)
	managers.menu:active_menu().logic:select_node("blackmarket")

	return 
end
MenuCallbackHandler.leave_blackmarket = function (self, item)
	self._leave_blackmarket(self)

	if managers.blackmarket:remove_all_new_drop() then
		managers.savefile:save_progress()
	end

	return 
end
MenuCallbackHandler._leave_blackmarket = function (self)
	managers.menu_component:close_weapon_box()
	managers.menu_scene:remove_item()
	managers.menu_scene:delete_workbench_room()
	managers.blackmarket:release_preloaded_blueprints()

	return 
end
MenuCallbackHandler._left_blackmarket = function (self)
	managers.menu_scene:remove_item()

	return 
end
MenuCallbackHandler.blackmarket_abort_customize_mask = function (self)
	managers.blackmarket:abort_customize_mask()

	return 
end
MenuCallbackHandler.got_skillpoint_to_spend = function (self)
	return managers.skilltree and 0 < managers.skilltree:points()
end
MenuCallbackHandler.got_new_lootdrop = function (self)
	return managers.blackmarket and managers.blackmarket:got_any_new_drop()
end
MenuCallbackHandler.not_completed_all_story_assignments = function (self)
	local current = managers.story:current_mission() or {}

	return not current.last_mission or not current.rewarded
end
MenuCallbackHandler.got_completed_story_mission = function (self)
	local current = managers.story:current_mission() or {}

	return current.completed and not current.rewarded
end
MenuCallbackHandler.show_custom_safehouse_menu_icon = function (self)
	return managers.custom_safehouse:is_daily_new() or (managers.custom_safehouse:has_completed_daily() and not managers.custom_safehouse:has_rewarded_daily())
end
MenuCallbackHandler.close_custom_safehouse_menu = function (self)
	managers.custom_safehouse:disable_in_game_menu()

	return 
end
MenuCallbackHandler.should_show_old_safehouse = function (self)
	return false
	return not managers.custom_safehouse:unlocked()
end
MenuCallbackHandler.got_new_content_update = function (self)
	return false
end
MenuCallbackHandler.got_new_fav_videos = function (self)
	return false
end
MenuCallbackHandler.not_got_new_content_update = function (self)
	return not self.got_new_content_update(self)
end
MenuCallbackHandler.do_content_lootdrop = function (self, node)
	managers.menu:open_node("crimenet_contract_casino_lootdrop", {
		increase_infamous = false,
		secure_cards = 0
	})

	return 
end
MenuCallbackHandler.test_clicked_weapon = function (self, item)
	if not item.parameter(item, "customize") then
		managers.menu_scene:clicked_blackmarket_item()
		managers.menu_component:create_weapon_box(item.parameter(item, "weapon_id"), {
			condition = math.round(item.parameter(item, "condition")/item._max_condition(item)*100)
		})
	end

	return 
end
MenuCallbackHandler.buy_weapon = function (self, item)
	local name = managers.localization:text(tweak_data.weapon[item.parameter(item, "weapon_id")].name_id)
	local cost = 50000
	local yes_func = callback(self, self, "on_buy_weapon_yes", {
		item = item,
		cost = cost
	})

	managers.menu:show_buy_weapon({
		yes_func = yes_func
	}, name, "$" .. cost)

	return 
end
MenuCallbackHandler.on_buy_weapon_yes = function (self, params)
	Global.blackmarket_manager.weapons[params.item:parameter("weapon_id")].owned = true
	params.item:parameter("parent_item"):parameters().owned = true

	params.item:dirty()
	params.item:parameters().parent_item:on_buy(params.item:parameters().gui_node)

	return 
end
MenuCallbackHandler.equip_weapon = function (self, item)
	Global.player_manager.kit.weapon_slots[item.parameter(item, "weapon_slot")] = item.parameter(item, "weapon_id")

	for weapon_id, data in pairs(Global.blackmarket_manager.weapons) do
		if data.selection_index == item.parameter(item, "weapon_slot") then
			data.equipped = weapon_id == item.parameter(item, "weapon_id")
		end
	end

	return 
end
MenuCallbackHandler.repair_weapon = function (self, item)
	if item._at_max_condition(item) then
		return 
	end

	local name = managers.localization:text(tweak_data.weapon[item.parameter(item, "weapon_id")].name_id)
	local cost = (item.parameter(item, "parent_item"):condition()/item._max_condition(item) - 1)*50000
	local yes_func = callback(self, self, "on_repair_yes", {
		item = item,
		cost = cost
	})

	managers.menu:show_repair_weapon({
		yes_func = yes_func
	}, name, "$" .. cost)

	return 
end
MenuCallbackHandler.on_repair_yes = function (self, params)
	Global.blackmarket_manager.weapons[params.item:parameters().weapon_id].condition = params.item:_max_condition()

	params.item:dirty()
	self.test_clicked_weapon(self, params.item:parameters().parent_item)

	return 
end
MenuCallbackHandler.clicked_weapon_upgrade_type = function (self, item)
	managers.menu_scene:clicked_weapon_upgrade_type(item.parameters(item).name)

	return 
end
MenuCallbackHandler.can_buy_weapon_upgrade = function (self, item)
	return not self.owns_weapon_upgrade(self, item)
end
MenuCallbackHandler.owns_weapon_upgrade = function (self, item)
	return Global.blackmarket_manager.weapon_upgrades[item.parameter(item, "weapon_id")][item.parameter(item, "weapon_upgrade")].owned
end
MenuCallbackHandler.buy_weapon_upgrades = function (self, item)
	return 
end
MenuCallbackHandler._on_buy_weapon_upgrade_yes = function (self, params)
	Global.blackmarket_manager.weapon_upgrades[params.item:parameter("weapon_id")][params.item:parameter("weapon_upgrade")].owned = true
	params.item:parameter("parent_item"):parameters().owned = true

	params.item:dirty()
	params.item:parameters().parent_item:on_buy(params.item:parameters().gui_node)

	return 
end
MenuCallbackHandler.clicked_customize_character_category = function (self, item)
	local name = item.name(item)

	if name == "masks" then
		if item.expanded(item) then
			managers.menu_scene:clicked_masks()

			return 
		end
	elseif name == "armor" and item.expanded(item) then
		managers.menu_scene:clicked_armor()

		return 
	end

	managers.menu_scene:clicked_customize_character_category()

	return 
end
MenuCallbackHandler.test_clicked_mask = function (self, item)
	if not item.parameter(item, "customize") then
		managers.menu_scene:clicked_blackmarket_item()
	end

	managers.menu_component:close_weapon_box()
	managers.menu_scene:spawn_mask(item.parameter(item, "mask_id"))

	return 
end
MenuCallbackHandler.can_buy_mask = function (self, item)
	return not self.owns_mask(self, item)
end
MenuCallbackHandler.owns_mask = function (self, item)
	return Global.blackmarket_manager.masks[item.parameter(item, "mask_id")].owned
end
MenuCallbackHandler.equip_mask = function (self, item)
	local mask_id = item.parameter(item, "mask_id")

	managers.blackmarket:on_buy_mask(mask_id, "normal", 9)
	managers.blackmarket:equip_mask(9)
	self._update_outfit_information(self)

	return 
end
MenuCallbackHandler._update_outfit_information = function (self)
	local outfit_string = managers.blackmarket:outfit_string()

	if self.is_steam(self) then
		Steam:set_rich_presence("outfit", outfit_string)
	end

	if managers.network:session() then
		local local_peer = managers.network:session():local_peer()
		local in_lobby = local_peer.in_lobby(local_peer) and game_state_machine:current_state_name() ~= "ingame_lobby_menu"

		if managers.menu_scene and in_lobby then
			local id = local_peer.id(local_peer)

			managers.menu_scene:set_lobby_character_out_fit(id, outfit_string, managers.experience:current_rank())
		end

		local kit_menu = managers.menu:get_menu("kit_menu")

		if kit_menu then
			local id = local_peer.id(local_peer)
			local criminal_name = local_peer.character(local_peer)

			kit_menu.renderer:set_slot_outfit(id, criminal_name, outfit_string)
		end

		local_peer.set_outfit_string(local_peer, outfit_string)

		local local_player = managers.player:local_player()

		if alive(local_player) and local_player.character_damage(local_player) then
			local_player.character_damage(local_player):update_armor_stored_health()
		end

		managers.network:session():check_send_outfit()
	end

	return 
end
MenuCallbackHandler.buy_mask = function (self, item)
	local name = managers.localization:text(tweak_data.blackmarket.masks[item.parameter(item, "mask_id")].name_id)
	local cost = 10000
	local yes_func = callback(self, self, "_on_buy_mask_yes", {
		item = item,
		cost = cost
	})

	managers.menu:show_buy_weapon({
		yes_func = yes_func
	}, name, "$" .. cost)

	return 
end
MenuCallbackHandler._on_buy_mask_yes = function (self, params)
	Global.blackmarket_manager.masks[params.item:parameter("mask_id")].owned = true
	params.item:parameter("parent_item"):parameters().owned = true

	params.item:dirty()
	params.item:parameters().parent_item:on_buy(params.item:parameters().gui_node)

	return 
end
MenuCallbackHandler.leave_character_customization = function (self)
	self.leave_blackmarket(self)

	return 
end
MenuCallbackHandler.clicked_character = function (self, item)
	print("MenuCallbackHandler:clicked_character", item)

	return 
end
MenuCallbackHandler.equip_character = function (self, item)
	local character_id = item.parameter(item, "character_id")
	Global.blackmarket_manager.characters[character_id].equipped = true

	managers.menu_scene:set_character(character_id)

	for id, character in pairs(Global.blackmarket_manager.characters) do
		if id ~= character_id then
			character.equipped = false
		end
	end

	self._update_outfit_information(self)

	return 
end
MenuCallbackHandler.can_buy_character = function (self, item)
	return not self.owns_character(self, item)
end
MenuCallbackHandler.owns_character = function (self, item)
	return Global.blackmarket_manager.characters[item.parameter(item, "character_id")].owned
end
MenuCallbackHandler.buy_character = function (self, item)
	local name = managers.localization:text(tweak_data.blackmarket.characters[item.parameter(item, "character_id")].name_id)
	local cost = 10000
	local yes_func = callback(self, self, "_on_buy_character_yes", {
		item = item,
		cost = cost
	})

	managers.menu:show_buy_weapon({
		yes_func = yes_func
	}, name, "$" .. cost)

	return 
end
MenuCallbackHandler._on_buy_character_yes = function (self, params)
	Global.blackmarket_manager.characters[params.item:parameter("character_id")].owned = true
	params.item:parameter("parent_item"):parameters().owned = true

	params.item:dirty()
	params.item:parameters().parent_item:on_buy(params.item:parameters().gui_node)

	return 
end
MenuCallbackHandler.test_clicked_armor = function (self, item)
	managers.menu_component:close_weapon_box()

	slot2 = item.parameter(item, "customize") or slot2

	return 
end
MenuCallbackHandler.can_buy_armor = function (self, item)
	return not self.owns_armor(self, item)
end
MenuCallbackHandler.owns_armor = function (self, item)
	return Global.blackmarket_manager.armors[item.parameter(item, "armor_id")].owned
end
MenuCallbackHandler.buy_armor = function (self, item)
	local name = managers.localization:text(tweak_data.blackmarket.armors[item.parameter(item, "armor_id")].name_id)
	local cost = 20000
	local yes_func = callback(self, self, "_on_buy_armor_yes", {
		item = item,
		cost = cost
	})

	managers.menu:show_buy_weapon({
		yes_func = yes_func
	}, name, "$" .. cost)

	return 
end
MenuCallbackHandler._on_buy_armor_yes = function (self, params)
	Global.blackmarket_manager.armors[params.item:parameter("armor_id")].owned = true
	params.item:parameter("parent_item"):parameters().owned = true

	params.item:dirty()
	params.item:parameters().parent_item:on_buy(params.item:parameters().gui_node)

	return 
end
MenuCallbackHandler.equip_armor = function (self, item)
	local armor_id = item.parameter(item, "armor_id")
	Global.blackmarket_manager.armors[armor_id].equipped = true

	managers.menu_scene:set_character_armor(armor_id)

	for id, armor in pairs(Global.blackmarket_manager.armors) do
		if id ~= armor_id then
			armor.equipped = false
		end
	end

	self._update_outfit_information(self)

	return 
end
MenuCallbackHandler.repair_armor = function (self, item)
	if item._at_max_condition(item) then
		return 
	end

	local armor_id = item.parameter(item, "armor_id")
	local name = managers.localization:text(tweak_data.blackmarket.armors[armor_id].name_id)
	local cost = (item.parameter(item, "parent_item"):condition()/item._max_condition(item) - 1)*30000
	local yes_func = callback(self, self, "on_repair_armor_yes", {
		item = item,
		cost = cost
	})

	managers.menu:show_repair_weapon({
		yes_func = yes_func
	}, name, "$" .. cost)

	return 
end
MenuCallbackHandler.on_repair_armor_yes = function (self, params)
	Global.blackmarket_manager.armors[params.item:parameters().armor_id].condition = params.item:_max_condition()

	params.item:dirty()

	return 
end
MenuCallbackHandler.stage_success = function (self)
	if not managers.job:has_active_job() then
		return true
	end

	return managers.job:stage_success()
end
MenuCallbackHandler.stage_not_success = function (self)
	return not self.stage_success(self)
end
MenuCallbackHandler.is_job_finished = function (self)
	return managers.job:is_job_finished()
end
MenuCallbackHandler.is_job_not_finished = function (self)
	return not self.is_job_finished(self)
end
MenuCallbackHandler.got_job = function (self)
	return managers.job:has_active_job()
end
MenuCallbackHandler.got_no_job = function (self)
	return not self.got_job(self)
end
MenuCallbackHandler.start_safe_test_overkill = function (self)
	return 
end
MenuCallbackHandler.start_safe_test_event_01 = function (self)
	managers.menu_scene:_test_start_open_economy_safe("event_01")

	return 
end
MenuCallbackHandler.start_safe_test_weapon_01 = function (self)
	managers.menu_scene:_test_start_open_economy_safe("weapon_01")

	return 
end
MenuCallbackHandler.start_safe_test_random = function (self)
	local safe_names = table.map_keys(tweak_data.economy.safes)

	table.delete(safe_names, "weapon_01")

	local safe_name = safe_names[math.random(#safe_names)]

	managers.menu_scene:_test_start_open_economy_safe(safe_name)

	return 
end
MenuCallbackHandler.reset_safe_scene = function (self)
	if not managers.menu:cash_safe_scene_done() then
		return true
	end

	managers.menu:set_cash_safe_scene_done(false)
	managers.menu_scene:reset_economy_safe()

	return 
end
MenuCallbackHandler.is_cash_safe_back_visible = function (self)
	return managers.menu:cash_safe_scene_done()
end
MenuComponentInitiator = MenuComponentInitiator or class()
MenuComponentInitiator.modify_node = function (self, original_node, data)
	local node = deep_clone(original_node)

	if data and data.back_callback then
		table.insert(node.parameters(node).back_callback, data.back_callback)
	end

	node.parameters(node).menu_component_data = data

	return node
end
MenuLoadoutInitiator = MenuLoadoutInitiator or class()
MenuLoadoutInitiator.modify_node = function (self, original_node, data)
	local node = deep_clone(original_node)
	node.parameters(node).menu_component_data = data
	node.parameters(node).menu_component_next_node_name = "loadout"

	return node
end
MenuCrimeNetInitiator = MenuCrimeNetInitiator or class()
MenuCrimeNetInitiator.modify_node = function (self, node)
	local new_node = deep_clone(node)

	self.refresh_node(self, new_node)

	return new_node
end
MenuCrimeNetInitiator.refresh_node = function (self, node)
	return node

	local dead_list = {}

	for _, item in ipairs(node.items(node)) do
		dead_list[item.parameters(item).name] = true
	end

	local online = {}
	local offline = {}

	if SystemInfo:distribution() == Idstring("STEAM") then
		for _, user in ipairs(Steam:friends()) do
			if (math.random(2) == 1 and user.state(user) == "online") or user.state(user) == "away" then
				table.insert(online, user)
			else
				table.insert(offline, user)
			end
		end
	end

	node.delete_item(node, "online")

	if not node.item(node, "online") then
		local params = {
			text_id = "menu_online",
			name = "online",
			type = "MenuItemDivider"
		}
		local new_item = node.create_item(node, {
			type = "MenuItemDivider"
		}, params)

		node.add_item(node, new_item)
	end

	for _, user in ipairs(online) do
		local name = user.id(user)
		local item = node.item(node, name)

		if item then
			node.delete_item(node, name)
		end

		local params = {
			localize = "false",
			name = name,
			text_id = user.name(user)
		}
		local new_item = node.create_item(node, nil, params)

		node.add_item(node, new_item)
	end

	node.delete_item(node, "offline")

	if not node.item(node, "offline") then
		local params = {
			text_id = "menu_offline",
			name = "offline",
			type = "MenuItemDivider"
		}
		local new_item = node.create_item(node, {
			type = "MenuItemDivider"
		}, params)

		node.add_item(node, new_item)
	end

	for _, user in ipairs(offline) do
		local name = user.id(user)
		local item = node.item(node, name)

		if item then
			node.delete_item(node, name)
		end

		local params = {
			localize = "false",
			name = name,
			text_id = user.name(user)
		}
		local new_item = node.create_item(node, nil, params)

		node.add_item(node, new_item)
	end

	managers.menu:add_back_button(node)

	return node
end
MenuManager.show_repair_weapon = function (self, params, weapon, cost)
	local dialog_data = {
		title = managers.localization:text("dialog_repair_weapon_title"),
		text = managers.localization:text("dialog_repair_weapon_message", {
			WEAPON = weapon,
			COST = cost
		})
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = params.yes_func
	}
	local no_button = {
		text = managers.localization:text("dialog_no")
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuManager.show_buy_weapon = function (self, params, weapon, cost)
	local dialog_data = {
		title = managers.localization:text("dialog_buy_weapon_title"),
		text = managers.localization:text("dialog_buy_weapon_message", {
			WEAPON = weapon,
			COST = cost
		})
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = params.yes_func
	}
	local no_button = {
		text = managers.localization:text("dialog_no")
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler.on_visit_crimefest_challenges = function (self)
	if SystemInfo:distribution() == Idstring("STEAM") then
		Steam:overlay_activate("url", tweak_data.gui.crimefest_challenges_webpage)
	end

	return 
end
MenuCallbackHandler.got_new_steam_lootdrop = function (self, item)
	return managers.blackmarket:has_new_tradable_items()
end
MenuCallbackHandler.leave_steam_inventory = function (self, item)
	MenuCallbackHandler:_leave_blackmarket()

	return 
end
MenuCallbackHandler.can_toggle_chat = function (self)
	local input = managers.menu:active_menu() and managers.menu:active_menu().input

	return not input or (input.can_toggle_chat and input.can_toggle_chat(input))
end
MenuCallbackHandler.on_visit_fbi_files = function (self)
	if SystemInfo:distribution() == Idstring("STEAM") then
		if MenuCallbackHandler:is_overlay_enabled() then
			Steam:overlay_activate("url", tweak_data.gui.fbi_files_webpage)
		else
			managers.menu:show_enable_steam_overlay()
		end
	end

	return 
end
MenuCallbackHandler.on_visit_fbi_files_suspect = function (self, item)
	if item and SystemInfo:distribution() == Idstring("STEAM") then
		if MenuCallbackHandler:is_overlay_enabled() then
			Steam:overlay_activate("url", tweak_data.gui.fbi_files_webpage .. ((item and "/suspect/" .. item.name(item) .. "/") or ""))
		else
			managers.menu:show_enable_steam_overlay()
		end
	end

	return 
end
FbiFilesInitiator = FbiFilesInitiator or class()
FbiFilesInitiator.modify_node = function (self, node, up)
	node.clean_items(node)

	local params = {
		callback = "on_visit_fbi_files",
		name = "on_visit_fbi_files",
		help_id = "menu_visit_fbi_files_help",
		text_id = "menu_visit_fbi_files"
	}
	local new_item = node.create_item(node, nil, params)

	node.add_item(node, new_item)

	if managers.network:session() then
		local peer = managers.network:session():local_peer()
		local params = {
			localize_help = false,
			localize = false,
			to_upper = false,
			callback = "on_visit_fbi_files_suspect",
			name = peer.user_id(peer),
			text_id = peer.name(peer) .. " (" .. ((0 < managers.experience:current_rank() and managers.experience:rank_string(managers.experience:current_rank()) .. "-") or "") .. (managers.experience:current_level() or "") .. ")",
			rpc = peer.rpc(peer),
			peer = peer
		}
		local new_item = node.create_item(node, nil, params)

		node.add_item(node, new_item)

		for _, peer in pairs(managers.network:session():peers()) do
			local params = {
				localize_help = false,
				localize = false,
				to_upper = false,
				callback = "on_visit_fbi_files_suspect",
				name = peer.user_id(peer),
				text_id = peer.name(peer) .. " (" .. ((0 < peer.rank(peer) and managers.experience:rank_string(peer.rank(peer)) .. "-") or "") .. (peer.level(peer) or "") .. ")",
				rpc = peer.rpc(peer),
				peer = peer
			}
			local new_item = node.create_item(node, nil, params)

			node.add_item(node, new_item)
		end
	end

	managers.menu:add_back_button(node)

	return node
end
FbiFilesInitiator.refresh_node = function (self, node)
	return self.modify_node(self, node)
end
PlayerListInitiator = PlayerListInitiator or class(MenuInitiatorBase)
PlayerListInitiator.get_peer_name = function (self, peer)
	if not peer then
		return "No peer"
	end

	if peer == managers.network:session():local_peer() then
		return peer.name(peer) .. " (" .. ((0 < managers.experience:current_rank() and managers.experience:rank_string(managers.experience:current_rank()) .. "-") or "") .. (managers.experience:current_level() or "") .. ")"
	else
		return peer.name(peer) .. " (" .. ((0 < peer.rank(peer) and managers.experience:rank_string(peer.rank(peer)) .. "-") or "") .. (peer.level(peer) or "") .. ")"
	end

	return 
end
PlayerListInitiator.add_peer_item = function (self, node, peer)
	local params = {
		localize_help = false,
		localize = false,
		to_upper = false,
		callback = "on_player_list_inspect_peer",
		name = peer.user_id(peer),
		text_id = self.get_peer_name(self, peer),
		rpc = peer.rpc(peer),
		peer = peer
	}
	local new_item = node.create_item(node, nil, params)

	node.add_item(node, new_item)

	return 
end
PlayerListInitiator.modify_node = function (self, node, up)
	node.clean_items(node)

	local params = {
		callback = "on_visit_fbi_files",
		name = "on_visit_fbi_files",
		help_id = "menu_visit_fbi_files_help",
		text_id = "menu_visit_fbi_files"
	}
	local new_item = node.create_item(node, nil, params)

	node.add_item(node, new_item)
	self.create_divider(self, node, "fbi_spacer")

	if managers.network:session() then
		self.add_peer_item(self, node, managers.network:session():local_peer())

		for _, peer in pairs(managers.network:session():peers()) do
			self.add_peer_item(self, node, peer)
		end
	end

	managers.menu:add_back_button(node)

	return node
end
PlayerListInitiator.refresh_node = function (self, node)
	return self.modify_node(self, node)
end
MenuCallbackHandler.on_player_list_inspect_peer = function (self, item, node)
	if item then
		managers.menu:open_node("inspect_player", {
			item.parameters(item).peer
		})
	end

	return 
end
InspectPlayerInitiator = InspectPlayerInitiator or class(MenuInitiatorBase)
InspectPlayerInitiator.modify_node = function (self, node, inspect_peer)
	node.clean_items(node)

	if not inspect_peer then
		Application:error("Can not open inpsect player without a specified peer!")
		managers.menu:back()
	end

	local is_local_peer = inspect_peer == managers.network:session():local_peer()
	local params = {
		name = "peer_name",
		localize = false,
		no_text = false,
		text_id = PlayerListInitiator.get_peer_name(self, inspect_peer),
		color = tweak_data.screen_colors.text
	}
	local data_node = {
		type = "MenuItemDivider"
	}
	local new_item = node.create_item(node, data_node, params)

	node.add_item(node, new_item)

	local params = {
		callback = "on_visit_fbi_files_suspect",
		help_id = "menu_visit_fbi_files_help",
		text_id = "menu_visit_fbi_files",
		name = inspect_peer.user_id(inspect_peer)
	}
	local new_item = node.create_item(node, nil, params)

	node.add_item(node, new_item)
	self.create_divider(self, node, "fbi_spacer")

	if not is_local_peer and Network:is_server() then
		if MenuCallbackHandler:kick_player_visible() or MenuCallbackHandler:kick_vote_visible() then
			local params = {
				callback = "kick_player",
				name = "kick_player",
				text_id = (MenuCallbackHandler:kick_player_visible() and "menu_kick_player") or "menu_kick_vote",
				rpc = inspect_peer.rpc(inspect_peer),
				peer = inspect_peer
			}
			local new_item = node.create_item(node, nil, params)

			node.add_item(node, new_item)
		end

		local function get_identifier(peer)
			return (SystemInfo:platform() == Idstring("WIN32") and peer.user_id(peer)) or peer.name(peer)
		end

		local params = {
			callback = "kick_ban_player",
			text_id = "menu_players_list_ban",
			name = inspect_peer.name(inspect_peer),
			identifier = get_identifier(inspect_peer),
			rpc = inspect_peer.rpc(inspect_peer),
			peer = inspect_peer
		}
		local new_item = node.create_item(node, nil, params)

		node.add_item(node, new_item)
	end

	if not is_local_peer then
		local toggle_mute = self.create_toggle(self, node, {
			localize = true,
			name = "toggle_mute",
			enabled = true,
			text_id = "menu_players_list_mute",
			callback = "mute_player",
			rpc = inspect_peer.rpc(inspect_peer),
			peer = inspect_peer
		})

		toggle_mute.set_value(toggle_mute, (inspect_peer.is_muted(inspect_peer) and "on") or "off")
	end

	self.create_divider(self, node, "admin_spacer")

	local user = Steam:user(inspect_peer.ip(inspect_peer))

	if (user and user.rich_presence(user, "is_modded") == "1") or inspect_peer.is_modded(inspect_peer) then
		local params = {
			text_id = "menu_players_list_mods",
			name = "peer_mods",
			no_text = false,
			size = 8,
			color = tweak_data.screen_colors.text
		}
		local data_node = {
			type = "MenuItemDivider"
		}
		local new_item = node.create_item(node, data_node, params)

		node.add_item(node, new_item)

		for i, mod in ipairs(inspect_peer.synced_mods(inspect_peer)) do
			local params = {
				callback = "inspect_mod",
				localize = false,
				name = "mod_" .. tostring(i),
				text_id = mod.name,
				mod_id = mod.id
			}
			local new_item = node.create_item(node, nil, params)

			node.add_item(node, new_item)
		end
	end

	managers.menu:add_back_button(node)
	node.set_default_item_name(node, "back")

	return node
end
InspectPlayerInitiator.refresh_node = function (self, node)
	return self.modify_node(self, node)
end
MenuCallbackHandler.inspect_mod = function (self, item)
	Steam:overlay_activate("url", "http://paydaymods.com/mods/" .. item.parameters(item).mod_id or "")

	return 
end
MenuCallbackHandler.kick_ban_player = function (self, item)
	local dialog_data = {
		title = managers.localization:text("dialog_sure_to_ban_title"),
		text = managers.localization:text("dialog_sure_to_kick_ban_body", {
			USER = item.parameters(item).name
		})
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_kick_ban_player_confirm", item)
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler._kick_ban_player_confirm = function (self, item)
	local peer = item.parameters(item).peer

	if peer then
		managers.ban_list:ban(item.parameters(item).identifier, item.parameters(item).name)

		local message_id = 0
		message_id = 6

		managers.network:session():send_to_peers("kick_peer", peer.id(peer), message_id)
		managers.network:session():on_peer_kicked(peer, peer.id(peer), message_id)
	end

	return 
end
MenuChooseWeaponCosmeticInitiator = MenuChooseWeaponCosmeticInitiator or class(MenuInitiatorBase)
MenuChooseWeaponCosmeticInitiator.modify_node = function (self, original_node, data)
	local node = deep_clone(original_node)

	node.clean_items(node)

	if not node.item(node, "divider_end") then
		if data and data.instance_ids then
			local sort_items = {}

			for id, data in pairs(data.instance_ids) do
				table.insert(sort_items, id)
			end

			for _, instance_id in ipairs(sort_items) do
				self.create_item(self, node, {
					localize = false,
					enabled = true,
					name = instance_id,
					text_id = instance_id
				})
			end

			print(inspect(data.instance_ids))
		end

		self.create_divider(self, node, "end")
		self.add_back_button(self, node)
		node.set_default_item_name(node, "back")
		node.select_item(node, "back")
	end

	managers.menu_component:set_blackmarket_enabled(false)

	return node
end
MenuChooseWeaponCosmeticInitiator.add_back_button = function (self, node)
	node.delete_item(node, "back")

	local params = {
		visible_callback = "is_pc_controller",
		name = "back",
		halign = "right",
		text_id = "menu_back",
		last_item = "true",
		align = "right",
		previous_node = "true"
	}
	local data_node = {}
	local new_item = node.create_item(node, data_node, params)

	node.add_item(node, new_item)

	return new_item
end
MenuOpenContainerInitiator = MenuOpenContainerInitiator or class(MenuInitiatorBase)
MenuOpenContainerInitiator.modify_node = function (self, original_node, data)
	local node = deep_clone(original_node)
	node.parameters(node).container_data = data.container or {}

	managers.menu_component:set_blackmarket_enabled(false)
	self.update_node(self, node)

	return node
end
MenuOpenContainerInitiator.refresh_node = function (self, node)
	self.update_node(self, node)

	return node
end
MenuOpenContainerInitiator.update_node = function (self, node)
	local item = node.item(node, "open_container")

	if item then
		item.set_enabled(item, MenuCallbackHandler:have_safe_and_drill_for_container(node.parameters(node).container_data))
	end

	return 
end
MenuCallbackHandler.have_no_drills_for_container = function (self, item)
	if not managers.menu:active_menu() or not managers.menu:active_menu().logic:selected_node() then
		return false
	end

	local data = managers.menu:active_menu().logic:selected_node():parameters().container_data

	return true
end
MenuCallbackHandler.can_buy_drill = function (self, item)
	if not managers.menu:active_menu() or not managers.menu:active_menu().logic:selected_node() then
		return false
	end

	local data = managers.menu:active_menu().logic:selected_node():parameters().container_data

	if not data then
		return 
	end

	local drill = data.drill

	if not drill then
		return 
	end

	return tweak_data.economy.drills[drill].price and not not tweak_data.economy.drills[drill].def_id
end
MenuCallbackHandler.have_safe_and_drill_for_container = function (self, data)
	if not data then
		return 
	end

	local drill = data.drill
	local safe = data.safe
	local safe_free = tweak_data.economy.safes[safe] and tweak_data.economy.safes[safe].free
	local have_drill = safe_free or managers.blackmarket:have_inventory_tradable_item("drills", drill)
	local have_safe = managers.blackmarket:have_inventory_tradable_item("safes", safe)

	return have_drill and have_safe
end
MenuCallbackHandler.steam_buy_drill = function (self, item, data)
	local node = managers.menu:active_menu() and managers.menu:active_menu().logic:selected_node()
	local quantity_item = node.item(node, "buy_quantity")
	data = data or managers.menu:active_menu().logic:selected_node():parameters().container_data

	if not data then
		return 
	end

	local drill = data.drill
	local quantity = (quantity_item and tonumber(quantity_item.value(quantity_item))) or 1
	local def_id = tweak_data.economy.drills[drill] and tweak_data.economy.drills[drill].def_id

	if not MenuCallbackHandler:is_overlay_enabled() then
		managers.menu:show_enable_steam_overlay_tradable_item()
	elseif def_id then
		managers.network.account:add_overlay_listener("steam_transaction_tradable_item", {
			"overlay_close"
		}, callback(MenuCallbackHandler, MenuCallbackHandler, "on_steam_transaction_over"))
		Steam:overlay_activate("url", tweak_data.economy:create_buy_tradable_url(def_id, quantity))
		managers.menu:show_buying_tradable_item_dialog()
	end

	return 
end
MenuCallbackHandler.steam_buy_safe_from_community = function (self, item, data)
	local node = managers.menu:active_menu() and managers.menu:active_menu().logic:selected_node()
	local quantity_item = node.item(node, "buy_quantity")
	data = data or managers.menu:active_menu().logic:selected_node():parameters().container_data

	if not data then
		return 
	end

	local safe = data.safe
	local quantity = (quantity_item and tonumber(quantity_item.value(quantity_item))) or 1

	if not MenuCallbackHandler:is_overlay_enabled() then
		managers.menu:show_enable_steam_overlay_tradable_item()
	elseif safe then
		managers.network.account:add_overlay_listener("steam_transaction_tradable_item", {
			"overlay_close"
		}, callback(MenuCallbackHandler, MenuCallbackHandler, "on_steam_transaction_over"))
		Steam:overlay_activate("url", tweak_data.economy:create_market_link_url("safes", safe))
		managers.menu:show_buying_tradable_item_dialog()
	end

	return 
end
MenuCallbackHandler.steam_find_item_from_community = function (self, item, data)
	local node = managers.menu:active_menu() and managers.menu:active_menu().logic:selected_node()
	local quantity_item = node.item(node, "buy_quantity")
	data = data or managers.menu:active_menu().logic:selected_node():parameters().container_data

	if not data then
		return 
	end

	local cosmetic_id = data.cosmetic_id
	local weapon_id = data.weapon_id

	if not MenuCallbackHandler:is_overlay_enabled() then
		managers.menu:show_enable_steam_overlay_tradable_item()
	elseif cosmetic_id and weapon_id then
		managers.network.account:add_overlay_listener("steam_transaction_tradable_item", {
			"overlay_close"
		}, callback(MenuCallbackHandler, MenuCallbackHandler, "on_steam_transaction_over"))
		Steam:overlay_activate("url", tweak_data.economy:create_weapon_skin_market_search_url(weapon_id, cosmetic_id))
		managers.menu:show_buying_tradable_item_dialog()
	elseif cosmetic_id and data.armor then
		managers.network.account:add_overlay_listener("steam_transaction_tradable_item", {
			"overlay_close"
		}, callback(MenuCallbackHandler, MenuCallbackHandler, "on_steam_transaction_over"))
		Steam:overlay_activate("url", tweak_data.economy:create_armor_skin_market_search_url(data.cosmetic_id))
		managers.menu:show_buying_tradable_item_dialog()
	end

	return 
end
MenuCallbackHandler.steam_sell_item = function (self, item)
	local steam_id = Steam:userid()
	local instance_id = item.instance_id

	if not MenuCallbackHandler:is_overlay_enabled() then
		managers.menu:show_enable_steam_overlay_tradable_item()
	elseif steam_id and instance_id then
		print("selling item", "steam_id", steam_id, "instance_id", instance_id)
		managers.network.account:add_overlay_listener("steam_transaction_tradable_item", {
			"overlay_close"
		}, callback(MenuCallbackHandler, MenuCallbackHandler, "on_steam_transaction_over"))
		Steam:overlay_activate("url", tweak_data.economy:create_sell_tradable_url(steam_id, instance_id))
	end

	return 
end
MenuCallbackHandler.on_steam_transaction_over = function (self, canceled)
	print("on_steam_transaction_over", canceled)
	managers.network.account:remove_overlay_listener("steam_transaction_tradable_item")
	managers.network.account:inventory_load()
	managers.system_menu:close("buy_tradable_item")

	return 
end
MenuCallbackHandler.steam_open_container = function (self, item)
	if not managers.menu:active_menu() or not managers.menu:active_menu().logic:selected_node() then
		return false
	end

	local data = managers.menu:active_menu().logic:selected_node():parameters().container_data

	if not MenuCallbackHandler:have_safe_and_drill_for_container(data) then
		return 
	end

	local safe_entry = data.safe
	local safe_tweak = tweak_data.economy.safes[safe_entry]

	local function ready_clbk()
		print("ECONOMY SAFE READY CALLBACK")
		managers.menu:back()
		managers.system_menu:force_close_all()
		managers.menu_component:set_blackmarket_enabled(false)
		managers.menu:open_node("open_steam_safe", {
			data.content
		})

		return 
	end

	managers.menu_component:set_blackmarket_disable_fetching(true)
	managers.menu_component:set_blackmarket_enabled(false)
	managers.menu_scene:create_economy_safe_scene(safe_entry, ready_clbk)

	if safe_tweak and safe_tweak.free then
		managers.network.account:inventory_reward_open(safe_entry, data.safe_id, callback(MenuCallbackHandler, MenuCallbackHandler, "_safe_result_recieved"))
	else
		managers.network.account:inventory_reward_unlock(safe_entry, data.safe_id, data.drill_id, callback(MenuCallbackHandler, MenuCallbackHandler, "_safe_result_recieved"))
	end

	return 
end
MenuCallbackHandler._safe_result_recieved = function (self, error, items_new, items_removed)
	local active_node_gui = managers.menu:active_menu().renderer:active_node_gui()

	managers.network.account:inventory_repair_list(items_new)
	managers.network.account:inventory_repair_list(items_removed)

	if active_node_gui and active_node_gui._safe_result_recieved then
		active_node_gui._safe_result_recieved(active_node_gui, error, items_new, items_removed)
	else
		managers.menu_scene:store_safe_result(error, items_new, items_removed)
	end

	return 
end
MenuEconomySafeInitiator = MenuEconomySafeInitiator or class()
MenuEconomySafeInitiator.modify_node = function (self, node, safe_entry)
	node.parameters(node).safe_entry = safe_entry

	return node
end
MenuBanListInitiator = MenuBanListInitiator or class(MenuInitiatorBase)
MenuBanListInitiator.modify_node = function (self, node)
	node.clean_items(node)

	local added = false

	local function get_identifier(peer)
		return (SystemInfo:platform() == Idstring("WIN32") and peer.user_id(peer)) or peer.name(peer)
	end

	if managers.network:session() then
		for _, user in pairs(managers.network:session():peers()) do
			if not managers.ban_list:banned(get_identifier(user)) then
				self.create_item(self, node, {
					localize = false,
					enabled = true,
					align = "left",
					callback = "ban_player",
					name = user.name(user),
					text_id = user.name(user),
					identifier = get_identifier(user)
				})

				added = true
			end
		end
	end

	if not added then
		self.create_item(self, node, {
			align = "left",
			name = "no_ban_items",
			enabled = false,
			text_id = "bm_menu_no_items"
		})
	end

	added = false

	for _, user in ipairs(managers.ban_list:ban_list()) do
		self.create_item(self, node, {
			localize = false,
			enabled = true,
			align = "left",
			callback = "unban_player",
			left_column = true,
			name = user.name,
			text_id = user.name,
			identifier = user.identifier
		})

		added = true
	end

	if not added then
		self.create_item(self, node, {
			name = "no_unban_items",
			enabled = false,
			text_id = "bm_menu_no_items",
			align = "left",
			left_column = true
		})
	end

	self.add_back_button(self, node)

	return node
end
MenuBanListInitiator.refresh_node = function (self, node)
	self.modify_node(self, node)

	return 
end
MenuCallbackHandler.ban_player = function (self, item, force)
	if item.parameters(item).identifier and item.parameters(item).name then
		if not force then
			local dialog_data = {
				title = managers.localization:text("dialog_sure_to_ban_title"),
				text = managers.localization:text("dialog_sure_to_ban_body", {
					USER = item.parameters(item).name
				})
			}
			local yes_button = {
				text = managers.localization:text("dialog_yes"),
				callback_func = callback(self, self, "ban_player", item, force)
			}
			local no_button = {
				text = managers.localization:text("dialog_no"),
				cancel_button = true
			}
			dialog_data.button_list = {
				yes_button,
				no_button
			}

			managers.system_menu:show(dialog_data)

			return 
		else
			managers.ban_list:ban(item.parameters(item).identifier, item.parameters(item).name)
		end
	end

	local node = managers.menu:active_menu().logic:get_node("ban_list")

	managers.menu:active_menu().renderer:active_node_gui():refresh_gui(node)

	return 
end
MenuCallbackHandler.unban_player = function (self, item, force)
	if item.parameters(item).identifier and item.parameters(item).name then
		if not force then
			local dialog_data = {
				title = managers.localization:text("dialog_sure_to_unban_title"),
				text = managers.localization:text("dialog_sure_to_unban_body", {
					USER = item.parameters(item).name
				})
			}
			local yes_button = {
				text = managers.localization:text("dialog_yes"),
				callback_func = callback(self, self, "unban_player", item, force)
			}
			local no_button = {
				text = managers.localization:text("dialog_no"),
				cancel_button = true
			}
			dialog_data.button_list = {
				yes_button,
				no_button
			}

			managers.system_menu:show(dialog_data)

			return 
		else
			managers.ban_list:unban(item.parameters(item).identifier)
		end
	end

	local node = managers.menu:active_menu().logic:get_node("ban_list")

	managers.menu:active_menu().renderer:active_node_gui():refresh_gui(node)

	return 
end
MenuCallbackHandler.start_quickplay_game = function (self, item)
	managers.crimenet:join_quick_play_game()

	return 
end
MenuQuickplaySettingsInitiator = MenuQuickplaySettingsInitiator or class(MenuInitiatorBase)
MenuQuickplaySettingsInitiator.modify_node = function (self, node)
	local stealth_item = node.item(node, "quickplay_settings_stealth")
	local loud_item = node.item(node, "quickplay_settings_loud")
	local stealth_on = managers.user:get_setting("quickplay_stealth")
	local loud_on = managers.user:get_setting("quickplay_loud")

	stealth_item.set_value(stealth_item, (stealth_on and "on") or "off")
	loud_item.set_value(loud_item, (loud_on and "on") or "off")
	stealth_item.set_parameter(stealth_item, "loud", loud_item)
	loud_item.set_parameter(loud_item, "stealth", stealth_item)
	node.item(node, "quickplay_settings_level_min"):set_max(tweak_data.quickplay.max_level_diff[1])
	node.item(node, "quickplay_settings_level_min"):set_value((Global.crimenet and Global.crimenet.quickplay and Global.crimenet.quickplay.level_diff_min) or tweak_data.quickplay.default_level_diff[1])
	node.item(node, "quickplay_settings_level_max"):set_max(tweak_data.quickplay.max_level_diff[2])
	node.item(node, "quickplay_settings_level_max"):set_value((Global.crimenet and Global.crimenet.quickplay and Global.crimenet.quickplay.level_diff_max) or tweak_data.quickplay.default_level_diff[2])

	local mutators_item = node.item(node, "quickplay_settings_mutators")
	local mutators_on = managers.user:get_setting("quickplay_mutators")

	mutators_item.set_value(mutators_item, (mutators_on and "on") or "off")

	local difficulty_item = node.item(node, "quickplay_settings_difficulty")

	if not difficulty_item then
		local options = {
			{
				value = "any",
				text_id = "menu_any",
				_meta = "option"
			}
		}

		for _, difficulty in ipairs(tweak_data.difficulties) do
			if difficulty ~= "easy" then
				table.insert(options, {
					_meta = "option",
					text_id = tweak_data.difficulty_name_ids[difficulty],
					value = difficulty
				})
			end
		end

		difficulty_item = self.create_multichoice(self, node, options, {
			callback = "quickplay_difficulty",
			name = "quickplay_settings_difficulty",
			help_id = "menu_quickplay_settings_difficulty",
			text_id = "menu_quickplay_settings_difficulty"
		}, 1)
	end

	if Global.crimenet and Global.crimenet.quickplay and Global.crimenet.quickplay.difficulty then
		difficulty_item.set_value(difficulty_item, Global.crimenet.quickplay.difficulty)
	else
		difficulty_item.set_value(difficulty_item, "any")
	end

	return node
end
MenuQuickplaySettingsInitiator.refresh_node = function (self, node)
	self.modify_node(self, node)

	return 
end
MenuCallbackHandler.quickplay_stealth_toggle = function (self, item)
	local on = item.value(item) == "on"

	managers.user:set_setting("quickplay_stealth", on)

	if not on and item.parameter(item, "loud"):value() == "off" then
		item.parameter(item, "loud"):set_value("on")
		managers.user:set_setting("quickplay_loud", true)
	end

	return 
end
MenuCallbackHandler.quickplay_loud_toggle = function (self, item)
	local on = item.value(item) == "on"

	managers.user:set_setting("quickplay_loud", on)

	if not on and item.parameter(item, "stealth"):value() == "off" then
		item.parameter(item, "stealth"):set_value("on")
		managers.user:set_setting("quickplay_stealth", true)
	end

	return 
end
MenuCallbackHandler.quickplay_mutators_toggle = function (self, item)
	local on = item.value(item) == "on"

	managers.user:set_setting("quickplay_mutators", on)

	return 
end
MenuCallbackHandler.quickplay_level_min = function (self, item)
	Global.crimenet.quickplay.level_diff_min = math.floor(item.value(item) + 0.5)

	return 
end
MenuCallbackHandler.quickplay_level_max = function (self, item)
	Global.crimenet.quickplay.level_diff_max = math.floor(item.value(item) + 0.5)

	return 
end
MenuCallbackHandler.save_crimenet = function (self)
	managers.savefile:save_progress()

	return 
end
MenuCallbackHandler.quickplay_difficulty = function (self, item)
	if item.value(item) == "any" then
		Global.crimenet.quickplay.difficulty = nil
	else
		Global.crimenet.quickplay.difficulty = item.value(item)
	end

	return 
end
MenuCallbackHandler.set_default_quickplay_options = function (self)
	local params = {
		text = managers.localization:text("dialog_default_quickplay_options_message"),
		callback = function ()
			managers.user:reset_quickplay_setting_map()

			Global.crimenet.quickplay = {}

			self:refresh_node()

			return 
		end
	}

	managers.menu:show_default_option_dialog(params)

	return 
end
MenuMutatorsInitiator = MenuMutatorsInitiator or class(MenuInitiatorBase)
MenuMutatorsInitiator.modify_node = function (self, node)
	node.clean_items(node)

	local function get_identifier(peer)
		return (SystemInfo:platform() == Idstring("WIN32") and peer.user_id(peer)) or peer.name(peer)
	end

	if #managers.mutators:mutators() < 1 then
		self.create_item(self, node, {
			align = "left",
			name = "no_mutators",
			enabled = false,
			text_id = "bm_menu_no_items"
		})
	else
		self.populate_mutators_list(self, node)
	end

	self.add_back_button(self, node)

	return node
end
MenuMutatorsInitiator.populate_mutators_list = function (self, node)
	self.create_item(self, node, {
		name = "header_active",
		enabled = false,
		text_id = "menu_mutators_active",
		align = "left",
		both_column = true
	})

	for i, mutator in ipairs(managers.mutators:active_mutators()) do
		self._create_mutator_node(self, node, mutator)
	end

	local params = {
		size = 16,
		name = "divider_mutators_list",
		both_column = true,
		no_text = true
	}
	local data_node = {
		type = "MenuItemDivider"
	}
	local new_item = node.create_item(node, data_node, params)

	node.add_item(node, new_item)
	self.create_item(self, node, {
		name = "header_inactive",
		enabled = false,
		text_id = "menu_mutators_inactive",
		align = "left",
		both_column = true
	})

	for i, mutator in ipairs(managers.mutators:inactive_mutators()) do
		self._create_mutator_node(self, node, mutator)
	end

	return 
end
MenuMutatorsInitiator._create_mutator_node = function (self, node, mutator)
	self.create_item(self, node, {
		localize = false,
		enabled = true,
		align = "left",
		left_column = true,
		name = mutator.id(mutator),
		text_id = mutator.name(mutator),
		mutator = mutator
	})

	if mutator.show_options(mutator) then
		self.create_item(self, node, {
			callback = "_open_mutator_options",
			enabled = true,
			text_id = "menu_mutators_option",
			options = true,
			align = "left",
			name = mutator.id(mutator) .. "_options",
			mutator = mutator
		})
	end

	return 
end
MenuMutatorsInitiator.refresh_node = function (self, node)
	for i, item in ipairs(node.items(node)) do
		if item.parameters(item).mutator and not item.parameters(item).options then
			item.set_parameter(item, "text_id", item.parameters(item).mutator:name())
			item.dirty(item)
		end
	end

	return 
end
MenuCallbackHandler._open_mutator_options = function (self, item)
	managers.menu:open_node("mutators_options", {
		item.parameters(item).mutator
	})

	return 
end
MenuCallbackHandler._update_mutator_value = function (self, item)
	if item.parameters(item).update_callback then
		item.parameters(item).update_callback(item)
	end

	return 
end
MenuSkinEditorInitiator = MenuSkinEditorInitiator or class(MenuInitiatorBase)
MenuSkinEditorInitiator.modify_node = function (self, node, data)
	data = data or {}
	local name = node.parameters(node).name
	local skin_editor = managers.blackmarket:skin_editor()

	skin_editor.set_active(skin_editor, true)

	if name == "skin_editor" then
		if data.slot and data.category then
			local crafted = managers.blackmarket:get_crafted_category_slot(data.category, data.slot)

			skin_editor.set_weapon_id(skin_editor, crafted.weapon_id)
			skin_editor.set_category_slot(skin_editor, data.category, data.slot)
			skin_editor.set_weapon_unit(skin_editor, managers.menu_scene._item_unit.unit)
			skin_editor.set_second_weapon_unit(skin_editor, managers.menu_scene._item_unit.second_unit)

			if skin_editor.get_current_skin(skin_editor) then
				skin_editor.reload_current_skin(skin_editor)
			end
		end

		local name_input = node.item(node, "name_input")

		if managers.blackmarket:skin_editor() and managers.blackmarket:skin_editor():get_current_skin() then
			local skin = managers.blackmarket:skin_editor():get_current_skin()

			name_input.set_input_text(name_input, skin.config(skin).name or "")
		else
			name_input.set_input_text(name_input, "")
		end

		local skin_exists = managers.blackmarket:skin_editor():get_current_skin() and true

		local function disable_func(item)
			if not skin_exists and item.name(item) ~= "new_skin" and item.name(item) ~= "edit_skin" then
				item.set_enabled(item, false)
			else
				item.set_enabled(item, true)
			end

			return 
		end

		table.for_each_value(node.items(node), disable_func)
	elseif name == "skin_editor_select" then
		node.clean_items(node)

		local skin_editor = managers.blackmarket:skin_editor()

		if skin_editor.skin_count(skin_editor) < 1 then
			self.create_item(self, node, {
				name = "no_skins",
				enabled = false,
				text_id = "debug_wskn_no_skin"
			})
		else
			local skins = skin_editor.skins(skin_editor)
			local default_item = skin_editor.get_current_skin(skin_editor):config().name or "untitled"

			for id, skin in ipairs(skins) do
				local name = skin.config(skin).name or "untitled"

				self.create_item(self, node, {
					enabled = true,
					localize = false,
					callback = "select_weapon_skin",
					name = name,
					text_id = name,
					skin_id = id
				})
			end

			node.set_default_item_name(node, default_item)
			node.select_item(node, default_item)
		end
	elseif name == "skin_editor_part" then
		node.clean_items(node)

		local default_item = nil
		slot6 = pairs
		slot7 = skin_editor.weapon_unit(skin_editor):base()._materials or {}

		for part_id, materials in slot6(slot7) do
			self.create_item(self, node, {
				localize = false,
				enabled = true,
				next_node = "skin_editor_materials",
				name = part_id,
				text_id = (tweak_data.weapon.factory.parts[part_id] and managers.localization:text("bm_menu_" .. tweak_data.weapon.factory.parts[part_id].type) .. " - ") .. part_id,
				next_node_parameters = {
					{
						part_id = part_id
					}
				}
			})

			default_item = default_item or part_id
		end

		node.set_default_item_name(node, default_item)
		node.select_item(node, default_item)
	elseif name == "skin_editor_type" then
		node.clean_items(node)

		local types = managers.weapon_factory._parts_by_type or {}
		local default_item = nil
		local sort_types = {}
		local excluded_types = skin_editor.get_excluded_type_categories(skin_editor)

		for type, parts in pairs(types) do
			if managers.localization:exists("bm_menu_" .. type) and not table.contains(excluded_types, type) then
				table.insert(sort_types, type)
			end
		end

		table.sort(sort_types)

		for _, mod_type in ipairs(sort_types) do
			self.create_item(self, node, {
				localize = false,
				enabled = true,
				next_node = "skin_editor_base",
				name = mod_type,
				text_id = managers.localization:text("bm_menu_" .. mod_type),
				next_node_parameters = {
					{
						mod_type = mod_type
					}
				}
			})

			default_item = default_item or mod_type
		end

		node.set_default_item_name(node, default_item)
		node.select_item(node, default_item)
	elseif name == "skin_editor_materials" then
		node.clean_items(node)

		local default_item = nil
		local items_map = {}
		local index = 1
		slot8 = pairs
		slot9 = skin_editor.weapon_unit(skin_editor):base()._materials[data.part_id] or {}

		for _, material in slot8(slot9) do
			if not items_map[material.name(material):key()] then
				self.create_item(self, node, {
					localize = false,
					enabled = true,
					next_node = "skin_editor_base",
					name = material.name(material):key(),
					text_id = "Subpart " .. index,
					next_node_parameters = {
						{
							part_id = data.part_id,
							material_name = material.name(material)
						}
					}
				})

				index = index + 1
				default_item = default_item or material.name(material):key()
				items_map[material.name(material):key()] = true
			end
		end

		node.set_default_item_name(node, default_item)
		node.select_item(node, default_item)
	elseif name == "skin_editor_base" then
		node.clean_items(node)

		local skin = skin_editor.get_current_skin(skin_editor)

		if skin then
			local cdata = skin.config(skin).data

			if data.part_id then
				cdata.parts = cdata.parts or {}
				cdata.parts[data.part_id] = cdata.parts[data.part_id] or {}

				if data.material_name then
					cdata.parts[data.part_id][data.material_name:key()] = cdata.parts[data.part_id][data.material_name:key()] or {}
					cdata = cdata.parts[data.part_id][data.material_name:key()]
				else
					cdata = cdata.parts[data.part_id]
				end
			elseif data.mod_type then
				cdata.types = cdata.types or {}
				cdata.types[data.mod_type] = cdata.types[data.mod_type] or {}
				cdata = cdata.types[data.mod_type]
			end

			skin_editor.reload_current_skin(skin_editor)

			local base_gradient_textures = skin_editor.get_texture_list_by_type(skin_editor, skin, "base_gradient")
			local multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(base_gradient_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local base_gradient_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_base_gradient",
				name = "base_gradient",
				callback = "weapon_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			base_gradient_item.set_value(base_gradient_item, cdata.base_gradient_name)

			local pattern_gradient_textures = skin_editor.get_texture_list_by_type(skin_editor, skin, "pattern_gradient")
			multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(pattern_gradient_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local pattern_gradient_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_pattern_gradient",
				name = "pattern_gradient",
				callback = "weapon_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_gradient_item.set_value(pattern_gradient_item, cdata.pattern_gradient_name)

			local pattern_textures = skin_editor.get_texture_list_by_type(skin_editor, skin, "pattern")
			multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(pattern_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local pattern_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_pattern",
				name = "pattern",
				callback = "weapon_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_item.set_value(pattern_item, cdata.pattern_name)
			self.create_divider(self, node, "sliders")

			local wear_and_tear_item = self.create_slider(self, node, {
				text_id = "debug_wskn_wear_and_tear",
				name = "wear_and_tear",
				max = 1,
				step = 0.2,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			wear_and_tear_item.set_value(wear_and_tear_item, cdata.wear_and_tear or 1)

			local pattern_pos_x_item = self.create_slider(self, node, {
				name = "pattern_pos1",
				key = "pattern_pos",
				max = 2,
				text_id = "debug_wskn_pattern_pos_x",
				step = 0.001,
				vector = 1,
				min = -2,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_pos_x_item.set_value(pattern_pos_x_item, (cdata.pattern_pos and mvector3.x(cdata.pattern_pos)) or 0)

			local pattern_pos_y_item = self.create_slider(self, node, {
				name = "pattern_pos2",
				key = "pattern_pos",
				max = 2,
				text_id = "debug_wskn_pattern_pos_y",
				step = 0.001,
				vector = 2,
				min = -2,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_pos_y_item.set_value(pattern_pos_y_item, (cdata.pattern_pos and mvector3.y(cdata.pattern_pos)) or 0)

			local pattern_tweak_x_item = self.create_slider(self, node, {
				name = "pattern_tweak1",
				key = "pattern_tweak",
				max = 20,
				text_id = "debug_wskn_pattern_tweak_x",
				step = 0.001,
				vector = 1,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_tweak_x_item.set_value(pattern_tweak_x_item, (cdata.pattern_tweak and mvector3.x(cdata.pattern_tweak)) or 1)
			self.create_divider(self, node, 1)

			local pattern_tweak_y_item = self.create_slider(self, node, {
				name = "pattern_tweak2",
				key = "pattern_tweak",
				text_id = "debug_wskn_pattern_tweak_y",
				step = 0.001,
				vector = 2,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				max = math.pi*2,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_tweak_y_item.set_value(pattern_tweak_y_item, (cdata.pattern_tweak and mvector3.y(cdata.pattern_tweak)) or 0)

			local pattern_tweak_z_item = self.create_slider(self, node, {
				name = "pattern_tweak3",
				key = "pattern_tweak",
				max = 1,
				text_id = "debug_wskn_pattern_tweak_z",
				step = 0.001,
				vector = 3,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_tweak_z_item.set_value(pattern_tweak_z_item, (cdata.pattern_tweak and mvector3.z(cdata.pattern_tweak)) or 1)
			self.create_divider(self, node, 2)

			local cubemap_pattern_control_x_item = self.create_slider(self, node, {
				name = "cubemap_pattern_control1",
				key = "cubemap_pattern_control",
				max = 1,
				text_id = "debug_wskn_cubemap_pattern_control_x",
				step = 0.001,
				vector = 1,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			cubemap_pattern_control_x_item.set_value(cubemap_pattern_control_x_item, (cdata.cubemap_pattern_control and mvector3.x(cdata.cubemap_pattern_control)) or 0)

			local cubemap_pattern_control_y_item = self.create_slider(self, node, {
				name = "cubemap_pattern_control2",
				key = "cubemap_pattern_control",
				max = 1,
				text_id = "debug_wskn_cubemap_pattern_control_y",
				step = 0.001,
				vector = 2,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			cubemap_pattern_control_y_item.set_value(cubemap_pattern_control_y_item, (cdata.cubemap_pattern_control and mvector3.y(cdata.cubemap_pattern_control)) or 0)
			self.create_divider(self, node, "sticker")
			self.create_divider(self, node, "sticker2")

			local sticker_textures = skin_editor.get_texture_list_by_type(skin_editor, skin, "sticker")
			multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(sticker_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local sticker_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_sticker",
				name = "sticker",
				callback = "weapon_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			sticker_item.set_value(sticker_item, cdata.sticker_name)

			local uv_offset_rot_x_item = self.create_slider(self, node, {
				name = "uv_offset_rot1",
				key = "uv_offset_rot",
				max = 2,
				text_id = "debug_wskn_uv_offset_rot_x",
				step = 0.001,
				vector = 1,
				min = -2,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_offset_rot_x_item.set_value(uv_offset_rot_x_item, (cdata.uv_offset_rot and mvector3.x(cdata.uv_offset_rot)) or 0)

			local uv_offset_rot_y_item = self.create_slider(self, node, {
				name = "uv_offset_rot2",
				key = "uv_offset_rot",
				max = 2,
				text_id = "debug_wskn_uv_offset_rot_y",
				step = 0.001,
				vector = 2,
				min = -2,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_offset_rot_y_item.set_value(uv_offset_rot_y_item, (cdata.uv_offset_rot and mvector3.y(cdata.uv_offset_rot)) or 0)

			local uv_scale_x_item = self.create_slider(self, node, {
				name = "uv_scale1",
				key = "uv_scale",
				max = 20,
				text_id = "debug_wskn_uv_scale_x",
				step = 0.001,
				vector = 1,
				min = 0.01,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_scale_x_item.set_value(uv_scale_x_item, ((cdata.uv_scale and mvector3.x(cdata.uv_scale)) or 1) - 20.01)

			local uv_scale_y_item = self.create_slider(self, node, {
				name = "uv_scale2",
				key = "uv_scale",
				max = 20,
				text_id = "debug_wskn_uv_scale_y",
				step = 0.001,
				vector = 2,
				min = 0.01,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_scale_y_item.set_value(uv_scale_y_item, ((cdata.uv_scale and mvector3.y(cdata.uv_scale)) or 1) - 20.01)

			local uv_scale_lock_item = self.create_toggle(self, node, {
				localize = true,
				name = "uv_scale_lock",
				enabled = true,
				text_id = "debug_wskn_uv_scale_lock"
			})

			uv_scale_lock_item.set_value(uv_scale_lock_item, "on")
			self.create_divider(self, node, 3)

			local uv_offset_rot_z_item = self.create_slider(self, node, {
				name = "uv_offset_rot3",
				key = "uv_offset_rot",
				text_id = "debug_wskn_uv_offset_rot_z",
				step = 0.001,
				vector = 3,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				max = math.pi*2,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_offset_rot_z_item.set_value(uv_offset_rot_z_item, (cdata.uv_offset_rot and mvector3.z(cdata.uv_offset_rot)) or 0)

			local uv_scale_z_item = self.create_slider(self, node, {
				name = "uv_scale3",
				key = "uv_scale",
				max = 1,
				text_id = "debug_wskn_uv_scale_z",
				step = 0.001,
				vector = 3,
				min = 0,
				callback = "weapon_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_scale_z_item.set_value(uv_scale_z_item, (cdata.uv_scale and mvector3.z(cdata.uv_scale)) or 1)
			node.set_default_item_name(node, "base_gradient")
			node.select_item(node, "base_gradient")
		end
	elseif name == "skin_editor_screenshot" then
		local multichoice_list = {
			{
				value = "none",
				text_id = "debug_wskn_none",
				_meta = "option"
			},
			{
				value = "green",
				text_id = "debug_wskn_green",
				_meta = "option"
			},
			{
				value = "black",
				text_id = "debug_wskn_black",
				_meta = "option"
			},
			{
				value = "red",
				text_id = "debug_wskn_red",
				_meta = "option"
			},
			{
				value = "blue",
				text_id = "debug_wskn_blue",
				_meta = "option"
			},
			{
				value = "pink",
				text_id = "debug_wskn_pink",
				_meta = "option"
			},
			{
				value = "cyan",
				text_id = "debug_wskn_cyan",
				_meta = "option"
			},
			{
				value = "yellow",
				text_id = "debug_wskn_yellow",
				_meta = "option"
			},
			{
				value = "white",
				text_id = "debug_wskn_white",
				_meta = "option"
			}
		}

		if node.item(node, "screenshot_color") then
			node.delete_item(node, "screenshot_color")
		end

		if node.item(node, "wear_and_tear") then
			node.delete_item(node, "wear_and_tear")
		end

		local color_item = self.create_multichoice(self, node, multichoice_list, {
			text_id = "debug_wskn_color",
			text_offset = 50,
			name = "screenshot_color",
			callback = "screenshot_color_changed"
		})

		color_item.set_value(color_item, "none")

		local skin_data = skin_editor.get_current_skin(skin_editor):config().data
		local wear_and_tear_item = self.create_slider(self, node, {
			step = 0.2,
			min = 0,
			max = 1,
			callback = "wear_and_tear_changed",
			text_id = "debug_wskn_wear_and_tear",
			name = "wear_and_tear",
			show_value = true
		})

		wear_and_tear_item.set_value(wear_and_tear_item, skin_data.wear_and_tear or 1)
		managers.menu:active_menu().renderer.ws:panel():rect({
			name = "screenshot_visibility",
			h = 85,
			y = 35,
			w = 450,
			x = 775,
			color = Color(0, 0, 0)
		})
		skin_editor.enter_screenshot_mode(skin_editor)
	elseif name == "skin_editor_publish" then
		local title_input = node.item(node, "title_input")
		local desc_input = node.item(node, "desc_input")

		if managers.blackmarket:skin_editor() and managers.blackmarket:skin_editor():get_current_skin() then
			local skin = skin_editor.get_current_skin(skin_editor)

			title_input.set_input_text(title_input, skin.title(skin) or "")
			desc_input.set_input_text(desc_input, skin.description(skin) or "")
		end

		if node.item(node, "screenshot") then
			node.delete_item(node, "screenshot")
		end

		local multichoice_list = {
			{
				text_id = "NONE",
				localize = false,
				_meta = "option"
			}
		}

		if skin_editor.has_screenshots(skin_editor, skin_editor.get_current_skin(skin_editor)) then
			local screenshots = skin_editor.get_screenshot_list(skin_editor)

			for id, screenshot in ipairs(screenshots) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = screenshot,
					value = screenshot
				})
			end
		end

		local item_index = table.index_of(node.items(node), node.item(node, "divider_publish"))
		local screenshot_item = self.create_multichoice(self, node, multichoice_list, {
			text_id = "debug_wskn_screenshot_skin",
			text_offset = 50,
			name = "screenshot",
			callback = "screenshot_chosen"
		}, item_index)

		screenshot_item.set_value(screenshot_item, nil)
	end

	if not node.item(node, "divider_end") then
		self.create_divider(self, node, "end")
		self.add_back_button(self, node)
	end

	return node
end
MenuCallbackHandler.convert_skin = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()
	local skin = skin_editor.get_current_skin(skin_editor)

	skin_editor.convert_file_layout(skin_editor, skin)
	item.set_enabled(item, false)

	return 
end
MenuCallbackHandler.need_convert_skin = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()
	local skin = skin_editor.get_current_skin(skin_editor)

	return skin and not skin_editor.has_texture_folders(skin_editor, skin)
end
MenuCallbackHandler.should_add_changelog = function (self, item)
	return managers.blackmarket:skin_editor():get_current_skin():item_exists()
end
MenuCallbackHandler.browse_skin = function (self, item)
	local skin = managers.blackmarket:skin_editor():get_current_skin()
	local path = Application:nice_path(skin.path(skin), false)

	Application:shell_explore_to_folder(path)

	return 
end
MenuCallbackHandler.screenshot_chosen = function (self, item)
	local skin = managers.blackmarket:skin_editor():get_current_skin()
	skin.config(skin).screenshot = item.value(item)

	return 
end
MenuCallbackHandler.wear_and_tear_changed = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()
	local wear_and_tear = item.value(item)
	local skin_data = skin_editor.get_current_skin(skin_editor):config().data
	skin_data.wear_and_tear = wear_and_tear

	skin_editor.apply_changes(skin_editor, skin_data)

	return 
end
MenuCallbackHandler.screenshot_color_changed = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()

	if item.value(item) == "none" then
		skin_editor.hide_screenshot_bg(skin_editor)
	elseif item.value(item) == "green" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 1, 0))
	elseif item.value(item) == "black" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 0, 0))
	elseif item.value(item) == "red" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 0, 0))
	elseif item.value(item) == "blue" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 0, 1))
	elseif item.value(item) == "cyan" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 1, 1))
	elseif item.value(item) == "pink" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 0, 1))
	elseif item.value(item) == "yellow" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 1, 0))
	elseif item.value(item) == "white" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 1, 1))
	end

	return 
end
MenuCallbackHandler.leave_screenshot_menu = function (self, item)
	managers.blackmarket:skin_editor():leave_screenshot_mode()
	managers.blackmarket:skin_editor():reload_current_skin()

	local visibility_bg = managers.menu:active_menu().renderer.ws:panel():child("screenshot_visibility")

	if visibility_bg then
		managers.menu:active_menu().renderer.ws:panel():remove(visibility_bg)
	end

	return 
end
MenuCallbackHandler.on_exit_skin_editor = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()

	if not skin_editor.unsaved(skin_editor) then
		skin_editor.set_ignore_unsaved(skin_editor, false)

		skin_editor._unsaved = false
		local cat, slot = skin_editor.category_slot(skin_editor)

		managers.blackmarket:view_weapon(cat, slot, function ()
			return 
		end, true, BlackMarketGui.get_crafting_custom_data())
		skin_editor.set_active(skin_editor, false)

		return false
	end

	local function on_yes()
		managers.blackmarket:skin_editor():save_current_skin()
		managers.menu:back(true)

		return 
	end

	local function on_no()
		managers.blackmarket:skin_editor():set_ignore_unsaved(true)
		managers.menu:back(true)

		return 
	end

	local dialog_data = {
		title = managers.localization:text("dialog_warning_title"),
		text = managers.localization:text("debug_wskn_want_to_save")
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = on_yes
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = on_no
	}
	local cancel_button = {
		text = managers.localization:text("dialog_cancel"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button,
		cancel_button
	}

	managers.system_menu:show(dialog_data)

	return true
end
MenuCallbackHandler.clear_weapon_skin = function (self)
	managers.blackmarket:skin_editor():clear_current_skin()

	return 
end
MenuCallbackHandler.save_weapon_skin = function (self, item)
	local crafted_item = managers.blackmarket:get_crafted_category_slot(managers.blackmarket:skin_editor():category_slot())
	local name = managers.menu:active_menu().logic:selected_node():item("name_input"):input_text()

	if not name or name == "" then
		name = "My Skin"
	end

	local name_id = string.gsub(string.lower(name), " ", "_")
	local item_id = crafted_item.weapon_id .. "_" .. name_id
	local copy_data = deep_clone(managers.blackmarket:skin_editor():get_current_skin():config().data)
	copy_data.name_id = "bm_wskn_" .. item_id
	copy_data.wear_and_tear = nil
	copy_data.reserve_quality = true

	managers.blackmarket:skin_editor():save_current_skin(name, copy_data)

	return 
end
MenuCallbackHandler.publish_weapon_skin = function (self, item)
	local title = managers.menu:active_menu().logic:selected_node():item("title_input"):input_text()
	local desc = managers.menu:active_menu().logic:selected_node():item("desc_input"):input_text()
	local changelog = managers.menu:active_menu().logic:selected_node():item("changelog_input"):input_text()

	if not changelog or changelog == "" then
		changelog = "Initial submission"
	end

	local skin_editor = managers.blackmarket:skin_editor()
	local skin = skin_editor.get_current_skin(skin_editor)

	if skin.config(skin).screenshot then
		local screenshot_path = skin_editor.get_screenshot_path(skin_editor, skin)

		SystemFS:copy_file(screenshot_path .. "/" .. skin.config(skin).screenshot, Application:nice_path(skin.path(skin), true) .. "preview.png")
	else
		local dialog_data = {
			title = managers.localization:text("dialog_warning_title"),
			text = managers.localization:text("debug_wskn_submit_no_screenshot")
		}
		local ok_button = {
			text = managers.localization:text("dialog_ok"),
			callback_func = callback(self, self, "_dialog_ok")
		}
		dialog_data.button_list = {
			ok_button
		}

		managers.system_menu:show(dialog_data)

		return 
	end

	skin_editor.publish_skin(skin_editor, skin, title, desc, changelog)

	return 
end
MenuCallbackHandler._dialog_ok = function (self)
	return 
end
MenuCallbackHandler.take_screenshot_skin = function (self, item)
	local function screenshot_done(success)
		managers.mouse_pointer:enable()
		managers.menu:active_menu().renderer:show()
		managers.menu:active_menu().renderer.ws:panel():child("screenshot_visibility"):show()
		item:set_enabled(true)

		return 
	end

	managers.menu:active_menu().renderer:hide()
	managers.mouse_pointer:disable()
	managers.menu:active_menu().renderer.ws:panel():child("screenshot_visibility"):hide()

	local name = managers.blackmarket:skin_editor():get_screenshot_name()
	local x, y, w, h = managers.blackmarket:skin_editor():get_screenshot_rect()

	item.set_enabled(item, false)

	local function co_screenshot(o)
		for i = 0, 5, 1 do
			coroutine.yield()
		end

		Application:screenshot(name, x, y, w, h, true, screenshot_done, 1024, 576)

		return 
	end

	managers.menu:active_menu().renderer.ws:panel():animate(co_screenshot)

	return 
end
MenuCallbackHandler.new_weapon_skin = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()

	if not skin_editor then
		return 
	end

	local data = {
		weapon_id = managers.blackmarket:get_crafted_category_slot(managers.blackmarket:skin_editor():category_slot()).weapon_id
	}

	skin_editor.select_skin(skin_editor, skin_editor.create_new_skin(skin_editor, data))

	return 
end
MenuCallbackHandler.delete_weapon_skin = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()

	if not skin_editor then
		return 
	end

	local dialog_data = {
		title = managers.localization:text("dialog_warning_title"),
		text = managers.localization:text("debug_wskn_sure_to_delete")
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = callback(self, self, "_dialog_delete_yes")
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = callback(self, self, "_dialog_delete_no"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}

	managers.system_menu:show(dialog_data)

	return 
end
MenuCallbackHandler._dialog_delete_no = function (self)
	return 
end
MenuCallbackHandler._dialog_delete_yes = function (self)
	managers.blackmarket:skin_editor():delete_current()

	return 
end
MenuCallbackHandler.select_weapon_skin = function (self, item)
	local skin_editor = managers.blackmarket:skin_editor()

	if not skin_editor then
		return 
	end

	skin_editor.select_skin(skin_editor, item.parameters(item).skin_id)

	return 
end
MenuCallbackHandler.cleanup_weapon_skin_data = function (self, copy_data, skip_base)
	local function remove_empty_func(data)
		local remove = {}

		for key, v in pairs(data) do
			if key == "pattern_tweak" and v == Vector3(1, 0, 1) then
				table.insert(remove, key)
			elseif key == "pattern_pos" and v == Vector3(0, 0, 0) then
				table.insert(remove, key)
			elseif key == "uv_scale" and v == Vector3(1, 1, 1) then
				table.insert(remove, key)
			elseif key == "uv_offset_rot" and v == Vector3(0, 0, 0) then
				table.insert(remove, key)
			elseif key == "cubemap_pattern_control" and v == Vector3(0, 0, 0) then
				table.insert(remove, key)
			elseif key == "wear_and_tear" and v == 1 then
				table.insert(remove, key)
			end
		end

		if not data.pattern then
			table.insert(remove, "pattern_tweak")
			table.insert(remove, "pattern_pos")
		end

		if not data.sticker then
			table.insert(remove, "uv_offset_rot")
			table.insert(remove, "uv_scale")
		end

		for _, key in ipairs(remove) do
			data[key] = nil
		end

		return 
	end

	if not skip_base then
		remove_empty_func(copy_data)
	end

	if copy_data.parts then
		local remove_parts = {}

		for part_id, materials in pairs(copy_data.parts) do
			local remove_materials = {}

			for k, data in pairs(materials) do
				data.wear_and_tear = nil

				remove_empty_func(data)

				if table.size(data) == 0 then
					table.insert(remove_materials, k)
				end
			end

			for _, key in ipairs(remove_materials) do
				materials[key] = nil
			end

			if table.size(materials) == 0 then
				table.insert(remove_parts, part_id)
			end
		end

		for _, part_id in ipairs(remove_parts) do
			copy_data.parts[part_id] = nil
		end

		if copy_data.parts and table.size(copy_data.parts) == 0 then
			copy_data.parts = nil
		end
	end

	if copy_data.types then
		local remove_types = {}

		for type_id, data in pairs(copy_data.types) do
			remove_empty_func(data)

			if table.size(data) == 0 then
				table.insert(remove_types, type_id)
			end
		end

		for _, type_id in ipairs(remove_types) do
			copy_data.types[type_id] = nil
		end

		if copy_data.types and table.size(copy_data.types) == 0 then
			copy_data.types = nil
		end
	end

	return 
end
MenuCallbackHandler.weapon_skin_changed = function (self, item)
	local key = item.parameters(item).key or item.name(item)
	local part_id = item.parameters(item).part_id
	local material_name = item.parameters(item).material_name
	local mod_type = item.parameters(item).mod_type
	local value = item.value(item)
	local vector = item.parameters(item).vector
	local skin_editor = managers.blackmarket:skin_editor()
	local skin = skin_editor.get_current_skin(skin_editor)

	if not skin then
		return 
	end

	local data = skin.config(skin).data

	if part_id then
		data.parts = data.parts or {}
		data.parts[part_id] = data.parts[part_id] or {}

		if material_name then
			data.parts[part_id][material_name.key(material_name)] = data.parts[part_id][material_name.key(material_name)] or {}
			data = data.parts[part_id][material_name.key(material_name)]
		else
			data = data.parts[part_id]
		end
	elseif mod_type then
		data.types = data.types or {}
		data.types[mod_type] = data.types[mod_type] or {}
		data = data.types[mod_type]
	end

	if value then
		local lock = false

		if string.find(item.parameters(item).name, "uv_scale[1-2]") then
			value = value - 20.01
			local lock_item = managers.menu:active_menu().logic:selected_node():item("uv_scale_lock")

			if lock_item then
				lock = lock_item.value(lock_item) == "on"
			end
		end

		if vector then
			local v = data[key]

			if not v then
				local i1 = managers.menu:active_menu().logic:selected_node():item(key .. "1")
				local i2 = managers.menu:active_menu().logic:selected_node():item(key .. "2")
				local i3 = managers.menu:active_menu().logic:selected_node():item(key .. "3")
				local i1v = (i1 and i1.value(i1)) or 0
				local i2v = (i2 and i2.value(i2)) or 0
				local i3v = (i3 and i3.value(i3)) or 0

				if string.find(item.parameters(item).name, "uv_scale") then
					i1v = i1v - 20.01
					i2v = i2v - 20.01
				end

				v = Vector3(i1v, i2v, i3v)
			end

			if vector == 1 then
				mvector3.set_x(v, value)

				if lock then
					mvector3.set_y(v, value)
				end
			elseif vector == 2 then
				mvector3.set_y(v, value)

				if lock then
					mvector3.set_x(v, value)
				end
			elseif vector == 3 then
				mvector3.set_z(v, value)
			end

			value = v
			local i1 = managers.menu:active_menu().logic:selected_node():item(key .. "1")
			local i2 = managers.menu:active_menu().logic:selected_node():item(key .. "2")
			local i3 = managers.menu:active_menu().logic:selected_node():item(key .. "3")

			if string.find(item.parameters(item).name, "uv_scale") then
				i1.set_value(i1, mvector3.x(value) - 20.01)
				i2.set_value(i2, mvector3.y(value) - 20.01)
			else
				i1.set_value(i1, mvector3.x(value))
				i2.set_value(i2, mvector3.y(value))
			end

			if i3 then
				i3.set_value(i3, mvector3.z(value))
			end
		elseif item.parameters(item).type ~= "CoreMenuItemSlider.ItemSlider" then
			local orig_value = value
			value = skin_editor.get_texture_string(skin_editor, skin, orig_value, key)
			data[key .. "_name"] = orig_value

			skin_editor.load_textures(skin_editor, skin)

			if not skin_editor.check_texture_db(skin_editor, value) then
				return 
			elseif not skin_editor.check_texture_disk(skin_editor, value) then
				if not item.options then
					return 
				end

				local index = table.index_of(item.options(item), table.find_value(item.options(item), function (v)
					return v.value(v) == orig_value
				end))

				item.clear_options(item)
				item.add_option(item, CoreMenuItemOption.ItemOption:new({
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}))
				skin_editor.load_textures(skin_editor, skin)

				local textures = skin_editor.get_texture_list_by_type(skin_editor, skin, key)

				for _, texture in ipairs(textures) do
					item.add_option(item, CoreMenuItemOption.ItemOption:new({
						localize = false,
						_meta = "option",
						text_id = texture,
						value = texture
					}))
				end

				item.dirty(item)

				value = nil
				data[key .. "_name"] = nil
			end
		end
	else
		data[key .. "_name"] = nil
	end

	data[key] = value

	self.cleanup_weapon_skin_data(self, data)
	skin_editor.apply_changes(skin_editor, skin.config(skin).data)

	return 
end
MenuCallbackHandler.toggle_controller_hint = function (self, item)
	managers.user:set_setting("loading_screen_show_controller", item.value(item) == "on")

	return 
end
MenuCallbackHandler.toggle_loading_hints = function (self, item)
	managers.user:set_setting("loading_screen_show_hints", item.value(item) == "on")

	return 
end
MenuArmorSkinEditorInitiator = MenuArmorSkinEditorInitiator or class(MenuInitiatorBase)
MenuArmorSkinEditorInitiator.modify_node = function (self, node, data)
	data = data or {}
	local name = node.parameters(node).name
	local editor = managers.blackmarket:armor_skin_editor()

	editor.set_active(editor, true)
	call_on_next_update(function ()
		local skin = editor:get_current_skin()

		if skin then
			local cdata = skin.config(skin).data

			editor:apply_changes_to_character(cdata)
		end

		return 
	end)

	if name == "armor_skin_editor" then
		local name_input = node.item(node, "name_input")

		if editor and editor.get_current_skin(editor) then
			local skin = editor.get_current_skin(editor)

			name_input.set_input_text(name_input, skin.config(skin).name or "")
		else
			name_input.set_input_text(name_input, "")
		end

		local skin_exists = editor.get_current_skin(editor) and true

		local function disable_func(item)
			if not skin_exists and item.name(item) ~= "new_skin" and item.name(item) ~= "edit_skin" then
				item.set_enabled(item, false)
			else
				item.set_enabled(item, true)
			end

			return 
		end

		table.for_each_value(node.items(node), disable_func)
	elseif name == "armor_skin_editor_select" then
		node.clean_items(node)

		if editor.skin_count(editor) < 1 then
			self.create_item(self, node, {
				name = "no_skins",
				enabled = false,
				text_id = "debug_wskn_no_skin"
			})
		else
			local skins = editor.skins(editor)
			local default_item = editor.get_current_skin(editor):config().name or "untitled"

			for id, skin in ipairs(skins) do
				local name = skin.config(skin).name or "untitled"

				self.create_item(self, node, {
					enabled = true,
					localize = false,
					callback = "select_armor_skin",
					name = name,
					text_id = name,
					skin_id = id
				})
			end

			node.set_default_item_name(node, default_item)
			node.select_item(node, default_item)
		end
	elseif name == "armor_skin_editor_base" then
		node.clean_items(node)

		local skin = editor.get_current_skin(editor)

		if skin then
			local cdata = skin.config(skin).data

			editor.reload_current_skin(editor)

			local armor_level = MenuCallbackHandler:editor_get_armor_level()
			local base_gradient_textures = editor.get_texture_list_by_type(editor, skin, "base_gradient")
			local multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(base_gradient_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local base_gradient_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_base_gradient",
				name = "base_gradient",
				callback = "armor_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			base_gradient_item.set_value(base_gradient_item, tweak_data.economy:get_armor_based_value(cdata.base_gradient_name or cdata.base_gradient, armor_level))

			local pattern_gradient_textures = editor.get_texture_list_by_type(editor, skin, "pattern_gradient")
			multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(pattern_gradient_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local pattern_gradient_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_pattern_gradient",
				name = "pattern_gradient",
				callback = "armor_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_gradient_item.set_value(pattern_gradient_item, tweak_data.economy:get_armor_based_value(cdata.pattern_gradient_name or cdata.pattern_gradient, armor_level))

			local pattern_textures = editor.get_texture_list_by_type(editor, skin, "pattern")
			multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(pattern_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local pattern_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_pattern",
				name = "pattern",
				callback = "armor_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_item.set_value(pattern_item, tweak_data.economy:get_armor_based_value(cdata.pattern_name or cdata.pattern, armor_level))
			self.create_divider(self, node, "sliders")

			local pattern_pos = tweak_data.economy:get_armor_based_value(cdata.pattern_pos, armor_level)
			local pattern_tweak = tweak_data.economy:get_armor_based_value(cdata.pattern_tweak, armor_level)
			local cubemap_pattern_control = tweak_data.economy:get_armor_based_value(cdata.cubemap_pattern_control, armor_level)
			local pattern_pos_x_item = self.create_slider(self, node, {
				name = "pattern_pos1",
				key = "pattern_pos",
				max = 2,
				text_id = "debug_wskn_pattern_pos_x",
				step = 0.001,
				vector = 1,
				min = -2,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_pos_x_item.set_value(pattern_pos_x_item, (pattern_pos and mvector3.x(pattern_pos)) or 0)

			local pattern_pos_y_item = self.create_slider(self, node, {
				name = "pattern_pos2",
				key = "pattern_pos",
				max = 2,
				text_id = "debug_wskn_pattern_pos_y",
				step = 0.001,
				vector = 2,
				min = -2,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_pos_y_item.set_value(pattern_pos_y_item, (pattern_pos and mvector3.y(pattern_pos)) or 0)

			local pattern_tweak_x_item = self.create_slider(self, node, {
				name = "pattern_tweak1",
				key = "pattern_tweak",
				max = 20,
				text_id = "debug_wskn_pattern_tweak_x",
				step = 0.001,
				vector = 1,
				min = 0,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_tweak_x_item.set_value(pattern_tweak_x_item, (pattern_tweak and mvector3.x(pattern_tweak)) or 1)
			self.create_divider(self, node, 1)

			local pattern_tweak_y_item = self.create_slider(self, node, {
				name = "pattern_tweak2",
				key = "pattern_tweak",
				text_id = "debug_wskn_pattern_tweak_y",
				step = 0.001,
				vector = 2,
				min = 0,
				callback = "armor_skin_changed",
				show_value = true,
				max = math.pi*2,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_tweak_y_item.set_value(pattern_tweak_y_item, (pattern_tweak and mvector3.y(pattern_tweak)) or 0)

			local pattern_tweak_z_item = self.create_slider(self, node, {
				name = "pattern_tweak3",
				key = "pattern_tweak",
				max = 1,
				text_id = "debug_wskn_pattern_tweak_z",
				step = 0.001,
				vector = 3,
				min = 0,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			pattern_tweak_z_item.set_value(pattern_tweak_z_item, (pattern_tweak and mvector3.z(pattern_tweak)) or 1)
			self.create_divider(self, node, 2)

			local cubemap_pattern_control_x_item = self.create_slider(self, node, {
				name = "cubemap_pattern_control1",
				key = "cubemap_pattern_control",
				max = 1,
				text_id = "debug_wskn_cubemap_pattern_control_x",
				step = 0.001,
				vector = 1,
				min = 0,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			cubemap_pattern_control_x_item.set_value(cubemap_pattern_control_x_item, (cubemap_pattern_control and mvector3.x(cubemap_pattern_control)) or 0)

			local cubemap_pattern_control_y_item = self.create_slider(self, node, {
				name = "cubemap_pattern_control2",
				key = "cubemap_pattern_control",
				max = 1,
				text_id = "debug_wskn_cubemap_pattern_control_y",
				step = 0.001,
				vector = 2,
				min = 0,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			cubemap_pattern_control_y_item.set_value(cubemap_pattern_control_y_item, (cubemap_pattern_control and mvector3.y(cubemap_pattern_control)) or 0)
			self.create_divider(self, node, "sticker")
			self.create_divider(self, node, "sticker2")

			local sticker_textures = editor.get_texture_list_by_type(editor, skin, "sticker")
			multichoice_list = {
				{
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}
			}

			for id, texture in ipairs(sticker_textures) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = texture,
					value = texture
				})
			end

			local sticker_item = self.create_multichoice(self, node, multichoice_list, {
				text_id = "debug_wskn_sticker",
				name = "sticker",
				callback = "armor_skin_changed",
				text_offset = 50,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			sticker_item.set_value(sticker_item, tweak_data.economy:get_armor_based_value(cdata.sticker_name or cdata.sticker, armor_level))

			local uv_offset_rot = tweak_data.economy:get_armor_based_value(cdata.uv_offset_rot, armor_level)
			local uv_scale = tweak_data.economy:get_armor_based_value(cdata.uv_scale, armor_level)
			local uv_offset_rot_x_item = self.create_slider(self, node, {
				name = "uv_offset_rot1",
				key = "uv_offset_rot",
				max = 2,
				text_id = "debug_wskn_uv_offset_rot_x",
				step = 0.001,
				vector = 1,
				min = -2,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_offset_rot_x_item.set_value(uv_offset_rot_x_item, (uv_offset_rot and mvector3.x(uv_offset_rot)) or 0)

			local uv_offset_rot_y_item = self.create_slider(self, node, {
				name = "uv_offset_rot2",
				key = "uv_offset_rot",
				max = 2,
				text_id = "debug_wskn_uv_offset_rot_y",
				step = 0.001,
				vector = 2,
				min = -2,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_offset_rot_y_item.set_value(uv_offset_rot_y_item, (uv_offset_rot and mvector3.y(uv_offset_rot)) or 0)

			local uv_scale_x_item = self.create_slider(self, node, {
				name = "uv_scale1",
				key = "uv_scale",
				max = 20,
				text_id = "debug_wskn_uv_scale_x",
				step = 0.001,
				vector = 1,
				min = 0.01,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_scale_x_item.set_value(uv_scale_x_item, ((uv_scale and mvector3.x(uv_scale)) or 1) - 20.01)

			local uv_scale_y_item = self.create_slider(self, node, {
				name = "uv_scale2",
				key = "uv_scale",
				max = 20,
				text_id = "debug_wskn_uv_scale_y",
				step = 0.001,
				vector = 2,
				min = 0.01,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_scale_y_item.set_value(uv_scale_y_item, ((uv_scale and mvector3.y(uv_scale)) or 1) - 20.01)

			local uv_scale_lock_item = self.create_toggle(self, node, {
				localize = true,
				name = "uv_scale_lock",
				enabled = true,
				text_id = "debug_wskn_uv_scale_lock"
			})

			uv_scale_lock_item.set_value(uv_scale_lock_item, "on")
			self.create_divider(self, node, 3)

			local uv_offset_rot_z_item = self.create_slider(self, node, {
				name = "uv_offset_rot3",
				key = "uv_offset_rot",
				text_id = "debug_wskn_uv_offset_rot_z",
				step = 0.001,
				vector = 3,
				min = 0,
				callback = "armor_skin_changed",
				show_value = true,
				max = math.pi*2,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_offset_rot_z_item.set_value(uv_offset_rot_z_item, (uv_offset_rot and mvector3.z(uv_offset_rot)) or 0)

			local uv_scale_z_item = self.create_slider(self, node, {
				name = "uv_scale3",
				key = "uv_scale",
				max = 1,
				text_id = "debug_wskn_uv_scale_z",
				step = 0.001,
				vector = 3,
				min = 0,
				callback = "armor_skin_changed",
				show_value = true,
				part_id = data.part_id,
				material_name = data.material_name,
				mod_type = data.mod_type
			})

			uv_scale_z_item.set_value(uv_scale_z_item, (uv_scale and mvector3.z(uv_scale)) or 1)
			node.set_default_item_name(node, "base_gradient")
			node.select_item(node, "base_gradient")
		end
	elseif name == "armor_skin_editor_screenshot" then
		local multichoice_list = {
			{
				value = "none",
				text_id = "debug_wskn_none",
				_meta = "option"
			},
			{
				value = "green",
				text_id = "debug_wskn_green",
				_meta = "option"
			},
			{
				value = "black",
				text_id = "debug_wskn_black",
				_meta = "option"
			},
			{
				value = "red",
				text_id = "debug_wskn_red",
				_meta = "option"
			},
			{
				value = "blue",
				text_id = "debug_wskn_blue",
				_meta = "option"
			},
			{
				value = "pink",
				text_id = "debug_wskn_pink",
				_meta = "option"
			},
			{
				value = "cyan",
				text_id = "debug_wskn_cyan",
				_meta = "option"
			},
			{
				value = "yellow",
				text_id = "debug_wskn_yellow",
				_meta = "option"
			},
			{
				value = "white",
				text_id = "debug_wskn_white",
				_meta = "option"
			}
		}

		if node.item(node, "screenshot_color") then
			node.delete_item(node, "screenshot_color")
		end

		local color_item = self.create_multichoice(self, node, multichoice_list, {
			text_id = "debug_wskn_color",
			text_offset = 50,
			name = "screenshot_color",
			callback = "armor_screenshot_color_changed"
		})

		color_item.set_value(color_item, "none")

		if node.item(node, "hide_weapons") then
			node.delete_item(node, "hide_weapons")
		end

		self.create_item(self, node, {
			localize = true,
			name = "hide_weapons",
			enabled = true,
			text_id = "bm_menu_btn_hide_weapons",
			callback = "armor_screenshots_hide_weapons"
		})

		if node.item(node, "show_weapons") then
			node.delete_item(node, "show_weapons")
		end

		self.create_item(self, node, {
			localize = true,
			name = "show_weapons",
			enabled = true,
			text_id = "bm_menu_btn_show_weapons",
			callback = "armor_screenshots_show_weapons"
		})
		editor.enter_screenshot_mode(editor)
	elseif name == "armor_skin_editor_screenshot_level" or name == "armor_skin_editor_select_level" then
		local sort_types = {}

		for armor_name, armor_table in pairs(tweak_data.blackmarket.armors) do
			if not table.contains(sort_types, armor_name) then
				table.insert(sort_types, armor_name)
			end
		end

		table.sort(sort_types, function (a, b)
			return b < a
		end)

		for _, armor_name in ipairs(sort_types) do
			if node.item(node, armor_name) then
				node.delete_item(node, armor_name)
			end

			self.create_item(self, node, {
				localize = true,
				enabled = true,
				callback = "select_armor_skin_level",
				name = armor_name,
				text_id = "bm_armor_" .. armor_name
			})
		end
	elseif name == "armor_skin_editor_screenshot_pose" or name == "armor_skin_editor_select_pose" then
		local sort_types = {}

		for pose_name, pose_table in pairs(managers.menu_scene._global_poses) do
			for _, pose in ipairs(pose_table) do
				if not table.contains(sort_types, pose) then
					table.insert(sort_types, pose)
				end
			end
		end

		table.sort(sort_types)

		for _, pose in ipairs(sort_types) do
			if node.item(node, pose) then
				node.delete_item(node, pose)
			end

			self.create_item(self, node, {
				localize = false,
				enabled = true,
				callback = "select_armor_skin_pose",
				name = pose,
				text_id = pose
			})
		end
	elseif name == "armor_skin_editor_publish" then
		local title_input = node.item(node, "title_input")
		local desc_input = node.item(node, "desc_input")

		if editor and editor.get_current_skin(editor) then
			local skin = editor.get_current_skin(editor)

			title_input.set_input_text(title_input, skin.title(skin) or "")
			desc_input.set_input_text(desc_input, skin.description(skin) or "")
		end

		if node.item(node, "screenshot") then
			node.delete_item(node, "screenshot")
		end

		local multichoice_list = {
			{
				text_id = "NONE",
				localize = false,
				_meta = "option"
			}
		}

		if editor.has_screenshots(editor, editor.get_current_skin(editor)) then
			local screenshots = editor.get_screenshot_list(editor)

			for id, screenshot in ipairs(screenshots) do
				table.insert(multichoice_list, {
					localize = false,
					_meta = "option",
					text_id = screenshot,
					value = screenshot
				})
			end
		end

		local item_index = table.index_of(node.items(node), node.item(node, "divider_publish"))
		local screenshot_item = self.create_multichoice(self, node, multichoice_list, {
			text_id = "debug_wskn_screenshot_skin",
			text_offset = 50,
			name = "screenshot",
			callback = "armor_screenshot_chosen"
		}, item_index)

		screenshot_item.set_value(screenshot_item, nil)
	end

	if not node.item(node, "divider_end") then
		self.create_divider(self, node, "end")
		self.add_back_button(self, node)
	end

	return node
end
MenuCallbackHandler.clear_armor_skin = function (self)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		editor.clear_current_skin(editor)
	end

	return 
end
MenuCallbackHandler.new_armor_skin = function (self)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		local skin = editor.create_new_skin(editor, {})

		editor.select_skin(editor, skin)
	end

	return 
end
MenuCallbackHandler.select_armor_skin = function (self, item)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		editor.select_skin(editor, item.parameters(item).skin_id)
	end

	return 
end
MenuCallbackHandler.delete_armor_skin = function (self)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		local dialog_data = {
			title = managers.localization:text("dialog_warning_title"),
			text = managers.localization:text("debug_wskn_sure_to_delete")
		}
		local yes_button = {
			text = managers.localization:text("dialog_yes"),
			callback_func = callback(self, self, "_dialog_delete_armor_skin_yes")
		}
		local no_button = {
			text = managers.localization:text("dialog_no"),
			callback_func = callback(self, self, "_dialog_delete_armor_skin_no"),
			cancel_button = true
		}
		dialog_data.button_list = {
			yes_button,
			no_button
		}

		managers.system_menu:show(dialog_data)
	end

	return 
end
MenuCallbackHandler._dialog_delete_armor_skin_yes = function (self)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		editor.delete_current(editor)
	end

	return 
end
MenuCallbackHandler._dialog_delete_armor_skin_no = function (self)
	return 
end
MenuCallbackHandler.browse_armor_skin = function (self)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		local skin = editor.get_current_skin(editor)

		if skin then
			local path = Application:nice_path(skin.path(skin), false)

			Application:shell_explore_to_folder(path)
		end
	end

	return 
end
MenuCallbackHandler.save_armor_skin = function (self)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		local name = managers.menu:active_menu().logic:selected_node():item("name_input"):input_text()

		if not name or name == "" then
			name = "My Skin"
		end

		local name_id = string.gsub(string.lower(name), " ", "_")
		local copy_data = deep_clone(editor.get_current_skin(editor):config().data)
		copy_data.name_id = "bm_askn_" .. name_id
		copy_data.reserve_quality = true

		editor.save_current_skin(editor, name, copy_data)
	end

	return 
end
MenuCallbackHandler.need_convert_armor_skin = function (self, item)
	return false
end
MenuCallbackHandler.convert_armor_skin = function (self)
	return 
end
MenuCallbackHandler.on_exit_armor_skin_editor = function (self, item)
	local editor = managers.blackmarket:armor_skin_editor()

	if not editor then
		return true
	end

	if not editor.get_current_skin(editor) then
		return false
	end

	if not editor.unsaved(editor) then
		editor.set_ignore_unsaved(editor, false)

		editor._unsaved = false

		editor.set_active(editor, false)

		return false
	end

	local function on_yes()
		editor:save_current_skin()
		managers.menu:back(true)

		return 
	end

	local function on_no()
		editor:set_ignore_unsaved(true)
		managers.menu:back(true)

		return 
	end

	local dialog_data = {
		title = managers.localization:text("dialog_warning_title"),
		text = managers.localization:text("debug_wskn_want_to_save")
	}
	local yes_button = {
		text = managers.localization:text("dialog_yes"),
		callback_func = on_yes
	}
	local no_button = {
		text = managers.localization:text("dialog_no"),
		callback_func = on_no
	}
	local cancel_button = {
		text = managers.localization:text("dialog_cancel"),
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button,
		cancel_button
	}

	managers.system_menu:show(dialog_data)

	return true
end
MenuCallbackHandler.armor_skin_changed = function (self, item)
	local key = item.parameters(item).key or item.name(item)
	local value = item.value(item)
	local vector = item.parameters(item).vector
	local editor = managers.blackmarket:armor_skin_editor()
	local skin = editor and editor.get_current_skin(editor)
	local data = skin and skin.config(skin).data

	if not data then
		return 
	end

	if value then
		local lock = false

		if string.find(item.parameters(item).name, "uv_scale[1-2]") then
			value = value - 20.01
			local lock_item = managers.menu:active_menu().logic:selected_node():item("uv_scale_lock")

			if lock_item then
				lock = lock_item.value(lock_item) == "on"
			end
		end

		if vector then
			local armor_level = MenuCallbackHandler:editor_get_armor_level()
			local v = tweak_data.economy:get_armor_based_value(data[key], armor_level)

			if not v then
				local i1 = managers.menu:active_menu().logic:selected_node():item(key .. "1")
				local i2 = managers.menu:active_menu().logic:selected_node():item(key .. "2")
				local i3 = managers.menu:active_menu().logic:selected_node():item(key .. "3")
				local i1v = (i1 and i1.value(i1)) or 0
				local i2v = (i2 and i2.value(i2)) or 0
				local i3v = (i3 and i3.value(i3)) or 0

				if string.find(item.parameters(item).name, "uv_scale") then
					i1v = i1v - 20.01
					i2v = i2v - 20.01
				end

				v = Vector3(i1v, i2v, i3v)
			end

			if vector == 1 then
				mvector3.set_x(v, value)

				if lock then
					mvector3.set_y(v, value)
				end
			elseif vector == 2 then
				mvector3.set_y(v, value)

				if lock then
					mvector3.set_x(v, value)
				end
			elseif vector == 3 then
				mvector3.set_z(v, value)
			end

			value = v
			local i1 = managers.menu:active_menu().logic:selected_node():item(key .. "1")
			local i2 = managers.menu:active_menu().logic:selected_node():item(key .. "2")
			local i3 = managers.menu:active_menu().logic:selected_node():item(key .. "3")

			if string.find(item.parameters(item).name, "uv_scale") then
				i1.set_value(i1, mvector3.x(value) - 20.01)
				i2.set_value(i2, mvector3.y(value) - 20.01)
			else
				i1.set_value(i1, mvector3.x(value))
				i2.set_value(i2, mvector3.y(value))
			end

			if i3 then
				i3.set_value(i3, mvector3.z(value))
			end
		elseif item.parameters(item).type ~= "CoreMenuItemSlider.ItemSlider" then
			local orig_value = value
			value = editor.get_texture_string(editor, skin, orig_value, key)
			data[key .. "_name"] = orig_value

			editor.load_textures(editor, skin)

			if not editor.check_texture_db(editor, value, true) then
				return 
			elseif not editor.check_texture_disk(editor, value) then
				if not item.options then
					return 
				end

				local index = table.index_of(item.options(item), table.find_value(item.options(item), function (v)
					return v.value(v) == orig_value
				end))

				item.clear_options(item)
				item.add_option(item, CoreMenuItemOption.ItemOption:new({
					text_id = "DEFAULT",
					localize = false,
					_meta = "option"
				}))
				editor.load_textures(editor, skin)

				local textures = editor.get_texture_list_by_type(editor, skin, key)

				for _, texture in ipairs(textures) do
					item.add_option(item, CoreMenuItemOption.ItemOption:new({
						localize = false,
						_meta = "option",
						text_id = texture,
						value = texture
					}))
				end

				item.dirty(item)

				value = nil
				data[key .. "_name"] = nil
			end
		end

		data[key] = data[key] or {}

		if type(data[key]) ~= "table" then
			data[key] = {}
		end

		if type_name(value) == "Vector3" then
			data[key][self.editor_get_armor_level(self)] = mvector3.copy(value)
		else
			data[key][self.editor_get_armor_level(self)] = value
		end
	else
		if data[key] then
			data[key][self.editor_get_armor_level(self)] = nil
		end

		data[key .. "_name"] = nil
	end

	editor.apply_changes(editor, skin.config(skin).data)

	return 
end
MenuCallbackHandler.editor_get_armor_level = function (self)
	local armor_level = 1

	if managers.menu_scene._character_unit and managers.menu_scene._character_unit:base() then
		armor_level = managers.menu_scene._character_unit:base():armor_level()
	else
		local armor = tweak_data.blackmarket.armors[managers.blackmarket:equipped_armor()]
		armor_level = (armor and armor.upgrade_level) or 1
	end

	return armor_level
end
MenuCallbackHandler.publish_armor_skin = function (self, item)
	local title = managers.menu:active_menu().logic:selected_node():item("title_input"):input_text()
	local desc = managers.menu:active_menu().logic:selected_node():item("desc_input"):input_text()
	local changelog = managers.menu:active_menu().logic:selected_node():item("changelog_input"):input_text()

	if not changelog or changelog == "" then
		changelog = "Initial submission"
	end

	local editor = managers.blackmarket:armor_skin_editor()
	local skin = editor.get_current_skin(editor)

	if skin.config(skin).screenshot then
		local screenshot_path = editor.get_screenshot_path(editor, skin)

		SystemFS:copy_file(screenshot_path .. "/" .. skin.config(skin).screenshot, Application:nice_path(skin.path(skin), true) .. "preview.png")
	else
		local dialog_data = {
			title = managers.localization:text("dialog_warning_title"),
			text = managers.localization:text("debug_wskn_submit_no_screenshot")
		}
		local ok_button = {
			text = managers.localization:text("dialog_ok"),
			callback_func = callback(self, self, "_dialog_ok")
		}
		dialog_data.button_list = {
			ok_button
		}

		managers.system_menu:show(dialog_data)

		return 
	end

	editor.publish_skin(editor, skin, title, desc, changelog)

	return 
end
MenuCallbackHandler.should_add_changelog_armor_skin = function (self, item)
	return managers.blackmarket:armor_skin_editor():get_current_skin():item_exists()
end
MenuCallbackHandler.armor_screenshot_chosen = function (self, item)
	local skin = managers.blackmarket:armor_skin_editor():get_current_skin()
	skin.config(skin).screenshot = item.value(item)

	return 
end
MenuCallbackHandler.take_armor_screenshot_skin = function (self, item)
	local editor = managers.blackmarket:armor_skin_editor()

	if not editor then
		return 
	end

	local function screenshot_done(success)
		managers.mouse_pointer:enable()
		managers.menu:active_menu().renderer:show()
		item:set_enabled(true)

		return 
	end

	managers.menu:active_menu().renderer:hide()
	managers.mouse_pointer:disable()

	local name = editor.get_screenshot_name(editor)
	local x, y, w, h = editor.get_screenshot_rect(editor)

	item.set_enabled(item, false)

	local function co_screenshot(o)
		for i = 0, 5, 1 do
			coroutine.yield()
		end

		Application:screenshot(name, x, y, w, h, true, screenshot_done, 1024, 576)

		return 
	end

	managers.menu:active_menu().renderer.ws:panel():animate(co_screenshot)

	return 
end
MenuCallbackHandler.leave_armor_screenshot_menu = function (self, item)
	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		editor.leave_screenshot_mode(editor)
		editor.reload_current_skin(editor)
	end

	return 
end
MenuCallbackHandler.armor_screenshot_color_changed = function (self, item)
	local skin_editor = managers.blackmarket:armor_skin_editor()

	if item.value(item) == "none" then
		skin_editor.hide_screenshot_bg(skin_editor)
	elseif item.value(item) == "green" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 1, 0))
	elseif item.value(item) == "black" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 0, 0))
	elseif item.value(item) == "red" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 0, 0))
	elseif item.value(item) == "blue" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 0, 1))
	elseif item.value(item) == "cyan" then
		skin_editor.set_screenshot_color(skin_editor, Color(0, 1, 1))
	elseif item.value(item) == "pink" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 0, 1))
	elseif item.value(item) == "yellow" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 1, 0))
	elseif item.value(item) == "white" then
		skin_editor.set_screenshot_color(skin_editor, Color(1, 1, 1))
	end

	return 
end
MenuCallbackHandler.armor_screenshots_hide_weapons = function (self, item)
	for _, data in pairs(managers.menu_scene._weapon_units) do
		for _, u_data in pairs(data) do
			if alive(u_data.unit) then
				self._armor_screenshots_set_weapon_visibility(self, u_data.unit, false)
			end
		end
	end

	return 
end
MenuCallbackHandler.armor_screenshots_show_weapons = function (self, item)
	for _, data in pairs(managers.menu_scene._weapon_units) do
		for _, u_data in pairs(data) do
			if alive(u_data.unit) then
				self._armor_screenshots_set_weapon_visibility(self, u_data.unit, true)
			end
		end
	end

	return 
end
MenuCallbackHandler._armor_screenshots_set_weapon_visibility = function (self, unit, state)
	unit.set_enabled(unit, state)

	for _, child_unit in ipairs(unit.children(unit)) do
		self._armor_screenshots_set_weapon_visibility(self, child_unit, state)
	end

	return 
end
MenuCallbackHandler.select_armor_skin_level = function (self, item)
	managers.menu_scene:set_character_armor(item.name(item))

	local editor = managers.blackmarket:armor_skin_editor()

	if editor then
		editor.reload_current_skin(editor)
	end

	return 
end
MenuCallbackHandler.select_armor_skin_pose = function (self, item)
	managers.menu_scene:_set_character_unit_pose(item.name(item), managers.menu_scene._character_unit)

	return 
end

return 
