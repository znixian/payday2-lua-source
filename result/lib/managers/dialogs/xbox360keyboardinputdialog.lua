core:module("SystemMenuManager")
require("lib/managers/dialogs/KeyboardInputDialog")

Xbox360KeyboardInputDialog = Xbox360KeyboardInputDialog or class(KeyboardInputDialog)
Xbox360KeyboardInputDialog.show = function (self)
	self._manager:event_dialog_shown(self)

	local end_parameter_list = {}

	table.insert(end_parameter_list, self.max_count(self))
	table.insert(end_parameter_list, callback(self, self, "done_callback"))
	XboxLive:show_keyboard_ui(self.get_platform_id(self), self.input_type(self), self.input_text(self), self.title(self), self.text(self), unpack(end_parameter_list))

	return true
end
Xbox360KeyboardInputDialog.done_callback = function (self, input_text)
	KeyboardInputDialog.done_callback(self, true, input_text)

	return 
end

return 
