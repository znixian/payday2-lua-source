core:module("SystemMenuManager")
require("lib/managers/dialogs/BaseDialog")

MarketplaceDialog = MarketplaceDialog or class(BaseDialog)
MarketplaceDialog.done_callback = function (self)
	if self._data.callback_func then
		self._data.callback_func()
	end

	self.fade_out_close(self)

	return 
end
MarketplaceDialog.item_type = function (self)
	return self._data.item_type
end
MarketplaceDialog.item_id = function (self)
	return self._data.item_id
end
MarketplaceDialog.to_string = function (self)
	return string.format("%s, Item type: %s, Item id: %s", tostring(BaseDialog.to_string(self)), tostring(self._data.item_type), tostring(self._data.item_id))
end

return 
