core:module("SystemMenuManager")
require("lib/managers/dialogs/PlayerDialog")

Xbox360PlayerDialog = Xbox360PlayerDialog or class(PlayerDialog)
Xbox360PlayerDialog.init = function (self, manager, data)
	PlayerDialog.init(self, manager, data)

	return 
end
Xbox360PlayerDialog.show = function (self)
	self._manager:event_dialog_shown(self)

	local player_id = self.player_id(self)

	if player_id then
		XboxLive:show_gamer_card_ui(self.get_platform_id(self), self.player_id(self))
	else
		Application:error("[SystemMenuManager] Unable to display player dialog since no player id was specified.")
	end

	self._show_time = TimerManager:main():time()

	return true
end
Xbox360PlayerDialog.update = function (self, t, dt)
	if self._show_time and self._show_time ~= t and not Application:is_showing_system_dialog() then
		self.done_callback(self)
	end

	return 
end
Xbox360PlayerDialog.done_callback = function (self)
	self._show_time = nil

	PlayerDialog.done_callback(self)

	return 
end

return 
