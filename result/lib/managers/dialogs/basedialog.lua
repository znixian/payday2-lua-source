core:module("SystemMenuManager")
core:import("CoreDebug")

BaseDialog = BaseDialog or class()
BaseDialog.init = function (self, manager, data)
	self._manager = manager
	self._data = data or {}

	return 
end
BaseDialog.id = function (self)
	return self._data.id
end
BaseDialog.priority = function (self)
	return self._data.priority or 0
end
BaseDialog.get_platform_id = function (self)
	return managers.user:get_platform_id(self._data.user_index) or 0
end
BaseDialog.is_generic = function (self)
	return self._data.is_generic
end
BaseDialog.fade_in = function (self)
	return 
end
BaseDialog.fade_out_close = function (self)
	self.close(self)

	return 
end
BaseDialog.fade_out = function (self)
	self.close(self)

	return 
end
BaseDialog.force_close = function (self)
	return 
end
BaseDialog.close = function (self)
	self._manager:event_dialog_closed(self)

	return 
end
BaseDialog.is_closing = function (self)
	return false
end
BaseDialog.show = function (self)
	Application:error("[SystemMenuManager] Unable to display dialog since the logic for it hasn't been implemented.")

	return false
end
BaseDialog._get_ws = function (self)
	return self._ws
end
BaseDialog._get_controller = function (self)
	return self._controller
end
BaseDialog.to_string = function (self)
	return string.format("Class: %s, Id: %s, User index: %s", CoreDebug.class_name(getmetatable(self), _M), tostring(self._data.id), tostring(self._data.user_index))
end
BaseDialog.blocks_exec = function (self)
	return true
end

return 
