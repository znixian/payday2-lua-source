core:module("SystemMenuManager")
require("lib/managers/dialogs/PlayerReviewDialog")

Xbox360PlayerReviewDialog = Xbox360PlayerReviewDialog or class(PlayerReviewDialog)
Xbox360PlayerReviewDialog.init = function (self, manager, data)
	PlayerReviewDialog.init(self, manager, data)

	return 
end
Xbox360PlayerReviewDialog.show = function (self)
	self._manager:event_dialog_shown(self)

	local player_id = self.player_id(self)

	if player_id then
		XboxLive:show_player_review_ui(self.get_platform_id(self), self.player_id(self))
	else
		Application:error("[SystemMenuManager] Unable to display player review dialog since no player id was specified.")
	end

	self._show_time = TimerManager:main():time()

	return true
end
Xbox360PlayerReviewDialog.update = function (self, t, dt)
	if self._show_time and self._show_time ~= t and not Application:is_showing_system_dialog() then
		self.done_callback(self)
	end

	return 
end
Xbox360PlayerReviewDialog.done_callback = function (self)
	self._show_time = nil

	PlayerReviewDialog.done_callback(self)

	return 
end

return 
