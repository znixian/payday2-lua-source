core:module("SystemMenuManager")
require("lib/managers/dialogs/AchievementsDialog")

Xbox360AchievementsDialog = Xbox360AchievementsDialog or class(AchievementsDialog)
Xbox360AchievementsDialog.init = function (self, manager, data)
	AchievementsDialog.init(self, manager, data)

	return 
end
Xbox360AchievementsDialog.show = function (self)
	self._manager:event_dialog_shown(self)
	XboxLive:show_achievements_ui(self.get_platform_id(self))

	self._show_time = TimerManager:main():time()

	return true
end
Xbox360AchievementsDialog.update = function (self, t, dt)
	if self._show_time and self._show_time ~= t and not Application:is_showing_system_dialog() then
		self.done_callback(self)
	end

	return 
end
Xbox360AchievementsDialog.done_callback = function (self)
	self._show_time = nil

	AchievementsDialog.done_callback(self)

	return 
end

return 
