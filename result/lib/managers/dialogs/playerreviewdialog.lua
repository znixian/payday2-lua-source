core:module("SystemMenuManager")
require("lib/managers/dialogs/Dialog")

PlayerReviewDialog = PlayerReviewDialog or class(BaseDialog)
PlayerReviewDialog.done_callback = function (self)
	if self._data.callback_func then
		self._data.callback_func()
	end

	self.fade_out_close(self)

	return 
end
PlayerReviewDialog.player_id = function (self)
	return self._data.player_id
end
PlayerReviewDialog.to_string = function (self)
	return string.format("%s, Player id: %s", tostring(BaseDialog.to_string(self)), tostring(self._data.player_id))
end

return 
