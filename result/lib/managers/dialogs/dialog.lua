core:module("SystemMenuManager")
require("lib/managers/dialogs/BaseDialog")

Dialog = Dialog or class(BaseDialog)
Dialog.init = function (self, manager, data)
	BaseDialog.init(self, manager, data)

	self._button_text_list = {}

	self.init_button_text_list(self)

	return 
end
Dialog.init_button_text_list = function (self)
	local button_list = self._data.button_list

	if button_list then
		for _, button in ipairs(button_list) do
			table.insert(self._button_text_list, button.text or "ERROR")
		end
	end

	if #self._button_text_list == 0 and not self._data.no_buttons then
		Application:error("[SystemMenuManager] Invalid dialog with no button texts. Adds an ok-button.")

		self._data.button_list = self._data.button_list or {}
		self._data.button_list[1] = self._data.button_list[1] or {}
		self._data.button_list[1].text = "ERROR: OK"

		table.insert(self._button_text_list, self._data.button_list[1].text)
	end

	return 
end
Dialog.title = function (self)
	return self._data.title
end
Dialog.text = function (self)
	return self._data.text
end
Dialog.focus_button = function (self)
	return self._data.focus_button
end
Dialog.button_pressed = function (self, button_index)
	cat_print("dialog_manager", "[SystemMenuManager] Button index pressed: " .. tostring(button_index))

	local button_list = self._data.button_list

	self.fade_out_close(self)

	if button_list then
		local button = button_list[button_index]

		if button and button.callback_func then
			button.callback_func(button_index, button)
		end
	end

	local callback_func = self._data.callback_func

	if callback_func then
		callback_func(button_index, self._data)
	end

	return 
end
Dialog.button_text_list = function (self)
	return self._button_text_list
end
Dialog.to_string = function (self)
	local buttons = ""

	if self._data.button_list then
		for _, button in ipairs(self._data.button_list) do
			buttons = buttons .. "[" .. tostring(button.text) .. "]"
		end
	end

	return string.format("%s, Title: %s, Text: %s, Buttons: %s", tostring(BaseDialog.to_string(self)), tostring(self._data.title), tostring(self._strip_to_string_text(self, self._data.text)), buttons)
end
Dialog._strip_to_string_text = function (self, text)
	return string.gsub(tostring(text), "\n", "\\n")
end

return 
