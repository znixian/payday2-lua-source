core:module("SystemMenuManager")
require("lib/managers/dialogs/Dialog")

Xbox360Dialog = Xbox360Dialog or class(Dialog)
Xbox360Dialog.show = function (self)
	local focus_button = self.focus_button(self)

	if focus_button then
		focus_button = focus_button - 1
	else
		focus_button = 0
	end

	local button_text_list = self.button_text_list(self)
	local success = Application:display_message_box_dialog(self.get_platform_id(self), self.title(self), self.text(self), focus_button, callback(self, self, "button_pressed"), false, unpack(button_text_list))

	if success then
		self._manager:event_dialog_shown(self)
	end

	return success
end
Xbox360Dialog.button_pressed = function (self, button_index)
	if button_index == -1 then
		button_index = self.focus_button(self) or 1

		cat_print("dialog_manager", "[SystemMenuManager] Dialog aborted. Defaults to focus button.")
	end

	Dialog.button_pressed(self, button_index + 1)

	return 
end
Xbox360Dialog.blocks_exec = function (self)
	return false
end

return 
