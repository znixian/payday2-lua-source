core:module("SystemMenuManager")
require("lib/managers/dialogs/BaseDialog")

KeyboardInputDialog = KeyboardInputDialog or class(BaseDialog)
KeyboardInputDialog.title = function (self)
	return self._data.title or ""
end
KeyboardInputDialog.text = function (self)
	return self._data.text or ""
end
KeyboardInputDialog.input_text = function (self)
	return self._data.input_text
end
KeyboardInputDialog.input_type = function (self)
	return self._data.input_type or "default"
end
KeyboardInputDialog.max_count = function (self)
	return self._data.max_count
end
KeyboardInputDialog.filter = function (self)
	return self._data.filter
end
KeyboardInputDialog.done_callback = function (self, success, input_text)
	if self._data.callback_func then
		self._data.callback_func(success, input_text)
	end

	self.fade_out_close(self)

	return 
end
KeyboardInputDialog.to_string = function (self)
	return string.format("%s, Title: %s, Text: %s, Input text: %s, Max count: %s, Filter: %s", tostring(BaseDialog.to_string(self)), tostring(self._data.title), tostring(self._data.text), tostring(self._data.input_text), tostring(self._data.max_count), tostring(self._data.filter))
end

return 
