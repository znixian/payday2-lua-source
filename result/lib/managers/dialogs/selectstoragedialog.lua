core:module("SystemMenuManager")
require("lib/managers/dialogs/BaseDialog")

SelectStorageDialog = SelectStorageDialog or class(BaseDialog)
SelectStorageDialog.content_type = function (self)
	return self._data.content_type or 1
end
SelectStorageDialog.min_bytes = function (self)
	return self._data.min_bytes or 0
end
SelectStorageDialog.auto_select = function (self)
	return self._data.auto_select
end
SelectStorageDialog.done_callback = function (self, success, result)
	if self._data.callback_func then
		self._data.callback_func(success, result)
	end

	self.fade_out_close(self)

	return 
end
SelectStorageDialog.to_string = function (self)
	return string.format("%s, Content type: %s, Min bytes: %s, Auto select: %s", tostring(BaseDialog.to_string(self)), tostring(self._data.content_type), tostring(self._data.min_bytes), tostring(self._data.auto_select))
end

return 
