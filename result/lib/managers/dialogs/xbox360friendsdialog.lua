core:module("SystemMenuManager")
require("lib/managers/dialogs/FriendsDialog")

Xbox360FriendsDialog = Xbox360FriendsDialog or class(FriendsDialog)
Xbox360FriendsDialog.init = function (self, manager, data)
	FriendsDialog.init(self, manager, data)

	return 
end
Xbox360FriendsDialog.show = function (self)
	self._manager:event_dialog_shown(self)
	XboxLive:show_friends_ui(self.get_platform_id(self))

	self._show_time = TimerManager:main():time()

	return true
end
Xbox360FriendsDialog.update = function (self, t, dt)
	if self._show_time and self._show_time ~= t and not Application:is_showing_system_dialog() then
		self.done_callback(self)
	end

	return 
end
Xbox360FriendsDialog.done_callback = function (self)
	self._show_time = nil

	FriendsDialog.done_callback(self)

	return 
end

return 
