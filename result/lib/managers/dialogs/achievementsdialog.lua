core:module("SystemMenuManager")
require("lib/managers/dialogs/BaseDialog")

AchievementsDialog = AchievementsDialog or class(BaseDialog)
AchievementsDialog.done_callback = function (self)
	if self._data.callback_func then
		self._data.callback_func()
	end

	self.fade_out_close(self)

	return 
end

return 
