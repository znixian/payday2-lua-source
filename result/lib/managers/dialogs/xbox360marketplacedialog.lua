core:module("SystemMenuManager")
require("lib/managers/dialogs/MarketplaceDialog")

Xbox360MarketplaceDialog = Xbox360MarketplaceDialog or class(MarketplaceDialog)
Xbox360MarketplaceDialog.init = function (self, manager, data)
	MarketplaceDialog.init(self, manager, data)

	return 
end
Xbox360MarketplaceDialog.show = function (self)
	self._manager:event_dialog_shown(self)

	local end_parameter_list = {}

	table.insert(end_parameter_list, self.item_type(self))
	table.insert(end_parameter_list, self.item_id(self))
	XboxLive:show_marketplace_ui(self.get_platform_id(self), unpack(end_parameter_list))

	self._show_time = TimerManager:main():time()

	return true
end
Xbox360MarketplaceDialog.update = function (self, t, dt)
	if self._show_time and self._show_time ~= t and not Application:is_showing_system_dialog() then
		self.done_callback(self)
	end

	return 
end
Xbox360MarketplaceDialog.done_callback = function (self)
	self._show_time = nil

	MarketplaceDialog.done_callback(self)

	return 
end

return 
