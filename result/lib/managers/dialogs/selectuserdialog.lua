core:module("SystemMenuManager")
require("lib/managers/dialogs/BaseDialog")

SelectUserDialog = SelectUserDialog or class(BaseDialog)
SelectUserDialog.count = function (self)
	return self._data.count or 1
end
SelectUserDialog.done_callback = function (self)
	if self._data.callback_func then
		self._data.callback_func()
	end

	self.fade_out_close(self)

	return 
end
SelectUserDialog.to_string = function (self)
	return string.format("%s, Count: %s", tostring(BaseDialog.to_string(self)), tostring(self._data.count))
end

return 
