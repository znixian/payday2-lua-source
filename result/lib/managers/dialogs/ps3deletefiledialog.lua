core:module("SystemMenuManager")
require("lib/managers/dialogs/DeleteFileDialog")

PS3DeleteFileDialog = PS3DeleteFileDialog or class(DeleteFileDialog)
PS3DeleteFileDialog.show = function (self)
	self._manager:event_dialog_shown(self)
	self.done_callback(self, true)

	return true
end

return 
