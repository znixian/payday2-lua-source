core:module("SystemMenuManager")
require("lib/managers/dialogs/KeyboardInputDialog")

PS3KeyboardInputDialog = PS3KeyboardInputDialog or class(KeyboardInputDialog)
PS3KeyboardInputDialog.show = function (self)
	local data = {
		title = self.title(self),
		text = self.input_text(self),
		filter = self.filter(self),
		limit = self.max_count(self) or 0,
		callback = callback(self, self, "done_callback")
	}

	PS3:display_keyboard(data)

	local success = PS3:is_displaying_box()

	if success then
		self._manager:event_dialog_shown(self)
	end

	return success
end
PS3KeyboardInputDialog.done_callback = function (self, input_text, success)
	KeyboardInputDialog.done_callback(self, success, input_text)

	return 
end

return 
