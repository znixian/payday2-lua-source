core:module("SystemMenuManager")
require("lib/managers/dialogs/SelectStorageDialog")

Xbox360SelectStorageDialog = Xbox360SelectStorageDialog or class(SelectStorageDialog)
Xbox360SelectStorageDialog.show = function (self)
	local success = Application:display_device_selection_dialog(self.get_platform_id(self), self.content_type(self), not self.auto_select(self), callback(self, self, "done_callback"), self.min_bytes(self), false)

	if success then
		self._manager:event_dialog_shown(self)
	end

	return success
end

return 
