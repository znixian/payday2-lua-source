require("lib/managers/group_ai_states/GroupAIStateBase")
require("lib/managers/group_ai_states/GroupAIStateEmpty")
require("lib/managers/group_ai_states/GroupAIStateBesiege")
require("lib/managers/group_ai_states/GroupAIStateStreet")

GroupAIManager = GroupAIManager or class()
GroupAIManager.init = function (self)
	self.set_state(self, "empty")

	return 
end
GroupAIManager.update = function (self, t, dt)
	self._state:update(t, dt)

	return 
end
GroupAIManager.paused_update = function (self, t, dt)
	self._state:paused_update(t, dt)

	return 
end
GroupAIManager.set_state = function (self, name)
	if name == "empty" then
		self._state = GroupAIStateEmpty:new()
	elseif name == "street" then
		self._state = GroupAIStateStreet:new()
	elseif name == "besiege" or name == "airport" or name == "zombie_apocalypse" then
		local level_tweak = tweak_data.levels[managers.job:current_level_id()]
		self._state = GroupAIStateBesiege:new((level_tweak and level_tweak.group_ai_state) or "besiege")
	else
		Application:error("[GroupAIManager:set_state] inexistent state name", name)

		return 
	end

	self._state_name = name

	return 
end
GroupAIManager.state = function (self)
	return self._state
end
GroupAIManager.state_name = function (self)
	return self._state_name
end
GroupAIManager.state_names = function (self)
	return {
		"empty",
		"airport",
		"besiege",
		"street",
		"zombie_apocalypse"
	}
end
GroupAIManager.on_simulation_started = function (self)
	self._state:on_simulation_started()

	return 
end
GroupAIManager.on_simulation_ended = function (self)
	self._state:on_simulation_ended()

	return 
end
GroupAIManager.visualization_enabled = function (self)
	return self._state._draw_enabled
end

return 
