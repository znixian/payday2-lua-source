core:module("SystemMenuManager")
core:import("CoreEvent")
core:import("CoreClass")
require("lib/managers/dialogs/GenericDialog")
require("lib/managers/dialogs/Xbox360Dialog")
require("lib/managers/dialogs/PS3Dialog")
require("lib/managers/dialogs/Xbox360SelectStorageDialog")
require("lib/managers/dialogs/PS3DeleteFileDialog")
require("lib/managers/dialogs/Xbox360KeyboardInputDialog")
require("lib/managers/dialogs/PS3KeyboardInputDialog")
require("lib/managers/dialogs/Xbox360SelectUserDialog")
require("lib/managers/dialogs/Xbox360AchievementsDialog")
require("lib/managers/dialogs/Xbox360FriendsDialog")
require("lib/managers/dialogs/Xbox360PlayerReviewDialog")
require("lib/managers/dialogs/Xbox360PlayerDialog")
require("lib/managers/dialogs/Xbox360MarketplaceDialog")
require("lib/managers/dialogs/NewUnlockDialog")
require("lib/managers/dialogs/SpecializationDialog")
require("lib/managers/dialogs/ButtonsDialog")

SystemMenuManager = SystemMenuManager or class()
SystemMenuManager.PLATFORM_CLASS_MAP = {}
SystemMenuManager.new = function (self, ...)
	local platform = SystemInfo:platform()

	return self.PLATFORM_CLASS_MAP[platform.key(platform)] or GenericSystemMenuManager:new(...)
end
GenericSystemMenuManager = GenericSystemMenuManager or class()
GenericSystemMenuManager.DIALOG_CLASS = GenericDialog
GenericSystemMenuManager.GENERIC_DIALOG_CLASS = GenericDialog
GenericSystemMenuManager.PLATFORM_DIALOG_CLASS = GenericDialog
GenericSystemMenuManager.NEW_UNLOCK_CLASS = NewUnlockDialog
GenericSystemMenuManager.GENERIC_NEW_UNLOCK_CLASS = NewUnlockDialog
GenericSystemMenuManager.SPECIALIZATION_CLASS = SpecializationDialog
GenericSystemMenuManager.GENERIC_SPECIALIZATION_CLASS = SpecializationDialog
GenericSystemMenuManager.BUTTON_DIALOG_CLASS = ButtonsDialog
GenericSystemMenuManager.GENERIC_BUTTON_DIALOG_CLASS = ButtonsDialog
GenericSystemMenuManager.init = function (self)
	if not Global.dialog_manager then
		Global.dialog_manager = {}
	end

	self._dialog_shown_callback_handler = CoreEvent.CallbackEventHandler:new()
	self._dialog_hidden_callback_handler = CoreEvent.CallbackEventHandler:new()
	self._dialog_closed_callback_handler = CoreEvent.CallbackEventHandler:new()
	self._active_changed_callback_handler = CoreEvent.CallbackEventHandler:new()
	self._controller = managers.controller:create_controller("dialog", nil, false)

	managers.controller:add_default_wrapper_index_change_callback(callback(self, self, "changed_controller_index"))

	self._resolution_changed_callback_id = managers.viewport:add_resolution_changed_func(callback(self, self, "resolution_changed"))

	return 
end
GenericSystemMenuManager.init_finalize = function (self)
	self._ws = managers.gui_data:create_1280_workspace()

	self._ws:hide()

	if Global.dialog_manager.init_show_data_list then
		local init_show_data_list = Global.dialog_manager.init_show_data_list
		Global.dialog_manager.init_show_data_list = nil

		for index, data in ipairs(init_show_data_list) do
			cat_print("dialog_manager", "[SystemMenuManager] Processing init dialog. Index: " .. tostring(index) .. "/" .. tostring(#init_show_data_list))
			self.show(self, data)
		end
	end

	return 
end
GenericSystemMenuManager.resolution_changed = function (self)
	managers.gui_data:layout_1280_workspace(self._ws)

	return 
end
GenericSystemMenuManager.add_init_show = function (self, data)
	local init_show_data_list = Global.dialog_manager.init_show_data_list
	local priority = data.priority or 0

	cat_print("dialog_manager", "[SystemMenuManager] Adding an init dialog with priority \"" .. tostring(priority) .. "\".")

	if init_show_data_list then
		for index = #init_show_data_list, 1, -1 do
			local next_data = init_show_data_list[index]
			local next_priority = next_data.priority or 0

			if priority < next_priority then
				cat_print("dialog_manager", "[SystemMenuManager] Ignoring request to show init dialog since it had lower priority than the existing priority \"" .. tostring(next_priority) .. "\". Index: " .. tostring(index) .. "/" .. tostring(#init_show_data_list))

				return false
			elseif next_priority < priority then
				cat_print("dialog_manager", "[SystemMenuManager] Removed an already added init dialog with the lower priority of \"" .. tostring(next_priority) .. "\". Index: " .. tostring(index) .. "/" .. tostring(#init_show_data_list))
				table.remove(init_show_data_list, index)
			end
		end
	else
		init_show_data_list = {}
	end

	table.insert(init_show_data_list, data)

	Global.dialog_manager.init_show_data_list = init_show_data_list

	return 
end
GenericSystemMenuManager.ps4_add_init_show = function (self, data)
	local init_show_data_list = Global.dialog_manager.init_show_data_list
	local priority = data.priority or 0

	cat_print("dialog_manager", "[SystemMenuManager] Adding an init dialog with priority \"" .. tostring(priority) .. "\".")

	if init_show_data_list then
		for index = #init_show_data_list, 1, -1 do
			local next_data = init_show_data_list[index]
			local next_priority = next_data.priority or 0

			if priority < next_priority then
				cat_print("dialog_manager", "[SystemMenuManager] Ignoring request to show init dialog since it had lower priority than the existing priority \"" .. tostring(next_priority) .. "\". Index: " .. tostring(index) .. "/" .. tostring(#init_show_data_list))

				return false
			elseif next_priority < priority then
				cat_print("dialog_manager", "[SystemMenuManager] Removed an already added init dialog with the lower priority of \"" .. tostring(next_priority) .. "\". Index: " .. tostring(index) .. "/" .. tostring(#init_show_data_list))
				table.remove(init_show_data_list, index)
			end
		end

		table.insert(init_show_data_list, data)

		Global.dialog_manager.init_show_data_list = init_show_data_list
	else
		local success = self._show_class(self, data, self.GENERIC_DIALOG_CLASS, self.DIALOG_CLASS, data.force)

		self._show_result(self, success, data)
	end

	return 
end
GenericSystemMenuManager.destroy = function (self)
	if alive(self._ws) then
		managers.gui_data:destroy_workspace(self._ws)

		self._ws = nil
	end

	if self._controller then
		self._controller:destroy()

		self._controller = nil
	end

	return 
end
GenericSystemMenuManager.changed_controller_index = function (self, default_wrapper_index)
	local was_enabled = self._controller:enabled()

	self._controller:destroy()

	self._controller = managers.controller:create_controller("dialog", nil, false)

	self._controller:set_enabled(was_enabled)

	return 
end
GenericSystemMenuManager.update = function (self, t, dt)
	if self._active_dialog and self._active_dialog.update then
		self._active_dialog:update(t, dt)
	end

	self.update_queue(self)
	self.check_active_state(self)

	return 
end
GenericSystemMenuManager.paused_update = function (self, t, dt)
	self.update(self, t, dt)

	return 
end
GenericSystemMenuManager.update_queue = function (self)
	if not self.is_active(self, true) and self._dialog_queue then
		local dialog, index = nil

		for next_index, next_dialog in ipairs(self._dialog_queue) do
			if not dialog or dialog.priority(dialog) < next_dialog.priority(next_dialog) then
				index = next_index
				dialog = next_dialog
			end
		end

		table.remove(self._dialog_queue, index)

		if not next(self._dialog_queue) then
			self._dialog_queue = nil
		end

		if dialog then
			self._show_instance(self, dialog, true)
		end
	end

	return 
end
GenericSystemMenuManager.check_active_state = function (self)
	local active = self.is_active(self, true)

	if not self._old_active_state ~= not active then
		self.event_active_changed(self, active)

		self._old_active_state = active
	end

	return 
end
GenericSystemMenuManager.block_exec = function (self)
	return self.is_active(self)
end
GenericSystemMenuManager.is_active = function (self)
	return self._active_dialog ~= nil
end
GenericSystemMenuManager.is_closing = function (self)
	return (self._active_dialog and self._active_dialog:is_closing()) or false
end
GenericSystemMenuManager.force_close_all = function (self)
	if self._active_dialog and self._active_dialog:blocks_exec() then
		self._active_dialog:fade_out_close()
	end

	if self._dialog_queue then
		for i, dialog in ipairs(self._dialog_queue) do
			if self._active_dialog and self._active_dialog ~= dialog then
				dialog.force_close(dialog)
			end
		end
	end

	self._dialog_queue = nil

	return 
end
GenericSystemMenuManager.get_dialog = function (self, id)
	if not id then
		return 
	end

	if self._active_dialog and self._active_dialog:id() == id then
		return self._active_dialog
	end

	return 
end
GenericSystemMenuManager.close = function (self, id, hard)
	if not id then
		return 
	end

	print("close active dialog", self._active_dialog and self._active_dialog:id(), id)

	if self._active_dialog and self._active_dialog:id() == id then
		if hard then
			self._active_dialog:close()
		else
			self._active_dialog:fade_out_close()
		end
	end

	if not self._dialog_queue then
		return 
	end

	local remove_list = nil

	for i, dialog in ipairs(self._dialog_queue) do
		if dialog.id(dialog) == id then
			print("remove from queue", id)

			remove_list = remove_list or {}

			table.insert(remove_list, 1, i)
		end
	end

	if remove_list then
		for _, i in ipairs(remove_list) do
			table.remove(self._dialog_queue, i)
		end
	end

	return 
end
GenericSystemMenuManager.is_active_by_id = function (self, id)
	if not self._active_dialog or not id then
		return false
	end

	if self._active_dialog:id() == id then
		return true, self._active_dialog
	end

	if not self._dialog_queue then
		return false
	end

	for i, dialog in ipairs(self._dialog_queue) do
		if dialog.id(dialog) == id then
			return true, dialog
		end
	end

	return false
end
GenericSystemMenuManager._show_result = function (self, success, data)
	if not success and data then
		local default_button_index = data.focus_button or 1
		local button_list = data.button_list

		if data.button_list then
			local button_data = data.button_list[default_button_index]

			if button_data then
				local callback_func = button_data.callback_func

				if callback_func then
					callback_func(default_button_index, button_data)
				end
			end
		end

		if data.callback_func then
			data.callback_func(default_button_index, data)
		end
	end

	return 
end
GenericSystemMenuManager.show = function (self, data)
	if _G.setup and _G.setup:has_queued_exec() then
		return 
	end

	local success = self._show_class(self, data, self.GENERIC_DIALOG_CLASS, self.DIALOG_CLASS, data.force)

	self._show_result(self, success, data)

	return 
end
GenericSystemMenuManager.show_platform = function (self, data)
	local success = self._show_class(self, data, self.GENERIC_DIALOG_CLASS, self.PLATFORM_DIALOG_CLASS, data.force)

	self._show_result(self, success, data)

	return 
end
GenericSystemMenuManager.show_select_storage = function (self, data)
	self._show_class(self, data, self.GENERIC_SELECT_STORAGE_DIALOG_CLASS, self.SELECT_STORAGE_DIALOG_CLASS, false)

	return 
end
GenericSystemMenuManager.show_delete_file = function (self, data)
	self._show_class(self, data, self.GENERIC_DELETE_FILE_DIALOG_CLASS, self.DELETE_FILE_DIALOG_CLASS, false)

	return 
end
GenericSystemMenuManager.show_keyboard_input = function (self, data)
	self._show_class(self, data, self.GENERIC_KEYBOARD_INPUT_DIALOG, self.KEYBOARD_INPUT_DIALOG, false)

	return 
end
GenericSystemMenuManager.show_select_user = function (self, data)
	self._show_class(self, data, self.GENERIC_SELECT_USER_DIALOG, self.SELECT_USER_DIALOG, false)

	return 
end
GenericSystemMenuManager.show_achievements = function (self, data)
	self._show_class(self, data, self.GENERIC_ACHIEVEMENTS_DIALOG, self.ACHIEVEMENTS_DIALOG, false)

	return 
end
GenericSystemMenuManager.show_friends = function (self, data)
	self._show_class(self, data, self.GENERIC_FRIENDS_DIALOG, self.FRIENDS_DIALOG, false)

	return 
end
GenericSystemMenuManager.show_player_review = function (self, data)
	self._show_class(self, data, self.GENERIC_PLAYER_REVIEW_DIALOG, self.PLAYER_REVIEW_DIALOG, false)

	return 
end
GenericSystemMenuManager.show_player = function (self, data)
	self._show_class(self, data, self.GENERIC_PLAYER_DIALOG, self.PLAYER_DIALOG, false)

	return 
end
GenericSystemMenuManager.show_marketplace = function (self, data)
	self._show_class(self, data, self.GENERIC_MARKETPLACE_DIALOG, self.MARKETPLACE_DIALOG, false)

	return 
end
GenericSystemMenuManager.show_new_unlock = function (self, data)
	local success = self._show_class(self, data, self.GENERIC_NEW_UNLOCK_CLASS, self.NEW_UNLOCK_CLASS, data.force)

	self._show_result(self, success, data)

	return 
end
GenericSystemMenuManager.show_specialization_convert = function (self, data)
	local success = self._show_class(self, data, self.GENERIC_SPECIALIZATION_CLASS, self.SPECIALIZATION_CLASS, data.force)

	self._show_result(self, success, data)

	return 
end
GenericSystemMenuManager.show_buttons = function (self, data)
	local success = self._show_class(self, data, self.GENERIC_BUTTON_DIALOG_CLASS, self.BUTTON_DIALOG_CLASS, data.force)

	self._show_result(self, success, data)

	return 
end
GenericSystemMenuManager._show_class = function (self, data, generic_dialog_class, dialog_class, force)
	local dialog_class = (data and data.is_generic and generic_dialog_class) or dialog_class

	if dialog_class then
		local dialog = dialog_class.new(dialog_class, self, data)

		self._show_instance(self, dialog, force)

		return true
	else
		if data then
			local callback_func = data.callback_func

			if callback_func then
				callback_func()
			end
		end

		return false
	end

	return 
end
GenericSystemMenuManager._show_instance = function (self, dialog, force)
	local is_active = self.is_active(self, true)

	if is_active and force then
		self.hide_active_dialog(self)
	end

	local queue = true

	if not is_active then
		queue = not dialog.show(dialog)
	end

	if queue then
		self.queue_dialog(self, dialog, (force and 1) or nil)
	end

	return 
end
GenericSystemMenuManager.hide_active_dialog = function (self)
	if self._active_dialog and not self._active_dialog:is_closing() and self._active_dialog.hide then
		self.queue_dialog(self, self._active_dialog, 1)
		self._active_dialog:hide()
	end

	return 
end
GenericSystemMenuManager.queue_dialog = function (self, dialog, index)
	if Global.category_print.dialog_manager then
		cat_print("dialog_manager", "[SystemMenuManager] [Queue dialog (index: " .. tostring(index) .. "/" .. tostring(self._dialog_queue and #self._dialog_queue) .. ")] " .. tostring(dialog.to_string(dialog)))
	end

	self._dialog_queue = self._dialog_queue or {}

	if index then
		table.insert(self._dialog_queue, index, dialog)
	else
		table.insert(self._dialog_queue, dialog)
	end

	return 
end
GenericSystemMenuManager.set_active_dialog = function (self, dialog)
	self._active_dialog = dialog
	local is_ws_visible = dialog and dialog._get_ws and dialog._get_ws(dialog) == self._ws

	if not self._is_ws_visible ~= not is_ws_visible then
		if is_ws_visible then
			self._ws:show()
		else
			self._ws:hide()
		end

		self._is_ws_visible = is_ws_visible
	end

	local is_controller_enabled = dialog and dialog._get_controller(dialog) == self._controller

	if not self._controller:enabled() ~= not is_controller_enabled then
		self._controller:set_enabled(is_controller_enabled)
	end

	return 
end
GenericSystemMenuManager._is_engine_delaying_signin_change = function (self)
	if self._is_engine_delaying_signin_change_delay then
		self._is_engine_delaying_signin_change_delay = self._is_engine_delaying_signin_change_delay - TimerManager:main():delta_time()

		if self._is_engine_delaying_signin_change_delay <= 0 then
			self._is_engine_delaying_signin_change_delay = nil

			return false
		end
	else
		self._is_engine_delaying_signin_change_delay = 1.2
	end

	return true
end
GenericSystemMenuManager._get_ws = function (self)
	return self._ws
end
GenericSystemMenuManager._get_controller = function (self)
	return self._controller
end
GenericSystemMenuManager.add_dialog_shown_callback = function (self, func)
	self._dialog_shown_callback_handler:add(func)

	return 
end
GenericSystemMenuManager.remove_dialog_shown_callback = function (self, func)
	self._dialog_shown_callback_handler:remove(func)

	return 
end
GenericSystemMenuManager.add_dialog_hidden_callback = function (self, func)
	self._dialog_hidden_callback_handler:add(func)

	return 
end
GenericSystemMenuManager.remove_dialog_hidden_callback = function (self, func)
	self._dialog_hidden_callback_handler:remove(func)

	return 
end
GenericSystemMenuManager.add_dialog_closed_callback = function (self, func)
	self._dialog_closed_callback_handler:add(func)

	return 
end
GenericSystemMenuManager.remove_dialog_closed_callback = function (self, func)
	self._dialog_closed_callback_handler:remove(func)

	return 
end
GenericSystemMenuManager.add_active_changed_callback = function (self, func)
	self._active_changed_callback_handler:add(func)

	return 
end
GenericSystemMenuManager.remove_active_changed_callback = function (self, func)
	self._active_changed_callback_handler:remove(func)

	return 
end
GenericSystemMenuManager.event_dialog_shown = function (self, dialog)
	if Global.category_print.dialog_manager then
		cat_print("dialog_manager", "[SystemMenuManager] [Show dialog] " .. tostring(dialog.to_string(dialog)))
	end

	if dialog.fade_in then
		dialog.fade_in(dialog)
	end

	self.set_active_dialog(self, dialog)
	self._dialog_shown_callback_handler:dispatch(dialog)

	return 
end
GenericSystemMenuManager.event_dialog_hidden = function (self, dialog)
	if Global.category_print.dialog_manager then
		cat_print("dialog_manager", "[SystemMenuManager] [Hide dialog] " .. tostring(dialog.to_string(dialog)))
	end

	self.set_active_dialog(self, nil)
	self._dialog_hidden_callback_handler:dispatch(dialog)

	return 
end
GenericSystemMenuManager.event_dialog_closed = function (self, dialog)
	if Global.category_print.dialog_manager then
		cat_print("dialog_manager", "[SystemMenuManager] [Close dialog] " .. tostring(dialog.to_string(dialog)))
	end

	self.set_active_dialog(self, nil)
	self._dialog_closed_callback_handler:dispatch(dialog)

	return 
end
GenericSystemMenuManager.event_active_changed = function (self, active)
	if Global.category_print.dialog_manager then
		cat_print("dialog_manager", "[SystemMenuManager] [Active changed] Active: " .. tostring(not not active))
	end

	self._active_changed_callback_handler:dispatch(active)

	return 
end
WinSystemMenuManager = WinSystemMenuManager or class(GenericSystemMenuManager)
SystemMenuManager.PLATFORM_CLASS_MAP[Idstring("win32"):key()] = WinSystemMenuManager
Xbox360SystemMenuManager = Xbox360SystemMenuManager or class(GenericSystemMenuManager)
Xbox360SystemMenuManager.PLATFORM_DIALOG_CLASS = Xbox360Dialog
Xbox360SystemMenuManager.SELECT_STORAGE_DIALOG_CLASS = Xbox360SelectStorageDialog
Xbox360SystemMenuManager.GENERIC_SELECT_STORAGE_DIALOG_CLASS = Xbox360SelectStorageDialog
Xbox360SystemMenuManager.KEYBOARD_INPUT_DIALOG = Xbox360KeyboardInputDialog
Xbox360SystemMenuManager.GENERIC_KEYBOARD_INPUT_DIALOG = Xbox360KeyboardInputDialog
Xbox360SystemMenuManager.GENERIC_SELECT_USER_DIALOG = Xbox360SelectUserDialog
Xbox360SystemMenuManager.SELECT_USER_DIALOG = Xbox360SelectUserDialog
Xbox360SystemMenuManager.GENERIC_ACHIEVEMENTS_DIALOG = Xbox360AchievementsDialog
Xbox360SystemMenuManager.ACHIEVEMENTS_DIALOG = Xbox360AchievementsDialog
Xbox360SystemMenuManager.GENERIC_FRIENDS_DIALOG = Xbox360FriendsDialog
Xbox360SystemMenuManager.FRIENDS_DIALOG = Xbox360FriendsDialog
Xbox360SystemMenuManager.GENERIC_PLAYER_REVIEW_DIALOG = Xbox360PlayerReviewDialog
Xbox360SystemMenuManager.PLAYER_REVIEW_DIALOG = Xbox360PlayerReviewDialog
Xbox360SystemMenuManager.GENERIC_PLAYER_DIALOG = Xbox360PlayerDialog
Xbox360SystemMenuManager.PLAYER_DIALOG = Xbox360PlayerDialog
Xbox360SystemMenuManager.GENERIC_MARKETPLACE_DIALOG = Xbox360MarketplaceDialog
Xbox360SystemMenuManager.MARKETPLACE_DIALOG = Xbox360MarketplaceDialog
SystemMenuManager.PLATFORM_CLASS_MAP[Idstring("X360"):key()] = Xbox360SystemMenuManager
Xbox360SystemMenuManager.is_active = function (self, skip_block_exec)
	local dialog_block = self._active_dialog and (skip_block_exec or self._active_dialog:blocks_exec())

	return dialog_block and (GenericSystemMenuManager.is_active(self) or Application:is_showing_system_dialog())
end
XB1SystemMenuManager = XB1SystemMenuManager or class(GenericSystemMenuManager)
XB1SystemMenuManager.KEYBOARD_INPUT_DIALOG = Xbox360KeyboardInputDialog
XB1SystemMenuManager.GENERIC_KEYBOARD_INPUT_DIALOG = Xbox360KeyboardInputDialog
XB1SystemMenuManager.GENERIC_SELECT_USER_DIALOG = Xbox360SelectUserDialog
XB1SystemMenuManager.SELECT_USER_DIALOG = Xbox360SelectUserDialog
XB1SystemMenuManager.GENERIC_ACHIEVEMENTS_DIALOG = Xbox360AchievementsDialog
XB1SystemMenuManager.ACHIEVEMENTS_DIALOG = Xbox360AchievementsDialog
XB1SystemMenuManager.GENERIC_FRIENDS_DIALOG = Xbox360FriendsDialog
XB1SystemMenuManager.FRIENDS_DIALOG = Xbox360FriendsDialog
XB1SystemMenuManager.GENERIC_PLAYER_REVIEW_DIALOG = Xbox360PlayerReviewDialog
XB1SystemMenuManager.PLAYER_REVIEW_DIALOG = Xbox360PlayerReviewDialog
XB1SystemMenuManager.GENERIC_PLAYER_DIALOG = Xbox360PlayerDialog
XB1SystemMenuManager.PLAYER_DIALOG = Xbox360PlayerDialog
XB1SystemMenuManager.GENERIC_MARKETPLACE_DIALOG = Xbox360MarketplaceDialog
XB1SystemMenuManager.MARKETPLACE_DIALOG = Xbox360MarketplaceDialog
SystemMenuManager.PLATFORM_CLASS_MAP[Idstring("XB1"):key()] = XB1SystemMenuManager
XB1SystemMenuManager.is_active = function (self, skip_block_exec)
	local dialog_block = self._active_dialog and (skip_block_exec or self._active_dialog:blocks_exec())

	return dialog_block and (GenericSystemMenuManager.is_active(self) or Application:is_showing_system_dialog())
end
PS3SystemMenuManager = PS3SystemMenuManager or class(GenericSystemMenuManager)
PS3SystemMenuManager.DELETE_FILE_DIALOG_CLASS = PS3DeleteFileDialog
PS3SystemMenuManager.GENERIC_DELETE_FILE_DIALOG_CLASS = PS3DeleteFileDialog
PS3SystemMenuManager.KEYBOARD_INPUT_DIALOG = PS3KeyboardInputDialog
PS3SystemMenuManager.GENERIC_KEYBOARD_INPUT_DIALOG = PS3KeyboardInputDialog
SystemMenuManager.PLATFORM_CLASS_MAP[Idstring("PS3"):key()] = PS3SystemMenuManager
PS3SystemMenuManager.init = function (self)
	GenericSystemMenuManager.init(self)

	self._is_ps_button_menu_visible = false

	PS3:set_ps_button_callback(callback(self, self, "ps_button_menu_callback"))

	return 
end
PS3SystemMenuManager.ps_button_menu_callback = function (self, is_ps_button_menu_visible)
	self._is_ps_button_menu_visible = is_ps_button_menu_visible

	return 
end
PS3SystemMenuManager.block_exec = function (self)
	return GenericSystemMenuManager.is_active(self) or PS3:is_displaying_box()
end
PS3SystemMenuManager.is_active = function (self)
	return GenericSystemMenuManager.is_active(self) or PS3:is_displaying_box() or self._is_ps_button_menu_visible
end
PS4SystemMenuManager = PS4SystemMenuManager or class(GenericSystemMenuManager)
PS4SystemMenuManager.DELETE_FILE_DIALOG_CLASS = PS3DeleteFileDialog
PS4SystemMenuManager.GENERIC_DELETE_FILE_DIALOG_CLASS = PS3DeleteFileDialog
PS4SystemMenuManager.KEYBOARD_INPUT_DIALOG = PS3KeyboardInputDialog
PS4SystemMenuManager.GENERIC_KEYBOARD_INPUT_DIALOG = PS3KeyboardInputDialog
SystemMenuManager.PLATFORM_CLASS_MAP[Idstring("PS4"):key()] = PS4SystemMenuManager
PS4SystemMenuManager.init = function (self)
	GenericSystemMenuManager.init(self)

	self._is_ps_button_menu_visible = false

	PS3:set_ps_button_callback(callback(self, self, "ps_button_menu_callback"))

	return 
end
PS4SystemMenuManager.ps_button_menu_callback = function (self, is_ps_button_menu_visible)
	self._is_ps_button_menu_visible = is_ps_button_menu_visible

	return 
end
PS4SystemMenuManager.block_exec = function (self)
	return GenericSystemMenuManager.is_active(self) or PS3:is_displaying_box()
end
PS4SystemMenuManager.is_active = function (self)
	return GenericSystemMenuManager.is_active(self) or PS3:is_displaying_box() or self._is_ps_button_menu_visible
end

return 
