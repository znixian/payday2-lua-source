core:import("CoreEnvironmentEffectsManager")

local is_editor = Application:editor()
EnvironmentEffectsManager = EnvironmentEffectsManager or class(CoreEnvironmentEffectsManager.EnvironmentEffectsManager)
EnvironmentEffectsManager.init = function (self)
	EnvironmentEffectsManager.super.init(self)
	self.add_effect(self, "rain", RainEffect:new())
	self.add_effect(self, "snow", SnowEffect:new())
	self.add_effect(self, "raindrop_screen", RainDropScreenEffect:new())
	self.add_effect(self, "lightning", LightningEffect:new())

	self._camera_position = Vector3()
	self._camera_rotation = Rotation()

	return 
end
EnvironmentEffectsManager.set_active_effects = function (self, effects)
	for effect_name, effect in pairs(self._effects) do
		if not table.contains(effects, effect_name) then
			if table.contains(self._current_effects, effect) then
				effect.stop(effect)
				table.delete(self._current_effects, effect)
			end
		else
			self.use(self, effect_name)
		end
	end

	return 
end
EnvironmentEffectsManager.update = function (self, t, dt)
	self._camera_position = managers.viewport:get_current_camera_position()
	self._camera_rotation = managers.viewport:get_current_camera_rotation()

	EnvironmentEffectsManager.super.update(self, t, dt)

	return 
end
EnvironmentEffectsManager.camera_position = function (self)
	return self._camera_position
end
EnvironmentEffectsManager.camera_rotation = function (self)
	return self._camera_rotation
end
EnvironmentEffect = EnvironmentEffect or class()
EnvironmentEffect.init = function (self, default)
	self._default = default

	return 
end
EnvironmentEffect.load_effects = function (self)
	return 
end
EnvironmentEffect.update = function (self, t, dt)
	return 
end
EnvironmentEffect.start = function (self)
	return 
end
EnvironmentEffect.stop = function (self)
	return 
end
EnvironmentEffect.default = function (self)
	return self._default
end
RainEffect = RainEffect or class(EnvironmentEffect)
local ids_rain_post_processor = Idstring("rain_post_processor")
local ids_rain_ripples = Idstring("rain_ripples")
local ids_rain_off = Idstring("rain_off")
RainEffect.init = function (self)
	EnvironmentEffect.init(self)

	self._effect_name = Idstring("effects/particles/rain/rain_01_a")

	return 
end
RainEffect.load_effects = function (self)
	if is_editor then
		CoreEngineAccess._editor_load(Idstring("effect"), self._effect_name)
	end

	return 
end
RainEffect.update = function (self, t, dt)
	local vp = managers.viewport:first_active_viewport()

	if vp and self._vp ~= vp then
		vp.vp(vp):set_post_processor_effect("World", ids_rain_post_processor, ids_rain_ripples)

		if alive(self._vp) then
			self._vp:vp():set_post_processor_effect("World", ids_rain_post_processor, ids_rain_off)
		end

		self._vp = vp
	end

	local c_rot = managers.environment_effects:camera_rotation()

	if not c_rot then
		return 
	end

	local c_pos = managers.environment_effects:camera_position()

	if not c_pos then
		return 
	end

	World:effect_manager():move_rotate(self._effect, c_pos, c_rot)

	return 
end
RainEffect.start = function (self)
	self._effect = World:effect_manager():spawn({
		effect = self._effect_name,
		position = Vector3(),
		rotation = Rotation()
	})

	return 
end
RainEffect.stop = function (self)
	World:effect_manager():kill(self._effect)

	self._effect = nil

	if alive(self._vp) then
		self._vp:vp():set_post_processor_effect("World", ids_rain_post_processor, ids_rain_off)

		self._vp = nil
	end

	return 
end
SnowEffect = SnowEffect or class(EnvironmentEffect)
local ids_snow_post_processor = Idstring("snow_post_processor")
local ids_snow_ripples = Idstring("snow_ripples")
local ids_snow_off = Idstring("snow_off")
SnowEffect.init = function (self)
	EnvironmentEffect.init(self)

	self._effect_name = Idstring("effects/particles/snow/snow_01")

	return 
end
SnowEffect.load_effects = function (self)
	if is_editor then
		CoreEngineAccess._editor_load(Idstring("effect"), self._effect_name)
	end

	return 
end
SnowEffect.update = function (self, t, dt)
	local vp = managers.viewport:first_active_viewport()

	if vp and self._vp ~= vp then
		vp.vp(vp):set_post_processor_effect("World", ids_snow_post_processor, ids_snow_ripples)

		if alive(self._vp) then
			self._vp:vp():set_post_processor_effect("World", ids_snow_post_processor, ids_snow_off)
		end

		self._vp = vp
	end

	local c_rot = managers.environment_effects:camera_rotation()

	if not c_rot then
		return 
	end

	local c_pos = managers.environment_effects:camera_position()

	if not c_pos then
		return 
	end

	World:effect_manager():move_rotate(self._effect, c_pos, c_rot)

	return 
end
SnowEffect.start = function (self)
	self._effect = World:effect_manager():spawn({
		effect = self._effect_name,
		position = Vector3(),
		rotation = Rotation()
	})

	return 
end
SnowEffect.stop = function (self)
	World:effect_manager():kill(self._effect)

	self._effect = nil

	if alive(self._vp) then
		self._vp:vp():set_post_processor_effect("World", ids_snow_post_processor, ids_snow_off)

		self._vp = nil
	end

	return 
end
LightningEffect = LightningEffect or class(EnvironmentEffect)
LightningEffect.init = function (self)
	EnvironmentEffect.init(self)

	return 
end
LightningEffect.load_effects = function (self)
	return 
end
LightningEffect._update_wait_start = function (self)
	if Underlay:loaded() then
		self.start(self)
	end

	return 
end
LightningEffect._update = function (self, t, dt)
	if not self._started then
		return 
	end

	if not self._sky_material or not self._sky_material:alive() then
		self._sky_material = Underlay:material(Idstring("sky"))
	end

	if self._flashing then
		self._update_function(self, t, dt)
	end

	if self._sound_delay then
		self._sound_delay = self._sound_delay - dt

		if self._sound_delay <= 0 then
			self._sound_delay = nil
		end
	end

	self._next = self._next - dt

	if self._next <= 0 then
		self._set_lightning_values(self)
		self._make_lightning(self)

		self._update_function = self._update_first

		self._set_next_timer(self)

		self._flashing = true
	end

	return 
end
LightningEffect.start = function (self)
	if not Underlay:loaded() then
		self.update = self._update_wait_start

		return 
	end

	print("[LightningEffect] Start")

	self._started = true
	self.update = self._update
	self._sky_material = Underlay:material(Idstring("sky"))
	self._original_color0 = self._sky_material:get_variable(Idstring("color0"))
	self._original_light_color = Global._global_light:color()
	self._original_sun_horizontal = Underlay:time(Idstring("sun_horizontal"))
	self._min_interval = tweak_data.env_effect.lightning.min_interval
	self._rnd_interval = tweak_data.env_effect.lightning.rnd_interval
	self._sound_source = SoundDevice:create_source("thunder")

	self._set_next_timer(self)

	return 
end
LightningEffect.stop = function (self)
	print("[LightningEffect] Stop")

	self._started = false

	if self._soundsource then
		self._soundsource:stop()
		self._soundsource:delete()

		self._soundsource = nil
	end

	self._set_original_values(self)

	return 
end
LightningEffect._update_first = function (self, t, dt)
	self._first_flash_time = self._first_flash_time - dt

	if self._first_flash_time <= 0 then
		self._set_original_values(self)

		self._update_function = self._update_pause
	end

	return 
end
LightningEffect._update_pause = function (self, t, dt)
	self._pause_flash_time = self._pause_flash_time - dt

	if self._pause_flash_time <= 0 then
		self._make_lightning(self)

		self._update_function = self._update_second
	end

	return 
end
LightningEffect._update_second = function (self, t, dt)
	self._second_flash_time = self._second_flash_time - dt

	if self._second_flash_time <= 0 then
		self._set_original_values(self)

		self._flashing = false
	end

	return 
end
LightningEffect._set_original_values = function (self)
	if alive(self._sky_material) then
		self._sky_material:set_variable(Idstring("color0"), self._original_color0)
	end

	if self._original_light_color then
		Global._global_light:set_color(self._original_light_color)
	end

	if self._original_sun_horizontal then
		Underlay:set_time(Idstring("sun_horizontal"), self._original_sun_horizontal)
	end

	return 
end
LightningEffect._make_lightning = function (self)
	if alive(self._sky_material) then
		self._sky_material:set_variable(Idstring("color0"), self._intensity_value)
	end

	Global._global_light:set_color(self._intensity_value)
	Underlay:set_time(Idstring("sun_horizontal"), self._flash_anim_time)
	self._sound_source:post_event(tweak_data.env_effect.lightning.event_name)

	return 
end
LightningEffect._set_lightning_values = function (self)
	self._first_flash_time = 0.1
	self._pause_flash_time = 0.1
	self._second_flash_time = 0.3
	self._flash_roll = math.rand(360)
	self._flash_dir = Rotation(0, 0, self._flash_roll):y()
	self._flash_anim_time = math.rand(0, 1)
	self._distance = math.rand(1)
	self._intensity_value = math.lerp(Vector3(2, 2, 2), Vector3(5, 5, 5), self._distance)
	local c_pos = managers.environment_effects:camera_position()

	if c_pos then
		local sound_speed = 30000
		self._sound_delay = self._distance*2

		if self._sound_source then
			self._sound_source:set_rtpc("lightning_distance", self._distance*4000)
		end
	end

	return 
end
LightningEffect._set_next_timer = function (self)
	self._next = self._min_interval + math.rand(self._rnd_interval)

	return 
end
RainDropEffect = RainDropEffect or class(EnvironmentEffect)
RainDropEffect.init = function (self)
	EnvironmentEffect.init(self)

	self._under_roof = false
	self._slotmask = managers.slot:get_mask("statics")

	return 
end
RainDropEffect.load_effects = function (self)
	if is_editor then
		CoreEngineAccess._editor_load(Idstring("effect"), self._effect_name)
	end

	return 
end
RainDropEffect.update = function (self, t, dt)
	return 
end
RainDropEffect.start = function (self)
	local t = {
		effect = self._effect_name,
		position = Vector3(),
		rotation = Rotation()
	}
	self._raindrops = World:effect_manager():spawn(t)
	self._extra_raindrops = World:effect_manager():spawn(t)

	return 
end
RainDropEffect.stop = function (self)
	if self._raindrops then
		World:effect_manager():fade_kill(self._raindrops)
		World:effect_manager():fade_kill(self._extra_raindrops)

		self._raindrops = nil
	end

	return 
end
RainDropScreenEffect = RainDropScreenEffect or class(RainDropEffect)
RainDropScreenEffect.init = function (self)
	RainDropEffect.init(self)

	self._effect_name = Idstring("effects/particles/rain/raindrop_screen")

	return 
end

CoreClass.override_class(CoreEnvironmentEffectsManager.EnvironmentEffectsManager, EnvironmentEffectsManager)

return 
