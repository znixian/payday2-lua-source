GroupAIStateEmpty = GroupAIStateEmpty or class(GroupAIStateBase)
GroupAIStateEmpty.assign_enemy_to_group_ai = function (self, unit)
	return 
end
GroupAIStateEmpty.on_enemy_tied = function (self, u_key)
	return 
end
GroupAIStateEmpty.on_enemy_untied = function (self, u_key)
	return 
end
GroupAIStateEmpty.on_civilian_tied = function (self, u_key)
	return 
end
GroupAIStateEmpty.can_hostage_flee = function (self)
	return 
end
GroupAIStateEmpty.add_to_surrendered = function (self, unit, update)
	return 
end
GroupAIStateEmpty.remove_from_surrendered = function (self, unit)
	return 
end
GroupAIStateEmpty.flee_point = function (self, start_nav_seg)
	return 
end
GroupAIStateEmpty.on_security_camera_spawned = function (self)
	return 
end
GroupAIStateEmpty.on_security_camera_broken = function (self)
	return 
end
GroupAIStateEmpty.on_security_camera_destroyed = function (self)
	return 
end
GroupAIStateEmpty.on_nav_segment_state_change = function (self, changed_seg, state)
	return 
end
GroupAIStateEmpty.set_area_min_police_force = function (self, id, force, pos)
	return 
end
GroupAIStateEmpty.set_wave_mode = function (self, flag)
	return 
end
GroupAIStateEmpty.add_preferred_spawn_points = function (self, id, spawn_points)
	return 
end
GroupAIStateEmpty.remove_preferred_spawn_points = function (self, id)
	return 
end
GroupAIStateEmpty.register_criminal = function (self, unit)
	return 
end
GroupAIStateEmpty.unregister_criminal = function (self, unit)
	return 
end
GroupAIStateEmpty.on_defend_travel_end = function (self, unit, objective)
	return 
end
GroupAIStateEmpty.is_area_safe = function (self)
	return true
end
GroupAIStateEmpty.is_nav_seg_safe = function (self)
	return true
end
GroupAIStateEmpty.set_mission_fwd_vector = function (self, direction)
	return 
end
GroupAIStateEmpty.set_drama_build_period = function (self, period)
	return 
end
GroupAIStateEmpty.add_special_objective = function (self, id, objective_data)
	return 
end
GroupAIStateEmpty.remove_special_objective = function (self, id)
	return 
end
GroupAIStateEmpty.save = function (self, save_data)
	return 
end
GroupAIStateEmpty.load = function (self, load_data)
	return 
end
GroupAIStateEmpty.on_cop_jobless = function (self, unit)
	return 
end
GroupAIStateEmpty.spawn_one_teamAI = function (self, unit)
	return 
end
GroupAIStateEmpty.remove_one_teamAI = function (self, unit)
	return 
end
GroupAIStateEmpty.fill_criminal_team_with_AI = function (self, unit)
	return 
end
GroupAIStateEmpty.set_importance_weight = function (self, cop_unit, dis_report)
	return 
end
GroupAIStateEmpty.on_criminal_recovered = function (self, criminal_unit)
	return 
end
GroupAIStateEmpty.on_criminal_disabled = function (self, unit)
	return 
end
GroupAIStateEmpty.on_criminal_neutralized = function (self, unit)
	return 
end
GroupAIStateEmpty.is_detection_persistent = function (self)
	return 
end
GroupAIStateEmpty.on_nav_link_unregistered = function (self)
	return 
end
GroupAIStateEmpty.save = function (self)
	return 
end
GroupAIStateEmpty.load = function (self)
	return 
end

return 
