UnitBase = UnitBase or class()
UnitBase.init = function (self, unit, update_enabled)
	self._unit = unit

	if not update_enabled then
		unit.set_extension_update_enabled(unit, Idstring("base"), false)
	end

	self._destroy_listener_holder = ListenerHolder:new()

	return 
end
UnitBase.add_destroy_listener = function (self, key, clbk)
	if not self._destroying then
		self._destroy_listener_holder:add(key, clbk)
	end

	return 
end
UnitBase.remove_destroy_listener = function (self, key)
	self._destroy_listener_holder:remove(key)

	return 
end
UnitBase.save = function (self, data)
	return 
end
UnitBase.load = function (self, data)
	managers.worlddefinition:use_me(self._unit)

	return 
end
UnitBase.pre_destroy = function (self, unit)
	self._destroying = true

	self._destroy_listener_holder:call(unit)

	return 
end
UnitBase.destroy = function (self, unit)
	if self._destroying then
		return 
	end

	self._destroy_listener_holder:call(unit)

	return 
end
UnitBase.set_slot = function (self, unit, slot)
	unit.set_slot(unit, slot)

	return 
end

return 
