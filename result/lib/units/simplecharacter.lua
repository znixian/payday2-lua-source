SimpleCharacter = SimpleCharacter or class()
SimpleCharacter.SPEED = 150
SimpleCharacter.init = function (self, unit)
	self._unit = unit

	return 
end
SimpleCharacter.update = function (self, unit, t, dt)
	self.move(self, t, dt)

	return 
end
SimpleCharacter.paused_update = function (self, unit, t, dt)
	self.move(self, t, dt)

	return 
end
SimpleCharacter.move = function (self, t, dt)
	local move_vec = Vector3(0, 0, 0)
	local keyboard = Input:keyboard()
	local viewport = managers.viewport:first_active_viewport()

	if viewport == nil then
		return 
	end

	local camera = viewport.camera(viewport)
	local cam_rot = camera.rotation(camera)
	local rotation = Rotation(90, cam_rot.pitch(cam_rot), cam_rot.roll(cam_rot))

	if self._unit:mover() then
		if keyboard.down(keyboard, Idstring("t")) then
			move_vec = move_vec - rotation.z(rotation)
		end

		if keyboard.down(keyboard, Idstring("g")) then
			move_vec = move_vec + rotation.z(rotation)
		end

		if keyboard.down(keyboard, Idstring("f")) then
			move_vec = move_vec - rotation.x(rotation)
		end

		if keyboard.down(keyboard, Idstring("h")) then
			move_vec = move_vec + rotation.x(rotation)
		end

		local length = move_vec.length(move_vec)

		if 0.001 < length then
			move_vec = move_vec/length
		end

		move_vec = move_vec*self.SPEED*dt

		if keyboard.down(keyboard, Idstring("c")) then
			self._unit:mover():jump()
		end

		if keyboard.down(keyboard, Idstring("k")) then
			self._unit:mover():set_stiff(true)
		end

		if keyboard.down(keyboard, Idstring("l")) then
			self._unit:mover():set_stiff(false)
		end
	end

	self._unit:move(move_vec)

	return 
end

return 
