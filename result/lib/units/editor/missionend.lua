MissionEndUnitElement = MissionEndUnitElement or class(MissionElement)
MissionEndUnitElement.init = function (self, unit)
	MissionEndUnitElement.super.init(self, unit)

	self._hed.state = "none"

	table.insert(self._save_values, "state")

	return 
end
MissionEndUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "state", {
		"none",
		"success",
		"failed",
		"leave",
		"leave_safehouse"
	})

	return 
end

return 
