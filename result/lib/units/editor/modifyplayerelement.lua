ModifyPlayerElement = ModifyPlayerElement or class(MissionElement)
ModifyPlayerElement.init = function (self, unit)
	ModifyPlayerElement.super.init(self, unit)

	self._hed.damage_fall_disabled = false
	self._hed.invulnerable = nil

	table.insert(self._save_values, "damage_fall_disabled")
	table.insert(self._save_values, "invulnerable")

	return 
end
ModifyPlayerElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_checkbox(self, panel, panel_sizer, "damage_fall_disabled", "Set player damage fall disabled", "Set player damage fall disabled")
	self._build_value_checkbox(self, panel, panel_sizer, "invulnerable", "Player cannot be hurt", "Invulnerable")
	self._add_help_text(self, "Modifies player properties. The changes are only applied to a player as instigator and cannot be used as a global state")

	return 
end

return 
