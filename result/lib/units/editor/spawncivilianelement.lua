core:import("CoreEditorUtils")
core:import("CoreUnit")

SpawnCivilianUnitElement = SpawnCivilianUnitElement or class(MissionElement)
SpawnCivilianUnitElement.USES_POINT_ORIENTATION = true
SpawnCivilianUnitElement.INSTANCE_VAR_NAMES = {
	{
		value = "enemy",
		type = "civilian"
	},
	{
		value = "state",
		type = "civilian_spawn_state"
	}
}
SpawnCivilianUnitElement.init = function (self, unit)
	SpawnCivilianUnitElement.super.init(self, unit)

	self._enemies = {}
	self._states = CopActionAct._act_redirects.civilian_spawn
	self._hed.state = "none"
	self._hed.enemy = "units/payday2/characters/civ_male_casual_1/civ_male_casual_1"
	self._hed.force_pickup = "none"
	self._hed.team = "default"

	table.insert(self._save_values, "enemy")
	table.insert(self._save_values, "state")
	table.insert(self._save_values, "force_pickup")
	table.insert(self._save_values, "team")

	return 
end
SpawnCivilianUnitElement.post_init = function (self, ...)
	SpawnCivilianUnitElement.super.post_init(self, ...)
	self._load_pickup(self)

	return 
end
SpawnCivilianUnitElement.test_element = function (self)
	SpawnEnemyUnitElement.test_element(self)

	return 
end
SpawnCivilianUnitElement.get_spawn_anim = function (self)
	return self._hed.state
end
SpawnCivilianUnitElement.stop_test_element = function (self)
	for _, enemy in ipairs(self._enemies) do
		if enemy.base(enemy) and enemy.base(enemy).set_slot then
			enemy.base(enemy):set_slot(enemy, 0)
		else
			enemy.set_slot(enemy, 0)
		end
	end

	self._enemies = {}

	return 
end
SpawnCivilianUnitElement.set_element_data = function (self, params, ...)
	SpawnCivilianUnitElement.super.set_element_data(self, params, ...)

	if params.value == "force_pickup" then
		self._load_pickup(self)
	end

	return 
end
SpawnCivilianUnitElement._reload_unit_list_btn = function (self)
	self.stop_test_element(self)

	if self._hed.enemy ~= "none" then
		managers.editor:reload_units({
			Idstring(self._hed.enemy)
		}, true, true)
	end

	return 
end
SpawnCivilianUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local enemy_sizer = EWS:BoxSizer("HORIZONTAL")

	panel_sizer.add(panel_sizer, enemy_sizer, 0, 0, "EXPAND")
	self._build_value_combobox(self, panel, enemy_sizer, "enemy", self._options, nil, nil, {
		horizontal_sizer_proportions = 1
	})

	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "ADD_UNIT_LIST", "Reload unit", CoreEws.image_path("toolbar\\refresh_16x16.png"), nil)
	toolbar.connect(toolbar, "ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_reload_unit_list_btn"), nil)
	toolbar.realize(toolbar)
	enemy_sizer.add(enemy_sizer, toolbar, 0, 0, "EXPAND,LEFT")
	self._build_value_combobox(self, panel, panel_sizer, "state", table.list_add(self._states, {
		"none"
	}))

	local pickups = table.map_keys(tweak_data.pickups)

	table.insert(pickups, "none")
	self._build_value_combobox(self, panel, panel_sizer, "force_pickup", pickups)
	self._build_value_combobox(self, panel, panel_sizer, "team", table.list_add({
		"default"
	}, tweak_data.levels:get_team_names_indexed()), "Select the character's team.")

	return 
end
SpawnCivilianUnitElement._load_pickup = function (self)
	if self._hed.force_pickup ~= "none" then
		local unit_name = tweak_data.pickups[self._hed.force_pickup].unit

		CoreUnit.editor_load_unit(unit_name)
	end

	return 
end
SpawnCivilianUnitElement.add_to_mission_package = function (self)
	if self._hed.force_pickup ~= "none" then
		local unit_name = tweak_data.pickups[self._hed.force_pickup].unit

		managers.editor:add_to_world_package({
			category = "units",
			name = unit_name.s(unit_name),
			continent = self._unit:unit_data().continent
		})

		local sequence_files = {}

		CoreEditorUtils.get_sequence_files_by_unit_name(unit_name, sequence_files)

		for _, file in ipairs(sequence_files) do
			managers.editor:add_to_world_package({
				init = true,
				category = "script_data",
				name = file.s(file) .. ".sequence_manager",
				continent = self._unit:unit_data().continent
			})
		end
	end

	return 
end
SpawnCivilianUnitElement._resolve_team = function (self, unit)
	if self._hed.team == "default" then
		return tweak_data.levels:get_default_team_ID("non_combatant")
	else
		return self._hed.team
	end

	return 
end
SpawnCivilianUnitElement.destroy = function (self, ...)
	SpawnCivilianUnitElement.super.destroy(self, ...)
	self.stop_test_element(self)

	return 
end

return 
