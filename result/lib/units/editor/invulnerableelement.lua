InvulnerableUnitElement = InvulnerableUnitElement or class(MissionElement)
InvulnerableUnitElement.LINK_ELEMENTS = {
	"elements"
}
InvulnerableUnitElement.init = function (self, unit)
	InvulnerableUnitElement.super.init(self, unit)

	self._hed.invulnerable = true
	self._hed.immortal = false
	self._hed.elements = {}

	table.insert(self._save_values, "invulnerable")
	table.insert(self._save_values, "immortal")
	table.insert(self._save_values, "elements")

	return 
end
InvulnerableUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	MissionElement.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.85,
				b = 0,
				r = 0,
				from_unit = unit,
				to_unit = self._unit
			})
		end
	end

	return 
end
InvulnerableUnitElement.get_links_to_unit = function (self, ...)
	InvulnerableUnitElement.super.get_links_to_unit(self, ...)
	self._get_links_of_type_from_elements(self, self._hed.elements, "trigger", ...)

	return 
end
InvulnerableUnitElement.update_editing = function (self)
	return 
end
InvulnerableUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and self._correct_unit(self, ray.unit:name():s()) then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
InvulnerableUnitElement._correct_unit = function (self, u_name)
	local names = {
		"ai_spawn_enemy",
		"ai_enemy_group",
		"ai_spawn_civilian",
		"ai_civilian_group",
		"point_spawn_player"
	}

	for _, name in ipairs(names) do
		if string.find(u_name, name, 1, true) then
			return true
		end
	end

	return false
end
InvulnerableUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
InvulnerableUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local invulnerable = EWS:CheckBox(panel, "Invulnerable", "")

	invulnerable.set_value(invulnerable, self._hed.invulnerable)
	invulnerable.connect(invulnerable, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "invulnerable",
		ctrlr = invulnerable
	})
	panel_sizer.add(panel_sizer, invulnerable, 0, 0, "EXPAND")

	local immortal = EWS:CheckBox(panel, "Immortal", "")

	immortal.set_value(immortal, self._hed.immortal)
	immortal.connect(immortal, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "immortal",
		ctrlr = immortal
	})
	panel_sizer.add(panel_sizer, immortal, 0, 0, "EXPAND")

	local help = {
		text = "Makes a unit invulnerable or immortal.",
		panel = panel,
		sizer = panel_sizer
	}

	self.add_help_text(self, help)

	return 
end

return 
