MandatoryBagsUnitElement = MandatoryBagsUnitElement or class(MissionElement)
MandatoryBagsUnitElement.SAVE_UNIT_POSITION = false
MandatoryBagsUnitElement.SAVE_UNIT_ROTATION = false
MandatoryBagsUnitElement.init = function (self, unit)
	MandatoryBagsUnitElement.super.init(self, unit)

	self._hed.carry_id = "none"
	self._hed.amount = 0

	table.insert(self._save_values, "carry_id")
	table.insert(self._save_values, "amount")

	return 
end
MandatoryBagsUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "carry_id", table.list_add({
		"none"
	}, tweak_data.carry:get_carry_ids()))
	self._build_value_number(self, panel, panel_sizer, "amount", {
		min = 0,
		floats = 0,
		max = 100
	}, "Amount of mandatory bags.")

	return 
end

return 
