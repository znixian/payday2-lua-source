VehicleOperatorUnitElement = VehicleOperatorUnitElement or class(MissionElement)
VehicleOperatorUnitElement.ACTIONS = {
	"none",
	"lock",
	"unlock",
	"secure",
	"break_down",
	"repair",
	"damage",
	"activate",
	"deactivate",
	"block"
}
VehicleOperatorUnitElement.init = function (self, unit)
	VehicleOperatorUnitElement.super.init(self, unit)

	self._hed.operation = "none"
	self._hed.damage = "0"
	self._hed.elements = {}

	table.insert(self._save_values, "use_instigator")
	table.insert(self._save_values, "operation")
	table.insert(self._save_values, "damage")
	table.insert(self._save_values, "elements")

	self._actions = VehicleOperatorUnitElement.ACTIONS

	return 
end
VehicleOperatorUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body",
		sample = true,
		mask = managers.slot:get_mask("vehicles")
	})

	if ray and ray.unit then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
VehicleOperatorUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
VehicleOperatorUnitElement.update_editing = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body",
		sample = true,
		mask = managers.slot:get_mask("vehicles")
	})

	if ray and ray.unit then
		local sequences = managers.sequence:get_sequence_list(ray.unit:name())

		if 0 < #sequences then
			Application:draw(ray.unit, 0, 1, 0)
		end
	end

	return 
end
VehicleOperatorUnitElement.draw_links_unselected = function (self, ...)
	VehicleOperatorUnitElement.super.draw_links_unselected(self, ...)

	for _, id in ipairs(self._hed.elements) do
		local unit = managers.editor:unit_with_id(id)

		if alive(unit) then
			local params = {
				g = 0,
				b = 0.5,
				r = 0,
				from_unit = unit,
				to_unit = self._unit
			}

			self._draw_link(self, params)
			Application:draw(unit, 0, 0, 0.5)
		end
	end

	return 
end
VehicleOperatorUnitElement.draw_links_selected = function (self, ...)
	VehicleOperatorUnitElement.super.draw_links_selected(self, ...)

	for _, id in ipairs(self._hed.elements) do
		local unit = managers.editor:unit_with_id(id)
		local params = {
			g = 0,
			b = 0.5,
			r = 0,
			from_unit = unit,
			to_unit = self._unit
		}

		self._draw_link(self, params)
		Application:draw(unit, 0.25, 1, 0.25)
	end

	return 
end
VehicleOperatorUnitElement.add_unit_list_btn = function (self)
	local script = self._unit:mission_element_data().script

	local function f(unit)
		if not unit.mission_element_data(unit) or unit.mission_element_data(unit).script ~= script then
			return 
		end

		local id = unit.unit_data(unit).unit_id

		if table.contains(self._hed.elements, id) then
			return false
		end

		return managers.editor:layer("Mission"):category_map()[unit.type(unit):s()]
	end

	local dialog = SelectUnitByNameModal:new("Add Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		table.insert(self._hed.elements, id)
	end

	return 
end
VehicleOperatorUnitElement.remove_unit_list_btn = function (self)
	local function f(unit)
		return table.contains(self._hed.elements, unit.unit_data(unit).unit_id)
	end

	local dialog = SelectUnitByNameModal:new("Remove Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		table.delete(self._hed.elements, id)
	end

	return 
end
VehicleOperatorUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "operation", self._actions, "Select an operation for the selected elements")
	self._build_value_number(self, panel, panel_sizer, "damage", {
		floats = 0,
		min = 1
	}, "Specify the amount of damage.")

	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	toolbar.add_tool(toolbar, "REMOVE_UNIT_LIST", "Remove unit from unit list", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	toolbar.connect(toolbar, "REMOVE_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "remove_unit_list_btn"), nil)
	toolbar.realize(toolbar)
	panel_sizer.add(panel_sizer, toolbar, 0, 1, "EXPAND,LEFT")
	self._build_value_checkbox(self, panel, panel_sizer, "use_instigator")
	self._add_help_text(self, "Choose an operation to perform on the selected elements. An element might not have the selected operation implemented and will then generate error when executed.")

	return 
end

return 
