AIAreaElement = AIAreaElement or class(MissionElement)
AIAreaElement.SAVE_UNIT_ROTATION = false
AIAreaElement.init = function (self, unit)
	AIAreaElement.super.init(self, unit)

	self._nav_seg_units = {}

	table.insert(self._save_values, "nav_segs")

	return 
end
AIAreaElement.post_init = function (self, ...)
	AIAreaElement.super.post_init(self, ...)

	return 
end
AIAreaElement.layer_finished = function (self)
	AIAreaElement.super.layer_finished(self)

	if not self._hed.nav_segs then
		return 
	end

	for _, u_id in ipairs(self._hed.nav_segs) do
		local unit = managers.worlddefinition:get_unit_on_load(u_id, callback(self, self, "load_nav_seg_unit"))

		if unit then
			self._nav_seg_units[u_id] = unit
		end
	end

	return 
end
AIAreaElement.load_nav_seg_unit = function (self, unit)
	self._nav_seg_units[unit.unit_data(unit).unit_id] = unit

	return 
end
AIAreaElement.draw_links = function (self, t, dt, selected_unit, all_units)
	AIAreaElement.super.draw_links(self, t, dt, selected_unit)

	if selected_unit and self._unit ~= selected_unit and not self._nav_seg_units[selected_unit.unit_data(selected_unit).unit_id] then
		return 
	end

	for u_id, unit in pairs(self._nav_seg_units) do
		self._draw_link(self, {
			g = 0.75,
			b = 0,
			r = 0,
			from_unit = self._unit,
			to_unit = unit
		})
	end

	return 
end
AIAreaElement.update_selected = function (self, t, dt, selected_unit, all_units)
	self._chk_units_alive(self)
	managers.editor:layer("Ai"):external_draw(t, dt)
	self.draw_links(self, t, dt, selected_unit, all_units)
	SpecialObjectiveUnitElement._highlight_if_outside_the_nav_field(self, t)

	return 
end
AIAreaElement.update_unselected = function (self, t, dt, selected_unit, all_units)
	self._chk_units_alive(self)

	return 
end
AIAreaElement._chk_units_alive = function (self)
	for u_id, unit in pairs(self._nav_seg_units) do
		if not alive(unit) then
			self._nav_seg_units[u_id] = nil

			self._remove_nav_seg(self, u_id)
		end
	end

	return 
end
AIAreaElement.update_editing = function (self)
	self._raycast(self)

	return 
end
AIAreaElement._raycast = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 19
	})

	if not ray then
		return 
	end

	if ray.unit:name():s() == "core/units/nav_surface/nav_surface" then
		Application:draw(ray.unit, 0, 1, 0)

		return ray.unit
	else
		Application:draw_sphere(ray.position, 10, 1, 1, 1)
	end

	return 
end
AIAreaElement._lmb = function (self)
	local unit = self._raycast(self)

	if not unit then
		return 
	end

	local u_id = unit.unit_data(unit).unit_id

	if self._nav_seg_units[u_id] then
		self._nav_seg_units[u_id] = nil

		self._remove_nav_seg(self, u_id)
	else
		self._nav_seg_units[u_id] = unit

		self._add_nav_seg(self, unit)
	end

	return 
end
AIAreaElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "_lmb"))

	return 
end
AIAreaElement.selected = function (self)
	AIAreaElement.super.selected(self)
	self._chk_units_alive(self)

	return 
end
AIAreaElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	return 
end
AIAreaElement.add_to_mission_package = function (self)
	return 
end
AIAreaElement._add_nav_seg = function (self, unit)
	self._hed.nav_segs = self._hed.nav_segs or {}

	table.insert(self._hed.nav_segs, unit.unit_data(unit).unit_id)

	return 
end
AIAreaElement._remove_nav_seg = function (self, u_id)
	for i, test_u_id in ipairs(self._hed.nav_segs) do
		if u_id == test_u_id then
			table.remove(self._hed.nav_segs, i)

			if #self._hed.nav_segs == 0 then
				self._hed.nav_segs = nil
			end

			return 
		end
	end

	return 
end

return 
