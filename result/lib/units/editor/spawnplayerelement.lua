SpawnPlayerElement = SpawnPlayerElement or class(MissionElement)
SpawnPlayerElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.state = managers.player:default_player_state()

	table.insert(self._save_values, "state")

	return 
end
SpawnPlayerElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "state", managers.player:player_states(), "Select a state from the combobox")
	self._add_help_text(self, "The state defines how the player will be spawned")

	return 
end
SpawnPlayerElement.add_to_mission_package = function (self)
	return 
end

return 
