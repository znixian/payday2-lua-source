TeleportPlayerUnitElement = TeleportPlayerUnitElement or class(MissionElement)
TeleportPlayerUnitElement.init = function (self, unit)
	TeleportPlayerUnitElement.super.init(self, unit)

	self._hed.state = managers.player:default_player_state()
	self._hed.refill = false
	self._hed.equip_selection = "none"
	self._hed.fade_in = 0
	self._hed.sustain = 0
	self._hed.fade_out = 0

	table.insert(self._save_values, "state")
	table.insert(self._save_values, "refill")
	table.insert(self._save_values, "equip_selection")
	table.insert(self._save_values, "fade_in")
	table.insert(self._save_values, "sustain")
	table.insert(self._save_values, "fade_out")

	return 
end
TeleportPlayerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "state", managers.player:player_states(), "Select a state from the combobox")
	self._build_value_combobox(self, panel, panel_sizer, "equip_selection", {
		"none",
		"primary",
		"secondary"
	}, "Equips the given selection or keeps the current if none")
	self._build_value_number(self, panel, panel_sizer, "fade_in", {
		floats = 2,
		min = 0
	})
	self._build_value_number(self, panel, panel_sizer, "sustain", {
		floats = 2,
		min = 0
	})
	self._build_value_number(self, panel, panel_sizer, "fade_out", {
		floats = 2,
		min = 0
	})
	self._build_value_checkbox(self, panel, panel_sizer, "refill", "Refills the player's ammo and health after teleport")

	return 
end

return 
