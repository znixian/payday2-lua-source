DisableShoutElement = DisableShoutElement or class(MissionElement)
DisableShoutElement.LINK_ELEMENTS = {
	"elements"
}
DisableShoutElement.init = function (self, unit)
	DisableShoutElement.super.init(self, unit)

	self._hed.elements = {}
	self._hed.disable_shout = true

	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "disable_shout")

	return 
end
DisableShoutElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"ai_spawn_enemy",
		"ai_spawn_civilian"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)

	local dis_shout = EWS:CheckBox(panel, "Disable shout", "")

	dis_shout.set_value(dis_shout, self._hed.disable_shout)
	dis_shout.connect(dis_shout, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "disable_shout",
		ctrlr = dis_shout
	})
	panel_sizer.add(panel_sizer, dis_shout, 0, 0, "EXPAND")

	return 
end
DisableShoutElement.update_editing = function (self)
	return 
end
DisableShoutElement.update_selected = function (self, t, dt, selected_unit, all_units)
	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.5,
				b = 1,
				r = 0.9,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
DisableShoutElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and (string.find(ray.unit:name():s(), "ai_spawn_enemy", 1, true) or string.find(ray.unit:name():s(), "ai_spawn_civilian", 1, true)) then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
DisableShoutElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end

return 
