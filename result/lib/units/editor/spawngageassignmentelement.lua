core:import("CoreEditorUtils")

SpawnGageAssignmentElement = SpawnGageAssignmentElement or class(MissionElement)
SpawnGageAssignmentElement.USES_POINT_ORIENTATION = true
SpawnGageAssignmentElement.init = function (self, unit)
	SpawnGageAssignmentElement.super.init(self, unit)

	return 
end
SpawnGageAssignmentElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	return 
end
SpawnGageAssignmentElement.destroy = function (self, ...)
	SpawnGageAssignmentElement.super.destroy(self, ...)

	return 
end

return 
