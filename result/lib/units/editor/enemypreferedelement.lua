EnemyPreferedAddUnitElement = EnemyPreferedAddUnitElement or class(MissionElement)
EnemyPreferedAddUnitElement.SAVE_UNIT_POSITION = false
EnemyPreferedAddUnitElement.SAVE_UNIT_ROTATION = false
EnemyPreferedAddUnitElement.LINK_ELEMENTS = {
	"spawn_points",
	"spawn_groups"
}
EnemyPreferedAddUnitElement.init = function (self, unit)
	EnemyPreferedRemoveUnitElement.super.init(self, unit)
	table.insert(self._save_values, "spawn_points")
	table.insert(self._save_values, "spawn_groups")

	return 
end
EnemyPreferedAddUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	EnemyPreferedRemoveUnitElement.super.draw_links(self, t, dt, selected_unit, all_units)
	self._private_draw_links(self, t, dt, selected_unit, all_units)

	return 
end
EnemyPreferedAddUnitElement._private_draw_links = function (self, t, dt, selected_unit, all_units)
	local function _draw_func(element_ids)
		if not element_ids then
			return 
		end

		for _, id in ipairs(element_ids) do
			local unit = all_units[id]
			local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

			if draw then
				self:_draw_link({
					g = 0,
					b = 0.75,
					r = 0,
					from_unit = self._unit,
					to_unit = unit
				})
			end
		end

		return 
	end

	_draw_func(self._hed.spawn_points)
	_draw_func(self._hed.spawn_groups)

	return 
end
EnemyPreferedAddUnitElement.update_editing = function (self)
	return 
end
EnemyPreferedAddUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit then
		local is_group, id = nil

		if string.find(ray.unit:name():s(), "ai_spawn_enemy", 1, true) then
			id = ray.unit:unit_data().unit_id
		elseif string.find(ray.unit:name():s(), "ai_enemy_group", 1, true) then
			id = ray.unit:unit_data().unit_id
			is_group = true
		end

		if id then
			if is_group then
				if self._hed.spawn_groups and table.contains(self._hed.spawn_groups, id) then
					self._delete_id_from_table(self, id, "spawn_groups")
				else
					self._hed.spawn_groups = self._hed.spawn_groups or {}

					table.insert(self._hed.spawn_groups, id)
				end
			elseif self._hed.spawn_points and table.contains(self._hed.spawn_points, id) then
				self._delete_id_from_table(self, id, "spawn_points")
			else
				self._hed.spawn_points = self._hed.spawn_points or {}

				table.insert(self._hed.spawn_points, id)
			end
		end
	end

	return 
end
EnemyPreferedAddUnitElement._delete_id_from_table = function (self, id, table_name)
	if not self._hed[table_name] then
		return 
	end

	table.delete(self._hed[table_name], id)

	if not next(self._hed[table_name]) then
		self._hed[table_name] = nil
	end

	return 
end
EnemyPreferedAddUnitElement.get_links_to_unit = function (self, ...)
	EnemyPreferedAddUnitElement.super.get_links_to_unit(self, ...)

	if self._hed.spawn_groups then
		self._get_links_of_type_from_elements(self, self._hed.spawn_groups, "spawn_group", ...)
	end

	if self._hed.spawn_points then
		self._get_links_of_type_from_elements(self, self._hed.spawn_points, "spawn_point", ...)
	end

	return 
end
EnemyPreferedAddUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
EnemyPreferedAddUnitElement.add_unit_list_btn = function (self)
	local script = self._unit:mission_element_data().script

	local function f(unit)
		if not unit.mission_element_data(unit) or unit.mission_element_data(unit).script ~= script then
			return 
		end

		local id = unit.unit_data(unit).unit_id

		if self._hed.spawn_points and table.contains(self._hed.spawn_points, id) then
			return false
		end

		if self._hed.spawn_groups and table.contains(self._hed.spawn_groups, id) then
			return false
		end

		if string.find(unit.name(unit):s(), "ai_enemy_group", 1, true) or string.find(unit.name(unit):s(), "ai_spawn_enemy", 1, true) then
			return true
		end

		return false
	end

	local dialog = SelectUnitByNameModal:new("Add Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		if string.find(unit.name(unit):s(), "ai_enemy_group", 1, true) then
			self._hed.spawn_groups = self._hed.spawn_groups or {}

			table.insert(self._hed.spawn_groups, id)
		elseif string.find(unit.name(unit):s(), "ai_spawn_enemy", 1, true) then
			self._hed.spawn_points = self._hed.spawn_points or {}

			table.insert(self._hed.spawn_points, id)
		end
	end

	return 
end
EnemyPreferedAddUnitElement.remove_unit_list_btn = function (self)
	local function f(unit)
		return (self._hed.spawn_groups and table.contains(self._hed.spawn_groups, unit.unit_data(unit).unit_id)) or (self._hed.spawn_points and table.contains(self._hed.spawn_points, unit.unit_data(unit).unit_id))
	end

	local dialog = SelectUnitByNameModal:new("Remove Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		self._delete_id_from_table(self, id, "spawn_groups")
		self._delete_id_from_table(self, id, "spawn_points")
	end

	return 
end
EnemyPreferedAddUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	toolbar.add_tool(toolbar, "REMOVE_UNIT_LIST", "Remove unit from unit list", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	toolbar.connect(toolbar, "REMOVE_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "remove_unit_list_btn"), nil)
	toolbar.realize(toolbar)
	panel_sizer.add(panel_sizer, toolbar, 0, 1, "EXPAND,LEFT")

	return 
end
EnemyPreferedRemoveUnitElement = EnemyPreferedRemoveUnitElement or class(MissionElement)
EnemyPreferedRemoveUnitElement.SAVE_UNIT_POSITION = false
EnemyPreferedRemoveUnitElement.SAVE_UNIT_ROTATION = false
EnemyPreferedRemoveUnitElement.LINK_ELEMENTS = {
	"elements"
}
EnemyPreferedRemoveUnitElement.init = function (self, unit)
	EnemyPreferedRemoveUnitElement.super.init(self, unit)

	self._hed.elements = {}

	table.insert(self._save_values, "elements")

	return 
end
EnemyPreferedRemoveUnitElement.update_editing = function (self)
	return 
end
EnemyPreferedRemoveUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	EnemyPreferedRemoveUnitElement.super.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0,
				b = 0,
				r = 0.75,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
EnemyPreferedRemoveUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and string.find(ray.unit:name():s(), "ai_enemy_prefered_add", 1, true) then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
EnemyPreferedRemoveUnitElement.get_links_to_unit = function (self, ...)
	EnemyPreferedRemoveUnitElement.super.get_links_to_unit(self, ...)

	if self._hed.elements then
		self._get_links_of_type_from_elements(self, self._hed.elements, "operator", ...)
	end

	return 
end
EnemyPreferedRemoveUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
EnemyPreferedRemoveUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"ai_enemy_prefered_add"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)

	return 
end

return 
