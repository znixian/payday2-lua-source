SpawnVehicleElement = SpawnVehicleElement or class(MissionElement)
SpawnVehicleElement.init = function (self, unit)
	Application:trace("SpawnVehicleElement:init", unit)
	MissionElement.init(self, unit)

	self._hed.state = VehicleDrivingExt.STATE_INACTIVE
	self._hed.vehicle = "falcogini"

	table.insert(self._save_values, "state")
	table.insert(self._save_values, "vehicle")

	return 
end
SpawnVehicleElement._build_panel = function (self, panel, panel_sizer)
	Application:trace("SpawnVehicleElement:_build_panel")
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local vehicles = {
		"falcogini",
		"escape_van",
		"muscle"
	}

	self._build_value_combobox(self, panel, panel_sizer, "vehicle", vehicles, "Select a vehicle from the combobox")
	self._add_help_text(self, "The vehicle that will be spawned")

	return 
end
SpawnVehicleElement.add_to_mission_package = function (self)
	Application:trace("SpawnVehicleElement:add_to_mission_package")

	return 
end

return 
