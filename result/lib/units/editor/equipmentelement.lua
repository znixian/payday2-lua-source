EquipmentUnitElement = EquipmentUnitElement or class(MissionElement)
EquipmentUnitElement.init = function (self, unit)
	EquipmentUnitElement.super.init(self, unit)

	self._hed.equipment = "none"
	self._hed.amount = 1

	table.insert(self._save_values, "equipment")
	table.insert(self._save_values, "amount")

	return 
end
EquipmentUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "equipment", table.list_add({
		"none"
	}, table.map_keys(tweak_data.equipments.specials)))
	self._build_value_number(self, panel, panel_sizer, "amount", {
		floats = 0,
		min = 1
	}, "Specifies how many of this equipment to recieve (only work on those who has a max_amount set in their tweak data).")

	return 
end

return 
