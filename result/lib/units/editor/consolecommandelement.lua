ConsoleCommandUnitElement = ConsoleCommandUnitElement or class(MissionElement)
ConsoleCommandUnitElement.init = function (self, unit)
	ConsoleCommandUnitElement.super.init(self, unit)

	self._hed.cmd = ""

	table.insert(self._save_values, "cmd")

	return 
end
ConsoleCommandUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local text = EWS:TextCtrl(panel, self._hed.cmd, "", "TE_MULTILINE,TE_PROCESS_ENTER")

	text.connect(text, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "cmd",
		ctrlr = text
	})
	text.connect(text, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "cmd",
		ctrlr = text
	})
	panel_sizer.add(panel_sizer, text, 1, 0, "EXPAND")

	return 
end

return 
