PickupUnitElement = PickupUnitElement or class(MissionElement)
PickupUnitElement.SAVE_UNIT_POSITION = false
PickupUnitElement.SAVE_UNIT_ROTATION = false
PickupUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.pickup = "remove"

	table.insert(self._save_values, "pickup")

	return 
end
PickupUnitElement.get_options = function ()
	return table.map_keys(tweak_data.pickups)
end
PickupUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "pickup", table.list_add({
		"remove"
	}, table.map_keys(tweak_data.pickups)), "Select a pickup to be set or remove.")

	return 
end

return 
