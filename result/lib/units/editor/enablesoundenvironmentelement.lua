EnableSoundEnvironmentElement = EnableSoundEnvironmentElement or class(MissionElement)
EnableSoundEnvironmentElement.init = function (self, unit)
	EnableSoundEnvironmentElement.super.init(self, unit)

	self._hed.enable = true
	self._hed.elements = {}

	table.insert(self._save_values, "enable")
	table.insert(self._save_values, "elements")

	return 
end
EnableSoundEnvironmentElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	self._btn_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._btn_toolbar:add_tool("ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	self._btn_toolbar:connect("ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	self._btn_toolbar:realize()
	panel_sizer.add(panel_sizer, self._btn_toolbar, 0, 1, "EXPAND,LEFT")

	local enable_sound_env = EWS:CheckBox(panel, "Enable", "")

	enable_sound_env.set_value(enable_sound_env, self._hed.enable)
	enable_sound_env.connect(enable_sound_env, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "enable",
		ctrlr = enable_sound_env
	})
	panel_sizer.add(panel_sizer, enable_sound_env, 0, 0, "EXPAND")

	return 
end
EnableSoundEnvironmentElement.update_editing = function (self)
	return 
end
EnableSoundEnvironmentElement.update_selected = function (self, t, dt)
	for _, area in ipairs(managers.sound_environment:areas()) do
		for _, name in ipairs(self._hed.elements) do
			if area.name(area) == name then
				self._draw_link(self, {
					g = 0.5,
					b = 1,
					r = 0.9,
					from_unit = self._unit,
					to_unit = area.unit(area)
				})
			end
		end
	end

	return 
end
EnableSoundEnvironmentElement.update_unselected = function (self)
	for _, name in ipairs(self._hed.elements) do
		for _, area in ipairs(managers.sound_environment:areas()) do
			if area.name(area) == name and not alive(area.unit(area)) then
				self._add_or_remove_graph(self, name)
			end
		end
	end

	return 
end
EnableSoundEnvironmentElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and string.find(ray.unit:name():s(), "core/units/sound_environment/sound_environment", 1, true) then
		for _, area in ipairs(managers.sound_environment:areas()) do
			if area.unit(area):key() == ray.unit:key() then
				self._add_or_remove_graph(self, area.name(area))

				return 
			end
		end
	end

	return 
end
EnableSoundEnvironmentElement.remove_links = function (self, unit)
	for _, area in ipairs(managers.sound_environment:areas()) do
		if area.unit(area):key() == unit.key(unit) then
			for _, name in ipairs(self._hed.elements) do
				if name == area.name(area) then
					table.delete(self._hed.elements, name)
				end
			end
		end
	end

	return 
end
EnableSoundEnvironmentElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
EnableSoundEnvironmentElement.add_unit_list_btn = function (self)
	local function f(unit)
		return unit.type(unit) == Idstring("sound")
	end

	local dialog = SelectUnitByNameModal:new("Add Trigger Unit", f)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		for _, area in ipairs(managers.sound_environment:areas()) do
			if area.unit(area):key() == unit.key(unit) then
				self._add_or_remove_graph(self, area.name(area))

				return 
			end
		end
	end

	return 
end
EnableSoundEnvironmentElement._add_or_remove_graph = function (self, id)
	if table.contains(self._hed.elements, id) then
		table.delete(self._hed.elements, id)
	else
		table.insert(self._hed.elements, id)
	end

	return 
end

return 
