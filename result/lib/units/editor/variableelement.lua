VariableElement = VariableElement or class(MissionElement)
VariableElement.SAVE_UNIT_POSITION = false
VariableElement.SAVE_UNIT_ROTATION = false
VariableElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.elements = {}
	self._hed.variable = ""
	self._hed.activated = true

	table.insert(self._save_values, "variable")
	table.insert(self._save_values, "activated")

	return 
end
VariableElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local text_sizer = EWS:BoxSizer("HORIZONTAL")
	local name = EWS:StaticText(panel, "Variable:", 0, "")

	text_sizer.add(text_sizer, name, 1, 0, "ALIGN_CENTER_VERTICAL,RIGHT,EXPAND")

	local input = EWS:TextCtrl(panel, self._hed.variable, "", "TE_PROCESS_ENTER")

	input.set_tool_tip(input, "Name of the variable to be used.")
	text_sizer.add(text_sizer, input, 3, 0, "RIGHT,EXPAND")
	input.connect(input, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "variable",
		ctrlr = input
	})
	input.connect(input, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "variable",
		ctrlr = input
	})
	panel_sizer.add(panel_sizer, text_sizer, 0, 0, "EXPAND")
	self._build_value_checkbox(self, panel, panel_sizer, "activated", "Set if the variable is active and uncheck if the variable is disabled.")

	return 
end

return 
