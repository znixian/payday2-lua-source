AIGraphUnitElement = AIGraphUnitElement or class(MissionElement)
AIGraphUnitElement.LINK_ELEMENTS = {
	"elements"
}
AIGraphUnitElement.init = function (self, unit)
	EnemyPreferedRemoveUnitElement.super.init(self, unit)

	self._hed.graph_ids = {}
	self._hed.operation = NavigationManager.nav_states[1]

	table.insert(self._save_values, "graph_ids")
	table.insert(self._save_values, "operation")

	return 
end
AIGraphUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	EnemyPreferedRemoveUnitElement.super.draw_links(self, t, dt, selected_unit)

	return 
end
AIGraphUnitElement.update_editing = function (self)
	return 
end
AIGraphUnitElement._get_unit = function (self, id)
	for _, unit in ipairs(managers.editor:layer("Ai"):created_units()) do
		if unit.unit_data(unit).unit_id == id then
			return unit
		end
	end

	return 
end
AIGraphUnitElement.update_selected = function (self, t, dt)
	managers.editor:layer("Ai"):external_draw(t, dt)

	for _, unit in ipairs(managers.editor:layer("Ai"):created_units()) do
		for _, id in ipairs(self._hed.graph_ids) do
			if unit.unit_data(unit).unit_id == id then
				self._draw_link(self, {
					g = 0.75,
					b = 0,
					r = 0,
					from_unit = self._unit,
					to_unit = unit
				})
			end
		end
	end

	return 
end
AIGraphUnitElement.update_unselected = function (self)
	for _, id in ipairs(self._hed.graph_ids) do
		local unit = self._get_unit(self, id)

		if not alive(unit) then
			self._add_or_remove_graph(self, id)
		end
	end

	return 
end
AIGraphUnitElement._add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 19
	})

	if ray and ray.unit and string.find(ray.unit:name():s(), "nav_surface", 1, true) then
		self._add_or_remove_graph(self, ray.unit:unit_data().unit_id)
	end

	return 
end
AIGraphUnitElement._add_or_remove_graph = function (self, id)
	if table.contains(self._hed.graph_ids, id) then
		table.delete(self._hed.graph_ids, id)
	else
		table.insert(self._hed.graph_ids, id)
	end

	return 
end
AIGraphUnitElement.add_unit_list_btn = function (self)
	local function f(unit)
		return unit.type(unit) == Idstring("ai")
	end

	local dialog = SelectUnitByNameModal:new("Add Trigger Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		self._add_or_remove_graph(self, unit.unit_data(unit).unit_id)
	end

	return 
end
AIGraphUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "_add_element"))

	return 
end
AIGraphUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	self._btn_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._btn_toolbar:add_tool("ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	self._btn_toolbar:connect("ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	self._btn_toolbar:realize()
	panel_sizer.add(panel_sizer, self._btn_toolbar, 0, 1, "EXPAND,LEFT")

	local operations_params = {
		name = "Operation:",
		name_proportions = 1,
		tooltip = "Select an operation to perform on the selected graphs",
		sorted = true,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = panel_sizer,
		options = NavigationManager.nav_states,
		value = self._hed.operation
	}
	local operations = CoreEWS.combobox(operations_params)

	operations.connect(operations, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		value = "operation",
		ctrlr = operations
	})

	local help = {
		text = "The operation defines what to do with the selected graphs",
		panel = panel,
		sizer = panel_sizer
	}

	self.add_help_text(self, help)

	return 
end

return 
