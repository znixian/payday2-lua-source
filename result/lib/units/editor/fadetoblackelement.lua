FadeToBlackElement = FadeToBlackElement or class(MissionElement)
FadeToBlackElement.init = function (self, unit)
	FadeToBlackElement.super.init(self, unit)

	self._hed.state = false

	table.insert(self._save_values, "state")

	return 
end
FadeToBlackElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local state = EWS:CheckBox(panel, "Fade in/out", "")

	state.set_value(state, self._hed.state)
	state.connect(state, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "state",
		ctrlr = state
	})
	panel_sizer.add(panel_sizer, state, 0, 0, "EXPAND")

	local help = {
		text = "Fade in or out, takes 3 seconds. Hardcore.",
		panel = panel,
		sizer = panel_sizer
	}

	self.add_help_text(self, help)

	return 
end

return 
