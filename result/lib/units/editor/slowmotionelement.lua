SlowMotionElement = SlowMotionElement or class(MissionElement)
SlowMotionElement.SAVE_UNIT_POSITION = false
SlowMotionElement.SAVE_UNIT_ROTATION = false
SlowMotionElement.init = function (self, unit)
	SlowMotionElement.super.init(self, unit)

	self._hed.eff_name = ""

	table.insert(self._save_values, "eff_name")

	return 
end
SlowMotionElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "eff_name", table.map_keys(tweak_data.timespeed.mission_effects), "Choose effect. Descriptions in lib/TimeSpeedEffectTweakData.lua")

	local help = {
		panel = panel,
		sizer = panel_sizer,
		text = "Choose effect. Descriptions in lib/TimeSpeedEffectTweakData.lua."
	}

	self.add_help_text(self, help)

	return 
end

return 
