SetOutlineElement = SetOutlineElement or class(MissionElement)
SetOutlineElement.LINK_ELEMENTS = {
	"elements"
}
SetOutlineElement.init = function (self, unit)
	SetOutlineElement.super.init(self, unit)

	self._hed.elements = {}
	self._hed.set_outline = true
	self._hed.use_instigator = false
	self._hed.clear_previous = false

	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "set_outline")
	table.insert(self._save_values, "use_instigator")
	table.insert(self._save_values, "clear_previous")

	return 
end
SetOutlineElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"ai_spawn_enemy",
		"ai_spawn_civilian"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)

	local set_outline = EWS:CheckBox(panel, "Enable outline", "")

	set_outline.set_value(set_outline, self._hed.set_outline)
	set_outline.connect(set_outline, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "set_outline",
		ctrlr = set_outline
	})
	panel_sizer.add(panel_sizer, set_outline, 0, 0, "EXPAND")
	self._build_value_checkbox(self, panel, panel_sizer, "use_instigator", "Sets outline on the instigator")
	self._build_value_checkbox(self, panel, panel_sizer, "clear_previous", "Clears any previously set outlines (fixes issue with escorts)")

	return 
end
SetOutlineElement.draw_links = function (self, t, dt, selected_unit, all_units)
	MissionElement.draw_links(self, t, dt, selected_unit, all_units)

	return 
end
SetOutlineElement.update_editing = function (self)
	return 
end
SetOutlineElement.update_selected = function (self, t, dt, selected_unit, all_units)
	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.5,
				b = 1,
				r = 0.9,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
SetOutlineElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and (string.find(ray.unit:name():s(), "ai_spawn_enemy", 1, true) or string.find(ray.unit:name():s(), "ai_spawn_civilian", 1, true)) then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
SetOutlineElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end

return 
