CharacterTeamElement = CharacterTeamElement or class(MissionElement)
CharacterTeamElement.SAVE_UNIT_POSITION = false
CharacterTeamElement.SAVE_UNIT_ROTATION = false
CharacterTeamElement.LINK_ELEMENTS = {
	"elements"
}
CharacterTeamElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.elements = {}
	self._hed.ignore_disabled = nil
	self._hed.team = ""
	self._hed.use_instigator = nil

	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "ignore_disabled")
	table.insert(self._save_values, "team")
	table.insert(self._save_values, "use_instigator")

	return 
end
CharacterTeamElement.draw_links = function (self, t, dt, selected_unit, all_units)
	MissionElement.draw_links(self, t, dt, selected_unit, all_units)

	return 
end
CharacterTeamElement.update_editing = function (self)
	return 
end
CharacterTeamElement.update_selected = function (self, t, dt, selected_unit, all_units)
	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.75,
				b = 0,
				r = 0,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
CharacterTeamElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and (string.find(ray.unit:name():s(), "ai_spawn_enemy", 1, true) or string.find(ray.unit:name():s(), "ai_spawn_civilian", 1, true)) then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
CharacterTeamElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CharacterTeamElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"ai_spawn_enemy",
		"ai_spawn_civilian"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)

	local use_instigator = EWS:CheckBox(panel, "Use instigator", "")

	use_instigator.set_value(use_instigator, self._hed.use_instigator)
	use_instigator.connect(use_instigator, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "use_instigator",
		ctrlr = use_instigator
	})
	panel_sizer.add(panel_sizer, use_instigator, 0, 0, "EXPAND")

	local ignore_disabled = EWS:CheckBox(panel, "Ignore disabled", "")

	ignore_disabled.set_tool_tip(ignore_disabled, "Select if disabled spawn points should be ignored or not")
	ignore_disabled.set_value(ignore_disabled, self._hed.ignore_disabled)
	ignore_disabled.connect(ignore_disabled, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "ignore_disabled",
		ctrlr = ignore_disabled
	})
	panel_sizer.add(panel_sizer, ignore_disabled, 0, 0, "EXPAND")

	local team_params = {
		default = "",
		name = "Team:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Select wanted team for the character.",
		sorted = true,
		panel = panel,
		sizer = panel_sizer,
		options = tweak_data.levels:get_team_names_indexed(),
		value = self._hed.team
	}
	local team_combo_box = CoreEWS.combobox(team_params)

	team_combo_box.connect(team_combo_box, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		value = "team",
		ctrlr = team_combo_box
	})
	team_combo_box.connect(team_combo_box, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "team",
		ctrlr = team_combo_box
	})

	return 
end

return 
