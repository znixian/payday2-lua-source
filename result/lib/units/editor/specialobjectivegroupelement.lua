SpecialObjectiveGroupElement = SpecialObjectiveGroupElement or class(MissionElement)
SpecialObjectiveGroupElement.init = function (self, unit)
	SpecialObjectiveGroupElement.super.init(self, unit)

	self._hed.base_chance = 1
	self._hed.use_instigator = false
	self._hed.followup_elements = nil
	self._hed.spawn_instigator_ids = nil
	self._hed.mode = "randomizer"

	table.insert(self._save_values, "base_chance")
	table.insert(self._save_values, "use_instigator")
	table.insert(self._save_values, "followup_elements")
	table.insert(self._save_values, "spawn_instigator_ids")
	table.insert(self._save_values, "mode")

	return 
end
SpecialObjectiveGroupElement.post_init = function (self, ...)
	SpecialObjectiveGroupElement.super.post_init(self, ...)

	return 
end
SpecialObjectiveGroupElement.draw_links = function (self, t, dt, selected_unit, all_units)
	SpecialObjectiveUnitElement.super.draw_links(self, t, dt, selected_unit)
	self._draw_follow_up(self, selected_unit, all_units)

	return 
end
SpecialObjectiveGroupElement.update_selected = function (self, t, dt, selected_unit, all_units)
	self._draw_follow_up(self, selected_unit, all_units)

	if self._hed.spawn_instigator_ids then
		for _, id in ipairs(self._hed.spawn_instigator_ids) do
			local unit = all_units[id]
			local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

			if draw then
				self._draw_link(self, {
					g = 0,
					b = 0.75,
					r = 0,
					from_unit = unit,
					to_unit = self._unit
				})
			end
		end
	end

	return 
end
SpecialObjectiveGroupElement.update_unselected = function (self, t, dt, selected_unit, all_units)
	if self._hed.followup_elements then
		local followup_elements = self._hed.followup_elements
		local i = #followup_elements

		while 0 < i do
			local element_id = followup_elements[i]

			if not alive(all_units[element_id]) then
				table.remove(followup_elements, i)
			end

			i = i - 1
		end
	end

	if self._hed.spawn_instigator_ids then
		local spawn_instigator_ids = self._hed.spawn_instigator_ids
		local i = #spawn_instigator_ids

		while 0 < i do
			local id = spawn_instigator_ids[i]

			if not alive(all_units[id]) then
				table.remove(self._hed.spawn_instigator_ids, i)
			end

			i = i - 1
		end
	end

	return 
end
SpecialObjectiveGroupElement._draw_follow_up = function (self, selected_unit, all_units)
	SpecialObjectiveUnitElement._draw_follow_up(self, selected_unit, all_units)

	return 
end
SpecialObjectiveGroupElement.update_editing = function (self)
	self._so_raycast(self)
	self._spawn_raycast(self)
	self._raycast(self)

	return 
end
SpecialObjectiveGroupElement._so_raycast = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and (string.find(ray.unit:name():s(), "point_special_objective", 1, true) or string.find(ray.unit:name():s(), "ai_so_group", 1, true)) then
		local id = ray.unit:unit_data().unit_id

		Application:draw(ray.unit, 0, 1, 0)

		return id
	end

	return nil
end
SpecialObjectiveGroupElement._spawn_raycast = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if not ray or not ray.unit then
		return 
	end

	local id = nil

	if string.find(ray.unit:name():s(), "ai_enemy_group", 1, true) or string.find(ray.unit:name():s(), "ai_spawn_enemy", 1, true) or string.find(ray.unit:name():s(), "ai_civilian_group", 1, true) or string.find(ray.unit:name():s(), "ai_spawn_civilian", 1, true) then
		id = ray.unit:unit_data().unit_id

		Application:draw(ray.unit, 0, 0, 1)
	end

	return id
end
SpecialObjectiveGroupElement._raycast = function (self)
	local from = managers.editor:get_cursor_look_point(0)
	local to = managers.editor:get_cursor_look_point(100000)
	local ray = World:raycast(from, to, nil, managers.slot:get_mask("all"))

	if ray and ray.position then
		Application:draw_sphere(ray.position, 10, 1, 1, 1)

		return ray.position
	end

	return nil
end
SpecialObjectiveGroupElement._lmb = function (self)
	local id = self._so_raycast(self)

	if id then
		if self._hed.followup_elements then
			for i, element_id in ipairs(self._hed.followup_elements) do
				if element_id == id then
					table.remove(self._hed.followup_elements, i)

					return 
				end
			end
		end

		self._hed.followup_elements = self._hed.followup_elements or {}

		table.insert(self._hed.followup_elements, id)

		return 
	end

	local id = self._spawn_raycast(self)

	if id then
		if self._hed.spawn_instigator_ids then
			for i, si_id in ipairs(self._hed.spawn_instigator_ids) do
				if si_id == id then
					table.remove(self._hed.spawn_instigator_ids, i)

					return 
				end
			end
		end

		self._hed.spawn_instigator_ids = self._hed.spawn_instigator_ids or {}

		table.insert(self._hed.spawn_instigator_ids, id)

		return 
	end

	return 
end
SpecialObjectiveGroupElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "_lmb"))

	return 
end
SpecialObjectiveGroupElement.selected = function (self)
	SpecialObjectiveUnitElement.super.selected(self)

	return 
end
SpecialObjectiveGroupElement.add_unit_list_btn = function (self)
	local script = self._unit:mission_element_data().script

	local function f(unit)
		if not unit.mission_element_data(unit) or unit.mission_element_data(unit).script ~= script then
			return 
		end

		local id = unit.unit_data(unit).unit_id

		if self._hed.spawn_instigator_ids and table.contains(self._hed.spawn_instigator_ids, id) then
			return false
		end

		if self._hed.followup_elements and table.contains(self._hed.followup_elements, id) then
			return false
		end

		if string.find(unit.name(unit):s(), "ai_enemy_group", 1, true) or string.find(unit.name(unit):s(), "ai_spawn_enemy", 1, true) or string.find(unit.name(unit):s(), "ai_civilian_group", 1, true) or string.find(unit.name(unit):s(), "ai_spawn_civilian", 1, true) or string.find(unit.name(unit):s(), "point_special_objective", 1, true) or string.find(unit.name(unit):s(), "ai_so_group", 1, true) then
			return true
		end

		return false
	end

	local dialog = SelectUnitByNameModal:new("Add Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		if string.find(unit.name(unit):s(), "ai_enemy_group", 1, true) or string.find(unit.name(unit):s(), "ai_spawn_enemy", 1, true) or string.find(unit.name(unit):s(), "ai_civilian_group", 1, true) or string.find(unit.name(unit):s(), "ai_spawn_civilian", 1, true) then
			self._hed.spawn_instigator_ids = self._hed.spawn_instigator_ids or {}

			table.insert(self._hed.spawn_instigator_ids, id)
		elseif string.find(unit.name(unit):s(), "point_special_objective", 1, true) or string.find(unit.name(unit):s(), "ai_so_group", 1, true) then
			self._hed.followup_elements = self._hed.followup_elements or {}

			table.insert(self._hed.followup_elements, id)
		end
	end

	return 
end
SpecialObjectiveGroupElement.remove_unit_list_btn = function (self)
	local function f(unit)
		return (self._hed.spawn_instigator_ids and table.contains(self._hed.spawn_instigator_ids, unit.unit_data(unit).unit_id)) or (self._hed.followup_elements and table.contains(self._hed.followup_elements, unit.unit_data(unit).unit_id))
	end

	local dialog = SelectUnitByNameModal:new("Remove Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		if self._hed.spawn_instigator_ids then
			table.delete(self._hed.spawn_instigator_ids, id)
		end

		if self._hed.followup_elements then
			table.delete(self._hed.followup_elements, id)
		end
	end

	return 
end
SpecialObjectiveGroupElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local mode_params = {
		name = "Mode:",
		name_proportions = 1,
		tooltip = "Randomizer: assigns SOs to instigators. Forced Spawn: Will spawn a new group of choice. Recurring: Spawns new group. After failure, a new group will be spawned with a delay.",
		sorted = false,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = panel_sizer,
		options = {
			"randomizer",
			"forced_spawn",
			"recurring_cloaker_spawn",
			"recurring_spawn_1"
		},
		value = self._hed.mode
	}
	local mode = CoreEws.combobox(mode_params)

	mode.connect(mode, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		value = "mode",
		ctrlr = mode
	})

	local use_instigator = EWS:CheckBox(panel, "Use instigator", "")

	use_instigator.set_value(use_instigator, self._hed.use_instigator)
	use_instigator.connect(use_instigator, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "use_instigator",
		ctrlr = use_instigator
	})
	panel_sizer.add(panel_sizer, use_instigator, 0, 0, "EXPAND")

	local base_chance_params = {
		name_proportions = 1,
		name = "Base chance:",
		ctrlr_proportions = 2,
		tooltip = "Used to specify chance to happen (1==absolutely!)",
		min = 0,
		floats = 2,
		max = 1,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.base_chance
	}
	local base_chance = CoreEws.number_controller(base_chance_params)

	base_chance.connect(base_chance, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "base_chance",
		ctrlr = base_chance
	})
	base_chance.connect(base_chance, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "base_chance",
		ctrlr = base_chance
	})

	self._btn_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._btn_toolbar:add_tool("ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	self._btn_toolbar:connect("ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	self._btn_toolbar:add_tool("REMOVE_UNIT_LIST", "Remove unit from unit list", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	self._btn_toolbar:connect("REMOVE_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "remove_unit_list_btn"), nil)
	self._btn_toolbar:realize()
	panel_sizer.add(panel_sizer, self._btn_toolbar, 0, 1, "EXPAND,LEFT")

	return 
end
SpecialObjectiveGroupElement.add_to_mission_package = function (self)
	return 
end

return 
