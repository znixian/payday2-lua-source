ExperienceUnitElement = ExperienceUnitElement or class(MissionElement)
ExperienceUnitElement.SAVE_UNIT_POSITION = false
ExperienceUnitElement.SAVE_UNIT_ROTATION = false
ExperienceUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.elements = {}
	self._hed.amount = 0

	table.insert(self._save_values, "amount")

	return 
end
ExperienceUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_number(self, panel, panel_sizer, "amount", {
		floats = 0,
		min = 0
	}, "Specify the amount of experience given.")

	return 
end

return 
