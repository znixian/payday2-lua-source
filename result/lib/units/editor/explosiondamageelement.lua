ExplosionDamageUnitElement = ExplosionDamageUnitElement or class(MissionElement)
ExplosionDamageUnitElement.init = function (self, unit)
	ExplosionDamageUnitElement.super.init(self, unit)

	self._hed.range = 100
	self._hed.damage = 40

	table.insert(self._save_values, "range")
	table.insert(self._save_values, "damage")

	return 
end
ExplosionDamageUnitElement.update_selected = function (self)
	local brush = Draw:brush()

	brush.set_color(brush, Color(0.15, 1, 1, 1))

	local pen = Draw:pen(Color(0.15, 0.5, 0.5, 0.5))

	brush.sphere(brush, self._unit:position(), self._hed.range, 4)
	pen.sphere(pen, self._unit:position(), self._hed.range)

	return 
end
ExplosionDamageUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_number(self, panel, panel_sizer, "range", {
		floats = 0,
		min = 0
	}, "The range the explosion should reach")
	self._build_value_number(self, panel, panel_sizer, "damage", {
		min = 0,
		floats = 0,
		max = 100
	}, "The damage from the explosion")

	return 
end

return 
