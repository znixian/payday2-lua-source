SpawnPlayerHubElement = SpawnPlayerHubElement or class(HubElement)
SpawnPlayerHubElement.init = function (self, unit)
	HubElement.init(self, unit)
	table.insert(self._save_values, "unit:position")
	table.insert(self._save_values, "unit:rotation")

	return 
end

return 
