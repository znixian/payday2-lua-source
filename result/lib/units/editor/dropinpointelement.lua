DropInPointUnitElement = DropInPointUnitElement or class(MissionElement)
DropInPointUnitElement.init = function (self, unit)
	DropInPointUnitElement.super.init(self, unit)

	return 
end
DropInPointUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)
	self._add_help_text(self, "Will act as a drop-in point when enabled. Drop-in points affect users that drop-in, respawns and team AI spawns.")

	return 
end

return 
