MissionLoadDelayedElement = MissionLoadDelayedElement or class(MissionElement)
MissionLoadDelayedElement.SAVE_UNIT_POSITION = false
MissionLoadDelayedElement.SAVE_UNIT_ROTATION = false
MissionLoadDelayedElement.init = function (self, unit)
	MissionLoadDelayedElement.super.init(self, unit)

	self._units = {}
	self._hed.unit_ids = {}

	table.insert(self._save_values, "unit_ids")

	return 
end
MissionLoadDelayedElement.layer_finished = function (self)
	MissionElement.layer_finished(self)

	for _, id in pairs(self._hed.unit_ids) do
		local unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "save_unit_data"))

		self.save_unit_data(self, unit)
	end

	return 
end
MissionLoadDelayedElement.save_unit_data = function (self, unit)
	if unit then
		self._units[unit.unit_data(unit).unit_id] = unit
	end

	return 
end
MissionLoadDelayedElement.update_selected = function (self)
	for _, id in pairs(self._hed.unit_ids) do
		if not alive(self._units[id]) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		end
	end

	for id, unit in pairs(self._units) do
		if not alive(unit) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		else
			local params = {
				g = 0,
				b = 1,
				r = 1,
				from_unit = self._unit,
				to_unit = unit
			}

			self._draw_link(self, params)
			Application:draw(unit, 1, 0, 1)
		end
	end

	return 
end
MissionLoadDelayedElement.update_unselected = function (self, t, dt, selected_unit, all_units)
	for _, id in pairs(self._hed.unit_ids) do
		if not alive(self._units[id]) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		end
	end

	for id, unit in pairs(self._units) do
		if not alive(unit) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		end
	end

	return 
end
MissionLoadDelayedElement.draw_links_unselected = function (self, ...)
	MissionLoadDelayedElement.super.draw_links_unselected(self, ...)

	for id, unit in pairs(self._units) do
		local params = {
			g = 0,
			b = 0.5,
			r = 0.5,
			from_unit = self._unit,
			to_unit = unit
		}

		self._draw_link(self, params)
		Application:draw(unit, 0.5, 0, 0.5)
	end

	return 
end
MissionLoadDelayedElement.update_editing = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		sample = true,
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit then
		Application:draw(ray.unit, 1, 0, 1)
	end

	return 
end
MissionLoadDelayedElement.select_unit = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		sample = true,
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit then
		local unit = ray.unit

		if self._units[unit.unit_data(unit).unit_id] then
			self._remove_unit(self, unit)
		else
			self._add_unit(self, unit)
		end
	end

	return 
end
MissionLoadDelayedElement._remove_unit = function (self, unit)
	self._units[unit.unit_data(unit).unit_id] = nil

	table.delete(self._hed.unit_ids, unit.unit_data(unit).unit_id)

	return 
end
MissionLoadDelayedElement._add_unit = function (self, unit)
	self.save_unit_data(self, unit)
	table.insert(self._hed.unit_ids, unit.unit_data(unit).unit_id)

	return 
end
MissionLoadDelayedElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "select_unit"))

	return 
end
MissionLoadDelayedElement.add_unit_list_btn = function (self)
	local function filter_p(unit)
		if self._units[unit.unit_data(unit).unit_id] then
			return false
		end

		if unit.unit_data(unit).delayed_load then
			return managers.editor:layer("Statics"):category_map()[unit.type(unit):s()]
		end

		return false
	end

	local dialog = SelectUnitByNameModal:new("Add Unit", filter_p)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		if not self._units[unit.unit_data(unit).unit_id] then
			self._add_unit(self, unit)
		end
	end

	return 
end
MissionLoadDelayedElement.remove_unit_list_btn = function (self)
	local function filter_p(unit)
		return self._units[unit.unit_data(unit).unit_id]
	end

	local dialog = SelectUnitByNameModal:new("Remove Unit", filter_p)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		if self._units[unit.unit_data(unit).unit_id] then
			self._remove_unit(self, unit)
		end
	end

	return 
end
MissionLoadDelayedElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	self._btn_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._btn_toolbar:add_tool("ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	self._btn_toolbar:connect("ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	self._btn_toolbar:add_tool("REMOVE_UNIT_LIST", "Remove unit from unit list", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	self._btn_toolbar:connect("REMOVE_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "remove_unit_list_btn"), nil)
	self._btn_toolbar:realize()
	panel_sizer.add(panel_sizer, self._btn_toolbar, 0, 1, "EXPAND,LEFT")

	return 
end

return 
