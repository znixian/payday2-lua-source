KillzoneUnitElement = KillzoneUnitElement or class(MissionElement)
KillzoneUnitElement.init = function (self, unit)
	KillzoneUnitElement.super.init(self, unit)

	self._hed.type = "sniper"

	table.insert(self._save_values, "type")

	return 
end
KillzoneUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "type", {
		"sniper",
		"gas",
		"fire"
	})

	return 
end

return 
