EnemyDummyTriggerUnitElement = EnemyDummyTriggerUnitElement or class(MissionElement)
EnemyDummyTriggerUnitElement.LINK_ELEMENTS = {
	"elements"
}
EnemyDummyTriggerUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.event = "death"
	self._hed.elements = {}

	table.insert(self._save_values, "event")
	table.insert(self._save_values, "elements")

	return 
end
EnemyDummyTriggerUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	MissionElement.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.75,
				b = 0,
				r = 0,
				from_unit = unit,
				to_unit = self._unit
			})
		end
	end

	return 
end
EnemyDummyTriggerUnitElement.get_links_to_unit = function (self, ...)
	EnemyDummyTriggerUnitElement.super.get_links_to_unit(self, ...)
	self._get_links_of_type_from_elements(self, self._hed.elements, "trigger", ...)

	return 
end
EnemyDummyTriggerUnitElement.update_editing = function (self)
	return 
end
EnemyDummyTriggerUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and self._correct_unit(self, ray.unit:name():s()) then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
EnemyDummyTriggerUnitElement._correct_unit = function (self, u_name)
	local names = {
		"ai_spawn_enemy",
		"ai_enemy_group",
		"ai_spawn_civilian",
		"ai_civilian_group"
	}

	for _, name in ipairs(names) do
		if string.find(u_name, name, 1, true) then
			return true
		end
	end

	return false
end
EnemyDummyTriggerUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
EnemyDummyTriggerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"ai_spawn_enemy",
		"ai_enemy_group",
		"ai_spawn_civilian",
		"ai_civilian_group"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)

	local options = {
		"alerted",
		"death",
		"killshot",
		"fled",
		"spawn",
		"panic",
		"weapons_hot",
		"tied",
		"anim_act_01",
		"anim_act_02",
		"anim_act_03",
		"anim_act_04",
		"anim_act_05",
		"anim_act_06",
		"anim_act_07",
		"anim_act_08",
		"anim_act_09",
		"anim_act_10"
	}

	self._build_value_combobox(self, panel, panel_sizer, "event", options)

	return 
end

return 
