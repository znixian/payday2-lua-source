InventoryDummyUnitElement = InventoryDummyUnitElement or class(MissionElement)
InventoryDummyUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.category = "none"
	self._hed.slot = 1

	table.insert(self._save_values, "category")
	table.insert(self._save_values, "slot")

	return 
end
InventoryDummyUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "category", {
		"none",
		"secondaries",
		"primaries",
		"masks"
	}, "Select a crafted category.")
	self._build_value_number(self, panel, panel_sizer, "slot", {
		min = 1,
		floats = 0,
		max = 9
	}, "Set inventory slot to spawn")

	return 
end

return 
