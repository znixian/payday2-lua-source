SpotterUnitElement = SpotterUnitElement or class(MissionElement)
SpotterUnitElement.USES_POINT_ORIENTATION = true
SpotterUnitElement.ON_EXECUTED_ALTERNATIVES = {
	"on_outlined",
	"on_spotted"
}
SpotterUnitElement.init = function (self, unit)
	SpotterUnitElement.super.init(self, unit)

	return 
end
SpotterUnitElement.update_selected = function (self, time, rel_time)
	local brush = Draw:brush(Color.white:with_alpha(((math.sin(time*100) + 1)/2 - 1)*0.15))
	local len = (math.sin(time*100) + 1)/2*3000

	brush.cone(brush, self._unit:position(), self._unit:position() + self._unit:rotation():y()*len, len)
	brush.set_color(brush, Color.white:with_alpha(0.15))
	brush.cone(brush, self._unit:position(), self._unit:position() + self._unit:rotation():y()*3000, 3000)
	Application:draw_cone(self._unit:position(), self._unit:position() + self._unit:rotation():y()*3000, 3000, 0.75, 0.75, 0.75, 0.1)

	return 
end
SpotterUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	return 
end

return 
