LootSecuredTriggerUnitElement = LootSecuredTriggerUnitElement or class(MissionElement)
LootSecuredTriggerUnitElement.SAVE_UNIT_POSITION = false
LootSecuredTriggerUnitElement.SAVE_UNIT_ROTATION = false
LootSecuredTriggerUnitElement.init = function (self, unit)
	LootSecuredTriggerUnitElement.super.init(self, unit)

	self._hed.trigger_times = 1
	self._hed.amount = 0
	self._hed.include_instant_cash = false
	self._hed.report_only = false

	table.insert(self._save_values, "amount")
	table.insert(self._save_values, "include_instant_cash")
	table.insert(self._save_values, "report_only")

	return 
end
LootSecuredTriggerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_number(self, panel, panel_sizer, "amount", {
		floats = 0,
		min = 0
	}, "Minimum amount of loot required to trigger")
	self._build_value_checkbox(self, panel, panel_sizer, "include_instant_cash")
	self._build_value_checkbox(self, panel, panel_sizer, "report_only")

	return 
end

return 
