ArcadeStateUnitElement = ArcadeStateUnitElement or class(MissionElement)
ArcadeStateUnitElement.init = function (self, unit)
	ArcadeStateUnitElement.super.init(self, unit)
	table.insert(self._save_values, "state")

	return 
end
ArcadeStateUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "state", JobManager.arcade_states, "Select a state from the combobox")

	return 
end

return 
