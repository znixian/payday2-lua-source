UnitDamageTriggerUnitElement = UnitDamageTriggerUnitElement or class(MissionElement)
UnitDamageTriggerUnitElement.init = function (self, unit)
	UnitDamageTriggerUnitElement.super.init(self, unit)

	self._units = {}
	self._hed.unit_ids = {}
	self._hed.damage_types = ""

	table.insert(self._save_values, "unit_ids")
	table.insert(self._save_values, "damage_types")

	return 
end
UnitDamageTriggerUnitElement.layer_finished = function (self)
	MissionElement.layer_finished(self)

	for _, id in pairs(self._hed.unit_ids) do
		local unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "load_unit"))

		if unit then
			self._units[unit.unit_data(unit).unit_id] = unit
		end
	end

	return 
end
UnitDamageTriggerUnitElement.load_unit = function (self, unit)
	if unit then
		self._units[unit.unit_data(unit).unit_id] = unit
	end

	return 
end
UnitDamageTriggerUnitElement.update_selected = function (self)
	for _, id in pairs(self._hed.unit_ids) do
		if not alive(self._units[id]) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		end
	end

	for id, unit in pairs(self._units) do
		if not alive(unit) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		else
			local params = {
				g = 0,
				b = 0,
				r = 1,
				from_unit = unit,
				to_unit = self._unit
			}

			self._draw_link(self, params)
			Application:draw(unit, 1, 0, 0)
		end
	end

	return 
end
UnitDamageTriggerUnitElement.update_unselected = function (self, t, dt, selected_unit, all_units)
	for _, id in pairs(self._hed.unit_ids) do
		if not alive(self._units[id]) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		end
	end

	for id, unit in pairs(self._units) do
		if not alive(unit) then
			table.delete(self._hed.unit_ids, id)

			self._units[id] = nil
		end
	end

	return 
end
UnitDamageTriggerUnitElement.draw_links_unselected = function (self, ...)
	UnitDamageTriggerUnitElement.super.draw_links_unselected(self, ...)

	for id, unit in pairs(self._units) do
		local params = {
			g = 0,
			b = 0,
			r = 0.5,
			from_unit = unit,
			to_unit = self._unit
		}

		self._draw_link(self, params)
		Application:draw(unit, 0.5, 0, 0)
	end

	return 
end
UnitDamageTriggerUnitElement.update_editing = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		sample = true,
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit then
		Application:draw(ray.unit, 0, 1, 0)
	end

	return 
end
UnitDamageTriggerUnitElement.select_unit = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		sample = true,
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit then
		local unit = ray.unit

		if unit.damage and unit.damage(unit) then
			if self._units[unit.unit_data(unit).unit_id] then
				self._remove_unit(self, unit)
			else
				self._add_unit(self, unit)
			end
		else
			self.add_on_executed(self, unit)
		end
	end

	return 
end
UnitDamageTriggerUnitElement._remove_unit = function (self, unit)
	self._units[unit.unit_data(unit).unit_id] = nil

	table.delete(self._hed.unit_ids, unit.unit_data(unit).unit_id)

	return 
end
UnitDamageTriggerUnitElement._add_unit = function (self, unit)
	self._units[unit.unit_data(unit).unit_id] = unit

	table.insert(self._hed.unit_ids, unit.unit_data(unit).unit_id)

	return 
end
UnitDamageTriggerUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "select_unit"))

	return 
end
UnitDamageTriggerUnitElement.add_unit_list_btn = function (self)
	local function f(unit)
		if self._units[unit.unit_data(unit).unit_id] then
			return false
		end

		return unit.damage and unit.damage(unit)
	end

	local dialog = SelectUnitByNameModal:new("Add Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		if not self._units[unit.unit_data(unit).unit_id] then
			self._add_unit(self, unit)
		end
	end

	return 
end
UnitDamageTriggerUnitElement.remove_unit_list_btn = function (self)
	local function f(unit)
		return self._units[unit.unit_data(unit).unit_id]
	end

	local dialog = SelectUnitByNameModal:new("Remove Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		if self._units[unit.unit_data(unit).unit_id] then
			self._remove_unit(self, unit)
		end
	end

	return 
end
UnitDamageTriggerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	self._btn_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._btn_toolbar:add_tool("ADD_UNIT_LIST", "Add unit damage reporter from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	self._btn_toolbar:connect("ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	self._btn_toolbar:add_tool("REMOVE_UNIT_LIST", "Remove unit damage reporter from unit list", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	self._btn_toolbar:connect("REMOVE_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "remove_unit_list_btn"), nil)
	self._btn_toolbar:realize()
	panel_sizer.add(panel_sizer, self._btn_toolbar, 0, 1, "EXPAND,LEFT")

	local dmg_sizer = EWS:BoxSizer("HORIZONTAL")

	dmg_sizer.add(dmg_sizer, EWS:StaticText(panel, "Damage Types Filter:", 0, ""), 1, 0, "ALIGN_CENTER_VERTICAL")

	local dmg_types = EWS:TextCtrl(panel, self._hed.damage_types, "", "TE_PROCESS_ENTER")

	dmg_types.connect(dmg_types, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "damage_types",
		ctrlr = dmg_types
	})
	dmg_types.connect(dmg_types, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "damage_types",
		ctrlr = dmg_types
	})
	dmg_sizer.add(dmg_sizer, dmg_types, 2, 0, "ALIGN_CENTER_VERTICAL")
	panel_sizer.add(panel_sizer, dmg_sizer, 0, 0, "EXPAND")
	self.add_help_text(self, {
		text = "logic_counter_operator elements will use the reported <damage> as the amount to add/subtract/set.\nDamage types can be filtered by specifying specific damage types separated by spaces.",
		panel = panel,
		sizer = panel_sizer
	})

	return 
end

return 
