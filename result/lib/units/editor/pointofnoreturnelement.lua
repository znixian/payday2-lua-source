PointOfNoReturnElement = PointOfNoReturnElement or class(MissionElement)
PointOfNoReturnElement.LINK_ELEMENTS = {
	"elements"
}
PointOfNoReturnElement.init = function (self, unit)
	PointOfNoReturnElement.super.init(self, unit)

	self._hed.elements = {}
	self._hed.time_easy = 300
	self._hed.time_normal = 240
	self._hed.time_hard = 120
	self._hed.time_overkill = 60
	self._hed.time_overkill_145 = 30
	self._hed.time_easy_wish = nil
	self._hed.time_overkill_290 = 15
	self._hed.time_sm_wish = nil

	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "time_easy")
	table.insert(self._save_values, "time_normal")
	table.insert(self._save_values, "time_hard")
	table.insert(self._save_values, "time_overkill")
	table.insert(self._save_values, "time_overkill_145")
	table.insert(self._save_values, "time_easy_wish")
	table.insert(self._save_values, "time_overkill_290")
	table.insert(self._save_values, "time_sm_wish")

	return 
end
PointOfNoReturnElement.post_init = function (self, ...)
	PointOfNoReturnElement.super.post_init(self, ...)

	if self._hed.time_easy_wish == nil then
		self._hed.time_easy_wish = self._hed.time_overkill_290
	end

	if self._hed.time_sm_wish == nil then
		self._hed.time_sm_wish = self._hed.time_overkill_290
	end

	return 
end
PointOfNoReturnElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local exact_names = {
		"core/units/mission_elements/trigger_area/trigger_area"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, nil, exact_names)

	local time_params_easy = {
		name = "Time left on easy:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on Easy difficulty (Currently not used on Payday 2)",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_easy
	}
	local time_easy = CoreEWS.number_controller(time_params_easy)

	time_easy.connect(time_easy, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_easy",
		ctrlr = time_easy
	})
	time_easy.connect(time_easy, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_easy",
		ctrlr = time_easy
	})

	local time_params_normal = {
		name = "Time left on normal:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on Normal difficulty",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_normal
	}
	local time_normal = CoreEWS.number_controller(time_params_normal)

	time_normal.connect(time_normal, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_normal",
		ctrlr = time_normal
	})
	time_normal.connect(time_normal, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_normal",
		ctrlr = time_normal
	})

	local time_params_hard = {
		name = "Time left on hard:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on Hard difficulty",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_hard
	}
	local time_hard = CoreEWS.number_controller(time_params_hard)

	time_hard.connect(time_hard, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_hard",
		ctrlr = time_hard
	})
	time_hard.connect(time_hard, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_hard",
		ctrlr = time_hard
	})

	local time_params_overkill = {
		name = "Time left on overkill:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on Very Hard difficulty",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_overkill
	}
	local time_overkill = CoreEWS.number_controller(time_params_overkill)

	time_overkill.connect(time_overkill, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_overkill",
		ctrlr = time_overkill
	})
	time_overkill.connect(time_overkill, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_overkill",
		ctrlr = time_overkill
	})

	local time_params_overkill_145 = {
		name = "Time left on overkill 145:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on Overkill difficulty",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_overkill_145
	}
	local time_overkill_145 = CoreEWS.number_controller(time_params_overkill_145)

	time_overkill_145.connect(time_overkill_145, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_overkill_145",
		ctrlr = time_overkill_145
	})
	time_overkill_145.connect(time_overkill_145, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_overkill_145",
		ctrlr = time_overkill_145
	})

	local time_params_easy_wish = {
		name = "Time left on easy wish:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on Easy Wish difficulty",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_easy_wish
	}
	local time_easy_wish = CoreEWS.number_controller(time_params_easy_wish)

	time_easy_wish.connect(time_easy_wish, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_easy_wish",
		ctrlr = time_easy_wish
	})
	time_easy_wish.connect(time_easy_wish, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_easy_wish",
		ctrlr = time_easy_wish
	})

	local time_params_overkill_290 = {
		name = "Time left on overkill 290:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on Death Wish difficulty",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_overkill_290
	}
	local time_overkill_290 = CoreEWS.number_controller(time_params_overkill_290)

	time_overkill_290.connect(time_overkill_290, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_overkill_290",
		ctrlr = time_overkill_290
	})
	time_overkill_290.connect(time_overkill_290, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_overkill_290",
		ctrlr = time_overkill_290
	})

	local time_params_sm_wish = {
		name = "Time left on sm wish:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Set the time left on SM Wish difficulty",
		min = 1,
		floats = 0,
		panel = panel,
		sizer = panel_sizer,
		value = self._hed.time_sm_wish
	}
	local time_sm_wish = CoreEWS.number_controller(time_params_sm_wish)

	time_sm_wish.connect(time_sm_wish, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "time_sm_wish",
		ctrlr = time_sm_wish
	})
	time_sm_wish.connect(time_sm_wish, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "time_sm_wish",
		ctrlr = time_sm_wish
	})

	return 
end
PointOfNoReturnElement.draw_links = function (self, t, dt, selected_unit, all_units)
	MissionElement.draw_links(self, t, dt, selected_unit, all_units)

	return 
end
PointOfNoReturnElement.update_editing = function (self)
	return 
end
PointOfNoReturnElement.update_selected = function (self, t, dt, selected_unit, all_units)
	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0,
				b = 0.75,
				r = 0.75,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
PointOfNoReturnElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and ray.unit:name():s() == "core/units/mission_elements/trigger_area/trigger_area" then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
PointOfNoReturnElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end

return 
