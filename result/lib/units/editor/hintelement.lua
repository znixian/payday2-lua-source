HintUnitElement = HintUnitElement or class(MissionElement)
HintUnitElement.init = function (self, unit)
	HintUnitElement.super.init(self, unit)

	self._hed.hint_id = "none"
	self._hed.instigator_only = false

	table.insert(self._save_values, "hint_id")
	table.insert(self._save_values, "instigator_only")

	return 
end
HintUnitElement._set_text = function (self)
	local hint = managers.hint:hint(self._hed.hint_id)

	self._text:set_value((hint and managers.localization:text(hint.text_id)) or "none")

	return 
end
HintUnitElement.set_element_data = function (self, params, ...)
	HintUnitElement.super.set_element_data(self, params, ...)

	if params.value == "hint_id" then
		self._set_text(self)
	end

	return 
end
HintUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_checkbox(self, panel, panel_sizer, "instigator_only", "Only show the hint on the instigator unit")
	self._build_value_combobox(self, panel, panel_sizer, "hint_id", table.list_add({
		"none"
	}, managers.hint:ids()), "Select a text id from the combobox")

	local text_sizer = EWS:BoxSizer("HORIZONTAL")

	text_sizer.add(text_sizer, EWS:StaticText(panel, "Text: ", "", ""), 1, 2, "ALIGN_CENTER_VERTICAL,RIGHT,EXPAND")

	self._text = EWS:StaticText(panel, "", "", "")

	text_sizer.add(text_sizer, self._text, 2, 2, "RIGHT,TOP,EXPAND")
	self._set_text(self)
	panel_sizer.add(panel_sizer, text_sizer, 1, 0, "EXPAND")

	return 
end

return 
