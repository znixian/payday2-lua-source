LootBagUnitElement = LootBagUnitElement or class(MissionElement)
LootBagUnitElement.USES_POINT_ORIENTATION = true
LootBagUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._test_units = {}
	self._hed.spawn_dir = Vector3(0, 0, 1)
	self._hed.push_multiplier = 0
	self._hed.carry_id = "none"
	self._hed.from_respawn = false

	return 
end
LootBagUnitElement.save = function (self, list)
	if self._hed.push_multiplier ~= 0 then
		list.spawn_dir = self._hed.spawn_dir
		list.push_multiplier = self._hed.push_multiplier
	end

	list.carry_id = self._hed.carry_id
	list.from_respawn = self._hed.from_respawn

	return 
end
LootBagUnitElement.test_element = function (self)
	local unit_name = "units/payday2/pickups/gen_pku_lootbag/gen_pku_lootbag"
	local throw_distance_multiplier = 1

	if self._hed.carry_id ~= "none" then
		unit_name = tweak_data.carry[self._hed.carry_id].unit or unit_name
		local carry_type = tweak_data.carry[self._hed.carry_id].type
		throw_distance_multiplier = tweak_data.carry.types[carry_type].throw_distance_multiplier or throw_distance_multiplier
	end

	local unit = safe_spawn_unit(unit_name, self._unit:position(), self._unit:rotation())

	table.insert(self._test_units, unit)

	local push_value = (self._hed.push_multiplier and self._hed.spawn_dir*self._hed.push_multiplier) or 0

	unit.push(unit, 100, push_value*600*throw_distance_multiplier)

	return 
end
LootBagUnitElement.stop_test_element = function (self)
	for _, unit in ipairs(self._test_units) do
		if alive(unit) then
			World:delete_unit(unit)
		end
	end

	self._test_units = {}

	return 
end
LootBagUnitElement.update_selected = function (self, time, rel_time)
	Application:draw_arrow(self._unit:position(), self._unit:position() + self._hed.spawn_dir*50, 0.75, 0.75, 0.75, 0.1)

	return 
end
LootBagUnitElement.update_editing = function (self, time, rel_time)
	local kb = Input:keyboard()
	local speed = rel_time*60

	if kb.down(kb, Idstring("left")) then
		self._hed.spawn_dir = self._hed.spawn_dir:rotate_with(Rotation(speed, 0, 0))
	end

	if kb.down(kb, Idstring("right")) then
		self._hed.spawn_dir = self._hed.spawn_dir:rotate_with(Rotation(-speed, 0, 0))
	end

	if kb.down(kb, Idstring("up")) then
		self._hed.spawn_dir = self._hed.spawn_dir:rotate_with(Rotation(0, 0, speed))
	end

	if kb.down(kb, Idstring("down")) then
		self._hed.spawn_dir = self._hed.spawn_dir:rotate_with(Rotation(0, 0, -speed))
	end

	local from = self._unit:position()
	local to = from + self._hed.spawn_dir*100000
	local ray = managers.editor:unit_by_raycast({
		from = from,
		to = to,
		mask = managers.slot:get_mask("statics_layer")
	})

	if ray and ray.unit then
		Application:draw_sphere(ray.position, 25, 1, 0, 0)
	end

	return 
end
LootBagUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_number(self, panel, panel_sizer, "push_multiplier", {
		floats = 1,
		min = 0
	}, "Use this to add a velocity to a physic push on the spawned unit")
	self._build_value_combobox(self, panel, panel_sizer, "carry_id", table.list_add({
		"none"
	}, tweak_data.carry:get_carry_ids()), "Select a carry_id to be created.")
	self._build_value_checkbox(self, panel, panel_sizer, "from_respawn")

	return 
end
LootBagTriggerUnitElement = LootBagTriggerUnitElement or class(MissionElement)
LootBagTriggerUnitElement.SAVE_UNIT_POSITION = false
LootBagTriggerUnitElement.SAVE_UNIT_ROTATION = false
LootBagTriggerUnitElement.LINK_ELEMENTS = {
	"elements"
}
LootBagTriggerUnitElement.init = function (self, unit)
	LootBagTriggerUnitElement.super.init(self, unit)

	self._hed.elements = {}
	self._hed.trigger_type = "load"

	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "trigger_type")

	return 
end
LootBagTriggerUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	LootBagTriggerUnitElement.super.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.85,
				b = 0.25,
				r = 0.85,
				from_unit = unit,
				to_unit = self._unit
			})
		end
	end

	return 
end
LootBagTriggerUnitElement.update_editing = function (self)
	return 
end
LootBagTriggerUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and ray.unit:name() == Idstring("units/dev_tools/mission_elements/point_loot_bag/point_loot_bag") then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
LootBagTriggerUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
LootBagTriggerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"point_loot_bag/point_loot_bag"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)
	self._build_value_combobox(self, panel, panel_sizer, "trigger_type", {
		"load",
		"spawn"
	}, "Select a trigger type for the selected elements")
	self._add_help_text(self, "This element is a trigger to point_loot_bag element.")

	return 
end

return 
