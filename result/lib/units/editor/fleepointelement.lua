FleePointElement = FleePointElement or class(MissionElement)
FleePointElement.SAVE_UNIT_ROTATION = false
FleePointElement.init = function (self, unit)
	FleePointElement.super.init(self, unit)

	self._hed.functionality = "flee_point"

	table.insert(self._save_values, "functionality")

	return 
end
FleePointElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "functionality", {
		"flee_point",
		"loot_drop"
	}, "Select the functionality of the point")

	return 
end

return 
