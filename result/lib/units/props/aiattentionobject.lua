AIAttentionObject = AIAttentionObject or class()
AIAttentionObject.REACT_IDLE = 1
AIAttentionObject.REACT_CURIOUS = 2
AIAttentionObject.REACT_CHECK = 3
AIAttentionObject.REACT_SUSPICIOUS = 4
AIAttentionObject.REACT_SURPRISED = 5
AIAttentionObject.REACT_SCARED = 6
AIAttentionObject.REACT_AIM = 7
AIAttentionObject.REACT_ARREST = 8
AIAttentionObject.REACT_DISARM = 9
AIAttentionObject.REACT_SHOOT = 10
AIAttentionObject.REACT_MELEE = 11
AIAttentionObject.REACT_COMBAT = 12
AIAttentionObject.REACT_SPECIAL_ATTACK = 13
AIAttentionObject.REACT_MIN = AIAttentionObject.REACT_IDLE
AIAttentionObject.REACT_MAX = AIAttentionObject.REACT_SPECIAL_ATTACK
AIAttentionObject.init = function (self, unit, is_not_extension)
	self._unit = unit
	self._attention_data = nil
	self._listener_holder = ListenerHolder:new()

	self.setup_attention_positions(self, nil, nil)

	self._is_extension = not is_not_extension

	if self._is_extension then
		self.set_update_enabled(self, true)

		if Network:is_client() and unit.unit_data(unit).only_visible_in_editor then
			unit.set_visible(unit, false)
		end

		if self._initial_settings then
			local preset_list = string.split(self._initial_settings, " ")

			for _, preset_name in ipairs(preset_list) do
				local attention_desc = tweak_data.attention.settings[preset_name]
				local att_setting = PlayerMovement._create_attention_setting_from_descriptor(self, attention_desc, preset_name)

				self.add_attention(self, att_setting)
			end
		end
	end

	return 
end
AIAttentionObject.update = function (self, unit, t, dt)
	self._attention_obj:m_position(self._observer_info.m_pos)

	return 
end
AIAttentionObject.set_update_enabled = function (self, state)
	self._unit:set_extension_update_enabled(Idstring("attention"), state)

	return 
end
AIAttentionObject.set_detection_object_name = function (self, obj_name)
	self._attention_obj_name = obj_name

	self.setup_attention_positions(self)

	return 
end
AIAttentionObject.setup_attention_positions = function (self)
	if self._attention_obj_name then
		self._attention_obj = self._unit:get_object(Idstring(self._attention_obj_name))
	else
		self._attention_obj = self._unit:orientation_object()
	end

	self._observer_info = {
		m_pos = self._attention_obj:position()
	}

	return 
end
AIAttentionObject.attention_data = function (self)
	return self._attention_data
end
AIAttentionObject.unit = function (self)
	return self._unit
end
AIAttentionObject.add_attention = function (self, settings)
	local needs_register = nil

	if not self._attention_data then
		self._attention_data = {}
		needs_register = true
	end

	self._attention_data[settings.id] = settings

	if needs_register then
		self._register(self)
	end

	self._call_listeners(self)

	return 
end
AIAttentionObject.remove_attention = function (self, id)
	if not self._attention_data then
		return 
	end

	if self._attention_data[id] then
		self._attention_data[id] = nil

		if not next(self._attention_data) then
			managers.groupai:state():unregister_AI_attention_object(self._parent_unit or self._unit:key())

			self._attention_data = nil
		end

		self._call_listeners(self)
	end

	return 
end
AIAttentionObject.set_attention = function (self, settings, id)
	if self._attention_data then
		if settings then
			self._attention_data = {
				[id or settings.id] = settings
			}
		else
			self._attention_data = nil

			managers.groupai:state():unregister_AI_attention_object(self._parent_unit or self._unit:key())
		end

		self._call_listeners(self)
	elseif settings then
		self._attention_data = {}
		self._attention_data = {
			[id or settings.id] = settings
		}

		self._register(self)
		self._call_listeners(self)
	end

	return 
end
AIAttentionObject.override_attention = function (self, original_preset_name, override_preset)
	if override_preset then
		self._overrides = self._overrides or {}
		local call_listeners = (self._attention_data and self._attention_data[original_preset_name]) or self._overrides[original_preset_name]
		self._overrides[original_preset_name] = override_preset

		if call_listeners then
			self._call_listeners(self)
		end
	elseif self._overrides and self._overrides[original_preset_name] then
		self._overrides[original_preset_name] = nil

		if not next(self._overrides) then
			self._overrides = nil
		end

		self._call_listeners(self)
	end

	return 
end
AIAttentionObject.get_attention = function (self, filter, min, max, team)
	if not self._attention_data then
		return 
	end

	min = min or AIAttentionObject.REACT_MIN
	max = max or AIAttentionObject.REACT_MAX
	local nav_manager = managers.navigation
	local access_f = nav_manager.check_access
	local settings_match, relation = nil

	if team and self._team then
		if team.foes[self._team.id] then
			relation = "foe"
		else
			relation = "friend"
		end
	end

	for id, settings in pairs(self._attention_data) do
		if (not self._overrides or not self._overrides[id]) and min <= settings.reaction and settings.reaction <= max and (not settings_match or settings_match.reaction < settings.reaction) and (not relation or not settings.relation or relation == settings.relation) and access_f(nav_manager, settings.filter, filter, 0) then
			settings_match = settings
		end
	end

	if self._overrides then
		for id, settings in pairs(self._overrides) do
			if min <= settings.reaction and settings.reaction <= max and (not settings_match or settings_match.reaction < settings.reaction) and (not relation or not settings.relation or relation == settings.relation) and access_f(nav_manager, settings.filter, filter, 0) then
				settings_match = settings
			end
		end
	end

	return settings_match
end
AIAttentionObject.verify_attention = function (self, test_settings, min, max, team)
	if not self._attention_data then
		return 
	end

	local new_settings = self.get_attention(self, filter, min, max, team)

	return new_settings == test_settings
end
AIAttentionObject.get_attention_m_pos = function (self, settings)
	return self._observer_info.m_pos
end
AIAttentionObject.get_detection_m_pos = function (self)
	return self._observer_info.m_pos
end
AIAttentionObject.get_ground_m_pos = function (self)
	return self._observer_info.m_pos
end
AIAttentionObject.add_listener = function (self, key, clbk)
	self._listener_holder:add(key, clbk)

	return 
end
AIAttentionObject.remove_listener = function (self, key)
	self._listener_holder:remove(key)

	return 
end
AIAttentionObject._call_listeners = function (self)
	local u_key = self._parent_unit or self._unit:key()

	managers.groupai:state():on_AI_attention_changed(u_key)
	self._listener_holder:call(u_key)

	return 
end
AIAttentionObject._register = function (self)
	managers.groupai:state():register_AI_attention_object(self._parent_unit or self._unit, self, nil)

	return 
end
AIAttentionObject.link = function (self, parent_unit, obj_name, local_pos)
	self._unit:unlink()

	if parent_unit then
		self._parent_unit = parent_unit
		self._parent_obj_name = obj_name
		self._local_pos = local_pos
		self._parent_unit_key = parent_unit.key(parent_unit)

		self._unit:body(0):set_enabled(false)
		self._unit:set_moving()

		local parent_obj = parent_unit.get_object(parent_unit, Idstring(obj_name))
		local parent_pos = parent_obj.position(parent_obj)
		local parent_rot = parent_obj.rotation(parent_obj)
		local att_obj_w_pos = local_pos.rotate_with(local_pos, parent_rot) + parent_pos

		parent_unit.link(parent_unit, Idstring(obj_name), self._unit)
		self._unit:set_position(att_obj_w_pos)

		if Network:is_server() then
			if self._parent_unit:id() == -1 then
				debug_pause_unit(self._parent_unit, "[AIAttentionObject:set_parent_unit] attention object parent is not network synched", self._parent_unit)
			end

			managers.network:session():send_to_peers_synched("link_attention_no_rot", self._parent_unit, self._unit, obj_name, local_pos)
		end

		self.set_update_enabled(self, true)
	else
		self._parent_unit = nil
		self._parent_obj_name = nil
		self._local_pos = nil
		self._parent_unit_key = nil

		if Network:is_server() then
			managers.network:session():send_to_peers_synched("unlink_attention", self._unit)
		end

		self.set_update_enabled(self, false)
	end

	return 
end
AIAttentionObject.set_team = function (self, team)
	local call_listeners = self._team ~= team or (team and team.id ~= self._team.id)
	self._team = team

	if self._attention_data then
		for id, setting in pairs(self._attention_data) do
			setting.team = team
		end
	end

	if self._overrides then
		for id, setting in pairs(self._overrides) do
			setting.team = team
		end
	end

	self._call_listeners(self)

	return 
end
AIAttentionObject.save = function (self, data)
	if alive(self._parent_unit) then
		data.parent_u_id = self._parent_unit:unit_data().unit_id
		data.parent_obj_name = self._parent_obj_name
		data.local_pos = self._local_pos
	end

	return 
end
AIAttentionObject.load = function (self, data)
	if not data or not data.parent_u_id then
		return 
	end

	local parent_unit = nil

	if Application:editor() then
		parent_unit = managers.editor:unit_with_id(data.parent_u_id)
	else
		parent_unit = managers.worlddefinition:get_unit_on_load(data.parent_u_id, callback(self, self, "clbk_load_parent_unit"))
	end

	if parent_unit then
		self.link(self, parent_unit, data.parent_obj_name, data.local_pos)
	elseif not Application:editor() then
		self._load_data = data
	else
		debug_pause_unit(self._unit, "[AIAttentionObject:load] failed to link", self._unit)
	end

	return 
end
AIAttentionObject.clbk_load_parent_unit = function (self, parent_unit)
	if parent_unit then
		self.link(self, parent_unit, self._load_data.parent_obj_name, self._load_data.local_pos)
	end

	self._load_data = nil

	return 
end
AIAttentionObject.destroy = function (self)
	self.set_attention(self, nil)

	local extensions = self._unit:extensions()
	local last_extension_name = extensions[#extensions]
	local last_extension = self._unit[last_extension_name]

	if self == last_extension then
		self._unit:base():pre_destroy(self._unit)
	end

	return 
end

return 
