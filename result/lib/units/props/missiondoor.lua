MissionDoor = MissionDoor or class(UnitBase)
MissionDoor.init = function (self, unit)
	MissionDoor.super.init(self, unit, false)

	self._unit = unit
	self._devices = {}
	self._powered = true

	return 
end
MissionDoor.update = function (self, unit, t, dt)
	if self._explode_t and self._explode_t < t then
		self._c4_sequence_done(self)
	end

	return 
end
MissionDoor.activate = function (self)
	if Network:is_client() then
		return 
	end

	if self._active then
		Application:error("[MissionDoor:activate()] allready active", self._unit)

		return 
	end

	self._active = true

	CoreDebug.cat_debug("gaspode", "MissionDoor:activate", self.tweak_data)

	local devices_data = tweak_data.mission_door[self.tweak_data].devices

	for type, device_data in pairs(devices_data) do
		local amount = #device_data
		self._devices[type] = {
			placed = false,
			placed_counter = 0,
			completed_counter = 0,
			completed = false,
			units = {},
			amount = amount
		}

		for _, unit_data in ipairs(device_data) do
			local a_obj = self._unit:get_object(Idstring(unit_data.align))
			local position = a_obj.position(a_obj)
			local rotation = a_obj.rotation(a_obj)
			local unit = World:spawn_unit(unit_data.unit, position, rotation)

			unit.mission_door_device(unit):set_parent_data(self._unit, type)

			if unit_data.can_jam ~= nil then
				unit.timer_gui(unit):set_can_jam(unit_data.can_jam)
			end

			if unit_data.timer then
				unit.timer_gui(unit):set_override_timer(unit_data.timer)
			end

			MissionDoor.run_mission_door_device_sequence(unit, "activate")

			if managers.network:session() then
				managers.network:session():send_to_peers_synched("run_mission_door_device_sequence", unit, "activate")
			end

			table.insert(self._devices[type].units, {
				placed = false,
				completed = false,
				unit = unit
			})
		end
	end

	return 
end
MissionDoor.run_mission_door_device_sequence = function (unit, sequence_name)
	if unit.damage(unit):has_sequence(sequence_name) then
		unit.damage(unit):run_sequence_simple(sequence_name)
	end

	return 
end
MissionDoor.deactivate = function (self)
	CoreDebug.cat_debug("gaspode", "MissionDoor:deactivate")

	self._active = nil

	self._destroy_devices(self)

	return 
end
MissionDoor.set_mission_door_device_powered = function (unit, powered, enabled_interaction)
	unit.timer_gui(unit):set_powered(powered, enabled_interaction)

	return 
end
MissionDoor.set_powered = function (self, powered)
	self._powered = powered
	local drills = self._devices.drill

	if drills then
		for _, unit_data in ipairs(drills.units) do
			if unit_data.placed and alive(unit_data.unit) then
				unit_data.unit:timer_gui():set_powered(powered)

				if managers.network:session() then
					managers.network:session():send_to_peers_synched("set_mission_door_device_powered", unit_data.unit, powered, false)
				end
			end
		end
	end

	return 
end
MissionDoor.set_on = function (self, state)
	local drills = self._devices.drill

	if drills then
		for _, unit_data in ipairs(drills.units) do
			if unit_data.placed and alive(unit_data.unit) then
				unit_data.unit:timer_gui():set_powered(state, true)

				if managers.network:session() then
					managers.network:session():send_to_peers_synched("set_mission_door_device_powered", unit_data.unit, state, true)
				end
			end
		end
	end

	return 
end
MissionDoor._get_device_unit_data = function (self, unit, type)
	if not self._devices or not self._devices[type] then
		debug_pause("[MissionDoor:_get_device_unit_data] Mission Door do not have any devices of this type!", "type", type, "Mission Door", unit, "Devices", inspect(self._devices))

		return nil
	end

	for _, unit_data in ipairs(self._devices[type].units) do
		if unit_data.unit == unit then
			return unit_data
		end
	end

	return 
end
MissionDoor.device_placed = function (self, unit, type)
	local device_unit_data = self._get_device_unit_data(self, unit, type)

	if not device_unit_data or device_unit_data.placed then
		CoreDebug.cat_debug("gaspode", "MissionDoor:device_placed", "Allready placed")

		return 
	end

	self._devices[type].placed_counter = self._devices[type].placed_counter + 1
	device_unit_data.placed = true

	self.trigger_sequence(self, type .. "_placed")
	self._check_placed_counter(self, type)

	return 
end
MissionDoor.device_completed = function (self, type)
	self._devices[type].completed = true
	self._devices[type].completed_counter = self._devices[type].completed_counter + 1

	self.trigger_sequence(self, type .. "_completed")
	self._check_completed_counter(self, type)

	return 
end
MissionDoor.device_jammed = function (self, type)
	self.trigger_sequence(self, type .. "_jammed")

	return 
end
MissionDoor.device_resumed = function (self, type)
	self.trigger_sequence(self, type .. "_resumed")

	return 
end
MissionDoor._check_placed_counter = function (self, type)
	if self._devices[type].placed_counter ~= self._devices[type].amount then
		CoreDebug.cat_debug("gaspode", "MissionDoor:_check_placed_counter", "All", type, "are not placed yet")

		return 
	end

	CoreDebug.cat_debug("gaspode", "MissionDoor:_check_placed_counter", "All of type", type, "has been placed")
	self.trigger_sequence(self, "all_" .. type .. "_placed")

	if type == "c4" and self._devices[type].placed_counter == self._devices[type].amount then
		self._initiate_c4_sequence(self)

		return 
	end

	if (type == "key" or type == "ecm") and self._devices[type].placed_counter == self._devices[type].amount then
		for _, unit_data in ipairs(self._devices[type].units) do
			self.device_completed(self, type)
		end

		return 
	end

	return 
end
MissionDoor._check_completed_counter = function (self, type)
	if self._devices[type].completed_counter == self._devices[type].amount then
		self._destroy_devices(self)
		self.trigger_sequence(self, "door_opened")

		local sequence_name = "open_door"

		if type ~= "drill" or false then
			if type == "c4" then
				sequence_name = "explode_door"

				if Network:is_server() then
					if self._unit:base() then
						self._unit:base().c4 = true
					end

					local alert_event = {
						"aggression",
						self._unit:position(),
						tweak_data.weapon.trip_mines.alert_radius,
						managers.groupai:state():get_unit_type_filter("civilians_enemies"),
						self._unit
					}

					managers.groupai:state():propagate_alert(alert_event)
				end
			elseif type == "key" then
				sequence_name = "open_door_keycard"
			elseif type == "ecm" then
				sequence_name = "open_door_ecm"
			end
		end

		if managers.network:session() then
			managers.network:session():send_to_peers_synched("run_mission_door_sequence", self._unit, sequence_name)
		end

		self.run_sequence_simple(self, sequence_name)
	end

	return 
end
MissionDoor._initiate_c4_sequence = function (self)
	for type, device in pairs(self._devices) do
		if type ~= "c4" then
			for _, unit_data in ipairs(device.units) do
				if alive(unit_data.unit) then
					unit_data.unit:set_slot(0)
				end
			end
		end
	end

	for _, unit_data in ipairs(self._devices.c4.units) do
		MissionDoor.run_mission_door_device_sequence(unit_data.unit, "activate_explode_sequence")

		if managers.network:session() then
			managers.network:session():send_to_peers_synched("run_mission_door_device_sequence", unit_data.unit, "activate_explode_sequence")
		end
	end

	self._explode_t = Application:time() + 5

	self._unit:set_extension_update_enabled(Idstring("base"), true)

	return 
end
MissionDoor._c4_sequence_done = function (self)
	self._explode_t = nil

	self._unit:set_extension_update_enabled(Idstring("base"), false)

	if not self._devices.c4 then
		return 
	end

	for _, unit_data in ipairs(self._devices.c4.units) do
		self.device_completed(self, "c4")
	end

	return 
end
MissionDoor.run_sequence_simple = function (self, sequence_name)
	self._run_sequence_simple(self, sequence_name)

	return 
end
MissionDoor.trigger_sequence = function (self, trigger_sequence_name)
	CoreDebug.cat_debug("gaspode", "MissionDoor:trigger_sequence", trigger_sequence_name)
	self._run_sequence_simple(self, trigger_sequence_name)

	return 
end
MissionDoor._run_sequence_simple = function (self, sequence_name)
	self._unit:damage():run_sequence_simple(sequence_name)

	return 
end
MissionDoor._destroy_devices = function (self)
	for _, device in pairs(self._devices) do
		for _, unit_data in ipairs(device.units) do
			if alive(unit_data.unit) then
				unit_data.unit:set_slot(0)
			end
		end
	end

	self._devices = {}

	return 
end
MissionDoor.destroy = function (self)
	for _, device in pairs(self._devices) do
		for _, unit_data in ipairs(device.units) do
			if alive(unit_data.unit) then
				unit_data.unit:set_slot(0)
			end
		end
	end

	return 
end
MissionDoorDevice = MissionDoorDevice or class()
MissionDoorDevice.init = function (self, unit)
	self._unit = unit

	return 
end
MissionDoorDevice.set_parent_data = function (self, door_unit, device_type)
	self._parent_door = door_unit
	self._device_type = device_type

	return 
end
MissionDoorDevice.placed = function (self)
	if not alive(self._parent_door) then
		CoreDebug.cat_debug("gaspode", "MissionDoor:placed", "Had no parent door unit")

		return 
	end

	self._placed = true

	self._parent_door:base():device_placed(self._unit, self._device_type)

	return 
end
MissionDoorDevice.can_place = function (self)
	return not self._placed
end
MissionDoorDevice.report_jammed_state = function (self, jammed)
	if not alive(self._parent_door) then
		CoreDebug.cat_debug("gaspode", "MissionDoor:report_jammed_state", "Had no parent door unit")

		return 
	end

	if jammed then
		self._parent_door:base():device_jammed(self._device_type)
	else
		self._parent_door:base():device_resumed(self._device_type)
	end

	return 
end
MissionDoorDevice.report_resumed = function (self)
	if not alive(self._parent_door) then
		CoreDebug.cat_debug("gaspode", "MissionDoor:report_jammed_state", "Had no parent door unit")

		return 
	end

	self._parent_door:base():device_resumed(self._device_type)

	return 
end
MissionDoorDevice.report_completed = function (self)
	if not alive(self._parent_door) then
		CoreDebug.cat_debug("gaspode", "MissionDoor:report_completed", "Had no parent door unit")

		return 
	end

	self._parent_door:base():device_completed(self._device_type)

	return 
end
MissionDoorDevice.report_trigger_sequence = function (self, trigger_sequence_name)
	CoreDebug.cat_debug("gaspode", "MissionDoor:report_trigger_sequence", trigger_sequence_name)

	if not alive(self._parent_door) then
		CoreDebug.cat_debug("gaspode", "MissionDoor:report_trigger_sequence", "Had no parent door unit")

		return 
	end

	self._parent_door:base():trigger_sequence(trigger_sequence_name)

	return 
end
MissionDoorDevice.destroy = function (self)
	return 
end

return 
