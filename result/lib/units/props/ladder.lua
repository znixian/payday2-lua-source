Ladder = Ladder or class()
Ladder.ladders = Ladder.ladders or {}
Ladder.active_ladders = Ladder.active_ladders or {}
Ladder.ladder_index = 1
Ladder.LADDERS_PER_FRAME = 1
Ladder.SNAP_LENGTH = 125
Ladder.SEGMENT_LENGTH = 200
Ladder.MOVER_NORMAL_OFFSET = 30
Ladder.EXIT_OFFSET_TOP = 50
Ladder.ON_LADDER_NORMAL_OFFSET = 60
Ladder.DEBUG = false
Ladder.EVENT_IDS = {}
Ladder.current_ladder = function ()
	return Ladder.active_ladders[Ladder.ladder_index]
end
Ladder.next_ladder = function ()
	Ladder.ladder_index = Ladder.ladder_index + 1

	if #Ladder.active_ladders < Ladder.ladder_index then
		Ladder.ladder_index = 1
	end

	return Ladder.current_ladder()
end
Ladder.init = function (self, unit)
	self._unit = unit
	self.normal_axis = self.normal_axis or "y"
	self.up_axis = self.up_axis or "z"
	self._offset = self._offset or 0

	self.set_enabled(self, true)

	self._climb_on_top_offset = 50
	self._normal_target_offset = self._normal_target_offset or 40

	self.set_config(self)
	table.insert(Ladder.ladders, self._unit)

	return 
end
Ladder.set_config = function (self)
	self._ladder_orientation_obj = self._unit:get_object(Idstring(self._ladder_orientation_obj_name))
	local rotation = self._ladder_orientation_obj:rotation()
	local position = self._ladder_orientation_obj:position()
	self._normal = rotation[self.normal_axis](rotation)

	if self.invert_normal_axis then
		mvector3.multiply(self._normal, -1)
	end

	self._up = rotation[self.up_axis](rotation)
	self._w_dir = math.cross(self._up, self._normal)
	position = position + self._up*self._offset
	local top = position + self._up*self._height
	self._bottom = position
	self._top = top
	self._rotation = Rotation(self._w_dir, self._up, self._normal)
	self._corners = {
		position - (self._w_dir*self._width)/2,
		position + (self._w_dir*self._width)/2,
		top + (self._w_dir*self._width)/2,
		top - (self._w_dir*self._width)/2
	}
	local snap_start = Ladder.SNAP_LENGTH

	if Ladder.SNAP_LENGTH*2 < self._height then
		self._climb_distance = self._height - Ladder.SNAP_LENGTH*2
	else
		snap_start = self._height*0.2
		self._climb_distance = self._height*0.6
	end

	self._start_point = self._bottom + self._up*snap_start + self._normal*Ladder.MOVER_NORMAL_OFFSET
	local segments = 1

	if Ladder.SEGMENT_LENGTH < self._climb_distance then
		segments = self._climb_distance/Ladder.SEGMENT_LENGTH
		local percent = (segments - math.floor(segments))/math.floor(segments)

		if 0.1 < percent then
			segments = math.ceil(segments)
		else
			segments = math.floor(segments)
		end
	end

	self._segments = segments
	self._top_exit = mvector3.copy(self._normal)

	mvector3.multiply(self._top_exit, -Ladder.EXIT_OFFSET_TOP)
	mvector3.add(self._top_exit, self._top)

	self._bottom_exit = mvector3.copy(self._normal)

	mvector3.multiply(self._bottom_exit, Ladder.MOVER_NORMAL_OFFSET)
	mvector3.add(self._bottom_exit, self._bottom)

	self._up_dot = math.dot(self._up, math.UP)
	self._w_dir_half = self._w_dir*self._width*0.5

	return 
end
Ladder.update = function (self, t, dt)
	if Ladder.DEBUG then
		self.debug_draw(self)
	end

	return 
end
local mvec1 = Vector3()
Ladder.can_access = function (self, pos, move_dir)
	if not self._enabled then
		return 
	end

	if Ladder.DEBUG then
		local brush = Draw:brush(Color.red)

		brush.cylinder(brush, self._bottom, self._top, 5)
	end

	if _G.IS_VR then
		return self._can_access_vr(self, pos, move_dir)
	end

	mvector3.set(mvec1, pos)
	mvector3.subtract(mvec1, self._corners[1])

	local n_dot = mvector3.dot(self._normal, mvec1)

	if n_dot < 0 or 50 < n_dot then
		return false
	end

	local w_dot = mvector3.dot(self._w_dir, mvec1)

	if w_dot < 0 or self._width < w_dot then
		return false
	end

	local h_dot = mvector3.dot(self._up, mvec1)

	if h_dot < 0 or self._height < h_dot then
		return false
	end

	local towards_dot = mvector3.dot(move_dir, self._normal)

	if self._height - self._climb_on_top_offset < h_dot then
		return 0.5 < towards_dot
	end

	if towards_dot < -0.5 then
		return true
	end

	return 
end
Ladder._can_access_vr = function (self, pos, move_dir)
	if mvector3.distance_sq(pos, self.bottom(self)) < 250000 or mvector3.distance_sq(pos, self.top(self)) < 250000 then
		return true
	end

	return 
end
Ladder._check_end_climbing_vr = function (self, pos, move_dir, gnd_ray)
	mvector3.set(mvec1, pos)
	mvector3.subtract(mvec1, self._corners[1])

	local w_dot = mvector3.dot(self._w_dir, mvec1)
	local h_dot = mvector3.dot(self._up, mvec1)

	if w_dot < 100 or self._width + 100 < w_dot then
		return true
	elseif h_dot < 0 or self._height < h_dot then
		return true
	elseif gnd_ray and move_dir then
		local towards_dot = mvector3.dot(move_dir, self._normal)

		if 0 < towards_dot then
			if self._height - self._climb_on_top_offset < h_dot then
				return false
			end

			return true
		end
	end

	return 
end
Ladder.check_end_climbing = function (self, pos, move_dir, gnd_ray)
	if not self._enabled then
		return true
	end

	if _G.IS_VR then
		return self._check_end_climbing_vr(self, pos, move_dir, gnd_ray)
	end

	mvector3.set(mvec1, pos)
	mvector3.subtract(mvec1, self._corners[1])

	local w_dot = mvector3.dot(self._w_dir, mvec1)
	local h_dot = mvector3.dot(self._up, mvec1)

	if w_dot < 0 or self._width < w_dot then
		return true
	elseif h_dot < 0 or self._height < h_dot then
		return true
	elseif gnd_ray and move_dir then
		local towards_dot = mvector3.dot(move_dir, self._normal)

		if 0 < towards_dot then
			if self._height - self._climb_on_top_offset < h_dot then
				return false
			end

			return true
		end
	end

	return 
end
Ladder.get_normal_move_offset = function (self, pos)
	mvector3.set(mvec1, pos)
	mvector3.subtract(mvec1, self._corners[1])

	local normal_move_offset = math.dot(self._normal, mvec1)
	normal_move_offset = math.lerp(0, self._normal_target_offset - normal_move_offset, 0.1)

	return normal_move_offset
end
Ladder.position = function (self, t)
	local pos = mvector3.copy(self._up)

	mvector3.multiply(pos, t*self._climb_distance)
	mvector3.add(pos, self._start_point)

	return pos
end
Ladder.on_ladder = function (self, pos, t)
	local l_pos = self.position(self, t) - self._w_dir_half

	mvector3.set(mvec1, pos)
	mvector3.subtract(mvec1, l_pos)

	local w_dot = math.dot(self._w_dir, mvec1)

	if w_dot < 0 or self._width < w_dot then
		return false
	end

	local n_dot = math.dot(self._normal, mvec1)

	if Ladder.ON_LADDER_NORMAL_OFFSET < n_dot then
		return false
	end

	return true
end
Ladder.horizontal_offset = function (self, pos)
	mvector3.set(mvec1, pos)
	mvector3.subtract(mvec1, self._bottom)

	local offset = mvector3.copy(self._w_dir)

	mvector3.multiply(offset, math.dot(self._w_dir, mvec1))

	return offset
end
Ladder.rotation = function (self)
	return self._rotation
end
Ladder.up = function (self)
	return self._up
end
Ladder.normal = function (self)
	return self._normal
end
Ladder.w_dir = function (self)
	return self._w_dir
end
Ladder.bottom = function (self)
	return self._bottom
end
Ladder.bottom_exit = function (self)
	return self._bottom_exit
end
Ladder.top = function (self)
	return self._top
end
Ladder.top_exit = function (self)
	return self._top_exit
end
Ladder.segments = function (self)
	return self._segments
end
Ladder.set_width = function (self, width)
	self._width = width

	self.set_config(self)

	return 
end
Ladder.width = function (self)
	return self._width
end
Ladder.set_height = function (self, height)
	self._height = height

	self.set_config(self)

	return 
end
Ladder.height = function (self)
	return self._height
end
Ladder.corners = function (self)
	return self._corners
end
Ladder.set_enabled = function (self, enabled)
	self._enabled = enabled

	if self._enabled then
		if not table.contains(Ladder.active_ladders, self._unit) then
			table.insert(Ladder.active_ladders, self._unit)
		end
	else
		table.delete(Ladder.active_ladders, self._unit)
	end

	return 
end
Ladder.destroy = function (self, unit)
	table.delete(Ladder.ladders, self._unit)
	table.delete(Ladder.active_ladders, self._unit)

	return 
end
Ladder.debug_draw = function (self)
	local brush = Draw:brush(Color.white:with_alpha(0.5))

	brush.quad(brush, self._corners[1], self._corners[2], self._corners[3], self._corners[4])

	for i = 1, 4, 1 do
		brush.line(brush, self._corners[i], self._corners[i] + self._normal*(i*25 + 50))
	end

	local brush = Draw:brush(Color.red)

	brush.sphere(brush, self._corners[1], 5)

	return 
end
Ladder.save = function (self, data)
	local state = {
		enabled = self._enabled,
		height = self._height,
		width = self._width
	}
	data.Ladder = state

	return 
end
Ladder.load = function (self, data)
	local state = data.Ladder

	if state.enabled ~= self._enabled then
		self.set_enabled(self, state.enabled)
	end

	self._width = state.width
	self._height = state.height

	self.set_config(self)

	return 
end

return 
