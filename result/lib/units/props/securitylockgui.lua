SecurityLockGui = SecurityLockGui or class()
SecurityLockGui.init = function (self, unit)
	self._unit = unit
	self._visible = true
	self._powered = true
	self._gui_start = self._gui_start or "prop_timer_gui_start"
	self._gui_working = "prop_timer_gui_working"
	self._gui_done = "prop_timer_gui_done"
	self._cull_distance = self._cull_distance or 5000
	self._size_multiplier = self._size_multiplier or 1
	self._gui_object = self._gui_object or "gui_name"
	self._bars = self._bars or 3
	self._done_bars = {}
	self._5sec_bars = {}
	self._new_gui = World:newgui()

	self.add_workspace(self, self._unit:get_object(Idstring(self._gui_object)))
	self.setup(self)
	self._unit:set_extension_update_enabled(Idstring("timer_gui"), false)

	self._update_enabled = false

	return 
end
SecurityLockGui.add_workspace = function (self, gui_object)
	self._ws = self._new_gui:create_object_workspace(0, 0, gui_object, Vector3(0, 0, 0))
	self._gui = self._ws:panel():gui(Idstring("guis/security_lock_gui"))
	self._gui_script = self._gui:script()

	return 
end
SecurityLockGui.setup = function (self)
	self._gui_script.working_text:set_render_template(Idstring("VertexColorTextured"))
	self._gui_script.time_header_text:set_render_template(Idstring("VertexColorTextured"))
	self._gui_script.time_text:set_render_template(Idstring("VertexColorTextured"))
	self._gui_script.screen_background:set_size(self._gui_script.screen_background:parent():size())

	local gui_w, gui_h = self._gui_script.screen_background:parent():size()
	local pad = 64
	local bar_pad = 16

	for i = 1, 3, 1 do
		local icon = self._gui_script["timer_icon" .. i]
		local timer_bg = self._gui_script["timer" .. i .. "_background"]
		local timer = self._gui_script["timer" .. i]
		local title = self._gui_script["timer" .. i .. "_title"]
		local visible = i <= self._bars

		icon.set_visible(icon, visible)
		timer_bg.set_visible(timer_bg, visible)
		timer.set_visible(timer, visible)
		title.set_visible(title, visible)
		title.set_render_template(title, Idstring("VertexColorTextured"))
		icon.set_size(icon, 132, 132)
		icon.set_x(icon, pad)
		icon.set_y(icon, pad + icon.h(icon)*(i - 1) + pad*(i - 1) + 350)

		local w, h = icon.size(icon)

		timer_bg.set_h(timer_bg, h/2)
		timer.set_h(timer, timer_bg.h(timer_bg) - 8)
		title.set_h(title, h/2)
		title.set_text(title, managers.localization:text("prop_security_lock_title", {
			NR = i
		}))
		title.set_font_size(title, h/2*self._size_multiplier)
		title.set_top(title, icon.top(icon))
		title.set_left(title, icon.right(icon) + pad)
		title.set_w(title, gui_w - (pad*3 + w))
		timer_bg.set_bottom(timer_bg, icon.bottom(icon))
		timer_bg.set_left(timer_bg, icon.right(icon) + pad/2)
		timer_bg.set_w(timer_bg, gui_w - (pad*3 + w))
		timer.set_h(timer, timer_bg.h(timer_bg) - bar_pad)
		timer.set_w(timer, timer_bg.w(timer_bg) - bar_pad)
		timer.set_center(timer, timer_bg.center(timer_bg))

		self._timer_lenght = timer.w(timer)

		timer.set_w(timer, 0)
	end

	self._gui_script.working_text:set_center_x(self._gui_script.working_text:parent():w()/2)
	self._gui_script.working_text:set_center_y(self._gui_script.working_text:parent():h()/1.25)
	self._gui_script.working_text:set_font_size(self._size_multiplier*80)
	self._gui_script.working_text:set_text(managers.localization:text(self._gui_start))
	self._gui_script.working_text:set_visible(true)
	self._gui_script.time_header_text:set_font_size(self._size_multiplier*80)
	self._gui_script.time_header_text:set_visible(false)
	self._gui_script.time_header_text:set_center_x(self._gui_script.working_text:parent():w()/2)
	self._gui_script.time_header_text:set_center_y(self._gui_script.working_text:parent():h()/1.25)
	self._gui_script.time_text:set_font_size(self._size_multiplier*110)
	self._gui_script.time_text:set_visible(false)
	self._gui_script.time_text:set_center_x(self._gui_script.working_text:parent():w()/2)
	self._gui_script.time_text:set_center_y(self._gui_script.working_text:parent():h()/1.15)

	self._original_colors = {}

	for _, child in ipairs(self._gui_script.panel:children()) do
		self._original_colors[child.key(child)] = child.color(child)
	end

	return 
end
SecurityLockGui._start = function (self, bar, timer, current_timer)
	self._current_bar = bar
	self._started = true
	self._done = false
	self._timer = timer or 5
	self._current_timer = current_timer or self._timer

	self._gui_script["timer_icon" .. self._current_bar]:set_image("units/world/architecture/secret_stash/props_textures/security_station_locked_df")
	self._gui_script["timer" .. self._current_bar]:set_w(self._timer_lenght*(self._current_timer/self._timer - 1))
	self._gui_script.working_text:set_visible(false)
	self._unit:set_extension_update_enabled(Idstring("timer_gui"), true)

	self._update_enabled = true

	self.post_event(self, self._start_event)
	self._gui_script.time_header_text:set_visible(true)
	self._gui_script.time_text:set_visible(true)
	self._gui_script.time_text:set_text(math.floor(self._current_timer) .. " " .. managers.localization:text("prop_timer_gui_seconds"))

	if Network:is_client() then
		return 
	end

	return 
end
SecurityLockGui.restart = function (self, bar, timer)
	self.start(self, bar, timer, true)

	return 
end
SecurityLockGui.start = function (self, bar, timer, restart)
	if not restart and self._started then
		return 
	end

	self._start(self, bar, timer)

	slot4 = managers.network:session() and slot4

	return 
end
SecurityLockGui.sync_start = function (self, bar, timer)
	self._start(self, bar, timer)

	return 
end
SecurityLockGui.update = function (self, unit, t, dt)
	if not self._powered then
		return 
	end

	self._current_timer = self._current_timer - dt

	if not self._5sec_bars[self._current_bar] and self._current_timer < 5 then
		self._5sec_bars[self._current_bar] = true

		if self._unit:damage() then
			self._unit:damage():run_sequence_simple("5sec_" .. self._current_bar)
		end
	end

	self._gui_script.time_text:set_text(math.floor(self._current_timer) .. " " .. managers.localization:text("prop_timer_gui_seconds"))
	self._gui_script["timer" .. self._current_bar]:set_w(self._timer_lenght*(self._current_timer/self._timer - 1))

	if self._current_timer <= 0 then
		self._unit:set_extension_update_enabled(Idstring("timer_gui"), false)

		self._update_enabled = false

		self.done(self)
	else
		self._gui_script.working_text:set_color(self._gui_script.working_text:color():with_alpha((math.sin(t*750) + 1)/4 + 0.5))
	end

	return 
end
SecurityLockGui.set_visible = function (self, visible)
	self._visible = visible

	self._gui:set_visible(visible)

	return 
end
SecurityLockGui.set_powered = function (self, powered)
	self._set_powered(self, powered)

	return 
end
SecurityLockGui._set_powered = function (self, powered)
	self._powered = powered

	if not self._powered then
		for _, child in ipairs(self._gui_script.panel:children()) do
			local color = self._original_colors[child.key(child)]
			local c = Color(0, 0, 0, 0)

			child.set_color(child, c)
		end
	else
		for _, child in ipairs(self._gui_script.panel:children()) do
			child.set_color(child, self._original_colors[child.key(child)])
		end
	end

	return 
end
SecurityLockGui.done = function (self)
	self._set_done(self)

	if self._unit:damage() then
		self._unit:damage():run_sequence_simple("done_" .. self._current_bar)
	end

	self.post_event(self, self._done_event)

	return 
end
SecurityLockGui._set_done = function (self, bar)
	bar = bar or self._current_bar
	self._done_bars[bar] = true
	self._done = true

	self._gui_script["timer_icon" .. bar]:set_image("units/world/architecture/secret_stash/props_textures/security_station_unlocked_df")
	self._gui_script["timer" .. bar]:set_w(self._timer_lenght)
	self._gui_script.working_text:set_color(self._gui_script.working_text:color():with_alpha(1))
	self._gui_script.working_text:set_visible(true)

	if self._bars == bar then
		self._gui_script.working_text:set_text(managers.localization:text(self._gui_done))
	else
		self._started = false
	end

	self._gui_script.time_header_text:set_visible(false)
	self._gui_script.time_text:set_visible(false)

	return 
end
SecurityLockGui.post_event = function (self, event)
	if not event then
		return 
	end

	self._unit:sound_source():post_event(event)

	return 
end
SecurityLockGui.lock_gui = function (self)
	self._ws:set_cull_distance(self._cull_distance)
	self._ws:set_frozen(true)

	return 
end
SecurityLockGui.destroy = function (self)
	if alive(self._new_gui) and alive(self._ws) then
		self._new_gui:destroy_workspace(self._ws)

		self._ws = nil
		self._new_gui = nil
	end

	return 
end
SecurityLockGui.save = function (self, data)
	local state = {
		update_enabled = self._update_enabled,
		timer = self._timer,
		current_bar = self._current_bar,
		current_timer = self._current_timer,
		powered = self._powered,
		done = self._done,
		done_bars = self._done_bars,
		visible = self._visible
	}
	data.SecurityLockGui = state

	return 
end
SecurityLockGui.load = function (self, data)
	local state = data.SecurityLockGui

	for bar, _ in pairs(state.done_bars) do
		self._set_done(self, bar)
	end

	if state.update_enabled then
		self._start(self, state.current_bar, state.timer, state.current_timer)
	end

	if not state.powered then
		self._set_powered(self, state.powered)
	end

	self.set_visible(self, state.visible)

	return 
end

return 
