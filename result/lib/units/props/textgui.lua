TextGui = TextGui or class()
TextGui.COLORS = {}
TextGui.COLORS = {
	black = Color(0, 0, 0),
	white = Color(1, 1, 1),
	red = Color(0.8, 0, 0),
	green = Color(0, 0.8, 0),
	blue = Color(0, 0, 0.8),
	yellow = Color(0.8, 0.8, 0),
	orange = Color(0.8, 0.4, 0),
	light_red = Color(0.8, 0.4, 0.4),
	light_blue = Color(0.4, 0.6, 0.8),
	light_green = Color(0.6, 0.8, 0.4),
	light_yellow = Color(0.8, 0.8, 0.4),
	light_orange = Color(0.8, 0.6, 0.4)
}
TextGui.GUI_EVENT_IDS = {
	syncronize = 1,
	timer_set = 2,
	timer_start_count_up = 3,
	timer_start_count_down = 4,
	timer_pause = 5,
	timer_resume = 6,
	number_set = 7
}
TextGui.init = function (self, unit)
	self._unit = unit
	self._visible = true
	self.ROWS = self.ROWS or 2
	self.WIDTH = self.WIDTH or 640
	self.HEIGHT = self.HEIGHT or 360
	self.FONT = self.FONT or "fonts/font_large_mf"
	self.FONT_SIZE = self.FONT_SIZE or 180
	self.COLOR_TYPE = self.COLOR_TYPE or "light_blue"
	self.BG_COLOR_TYPE = self.BG_COLOR_TYPE or nil
	self.TEXT_COLOR = TextGui.COLORS[self.COLOR_TYPE]

	if self.BG_COLOR_TYPE then
		self.BG_COLOR = TextGui.COLORS[self.BG_COLOR_TYPE]
	end

	self._texts_data = {}

	for i = 1, self.ROWS, 1 do
		self._texts_data[i] = {
			speed = math.rand(1)*240 + 120,
			gap = 20,
			texts_data = {},
			iterator = 1,
			guis = {}
		}
	end

	self._text = "HELLO WORLD!"
	self._gui_object = self._gui_object or "gui_object"
	self._new_gui = World:gui()

	self.add_workspace(self, self._unit:get_object(Idstring(self._gui_object)))
	self.setup(self)
	self._unit:set_extension_update_enabled(Idstring("text_gui"), true)

	return 
end
TextGui.add_workspace = function (self, gui_object)
	self._ws = self._new_gui:create_object_workspace(self.WIDTH, self.HEIGHT, gui_object, Vector3(0, 0, 0))
	self._panel = self._ws:panel()

	return 
end
TextGui.setup = function (self)
	self._panel:clear()

	if self.BG_COLOR then
		self._bg_rect = self._panel:rect({
			layer = -1,
			color = self.BG_COLOR
		})
	end

	local font_size = self.FONT_SIZE

	return 
end
TextGui._create_text_gui = function (self, row)
	local data = self._texts_data[row]
	local text_data = data.texts_data[data.iterator]

	if not text_data then
		return 
	end

	local color = self.COLORS[text_data.color_type or self.COLOR_TYPE]
	local font_size = text_data.font_size or self.FONT_SIZE
	local font = text_data.font or self.FONT
	local gui = self._panel:text({
		y = 0,
		vertical = "center",
		align = "center",
		visible = true,
		layer = 0,
		text = text_data.text,
		font = font,
		font_size = font_size,
		color = color
	})

	if self.RENDER_TEMPLATE then
		gui.set_render_template(gui, Idstring(self.RENDER_TEMPLATE))
	end

	if self.BLEND_MODE then
		gui.set_blend_mode(gui, self.BLEND_MODE)
	end

	local _, _, w, h = gui.text_rect(gui)

	gui.set_w(gui, w)
	gui.set_h(gui, h)

	local y = self._panel:h()

	if text_data.align_h and text_data.align_h == "bottom" then
		gui.set_bottom(gui, (row - 1)*y/self.ROWS + y/self.ROWS)
	else
		gui.set_center_y(gui, (row - 1)*y/self.ROWS + y/self.ROWS/2)
	end

	local x = self._panel:w()

	if not self.START_RIGHT then
		if 0 < #data.guis then
			local last_gui = data.guis[#data.guis]
			x = last_gui.x + last_gui.gui:w() + data.gap
		else
			x = 0
		end
	end

	gui.set_x(gui, x)
	table.insert(data.guis, {
		gui = gui,
		x = x
	})

	if text_data.once then
		table.remove(data.texts_data, data.iterator)
	end

	data.iterator = data.iterator + 1

	if #data.texts_data < data.iterator then
		data.iterator = 1
	end

	return 
end
TextGui.update = function (self, unit, t, dt)
	if not self._visible then
		return 
	end

	for row, data in ipairs(self._texts_data) do
		if 0 < #data.texts_data and #data.guis == 0 then
			self._create_text_gui(self, row)
		end

		local i = 1

		while i <= #data.guis do
			local gui_data = data.guis[i]

			gui_data.gui:set_x(gui_data.x)

			gui_data.x = gui_data.x - data.speed*dt

			if i == #data.guis and gui_data.x + gui_data.gui:w() + data.gap < self._panel:w() then
				self._create_text_gui(self, row)
			end

			if gui_data.x + gui_data.gui:w() < 0 then
				gui_data.gui:parent():remove(gui_data.gui)
				table.remove(data.guis, i)
			else
				i = i + 1
			end
		end
	end

	return 
end
TextGui.set_color_type = function (self, type)
	self.COLOR_TYPE = type
	self.TEXT_COLOR = TextGui.COLORS[self.COLOR_TYPE]

	return 
end
TextGui.set_bg_color_type = function (self, type)
	self.BG_COLOR_TYPE = type
	self.BG_COLOR = (self.BG_COLOR_TYPE and TextGui.COLORS[self.BG_COLOR_TYPE]) or nil

	if self.BG_COLOR then
		self._bg_rect = self._bg_rect or self._panel:rect({
			layer = -1,
			color = self.BG_COLOR
		})

		self._bg_rect:set_color(self.BG_COLOR)
	elseif alive(self._bg_rect) then
		self._bg_rect:parent():remove(self._bg_rect)

		self._bg_rect = nil
	end

	return 
end
TextGui.add_once_text = function (self, ...)
	local t = self.add_text(self, ...)
	t.once = true

	return 
end
TextGui.add_text = function (self, row, text, color_type, font_size, align_h, font)
	local data = self._texts_data[row]

	table.insert(data.texts_data, {
		text = text,
		color_type = color_type,
		font_size = font_size,
		align_h = align_h,
		font = font
	})

	return data.texts_data[#data.texts_data]
end
TextGui.set_row_speed = function (self, row, speed)
	local data = self._texts_data[row]
	data.speed = speed

	return 
end
TextGui.set_row_gap = function (self, row, gap)
	local data = self._texts_data[row]
	data.gap = gap

	return 
end
TextGui.clear_row_and_guis = function (self, row)
	local data = self._texts_data[row]

	while 0 < #data.guis do
		local gui_data = table.remove(data.guis)

		gui_data.gui:parent():remove(gui_data.gui)
	end

	self.clear_row(self, row)

	return 
end
TextGui.clear_row = function (self, row)
	local data = self._texts_data[row]
	data.texts_data = {}
	data.iterator = 1

	return 
end
TextGui._test = function (self)
	for i = 1, self.ROWS, 1 do
		self.clear_row_and_guis(self, i)
	end

	local companies = {
		"Big bank",
		"Starbeeze",
		"Overkill",
		"GenSEC",
		"505",
		"Google",
		"Apple",
		"Microsoft",
		"Ilija CORP",
		"New York RNGRS",
		"Nissan",
		"Nintendo",
		"WPC",
		"Murky Water",
		"Beer Company",
		"Ivan food sollution systems",
		"Catfight",
		"LOL"
	}

	for i, company in ipairs(companies) do
		local j = math.rand(20) - 10
		local row = math.mod(i, self.ROWS) + 1

		self.add_text(self, row, company, "white")
		self.add_text(self, row, "" .. ((j < 0 and "") or "+") .. string.format("%.2f", j) .. "%", (j < 0 and "light_red") or "light_green", self.FONT_SIZE/1.5, "bottom", nil)
		self.add_text(self, row, "  ", "white")
	end

	return 
end
TextGui._test2 = function (self)
	for i = 1, self.ROWS, 1 do
		self.clear_row_and_guis(self, i)
	end

	local texts = {
		"Welcome to Big Bank"
	}
	local texts2 = {
		"Loan",
		"Invest",
		"Market",
		"Stock",
		"Currencies",
		"Global Markets",
		"Sell",
		"Buy",
		"Portfolio",
		"Funds",
		"MONEY!"
	}

	for i, text in ipairs(texts) do
		self.add_text(self, 1, text, "green")
	end

	for i, text in ipairs(texts2) do
		self.add_text(self, 2, text, "light_green")
		self.add_text(self, 2, " - ", "light_green")
	end

	return 
end
TextGui._sequence_trigger = function (self, sequence_name)
	if not Network:is_server() then
		return 
	end

	if self._unit:damage():has_sequence(sequence_name) then
		self._unit:damage():run_sequence_simple(sequence_name)
	end

	return 
end
TextGui.set_visible = function (self, visible)
	self._visible = visible

	if visible then
		self._ws:show()
	else
		self._ws:hide()
	end

	return 
end
TextGui.lock_gui = function (self)
	self._ws:set_cull_distance(self._cull_distance)
	self._ws:set_frozen(true)

	return 
end
TextGui.sync_gui_net_event = function (self, event_id, value)
	if event_id == TextGui.GUI_EVENT_IDS.syncronize then
		self.timer_set(self, value)
	elseif event_id == TextGui.GUI_EVENT_IDS.timer_set then
		self.timer_set(self, value)
	elseif event_id == TextGui.GUI_EVENT_IDS.timer_start_count_up then
		self.timer_start_count_up(self)
	elseif event_id == TextGui.GUI_EVENT_IDS.timer_start_count_down then
		self.timer_start_count_down(self)
	elseif event_id == TextGui.GUI_EVENT_IDS.timer_pause then
		self.timer_pause(self)
	elseif event_id == TextGui.GUI_EVENT_IDS.timer_resume then
		self.timer_resume(self)
	elseif event_id == TextGui.GUI_EVENT_IDS.number_set then
		self.number_set(self, value)
	end

	return 
end
TextGui.destroy = function (self)
	if alive(self._new_gui) and alive(self._ws) then
		self._new_gui:destroy_workspace(self._ws)

		self._ws = nil
		self._new_gui = nil
	end

	return 
end
TextGui.save = function (self, data)
	local state = {
		COLOR_TYPE = self.COLOR_TYPE,
		BG_COLOR_TYPE = self.BG_COLOR_TYPE,
		visible = self._visible
	}
	data.TextGui = state

	return 
end
TextGui.load = function (self, data)
	local state = data.TextGui

	self.set_color_type(self, state.COLOR_TYPE)
	self.set_bg_color_type(self, state.BG_COLOR_TYPE)

	if state.visible ~= self._visible then
		self.set_visible(self, state.visible)
	end

	return 
end

return 
