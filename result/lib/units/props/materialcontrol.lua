MaterialControl = MaterialControl or class()
MaterialControl.init = function (self, unit)
	self._unit = unit

	return 
end
MaterialControl.save = function (self, save_data)
	local data = {}
	local materials = self._unit:get_objects_by_type(Idstring("material"))

	for idx, material in ipairs(materials) do
		local material_data = {
			time = material.time(material),
			playing = material.is_playing(material),
			playing_speed = material.playing_speed(material)
		}
		data[idx] = material_data
	end

	save_data.material_control = data

	return 
end
MaterialControl.load = function (self, load_data)
	local data = load_data.material_control

	if not data then
		return 
	end

	local materials = self._unit:get_objects_by_type(Idstring("material"))

	for idx, material in ipairs(materials) do
		local material_data = data[idx]

		if material_data then
			if material_data.is_playing then
				material.play(material, material_data.playing_speed)
				material.set_time(material, material_data.time)
			else
				material.stop(material)
				material.set_time(material, material_data.time)
			end
		end
	end

	return 
end
MaterialControl.play = function (self, material_name, speed)
	local materials = self._unit:get_objects_by_type(Idstring("material"))
	local material_id = Idstring(material_name)

	for i, material in ipairs(materials) do
		if material.name(material) == material_id then
			Application:trace("***********  PLAYING: ", material_name, ",   speed:  ", speed)
			material.play(material, speed)
		end
	end

	return 
end
MaterialControl.stop = function (self, material_name)
	local materials = self._unit:get_objects_by_type(Idstring("material"))
	local material_id = Idstring(material_name)

	for _, material in ipairs(materials) do
		if material.name(material) == material_id then
			Application:trace("***********  STOPPING: ", material_name)
			material.stop(material)
		end
	end

	return 
end
MaterialControl.pause = function (self, material_name)
	local materials = self._unit:get_objects_by_type(Idstring("material"))
	local material_id = Idstring(material_name)

	for _, material in ipairs(materials) do
		if material.name(material) == material_id then
			Application:trace("***********  PAUSING: ", material_name)
			material.pause(material)
		end
	end

	return 
end
MaterialControl.set_time = function (self, material_name, time)
	local materials = self._unit:get_objects_by_type(Idstring("material"))
	local material_id = Idstring(material_name)

	for _, material in ipairs(materials) do
		if material.name(material) == material_id then
			Application:trace("***********  SETTIMT TIME: ", material_name, "  ", time)
			material.set_time(material, time)
		end
	end

	return 
end

return 
