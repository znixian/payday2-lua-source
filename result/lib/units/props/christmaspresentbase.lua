ChristmasPresentBase = ChristmasPresentBase or class(UnitBase)
ChristmasPresentBase.init = function (self, unit)
	UnitBase.init(self, unit, false)

	self._unit = unit

	self._unit:set_slot(0)

	return 
end
ChristmasPresentBase.take_money = function (self, unit)
	local params = {
		effect = Idstring("effects/particles/environment/player_snowflakes"),
		position = Vector3(),
		rotation = Rotation()
	}

	World:effect_manager():spawn(params)
	managers.hud._sound_source:post_event("jingle_bells")
	Network:detach_unit(self._unit)
	self._unit:set_slot(0)

	return 
end
ChristmasPresentBase.update = function (self, unit, t, dt)
	return 
end
ChristmasPresentBase.destroy = function (self)
	return 
end

return 
