TeamAISound = TeamAISound or class(PlayerSound)
TeamAISound.init = function (self, unit)
	self._unit = unit

	unit.base(unit):post_init()

	local ss = unit.sound_source(unit)

	ss.set_switch(ss, "robber", tweak_data.character[unit.base(unit)._tweak_table].speech_prefix)
	ss.set_switch(ss, "int_ext", "third")

	return 
end
TeamAISound.set_voice = function (self, voice)
	local ss = self._unit:sound_source()

	ss.set_switch(ss, "robber", voice)
	ss.set_switch(ss, "int_ext", "third")

	return 
end

return 
