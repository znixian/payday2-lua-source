HuskTeamAIDamage = HuskTeamAIDamage or class(TeamAIDamage)
TeamAIDamage._RESULT_NAME_TABLE = {
	"hurt",
	"bleedout",
	"death",
	"light_hurt",
	"heavy_hurt",
	"fatal"
}
TeamAIDamage._ATTACK_VARIANTS = CopDamage._ATTACK_VARIANTS
HuskTeamAIDamage.update = function (self, unit, t, dt)
	return 
end
HuskTeamAIDamage.damage_bullet = function (self, attack_data)
	if self._dead or self._fatal then
		return 
	end

	if PlayerDamage.is_friendly_fire(self, attack_data.attacker_unit) then
		self._unit:network():send_to_host("friendly_fire_hit")

		return 
	end

	local damage_abs, damage_percent = self._clamp_health_percentage(self, attack_data.damage, true)

	if 0 < damage_percent then
		local body_index = self._unit:get_body_index(attack_data.col_ray.body:name())
		local hit_offset_height = math.clamp(attack_data.col_ray.position.z - self._unit:movement():m_pos().z, 0, 300)
		local attacker = attack_data.attacker_unit

		if attacker.id(attacker) == -1 then
			attacker = self._unit
		end

		managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)
		self._unit:network():send_to_host("damage_bullet", attacker, damage_percent, body_index, hit_offset_height, 0, false)
		self._send_damage_drama(self, attack_data, damage_abs)
	end

	return 
end
HuskTeamAIDamage.damage_explosion = function (self, attack_data)
	if self._dead or self._fatal or PlayerDamage.is_friendly_fire(self, attack_data.attacker_unit) then
		return 
	end

	local damage_abs, damage_percent = self._clamp_health_percentage(self, attack_data.damage, true)

	if 0 < damage_percent then
		local hit_offset_height = math.clamp(attack_data.col_ray.position.z - self._unit:movement():m_pos().z, 0, 300)
		local attacker = attack_data.attacker_unit

		if attacker and attacker.id(attacker) == -1 then
			attacker = self._unit
		end

		managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)
		self._unit:network():send_to_host("damage_explosion_fire", attacker, damage_percent, CopDamage._get_attack_variant_index(self, "explosion"), (self._dead and true) or false, attack_data.col_ray.ray, attack_data.weapon_unit)
		self._send_damage_drama(self, attack_data, damage_abs)
	end

	return 
end
HuskTeamAIDamage.damage_fire = function (self, attack_data)
	if self._dead or self._fatal then
		return 
	end

	local attacker_unit = attack_data.attacker_unit

	if attacker_unit and attacker_unit.base(attacker_unit) and attacker_unit.base(attacker_unit).thrower_unit then
		attacker_unit = attacker_unit.base(attacker_unit):thrower_unit()
	end

	if PlayerDamage.is_friendly_fire(self, attacker_unit) then
		self._unit:network():send_to_host("friendly_fire_hit")

		return 
	end

	local damage_abs, damage_percent = self._clamp_health_percentage(self, attack_data.damage, true)

	if 0 < damage_percent then
		local hit_offset_height = math.clamp(attack_data.col_ray.position.z - self._unit:movement():m_pos().z, 0, 300)
		local attacker = attack_data.attacker_unit

		if attacker and attacker.id(attacker) == -1 then
			attacker = self._unit
		end

		managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)
		self._unit:network():send_to_host("damage_explosion_fire", attacker, damage_percent, CopDamage._get_attack_variant_index(self, "fire"), (self._dead and true) or false, attack_data.col_ray.ray, attack_data.weapon_unit)
		self._send_damage_drama(self, attack_data, damage_abs)
	end

	return 
end
HuskTeamAIDamage.damage_melee = function (self, attack_data)
	if self._dead or self._fatal then
		return 
	end

	if PlayerDamage.is_friendly_fire(self, attack_data.attacker_unit) then
		return 
	end

	local damage_abs, damage_percent = self._clamp_health_percentage(self, attack_data.damage, true)

	if 0 < damage_percent then
		local hit_offset_height = math.clamp(attack_data.col_ray.position.z - self._unit:movement():m_pos().z, 0, 300)
		local attacker = attack_data.attacker_unit

		if attacker.id(attacker) == -1 then
			attacker = self._unit
		end

		managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)
		self._unit:network():send_to_host("damage_melee", attacker, damage_percent, 1, hit_offset_height, 0)
		self._send_damage_drama(self, attack_data, damage_abs)
	end

	return 
end
HuskTeamAIDamage.sync_damage_bullet = function (self, attacker_unit, hit_offset_height, result_index)
	if self._dead or self._fatal then
		return 
	end

	managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)

	local result_type = (result_index ~= 0 and self._RESULT_NAME_TABLE[result_index]) or nil
	local result = {
		variant = "bullet",
		type = result_type
	}
	local hit_pos = mvector3.copy(self._unit:movement():m_pos())

	mvector3.set_z(hit_pos, hit_pos.z + hit_offset_height)

	local attack_dir = nil

	if attacker_unit then
		attack_dir = hit_pos - attacker_unit.movement(attacker_unit):m_head_pos()

		mvector3.normalize(attack_dir)
	else
		attack_dir = self._unit:rotation():y()
	end

	if not self._no_blood then
		managers.game_play_central:sync_play_impact_flesh(hit_pos, attack_dir)
	end

	local attack_data = {
		variant = "bullet",
		attacker_unit = attacker_unit,
		attack_dir = attack_dir,
		pos = hit_pos,
		result = result
	}

	if result_type == "fatal" then
		self._on_fatal(self)
	elseif result_type == "bleedout" then
		self._on_bleedout(self)
	end

	self._call_listeners(self, attack_data)

	return 
end
HuskTeamAIDamage.sync_damage_explosion = function (self, attacker_unit, result_index, i_attack_variant)
	if self._dead or self._fatal then
		return 
	end

	local variant = CopDamage._ATTACK_VARIANTS[i_attack_variant]

	managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)

	local result_type = (result_index ~= 0 and self._RESULT_NAME_TABLE[result_index]) or nil
	local result = {
		variant = variant,
		type = result_type
	}
	local hit_pos = mvector3.copy(self._unit:movement():m_pos())

	mvector3.set_z(hit_pos, hit_pos.z + 130)

	local attack_dir = nil

	if attacker_unit then
		attack_dir = hit_pos - attacker_unit.position(attacker_unit)

		mvector3.normalize(attack_dir)
	else
		attack_dir = self._unit:rotation():y()
	end

	if not self._no_blood then
		managers.game_play_central:sync_play_impact_flesh(hit_pos, attack_dir)
	end

	local attack_data = {
		variant = variant,
		attacker_unit = attacker_unit,
		attack_dir = attack_dir,
		pos = hit_pos,
		result = result
	}

	if result_type == "fatal" then
		self._on_fatal(self)
	elseif result_type == "bleedout" then
		self._on_bleedout(self)
	end

	self._call_listeners(self, attack_data)

	return 
end
HuskTeamAIDamage.sync_damage_fire = function (self, attacker_unit, result_index, i_attack_variant)
	if self._dead or self._fatal then
		return 
	end

	local variant = CopDamage._ATTACK_VARIANTS[i_attack_variant]

	managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)

	local result_type = (result_index ~= 0 and self._RESULT_NAME_TABLE[result_index]) or nil
	local result = {
		variant = variant,
		type = result_type
	}
	local hit_pos = mvector3.copy(self._unit:movement():m_pos())

	mvector3.set_z(hit_pos, hit_pos.z + 130)

	local attack_dir = nil

	if attacker_unit then
		attack_dir = hit_pos - attacker_unit.position(attacker_unit)

		mvector3.normalize(attack_dir)
	else
		attack_dir = self._unit:rotation():y()
	end

	if not self._no_blood then
		managers.game_play_central:sync_play_impact_flesh(hit_pos, attack_dir)
	end

	local attack_data = {
		variant = variant,
		attacker_unit = attacker_unit,
		attack_dir = attack_dir,
		pos = hit_pos,
		result = result
	}

	if result_type == "fatal" then
		self._on_fatal(self)
	elseif result_type == "bleedout" then
		self._on_bleedout(self)
	end

	self._call_listeners(self, attack_data)

	return 
end
HuskTeamAIDamage.sync_damage_melee = function (self, attacker_unit, hit_offset_height, result_index)
	if self._dead or self._fatal then
		return 
	end

	managers.hud:set_mugshot_damage_taken(self._unit:unit_data().mugshot_id)

	local result_type = (result_index ~= 0 and self._RESULT_NAME_TABLE[result_index]) or nil
	local result = {
		variant = "melee",
		type = result_type
	}
	local hit_pos = mvector3.copy(self._unit:movement():m_pos())

	mvector3.set_z(hit_pos, hit_pos.z + 130)

	local attack_dir = nil

	if attacker_unit then
		attack_dir = hit_pos - attacker_unit.movement(attacker_unit):m_head_pos()

		mvector3.normalize(attack_dir)
	else
		attack_dir = self._unit:rotation():y()

		mvector3.negate(attack_dir)
	end

	if not self._no_blood then
		managers.game_play_central:sync_play_impact_flesh(hit_pos, attack_dir)
	end

	local attack_data = {
		variant = "melee",
		attacker_unit = attacker_unit,
		attack_dir = attack_dir,
		pos = hit_pos,
		result = result
	}

	if result_type == "fatal" then
		self._on_fatal(self)
	elseif result_type == "bleedout" then
		self._on_bleedout(self)
	end

	self._call_listeners(self, attack_data)

	return 
end
HuskTeamAIDamage.sync_damage_bleeding = function (self)
	local dmg_info = {
		variant = "bleeding",
		result = {
			type = "death"
		}
	}

	self._die(self)
	self._call_listeners(self, dmg_info)
	self._unregister_unit(self)

	return 
end
HuskTeamAIDamage.sync_damage_incapacitated = function (self)
	self._fatal = true

	self._unit:interaction():set_active(true, false)

	local dmg_info = {
		variant = "bleeding",
		result = {
			type = "fatal"
		}
	}

	self._call_listeners(self, dmg_info)

	return 
end
HuskTeamAIDamage.sync_damage_tase = function (self)
	self.damage_tase(self)

	return 
end
HuskTeamAIDamage.sync_unit_recovered = function (self)
	if self._tase_effect then
		World:effect_manager():fade_kill(self._tase_effect)
	end

	self._fatal = nil
	self._bleed_out = nil

	self._unit:interaction():set_active(false, false)
	managers.groupai:state():on_criminal_recovered(self._unit)
	managers.hud:set_mugshot_normal(self._unit:unit_data().mugshot_id)

	return 
end
HuskTeamAIDamage.revive = function (self, reviving_unit)
	if self._dead then
		return 
	end

	self._unit:network():send_to_host("revive_unit", reviving_unit)

	return 
end
HuskTeamAIDamage.pause_bleed_out = function (self)
	if self._dead then
		return 
	end

	self._unit:network():send_to_host("pause_bleed_out")

	return 
end
HuskTeamAIDamage.unpause_bleed_out = function (self)
	if self._dead then
		return 
	end

	if self._unit:id() == -1 then
		return 
	end

	self._unit:network():send_to_host("unpause_bleed_out")

	return 
end
HuskTeamAIDamage.pause_arrested_timer = function (self)
	if self._dead then
		return 
	end

	self._unit:network():send_to_host("pause_arrested_timer")

	return 
end
HuskTeamAIDamage.unpause_arrested_timer = function (self)
	if self._dead then
		return 
	end

	if self._unit:id() == -1 then
		return 
	end

	self._unit:network():send_to_host("unpause_arrested_timer")

	return 
end
HuskTeamAIDamage._on_bleedout = function (self)
	self._bleed_out = true
	self._fatal = nil

	self._unit:interaction():set_tweak_data("revive")
	self._unit:interaction():set_active(true, false)
	managers.hud:set_mugshot_downed(self._unit:unit_data().mugshot_id)

	return 
end
HuskTeamAIDamage._on_fatal = function (self)
	self._fatal = true

	if not self._bleed_out then
		self._unit:interaction():set_tweak_data("revive")
		self._unit:interaction():set_active(true, false)
		managers.hud:set_mugshot_downed(self._unit:unit_data().mugshot_id)
	end

	self._bleed_out = nil

	return 
end
HuskTeamAIDamage._on_death = function (self)
	self._dead = true
	self._bleed_out = nil
	self._fatal = nil

	self._unit:interaction():set_active(false, false)
	self._unregister_unit(self)

	return 
end
HuskTeamAIDamage.load = function (self, data)
	if not data.char_dmg then
		return 
	end

	if data.char_dmg.arrested then
		self._unit:movement():sync_arrested()
	end

	if data.char_dmg.bleedout then
		self._on_bleedout(self)
	end

	if data.char_dmg.fatal then
		self._on_fatal(self)
	end

	return 
end

return 
