HuskTeamAIMovement = HuskTeamAIMovement or class(TeamAIMovement)
HuskTeamAIMovement.init = function (self, unit)
	HuskTeamAIMovement.super.init(self, unit)

	self._queued_actions = {}
	self._m_host_stop_pos = mvector3.copy(self._m_pos)

	return 
end
HuskTeamAIMovement._post_init = function (self)
	self.play_redirect(self, "idle")

	return 
end
HuskTeamAIMovement.sync_arrested = function (self)
	self._unit:interaction():set_tweak_data("free")
	self._unit:interaction():set_active(true, false)
	managers.hud:set_mugshot_cuffed(self._unit:unit_data().mugshot_id)
	self._unit:base():set_slot(self._unit, 24)

	return 
end
HuskTeamAIMovement.add_weapons = function (self)
	local weapon = self._ext_base:default_weapon_name("primary")
	local _ = weapon and self._unit:inventory():add_unit_by_factory_name(weapon, false, false, nil, "")
	local sec_weap_name = self._ext_base:default_weapon_name("secondary")
	local _ = sec_weap_name and sec_weap_name ~= weapon and self._unit:inventory():add_unit_by_name(sec_weap_name)

	return 
end
HuskTeamAIMovement._upd_actions = function (self, t)
	TeamAIMovement._upd_actions(self, t)
	HuskCopMovement._chk_start_queued_action(self)

	return 
end
HuskTeamAIMovement.action_request = function (self, action_desc)
	return HuskCopMovement.action_request(self, action_desc)
end
HuskTeamAIMovement.chk_action_forbidden = function (self, action_desc)
	return HuskCopMovement.chk_action_forbidden(self, action_desc)
end

return 
