TeamAIBase = TeamAIBase or class(CopBase)
TeamAIBase.post_init = function (self)
	self._ext_movement = self._unit:movement()
	self._ext_anim = self._unit:anim_data()

	self._ext_movement:post_init(true)
	self._unit:brain():post_init()
	self.set_anim_lod(self, 1)

	self._lod_stage = 1
	self._allow_invisible = true

	self._register(self)
	managers.occlusion:remove_occlusion(self._unit)

	return 
end
TeamAIBase.nick_name = function (self)
	local name = self._tweak_table

	return managers.localization:text("menu_" .. name)
end
TeamAIBase.default_weapon_name = function (self, slot)
	return tweak_data.character[self._tweak_table].weapon.weapons_of_choice[slot or "primary"]
end
TeamAIBase.arrest_settings = function (self)
	return tweak_data.character[self._tweak_table].arrest
end
TeamAIBase.pre_destroy = function (self, unit)
	self.remove_upgrades(self)
	self.unregister(self)
	UnitBase.pre_destroy(self, unit)
	unit.brain(unit):pre_destroy(unit)
	unit.movement(unit):pre_destroy()
	unit.inventory(unit):pre_destroy(unit)
	unit.character_damage(unit):pre_destroy()

	return 
end
TeamAIBase.set_loadout = function (self, loadout)
	if self._loadout then
		self.remove_upgrades(self)
	end

	local function aquire(item)
		if not tweak_data.upgrades.crew_skill_definitions[item] then
			local definition = {
				upgrades = (item and {
					{
						category = "team",
						upgrade = item
					}
				}) or {}
			}
		end

		for _, v in pairs(definition.upgrades) do
			managers.player:aquire_incremental_upgrade({
				category = v.category,
				upgrade = v.upgrade
			})
		end

		return 
	end

	aquire("crew_active")
	aquire(loadout.ability)
	aquire(loadout.skill)

	self._loadout = loadout

	return 
end
TeamAIBase.remove_upgrades = function (self)
	if self._loadout then
		local function unaquire(item)
			if not tweak_data.upgrades.crew_skill_definitions[item] then
				local definition = {
					upgrades = (item and {
						{
							category = "team",
							upgrade = item
						}
					}) or {}
				}
			end

			for _, v in pairs(definition.upgrades) do
				managers.player:unaquire_incremental_upgrade({
					category = v.category,
					upgrade = v.upgrade
				})
			end

			return 
		end

		unaquire("crew_active")
		unaquire(self._loadout.ability)
		unaquire(self._loadout.skill)

		self._loadout = nil
	end

	return 
end
TeamAIBase.save = function (self, data)
	data.base = {
		tweak_table = self._tweak_table,
		loadout = managers.blackmarket:henchman_loadout_string_from_loadout(self._loadout)
	}

	return 
end
TeamAIBase.on_death_exit = function (self)
	TeamAIBase.super.on_death_exit(self)
	self.unregister(self)
	self.set_slot(self, self._unit, 0)

	return 
end
TeamAIBase._register = function (self)
	if not self._registered then
		managers.groupai:state():register_criminal(self._unit)

		self._registered = true
	end

	return 
end
TeamAIBase.unregister = function (self)
	if self._registered then
		if Network:is_server() then
			self._unit:brain():attention_handler():set_attention(nil)
		end

		if managers.groupai:state():all_AI_criminals()[self._unit:key()] then
			managers.groupai:state():unregister_criminal(self._unit)
		end

		self._char_name = managers.criminals:character_name_by_unit(self._unit)
		self._registered = nil
	end

	return 
end
TeamAIBase.chk_freeze_anims = function (self)
	return 
end

return 
