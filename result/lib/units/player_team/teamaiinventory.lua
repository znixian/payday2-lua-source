TeamAIInventory = TeamAIInventory or class(CopInventory)
TeamAIInventory.add_unit_by_factory_name = HuskPlayerInventory.add_unit_by_factory_name
TeamAIInventory.add_unit_by_factory_blueprint = HuskPlayerInventory.add_unit_by_factory_blueprint
TeamAIInventory.preload_mask = function (self)
	local character_name = managers.criminals:character_name_by_unit(self._unit)
	local mask_unit_name = managers.criminals:character_data_by_name(character_name).mask_obj
	mask_unit_name = mask_unit_name[Global.level_data.level_id] or mask_unit_name.default or mask_unit_name
	self._mask_unit_name = mask_unit_name

	managers.dyn_resource:load(Idstring("unit"), Idstring(mask_unit_name), managers.dyn_resource.DYN_RESOURCES_PACKAGE, callback(self, self, "clbk_mask_unit_loaded"))

	return 
end
TeamAIInventory.clbk_mask_unit_loaded = function (self, status, asset_type, asset_name)
	self._mask_unit_loaded = status

	self._reset_mask_visibility(self)

	return 
end
TeamAIInventory.is_mask_unit_loaded = function (self)
	return self._mask_unit_loaded
end
TeamAIInventory.add_unit_by_name = function (self, new_unit_name, equip)
	local new_unit = World:spawn_unit(new_unit_name, Vector3(), Rotation())
	local setup_data = {
		user_unit = self._unit,
		ignore_units = {
			self._unit,
			new_unit
		},
		expend_ammo = false,
		hit_slotmask = managers.slot:get_mask("bullet_impact_targets"),
		user_sound_variant = tweak_data.character[self._unit:base()._tweak_table].weapon_voice,
		alert_AI = true,
		alert_filter = self._unit:brain():SO_access()
	}

	new_unit.base(new_unit):setup(setup_data)
	self.add_unit(self, new_unit, equip)
	new_unit.set_enabled(new_unit, false)

	return 
end
TeamAIInventory.add_unit = function (self, new_unit, equip)
	TeamAIInventory.super.add_unit(self, new_unit, equip)

	if new_unit.base(new_unit).set_user_is_team_ai then
		print("Set as team ai")
		new_unit.base(new_unit):set_user_is_team_ai(true)
	end

	new_unit.set_enabled(new_unit, false)
	new_unit.base(new_unit):set_visibility_state(false)

	return 
end
TeamAIInventory._ensure_weapon_visibility = function (self, override_weapon, override)
	local show_gun = override or not self._unit:movement():cool()
	local weapon = override_weapon or self.equipped_unit(self)

	weapon.set_enabled(weapon, show_gun)
	weapon.set_visible(weapon, show_gun)

	local base = weapon.base(weapon)

	if base and base.set_visibility_state then
		local our_stage = (show_gun and self._unit:base()._lod_stage) or show_gun

		base.set_visibility_state(base, show_gun)
	end

	return 
end
TeamAIInventory.equip_selection = function (self, selection_index, instant)
	local res = TeamAIInventory.super.equip_selection(self, selection_index, instant)

	self._ensure_weapon_visibility(self)

	return res
end
TeamAIInventory.synch_equipped_weapon = function (self, weap_index, blueprint_string, cosmetics_string)
	local weapon_name = self._get_weapon_name_from_sync_index(weap_index)

	if type(weapon_name) == "string" then
		self.add_unit_by_factory_name(self, weapon_name, true, true, blueprint_string, cosmetics_string or self.cosmetics_string_from_peer(self, peer, weapon_name))

		return 
	end

	self.add_unit_by_name(self, weapon_name, true, true)

	return 
end
TeamAIInventory._unload_mask = function (self)
	if self._mask_unit_name then
		managers.dyn_resource:unload(Idstring("unit"), Idstring(self._mask_unit_name), DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)

		self._mask_unit_name = nil
	end

	return 
end
TeamAIInventory._reset_mask_visibility = function (self)
	self.set_mask_visibility(self, (self._mask_visibility and true) or false)

	return 
end
TeamAIInventory.pre_destroy = function (self, unit)
	TeamAIInventory.super.pre_destroy(self, unit)
	self._unload_mask(self)

	return 
end

return 
