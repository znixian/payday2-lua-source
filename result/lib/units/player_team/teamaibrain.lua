require("lib/units/player_team/logics/TeamAILogicBase")
require("lib/units/player_team/logics/TeamAILogicInactive")
require("lib/units/player_team/logics/TeamAILogicIdle")
require("lib/units/player_team/logics/TeamAILogicAssault")
require("lib/units/player_team/logics/TeamAILogicTravel")
require("lib/units/player_team/logics/TeamAILogicDisabled")
require("lib/units/player_team/logics/TeamAILogicSurrender")

TeamAIBrain = TeamAIBrain or class(CopBrain)
TeamAIBrain._create_attention_setting_from_descriptor = PlayerMovement._create_attention_setting_from_descriptor
TeamAIBrain._logics = {
	inactive = TeamAILogicInactive,
	idle = TeamAILogicIdle,
	surrender = TeamAILogicSurrender,
	travel = TeamAILogicTravel,
	assault = TeamAILogicAssault,
	disabled = TeamAILogicDisabled
}
local reload = nil

if TeamAIBrain._reload_clbks then
	reload = true
else
	TeamAIBrain._reload_clbks = {}
end

TeamAIBrain.init = function (self, unit)
	self._unit = unit
	self._timer = TimerManager:game()

	self.set_update_enabled_state(self, false)

	self._current_logic = nil
	self._current_logic_name = nil
	self._active = true
	self._SO_access = managers.navigation:convert_access_flag(tweak_data.character[unit.base(unit)._tweak_table].access)
	self._reload_clbks[unit.key(unit)] = callback(self, self, "on_reload")

	return 
end
TeamAIBrain.post_init = function (self)
	self._reset_logic_data(self)

	local my_key = tostring(self._unit:key())

	self._unit:character_damage():add_listener("TeamAIBrain_hurt" .. my_key, {
		"bleedout",
		"hurt",
		"light_hurt",
		"heavy_hurt",
		"fatal",
		"none"
	}, callback(self, self, "clbk_damage"))
	self._unit:character_damage():add_listener("TeamAIBrain_death" .. my_key, {
		"death"
	}, callback(self, self, "clbk_death"))
	managers.groupai:state():add_listener("TeamAIBrain" .. my_key, {
		"enemy_weapons_hot"
	}, callback(self, self, "clbk_heat"))

	if not self._current_logic then
		self.set_init_logic(self, "idle")
	end

	self._setup_attention_handler(self)

	self._alert_listen_key = "TeamAIBrain" .. tostring(self._unit:key())
	local alert_listen_filter = managers.groupai:state():get_unit_type_filter("combatant")

	managers.groupai:state():add_alert_listener(self._alert_listen_key, callback(self, self, "on_alert"), alert_listen_filter, {
		bullet = true,
		vo_intimidate = true
	}, self._unit:movement():m_head_pos())

	return 
end
TeamAIBrain._reset_logic_data = function (self)
	TeamAIBrain.super._reset_logic_data(self)

	self._logic_data.enemy_slotmask = managers.slot:get_mask("enemies")
	self._logic_data.objective_complete_clbk = callback(managers.groupai:state(), managers.groupai:state(), "on_criminal_objective_complete")
	self._logic_data.objective_failed_clbk = callback(managers.groupai:state(), managers.groupai:state(), "on_criminal_objective_failed")

	return 
end
TeamAIBrain.set_spawn_ai = function (self, spawn_ai)
	TeamAIBrain.super.set_spawn_ai(self, spawn_ai)

	if managers.groupai:state():enemy_weapons_hot() then
		self.clbk_heat(self)
	end

	return 
end
TeamAIBrain.clbk_damage = function (self, my_unit, damage_info)
	self._current_logic.damage_clbk(self._logic_data, damage_info)

	return 
end
TeamAIBrain.clbk_death = function (self, my_unit, damage_info)
	TeamAIBrain.super.clbk_death(self, my_unit, damage_info)
	self.set_objective(self)

	return 
end
TeamAIBrain.on_cop_neutralized = function (self, cop_key)
	return self._current_logic.on_cop_neutralized(self._logic_data, cop_key)
end
TeamAIBrain.on_long_dis_interacted = function (self, amount, other_unit, secondary)
	self._unit:movement():set_cool(false)

	return self._current_logic.on_long_dis_interacted(self._logic_data, other_unit, secondary)
end
TeamAIBrain.on_recovered = function (self, reviving_unit)
	self._current_logic.on_recovered(self._logic_data, reviving_unit)

	return 
end
TeamAIBrain.clbk_heat = function (self)
	self._current_logic.clbk_heat(self._logic_data)

	return 
end
TeamAIBrain.pre_destroy = function (self, unit)
	TeamAIBrain.super.pre_destroy(self, unit)
	managers.groupai:state():remove_listener("TeamAIBrain" .. tostring(self._unit:key()))

	return 
end
TeamAIBrain.set_active = function (self, state)
	TeamAIBrain.super.set_active(self, state)

	if not state then
		self.set_objective(self)
	end

	self._unit:character_damage():disable()

	return 
end
TeamAIBrain.set_player_ignore = function (self, state)
	self._ignore_player = state or nil

	return 
end
TeamAIBrain.player_ignore = function (self)
	return self._ignore_player
end
TeamAIBrain._setup_attention_handler = function (self)
	TeamAIBrain.super._setup_attention_handler(self)
	self.on_cool_state_changed(self, self._unit:movement():cool())

	return 
end
TeamAIBrain.on_cool_state_changed = function (self, state)
	if self._logic_data then
		self._logic_data.cool = state
	end

	if not self._attention_handler then
		return 
	end

	local att_settings = nil

	if state then
		att_settings = {
			"team_team_idle"
		}
	else
		att_settings = {
			"team_enemy_cbt"
		}
	end

	PlayerMovement.set_attention_settings(self, att_settings, "team_AI")

	return 
end
TeamAIBrain.clbk_attention_notice_sneak = function (self, observer_unit, status)
	return 
end
TeamAIBrain._chk_enable_bodybag_interaction = function (self)
	self._unit:interaction():set_tweak_data("dead")

	return 
end

return 
