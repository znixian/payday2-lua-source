HuskTeamAIBase = HuskTeamAIBase or class(HuskCopBase)
HuskTeamAIBase.set_loadout = TeamAIBase.set_loadout
HuskTeamAIBase.remove_upgrades = TeamAIBase.remove_upgrades
HuskTeamAIBase.default_weapon_name = function (self)
	return TeamAIBase.default_weapon_name(self)
end
HuskTeamAIBase.post_init = function (self)
	self._ext_anim = self._unit:anim_data()

	self._unit:movement():post_init()
	self.set_anim_lod(self, 1)

	self._lod_stage = 1
	self._allow_invisible = true

	TeamAIBase._register(self)
	managers.occlusion:remove_occlusion(self._unit)

	return 
end
HuskTeamAIBase.nick_name = function (self)
	return TeamAIBase.nick_name(self)
end
HuskTeamAIBase.on_death_exit = function (self)
	HuskTeamAIBase.super.on_death_exit(self)
	TeamAIBase.unregister(self)
	self.set_slot(self, self._unit, 0)

	return 
end
HuskTeamAIBase.pre_destroy = function (self, unit)
	self.remove_upgrades(self)
	unit.movement(unit):pre_destroy()
	unit.inventory(unit):pre_destroy(unit)
	TeamAIBase.unregister(self)
	UnitBase.pre_destroy(self, unit)

	return 
end
HuskTeamAIBase.load = function (self, data)
	self._tweak_table = data.base.tweak_table or self._tweak_table
	local character_name = self._tweak_table

	if character_name then
		local old_unit = managers.criminals:character_unit_by_name(character_name)

		if old_unit then
			local peer = managers.network:session():peer_by_unit(old_unit)

			if peer then
				managers.network:session():on_peer_lost(peer, peer.id(peer))
			end
		end

		local loadout = managers.blackmarket:unpack_henchman_loadout_string(data.base.loadout)

		managers.blackmarket:verfify_recived_crew_loadout(loadout, true)
		managers.criminals:add_character(character_name, self._unit, nil, true, loadout)
	end

	return 
end
HuskTeamAIBase.chk_freeze_anims = function (self)
	return 
end
HuskTeamAIBase.unregister = function (self)
	TeamAIBase.unregister(self)

	return 
end

return 
