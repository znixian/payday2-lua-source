AnimatedCamera = AnimatedCamera or class()
AnimatedCamera.init = function (self, unit)
	self._unit = unit

	return 
end
AnimatedCamera.update = function (self, unit, t, dt)
	return 
end
AnimatedCamera.set_position = function (self, pos)
	self._unit:set_position(pos)

	return 
end
AnimatedCamera.set_rotation = function (self, rot)
	self._unit:set_rotation(rot)

	return 
end
AnimatedCamera.position = function (self, pos)
	return self._unit:position()
end
AnimatedCamera.rotation = function (self, pos)
	return self._unit:rotation()
end
AnimatedCamera.play_redirect = function (self, redirect_name)
	local result = self._unit:play_redirect(redirect_name)

	return result ~= "" and result
end
AnimatedCamera.play_state = function (self, state_name)
	local result = self._unit:play_state(state_name)

	return result ~= "" and result
end
AnimatedCamera.destroy = function (self)
	return 
end

return 
