MissionAccessCamera = MissionAccessCamera or class()
MissionAccessCamera.init = function (self, unit)
	self._unit = unit
	self._camera = World:create_camera()
	self._default_fov = 57
	self._fov = self._default_fov

	self._camera:set_fov(self._default_fov)
	self._camera:set_near_range(15)
	self._camera:set_far_range(250000)

	local scale_x = 1
	local scale_y = 1

	if _G.IS_VR then
		local resolution = VRManager:target_resolution()
		scale_x = resolution.x/1280
		scale_y = resolution.y/720
	end

	self._viewport = managers.viewport:new_vp(0, 0, scale_x, scale_y, "MissionAccessCamera", CoreManagerBase.PRIO_WORLDCAMERA)
	self._director = self._viewport:director()
	self._shaker = self._director:shaker()
	self._camera_controller = self._director:make_camera(self._camera, Idstring("previs_camera"))

	self._viewport:set_camera(self._camera)
	self._director:set_camera(self._camera_controller)
	self._director:position_as(self._camera)
	self._camera_controller:set_both(self._unit)

	if _G.IS_VR then
		self._camera:set_stereo(false)
		self._viewport:set_render_params("World", self._viewport:vp(), managers.menu._render_target)
		self._viewport:set_enable_adaptive_quality(false)
		self._viewport:vp():set_post_processor_effect("World", Idstring("depth_projection"), Idstring("render_backbuffer_to_target"))
		managers.viewport:move_to_front(self._viewport)
	end

	return 
end
MissionAccessCamera._setup_sound_listener = function (self)
	self._listener_id = managers.listener:add_listener("access_camera", self._camera, self._camera, nil, false)

	managers.listener:add_set("access_camera", {
		"access_camera"
	})

	self._listener_activation_id = managers.listener:activate_set("main", "access_camera")
	self._sound_check_object = managers.sound_environment:add_check_object({
		primary = true,
		active = true,
		object = self._unit:orientation_object()
	})

	return 
end
MissionAccessCamera.set_rotation = function (self, rotation)
	self._original_rotation = rotation

	self._unit:set_rotation(rotation)

	return 
end
MissionAccessCamera.get_original_rotation = function (self)
	return self._original_rotation
end
MissionAccessCamera.get_offset_rotation = function (self)
	return self._offset_rotation
end
MissionAccessCamera.start = function (self, time)
	self._playing = true

	self._unit:anim_stop(Idstring("camera_animation"))

	self._fov = self._default_fov

	self._viewport:set_active(true)

	return 
end
MissionAccessCamera.stop = function (self)
	self._viewport:set_active(false)
	self._unit:anim_stop(Idstring("camera_animation"))
	self._unit:anim_set_time(Idstring("camera_animation"), 0)

	self._playing = false

	return 
end
MissionAccessCamera.set_destroyed = function (self, destroyed)
	return 
end
MissionAccessCamera.modify_fov = function (self, fov)
	self._fov = math.clamp(self._fov + fov, 25, 75)

	self._camera:set_fov(self._fov)

	return 
end
MissionAccessCamera.zoomed_value = function (self)
	return self._fov/self._default_fov
end
MissionAccessCamera.set_offset_rotation = function (self, yaw, pitch, roll)
	self._offset_rotation = self._offset_rotation or Rotation()
	yaw = yaw + mrotation.yaw(self._original_rotation)
	pitch = pitch + mrotation.pitch(self._original_rotation)

	mrotation.set_yaw_pitch_roll(self._offset_rotation, yaw, pitch, roll)
	self._unit:set_rotation(self._offset_rotation)

	return 
end
MissionAccessCamera.destroy = function (self)
	if self._viewport then
		self._viewport:destroy()

		self._viewport = nil
	end

	if alive(self._camera) then
		World:delete_camera(self._camera)

		self._camera = nil
	end

	if self._listener_id then
		managers.sound_environment:remove_check_object(self._sound_check_object)
		managers.listener:remove_listener(self._listener_id)
		managers.listener:remove_set("access_camera")

		self._listener_id = nil
	end

	return 
end

return 
