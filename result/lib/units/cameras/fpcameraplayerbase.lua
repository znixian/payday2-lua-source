FPCameraPlayerBase = FPCameraPlayerBase or class(UnitBase)
FPCameraPlayerBase.IDS_EMPTY = Idstring("empty")
FPCameraPlayerBase.IDS_NOSTRING = Idstring("")
FPCameraPlayerBase.bipod_location = nil
FPCameraPlayerBase.camera_last_pos = nil
FPCameraPlayerBase.init = function (self, unit)
	UnitBase.init(self, unit, true)

	self._unit = unit

	unit.set_timer(unit, managers.player:player_timer())
	unit.set_animation_timer(unit, managers.player:player_timer())
	unit.set_approximate_orientation(unit, false)

	self._anims_enabled = true
	self._obj_eye = self._unit:orientation_object()
	self._weap_align = self._unit:get_object(Idstring("right_weapon_align"))
	self._camera_properties = {
		pitch = 0.5,
		spin = 0
	}
	self._output_data = {
		position = unit.position(unit),
		rotation = unit.rotation(unit)
	}
	self._head_stance = {
		translation = Vector3(),
		rotation = Rotation()
	}
	self._shoulder_stance = {
		translation = Vector3(),
		rotation = Rotation()
	}
	self._vel_overshot = {
		translation = Vector3(),
		rotation = Rotation(),
		last_yaw = 0,
		last_pitch = 0,
		target_yaw = 0,
		target_pitch = 0
	}
	self._aim_assist = {
		direction = Vector3(),
		distance = 0,
		mrotation = Rotation(),
		distance_to_aim_line = 0
	}
	self._aim_assist_sticky = {
		direction = Vector3(),
		distance = 0,
		mrotation = Rotation(),
		distance_to_aim_line = 0,
		is_sticky = true
	}
	self._fov = {
		fov = 75
	}
	self._input = {}
	self._tweak_data = tweak_data.input.gamepad
	self._camera_properties.look_speed_current = self._tweak_data.look_speed_standard
	self._camera_properties.look_speed_transition_timer = 0
	self._camera_properties.target_tilt = 0
	self._camera_properties.current_tilt = 0
	self._recoil_kick = {
		h = {}
	}
	self._episilon = 1e-05

	self.check_flashlight_enabled(self)
	self.load_fps_mask_units(self)

	if _G.IS_VR then
		self._hmd_tracking = true
		local rot = VRManager:hmd_rotation()
		self._base_rotation = self._output_data.rotation
		self._fadeout = {
			value = 0,
			fadein_speed = 0,
			effect = {
				blend_mode = "normal",
				fade_out = 0,
				play_paused = true,
				fade_in = 0,
				color = Color(0, 0, 0, 0),
				timer = TimerManager:main()
			},
			slotmask = managers.slot:get_mask("statics")
		}
		self._ghost_reset_timer_t = 0
	end

	return 
end
FPCameraPlayerBase.set_parent_unit = function (self, parent_unit)
	self._parent_unit = parent_unit
	self._parent_movement_ext = self._parent_unit:movement()

	parent_unit.base(parent_unit):add_destroy_listener("FPCameraPlayerBase", callback(self, self, "parent_destroyed_clbk"))

	local controller_type = self._parent_unit:base():controller():get_default_controller_id()

	if controller_type == "keyboard" then
		self._look_function = callback(self, self, "_pc_look_function")
		self._tweak_data.uses_keyboard = true
	elseif controller_type == "steampad" then
		self._look_function = callback(self, self, "_steampad_look_function")
		self._tweak_data.uses_keyboard = true
	else
		self._look_function = callback(self, self, "_gamepad_look_function_ctl")
		self._tweak_data.uses_keyboard = false
	end

	if _G.IS_VR then
		self._fadeout.effect_id = self._fadeout.effect_id or managers.overlay_effect:play_effect(self._fadeout.effect)
	end

	return 
end
FPCameraPlayerBase.parent_destroyed_clbk = function (self, parent_unit)
	self._unit:set_extension_update_enabled(Idstring("base"), false)
	self.set_slot(self, self._unit, 0)

	self._parent_unit = nil

	return 
end
FPCameraPlayerBase.reset_properties = function (self)
	self._camera_properties.spin = self._parent_unit:rotation():y():to_polar().spin

	return 
end
FPCameraPlayerBase.update = function (self, unit, t, dt)
	if self._tweak_data.aim_assist_use_sticky_aim then
		self._update_aim_assist_sticky(self, t, dt)
	end

	if _G.IS_VR and self._hmd_tracking and not self._block_input then
		self._output_data.rotation = self._base_rotation*VRManager:hmd_rotation()
	end

	if not _G.IS_VR then
		self._parent_unit:base():controller():get_input_axis_clbk("look", callback(self, self, "_update_rot"))
	end

	self._update_stance(self, t, dt)
	self._update_movement(self, t, dt)

	if managers.player:current_state() ~= "driving" then
		self._parent_unit:camera():set_position(self._output_data.position)
		self._parent_unit:camera():set_rotation(self._output_data.rotation)
	else
		self._set_camera_position_in_vehicle(self)
	end

	if _G.IS_VR then
		self._parent_unit:camera():update_transform()
	end

	if self._fov.dirty then
		self._parent_unit:camera():set_FOV(self._fov.fov)

		self._fov.dirty = nil
	end

	if alive(self._light) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if weapon then
			local object = weapon.get_object(weapon, Idstring("fire"))
			local pos = object.position(object) + object.rotation(object):y()*10 + object.rotation(object):x()*0 + object.rotation(object):z()*-2

			self._light:set_position(pos)
			self._light:set_rotation(Rotation(object.rotation(object):z(), object.rotation(object):x(), object.rotation(object):y()))
			World:effect_manager():move_rotate(self._light_effect, pos, Rotation(object.rotation(object):x(), -object.rotation(object):y(), -object.rotation(object):z()))
		end
	end

	return 
end
FPCameraPlayerBase.check_flashlight_enabled = function (self)
	if managers.game_play_central:flashlights_on_player_on() then
		if not alive(self._light) then
			self._light = World:create_light("spot|specular")

			self._light:set_spot_angle_end(45)
			self._light:set_far_range(1000)
		end

		if not self._light_effect then
			self._light_effect = World:effect_manager():spawn({
				effect = Idstring("effects/particles/weapons/flashlight/fp_flashlight"),
				position = self._unit:position(),
				rotation = Rotation()
			})
		end

		self._light:set_enable(true)
		World:effect_manager():set_hidden(self._light_effect, false)
	elseif alive(self._light) then
		self._light:set_enable(false)
		World:effect_manager():set_hidden(self._light_effect, true)
	end

	return 
end
FPCameraPlayerBase.start_shooting = function (self)
	self._recoil_kick.accumulated = self._recoil_kick.to_reduce or 0
	self._recoil_kick.to_reduce = nil
	self._recoil_kick.current = (self._recoil_kick.current and self._recoil_kick.current) or self._recoil_kick.accumulated or 0
	self._recoil_kick.h.accumulated = self._recoil_kick.h.to_reduce or 0
	self._recoil_kick.h.to_reduce = nil
	self._recoil_kick.h.current = (self._recoil_kick.h.current and self._recoil_kick.h.current) or self._recoil_kick.h.accumulated or 0

	return 
end
FPCameraPlayerBase.stop_shooting = function (self, wait)
	self._recoil_kick.to_reduce = self._recoil_kick.accumulated
	self._recoil_kick.h.to_reduce = self._recoil_kick.h.accumulated
	self._recoil_wait = wait or 0

	return 
end
FPCameraPlayerBase.break_recoil = function (self)
	self._recoil_kick.current = 0
	self._recoil_kick.h.current = 0
	self._recoil_kick.accumulated = 0
	self._recoil_kick.h.accumulated = 0

	self.stop_shooting(self)

	return 
end
FPCameraPlayerBase.recoil_kick = function (self, up, down, left, right)
	if math.abs(self._recoil_kick.accumulated) < 20 then
		local v = math.lerp(up, down, math.random())
		self._recoil_kick.accumulated = (self._recoil_kick.accumulated or 0) + v
	end

	local h = math.lerp(left, right, math.random())
	self._recoil_kick.h.accumulated = (self._recoil_kick.h.accumulated or 0) + h

	return 
end
local bezier_values = {
	0,
	0,
	1,
	1
}
FPCameraPlayerBase._update_stance = function (self, t, dt)
	if self._shoulder_stance.transition then
		local trans_data = self._shoulder_stance.transition
		local elapsed_t = t - trans_data.start_t

		if trans_data.duration < elapsed_t then
			mvector3.set(self._shoulder_stance.translation, trans_data.end_translation)

			self._shoulder_stance.rotation = trans_data.end_rotation
			self._shoulder_stance.transition = nil
		else
			local progress = elapsed_t/trans_data.duration
			local progress_smooth = math.bezier(bezier_values, progress)

			mvector3.lerp(self._shoulder_stance.translation, trans_data.start_translation, trans_data.end_translation, progress_smooth)

			self._shoulder_stance.rotation = trans_data.start_rotation:slerp(trans_data.end_rotation, progress_smooth)
		end
	end

	if self._head_stance.transition then
		local trans_data = self._head_stance.transition
		local elapsed_t = t - trans_data.start_t

		if trans_data.duration < elapsed_t then
			mvector3.set(self._head_stance.translation, trans_data.end_translation)

			self._head_stance.transition = nil
		else
			local progress = elapsed_t/trans_data.duration
			local progress_smooth = math.bezier(bezier_values, progress)

			mvector3.lerp(self._head_stance.translation, trans_data.start_translation, trans_data.end_translation, progress_smooth)
		end
	end

	if self._vel_overshot.transition then
		local trans_data = self._vel_overshot.transition
		local elapsed_t = t - trans_data.start_t

		if trans_data.duration < elapsed_t then
			self._vel_overshot.yaw_neg = trans_data.end_yaw_neg
			self._vel_overshot.yaw_pos = trans_data.end_yaw_pos
			self._vel_overshot.pitch_neg = trans_data.end_pitch_neg
			self._vel_overshot.pitch_pos = trans_data.end_pitch_pos

			mvector3.set(self._vel_overshot.pivot, trans_data.end_pivot)

			self._vel_overshot.transition = nil
		else
			local progress = elapsed_t/trans_data.duration
			local progress_smooth = math.bezier(bezier_values, progress)
			self._vel_overshot.yaw_neg = math.lerp(trans_data.start_yaw_neg, trans_data.end_yaw_neg, progress_smooth)
			self._vel_overshot.yaw_pos = math.lerp(trans_data.start_yaw_pos, trans_data.end_yaw_pos, progress_smooth)
			self._vel_overshot.pitch_neg = math.lerp(trans_data.start_pitch_neg, trans_data.end_pitch_neg, progress_smooth)
			self._vel_overshot.pitch_pos = math.lerp(trans_data.start_pitch_pos, trans_data.end_pitch_pos, progress_smooth)

			mvector3.lerp(self._vel_overshot.pivot, trans_data.start_pivot, trans_data.end_pivot, progress_smooth)
		end
	end

	self._calculate_soft_velocity_overshot(self, dt)

	if self._fov.transition then
		local trans_data = self._fov.transition
		local elapsed_t = t - trans_data.start_t

		if trans_data.duration < elapsed_t then
			self._fov.fov = trans_data.end_fov
			self._fov.transition = nil
		else
			local progress = elapsed_t/trans_data.duration
			local progress_smooth = math.max(math.min(math.bezier(bezier_values, progress), 1), 0)
			self._fov.fov = math.lerp(trans_data.start_fov, trans_data.end_fov, progress_smooth)
		end

		self._fov.dirty = true
	end

	return 
end
local mrot1 = Rotation()
local mrot2 = Rotation()
local mrot3 = Rotation()
local mrot4 = Rotation()
local mvec1 = Vector3()
local mvec2 = Vector3()
local mvec3 = Vector3()
local mvec4 = Vector3()
FPCameraPlayerBase._update_movement = function (self, t, dt)
	local data = self._camera_properties
	local new_head_pos = mvec2
	local new_shoulder_pos = mvec1
	local new_shoulder_rot = mrot1
	local new_head_rot = mrot2

	self._parent_unit:m_position(new_head_pos)

	if _G.IS_VR then
		local hmd_position = mvec1
		local mover_position = mvec3

		mvector3.set(mover_position, new_head_pos)
		mvector3.set(hmd_position, self._parent_movement_ext:hmd_position())
		mvector3.set(new_head_pos, self._parent_movement_ext:ghost_position())
		mvector3.set_x(hmd_position, 0)
		mvector3.set_y(hmd_position, 0)
		mvector3.add(new_head_pos, hmd_position)
		mvector3.add(mover_position, hmd_position)
		self._update_fadeout(self, mover_position, new_head_pos, t, dt)
		self._horizonatal_recoil_kick(self, t, dt)
		self._vertical_recoil_kick(self, t, dt)
	else
		mvector3.add(new_head_pos, self._head_stance.translation)

		local stick_input_x = 0
		local stick_input_y = 0
		local aim_assist_x, aim_assist_y = self._get_aim_assist(self, t, dt, self._tweak_data.aim_assist_snap_speed, self._aim_assist)
		stick_input_x = stick_input_x + self._horizonatal_recoil_kick(self, t, dt) + aim_assist_x
		stick_input_y = stick_input_y + self._vertical_recoil_kick(self, t, dt) + aim_assist_y
		local look_polar_spin = data.spin - stick_input_x
		local look_polar_pitch = math.clamp(data.pitch + stick_input_y, -85, 85)

		if not self._limits or not self._limits.spin then
			look_polar_spin = look_polar_spin%360
		end

		local look_polar = Polar(1, look_polar_pitch, look_polar_spin)
		local look_vec = look_polar.to_vector(look_polar)
		local cam_offset_rot = mrot3

		mrotation.set_look_at(cam_offset_rot, look_vec, math.UP)
		mrotation.set_zero(new_head_rot)
		mrotation.multiply(new_head_rot, self._head_stance.rotation)
		mrotation.multiply(new_head_rot, cam_offset_rot)

		data.pitch = look_polar_pitch
		data.spin = look_polar_spin
		self._output_data.rotation = new_head_rot or self._output_data.rotation

		if self._camera_properties.current_tilt ~= self._camera_properties.target_tilt then
			self._camera_properties.current_tilt = math.step(self._camera_properties.current_tilt, self._camera_properties.target_tilt, dt*150)
		end

		if self._camera_properties.current_tilt ~= 0 then
			self._output_data.rotation = Rotation(self._output_data.rotation:yaw(), self._output_data.rotation:pitch(), self._output_data.rotation:roll() + self._camera_properties.current_tilt)
		end
	end

	self._output_data.position = new_head_pos

	mvector3.set(new_shoulder_pos, self._shoulder_stance.translation)
	mvector3.add(new_shoulder_pos, self._vel_overshot.translation)
	mvector3.rotate_with(new_shoulder_pos, self._output_data.rotation)
	mvector3.add(new_shoulder_pos, new_head_pos)
	mrotation.set_zero(new_shoulder_rot)
	mrotation.multiply(new_shoulder_rot, self._output_data.rotation)
	mrotation.multiply(new_shoulder_rot, self._shoulder_stance.rotation)
	mrotation.multiply(new_shoulder_rot, self._vel_overshot.rotation)
	self.set_position(self, new_shoulder_pos)
	self.set_rotation(self, new_shoulder_rot)

	return 
end
local mvec1 = Vector3()
FPCameraPlayerBase._update_rot = function (self, axis, unscaled_axis)
	if self._animate_pitch then
		self.animate_pitch_upd(self)
	end

	local t = managers.player:player_timer():time()
	local dt = t - (self._last_rot_t or t)
	self._last_rot_t = t
	local data = self._camera_properties
	local new_head_pos = mvec2
	local new_shoulder_pos = mvec1
	local new_shoulder_rot = mrot1
	local new_head_rot = mrot2

	self._parent_unit:m_position(new_head_pos)
	mvector3.add(new_head_pos, self._head_stance.translation)

	self._input.look = axis
	self._input.look_multiplier = self._parent_unit:base():controller():get_setup():get_connection("look"):get_multiplier()
	local stick_input_x, stick_input_y = self._look_function(axis, self._input.look_multiplier, dt, unscaled_axis)
	local look_polar_spin = data.spin - stick_input_x
	local look_polar_pitch = math.clamp(data.pitch + stick_input_y, -85, 85)
	local player_state = managers.player:current_state()

	if self._limits then
		if self._limits.spin then
			local d = (look_polar_spin - self._limits.spin.mid)/self._limits.spin.offset
			d = math.clamp(d, -1, 1)
			look_polar_spin = data.spin - math.lerp(stick_input_x, 0, math.abs(d))
		end

		if self._limits.pitch then
			local d = math.abs((look_polar_pitch - self._limits.pitch.mid)/self._limits.pitch.offset)
			d = math.clamp(d, -1, 1)
			look_polar_pitch = data.pitch + math.lerp(stick_input_y, 0, math.abs(d))
			look_polar_pitch = math.clamp(look_polar_pitch, -85, 85)
		end
	end

	if not self._limits or not self._limits.spin then
		look_polar_spin = look_polar_spin%360
	end

	local look_polar = Polar(1, look_polar_pitch, look_polar_spin)
	local look_vec = look_polar.to_vector(look_polar)
	local cam_offset_rot = mrot3

	mrotation.set_look_at(cam_offset_rot, look_vec, math.UP)

	if self._animate_pitch == nil then
		mrotation.set_zero(new_head_rot)
		mrotation.multiply(new_head_rot, self._head_stance.rotation)
		mrotation.multiply(new_head_rot, cam_offset_rot)

		data.pitch = look_polar_pitch
		data.spin = look_polar_spin
	end

	self._output_data.position = new_head_pos

	if self._p_exit then
		self._p_exit = false
		self._output_data.rotation = self._parent_unit:movement().fall_rotation

		mrotation.multiply(self._output_data.rotation, self._parent_unit:camera():rotation())

		data.spin = self._output_data.rotation:y():to_polar().spin
	else
		self._output_data.rotation = new_head_rot or self._output_data.rotation
	end

	if self._camera_properties.current_tilt ~= self._camera_properties.target_tilt then
		self._camera_properties.current_tilt = math.step(self._camera_properties.current_tilt, self._camera_properties.target_tilt, dt*150)
	end

	if self._camera_properties.current_tilt ~= 0 then
		self._output_data.rotation = Rotation(self._output_data.rotation:yaw(), self._output_data.rotation:pitch(), self._output_data.rotation:roll() + self._camera_properties.current_tilt)
	end

	local equipped_weapon = self._parent_unit:inventory():equipped_unit()
	local bipod_weapon_translation = Vector3(0, 0, 0)

	if equipped_weapon and equipped_weapon.base(equipped_weapon) then
		local weapon_tweak_data = equipped_weapon.base(equipped_weapon):weapon_tweak_data()

		if weapon_tweak_data and weapon_tweak_data.bipod_weapon_translation then
			bipod_weapon_translation = weapon_tweak_data.bipod_weapon_translation
		end
	end

	local bipod_pos = Vector3(0, 0, 0)
	local bipod_rot = new_shoulder_rot

	mvector3.set(bipod_pos, bipod_weapon_translation)
	mvector3.rotate_with(bipod_pos, self._output_data.rotation)
	mvector3.add(bipod_pos, new_head_pos)
	mvector3.set(new_shoulder_pos, self._shoulder_stance.translation)
	mvector3.add(new_shoulder_pos, self._vel_overshot.translation)
	mvector3.rotate_with(new_shoulder_pos, self._output_data.rotation)
	mvector3.add(new_shoulder_pos, new_head_pos)
	mrotation.set_zero(new_shoulder_rot)
	mrotation.multiply(new_shoulder_rot, self._output_data.rotation)
	mrotation.multiply(new_shoulder_rot, self._shoulder_stance.rotation)
	mrotation.multiply(new_shoulder_rot, self._vel_overshot.rotation)

	if player_state == "driving" then
		self._set_camera_position_in_vehicle(self)
	elseif player_state == "jerry1" or player_state == "jerry2" then
		mrotation.set_zero(cam_offset_rot)
		mrotation.multiply(cam_offset_rot, self._parent_unit:movement().fall_rotation)
		mrotation.multiply(cam_offset_rot, self._output_data.rotation)

		local shoulder_pos = mvec3
		local shoulder_rot = mrot4

		mrotation.set_zero(shoulder_rot)
		mrotation.multiply(shoulder_rot, cam_offset_rot)
		mrotation.multiply(shoulder_rot, self._shoulder_stance.rotation)
		mrotation.multiply(shoulder_rot, self._vel_overshot.rotation)
		mvector3.set(shoulder_pos, self._shoulder_stance.translation)
		mvector3.add(shoulder_pos, self._vel_overshot.translation)
		mvector3.rotate_with(shoulder_pos, cam_offset_rot)
		mvector3.add(shoulder_pos, self._parent_unit:position())
		self.set_position(self, shoulder_pos)
		self.set_rotation(self, shoulder_rot)
		self._parent_unit:camera():set_position(self._parent_unit:position())
		self._parent_unit:camera():set_rotation(cam_offset_rot)
	else
		self.set_position(self, new_shoulder_pos)
		self.set_rotation(self, new_shoulder_rot)
		self._parent_unit:camera():set_position(self._output_data.position)
		self._parent_unit:camera():set_rotation(self._output_data.rotation)
	end

	if player_state == "bipod" and not self._parent_unit:movement()._current_state:in_steelsight() then
		self.set_position(self, PlayerBipod._shoulder_pos or new_shoulder_pos)
		self.set_rotation(self, bipod_rot)
		self._parent_unit:camera():set_position(PlayerBipod._camera_pos or self._output_data.position)
	elseif not self._parent_unit:movement()._current_state:in_steelsight() then
		PlayerBipod:set_camera_positions(bipod_pos, self._output_data.position)
	end

	return 
end
FPCameraPlayerBase._set_camera_position_in_vehicle = function (self)
	local vehicle_data = managers.player:get_vehicle()
	local vehicle_unit = vehicle_data.vehicle_unit
	local vehicle = vehicle_unit.vehicle(vehicle_unit)
	local vehicle_ext = vehicle_unit.vehicle_driving(vehicle_unit)
	local seat = vehicle_ext.find_seat_for_player(vehicle_ext, managers.player:player_unit())
	local obj_pos, obj_rot = vehicle_ext.get_object_placement(vehicle_ext, managers.player:local_player())

	if obj_pos == nil or obj_rot == nil then
		return 
	end

	local stance = managers.player:local_player():movement():current_state():stance()

	if stance == PlayerDriving.STANCE_SHOOTING then
		mvector3.add(obj_pos, seat.shooting_pos:rotate_with(vehicle.rotation(vehicle)))
	end

	local camera_rot = mrot3

	mrotation.set_zero(camera_rot)
	mrotation.multiply(camera_rot, obj_rot)
	mrotation.multiply(camera_rot, self._output_data.rotation)

	local hands_rot = mrot4

	mrotation.set_zero(hands_rot)
	mrotation.multiply(hands_rot, obj_rot)

	local target = Vector3(0, 0, 145)
	local target_camera = Vector3(0, 0, 145)

	if vehicle_ext._tweak_data.driver_camera_offset then
		target_camera = target_camera + vehicle_ext._tweak_data.driver_camera_offset
	end

	mvector3.rotate_with(target, vehicle.rotation(vehicle))
	mvector3.rotate_with(target_camera, vehicle.rotation(vehicle))

	local pos = obj_pos + target
	local camera_pos = obj_pos + target_camera

	if _G.IS_VR then
		local rot = Rotation(obj_rot.yaw(obj_rot) - self._initial_hmd_rotation:yaw() - self.base_rotation(self):yaw(), 0, 0)

		mvector3.set(camera_pos, self._parent_movement_ext:ghost_position())
		mvector3.add(camera_pos, Vector3(0, 0, self._parent_movement_ext:hmd_position().z))
		mrotation.set_zero(camera_rot)
		mrotation.multiply(camera_rot, rot)
		mrotation.multiply(camera_rot, self._output_data.rotation)
	end

	if _G.IS_VR or seat.driving then
		if vehicle_unit.camera(vehicle_unit) then
			vehicle_unit.camera(vehicle_unit):update_camera()
		end

		self.set_position(self, pos)
		self.set_rotation(self, hands_rot)
		self._parent_unit:camera():set_position(camera_pos)
		self._parent_unit:camera():set_rotation(camera_rot)
	else
		local shoulder_pos = mvec3
		local shoulder_rot = mrot4

		mrotation.set_zero(shoulder_rot)
		mrotation.multiply(shoulder_rot, camera_rot)
		mrotation.multiply(shoulder_rot, self._shoulder_stance.rotation)
		mrotation.multiply(shoulder_rot, self._vel_overshot.rotation)
		mvector3.set(shoulder_pos, self._shoulder_stance.translation)
		mvector3.add(shoulder_pos, self._vel_overshot.translation)
		mvector3.rotate_with(shoulder_pos, camera_rot)
		mvector3.add(shoulder_pos, pos)
		self.set_position(self, shoulder_pos)
		self.set_rotation(self, shoulder_rot)
		self._parent_unit:camera():set_position(pos)
		self._parent_unit:camera():set_rotation(camera_rot)
	end

	return 
end
FPCameraPlayerBase._get_aim_assist = function (self, t, dt, speed, aim_data)
	if aim_data.distance == 0 then
		return 0, 0
	end

	local s_value = math.step(0, aim_data.distance, speed*dt)
	local r_value_x = mvector3.x(aim_data.direction)*s_value
	local r_value_y = mvector3.y(aim_data.direction)*s_value

	if aim_data.is_sticky and self._tweak_data.aim_assist_use_sticky_aim then
		local strength = math.min(1, (aim_data.distance_to_aim_line or 0)/100) - 1
		local mx = self._tweak_data.aim_assist_gradient_max - 1
		local mn = self._tweak_data.aim_assist_gradient_min - 1
		local min_strength = math.lerp(mn, mx, math.min(1, (aim_data.target_distance or 0)/self._tweak_data.aim_assist_gradient_max_distance))
		strength = math.max(0, strength - min_strength)/(min_strength - 1)
		r_value_x = r_value_x*strength
		r_value_y = r_value_y*strength
	end

	aim_data.distance = aim_data.distance - s_value

	if aim_data.distance <= 0 then
		self._stop_aim_assist(self, aim_data)
	end

	return r_value_x, r_value_y
end
FPCameraPlayerBase._vertical_recoil_kick = function (self, t, dt)
	local player_state = managers.player:current_state()

	if player_state == "bipod" then
		self.break_recoil(self)

		return 0
	end

	local r_value = 0

	if self._recoil_kick.current and self._episilon < self._recoil_kick.accumulated - self._recoil_kick.current then
		local n = math.step(self._recoil_kick.current, self._recoil_kick.accumulated, dt*40)
		r_value = n - self._recoil_kick.current
		self._recoil_kick.current = n
	elseif self._recoil_wait then
		self._recoil_wait = self._recoil_wait - dt

		if self._recoil_wait < 0 then
			self._recoil_wait = nil
		end
	elseif self._recoil_kick.to_reduce then
		self._recoil_kick.current = nil
		local n = math.lerp(self._recoil_kick.to_reduce, 0, dt*9)
		r_value = -(self._recoil_kick.to_reduce - n)
		self._recoil_kick.to_reduce = n

		if self._recoil_kick.to_reduce == 0 then
			self._recoil_kick.to_reduce = nil
		end
	end

	return r_value
end
FPCameraPlayerBase._horizonatal_recoil_kick = function (self, t, dt)
	local player_state = managers.player:current_state()

	if player_state == "bipod" then
		return 0
	end

	local r_value = 0

	if self._recoil_kick.h.current and self._episilon < math.abs(self._recoil_kick.h.accumulated - self._recoil_kick.h.current) then
		local n = math.step(self._recoil_kick.h.current, self._recoil_kick.h.accumulated, dt*40)
		r_value = n - self._recoil_kick.h.current
		self._recoil_kick.h.current = n
	elseif self._recoil_wait then
		self._recoil_wait = self._recoil_wait - dt

		if self._recoil_wait < 0 then
			self._recoil_wait = nil
		end
	elseif self._recoil_kick.h.to_reduce then
		self._recoil_kick.h.current = nil
		local n = math.lerp(self._recoil_kick.h.to_reduce, 0, dt*5)
		r_value = -(self._recoil_kick.h.to_reduce - n)
		self._recoil_kick.h.to_reduce = n

		if self._recoil_kick.h.to_reduce == 0 then
			self._recoil_kick.h.to_reduce = nil
		end
	end

	return r_value
end
FPCameraPlayerBase._pc_look_function = function (self, stick_input, stick_input_multiplier, dt)
	return stick_input.x, stick_input.y
end
FPCameraPlayerBase._gamepad_look_function = function (self, stick_input, stick_input_multiplier, dt)
	if self._tweak_data.look_speed_dead_zone*stick_input_multiplier.x < mvector3.length(stick_input) then
		local x = stick_input.x
		local y = stick_input.y
		stick_input = Vector3(x/((math.abs(y) - 1)*0.3 - 1.3), y/((math.abs(x) - 1)*0.3 - 1.3), 0)
		local look_speed = self._get_look_speed(self, stick_input, stick_input_multiplier, dt)
		local stick_input_x = stick_input.x*dt*look_speed
		local stick_input_y = stick_input.y*dt*look_speed

		return stick_input_x, stick_input_y
	end

	return 0, 0
end
local multiplier = Vector3()
FPCameraPlayerBase._gamepad_look_function_ctl = function (self, stick_input, stick_input_multiplier, dt, unscaled_stick_input)
	local aim_assist_x = 0
	local aim_assist_y = 0
	local cs = managers.player:current_state()
	local aim_assist = false

	if (cs == "standard" or cs == "carry" or cs == "bipod") and managers.controller:get_default_wrapper_type() ~= "pc" and managers.user:get_setting("sticky_aim") then
		aim_assist = true
	end

	local dz = self._tweak_data.look_speed_dead_zone
	local length = mvector3.length(unscaled_stick_input)

	if dz < length then
		mvector3.set(multiplier, stick_input_multiplier)

		if 0.001 < multiplier.x - 1 then
			mvector3.set_x(multiplier, ((multiplier.x - 1)*1.6)/((tweak_data.player.camera.MAX_SENSITIVITY - tweak_data.player.camera.MIN_SENSITIVITY)*0.5) + 1)
		end

		if 0.001 < multiplier.y - 1 then
			mvector3.set_y(multiplier, ((multiplier.y - 1)*1.6)/((tweak_data.player.camera.MAX_SENSITIVITY - tweak_data.player.camera.MIN_SENSITIVITY)*0.5) + 1)
		end

		if aim_assist then
			aim_assist_x, aim_assist_y = self._get_aim_assist(self, 0, dt, self._tweak_data.aim_assist_look_speed, self._aim_assist_sticky)
		end

		local x = unscaled_stick_input.x
		local y = unscaled_stick_input.y
		x = x/length
		y = y/length
		length = math.min(length, 1)
		local scale = (length - dz)/(dz - 1)
		x = x*scale
		y = y*scale
		unscaled_stick_input = Vector3(x, y, 0)
		local look_speed_x, look_speed_y = self._get_look_speed_ctl(self, unscaled_stick_input, multiplier, dt)
		look_speed_y = look_speed_x
		look_speed_x = look_speed_x*multiplier.x
		look_speed_y = look_speed_y*multiplier.y
		local stick_input_x = unscaled_stick_input.x*dt*look_speed_x
		local stick_input_y = unscaled_stick_input.y*dt*look_speed_y
		local look = Vector3(stick_input_x, stick_input_y, 0)

		if aim_assist then
			local len = mvector3.length(look)
			look = Vector3(look.x + aim_assist_x, look.y, 0)

			if length < 0.08 then
				mvector3.normalize(look)

				look = look*len
			end
		end

		return look.x, look.y
	end

	if aim_assist then
		aim_assist_x, aim_assist_y = self._get_aim_assist(self, 0, dt, self._tweak_data.aim_assist_move_speed, self._aim_assist_sticky)
		local move = math.abs(self._parent_unit:movement()._current_state._stick_move.x)

		if self._tweak_data.aim_assist_move_th_min <= move and move <= self._tweak_data.aim_assist_move_th_max then
			return aim_assist_x, 0
		end
	end

	return 0, 0
end
FPCameraPlayerBase._steampad_look_function = function (self, stick_input, stick_input_multiplier, dt)
	if self._tweak_data.look_speed_dead_zone*stick_input_multiplier.x < mvector3.length(stick_input) then
		local x = stick_input.x
		local y = stick_input.y
		local look_speed = self._tweak_data.look_speed_standard*((alive(self._parent_unit) and self._parent_unit:base():controller():get_input_bool("change_sensitivity") and 1) or 0.5)
		local stick_input_x = x*dt*look_speed
		local stick_input_y = y*dt*look_speed

		return stick_input_x, stick_input_y
	end

	return 0, 0
end
FPCameraPlayerBase._get_look_speed = function (self, stick_input, stick_input_multiplier, dt)
	if self._parent_unit:movement()._current_state:in_steelsight() then
		return self._tweak_data.look_speed_steel_sight
	end

	if self._tweak_data.look_speed_transition_occluder*stick_input_multiplier.x >= mvector3.length(stick_input) or self._tweak_data.look_speed_transition_zone*stick_input_multiplier.x >= math.abs(stick_input.x) then
		self._camera_properties.look_speed_transition_timer = 0

		return self._tweak_data.look_speed_standard
	end

	if 1 <= self._camera_properties.look_speed_transition_timer then
		return self._tweak_data.look_speed_fast
	end

	local p1 = self._tweak_data.look_speed_standard
	local p2 = self._tweak_data.look_speed_standard
	local p3 = self._tweak_data.look_speed_standard + (self._tweak_data.look_speed_fast - self._tweak_data.look_speed_standard)/3*2
	local p4 = self._tweak_data.look_speed_fast
	self._camera_properties.look_speed_transition_timer = self._camera_properties.look_speed_transition_timer + dt/self._tweak_data.look_speed_transition_to_fast

	return math.bezier({
		p1,
		p2,
		p3,
		p4
	}, self._camera_properties.look_speed_transition_timer)
end

local function get_look_setting(a, b, c, t)
	if t < 0.5 then
		return math.lerp(a, b, t/0.5)
	end

	return math.lerp(b, c, (t - 0.5)/0.5)
end

local function get_look_setting_x_y(a, b, c, x, y)
	return get_look_setting(a, b, c, x), get_look_setting(a, b, c, y)
end

FPCameraPlayerBase._get_look_speed_ctl = function (self, stick_input, stick_input_multiplier, dt)
	if self._parent_unit:movement()._current_state:in_steelsight() then
		return self._tweak_data.look_speed_steel_sight
	end

	if self._tweak_data.look_speed_transition_occluder >= mvector3.length(stick_input) or self._tweak_data.look_speed_transition_zone >= math.abs(stick_input.x) then
		self._camera_properties.look_speed_transition_timer = 0

		return self._tweak_data.look_speed_standard
	end

	if 1 <= self._camera_properties.look_speed_transition_timer then
		return self._tweak_data.look_speed_fast
	end

	local p1 = self._tweak_data.look_speed_standard
	local p2 = self._tweak_data.look_speed_standard
	local p3 = self._tweak_data.look_speed_standard + (self._tweak_data.look_speed_fast - self._tweak_data.look_speed_standard)/3*2
	local p4 = self._tweak_data.look_speed_fast
	self._camera_properties.look_speed_transition_timer = self._camera_properties.look_speed_transition_timer + dt/self._tweak_data.look_speed_transition_to_fast

	return math.bezier({
		p1,
		p2,
		p3,
		p4
	}, self._camera_properties.look_speed_transition_timer)
end
FPCameraPlayerBase._calculate_soft_velocity_overshot = function (self, dt)
	local stick_input = self._input.look
	local vel_overshot = self._vel_overshot

	if not stick_input then
		return 
	end

	local input_yaw, input_pitch, input_x, input_z = nil
	local mul = (self._tweak_data.uses_keyboard and dt/0.002) or 0.4

	if 0 <= stick_input.x then
		local stick_input_x = math.pow(math.abs(math.clamp(mul*stick_input.x, 0, 1)), 1.5)*math.sign(stick_input.x)
		input_yaw = stick_input_x*vel_overshot.yaw_pos
	else
		local stick_input_x = math.pow(math.abs(math.clamp(mul*stick_input.x, -1, 0)), 1.5)
		input_yaw = stick_input_x*vel_overshot.yaw_neg
	end

	local last_yaw = vel_overshot.last_yaw
	local sign_in_yaw = math.sign(input_yaw)
	local abs_in_yaw = math.abs(input_yaw)
	local sign_last_yaw = math.sign(last_yaw)
	local abs_last_yaw = math.abs(last_yaw)
	local step_v = (self._tweak_data.uses_keyboard and dt*120) or 2
	vel_overshot.target_yaw = math.step(vel_overshot.target_yaw, input_yaw, step_v)
	local final_yaw = nil
	local diff = math.abs(vel_overshot.target_yaw - last_yaw)
	local diff_clamp = 40
	local diff_ratio = math.pow(diff/diff_clamp, 1)
	local diff_ratio_clamped = math.clamp(diff_ratio, 0, 1)
	local step_amount = math.lerp(3, 180, diff_ratio_clamped)*dt
	final_yaw = math.step(last_yaw, vel_overshot.target_yaw, step_amount)
	vel_overshot.last_yaw = final_yaw
	local mul = (self._tweak_data.uses_keyboard and dt/0.002) or 0.4

	if 0 <= stick_input.y then
		local stick_input_y = math.pow(math.abs(math.clamp(mul*stick_input.y, 0, 1)), 1.5)*math.sign(stick_input.y)
		input_pitch = stick_input_y*vel_overshot.pitch_pos
	else
		local stick_input_y = math.pow(math.abs(math.clamp(mul*stick_input.y, -1, 0)), 1.5)
		input_pitch = stick_input_y*vel_overshot.pitch_neg
	end

	local last_pitch = vel_overshot.last_pitch
	local sign_in_pitch = math.sign(input_pitch)
	local abs_in_pitch = math.abs(input_pitch)
	local sign_last_pitch = math.sign(last_pitch)
	local abs_last_pitch = math.abs(last_pitch)
	local step_v = (self._tweak_data.uses_keyboard and dt*120) or 2
	vel_overshot.target_pitch = math.step(vel_overshot.target_pitch, input_pitch, step_v)
	local final_pitch = nil
	local diff = math.abs(vel_overshot.target_pitch - last_pitch)
	local diff_clamp = 40
	local diff_ratio = math.pow(diff/diff_clamp, 1)
	local diff_ratio_clamped = math.clamp(diff_ratio, 0, 1)
	local step_amount = math.lerp(3, 180, diff_ratio_clamped)*dt
	final_pitch = math.step(last_pitch, vel_overshot.target_pitch, step_amount)
	vel_overshot.last_pitch = final_pitch

	mrotation.set_yaw_pitch_roll(vel_overshot.rotation, final_yaw, final_pitch, -final_yaw)

	local pivot = vel_overshot.pivot
	local new_root = mvec3

	mvector3.set(new_root, pivot)
	mvector3.negate(new_root)
	mvector3.rotate_with(new_root, vel_overshot.rotation)
	mvector3.add(new_root, pivot)
	mvector3.set(vel_overshot.translation, new_root)

	return 
end
FPCameraPlayerBase.set_position = function (self, pos)
	self._unit:set_position(pos)

	return 
end
FPCameraPlayerBase.set_rotation = function (self, rot)
	self._unit:set_rotation(rot)

	return 
end
FPCameraPlayerBase.eye_position = function (self)
	return self._obj_eye:position()
end
FPCameraPlayerBase.eye_rotation = function (self)
	return self._obj_eye:rotation()
end
FPCameraPlayerBase.play_redirect = function (self, redirect_name, speed, offset_time)
	self.set_anims_enabled(self, true)

	self._anim_empty_state_wanted = false
	local result = self._unit:play_redirect(redirect_name, offset_time)

	if result == self.IDS_NOSTRING then
		return false
	end

	if speed then
		self._unit:anim_state_machine():set_speed(result, speed)
	end

	return result
end
FPCameraPlayerBase.play_redirect_timeblend = function (self, state, redirect_name, offset_time, t)
	self.set_anims_enabled(self, true)

	self._anim_empty_state_wanted = false

	self._unit:anim_state_machine():set_parameter(state, "t", t)

	local result = self._unit:play_redirect(redirect_name, offset_time)

	if result == self.IDS_NOSTRING then
		return false
	end

	return result
end
FPCameraPlayerBase.play_raw = function (self, name, params)
	self.set_anims_enabled(self, true)

	self._anim_empty_state_wanted = false
	local asm = self._unit:anim_state_machine()
	local result = asm.play_raw(asm, name, params)

	return result ~= self.IDS_NOSTRING and result
end
FPCameraPlayerBase.set_steelsight_anim_enabled = function (self, enabled)
	self._steelsight_anims_enabled = enabled

	self._check_play_empty_state(self)

	return 
end
FPCameraPlayerBase.play_state = function (self, state_name)
	self.set_anims_enabled(self, true)

	self._anim_empty_state_wanted = false
	local result = self._unit:play_state(Idstring(state_name))

	return result ~= self.IDS_NOSTRING and result
end
FPCameraPlayerBase.set_target_tilt = function (self, tilt)
	self._camera_properties.target_tilt = tilt

	return 
end
FPCameraPlayerBase.set_lean_values = function (self, lean_position, lean_rotation)
	self._camera_properties.lean_position = lean_position
	self._camera_properties.lean_rotation = lean_rotation

	return 
end
FPCameraPlayerBase.set_camera_offset = function (self, camera_offset)
	self._camera_properties.camera_offset = camera_offset

	return 
end
FPCameraPlayerBase.set_stance_instant = function (self, stance_name)
	local new_stance = tweak_data.player.stances.default[stance_name].shoulders

	if new_stance then
		self._shoulder_stance.transition = nil
		self._shoulder_stance.translation = mvector3.copy(new_stance.translation)
		self._shoulder_stance.rotation = new_stance.rotation
	end

	local new_stance = tweak_data.player.stances.default[stance_name].head

	if new_stance then
		self._head_stance.transition = nil
		self._head_stance.translation = mvector3.copy(new_stance.translation)
		self._head_stance.rotation = new_stance.rotation
	end

	local new_overshot = tweak_data.player.stances.default[stance_name].vel_overshot

	if new_overshot then
		self._vel_overshot.transition = nil
		self._vel_overshot.yaw_neg = new_overshot.yaw_neg
		self._vel_overshot.yaw_pos = new_overshot.yaw_pos
		self._vel_overshot.pitch_neg = new_overshot.pitch_neg
		self._vel_overshot.pitch_pos = new_overshot.pitch_pos
		self._vel_overshot.pivot = mvector3.copy(new_overshot.pivot)
	end

	self.set_stance_fov_instant(self, stance_name)

	return 
end
FPCameraPlayerBase.is_stance_done = function (self)
	return not self._shoulder_stance.transition and not self._head_stance.transition and not self._vel_overshot.transition
end
FPCameraPlayerBase.set_fov_instant = function (self, new_fov)
	if new_fov then
		self._fov.transition = nil
		self._fov.fov = new_fov
		self._fov.dirty = true

		if Application:paused() then
			self._parent_unit:camera():set_FOV(self._fov.fov)
		end
	end

	return 
end
FPCameraPlayerBase.set_stance_fov_instant = function (self, stance_name)
	local new_fov = (tweak_data.player.stances.default[stance_name].zoom_fov and managers.user:get_setting("fov_zoom")) or managers.user:get_setting("fov_standard")

	if new_fov then
		self._fov.transition = nil
		self._fov.fov = new_fov
		self._fov.dirty = true

		if Application:paused() then
			self._parent_unit:camera():set_FOV(self._fov.fov)
		end
	end

	return 
end
FPCameraPlayerBase.clbk_stance_entered = function (self, new_shoulder_stance, new_head_stance, new_vel_overshot, new_fov, new_shakers, stance_mod, duration_multiplier, duration)
	local t = managers.player:player_timer():time()

	if new_shoulder_stance then
		local transition = {}
		self._shoulder_stance.transition = transition
		transition.end_translation = new_shoulder_stance.translation + (stance_mod.translation or Vector3())
		transition.end_rotation = new_shoulder_stance.rotation*(stance_mod.rotation or Rotation())
		transition.start_translation = mvector3.copy(self._shoulder_stance.translation)
		transition.start_rotation = self._shoulder_stance.rotation
		transition.start_t = t
		transition.duration = duration*duration_multiplier
	end

	if new_head_stance then
		local transition = {}
		self._head_stance.transition = transition
		transition.end_translation = new_head_stance.translation
		transition.end_rotation = new_head_stance.rotation
		transition.start_translation = mvector3.copy(self._head_stance.translation)
		transition.start_rotation = self._head_stance.rotation
		transition.start_t = t
		transition.duration = duration*duration_multiplier
	end

	if new_vel_overshot then
		local transition = {}
		self._vel_overshot.transition = transition
		transition.end_pivot = new_vel_overshot.pivot
		transition.end_yaw_neg = new_vel_overshot.yaw_neg
		transition.end_yaw_pos = new_vel_overshot.yaw_pos
		transition.end_pitch_neg = new_vel_overshot.pitch_neg
		transition.end_pitch_pos = new_vel_overshot.pitch_pos
		transition.start_pivot = mvector3.copy(self._vel_overshot.pivot)
		transition.start_yaw_neg = self._vel_overshot.yaw_neg
		transition.start_yaw_pos = self._vel_overshot.yaw_pos
		transition.start_pitch_neg = self._vel_overshot.pitch_neg
		transition.start_pitch_pos = self._vel_overshot.pitch_pos
		transition.start_t = t
		transition.duration = duration*duration_multiplier
	end

	if new_fov then
		if new_fov == self._fov.fov then
			self._fov.transition = nil
		else
			local transition = {}
			self._fov.transition = transition
			transition.end_fov = new_fov
			transition.start_fov = self._fov.fov
			transition.start_t = t
			transition.duration = duration*duration_multiplier
		end
	end

	if new_shakers then
		for effect, values in pairs(new_shakers) do
			for parameter, value in pairs(values) do
				self._parent_unit:camera():set_shaker_parameter(effect, parameter, value)
			end
		end
	end

	return 
end
FPCameraPlayerBase._start_aim_assist = function (self, col_ray, aim_data)
	if col_ray then
		local ray = col_ray.ray
		local r1 = self._parent_unit:camera():rotation()
		local r2 = aim_data.mrotation or Rotation()

		mrotation.set_look_at(r2, ray, r1.z(r1))

		local yaw = mrotation.yaw(r1) - mrotation.yaw(r2)
		local pitch = mrotation.pitch(r1) - mrotation.pitch(r2)

		if 180 < yaw then
			yaw = yaw - 360
		elseif yaw < -180 then
			yaw = yaw + 360
		end

		if 180 < pitch then
			pitch = pitch - 360
		elseif pitch < -180 then
			pitch = pitch + 360
		end

		mvector3.set_static(aim_data.direction, yaw, -pitch, 0)

		aim_data.distance = mvector3.normalize(aim_data.direction)
		aim_data.target_distance = col_ray.distance
		aim_data.distance_to_aim_line = col_ray.distance_to_aim_line
	end

	return 
end
FPCameraPlayerBase._stop_aim_assist = function (self, aim_data)
	mvector3.set_static(aim_data.direction, 0, 0, 0)

	aim_data.distance = 0
	aim_data.target_distance = 0
	aim_data.distance_to_aim_line = 0

	return 
end
FPCameraPlayerBase._update_aim_assist_sticky = function (self, t, dt)
	if managers.controller:get_default_wrapper_type() ~= "pc" and managers.user:get_setting("sticky_aim") then
		local weapon = self._parent_unit:inventory():equipped_unit()
		local player_state = self._parent_unit:movement():current_state()

		if weapon then
			local closest_ray = weapon.base(weapon):get_aim_assist(player_state.get_fire_weapon_position(player_state), player_state.get_fire_weapon_direction(player_state), nil, true)

			self._start_aim_assist(self, closest_ray, self._aim_assist_sticky)
		else
			self._stop_aim_assist(self, self._aim_assist_sticky)
		end
	end

	return 
end
FPCameraPlayerBase.clbk_aim_assist = function (self, col_ray)
	self._start_aim_assist(self, col_ray, self._aim_assist)

	return 
end
FPCameraPlayerBase.clbk_stop_aim_assist = function (self)
	self._stop_aim_assist(self, self._aim_assist)

	return 
end
FPCameraPlayerBase.animate_fov = function (self, new_fov, duration_multiplier)
	if new_fov == self._fov.fov then
		self._fov.transition = nil
	else
		local transition = {}
		self._fov.transition = transition
		transition.end_fov = new_fov
		transition.start_fov = self._fov.fov
		transition.start_t = managers.player:player_timer():time()
		transition.duration = (duration_multiplier or 1)*0.23
	end

	return 
end
FPCameraPlayerBase.anim_clbk_idle_full_blend = function (self)
	self._anim_empty_state_wanted = true

	self._check_play_empty_state(self)

	return 
end
FPCameraPlayerBase._check_play_empty_state = function (self)
	if not self._anim_empty_state_wanted then
		return 
	end

	if self._steelsight_anims_enabled then
		return 
	end

	self.play_redirect(self, self.IDS_EMPTY)

	return 
end
FPCameraPlayerBase.anim_clbk_idle_exit = function (self)
	return 
end
FPCameraPlayerBase.anim_clbk_empty_enter = function (self)
	self._playing_empty_state = true

	return 
end
FPCameraPlayerBase.anim_clbk_empty_exit = function (self)
	self._playing_empty_state = false

	return 
end
FPCameraPlayerBase.playing_empty_state = function (self)
	return self._playing_empty_state
end
FPCameraPlayerBase.anim_clbk_empty_full_blend = function (self)
	self._playing_empty_state = false

	self.set_anims_enabled(self, false)

	return 
end
FPCameraPlayerBase.set_handcuff_units = function (self, units)
	self._handcuff_units = units

	return 
end
FPCameraPlayerBase.anim_clbk_spawn_handcuffs = function (self)
	if not self._handcuff_units then
		local align_obj_l_name = Idstring("a_weapon_left")
		local align_obj_r_name = Idstring("a_weapon_right")
		local align_obj_l = self._unit:get_object(align_obj_l_name)
		local align_obj_r = self._unit:get_object(align_obj_r_name)
		local handcuff_unit_name = Idstring("units/equipment/handcuffs_first_person/handcuffs_first_person")
		local handcuff_unit_l = World:spawn_unit(handcuff_unit_name, align_obj_l.position(align_obj_l), align_obj_l.rotation(align_obj_l))
		local handcuff_unit_r = World:spawn_unit(handcuff_unit_name, align_obj_r.position(align_obj_r), align_obj_r.rotation(align_obj_r))

		self._unit:link(align_obj_l_name, handcuff_unit_l, handcuff_unit_l.orientation_object(handcuff_unit_l):name())
		self._unit:link(align_obj_r_name, handcuff_unit_r, handcuff_unit_r.orientation_object(handcuff_unit_r):name())

		local handcuff_units = {
			handcuff_unit_l,
			handcuff_unit_r
		}

		self.set_handcuff_units(self, handcuff_units)
	end

	return 
end
FPCameraPlayerBase.anim_clbk_unspawn_handcuffs = function (self)
	if self._handcuff_units then
		for _, handcuff_unit in pairs(self._handcuff_units) do
			if alive(handcuff_unit) then
				handcuff_unit.set_visible(handcuff_unit, false)
				handcuff_unit.set_slot(handcuff_unit, 0)
			end
		end
	end

	self.set_handcuff_units(self, nil)

	return 
end
FPCameraPlayerBase.get_weapon_offsets = function (self)
	local weapon = self._parent_unit:inventory():equipped_unit()
	local object = weapon.get_object(weapon, Idstring("a_sight"))

	print(object.position(object) - self._unit:position():rotate_HP(self._unit:rotation():inverse()))
	print(self._unit:rotation():inverse()*object.rotation(object))

	return 
end
FPCameraPlayerBase.set_anims_enabled = function (self, state)
	if state ~= self._anims_enabled then
		self._unit:set_animations_enabled(state)

		self._anims_enabled = state
	end

	return 
end
FPCameraPlayerBase.anims_enabled = function (self)
	return self._anims_enabled
end
FPCameraPlayerBase.play_sound = function (self, unit, event)
	if alive(self._parent_unit) then
		self._parent_unit:sound():play(event)
	end

	return 
end
FPCameraPlayerBase.play_melee_sound = function (self, unit, sound_id)
	local melee_entry = managers.blackmarket:equipped_melee_weapon()
	local tweak_data = tweak_data.blackmarket.melee_weapons[melee_entry]

	if not tweak_data.sounds or not tweak_data.sounds[sound_id] then
		return 
	end

	if alive(self._parent_unit) then
		self._parent_unit:sound():play(tweak_data.sounds[sound_id], nil, false)
	end

	return 
end
FPCameraPlayerBase.set_limits = function (self, spin, pitch)
	self._limits = {}

	if spin then
		self._limits.spin = {
			mid = self._camera_properties.spin,
			offset = spin
		}
	end

	if pitch then
		self._limits.pitch = {
			mid = self._camera_properties.pitch,
			offset = pitch
		}
	end

	return 
end
FPCameraPlayerBase.remove_limits = function (self)
	self._limits = nil

	return 
end
FPCameraPlayerBase.throw_projectile = function (self, unit)
	self.unspawn_grenade(self)

	if alive(self._parent_unit) then
		self._parent_unit:equipment():throw_projectile()
	end

	return 
end
FPCameraPlayerBase.throw_grenade = function (self, unit)
	self.unspawn_grenade(self)

	if alive(self._parent_unit) then
		self._parent_unit:equipment():throw_grenade()
	end

	return 
end
FPCameraPlayerBase.spawn_grenade = function (self)
	if alive(self._grenade_unit) then
		return 
	end

	local align_obj_l_name = Idstring("a_weapon_left")
	local align_obj_r_name = Idstring("a_weapon_right")
	local align_obj_l = self._unit:get_object(align_obj_l_name)
	local align_obj_r = self._unit:get_object(align_obj_r_name)
	local grenade_entry = managers.blackmarket:equipped_grenade()
	self._grenade_unit = World:spawn_unit(Idstring(tweak_data.blackmarket.projectiles[grenade_entry].unit_dummy), align_obj_r.position(align_obj_r), align_obj_r.rotation(align_obj_r))

	self._unit:link(align_obj_r.name(align_obj_r), self._grenade_unit, self._grenade_unit:orientation_object():name())

	return 
end
FPCameraPlayerBase.unspawn_grenade = function (self)
	if alive(self._grenade_unit) then
		self._grenade_unit:unlink()
		World:delete_unit(self._grenade_unit)

		self._grenade_unit = nil
	end

	return 
end
FPCameraPlayerBase.play_anim_melee_item = function (self, tweak_name)
	if not self._melee_item_units then
		return 
	end

	if self._melee_item_anim then
		for _, unit in ipairs(self._melee_item_units) do
			unit.anim_stop(unit, self._melee_item_anim)
		end

		self._melee_item_anim = nil
	end

	local melee_entry = managers.blackmarket:equipped_melee_weapon()
	local anims = tweak_data.blackmarket.melee_weapons[melee_entry].anims
	local anim_data = anims and anims[tweak_name]

	if not anim_data then
		return 
	end

	local ids = anim_data.anim and Idstring(anim_data.anim)

	if ids then
		for _, unit in ipairs(self._melee_item_units) do
			local length = unit.anim_length(unit, ids)

			if anim_data.loop then
				unit.anim_play_loop(unit, ids, 0, length, 1)
			else
				unit.anim_play_to(unit, ids, length, 1)
			end
		end

		self._melee_item_anim = ids
	end

	return 
end
FPCameraPlayerBase.spawn_melee_item = function (self)
	if self._melee_item_units then
		return 
	end

	local melee_entry = managers.blackmarket:equipped_melee_weapon()
	local unit_name = tweak_data.blackmarket.melee_weapons[melee_entry].unit

	if unit_name then
		local aligns = tweak_data.blackmarket.melee_weapons[melee_entry].align_objects or {
			"a_weapon_left"
		}
		local graphic_objects = tweak_data.blackmarket.melee_weapons[melee_entry].graphic_objects or {}
		self._melee_item_units = {}

		for _, align in ipairs(aligns) do
			local align_obj_name = Idstring(align)
			local align_obj = self._unit:get_object(align_obj_name)
			local unit = World:spawn_unit(Idstring(unit_name), align_obj.position(align_obj), align_obj.rotation(align_obj))

			self._unit:link(align_obj.name(align_obj), unit, unit.orientation_object(unit):name())

			for a_object, g_object in pairs(graphic_objects) do
				local graphic_obj_name = Idstring(g_object)
				local graphic_obj = unit.get_object(unit, graphic_obj_name)

				graphic_obj.set_visibility(graphic_obj, Idstring(a_object) == align_obj_name)
			end

			if alive(unit) and unit.damage(unit) and unit.damage(unit):has_sequence("game") then
				unit.damage(unit):run_sequence_simple("game")
			end

			table.insert(self._melee_item_units, unit)
		end

		self.play_anim_melee_item(self, "charge")
	end

	return 
end
FPCameraPlayerBase.unspawn_melee_item = function (self)
	if not self._melee_item_units then
		return 
	end

	for _, unit in ipairs(self._melee_item_units) do
		if alive(unit) then
			unit.unlink(unit)
			World:delete_unit(unit)
		end
	end

	self._melee_item_units = nil
	self._melee_item_anim = nil

	return 
end
FPCameraPlayerBase.hide_weapon = function (self)
	if alive(self._parent_unit) then
		self._parent_unit:inventory():hide_equipped_unit()
	end

	return 
end
FPCameraPlayerBase.show_weapon = function (self)
	if alive(self._parent_unit) then
		self._parent_unit:inventory():show_equipped_unit()
	end

	return 
end
FPCameraPlayerBase.enter_shotgun_reload_loop = function (self, unit, state, ...)
	if alive(self._parent_unit) then
		local speed_multiplier = self._parent_unit:inventory():equipped_unit():base():reload_speed_multiplier()

		self._unit:anim_state_machine():set_speed(Idstring(state), speed_multiplier)
	end

	return 
end
FPCameraPlayerBase.spawn_mask = function (self)
	if not self._mask_unit then
		local align_obj_l_name = Idstring("a_weapon_left")
		local align_obj_r_name = Idstring("a_weapon_right")
		local align_obj_l = self._unit:get_object(align_obj_l_name)
		local align_obj_r = self._unit:get_object(align_obj_r_name)
		local mask_unit_name = "units/payday2/masks/fps_temp_dallas/temp_mask_dallas"
		local equipped_mask = managers.blackmarket:equipped_mask()
		local peer_id = managers.network:session():local_peer():id()
		local blueprint = nil
		local mask_id = equipped_mask.mask_id and managers.blackmarket:get_real_mask_id(equipped_mask.mask_id, peer_id)

		if mask_id then
			mask_unit_name = managers.blackmarket:mask_unit_name_by_mask_id(mask_id, peer_id)
			blueprint = equipped_mask.blueprint
		else
			mask_unit_name = tweak_data.blackmarket.masks[equipped_mask].unit
		end

		self._mask_unit = World:spawn_unit(Idstring(mask_unit_name), align_obj_r.position(align_obj_r), align_obj_r.rotation(align_obj_r))
		local glass_id_string = Idstring("glass")
		local mtr_hair_solid_id_string = Idstring("mtr_hair_solid")
		local mtr_hair_effect_id_string = Idstring("mtr_hair_effect")
		local mtr_bloom_glow_id_string = Idstring("mtr_bloom_glow")
		local mtr_opacity = Idstring("mtr_opacity")
		local glow_id_strings = {}

		for i = 1, 5, 1 do
			glow_id_strings[Idstring("glow" .. tostring(i)):key()] = true
		end

		local sweep_id_strings = {}

		for i = 1, 5, 1 do
			sweep_id_strings[Idstring("sweep" .. tostring(i)):key()] = true
		end

		for _, material in ipairs(self._mask_unit:get_objects_by_type(Idstring("material"))) do
			if material.name(material) == glass_id_string then
				material.set_render_template(material, Idstring("opacity:CUBE_ENVIRONMENT_MAPPING:CUBE_FRESNEL:DIFFUSE_TEXTURE:FPS"))
			elseif (material.name(material) ~= mtr_hair_solid_id_string or false) and (material.name(material) ~= mtr_opacity or false) and (material.name(material) ~= mtr_hair_effect_id_string or false) then
				if material.name(material) == mtr_bloom_glow_id_string then
					material.set_render_template(material, Idstring("generic:DEPTH_SCALING:DIFFUSE_TEXTURE:SELF_ILLUMINATION:SELF_ILLUMINATION_BLOOM"))
				elseif glow_id_strings[material.name(material):key()] then
					material.set_render_template(material, Idstring("effect:BLEND_ADD:DIFFUSE0_TEXTURE"))
				elseif sweep_id_strings[material.name(material):key()] then
					material.set_render_template(material, Idstring("effect:BLEND_ADD:DIFFUSE0_TEXTURE:DIFFUSE0_THRESHOLD_SWEEP"))
				else
					material.set_render_template(material, Idstring("solid_mask:DEPTH_SCALING"))
				end
			end
		end

		if blueprint then
			print("FPCameraPlayerBase:spawn_mask", inspect(blueprint))
			self._mask_unit:base():apply_blueprint(blueprint)
		end

		print(inspect(self._mask_unit:get_objects_by_type(Idstring("material"))))
		self._mask_unit:set_timer(managers.player:player_timer())
		self._mask_unit:set_animation_timer(managers.player:player_timer())
		self._mask_unit:anim_stop()

		if not tweak_data.blackmarket.masks[mask_id].type then
			local backside = World:spawn_unit(Idstring("units/payday2/masks/msk_fps_back_straps/msk_fps_back_straps"), align_obj_r.position(align_obj_r), align_obj_r.rotation(align_obj_r))

			for _, material in ipairs(backside.get_objects_by_type(backside, Idstring("material"))) do
				material.set_render_template(material, Idstring("generic:DEPTH_SCALING:DIFFUSE_TEXTURE:NORMALMAP:SKINNED_3WEIGHTS"))
			end

			backside.set_timer(backside, managers.player:player_timer())
			backside.set_animation_timer(backside, managers.player:player_timer())
			backside.anim_play(backside, Idstring("mask_on"))
			self._mask_unit:link(self._mask_unit:orientation_object():name(), backside, backside.orientation_object(backside):name())
		end

		self._unit:link(align_obj_l.name(align_obj_l), self._mask_unit, self._mask_unit:orientation_object():name())
	end

	return 
end
FPCameraPlayerBase.relink_mask = function (self)
	return 
end
FPCameraPlayerBase.unspawn_mask = function (self)
	if alive(self._mask_unit) then
		for _, linked_unit in ipairs(self._mask_unit:children()) do
			linked_unit.unlink(linked_unit)
			World:delete_unit(linked_unit)
		end

		self._mask_unit:unlink()

		local name = self._mask_unit:name()

		World:delete_unit(self._mask_unit)

		self._mask_unit = nil
	end

	return 
end
FPCameraPlayerBase.counter_taser = function (self)
	local current_state = self._parent_movement_ext._current_state

	if current_state and current_state.give_shock_to_taser then
		current_state.give_shock_to_taser(current_state)

		if alive(self._taser_hooks_unit) then
			local align_obj = self._unit:get_object(Idstring("a_weapon_right"))

			World:effect_manager():spawn({
				effect = Idstring("effects/payday2/particles/character/taser_stop"),
				position = align_obj.position(align_obj),
				normal = align_obj.rotation(align_obj):y()
			})
		end
	end

	return 
end
FPCameraPlayerBase.spawn_taser_hooks = function (self)
	if not alive(self._taser_hooks_unit) and alive(self._parent_unit) then
		local hooks_align = self._unit:get_object(Idstring("a_weapon_right"))
		local taser_hooks_unit_name = "units/payday2/weapons/wpn_fps_taser_hooks/wpn_fps_taser_hooks"

		managers.dyn_resource:load(Idstring("unit"), Idstring(taser_hooks_unit_name), DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)

		self._taser_hooks_unit = World:spawn_unit(Idstring(taser_hooks_unit_name), hooks_align.position(hooks_align), hooks_align.rotation(hooks_align))

		self._taser_hooks_unit:set_timer(managers.player:player_timer())
		self._taser_hooks_unit:set_animation_timer(managers.player:player_timer())
		self._taser_hooks_unit:anim_play(Idstring("taser_hooks"))
		self._unit:link(hooks_align.name(hooks_align), self._taser_hooks_unit, self._taser_hooks_unit:orientation_object():name())
	end

	return 
end
FPCameraPlayerBase.unspawn_taser_hooks = function (self)
	if alive(self._taser_hooks_unit) then
		self._taser_hooks_unit:unlink()

		local name = self._taser_hooks_unit:name()

		World:delete_unit(self._taser_hooks_unit)
		managers.dyn_resource:unload(Idstring("unit"), name, DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)

		self._taser_hooks_unit = nil
	end

	return 
end
FPCameraPlayerBase.end_tase = function (self)
	local current_state = self._parent_movement_ext._current_state

	if current_state and current_state.clbk_exit_to_std then
		current_state.clbk_exit_to_std(current_state)
	end

	return 
end
FPCameraPlayerBase.anim_clbk_check_bullet_object = function (self)
	if alive(self._parent_unit) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if alive(weapon) then
			weapon.base(weapon):predict_bullet_objects()
		end
	end

	return 
end
FPCameraPlayerBase.anim_clbk_stop_weapon_reload = function (self)
	if alive(self._parent_unit) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if alive(weapon) then
			weapon.base(weapon):tweak_data_anim_stop("reload")
			weapon.base(weapon):tweak_data_anim_stop("reload_not_empty")
		end
	end

	return 
end
FPCameraPlayerBase.anim_clbk_play_weapon_anim = function (self, unit, anim, speed)
	if alive(self._parent_unit) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if alive(weapon) then
			weapon.base(weapon):tweak_data_anim_play(anim, speed)
		end
	end

	return 
end
FPCameraPlayerBase.anim_clbk_stop_weapon_anim = function (self, unit, anim)
	if alive(self._parent_unit) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if alive(weapon) then
			weapon.base(weapon):tweak_data_anim_stop(anim)
		end
	end

	return 
end
FPCameraPlayerBase.anim_clbk_stop_weapon_reload_all = function (self)
	if alive(self._parent_unit) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if alive(weapon) then
			weapon.base(weapon):tweak_data_anim_stop("reload_enter")
			weapon.base(weapon):tweak_data_anim_stop("reload_exit")
		end
	end

	return 
end
FPCameraPlayerBase.anim_clbk_spawn_shotgun_shell = function (self)
	if alive(self._parent_unit) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if alive(weapon) and weapon.base(weapon).shotgun_shell_data then
			local shotgun_shell_data = weapon.base(weapon):shotgun_shell_data()

			if not shotgun_shell_data then
				return 
			end

			local align_obj_l_name = Idstring("a_weapon_left")
			local align_obj_r_name = Idstring("a_weapon_right")
			local align_obj_l = self._unit:get_object(align_obj_l_name)
			local align_obj_r = self._unit:get_object(align_obj_r_name)
			local align_obj = align_obj_l

			if shotgun_shell_data.align and shotgun_shell_data.align == "right" then
				align_obj = align_obj_r
			end

			self._unspawn_shotgun_shell(self)

			self._shell = World:spawn_unit(Idstring(shotgun_shell_data.unit_name), align_obj.position(align_obj), align_obj.rotation(align_obj))

			self._unit:link(align_obj.name(align_obj), self._shell, self._shell:orientation_object():name())
		end
	end

	return 
end
FPCameraPlayerBase.anim_clbk_unspawn_shotgun_shell = function (self)
	self._unspawn_shotgun_shell(self)

	return 
end
FPCameraPlayerBase._unspawn_shotgun_shell = function (self)
	if not alive(self._shell) then
		return 
	end

	self._shell:unlink()
	World:delete_unit(self._shell)

	self._shell = nil

	return 
end
FPCameraPlayerBase.anim_clbk_stop_weapon_magazine_empty = function (self)
	if alive(self._parent_unit) then
		local weapon = self._parent_unit:inventory():equipped_unit()

		if alive(weapon) then
			weapon.base(weapon):tweak_data_anim_stop("magazine_empty")
		end
	end

	return 
end
FPCameraPlayerBase.load_fps_mask_units = function (self)
	if not self._mask_backface_loaded then
		self._mask_backface_loaded = true
	end

	return 
end
FPCameraPlayerBase.destroy = function (self)
	if self._parent_unit then
		self._parent_unit:base():remove_destroy_listener("FPCameraPlayerBase")
	end

	if self._light then
		World:delete_light(self._light)
	end

	if self._light_effect then
		World:effect_manager():kill(self._light_effect)

		self._light_effect = nil
	end

	self.anim_clbk_unspawn_handcuffs(self)
	self.unspawn_mask(self)
	self.unspawn_grenade(self)
	self.unspawn_melee_item(self)
	self._unspawn_shotgun_shell(self)

	if self._mask_backface_loaded then
		self._mask_backface_loaded = nil
	end

	return 
end
FPCameraPlayerBase.set_spin = function (self, _spin)
	self._camera_properties.spin = _spin

	return 
end
FPCameraPlayerBase.set_pitch = function (self, _pitch)
	self._camera_properties.pitch = _pitch

	return 
end
FPCameraPlayerBase.current_tilt = function (self)
	return self._camera_properties.current_tilt
end
FPCameraPlayerBase.animate_pitch = function (self, start_t, start_pitch, end_pitch, total_duration)
	self._animate_pitch = {
		start_t = start_t,
		start_pitch = start_pitch or self._camera_properties.pitch,
		end_pitch = end_pitch,
		duration = total_duration
	}

	return 
end
FPCameraPlayerBase.animate_pitch_upd = function (self)
	local t = Application:time()
	local elapsed_t = t - self._animate_pitch.start_t
	local step = elapsed_t/self._animate_pitch.duration

	if 1 < step then
		self._animate_pitch = nil
	else
		step = self.catmullrom(self, step, -10, 0, 1, 0.7)
		self._camera_properties.pitch = math.lerp(self._animate_pitch.start_pitch, self._animate_pitch.end_pitch, step)
	end

	return 
end
FPCameraPlayerBase.update_tilt_smooth = function (self, direction, max_tilt, tilt_speed, dt)
	self._tilt_dt = self._tilt_dt or 0

	if direction < 0 and self._camera_properties.current_tilt <= 0 then
		self.set_target_tilt(self, self.smoothstep(self, 0, max_tilt, self._tilt_dt, tilt_speed)*-1)

		if self._tilt_dt < tilt_speed then
			self._tilt_dt = self._tilt_dt + dt
		end
	elseif 0 < direction and 0 <= self._camera_properties.current_tilt then
		self.set_target_tilt(self, self.smoothstep(self, 0, max_tilt, self._tilt_dt, tilt_speed))

		if self._tilt_dt < tilt_speed then
			self._tilt_dt = self._tilt_dt + dt
		end
	else
		self.set_target_tilt(self, math.sign(self._camera_properties.current_tilt)*self.smoothstep(self, 0, max_tilt, self._tilt_dt, tilt_speed))

		if 0 < self._tilt_dt then
			self._tilt_dt = self._tilt_dt - dt*2

			if self._tilt_dt < 0 then
				self._tilt_dt = 0
			end
		end
	end

	return 
end
FPCameraPlayerBase.catmullrom = function (self, t, p0, p1, p2, p3)
	return (p1*2 + (-p0 + p2)*t + ((p0*2 - p1*5 + p2*4) - p3)*t*t + ((-p0 + p1*3) - p2*3 + p3)*t*t*t)*0.5
end
FPCameraPlayerBase.smoothstep = function (self, a, b, step, n)
	local v = step/n
	v = (v - 1)*(v - 1) - 1
	local x = a*(v - 1) + b*v

	return x
end
local mvec_temp1 = Vector3()
local mvec_temp2 = Vector3()
local mvec_temp3 = Vector3()
FPCameraPlayerBase._update_fadeout = function (self, mover_position, new_head_pos, t, dt)
	local fadeout_data = self._fadeout

	if FPCameraPlayerBase.NO_FADEOUT or self._parent_movement_ext:warping() or self._parent_movement_ext:current_state_name() == "driving" then
		fadeout_data.value = 0
		fadeout_data.effect.color.alpha = 0

		return 
	end

	local distance_to_mover = mvector3.distance(new_head_pos.with_z(new_head_pos, 0), mover_position.with_z(mover_position, 0))
	local rotation = self._output_data.rotation
	local dir = mvec_temp1

	mvector3.set(dir, math.Y)

	dir = dir.rotate_with(dir, rotation)

	mvector3.multiply(dir, 20)

	local p_behind = mvec_temp2

	mvector3.set(p_behind, new_head_pos)
	mvector3.subtract(p_behind, dir)

	local p_ahead = mvec_temp3

	mvector3.set(p_ahead, new_head_pos)
	mvector3.add(p_ahead, dir)

	local ghost_max_th = 50
	local fade_distance = 13
	local distance_to_obstacle = fade_distance
	local fadeout = 0
	local ray = self._parent_unit:raycast("ray", p_behind, p_ahead, "slot_mask", fadeout_data.slotmask, "ray_type", "body mover", "sphere_cast_radius", 10, "bundle", 5)

	if ray then
		local obstacle_min_th = 7
		local distance = mvector3.distance(new_head_pos, ray.position)

		if distance <= obstacle_min_th then
			distance_to_obstacle = 0
		elseif distance < obstacle_min_th + fade_distance then
			distance_to_obstacle = distance - obstacle_min_th
		end
	end

	if 10 < distance_to_mover and 0 < distance_to_obstacle then
		local obstacle_min_th = 5.5
		local p_ahead = mvec_temp1

		mvector3.set(p_ahead, new_head_pos)
		mvector3.subtract(p_ahead, mover_position)
		mvector3.normalize(p_ahead)
		mvector3.multiply(p_ahead, 60)
		mvector3.add(p_ahead, new_head_pos)

		local ray = self._parent_unit:raycast("ray", mover_position, p_ahead, "slot_mask", fadeout_data.slotmask, "ray_type", "body mover", "sphere_cast_radius", 10, "bundle", 5)

		if ray then
			local d1 = mvector3.distance(mover_position, ray.position)
			local d2 = mvector3.distance(mover_position, new_head_pos)
			local d = d1 - d2

			if d1 < d2 or d <= obstacle_min_th then
				distance_to_obstacle = 0
			elseif d < obstacle_min_th + fade_distance then
				distance_to_obstacle = d - obstacle_min_th
			end
		end
	end

	fadeout = distance_to_obstacle/fade_distance - 1

	if ghost_max_th < distance_to_mover then
		fadeout = math.max((distance_to_mover - ghost_max_th)/fade_distance, fadeout)
	end

	fadeout = math.clamp(fadeout, 0, 1)

	if fadeout_data.value < fadeout then
		fadeout_data.value = math.step(fadeout_data.value, fadeout, (fadeout < 1 and dt*3) or dt*10)
		fadeout_data.fadein_speed = 0
	elseif fadeout < fadeout_data.value then
		fadeout_data.value = math.step(fadeout_data.value, fadeout, dt*fadeout_data.fadein_speed)
		fadeout_data.fadein_speed = math.min(fadeout_data.fadein_speed + dt*1, 0.7)
	end

	local v = fadeout_data.value
	fadeout_data.effect.color.alpha = v*v*(v*2 - 3)

	if 0.95 < fadeout then
		self._ghost_reset_timer_t = self._ghost_reset_timer_t + dt
	else
		self._ghost_reset_timer_t = 0
	end

	if 1.5 < self._ghost_reset_timer_t then
		self._parent_movement_ext:reset_ghost_position()

		self._ghost_reset_timer_t = 0
	end

	return 
end
FPCameraPlayerBase.set_hmd_tracking = function (self, enabled)
	self._hmd_tracking = enabled

	return 
end
FPCameraPlayerBase.set_block_input = function (self, block)
	self._block_input = block

	return 
end
FPCameraPlayerBase.reset_base_rotation = function (self, rot)
	self._base_rotation = Rotation(self._output_data.rotation:yaw(), 0, 0)*rot

	return 
end
FPCameraPlayerBase.set_base_rotation = function (self, rot)
	self._base_rotation = Rotation(self._base_rotation:yaw() - self._output_data.rotation:yaw(), 0, 0)*rot

	return 
end
FPCameraPlayerBase.base_rotation = function (self)
	return self._base_rotation
end
FPCameraPlayerBase.enter_vehicle = function (self)
	self._initial_hmd_rotation = VRManager:hmd_rotation()

	return 
end

return 
