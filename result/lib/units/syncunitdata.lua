SyncUnitData = SyncUnitData or class()
SyncUnitData.init = function (self, unit)
	self._unit = unit

	return 
end
SyncUnitData.save = function (self, data)
	local state = {
		lights = {}
	}

	for _, light in ipairs(self._unit:get_objects_by_type(Idstring("light"))) do
		local l = {
			name = light.name(light),
			enabled = light.enable(light),
			far_range = light.far_range(light),
			near_range = light.near_range(light),
			color = light.color(light),
			spot_angle_start = light.spot_angle_start(light),
			spot_angle_end = light.spot_angle_end(light),
			multiplier_nr = light.multiplier(light),
			specular_multiplier_nr = light.specular_multiplier(light),
			falloff_exponent = light.falloff_exponent(light)
		}

		table.insert(state.lights, l)
	end

	state.projection_light = self._unit:unit_data().projection_light
	state.projection_textures = self._unit:unit_data().projection_textures
	data.SyncUnitData = state

	return 
end
SyncUnitData.load = function (self, data)
	local state = data.SyncUnitData
	self._unit:unit_data().unit_id = self._unit:editor_id()

	managers.worlddefinition:setup_lights(self._unit, state)
	managers.worlddefinition:setup_projection_light(self._unit, state)

	return 
end

return 
