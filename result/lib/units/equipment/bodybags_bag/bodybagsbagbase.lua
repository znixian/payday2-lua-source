BodyBagsBagBase = BodyBagsBagBase or class(UnitBase)
BodyBagsBagBase.spawn = function (pos, rot, upgrade_lvl, peer_id)
	local unit_name = "units/payday2/equipment/gen_equipment_bodybags_bag/gen_equipment_bodybags_bag"
	local unit = World:spawn_unit(Idstring(unit_name), pos, rot)

	managers.network:session():send_to_peers_synched("sync_equipment_setup", unit, upgrade_lvl, peer_id or 0)
	unit.base(unit):setup(upgrade_lvl)

	return unit
end
BodyBagsBagBase.set_server_information = function (self, peer_id)
	self._server_information = {
		owner_peer_id = peer_id
	}

	managers.network:session():peer(peer_id):set_used_deployable(true)

	return 
end
BodyBagsBagBase.server_information = function (self)
	return self._server_information
end
BodyBagsBagBase.init = function (self, unit)
	UnitBase.init(self, unit, false)

	self._unit = unit
	self._is_attachable = true
	self._max_bodybag_amount = tweak_data.upgrades.bodybag_crate_base

	self._unit:sound_source():post_event("ammo_bag_drop")

	if Network:is_client() then
		self._validate_clbk_id = "bodybags_bag_validate" .. tostring(unit.key(unit))

		managers.enemy:add_delayed_clbk(self._validate_clbk_id, callback(self, self, "_clbk_validate"), Application:time() + 60)
	end

	return 
end
BodyBagsBagBase.get_name_id = function (self)
	return "bodybags_bag"
end
BodyBagsBagBase._clbk_validate = function (self)
	self._validate_clbk_id = nil

	if not self._was_dropin then
		local peer = managers.network:session():server_peer()

		peer.mark_cheater(peer, VoteManager.REASON.many_assets)
	end

	return 
end
BodyBagsBagBase.sync_setup = function (self, upgrade_lvl, peer_id)
	if self._validate_clbk_id then
		managers.enemy:remove_delayed_clbk(self._validate_clbk_id)

		self._validate_clbk_id = nil
	end

	managers.player:verify_equipment(peer_id, "bodybags_bag")
	self.setup(self, upgrade_lvl)

	return 
end
BodyBagsBagBase.setup = function (self)
	self._bodybag_amount = tweak_data.upgrades.bodybag_crate_base
	self._empty = false

	self._set_visual_stage(self)

	if Network:is_server() and self._is_attachable then
		local from_pos = self._unit:position() + self._unit:rotation():z()*10
		local to_pos = self._unit:position() + self._unit:rotation():z()*-10
		local ray = self._unit:raycast("ray", from_pos, to_pos, "slot_mask", managers.slot:get_mask("world_geometry"))

		if ray then
			self._attached_data = {
				body = ray.body,
				position = ray.body:position(),
				rotation = ray.body:rotation(),
				index = 1,
				max_index = 3
			}

			self._unit:set_extension_update_enabled(Idstring("base"), true)
		end
	end

	return 
end
BodyBagsBagBase.update = function (self, unit, t, dt)
	self._check_body(self)

	return 
end
BodyBagsBagBase._check_body = function (self)
	if self._is_dynamic then
		return 
	end

	if not alive(self._attached_data.body) then
		self.server_set_dynamic(self)

		return 
	end

	if self._attached_data.index == 1 then
		if not self._attached_data.body:enabled() then
			self.server_set_dynamic(self)
		end
	elseif self._attached_data.index == 2 then
		if not mrotation.equal(self._attached_data.rotation, self._attached_data.body:rotation()) then
			self.server_set_dynamic(self)
		end
	elseif self._attached_data.index == 3 and mvector3.not_equal(self._attached_data.position, self._attached_data.body:position()) then
		self.server_set_dynamic(self)
	end

	self._attached_data.index = ((self._attached_data.index < self._attached_data.max_index and self._attached_data.index) or 0) + 1

	return 
end
BodyBagsBagBase.server_set_dynamic = function (self)
	self._set_dynamic(self)

	if managers.network:session() then
		managers.network:session():send_to_peers_synched("sync_unit_event_id_16", self._unit, "base", 2)
	end

	return 
end
BodyBagsBagBase.sync_net_event = function (self, event_id)
	if event_id == 1 then
		self.sync_bodybag_taken(self, 1)
	elseif event_id == 2 then
		self._set_dynamic(self)
	end

	return 
end
BodyBagsBagBase._set_dynamic = function (self)
	self._is_dynamic = true

	self._unit:body("dynamic"):set_enabled(true)

	return 
end
BodyBagsBagBase.take_bodybag = function (self, unit)
	if self._empty then
		return 
	end

	local can_take_bodybag = (self._can_take_bodybag(self) and 1) or 0

	if can_take_bodybag == 1 then
		unit.sound(unit):play("pickup_ammo")
		managers.player:add_body_bags_amount(1)
		managers.network:session():send_to_peers_synched("sync_unit_event_id_16", self._unit, "base", 1)

		self._bodybag_amount = self._bodybag_amount - 1

		managers.mission:call_global_event("player_refill_bodybagsbag")
	end

	if self._bodybag_amount <= 0 then
		self._set_empty(self)
	end

	self._set_visual_stage(self)

	return can_take_bodybag
end
BodyBagsBagBase._set_visual_stage = function (self)
	if alive(self._unit) and self._unit:damage() then
		local state = "state_" .. tostring(math.clamp(self._max_bodybag_amount - self._bodybag_amount, 0, self._max_bodybag_amount))

		if self._unit:damage():has_sequence(state) then
			self._unit:damage():run_sequence_simple(state)
		end
	end

	return 
end
BodyBagsBagBase.sync_bodybag_taken = function (self, amount)
	self._bodybag_amount = self._bodybag_amount - amount

	if self._bodybag_amount <= 0 then
		self._set_empty(self)
	end

	self._set_visual_stage(self)

	return 
end
BodyBagsBagBase._can_take_bodybag = function (self, unit)
	if self._empty or self._bodybag_amount < 1 or managers.player:has_max_body_bags() then
		return false
	end

	return true
end
BodyBagsBagBase._set_empty = function (self)
	self._empty = true

	if alive(self._unit) then
		self._unit:interaction():set_active(false)
	end

	return 
end
BodyBagsBagBase.save = function (self, data)
	local state = {
		bodybag_amount = self._bodybag_amount,
		is_dynamic = self._is_dynamic
	}
	data.BodyBagsBagBase = state

	return 
end
BodyBagsBagBase.load = function (self, data)
	local state = data.BodyBagsBagBase
	self._bodybag_amount = state.bodybag_amount

	if state.is_dynamic then
		self._set_dynamic(self)
	end

	if self._bodybag_amount <= 0 then
		self._set_empty(self)
	end

	self._set_visual_stage(self)

	self._was_dropin = true

	return 
end
BodyBagsBagBase.destroy = function (self)
	if self._validate_clbk_id then
		managers.enemy:remove_delayed_clbk(self._validate_clbk_id)

		self._validate_clbk_id = nil
	end

	return 
end
CustomBodyBagsBagBase = CustomBodyBagsBagBase or class(BodyBagsBagBase)
CustomBodyBagsBagBase.init = function (self, unit)
	CustomBodyBagsBagBase.super.init(self, unit)

	self._is_attachable = self.is_attachable or false

	if self._validate_clbk_id then
		managers.enemy:remove_delayed_clbk(self._validate_clbk_id)

		self._validate_clbk_id = nil
	end

	self.setup(self, self.upgrade_lvl or 0)

	return 
end
CustomBodyBagsBagBase._set_empty = function (self)
	self._empty = true

	if alive(self._unit) then
		self._unit:interaction():set_active(false)
	end

	if self._unit:damage():has_sequence("empty") then
		self._unit:damage():run_sequence_simple("empty")
	end

	return 
end

return 
