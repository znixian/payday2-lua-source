SentryGunBase = SentryGunBase or class(UnitBase)
SentryGunBase.DEPLOYEMENT_COST = {
	0.7,
	0.75,
	0.8
}
SentryGunBase.MIN_DEPLOYEMENT_COST = 0.2
SentryGunBase.AMMO_MUL = {
	1,
	1.5
}
SentryGunBase.ROTATION_SPEED_MUL = {
	1,
	1.5
}
SentryGunBase.SPREAD_MUL = {
	1,
	0.5
}
local sentry_uid = 1
SentryGunBase.init = function (self, unit)
	SentryGunBase.super.init(self, unit, false)

	self._unit = unit
	self._frame_callbacks = FrameCallback:new()

	self._frame_callbacks:add("check_body", callback(self, self, "_check_body"), 15)

	self._damage_multiplier = 1

	if self._place_snd_event then
		self._unit:sound_source():post_event(self._place_snd_event)
	end

	if Network:is_client() and not self._skip_authentication then
		self._validate_clbk_id = "sentry_gun_validate" .. tostring(unit.key(unit))

		managers.enemy:add_delayed_clbk(self._validate_clbk_id, callback(self, self, "_clbk_validate"), Application:time() + 60)
	end

	self.sentry_gun = true

	return 
end
SentryGunBase._clbk_validate = function (self)
	self._validate_clbk_id = nil

	if not self._was_dropin then
		local peer = managers.network:session():server_peer()

		peer.mark_cheater(peer, VoteManager.REASON.many_assets)
	end

	return 
end
SentryGunBase.sync_setup = function (self, upgrade_lvl, peer_id)
	if self._validate_clbk_id then
		managers.enemy:remove_delayed_clbk(self._validate_clbk_id)

		self._validate_clbk_id = nil
	end

	managers.player:verify_equipment(peer_id, "sentry_gun")

	return 
end
SentryGunBase.set_owner_id = function (self, owner_id)
	self._owner_id = owner_id

	if self._unit:interaction() then
		self._unit:interaction():set_owner_id(owner_id)
	end

	return 
end
SentryGunBase.is_owner = function (self)
	return self._owner_id and self._owner_id == managers.network:session():local_peer():id()
end
SentryGunBase.is_category = function (self, ...)
	local arg = {
		...
	}
	local categories = self.weapon_tweak_data(self).categories

	if not categories then
		return false
	end

	for i = 1, #arg, 1 do
		if table.contains(categories, arg[i]) then
			return true
		end
	end

	return false
end
SentryGunBase.post_init = function (self)
	if self._difficulty_sequences then
		local difficulty = (Global.game_settings and Global.game_settings.difficulty) or "normal"
		local difficulty_index = tweak_data:difficulty_to_index(difficulty)
		local difficulty_sequences_split = string.split(self._difficulty_sequences, ";")
		local difficulty_sequence = difficulty_sequences_split[difficulty_index]

		if difficulty_sequence and difficulty_sequence ~= "" then
			self._unit:damage():run_sequence_simple(difficulty_sequence)
		end
	end

	if Network:is_client() then
		self._unit:brain():set_active(true)
	end

	return 
end
SentryGunBase.spawn = function (owner, pos, rot, peer_id, verify_equipment, unit_idstring_index)
	local attached_data = SentryGunBase._attach(pos, rot)

	if not attached_data then
		return 
	end

	if verify_equipment and not managers.player:verify_equipment(peer_id, "sentry_gun") then
		return 
	end

	local sentry_owner = nil

	if owner and owner.base(owner).upgrade_value then
		sentry_owner = owner
	end

	local player_skill = PlayerSkill
	local ammo_multiplier = player_skill.skill_data("sentry_gun", "extra_ammo_multiplier", 1, sentry_owner)
	local armor_multiplier = player_skill.skill_data("sentry_gun", "armor_multiplier", 1, sentry_owner) - 1 + 1 + player_skill.skill_data("sentry_gun", "armor_multiplier2", 1, sentry_owner) - 1
	local spread_level = player_skill.skill_data("sentry_gun", "spread_multiplier", 1, sentry_owner)
	local rot_speed_level = player_skill.skill_data("sentry_gun", "rot_speed_multiplier", 1, sentry_owner)
	local ap_bullets = player_skill.has_skill("sentry_gun", "ap_bullets", sentry_owner)
	local has_shield = player_skill.has_skill("sentry_gun", "shield", sentry_owner)
	local id_string = Idstring("units/payday2/equipment/gen_equipment_sentry/gen_equipment_sentry")

	if unit_idstring_index then
		id_string = tweak_data.equipments.sentry_id_strings[unit_idstring_index]
	end

	local unit = World:spawn_unit(id_string, pos, rot)
	local spread_multiplier = SentryGunBase.SPREAD_MUL[spread_level]
	local rot_speed_multiplier = SentryGunBase.ROTATION_SPEED_MUL[rot_speed_level]

	managers.network:session():send_to_peers_synched("sync_equipment_setup", unit, 0, peer_id or 0)

	ammo_multiplier = SentryGunBase.AMMO_MUL[ammo_multiplier]

	unit.base(unit):setup(owner, ammo_multiplier, armor_multiplier, spread_multiplier, rot_speed_multiplier, has_shield, attached_data)

	local owner_id = unit.base(unit):get_owner_id()

	if ap_bullets and owner_id then
		local fire_mode_unit = World:spawn_unit(Idstring("units/payday2/equipment/gen_equipment_sentry/gen_equipment_sentry_fire_mode"), unit.position(unit), unit.rotation(unit))

		unit.weapon(unit):interaction_setup(fire_mode_unit, owner_id)
		managers.network:session():send_to_peers_synched("sync_fire_mode_interaction", unit, fire_mode_unit, owner_id)
	end

	local team = nil

	if owner then
		team = owner.movement(owner):team()
	else
		team = managers.groupai:state():team_data(tweak_data.levels:get_default_team_ID("player"))
	end

	unit.movement(unit):set_team(team)
	unit.brain(unit):set_active(true)

	SentryGunBase.deployed = (SentryGunBase.deployed or 0) + 1

	return unit, spread_level, rot_speed_level
end
SentryGunBase.spawn_from_sequence = function (self, align_obj_name, module_id)
	if not Network:is_server() then
		return 
	end

	local align_obj = self._unit:get_object(Idstring(align_obj_name))
	local pos = align_obj.position(align_obj)
	local rot = align_obj.rotation(align_obj)
	local attached_data = SentryGunBase._attach(pos, rot)

	if not attached_data then
		return 
	end

	local unit = nil
	unit = World:spawn_unit(Idstring("units/payday2/equipment/gen_equipment_sentry/gen_equipment_sentry_placement"), pos, rot)
	local rot_mul = SentryGunBase.ROTATION_SPEED_MUL[2]
	local spread_mul = SentryGunBase.SPREAD_MUL[2]
	local ammo_mul = SentryGunBase.AMMO_MUL[2]

	unit.base(unit):setup(managers.player:player_unit(), ammo_mul, 1, spread_mul, rot_mul, 1, true, attached_data)
	managers.network:session():send_to_peers_synched("sync_equipment_setup", unit, 0, 0)
	managers.network:session():send_to_peers_synched("from_server_sentry_gun_place_result", managers.network:session():local_peer():id(), 0, unit, 2, 2, true, 2)

	local team = managers.groupai:state():team_data(tweak_data.levels:get_default_team_ID("player"))

	unit.movement(unit):set_team(team)
	unit.brain(unit):set_active(true)

	return 
end
SentryGunBase.activate_as_module = function (self, team_type, tweak_table_id)
	self._tweak_table_id = tweak_table_id
	local team_id = tweak_data.levels:get_default_team_ID(team_type)
	local team_data = managers.groupai:state():team_data(team_id)

	self._unit:movement():set_team(team_data)
	self._unit:movement():on_activated(tweak_table_id)

	local weapon_setup_data = {
		auto_reload = true,
		expend_ammo = true,
		spread_mul = 1,
		autoaim = true,
		alert_AI = false,
		ignore_units = {
			self._unit
		},
		bullet_slotmask = managers.slot:get_mask("bullet_impact_targets")
	}

	self._unit:weapon():setup(weapon_setup_data, 1)

	if Network:is_server() then
		self._unit:weapon():set_ammo(tweak_data.weapon[tweak_table_id].CLIP_SIZE)
	end

	self._unit:character_damage():set_health(tweak_data.weapon[tweak_table_id].HEALTH_INIT, tweak_data.weapon[tweak_table_id].SHIELD_HEALTH_INIT)
	self._unit:brain():setup(1)
	self._unit:brain():on_activated(tweak_table_id)
	self._unit:brain():set_active(true)

	self._is_module = true

	managers.groupai:state():register_turret(self._unit)

	return 
end
SentryGunBase.get_name_id = function (self)
	return self._tweak_table_id
end
SentryGunBase.set_server_information = function (self, peer_id)
	self._server_information = {
		owner_peer_id = peer_id
	}

	managers.network:session():peer(peer_id):set_used_deployable(true)

	return 
end
SentryGunBase.server_information = function (self)
	return self._server_information
end
SentryGunBase.setup = function (self, owner, ammo_multiplier, armor_multiplier, spread_multiplier, rot_speed_multiplier, has_shield, attached_data)
	if Network:is_client() and not self._skip_authentication then
		self._validate_clbk_id = "sentry_gun_validate" .. tostring(unit:key())

		managers.enemy:add_delayed_clbk(self._validate_clbk_id, callback(self, self, "_clbk_validate"), Application:time() + 60)
	end

	self._attached_data = attached_data
	self._ammo_multiplier = ammo_multiplier
	self._armor_multiplier = armor_multiplier
	self._spread_multiplier = spread_multiplier
	self._rot_speed_multiplier = rot_speed_multiplier

	if has_shield then
		self.enable_shield(self)
	end

	local ammo_amount = tweak_data.upgrades.sentry_gun_base_ammo*ammo_multiplier

	self._unit:weapon():set_ammo(ammo_amount)

	local armor_amount = tweak_data.upgrades.sentry_gun_base_armor*armor_multiplier

	self._unit:character_damage():set_health(armor_amount, 0)

	self._owner = owner

	if owner then
		local peer = managers.network:session():peer_by_unit(owner)

		if peer then
			self._owner_id = peer.id(peer)

			if self._unit:interaction() then
				self._unit:interaction():set_owner_id(self._owner_id)
			end
		end
	end

	self._unit:movement():setup(rot_speed_multiplier)
	self._unit:brain():setup(rot_speed_multiplier/1)
	self.register(self)
	self._unit:movement():set_team(owner.movement(owner):team())

	local setup_data = {
		expend_ammo = true,
		autoaim = true,
		alert_AI = true,
		creates_alerts = true,
		user_unit = self._owner,
		ignore_units = {
			self._unit,
			self._owner
		},
		alert_filter = self._owner:movement():SO_access(),
		spread_mul = spread_multiplier
	}

	self._unit:weapon():setup(setup_data)
	self._unit:set_extension_update_enabled(Idstring("base"), true)
	self.post_setup(self)

	return true
end
SentryGunBase.post_setup = function (self)
	self._sentry_uid = "sentry_" .. tostring(sentry_uid)

	managers.mission:add_global_event_listener(self._sentry_uid, {
		"on_picked_up_carry"
	}, callback(self, self, "_on_picked_up_cash"))

	sentry_uid = sentry_uid + 1
	self._attached_data = self._attach(nil, nil, self._unit)

	return 
end
SentryGunBase._on_picked_up_cash = function (self, unit)
	if unit and self._attached_data and self._attached_data.unit and self._attached_data.position and unit == self._attached_data.unit then
		local new_pos = self._unit:position()

		mvector3.set_z(new_pos, mvector3.z(self._attached_data.position))
		self._unit:set_position(new_pos)
		self._frame_callbacks:reset_counter("check_body")
	end

	return 
end
SentryGunBase.get_owner = function (self)
	return self._owner or (self._owner_id and managers.network:session() and managers.network:session():peer(self._owner_id) and managers.network:session():peer(self._owner_id):unit())
end
SentryGunBase.get_owner_id = function (self)
	return self._owner_id
end
SentryGunBase.get_type = function (self)
	return self._type or "sentry_gun"
end
SentryGunBase.update = function (self, unit, t, dt)
	self._frame_callbacks:update()

	return 
end
SentryGunBase.on_interaction = function (self)
	if Network:is_server() then
		SentryGunBase.on_picked_up(self.get_type(self), self._unit:weapon():ammo_ratio(), self._unit:id())
		self.remove(self)
	else
		managers.network:session():send_to_host("picked_up_sentry_gun", self._unit)
	end

	return 
end
SentryGunBase.on_picked_up = function (sentry_type, ammo_ratio, sentry_uid)
	local pm = managers.player

	pm.add_sentry_gun(pm, 1, sentry_type)

	local player_unit = pm.player_unit(pm)
	local ammo = SentryGunBase.DEPLOYEMENT_COST[pm.upgrade_value(pm, "sentry_gun", "cost_reduction", 1)]*ammo_ratio + 1
	local hud = managers.hud

	if player_unit then
		local deployement_cost = player_unit.equipment(player_unit) and player_unit.equipment(player_unit):get_sentry_deployement_cost(sentry_uid)
		local inventory = player_unit.inventory(player_unit)

		if inventory and deployement_cost then
			for index, weapon in pairs(inventory.available_selections(inventory)) do
				local refund = math.ceil(deployement_cost[index]*ammo_ratio)

				weapon.unit:base():add_ammo_in_bullets(refund)
				hud.set_ammo_amount(hud, index, weapon.unit:base():ammo_info())
			end

			player_unit.equipment(player_unit):remove_sentry_deployement_cost(sentry_uid)
		end
	end

	return 
end
SentryGunBase._check_body = function (self)
	if self._attached_data.index == 1 then
		if alive(self._attached_data.body) and not self._attached_data.body:enabled() then
			self._attached_data = self._attach(nil, nil, self._unit)

			if not self._attached_data then
				self.remove(self)

				return 
			end
		end
	elseif self._attached_data.index == 2 then
		if not alive(self._attached_data.body) or not mrotation.equal(self._attached_data.rotation, self._attached_data.body:rotation()) then
			self._attached_data = self._attach(nil, nil, self._unit)

			if not self._attached_data then
				self.remove(self)

				return 
			end
		end
	elseif self._attached_data.index == 3 and (not alive(self._attached_data.body) or mvector3.not_equal(self._attached_data.position, self._attached_data.body:position())) then
		self._attached_data = self._attach(nil, nil, self._unit)

		if not self._attached_data then
			self.remove(self)

			return 
		end
	end

	self._attached_data.index = ((self._attached_data.index < self._attached_data.max_index and self._attached_data.index) or 0) + 1

	return 
end
SentryGunBase.remove = function (self)
	self._removed = true

	self._unit:set_slot(0)

	return 
end
SentryGunBase._attach = function (pos, rot, sentrygun_unit)
	pos = pos or sentrygun_unit.position(sentrygun_unit)
	rot = rot or sentrygun_unit.rotation(sentrygun_unit)
	local from_pos = pos + rot.z(rot)*10
	local to_pos = pos + rot.z(rot)*-20
	local ray = nil

	if sentrygun_unit then
		ray = sentrygun_unit.raycast(sentrygun_unit, "ray", from_pos, to_pos, "slot_mask", managers.slot:get_mask("world_geometry"))
	else
		ray = World:raycast("ray", from_pos, to_pos, "slot_mask", managers.slot:get_mask("world_geometry"))
	end

	if ray then
		local attached_data = {
			max_index = 3,
			index = 1,
			body = ray.body,
			position = ray.body:position(),
			rotation = ray.body:rotation(),
			unit = ray.unit
		}

		return attached_data
	end

	return 
end
SentryGunBase.set_visibility_state = function (self, stage)
	local state = stage and true

	if self._visibility_state ~= state then
		self._unit:set_visible(state)

		self._visibility_state = state
	end

	self._lod_stage = stage

	return 
end
SentryGunBase.weapon_tweak_data = function (self)
	return tweak_data.weapon[self._unit:weapon()._name_id]
end
SentryGunBase.check_interact_blocked = function (self, player)
	local result = not alive(self._unit) or self._unit:character_damage():dead() or self._unit:weapon():ammo_ratio() == 1 or not self.get_net_event_id(self, player) or false

	return result
end
SentryGunBase.can_interact = function (self, player)
	return not self.check_interact_blocked(self, player)
end
SentryGunBase.show_blocked_hint = function (self, interaction_tweak_data, player, skip_hint)
	local event_id, wanted, possible = self.get_net_event_id(self, player)

	if self._unit:weapon():ammo_ratio() == 1 or not wanted then
		managers.hint:show_hint("hint_full_sentry_gun")
	else
		managers.hint:show_hint("hint_nea_sentry_gun")
	end

	return 
end
local refill_ratios = {
	1,
	0.9375,
	0.875,
	0.8125,
	0.75,
	0.6875,
	0.625,
	0.5625,
	0.5,
	0.4375,
	0.375,
	0.3125,
	0.25,
	0.1875,
	0.125,
	0.0625
}
SentryGunBase.get_net_event_id = function (self, player)
	local sentry_gun_reload_ratio = tweak_data.upgrades.sentry_gun_reload_ratio or 1

	if sentry_gun_reload_ratio == 0 then
		return 1
	end

	local ammo_needed = self._unit:weapon():ammo_ratio() - 1
	local ammo_got = 0
	local i = 0

	for id, weapon in pairs(player.inventory(player):available_selections()) do
		ammo_got = ammo_got + weapon.unit:base():get_ammo_ratio()
		i = i + 1
	end

	ammo_got = ammo_got/math.max(i, 1)
	local ammo_wanted = ammo_needed
	local wanted_event_id = nil
	local index = 1

	repeat
		if not refill_ratios[index] then
			break
		elseif refill_ratios[index] <= ammo_wanted then
			wanted_event_id = index
		end

		index = index + 1
	until wanted_event_id

	local ammo_possible = ammo_got/sentry_gun_reload_ratio
	local possible_event_id = nil
	local index = 1

	repeat
		if not refill_ratios[index] then
			break
		elseif refill_ratios[index] <= ammo_possible then
			possible_event_id = index
		end

		index = index + 1
	until possible_event_id

	local event_id = wanted_event_id and possible_event_id and math.max(wanted_event_id, possible_event_id)

	return event_id, wanted_event_id, possible_event_id
end
SentryGunBase.interaction_text_id = function (self)
	return "hud_interact_sentry_gun_switch_fire_mode"
end
SentryGunBase.add_string_macros = function (self, macroes)
	local event_id, wanted_event_id, possible_event_id = self.get_net_event_id(self, managers.player:local_player())
	macroes.AMMO = (wanted_event_id and string.format("%2.f%%", (refill_ratios[wanted_event_id] - 1)*100)) or "100%"

	return 
end
SentryGunBase.sync_net_event = function (self, event_id, peer)
	print("SentryGunBase:sync_net_event", event_id, inspect(peer), Network:is_server())

	local player = peer.unit(peer)
	local ammo_ratio = refill_ratios[event_id]

	self.refill(self, ammo_ratio)

	if alive(player) and alive(managers.player:local_player()) and player.key(player) == managers.player:local_player():key() then
		local sentry_gun_reload_ratio = tweak_data.upgrades.sentry_gun_reload_ratio or 1

		if 0 < sentry_gun_reload_ratio then
			local ammo_reduction = ammo_ratio*sentry_gun_reload_ratio
			local leftover = 0
			local weapon_list = {}

			for id, weapon in pairs(player.inventory(player):available_selections()) do
				local ammo_ratio = weapon.unit:base():get_ammo_ratio()

				if ammo_ratio < ammo_reduction then
					leftover = (leftover + ammo_reduction) - ammo_ratio
					weapon_list[id] = {
						unit = weapon.unit,
						amount = ammo_ratio,
						total = ammo_ratio
					}
				else
					weapon_list[id] = {
						unit = weapon.unit,
						amount = ammo_reduction,
						total = ammo_ratio
					}
				end
			end

			for id, data in pairs(weapon_list) do
				local ammo_left = data.total - data.amount

				if 0 < leftover and 0 < ammo_left then
					local extra_ammo = (ammo_left < leftover and ammo_left) or leftover
					leftover = leftover - extra_ammo
					data.amount = data.amount + extra_ammo
				end

				if 0 < data.amount then
					data.unit:base():reduce_ammo_by_procentage_of_total(data.amount)
					managers.hud:set_ammo_amount(id, data.unit:base():ammo_info())
				end
			end

			if 0 < leftover then
				Application:error("[SentryGunBase:sync_net_event]: Not all ammo was reducted from the weapons")
			end
		end
	end

	return 
end
SentryGunBase.refill = function (self, ammo_ratio)
	if self._unit:character_damage():dead() then
		return 
	end

	if Network:is_server() then
		local ammo_total = self._unit:weapon():ammo_total()
		local ammo_max = self._unit:weapon():ammo_max()

		self._unit:weapon():change_ammo(math.ceil(ammo_max*ammo_ratio))
	else
		self.set_waiting_for_refill(self, true)
	end

	self._unit:brain():switch_on()
	self._unit:interaction():set_dirty(true)

	return 
end
SentryGunBase.set_waiting_for_refill = function (self, state)
	self._waiting_for_refill = (state and true) or nil

	return 
end
SentryGunBase.waiting_for_refill = function (self)
	return self._waiting_for_refill
end
SentryGunBase.on_death = function (self)
	self._unit:set_extension_update_enabled(Idstring("base"), false)
	self.unregister(self)

	return 
end
SentryGunBase.enable_shield = function (self)
	self._has_shield = true

	self._unit:damage():run_sequence_simple("shield_on")

	return 
end
SentryGunBase.has_shield = function (self)
	return self._has_shield or false
end
SentryGunBase.unregister = function (self)
	if self._registered then
		self._registered = nil

		managers.groupai:state():unregister_criminal(self._unit)
	end

	return 
end
SentryGunBase.register = function (self)
	self._registered = true

	managers.groupai:state():register_criminal(self._unit)

	return 
end
SentryGunBase.save = function (self, save_data)
	local my_save_data = {}
	save_data.base = my_save_data
	my_save_data.tweak_table_id = self._tweak_table_id
	my_save_data.is_module = self._is_module

	return 
end
SentryGunBase.ammo_ratio = function (self)
	return self._unit:weapon():ammo_ratio()
end
SentryGunBase.load = function (self, save_data)
	self._was_dropin = true
	local my_save_data = save_data.base
	self._tweak_table_id = my_save_data.tweak_table_id
	self._is_module = my_save_data.is_module

	if self._is_module then
		local turret_units = managers.groupai:state():turrets()

		if not turret_units or not table.contains(turret_units, self._unit) then
			managers.groupai:state():register_turret(self._unit)
		end
	end

	return 
end
SentryGunBase.pre_destroy = function (self)
	SentryGunBase.super.pre_destroy(self, self._unit)
	managers.mission:remove_global_event_listener(self._sentry_uid)
	self.unregister(self)

	self._removed = true

	if self._validate_clbk_id then
		managers.enemy:remove_delayed_clbk(self._validate_clbk_id)

		self._validate_clbk_id = nil
	end

	self._unit:event_listener():call("on_destroy_unit")

	return 
end

return 
