SentryGunEquipment = SentryGunEquipment or class()
SentryGunEquipment._DAMAGE_EFFECTS_1 = 0.5
SentryGunEquipment._DAMAGE_EFFECTS_2 = 0.25
SentryGunEquipment.init = function (self, unit)
	self._unit = unit
	local event_listener = unit.event_listener(unit)

	event_listener.add(event_listener, "SentryGunEquipment_on_damage_received", {
		"on_damage_received"
	}, callback(self, self, "_on_damage_received_event"))
	event_listener.add(event_listener, "SentryGunEquipment_on_death_event", {
		"on_death"
	}, callback(self, self, "_on_death_event"))
	event_listener.add(event_listener, "SentryGunEquipment_on_destroy_unit", {
		"on_destroy_unit"
	}, callback(self, self, "_on_destroy_unit"))

	return 
end
SentryGunEquipment._on_damage_received_event = function (self, health_ratio)
	if health_ratio < self._DAMAGE_EFFECTS_2 then
		self._check_sound(self)

		if not self._second_pass_active then
			self._unit:damage():run_sequence_simple("damage_second_pass")
		end
	elseif health_ratio < self._DAMAGE_EFFECTS_1 then
		self._check_sound(self)
		self._unit:damage():run_sequence_simple("damage_first_pass")
	end

	return 
end
SentryGunEquipment._on_death_event = function (self)
	self._unit:sound_source():post_event("wp_sentrygun_destroy")
	self._unit:damage():run_sequence_simple("destroyed")

	return 
end
SentryGunEquipment._on_destroy_unit = function (self)
	if self._broken_loop_snd_event then
		self._broken_loop_snd_event:stop()

		self._broken_loop_snd_event = nil
	end

	self._unit:damage():run_sequence_simple("remove_effects")

	return 
end
SentryGunEquipment._check_sound = function (self)
	if not self._broken_loop_snd_event then
		self._broken_loop_snd_event = self._unit:sound_source():post_event("wp_sentrygun_broken_loop")
	end

	return 
end

return 
