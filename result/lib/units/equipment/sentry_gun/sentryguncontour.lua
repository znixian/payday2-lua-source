SentryGunContour = SentryGunContour or class()
SentryGunContour.init = function (self, unit)
	self._unit = unit

	unit.event_listener(unit):add("SentryGunContour_on_setup_event", {
		"on_setup"
	}, callback(self, self, "_on_setup_event"))

	return 
end
SentryGunContour._on_setup_event = function (self, is_owner)
	local event_listener = self._unit:event_listener()

	if (self._owner_only and is_owner) or not self._owner_only then
		self._current_contour_id = self.standard_contour_id(self)

		self._set_contour(self, self._current_contour_id)
		event_listener.add(event_listener, "SentryGunContour_on_switch_fire_mode_event", {
			"on_switch_fire_mode"
		}, callback(self, self, "_on_switch_fire_mode_event"))
		event_listener.add(event_listener, "SentryGunContour_on_out_of_ammo_event", {
			"on_out_of_ammo"
		}, callback(self, self, "_on_out_of_ammo_event"))
		event_listener.add(event_listener, "SentryGunContour_on_death_event", {
			"on_death"
		}, callback(self, self, "_on_death_event"))
	end

	event_listener.remove(event_listener, "SentryGunContour_on_setup_event")

	return 
end
SentryGunContour._on_switch_fire_mode_event = function (self, ap_bullets)
	if ap_bullets then
		self._set_contour(self, self.ap_contour_id(self))
	else
		self._set_contour(self, self.standard_contour_id(self))
	end

	return 
end
SentryGunContour._on_out_of_ammo_event = function (self)
	self._set_contour(self, self.out_of_ammo_contour_id(self))

	return 
end
SentryGunContour._on_death_event = function (self)
	self._remove_contour(self)

	return 
end
SentryGunContour.standard_contour_id = function (self)
	return self._standard_contour_id or "deployable_active"
end
SentryGunContour.ap_contour_id = function (self)
	return self._ap_contour_id or "deployable_interactable"
end
SentryGunContour.out_of_ammo_contour_id = function (self)
	return self._no_ammo_contour_id or "deployable_disabled"
end
SentryGunContour._set_contour = function (self, contour_id)
	local contour = self._unit:contour()

	if contour then
		if self._current_contour_id then
			contour.remove(contour, self._current_contour_id)
		end

		contour.add(contour, contour_id)

		self._current_contour_id = contour_id
	end

	return 
end
SentryGunContour._remove_contour = function (self)
	local contour = self._unit:contour()

	if contour then
		contour.remove(contour, self._current_contour_id)

		self._current_contour_id = nil
	end

	return 
end

return 
