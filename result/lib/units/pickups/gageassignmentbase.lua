GageAssignmentBase = GageAssignmentBase or class(Pickup)
GageAssignmentBase.init = function (self, unit)
	assert(managers.gage_assignment, "GageAssignmentManager not yet created!")
	GageAssignmentBase.super.init(self, unit)
	managers.gage_assignment:on_unit_spawned(unit)

	return 
end
GageAssignmentBase.sync_pickup = function (self, peer)
	if not alive(self._unit) then
		return 
	end

	if not managers.gage_assignment:is_unit_an_assignment(self._unit) then
		if Network:is_server() then
			self.consume(self)
		end

		return 
	end

	self._picked_up = true

	managers.gage_assignment:on_unit_interact(self._unit, self._assignment)

	if Network:is_server() then
		managers.network:session():send_to_peers_synched("sync_unit_event_id_16", self._unit, "base", (peer and peer.id(peer)) or 1)
		self.show_pickup_msg(self, (peer and peer.id(peer)) or 1)
		self.consume(self)
	end

	return 
end
GageAssignmentBase._pickup = function (self, unit)
	if self._picked_up then
		return 
	end

	if not alive(unit) or not alive(self._unit) then
		return 
	end

	if Network:is_server() then
		self.sync_pickup(self)
	else
		managers.network:session():send_to_host("sync_pickup", self._unit)
	end

	return true
end
GageAssignmentBase.show_pickup_msg = function (self, peer_id)
	if managers.network:session() then
		local peer = managers.network:session():peer(peer_id or 1)
	end

	if peer then
		managers.gage_assignment:present_progress(self._assignment, peer.name(peer))
	end

	return 
end
GageAssignmentBase.sync_net_event = function (self, event_id)
	if Network:is_client() then
		local peer_id = event_id or 1

		self.sync_pickup(self)
		self.show_pickup_msg(self, peer_id)
	end

	return 
end
GageAssignmentBase.assignment = function (self)
	return self._assignment
end
GageAssignmentBase.delete_unit = function (self)
	if alive(self._unit) then
		self._unit:set_slot(0)
	end

	return 
end
GageAssignmentBase.interact_blocked = function (self)
	return false
end

return 
