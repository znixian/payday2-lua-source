SpecialEquipmentPickup = SpecialEquipmentPickup or class(Pickup)
SpecialEquipmentPickup.init = function (self, unit)
	SpecialEquipmentPickup.super.init(self, unit)
	managers.occlusion:remove_occlusion(unit)

	return 
end
SpecialEquipmentPickup._pickup = function (self, unit)
	local equipment = unit.equipment(unit)

	if not unit.character_damage(unit):dead() and equipment and managers.player:can_pickup_equipment(self._special) then
		managers.player:add_special({
			name = self._special,
			amount = self._amount
		})

		if Network:is_client() then
			managers.network:session():send_to_host("sync_pickup", self._unit)
		end

		if self._global_event then
			managers.mission:call_global_event(self._global_event, unit)
		end

		unit.sound(unit):play("pickup_ammo", nil, true)
		self.consume(self)

		return true
	end

	return false
end
SpecialEquipmentPickup.destroy = function (self, ...)
	managers.occlusion:add_occlusion(self._unit)
	SpecialEquipmentPickup.super.destroy(self, ...)

	return 
end

return 
