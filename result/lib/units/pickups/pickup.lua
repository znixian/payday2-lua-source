Pickup = Pickup or class()
Pickup.init = function (self, unit)
	if not Network:is_server() and unit.slot(unit) == 23 then
		unit.set_slot(unit, 20)
	end

	self._unit = unit
	self._active = true

	return 
end
Pickup.sync_pickup = function (self)
	self.consume(self)

	return 
end
Pickup._pickup = function (self)
	Application:error("Pickup didn't have a _pickup() function!")

	return 
end
Pickup.pickup = function (self, unit)
	if not self._active then
		return 
	end

	return self._pickup(self, unit)
end
Pickup.consume = function (self)
	self.delete_unit(self)

	return 
end
Pickup.set_active = function (self, active)
	self._active = active

	return 
end
Pickup.delete_unit = function (self)
	World:delete_unit(self._unit)

	return 
end
Pickup.save = function (self, data)
	local state = {
		active = self._active
	}
	data.Pickup = state

	return 
end
Pickup.load = function (self, data)
	local state = data.Pickup

	if state then
		self.set_active(self, state.active)
	end

	return 
end
Pickup.sync_net_event = function (self, event, peer)
	return 
end
Pickup.destroy = function (self, unit)
	return 
end

return 
