TripMineBase = TripMineBase or class(UnitBase)
TripMineBase.EVENT_IDS = {
	sensor_beep = 1,
	explosion_beep = 2
}
TripMineBase.spawn = function (pos, rot, sensor_upgrade, peer_id)
	local unit = World:spawn_unit(Idstring("units/payday2/equipment/gen_equipment_tripmine/gen_equipment_tripmine"), pos, rot)

	managers.network:session():send_to_peers_synched("sync_trip_mine_setup", unit, sensor_upgrade, peer_id or 0)
	unit.base(unit):setup(sensor_upgrade)

	return unit
end
TripMineBase.set_server_information = function (self, peer_id)
	self._server_information = {
		owner_peer_id = peer_id
	}

	managers.network:session():peer(peer_id):set_used_deployable(true)

	return 
end
TripMineBase.server_information = function (self)
	return self._server_information
end
TripMineBase.init = function (self, unit)
	UnitBase.init(self, unit, false)

	self._unit = unit
	self._position = self._unit:position()
	self._rotation = self._unit:rotation()
	self._forward = self._rotation:y()
	self._ray_from_pos = Vector3()
	self._ray_to_pos = Vector3()
	self._init_length = 500
	self._length = self._init_length
	self._ids_laser = Idstring("laser")
	self._g_laser = self._unit:get_object(Idstring("g_laser"))
	self._g_laser_sensor = self._unit:get_object(Idstring("g_laser_sensor"))
	self._use_draw_laser = SystemInfo:platform() == Idstring("PS3")

	if self._use_draw_laser then
		self._laser_color = Color(0.15, 1, 0, 0)
		self._laser_sensor_color = Color(0.15, 0.1, 0.1, 1)
		self._laser_brush = Draw:brush(self._laser_color)

		self._laser_brush:set_blend_mode("opacity_add")
	end

	if Network:is_client() then
		self._validate_clbk_id = "trip_mine_validate" .. tostring(unit.key(unit))

		managers.enemy:add_delayed_clbk(self._validate_clbk_id, callback(self, self, "_clbk_validate"), Application:time() + 60)
	end

	return 
end
TripMineBase._clbk_validate = function (self)
	self._validate_clbk_id = nil

	if not self._was_dropin then
		local peer = managers.network:session():server_peer()

		peer.mark_cheater(peer, VoteManager.REASON.many_assets)
	end

	return 
end
TripMineBase.get_name_id = function (self)
	return "trip_mine"
end
TripMineBase.interaction_text_id = function (self)
	return (self._sensor_upgrade and ((self.armed(self) and "hud_int_equipment_sensor_mode_trip_mine") or "hud_int_equipment_normal_mode_trip_mine")) or "debug_interact_trip_mine"
end
TripMineBase.is_category = function (self, ...)
	for _, cat in ipairs({
		...
	}) do
		if cat == "trip_mine" then
			return true
		end
	end

	return false
end
TripMineBase.sync_setup = function (self, sensor_upgrade)
	if self._validate_clbk_id then
		managers.enemy:remove_delayed_clbk(self._validate_clbk_id)

		self._validate_clbk_id = nil
	end

	self.setup(self, sensor_upgrade)

	return 
end
TripMineBase.setup = function (self, sensor_upgrade)
	self._slotmask = managers.slot:get_mask("trip_mine_targets")
	self._first_armed = false
	self._armed = false

	if sensor_upgrade then
		self._startup_armed = not managers.groupai:state():whisper_mode()
	else
		self._startup_armed = true
	end

	self._sensor_upgrade = sensor_upgrade

	self.set_active(self, false)
	self._unit:sound_source():post_event("trip_mine_attach")

	local upgrade = managers.player:has_category_upgrade("trip_mine", "can_switch_on_off") or managers.player:has_category_upgrade("trip_mine", "sensor_toggle")

	self._unit:contour():add((upgrade and "deployable_interactable") or "deployable_active")

	return 
end
TripMineBase.set_active = function (self, active, owner)
	self._active = active

	self._unit:set_extension_update_enabled(Idstring("base"), self._active or self._use_draw_laser)

	if self._active then
		self._owner = owner
		self._owner_peer_id = managers.network:session():local_peer():id()
		local from_pos = self._unit:position() + self._unit:rotation():y()*10
		local to_pos = from_pos + self._unit:rotation():y()*self._init_length
		local ray = self._unit:raycast("ray", from_pos, to_pos, "slot_mask", managers.slot:get_mask("world_geometry"))
		self._length = math.clamp((ray and ray.distance + 10) or self._init_length, 0, self._init_length)

		self._unit:anim_set_time(self._ids_laser, self._length/self._init_length)

		self._activate_timer = 3

		mvector3.set(self._ray_from_pos, self._position)
		mvector3.set(self._ray_to_pos, self._forward)
		mvector3.multiply(self._ray_to_pos, self._length)
		mvector3.add(self._ray_to_pos, self._ray_from_pos)

		local from_pos = self._unit:position() + self._unit:rotation():y()*10
		local to_pos = self._unit:position() + self._unit:rotation():y()*-10
		local ray = self._unit:raycast("ray", from_pos, to_pos, "slot_mask", managers.slot:get_mask("world_geometry"))

		if ray then
			self._attached_data = {
				body = ray.body,
				position = ray.body:position(),
				rotation = ray.body:rotation(),
				index = 1,
				max_index = 3
			}
		end

		self._alert_filter = owner.movement(owner):SO_access()
	elseif self._use_draw_laser then
		local from_pos = self._unit:position() + self._unit:rotation():y()*10
		local to_pos = from_pos + self._unit:rotation():y()*self._init_length
		local ray = self._unit:raycast("ray", from_pos, to_pos, "slot_mask", managers.slot:get_mask("world_geometry"))
		self._length = math.clamp((ray and ray.distance + 10) or self._init_length, 0, self._init_length)

		mvector3.set(self._ray_from_pos, self._position)
		mvector3.set(self._ray_to_pos, self._forward)
		mvector3.multiply(self._ray_to_pos, self._length)
		mvector3.add(self._ray_to_pos, self._ray_from_pos)
	end

	return 
end
TripMineBase.active = function (self)
	return self._active
end
TripMineBase.armed = function (self)
	if self._activate_timer then
		return self._startup_armed
	end

	return self._armed
end
TripMineBase._set_armed = function (self, armed)
	if not self._activate_timer then
		self._armed = armed

		self._g_laser:set_visibility(self._armed)
		self._g_laser_sensor:set_visibility(self._sensor_upgrade and not self._armed)

		if self._use_draw_laser then
			self._laser_brush:set_color((self._armed and self._laser_color) or (self._sensor_upgrade and self._laser_sensor_color) or self._laser_color)
		end

		if not self._first_armed then
			self._first_armed = true
			self._activate_timer = nil

			self._unit:sound_source():post_event("trip_mine_beep_armed")
		end
	else
		self._startup_armed = armed
	end

	self._unit:sound_source():post_event((self._armed and "trip_mine_arm") or "trip_mine_disarm")
	self._unit:interaction():set_dirty(true)

	return 
end
TripMineBase.set_armed = function (self, armed)
	if not managers.network:session() then
		return 
	end

	self._set_armed(self, armed)

	if managers.network:session() then
		managers.network:session():send_to_peers_synched("sync_trip_mine_set_armed", self._unit, self._armed, self._length)
	end

	return 
end
TripMineBase.sync_trip_mine_set_armed = function (self, armed, length)
	if not self._owner then
		self._length = length

		self._unit:anim_set_time(self._ids_laser, length/self._init_length)
	end

	self._set_armed(self, armed)

	return 
end
TripMineBase.contour_selected = function (self)
	self._unit:contour():add("deployable_selected")

	return 
end
TripMineBase.contour_unselected = function (self)
	self._unit:contour():remove("deployable_selected")

	return 
end
TripMineBase._update_draw_laser = function (self)
	if self._use_draw_laser and self._first_armed and (self._armed or self._sensor_upgrade) then
		self._laser_brush:cylinder(self._ray_from_pos, self._ray_to_pos, 0.5)
	end

	return 
end
TripMineBase.update = function (self, unit, t, dt)
	self._update_draw_laser(self)

	if not self._owner then
		return 
	end

	self._check_body(self)

	if self._explode_timer then
		self._explode_timer = self._explode_timer - dt

		if self._explode_timer <= 0 then
			self._explode(self, self._explode_ray)

			return 
		end
	end

	if self._activate_timer then
		self._activate_timer = self._activate_timer - dt

		if self._activate_timer <= 0 then
			self._activate_timer = nil

			self.set_armed(self, self._startup_armed)

			self._startup_armed = nil
		end

		return 
	end

	if self._deactive_timer then
		self._deactive_timer = self._deactive_timer - dt

		if self._deactive_timer <= 0 then
			self._deactive_timer = nil
		end

		return 
	end

	if not self._armed then
		if self._sensor_upgrade then
			self._sensor(self, t)

			if self._sensor_units_detected and self._sensor_last_unit_time and self._sensor_last_unit_time < t then
				self._sensor_units_detected = nil
				self._sensor_last_unit_time = nil
			end
		end

		return 
	end

	if not self._explode_timer then
		self._check(self)
	end

	return 
end
TripMineBase._raycast = function (self)
	return self._unit:raycast("ray", self._ray_from_pos, self._ray_to_pos, "slot_mask", self._slotmask, "ray_type", "trip_mine body")
end
TripMineBase._sensor = function (self, t)
	local ray = self._raycast(self)

	if ray and ray.unit and not tweak_data.character[ray.unit:base()._tweak_table].is_escort then
		self._sensor_units_detected = self._sensor_units_detected or {}

		if not self._sensor_units_detected[ray.unit:key()] then
			self._sensor_units_detected[ray.unit:key()] = true

			if managers.player:has_category_upgrade("trip_mine", "sensor_highlight") and ((managers.groupai:state():whisper_mode() and tweak_data.character[ray.unit:base()._tweak_table].silent_priority_shout) or tweak_data.character[ray.unit:base()._tweak_table].priority_shout) then
				managers.game_play_central:auto_highlight_enemy(ray.unit, true)
				self._emit_sensor_sound_and_effect(self)

				if managers.network:session() then
					managers.network:session():send_to_peers_synched("sync_unit_event_id_16", self._unit, "base", TripMineBase.EVENT_IDS.sensor_beep)
				end
			end

			self._sensor_last_unit_time = t + 5
		end
	end

	return 
end
TripMineBase.sync_trip_mine_beep_sensor = function (self)
	self._emit_sensor_sound_and_effect(self)

	return 
end
TripMineBase._check_body = function (self)
	if not self._attached_data then
		return 
	end

	if self._attached_data.index == 1 then
		if not alive(self._attached_data.body) or not self._attached_data.body:enabled() then
			self.explode(self)
		end
	elseif self._attached_data.index == 2 then
		if not alive(self._attached_data.body) or not mrotation.equal(self._attached_data.rotation, self._attached_data.body:rotation()) then
			self.explode(self)
		end
	elseif self._attached_data.index == 3 and (not alive(self._attached_data.body) or mvector3.not_equal(self._attached_data.position, self._attached_data.body:position())) then
		self.explode(self)
	end

	self._attached_data.index = ((self._attached_data.index < self._attached_data.max_index and self._attached_data.index) or 0) + 1

	return 
end
TripMineBase._check = function (self)
	if not managers.network:session() then
		return 
	end

	local ray = self._raycast(self)

	if ray and ray.unit and not tweak_data.character[ray.unit:base()._tweak_table].is_escort then
		if ray.unit:movement() and ray.unit:movement():team() then
			local team_id_player = tweak_data.levels:get_default_team_ID("player")
			local team_id_ray = ray.unit:movement():team().id

			if not managers.groupai:state():team_data(team_id_player).foes[team_id_ray] then
				return 
			end
		end

		self._explode_timer = tweak_data.weapon.trip_mines.delay + managers.player:upgrade_value("trip_mine", "explode_timer_delay", 0)
		self._explode_ray = ray

		self._unit:sound_source():post_event("trip_mine_beep_explode")

		if managers.network:session() then
			managers.network:session():send_to_peers_synched("sync_unit_event_id_16", self._unit, "base", TripMineBase.EVENT_IDS.explosion_beep)
		end
	end

	return 
end
TripMineBase.sync_trip_mine_beep_explode = function (self)
	self._unit:sound_source():post_event("trip_mine_beep_explode")

	return 
end
TripMineBase.explode = function (self)
	if not self._active then
		return 
	end

	self._active = false
	local col_ray = {
		ray = self._forward,
		position = self._position
	}

	self._explode(self, col_ray)

	return 
end
TripMineBase._explode = function (self, col_ray)
	if not managers.network:session() then
		return 
	end

	local damage_size = tweak_data.weapon.trip_mines.damage_size*managers.player:upgrade_value("trip_mine", "explosion_size_multiplier_1", 1)*managers.player:upgrade_value("trip_mine", "damage_multiplier", 1)
	local player = managers.player:player_unit()

	managers.explosion:give_local_player_dmg(self._position, damage_size, tweak_data.weapon.trip_mines.player_damage)
	self._unit:set_extension_update_enabled(Idstring("base"), false)

	self._deactive_timer = 5

	self._play_sound_and_effects(self)

	local slotmask = managers.slot:get_mask("explosion_targets")
	local bodies = World:find_bodies("intersect", "cylinder", self._ray_from_pos, self._ray_to_pos, damage_size, slotmask)
	local damage = tweak_data.weapon.trip_mines.damage*managers.player:upgrade_value("trip_mine", "damage_multiplier", 1)
	local characters_hit = {}

	for _, hit_body in ipairs(bodies) do
		if alive(hit_body) then
			local character = hit_body.unit(hit_body):character_damage() and hit_body.unit(hit_body):character_damage().damage_explosion
			local apply_dmg = hit_body.extension(hit_body) and hit_body.extension(hit_body).damage
			local dir, ray_hit = nil

			if character and not characters_hit[hit_body.unit(hit_body):key()] then
				local com = hit_body.center_of_mass(hit_body)
				local ray_from = math.point_on_line(self._ray_from_pos, self._ray_to_pos, com)
				ray_hit = not World:raycast("ray", ray_from, com, "slot_mask", slotmask, "ignore_unit", {
					hit_body.unit(hit_body)
				}, "report")

				if ray_hit then
					characters_hit[hit_body.unit(hit_body):key()] = true
				end
			elseif apply_dmg or hit_body.dynamic(hit_body) then
				ray_hit = true
			end

			if ray_hit then
				dir = hit_body.center_of_mass(hit_body)

				mvector3.direction(dir, self._ray_from_pos, dir)

				if apply_dmg then
					local normal = dir
					local prop_damage = math.min(damage, 200)
					local network_damage = math.ceil(prop_damage*163.84)
					prop_damage = network_damage/163.84

					hit_body.extension(hit_body).damage:damage_explosion(player, normal, hit_body.position(hit_body), dir, prop_damage)
					hit_body.extension(hit_body).damage:damage_damage(player, normal, hit_body.position(hit_body), dir, prop_damage)

					if hit_body.unit(hit_body):id() ~= -1 then
						if player then
							managers.network:session():send_to_peers_synched("sync_body_damage_explosion", hit_body, player, normal, hit_body.position(hit_body), dir, math.min(32768, network_damage))
						else
							managers.network:session():send_to_peers_synched("sync_body_damage_explosion_no_attacker", hit_body, normal, hit_body.position(hit_body), dir, math.min(32768, network_damage))
						end
					end
				end

				if hit_body.unit(hit_body):in_slot(managers.game_play_central._slotmask_physics_push) then
					hit_body.unit(hit_body):push(5, dir*500)
				end

				if character then
					self._give_explosion_damage(self, col_ray, hit_body.unit(hit_body), damage)
				end
			end
		end
	end

	if managers.network:session() then
		if managers.player:has_category_upgrade("trip_mine", "fire_trap") then
			local fire_trap_data = managers.player:upgrade_value("trip_mine", "fire_trap", nil)

			if fire_trap_data then
				managers.network:session():send_to_peers_synched("sync_trip_mine_explode_spawn_fire", self._unit, player, self._ray_from_pos, self._ray_to_pos, damage_size, damage, fire_trap_data[1], fire_trap_data[2])
				self._spawn_environment_fire(self, player, fire_trap_data[1], fire_trap_data[2])
			end
		elseif player then
			managers.network:session():send_to_peers_synched("sync_trip_mine_explode", self._unit, player, self._ray_from_pos, self._ray_to_pos, damage_size, damage)
		else
			managers.network:session():send_to_peers_synched("sync_trip_mine_explode_no_user", self._unit, self._ray_from_pos, self._ray_to_pos, damage_size, damage)
		end
	end

	local alert_event = {
		"aggression",
		self._position,
		tweak_data.weapon.trip_mines.alert_radius,
		self._alert_filter,
		self._unit
	}

	managers.groupai:state():propagate_alert(alert_event)

	if Network:is_server() then
		managers.mission:call_global_event("tripmine_exploded")
		Application:error("TRIPMINE EXPLODED")
	end

	self._unit:set_slot(0)

	return 
end
TripMineBase.sync_trip_mine_explode_and_spawn_fire = function (self, user_unit, ray_from, ray_to, damage_size, damage, added_time, range_multiplier)
	self._spawn_environment_fire(self, user_unit, added_time, range_multiplier)
	self.sync_trip_mine_explode(self, user_unit, ray_from, ray_to, damage_size, damage)

	return 
end
TripMineBase.sync_trip_mine_explode = function (self, user_unit, ray_from, ray_to, damage_size, damage)
	self._play_sound_and_effects(self)
	self._unit:set_slot(0)
	managers.explosion:give_local_player_dmg(self._position, damage_size, tweak_data.weapon.trip_mines.player_damage)

	local bodies = World:find_bodies("intersect", "cylinder", ray_from, ray_to, damage_size, managers.slot:get_mask("explosion_targets"))

	for _, hit_body in ipairs(bodies) do
		local apply_dmg = hit_body.extension(hit_body) and hit_body.extension(hit_body).damage
		local dir = nil

		if apply_dmg or hit_body.dynamic(hit_body) then
			dir = hit_body.center_of_mass(hit_body)

			mvector3.direction(dir, ray_from, dir)

			if apply_dmg then
				local normal = dir

				if hit_body.unit(hit_body):id() == -1 then
					hit_body.extension(hit_body).damage:damage_explosion(user_unit, normal, hit_body.position(hit_body), dir, damage)
					hit_body.extension(hit_body).damage:damage_damage(user_unit, normal, hit_body.position(hit_body), dir, damage)
				end
			end

			if hit_body.unit(hit_body):in_slot(managers.game_play_central._slotmask_physics_push) then
				hit_body.unit(hit_body):push(5, dir*500)
			end
		end
	end

	if Network:is_server() then
		managers.mission:call_global_event("tripmine_exploded")
		Application:error("TRIPMINE EXPLODED")
	end

	return 
end
TripMineBase._spawn_environment_fire = function (self, user_unit, added_time, range_multiplier)
	local position = self._unit:position()
	local rotation = self._unit:rotation()
	local data = tweak_data.env_effect:trip_mine_fire()
	local normal = self._unit:rotation():y()
	local dir = Vector3()

	mvector3.set(dir, normal)
	mvector3.multiply(dir, 20)
	mvector3.add(position, dir)
	EnvironmentFire.spawn(position, rotation, data, normal, user_unit, added_time, range_multiplier)

	return 
end
TripMineBase._play_sound_and_effects = function (self)
	World:effect_manager():spawn({
		effect = Idstring("effects/particles/explosions/explosion_grenade"),
		position = self._unit:position(),
		normal = self._unit:rotation():y()
	})

	local sound_source = SoundDevice:create_source("TripMineBase")

	sound_source.set_position(sound_source, self._unit:position())
	sound_source.post_event(sound_source, "trip_mine_explode")
	managers.enemy:add_delayed_clbk("TrMiexpl", callback(TripMineBase, TripMineBase, "_dispose_of_sound", {
		sound_source = sound_source
	}), TimerManager:game():time() + 4)

	return 
end
TripMineBase._emit_sensor_sound_and_effect = function (self)
	self._unit:sound_source():post_event("trip_mine_sensor_alarm")

	return 
end
TripMineBase._dispose_of_sound = function (...)
	return 
end
TripMineBase.sync_net_event = function (self, event_id)
	if event_id == TripMineBase.EVENT_IDS.sensor_beep then
		self.sync_trip_mine_beep_sensor(self)
	elseif event_id == TripMineBase.EVENT_IDS.explosion_beep then
		self.sync_trip_mine_beep_explode(self)
	end

	return 
end
TripMineBase._give_explosion_damage = function (self, col_ray, unit, damage)
	local action_data = {
		variant = "explosion",
		damage = damage,
		weapon_unit = self._unit,
		attacker_unit = managers.player:player_unit(),
		col_ray = col_ray,
		owner = managers.player:player_unit(),
		owner_peer_id = self._owner_peer_id
	}
	local defense_data = unit.character_damage(unit):damage_explosion(action_data)

	return defense_data
end
TripMineBase.save = function (self, data)
	local state = {
		armed = self._armed,
		length = self._length,
		first_armed = self._first_armed,
		sensor_upgrade = self._sensor_upgrade,
		ray_from_pos = self._ray_from_pos,
		ray_to_pos = self._ray_to_pos
	}
	data.TripMineBase = state

	return 
end
TripMineBase.load = function (self, data)
	local state = data.TripMineBase
	self._init_length = 500
	self._first_armed = state.first_armed
	self._sensor_upgrade = state.sensor_upgrade
	self._ray_from_pos = state.ray_from_pos
	self._ray_to_pos = state.ray_to_pos

	if self._use_draw_laser then
		self._unit:set_extension_update_enabled(Idstring("base"), self._use_draw_laser)
	end

	self.sync_trip_mine_set_armed(self, state.armed, state.length)

	self._was_dropin = true

	return 
end
TripMineBase._debug_draw = function (self, from, to)
	local brush = Draw:brush(Color.red:with_alpha(0.5))

	brush.cylinder(brush, from, to, 1)

	return 
end
TripMineBase.destroy = function (self)
	if self._validate_clbk_id then
		managers.enemy:remove_delayed_clbk(self._validate_clbk_id)

		self._validate_clbk_id = nil
	end

	return 
end

return 
