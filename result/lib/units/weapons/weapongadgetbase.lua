WeaponGadgetBase = WeaponGadgetBase or class(UnitBase)
WeaponGadgetBase.GADGET_TYPE = ""
WeaponGadgetBase.init = function (self, unit)
	WeaponGadgetBase.super.init(self, unit)

	self._on = false

	return 
end
WeaponGadgetBase.set_npc = function (self)
	return 
end
WeaponGadgetBase.set_state = function (self, on, sound_source, current_state)
	if not self.is_bipod(self) and not self.is_underbarrel(self) then
		self._set_on(self, on, sound_source)
	end

	self._check_state(self, current_state)

	return 
end
WeaponGadgetBase._set_on = function (self, on, sound_source)
	if self._on ~= on and sound_source and (self._on_event or self._off_event) then
		sound_source.post_event(sound_source, (on and self._on_event) or self._off_event)
	end

	self._on = on

	return 
end
WeaponGadgetBase.is_usable = function (self)
	return true
end
WeaponGadgetBase.set_on = function (self)
	self._on = true

	self._check_state(self)

	return 
end
WeaponGadgetBase.set_off = function (self)
	self._on = false

	self._check_state(self)

	return 
end
WeaponGadgetBase.toggle = function (self)
	self._on = not self._on

	self._check_state(self)

	return 
end
WeaponGadgetBase.is_on = function (self)
	return self._on
end
WeaponGadgetBase.toggle_requires_stance_update = function (self)
	return false
end
WeaponGadgetBase._check_state = function (self, current_state)
	return 
end
WeaponGadgetBase.is_bipod = function (self)
	return false
end
WeaponGadgetBase.is_underbarrel = function (self)
	return false
end
WeaponGadgetBase.overrides_weapon_firing = function (self)
	return self.is_underbarrel(self)
end
WeaponGadgetBase.destroy = function (self, unit)
	WeaponGadgetBase.super.pre_destroy(self, unit)

	return 
end

return 
