AkimboShotgunBase = AkimboShotgunBase or class(AkimboWeaponBase)
AkimboShotgunBase.init = function (self, ...)
	AkimboShotgunBase.super.init(self, ...)
	self.setup_default(self)

	return 
end
AkimboShotgunBase.setup_default = function (self)
	ShotgunBase.setup_default(self)

	return 
end
AkimboShotgunBase._fire_raycast = function (self, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
	return ShotgunBase._fire_raycast(self, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
end
AkimboShotgunBase.get_damage_falloff = function (self, damage, col_ray, user_unit)
	return ShotgunBase.get_damage_falloff(self, damage, col_ray, user_unit)
end
AkimboShotgunBase.run_and_shoot_allowed = function (self)
	return ShotgunBase.run_and_shoot_allowed(self)
end
AkimboShotgunBase._update_stats_values = function (self)
	ShotgunBase._update_stats_values(self)

	return 
end
NPCAkimboShotgunBase = NPCAkimboShotgunBase or class(NPCAkimboWeaponBase)

return 
