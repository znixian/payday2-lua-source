WeaponUnderbarrel = WeaponUnderbarrel or class(WeaponGadgetBase)
WeaponUnderbarrel.GADGET_TYPE = "underbarrel"
WeaponUnderbarrel.init = function (self, unit)
	self._unit = unit
	self._is_npc = false
	self._tweak_data = tweak_data.weapon[self.name_id]
	self._deployed = false

	self.setup_underbarrel(self)

	return 
end
WeaponUnderbarrel.destroy = function (self, unit)
	return 
end
WeaponUnderbarrel.setup_underbarrel = function (self)
	self._ammo = WeaponAmmo:new(self.name_id, self._tweak_data.CLIP_AMMO_MAX, self._tweak_data.AMMO_MAX)

	return 
end
WeaponUnderbarrel.check_state = function (self)
	if self._is_npc then
		return false
	end

	self.toggle(self)

	return 
end
WeaponUnderbarrel._fire_raycast = function (self, weapon_base, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
	return {}
end
WeaponUnderbarrel.start_shooting_allowed = function (self)
	if not self._next_fire_t or self._next_fire_t <= TimerManager:main():time() then
		return true
	end

	return false
end
WeaponUnderbarrel.on_shot = function (self)
	self._next_fire_t = TimerManager:main():time() + ((self._tweak_data.fire_mode_data and self._tweak_data.fire_mode_data.fire_rate) or 0)

	return 
end
WeaponUnderbarrel.fire_mode = function (self)
	return self._tweak_data.FIRE_MODE
end
WeaponUnderbarrel.can_toggle_firemode = function (self)
	return self._tweak_data.CAN_TOGGLE_FIREMODE
end
WeaponUnderbarrel.is_single_shot = function (self)
	return self._tweak_data.FIRE_MODE == "single"
end
WeaponUnderbarrel.replenish = function (self)
	self._ammo:replenish()

	return 
end
WeaponUnderbarrel.ammo_base = function (self)
	return self._ammo
end
WeaponUnderbarrel._get_sound_event = function (self, weapon, event, alternative_event)
	local str_name = self.name_id

	if not weapon.third_person_important or not weapon.third_person_important(weapon) then
		str_name = self.name_id:gsub("_crew", "")
	end

	local sounds = self._tweak_data.sounds
	local event = sounds and (sounds[event] or sounds[alternative_event])

	return event
end
WeaponUnderbarrel._get_tweak_data_weapon_animation = function (self, anim)
	return "bipod_" .. anim
end
WeaponUnderbarrel._spawn_muzzle_effect = function (self)
	return true
end
WeaponUnderbarrel._spawn_shell_eject_effect = function (self)
	return true
end
WeaponUnderbarrel._check_state = function (self, current_state)
	WeaponUnderbarrel.super._check_state(self, current_state)

	if self._anim_state ~= self._on then
		self._anim_state = self._on

		self.play_anim(self)
	end

	return 
end
WeaponUnderbarrel.play_anim = function (self)
	if not self._anim then
		return 
	end

	local length = self._unit:anim_length(self._anim)
	local speed = (self._anim_state and 1) or -1

	self._unit:anim_play_to(self._anim, (self._anim_state and length) or 0, speed)

	return 
end
WeaponUnderbarrel.reload_prefix = function (self)
	return "underbarrel_"
end
WeaponUnderbarrel.is_underbarrel = function (self)
	return true
end
WeaponUnderbarrel.toggle_requires_stance_update = function (self)
	return true
end

return 
