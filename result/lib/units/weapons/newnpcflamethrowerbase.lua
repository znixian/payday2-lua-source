NewNPCFlamethrowerBase = NewNPCFlamethrowerBase or class(NewNPCRaycastWeaponBase)
NewNPCFlamethrowerBase.init = function (self, ...)
	NewNPCFlamethrowerBase.super.init(self, ...)
	self.setup_default(self)

	return 
end
NewNPCFlamethrowerBase.setup_default = function (self)
	self._use_shell_ejection_effect = false
	self._use_trails = false

	return 
end
NewNPCFlamethrowerBase._spawn_muzzle_effect = function (self, from_pos, direction)
	return 
end
NewNPCFlamethrowerBase.update = function (self, unit, t, dt)
	if self._check_shooting_expired and self._check_shooting_expired.check_t < t then
		self._check_shooting_expired = nil

		self._unit:set_extension_update_enabled(Idstring("base"), false)
		SawWeaponBase._stop_sawing_effect(self)
		self.play_tweak_data_sound(self, "stop_fire")
	end

	return 
end
NewNPCFlamethrowerBase.fire_blank = function (self, direction, impact)
	if not self._check_shooting_expired then
		self.play_tweak_data_sound(self, "fire")
	end

	self._check_shooting_expired = {
		check_t = Application:time() + 0.3
	}

	self._unit:set_extension_update_enabled(Idstring("base"), true)
	self._unit:flamethrower_effect_extension():_spawn_muzzle_effect(self._obj_fire:position(), direction)

	return 
end
NewNPCFlamethrowerBase.auto_fire_blank = function (self, direction, impact)
	self.fire_blank(self, direction, impact)

	return 
end
NewNPCFlamethrowerBase._sound_autofire_start = function (self, nr_shots)
	local tweak_sound = tweak_data.weapon[self._name_id].sounds or {}

	self._sound_fire:stop()

	local sound = self._sound_fire:post_event(tweak_sound.fire, callback(self, self, "_on_auto_fire_stop"), nil, "end_of_event")
	sound = sound or self._sound_fire:post_event(tweak_sound.fire)

	return 
end
NewNPCFlamethrowerBase._sound_autofire_end = function (self)
	local tweak_sound = tweak_data.weapon[self._name_id].sounds or {}
	local sound = self._sound_fire:post_event(tweak_sound.stop_fire)
	sound = sound or self._sound_fire:post_event(tweak_sound.stop_fire)

	return 
end
NewNPCFlamethrowerBase.third_person_important = function (self)
	return NewFlamethrowerBase.third_person_important(self)
end

return 
