NPCGrenadeLauncherBase = NPCGrenadeLauncherBase or class(NPCRaycastWeaponBase)
NPCGrenadeLauncherBase.init = function (self, ...)
	NPCGrenadeLauncherBase.super.init(self, ...)

	return 
end
NPCGrenadeLauncherBase.fire_blank = function (self, direction, impact)
	World:effect_manager():spawn(self._muzzle_effect_table)
	self._sound_singleshot(self)

	return 
end

return 
