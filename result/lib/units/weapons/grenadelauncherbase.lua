GrenadeLauncherBase = GrenadeLauncherBase or class(ProjectileWeaponBase)
GrenadeLauncherContinousReloadBase = GrenadeLauncherContinousReloadBase or class(GrenadeLauncherBase)
GrenadeLauncherContinousReloadBase.init = function (self, ...)
	GrenadeLauncherContinousReloadBase.super.init(self, ...)

	self._use_shotgun_reload = true

	return 
end
GrenadeLauncherContinousReloadBase.shotgun_shell_data = function (self)
	return nil
end

return 
