SmokeGrenade = SmokeGrenade or class(GrenadeBase)
SmokeGrenade.init = function (self, unit)
	self._init_timer = 1.5

	SmokeGrenade.super.init(self, unit)

	return 
end
SmokeGrenade.update = function (self, unit, t, dt)
	SmokeGrenade.super.update(self, unit, t, dt)

	if self._smoke_timer then
		self._smoke_timer = self._smoke_timer - dt

		if self._smoke_timer <= 0 then
			self._smoke_timer = nil

			World:effect_manager():fade_kill(self._smoke_effect)
		end
	end

	return 
end
SmokeGrenade._detonate = function (self)
	local units = World:find_units("sphere", self._unit:position(), 400, self._slotmask)

	for _, unit in ipairs(units) do
		local col_ray = {
			ray = unit.position(unit) - self._unit:position():normalized(),
			position = self._unit:position()
		}

		self._give_smoke_damage(self, col_ray, unit, 10)
	end

	return 
end
SmokeGrenade._play_sound_and_effects = function (self)
	World:effect_manager():spawn({
		effect = Idstring("effects/particles/explosions/explosion_smoke_grenade"),
		position = self._unit:position(),
		normal = self._unit:rotation():y()
	})
	self._unit:sound_source():post_event("trip_mine_explode")

	local parent = self._unit:orientation_object()
	self._smoke_effect = World:effect_manager():spawn({
		effect = Idstring("effects/particles/explosions/smoke_grenade_smoke"),
		parent = parent
	})
	self._smoke_timer = 10

	return 
end
SmokeGrenade._give_smoke_damage = function (self, col_ray, unit, damage)
	return 
end
SmokeGrenade.destroy = function (self)
	if self._smoke_effect then
		World:effect_manager():kill(self._smoke_effect)
	end

	SmokeGrenade.super.destroy(self)

	return 
end

return 
