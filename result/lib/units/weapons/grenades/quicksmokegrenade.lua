QuickSmokeGrenade = QuickSmokeGrenade or class()
QuickSmokeGrenade.init = function (self, unit)
	self._unit = unit
	self._state = 0

	return 
end
QuickSmokeGrenade.update = function (self, unit, t, dt)
	if self._remove_t and self._remove_t < t then
		self._unit:set_slot(0)
	end

	if self._state == 1 then
		self._timer = self._timer - dt

		if self._timer <= 0 then
			self._timer = self._timer + 0.5
			self._state = 2

			self._play_sound_and_effects(self)
		end
	elseif self._state == 2 then
		self._timer = self._timer - dt

		if self._timer <= 0 then
			self._state = 3

			self.detonate(self)
		end
	end

	return 
end
QuickSmokeGrenade.activate = function (self, position, duration)
	self._activate(self, 1, 0.5, position, duration)

	return 
end
QuickSmokeGrenade.activate_immediately = function (self, position, duration)
	self._activate(self, 3, 0, position, duration)

	self._remove_t = TimerManager:game():time() + self._duration

	return 
end
QuickSmokeGrenade._activate = function (self, state, timer, position, duration)
	self._state = state
	self._timer = timer
	self._shoot_position = position
	self._duration = duration

	self._play_sound_and_effects(self)

	return 
end
QuickSmokeGrenade.detonate = function (self)
	self._play_sound_and_effects(self)

	self._remove_t = TimerManager:game():time() + self._duration

	return 
end
QuickSmokeGrenade.preemptive_kill = function (self)
	self._unit:sound_source():post_event("grenade_gas_stop")
	self._unit:set_slot(0)

	return 
end
QuickSmokeGrenade._play_sound_and_effects = function (self)
	if self._state == 1 then
		local sound_source = SoundDevice:create_source("grenade_fire_source")

		sound_source.set_position(sound_source, self._shoot_position)
		sound_source.post_event(sound_source, "grenade_gas_npc_fire")
	elseif self._state == 2 then
		local bounce_point = Vector3()

		mvector3.lerp(bounce_point, self._shoot_position, self._unit:position(), 0.65)

		local sound_source = SoundDevice:create_source("grenade_bounce_source")

		sound_source.set_position(sound_source, bounce_point)
		sound_source.post_event(sound_source, "grenade_gas_bounce")
	elseif self._state == 3 then
		World:effect_manager():spawn({
			effect = Idstring("effects/particles/explosions/explosion_smoke_grenade"),
			position = self._unit:position(),
			normal = self._unit:rotation():y()
		})
		self._unit:sound_source():post_event("grenade_gas_explode")

		local parent = self._unit:orientation_object()
		self._smoke_effect = World:effect_manager():spawn({
			effect = Idstring("effects/particles/explosions/smoke_grenade_smoke"),
			parent = parent
		})
	end

	return 
end
QuickSmokeGrenade.destroy = function (self)
	if self._smoke_effect then
		World:effect_manager():fade_kill(self._smoke_effect)
	end

	return 
end

return 
