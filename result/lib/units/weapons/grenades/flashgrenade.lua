FlashGrenade = FlashGrenade or class(GrenadeBase)
FlashGrenade.init = function (self, unit)
	self._init_timer = 1

	FlashGrenade.super.init(self, unit)

	return 
end
FlashGrenade._detonate = function (self)
	local units = World:find_units("sphere", self._unit:position(), 400, self._slotmask)

	for _, unit in ipairs(units) do
		local col_ray = {
			ray = unit.position(unit) - self._unit:position():normalized(),
			position = self._unit:position()
		}

		self._give_flash_damage(self, col_ray, unit, 10)
	end

	self._unit:set_slot(0)

	return 
end
FlashGrenade._play_sound_and_effects = function (self)
	World:effect_manager():spawn({
		effect = Idstring("effects/particles/explosions/explosion_flash_grenade"),
		position = self._unit:position(),
		normal = self._unit:rotation():y()
	})
	self._unit:sound_source():post_event("trip_mine_explode")

	return 
end
FlashGrenade._give_flash_damage = function (self, col_ray, unit, damage)
	return 
end

return 
