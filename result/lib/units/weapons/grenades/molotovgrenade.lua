MolotovGrenade = MolotovGrenade or class(GrenadeBase)
MolotovGrenade.init = function (self, unit)
	MolotovGrenade.super.super.init(self, unit)

	self._detonated = false

	return 
end
MolotovGrenade.clbk_impact = function (self, tag, unit, body, other_unit, other_body, position, normal, collision_velocity, velocity, other_velocity)
	self._detonate(self, normal)

	return 
end
MolotovGrenade._detonate = function (self, normal)
	if self._detonated == false then
		self._detonated = true

		self._spawn_environment_fire(self, normal)
		managers.network:session():send_to_peers_synched("sync_detonate_molotov_grenade", self._unit, "base", GrenadeBase.EVENT_IDS.detonate, normal)
	end

	return 
end
MolotovGrenade.sync_detonate_molotov_grenade = function (self, event_id, normal)
	if event_id == GrenadeBase.EVENT_IDS.detonate then
		self._detonate_on_client(self, normal)
	end

	return 
end
MolotovGrenade._detonate_on_client = function (self, normal)
	if self._detonated == false then
		self._detonated = true

		self._spawn_environment_fire(self, normal)
	end

	return 
end
MolotovGrenade._spawn_environment_fire = function (self, normal)
	local position = self._unit:position()
	local rotation = self._unit:rotation()
	local data = tweak_data.env_effect:molotov_fire()

	EnvironmentFire.spawn(position, rotation, data, normal, self._thrower_unit, 0, 1)
	self._unit:set_slot(0)

	return 
end
MolotovGrenade.bullet_hit = function (self)
	if not Network:is_server() then
		return 
	end

	self._detonate(self)

	return 
end
MolotovGrenade.add_damage_result = function (self, unit, is_dead, damage_percent)
	if not alive(self._thrower_unit) or self._thrower_unit ~= managers.player:player_unit() then
		return 
	end

	local unit_type = unit.base(unit)._tweak_table
	local is_civlian = unit.character_damage(unit).is_civilian(unit_type)
	local is_gangster = unit.character_damage(unit).is_gangster(unit_type)
	local is_cop = unit.character_damage(unit).is_cop(unit_type)

	if is_civlian then
		return 
	end

	if is_dead then
		self._check_achievements(self, unit, is_dead, damage_percent, 1, 1)
	end

	return 
end

return 
