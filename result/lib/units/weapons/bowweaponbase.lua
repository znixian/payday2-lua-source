BowWeaponBase = BowWeaponBase or class(ProjectileWeaponBase)
BowWeaponBase.init = function (self, unit)
	BowWeaponBase.super.init(self, unit)

	self._client_authoritative = true
	self._no_reload = false
	self._steelsight_speed = 0.5

	return 
end
BowWeaponBase.trigger_pressed = function (self, ...)
	self._start_charging(self)

	return 
end
BowWeaponBase.trigger_held = function (self, ...)
	if not self._charging and not self._cancelled then
		self._start_charging(self)
	end

	return 
end
BowWeaponBase._start_charging = function (self)
	self._cancelled = nil
	self._charging = true
	self._charge_start_t = managers.player:player_timer():time()

	self.play_tweak_data_sound(self, "charge")

	return 
end
BowWeaponBase.set_tased_shot = function (self, bool)
	self._is_tased_shot = bool

	return 
end
BowWeaponBase.trigger_released = function (self, ...)
	local fired = nil

	if self._charging and not self._cancelled and self.start_shooting_allowed(self) then
		fired = self.fire(self, ...)

		if fired then
			self.play_tweak_data_sound(self, (self.charge_fail(self) and "charge_release_fail") or "charge_release")

			local next_fire = ((tweak_data.weapon[self._name_id].fire_mode_data and tweak_data.weapon[self._name_id].fire_mode_data.fire_rate) or 0)/self.fire_rate_multiplier(self)
			self._next_fire_allowed = self._next_fire_allowed + next_fire
		end
	end

	self._charging = nil
	self._cancelled = nil

	return fired
end
BowWeaponBase.add_damage_result = function (self, unit, is_dead, attacker, damage_percent)
	if not alive(attacker) or attacker ~= managers.player:player_unit() then
		return 
	end

	managers.statistics:shot_fired({
		skip_bullet_count = true,
		hit = true,
		weapon_unit = self._unit
	})

	return 
end
BowWeaponBase._spawn_muzzle_effect = function (self)
	return 
end
BowWeaponBase.charge_fail = function (self)
	return self.charge_multiplier(self) < 0.2
end
BowWeaponBase.charge_multiplier = function (self)
	if self._is_tased_shot then
		return 1
	end

	local charge_multiplier = 1

	if self._charge_start_t then
		local delta_t = managers.player:player_timer():time() - self._charge_start_t
		charge_multiplier = math.min(delta_t/self.charge_max_t(self), 1)
	end

	return charge_multiplier
end
BowWeaponBase.projectile_speed_multiplier = function (self)
	return math.lerp(0.05, 1, self.charge_multiplier(self))
end
BowWeaponBase.projectile_damage_multiplier = function (self)
	return math.lerp(0.1, 1, self.charge_multiplier(self))
end
BowWeaponBase.projectile_charge_value = function (self)
	return self.charge_multiplier(self)
end
BowWeaponBase._adjust_throw_z = function (self, m_vec)
	local adjust_z = math.lerp(0, 0.05, math.abs(mvector3.z(m_vec)) - 1)

	mvector3.set_z(m_vec, mvector3.z(m_vec) + adjust_z)

	return 
end
BowWeaponBase.fire_on_release = function (self)
	return true
end
BowWeaponBase.can_refire_while_tased = function (self)
	return false
end
BowWeaponBase.charging = function (self)
	return self._charging and not self._cancelled
end
BowWeaponBase.interupt_charging = function (self)
	self._charging = nil
	self._cancelled = nil

	self.play_tweak_data_sound(self, "charge_cancel")

	return 
end
BowWeaponBase.manages_steelsight = function (self)
	return true
end
BowWeaponBase.steelsight_pressed = function (self)
	if self._cancelled then
		return 
	end

	if self._charging then
		self._cancelled = true

		self.play_tweak_data_sound(self, "charge_cancel")
	end

	return {
		exit_steelsight = true
	}
end
BowWeaponBase.wants_steelsight = function (self)
	return self._charging and not self._cancelled
end
BowWeaponBase.enter_steelsight_speed_multiplier = function (self)
	return self._steelsight_speed*BowWeaponBase.super.enter_steelsight_speed_multiplier(self)
end
BowWeaponBase.reload_speed_multiplier = function (self)
	local code_miss_multiplier = self.weapon_tweak_data(self).bow_reload_speed_multiplier or 1

	return code_miss_multiplier*BowWeaponBase.super.reload_speed_multiplier(self)
end
BowWeaponBase.set_ammo_max = function (self, ammo_max)
	BowWeaponBase.super.set_ammo_max(self, ammo_max)

	if self._no_reload then
		self.set_ammo_max_per_clip(self, ammo_max)
	end

	return 
end
BowWeaponBase.set_ammo_total = function (self, ammo_total)
	BowWeaponBase.super.set_ammo_total(self, ammo_total)

	if self._no_reload then
		self.set_ammo_remaining_in_clip(self, ammo_total)
	end

	return 
end
BowWeaponBase.replenish = function (self)
	BowWeaponBase.super.replenish(self)

	if self._no_reload then
		self.set_ammo_remaining_in_clip(self, self.get_ammo_total(self))
	end

	return 
end
BowWeaponBase.charge_max_t = function (self)
	return self.weapon_tweak_data(self).charge_data.max_t
end
CrossbowWeaponBase = CrossbowWeaponBase or class(ProjectileWeaponBase)
CrossbowWeaponBase.init = function (self, unit)
	CrossbowWeaponBase.super.init(self, unit)

	self._client_authoritative = true

	return 
end
CrossbowWeaponBase.should_reload_immediately = function (self)
	return true
end
CrossbowWeaponBase.charge_fail = function (self)
	return false
end
CrossbowWeaponBase.add_damage_result = function (self, unit, is_dead, attacker, damage_percent)
	if not alive(attacker) or attacker ~= managers.player:player_unit() then
		return 
	end

	managers.statistics:shot_fired({
		skip_bullet_count = true,
		hit = true,
		weapon_unit = self._unit
	})

	return 
end

return 
