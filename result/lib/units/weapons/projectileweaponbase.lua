ProjectileWeaponBase = ProjectileWeaponBase or class(NewRaycastWeaponBase)
ProjectileWeaponBase.init = function (self, ...)
	ProjectileWeaponBase.super.init(self, ...)

	self._projectile_type_index = self.weapon_tweak_data(self).projectile_type_index

	return 
end
local mvec_spread_direction = Vector3()
ProjectileWeaponBase._fire_raycast = function (self, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
	local unit = nil
	local spread_x, spread_y = self._get_spread(self, user_unit)
	local right = direction.cross(direction, Vector3(0, 0, 1)):normalized()
	local up = direction.cross(direction, right):normalized()
	local theta = math.random()*360
	local ax = math.sin(theta)*math.random()*spread_x*(spread_mul or 1)
	local ay = math.cos(theta)*math.random()*spread_y*(spread_mul or 1)

	mvector3.set(mvec_spread_direction, direction)
	mvector3.add(mvec_spread_direction, right*math.rad(ax))
	mvector3.add(mvec_spread_direction, up*math.rad(ay))

	local projectile_type_index = self._projectile_type_index or 2

	if self._ammo_data and self._ammo_data.launcher_grenade then
		if self.weapon_tweak_data(self).projectile_type_indices and self.weapon_tweak_data(self).projectile_type_indices[self._ammo_data.launcher_grenade] then
			projectile_type_index = self.weapon_tweak_data(self).projectile_type_indices[self._ammo_data.launcher_grenade]
		else
			projectile_type_index = tweak_data.blackmarket:get_index_from_projectile_id(self._ammo_data.launcher_grenade)
		end
	end

	self._adjust_throw_z(self, mvec_spread_direction)

	mvec_spread_direction = mvec_spread_direction*self.projectile_speed_multiplier(self)
	local spawn_offset = self._get_spawn_offset(self)
	self._dmg_mul = dmg_mul or 1

	if not self._client_authoritative then
		if Network:is_client() then
			managers.network:session():send_to_host("request_throw_projectile", projectile_type_index, from_pos, mvec_spread_direction)
		else
			unit = ProjectileBase.throw_projectile(projectile_type_index, from_pos, mvec_spread_direction, managers.network:session():local_peer():id())
		end
	else
		unit = ProjectileBase.throw_projectile(projectile_type_index, from_pos, mvec_spread_direction, managers.network:session():local_peer():id())
	end

	managers.statistics:shot_fired({
		hit = false,
		weapon_unit = self._unit
	})

	return {}
end
ProjectileWeaponBase._update_stats_values = function (self)
	ProjectileWeaponBase.super._update_stats_values(self)

	if self._ammo_data and self._ammo_data.projectile_type_index ~= nil then
		self._projectile_type_index = self._ammo_data.projectile_type_index
	end

	return 
end
ProjectileWeaponBase._adjust_throw_z = function (self, m_vec)
	return 
end
ProjectileWeaponBase.projectile_damage_multiplier = function (self)
	return self._dmg_mul
end
ProjectileWeaponBase.projectile_speed_multiplier = function (self)
	return 1
end
ProjectileWeaponBase._get_spawn_offset = function (self)
	return 0
end

return 
