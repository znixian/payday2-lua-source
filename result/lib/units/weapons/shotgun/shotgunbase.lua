ShotgunBase = ShotgunBase or class(NewRaycastWeaponBase)
ShotgunBase.init = function (self, ...)
	ShotgunBase.super.init(self, ...)
	self.setup_default(self)

	self._hip_fire_rate_inc = 0

	return 
end
ShotgunBase.setup_default = function (self)
	self._damage_near = tweak_data.weapon[self._name_id].damage_near
	self._damage_far = tweak_data.weapon[self._name_id].damage_far
	self._rays = tweak_data.weapon[self._name_id].rays or self._ammo_data.rays or 6
	self._range = self._damage_far

	if tweak_data.weapon[self._name_id].use_shotgun_reload == nil then
		self._use_shotgun_reload = self._use_shotgun_reload or self._use_shotgun_reload == nil
	else
		self._use_shotgun_reload = tweak_data.weapon[self._name_id].use_shotgun_reload
	end

	if not self.weapon_tweak_data(self).has_magazine then
		self._hip_fire_rate_inc = managers.player:upgrade_value("shotgun", "hip_rate_of_fire", 0)
	end

	return 
end
ShotgunBase._create_use_setups = function (self)
	local use_data = {}
	local player_setup = {
		selection_index = tweak_data.weapon[self._name_id].use_data.selection_index,
		equip = {
			align_place = tweak_data.weapon[self._name_id].use_data.align_place or "left_hand"
		},
		unequip = {
			align_place = "back"
		}
	}
	use_data.player = player_setup
	self._use_data = use_data

	return 
end
ShotgunBase.fire_rate_multiplier = function (self)
	local fire_rate_mul = self._fire_rate_multiplier

	if self._hip_fire_rate_inc ~= 0 then
		local user_unit = self._setup and self._setup.user_unit
		local current_state = alive(user_unit) and user_unit.movement(user_unit) and user_unit.movement(user_unit)._current_state

		if current_state and not current_state.in_steelsight(current_state) then
			fire_rate_mul = fire_rate_mul + self._hip_fire_rate_inc - 1
			fire_rate_mul = self._convert_add_to_mul(self, fire_rate_mul)
		end
	end

	return fire_rate_mul
end
ShotgunBase.run_and_shoot_allowed = function (self)
	local allowed = ShotgunBase.super.run_and_shoot_allowed(self)

	return allowed or managers.player:has_category_upgrade("shotgun", "hip_run_and_shoot")
end
ShotgunBase._update_stats_values = function (self)
	ShotgunBase.super._update_stats_values(self)
	self.setup_default(self)

	if self._ammo_data then
		if self._ammo_data.rays ~= nil then
			self._rays = self._ammo_data.rays
		end

		if self._ammo_data.damage_near ~= nil then
			self._damage_near = self._ammo_data.damage_near
		end

		if self._ammo_data.damage_near_mul ~= nil then
			self._damage_near = self._damage_near*self._ammo_data.damage_near_mul
		end

		if self._ammo_data.damage_far ~= nil then
			self._damage_far = self._ammo_data.damage_far
		end

		if self._ammo_data.damage_far_mul ~= nil then
			self._damage_far = self._damage_far*self._ammo_data.damage_far_mul
		end

		self._range = self._damage_far
	end

	return 
end
ShotgunBase.get_damage_falloff = function (self, damage, col_ray, user_unit)
	local distance = col_ray.distance or mvector3.distance(col_ray.unit:position(), user_unit.position(user_unit))
	local inc_range_mul = 1
	local current_state = user_unit.movement(user_unit)._current_state

	if current_state and current_state.in_steelsight(current_state) then
		inc_range_mul = managers.player:upgrade_value("shotgun", "steelsight_range_inc", 1)
	end

	return (math.min(1, math.max(0, distance - self._damage_near*inc_range_mul)/(self._damage_far*inc_range_mul)) - 1)*damage
end
local mvec_temp = Vector3()
local mvec_to = Vector3()
local mvec_direction = Vector3()
local mvec_spread_direction = Vector3()
ShotgunBase._fire_raycast = function (self, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
	local result = nil
	local hit_enemies = {}
	local hit_objects = {}
	local hit_something, col_rays = nil

	if self._alert_events then
		col_rays = {}
	end

	local damage = self._get_current_damage(self, dmg_mul)
	local autoaim, dodge_enemies = self.check_autoaim(self, from_pos, direction, self._range)
	local weight = 0.1
	local enemy_died = false

	local function hit_enemy(col_ray)
		if col_ray.unit:character_damage() then
			local enemy_key = col_ray.unit:key()

			if not hit_enemies[enemy_key] or (col_ray.unit:character_damage().is_head and col_ray.unit:character_damage():is_head(col_ray.body)) then
				hit_enemies[enemy_key] = col_ray
			end

			if not col_ray.unit:character_damage().is_head then
				self._bullet_class:on_collision_effects(col_ray, self._unit, user_unit, damage)
			end
		else
			add_shoot_through_bullet = self._can_shoot_through_shield or self._can_shoot_through_wall

			if add_shoot_through_bullet then
				hit_objects[col_ray.unit:key()] = hit_objects[col_ray.unit:key()] or {}

				table.insert(hit_objects[col_ray.unit:key()], col_ray)
			else
				self._bullet_class:on_collision(col_ray, self._unit, user_unit, damage)
			end
		end

		return 
	end

	local spread_x, spread_y = self._get_spread(self, user_unit)
	local right = direction.cross(direction, Vector3(0, 0, 1)):normalized()
	local up = direction.cross(direction, right):normalized()

	mvector3.set(mvec_direction, direction)

	slot25 = 1
	slot26 = (shoot_through_data and 1) or self._rays

	for i = slot25, slot26, 1 do
		local theta = math.random()*360
		local ax = math.sin(theta)*math.random()*spread_x*(spread_mul or 1)
		local ay = math.cos(theta)*math.random()*spread_y*(spread_mul or 1)

		mvector3.set(mvec_spread_direction, mvec_direction)
		mvector3.add(mvec_spread_direction, right*math.rad(ax))
		mvector3.add(mvec_spread_direction, up*math.rad(ay))
		mvector3.set(mvec_to, mvec_spread_direction)
		mvector3.multiply(mvec_to, 20000)
		mvector3.add(mvec_to, from_pos)

		local ray_from_unit = (shoot_through_data and alive(shoot_through_data.ray_from_unit) and shoot_through_data.ray_from_unit) or nil
		local col_ray = ray_from_unit or World:raycast("ray", from_pos, mvec_to, "slot_mask", self._bullet_slotmask, "ignore_unit", self._setup.ignore_units)

		if col_rays then
			if col_ray then
				table.insert(col_rays, col_ray)
			else
				local ray_to = mvector3.copy(mvec_to)
				local spread_direction = mvector3.copy(mvec_spread_direction)

				table.insert(col_rays, {
					position = ray_to,
					ray = spread_direction
				})
			end
		end

		if self._autoaim and autoaim then
			if col_ray and col_ray.unit:in_slot(managers.slot:get_mask("enemies")) then
				self._autohit_current = (self._autohit_current + weight)/(weight + 1)

				hit_enemy(col_ray)

				autoaim = false
			else
				autoaim = false
				local autohit = self.check_autoaim(self, from_pos, direction, self._range)

				if autohit then
					local autohit_chance = math.clamp((self._autohit_current - self._autohit_data.MIN_RATIO)/(self._autohit_data.MAX_RATIO - self._autohit_data.MIN_RATIO), 0, 1) - 1

					if math.random() < autohit_chance then
						self._autohit_current = (self._autohit_current + weight)/(weight + 1)
						hit_something = true

						hit_enemy(autohit)
					else
						self._autohit_current = self._autohit_current/(weight + 1)
					end
				elseif col_ray then
					hit_something = true

					hit_enemy(col_ray)
				end
			end
		elseif col_ray then
			hit_something = true

			hit_enemy(col_ray)
		end
	end

	for _, col_rays in pairs(hit_objects) do
		local center_ray = col_rays[1]

		if 1 < #col_rays then
			mvector3.set_static(mvec_temp, center_ray)

			for _, col_ray in ipairs(col_rays) do
				mvector3.add(mvec_temp, col_ray.position)
			end

			mvector3.divide(mvec_temp, #col_rays)

			local closest_dist_sq = mvector3.distance_sq(mvec_temp, center_ray.position)
			local dist_sq = nil

			for _, col_ray in ipairs(col_rays) do
				dist_sq = mvector3.distance_sq(mvec_temp, col_ray.position)

				if dist_sq < closest_dist_sq then
					closest_dist_sq = dist_sq
					center_ray = col_ray
				end
			end
		end

		ShotgunBase.super._fire_raycast(self, user_unit, from_pos, center_ray.ray, dmg_mul, shoot_player, 0, autohit_mul, suppr_mul, shoot_through_data)
	end

	local kill_data = {
		kills = 0,
		headshots = 0,
		civilian_kills = 0
	}

	for _, col_ray in pairs(hit_enemies) do
		local damage = self.get_damage_falloff(self, damage, col_ray, user_unit)

		if 0 < damage then
			local my_result = nil
			local add_shoot_through_bullet = self._can_shoot_through_shield or self._can_shoot_through_enemy or self._can_shoot_through_wall

			if add_shoot_through_bullet then
				my_result = ShotgunBase.super._fire_raycast(self, user_unit, from_pos, col_ray.ray, dmg_mul, shoot_player, 0, autohit_mul, suppr_mul, shoot_through_data)
			else
				my_result = self._bullet_class:on_collision(col_ray, self._unit, user_unit, damage)
			end

			my_result = managers.mutators:modify_value("ShotgunBase:_fire_raycast", my_result)

			if my_result and my_result.type == "death" then
				managers.game_play_central:do_shotgun_push(col_ray.unit, col_ray.position, col_ray.ray, col_ray.distance, user_unit)

				kill_data.kills = kill_data.kills + 1

				if col_ray.body and col_ray.body:name() == Idstring("head") then
					kill_data.headshots = kill_data.headshots + 1
				end

				if col_ray.unit and col_ray.unit:base() and (col_ray.unit:base()._tweak_table == "civilian" or col_ray.unit:base()._tweak_table == "civilian_female") then
					kill_data.civilian_kills = kill_data.civilian_kills + 1
				end
			end
		end
	end

	if dodge_enemies and self._suppression then
		for enemy_data, dis_error in pairs(dodge_enemies) do
			enemy_data.unit:character_damage():build_suppression(suppr_mul*dis_error*self._suppression, self._panic_suppression_chance)
		end
	end

	if not result then
		result = {
			hit_enemy = (next(hit_enemies) and true) or false
		}

		if self._alert_events then
			result.rays = 0 < #col_rays and col_rays
		end
	end

	if not shoot_through_data then
		managers.statistics:shot_fired({
			hit = false,
			weapon_unit = self._unit
		})
	end

	for _, d in pairs(hit_enemies) do
		managers.statistics:shot_fired({
			skip_bullet_count = true,
			hit = true,
			weapon_unit = self._unit
		})
	end

	for key, data in pairs(tweak_data.achievement.shotgun_single_shot_kills) do
		if (data.headshot and data.count <= kill_data.headshots - kill_data.civilian_kills) or data.count <= kill_data.kills - kill_data.civilian_kills then
			local should_award = true

			if data.blueprint then
				local missing_parts = false

				for _, part_or_parts in ipairs(data.blueprint) do
					if type(part_or_parts) == "string" then
						if not table.contains(self._blueprint or {}, part_or_parts) then
							missing_parts = true

							break
						end
					else
						local found_part = false

						for _, part in ipairs(part_or_parts) do
							if table.contains(self._blueprint or {}, part) then
								found_part = true

								break
							end
						end

						if not found_part then
							missing_parts = true

							break
						end
					end
				end

				if missing_parts then
					should_award = false
				end
			end

			if should_award then
				managers.achievment:_award_achievement(data, key)
			end
		end
	end

	return result
end
SaigaShotgun = SaigaShotgun or class(ShotgunBase)
SaigaShotgun.init = function (self, ...)
	SaigaShotgun.super.init(self, ...)

	self._use_shotgun_reload = false

	return 
end
InstantElectricBulletBase = InstantElectricBulletBase or class(InstantBulletBase)
InstantElectricBulletBase.give_impact_damage = function (self, col_ray, weapon_unit, user_unit, damage, armor_piercing)
	local hit_unit = col_ray.unit
	local action_data = {
		damage = 0,
		weapon_unit = weapon_unit,
		attacker_unit = user_unit,
		col_ray = col_ray,
		armor_piercing = armor_piercing,
		attacker_unit = user_unit,
		attack_dir = col_ray.ray,
		variant = (weapon_unit.base(weapon_unit) and weapon_unit.base(weapon_unit).get_tase_strength and weapon_unit.base(weapon_unit):get_tase_strength()) or "light"
	}
	local defense_data = hit_unit and hit_unit.character_damage(hit_unit).damage_tase and hit_unit.character_damage(hit_unit):damage_tase(action_data)

	return defense_data
end

return 
