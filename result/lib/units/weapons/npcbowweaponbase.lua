NPCBowWeaponBase = NPCBowWeaponBase or class(NewNPCRaycastWeaponBase)
NPCBowWeaponBase.init = function (self, ...)
	NPCBowWeaponBase.super.init(self, ...)

	return 
end
NPCBowWeaponBase.fire_blank = function (self, direction, impact)
	self._sound_singleshot(self)

	if self.weapon_tweak_data(self).has_fire_animation then
		self.tweak_data_anim_play(self, "fire")
	end

	return 
end
NPCCrossBowWeaponBase = NPCCrossBowWeaponBase or class(NPCBowWeaponBase)

return 
