AkimboWeaponBaseVR = AkimboWeaponBase or Application:error("AkimboWeaponBaseVR requires AkimboWeaponBase!")
AkimboWeaponBaseVR.fire = function (self, from_pos, direction, ...)
	from_pos = self.fire_object(self):position()
	direction = self.fire_object(self):rotation():y()

	return AkimboWeaponBaseVR.super.fire(self, from_pos, direction, ...)
end
local __check_auto_aim = AkimboWeaponBase.check_auto_aim
AkimboWeaponBaseVR.check_auto_aim = function (self, from_pos, direction, ...)
	from_pos = self.fire_object(self):position()
	direction = self.fire_object(self):rotation():y()

	return __check_auto_aim(self, from_pos, direction, ...)
end
local __start_reload = AkimboWeaponBase.start_reload
AkimboWeaponBaseVR.start_reload = function (self, ...)
	if alive(self._second_gun) then
		self._second_gun:base():start_reload(...)
	end

	__start_reload(self, ...)

	return 
end
local __on_reload = AkimboWeaponBase.on_reload
AkimboWeaponBaseVR.on_reload = function (self, ...)
	if alive(self._second_gun) then
		self._second_gun:base():on_reload(...)
	end

	__on_reload(self, ...)

	return 
end
local __update_reloading = AkimboWeaponBase.update_reloading
AkimboWeaponBaseVR.update_reloading = function (self, ...)
	if alive(self._second_gun) then
		self._second_gun:base():update_reloading(...)
	end

	__update_reloading(self, ...)

	return 
end
local __update_reload_finish = AkimboWeaponBase.update_reload_finish
AkimboWeaponBaseVR.update_reload_finish = function (self, ...)
	if alive(self._second_gun) and self._second_gun:base():is_finishing_reload() then
		self._second_gun:base():update_reload_finish(...)
	end

	__update_reload_finish(self, ...)

	return 
end
AkimboWeaponBaseVR.on_enabled = function (self, ...)
	AkimboWeaponBaseVR.super.on_enabled(self, ...)

	return 
end
AkimboWeaponBaseVR.on_disabled = function (self, ...)
	AkimboWeaponBaseVR.super.on_disabled(self, ...)

	return 
end
AkimboWeaponBaseVR.set_gadget_on = function (self, ...)
	AkimboWeaponBaseVR.super.set_gadget_on(self, ...)

	return 
end

return 
