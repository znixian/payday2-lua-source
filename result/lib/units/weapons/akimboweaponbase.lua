AkimboWeaponBase = AkimboWeaponBase or class(NewRaycastWeaponBase)
AkimboWeaponBase.AKIMBO = true
AkimboWeaponBase.init = function (self, ...)
	AkimboWeaponBase.super.init(self, ...)

	self._manual_fire_second_gun = self.weapon_tweak_data(self).manual_fire_second_gun

	self._unit:set_extension_update_enabled(Idstring("base"), true)

	self._fire_callbacks = {}

	return 
end
AkimboWeaponBase.update = function (self, unit, t, dt)
	for i = #self._fire_callbacks, 1, -1 do
		local data = self._fire_callbacks[i]
		data.t = data.t - dt

		if data.t <= 0 then
			data.callback(self)
			table.remove(self._fire_callbacks, i)
		end
	end

	return 
end
AkimboWeaponBase._create_second_gun = function (self, unit_name)
	local factory_weapon = tweak_data.weapon.factory[self._factory_id]
	local ids_unit_name = Idstring(factory_weapon.unit)
	local new_unit = World:spawn_unit(ids_unit_name, Vector3(), Rotation())

	new_unit.base(new_unit):set_factory_data(self._factory_id)

	if self._cosmetics_id then
		new_unit.base(new_unit):set_cosmetics_data({
			id = self._cosmetics_id,
			quality = self._cosmetics_quality,
			bonus = self._cosmetics_bonus
		})
	end

	if self._blueprint then
		new_unit.base(new_unit):assemble_from_blueprint(self._factory_id, self._blueprint)
	elseif not unit_name then
		new_unit.base(new_unit):assemble(self._factory_id)
	end

	self._second_gun = new_unit
	self._second_gun:base().SKIP_AMMO = true
	self._second_gun:base().parent_weapon = self._unit

	new_unit.base(new_unit):setup(self._setup)

	self.akimbo = true

	if self._enabled then
		self._second_gun:base():on_enabled()
	else
		self._second_gun:base():on_disabled()
	end

	return 
end
AkimboWeaponBase.toggle_firemode = function (self, skip_post_event)
	if AkimboWeaponBase.super.toggle_firemode(self, skip_post_event) then
		if alive(self._second_gun) then
			return self._second_gun:base():toggle_firemode(true)
		else
			return true
		end
	end

	return false
end
AkimboWeaponBase.create_second_gun = function (self, create_second_gun)
	self._create_second_gun(self, create_second_gun)
	self._setup.user_unit:camera()._camera_unit:link(Idstring("a_weapon_left"), self._second_gun, self._second_gun:orientation_object():name())

	return 
end
AkimboWeaponBase.start_shooting = function (self)
	AkimboWeaponBase.super.start_shooting(self)

	self._fire_second_sound = not self._fire_second_gun_next and self.fire_mode(self) ~= "auto"

	return 
end
AkimboWeaponBase.get_fire_time = function (self)
	return math.rand(0.075) + 0.025
end
AkimboWeaponBase.fire = function (self, ...)
	if not self._manual_fire_second_gun then
		local result = AkimboWeaponBase.super.fire(self, ...)

		if alive(self._second_gun) then
			table.insert(self._fire_callbacks, {
				t = self.get_fire_time(self),
				callback = callback(self, self, "_fire_second", {
					...
				})
			})
		end

		return result
	else
		local result = nil

		if self._fire_second_gun_next then
			if alive(self._second_gun) then
				result = self._second_gun:base().super.fire(self._second_gun:base(), ...)

				if result then
					if self._fire_second_sound then
						self._fire_second_sound = false

						self._second_gun:base():_fire_sound()
					end

					managers.hud:set_ammo_amount(self.selection_index(self), self.ammo_info(self))
					self._second_gun:base():tweak_data_anim_play("fire")
				end
			end

			self._fire_second_gun_next = false
		else
			result = AkimboWeaponBase.super.fire(self, ...)
			self._fire_second_gun_next = true
		end

		return result
	end

	return 
end
AkimboWeaponBase._fire_second = function (self, params)
	if alive(self._second_gun) and self._setup and alive(self._setup.user_unit) then
		local fired = self._second_gun:base().super.fire(self._second_gun:base(), unpack(params))

		if fired then
			if self._fire_second_sound then
				self._fire_second_sound = false

				self._second_gun:base():_fire_sound()
			end

			managers.hud:set_ammo_amount(self.selection_index(self), self.ammo_info(self))
			self._second_gun:base():tweak_data_anim_play("fire")
		end

		return fired
	end

	return 
end
AkimboWeaponBase._do_update_bullet_objects = function (self, weapon_base, ammo_func, is_second_gun)
	if weapon_base._bullet_objects then
		for i, objects in pairs(weapon_base._bullet_objects) do
			for _, object in ipairs(objects) do
				local ammo_base = weapon_base.ammo_base(weapon_base)
				local ammo = ammo_base[ammo_func](ammo_base)/2

				if is_second_gun then
					ammo = math.ceil(ammo)
				else
					ammo = math.floor(ammo)
				end

				object[1]:set_visibility(i <= ammo)
			end
		end
	end

	return 
end
AkimboWeaponBase._update_bullet_objects = function (self, ammo_func)
	self._do_update_bullet_objects(self, self, ammo_func, false)

	if alive(self._second_gun) then
		self._do_update_bullet_objects(self, self._second_gun:base(), ammo_func, true)
	end

	return 
end
AkimboWeaponBase._check_magazine_empty = function (self)
	AkimboWeaponBase.super._check_magazine_empty(self)

	if alive(self._second_gun) then
		self._second_gun:base():_check_magazine_empty()
	end

	return 
end
AkimboWeaponBase.on_enabled = function (self, ...)
	AkimboWeaponBase.super.on_enabled(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():on_enabled(...)
	end

	return 
end
AkimboWeaponBase.on_disabled = function (self, ...)
	AkimboWeaponBase.super.on_disabled(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():on_disabled(...)
	end

	return 
end
AkimboWeaponBase.on_equip = function (self, ...)
	AkimboWeaponBase.super.on_equip(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():on_equip(...)
	end

	return 
end
AkimboWeaponBase.set_magazine_empty = function (self, ...)
	AkimboWeaponBase.super.set_magazine_empty(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():set_magazine_empty(...)
	end

	return 
end
AkimboWeaponBase.set_gadget_on = function (self, ...)
	AkimboWeaponBase.super.set_gadget_on(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():set_gadget_on(...)
	end

	return 
end
AkimboWeaponBase.set_gadget_color = function (self, ...)
	if not self._enabled then
		return 
	end

	if not self._assembly_complete then
		return 
	end

	AkimboWeaponBase.super.set_gadget_color(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():set_gadget_color(...)
	end

	return 
end
AkimboWeaponBase._second_gun_tweak_data_anim_version = function (self, anim)
	if not self.weapon_tweak_data(self).animations.second_gun_versions then
		return anim
	end

	return self.weapon_tweak_data(self).animations.second_gun_versions[anim] or anim
end
AkimboWeaponBase.tweak_data_anim_play = function (self, anim, ...)
	if alive(self._second_gun) and anim ~= "fire" then
		local second_gun_anim = self._second_gun_tweak_data_anim_version(self, anim)

		self._second_gun:base():tweak_data_anim_play(second_gun_anim, ...)
	end

	return AkimboWeaponBase.super.tweak_data_anim_play(self, anim, ...)
end
AkimboWeaponBase.tweak_data_anim_stop = function (self, anim, ...)
	AkimboWeaponBase.super.tweak_data_anim_stop(self, anim, ...)

	if alive(self._second_gun) then
		local second_gun_anim = self._second_gun_tweak_data_anim_version(self, anim)

		self._second_gun:base():tweak_data_anim_stop(second_gun_anim, ...)
	end

	return 
end
AkimboWeaponBase.destroy = function (self, ...)
	AkimboWeaponBase.super.destroy(self, ...)

	if alive(self._second_gun) then
		self._second_gun:set_slot(0)
	end

	return 
end
NPCAkimboWeaponBase = NPCAkimboWeaponBase or class(NewNPCRaycastWeaponBase)
NPCAkimboWeaponBase.AKIMBO = true
NPCAkimboWeaponBase.init = function (self, ...)
	NPCAkimboWeaponBase.super.init(self, ...)

	self._manual_fire_second_gun = self.weapon_tweak_data(self).manual_fire_second_gun

	self._unit:set_extension_update_enabled(Idstring("base"), true)

	self._fire_callbacks = {}

	return 
end
NPCAkimboWeaponBase.update = function (self, unit, t, dt)
	for i = #self._fire_callbacks, 1, -1 do
		local data = self._fire_callbacks[i]
		data.t = data.t - dt

		if data.t <= 0 then
			data.callback(self)
			table.remove(self._fire_callbacks, i)
		end
	end

	return 
end
NPCAkimboWeaponBase.create_second_gun = function (self, create_second_gun)
	AkimboWeaponBase._create_second_gun(self, create_second_gun)
	self._setup.user_unit:link(Idstring("a_weapon_left_front"), self._second_gun, self._second_gun:orientation_object():name())

	return 
end
NPCAkimboWeaponBase.get_fire_time = function (self)
	return AkimboWeaponBase.get_fire_time(self)
end
NPCAkimboWeaponBase.fire_blank = function (self, ...)
	if not self._manual_fire_second_gun then
		NPCAkimboWeaponBase.super.fire_blank(self, ...)

		if alive(self._second_gun) then
			if self._setup.user_unit:movement():current_state_name() == "bleed_out" or self._setup.user_unit:movement():zipline_unit() then
				return 
			end

			managers.enemy:add_delayed_clbk("NPCAkimboWeaponBase", callback(self, self, "_fire_blank_second", {
				...
			}), TimerManager:game():time() + self.get_fire_time(self))
		end
	elseif self._fire_second_gun_next then
		if alive(self._second_gun) and alive(self._setup.user_unit) then
			self._second_gun:base():fire_blank(...)
		end

		self._fire_second_gun_next = false
	else
		NPCAkimboWeaponBase.super.fire_blank(self, ...)

		self._fire_second_gun_next = true
	end

	return 
end
NPCAkimboWeaponBase._fire_blank_second = function (self, params)
	if alive(self._second_gun) and alive(self._setup.user_unit) then
		self._second_gun:base():fire_blank(unpack(params))
	end

	return 
end
NPCAkimboWeaponBase.auto_fire_blank = function (self, direction, impact)
	NPCAkimboWeaponBase.super.auto_fire_blank(self, direction, impact)

	if alive(self._second_gun) and impact then
		table.insert(self._fire_callbacks, {
			t = self.get_fire_time(self),
			callback = callback(self, self, "_auto_fire_blank_second", {
				direction,
				impact
			})
		})
	end

	return true
end
NPCAkimboWeaponBase._auto_fire_blank_second = function (self, params)
	if alive(self._second_gun) and alive(self._setup.user_unit) then
		self._second_gun:base():auto_fire_blank(unpack(params))
	end

	return 
end
NPCAkimboWeaponBase.start_autofire = function (self, nr_shots)
	NPCAkimboWeaponBase.super.start_autofire(self, nr_shots)

	if alive(self._second_gun) then
		table.insert(self._fire_callbacks, {
			t = self.get_fire_time(self),
			callback = callback(self, self, "_start_autofire_second", nr_shots or false)
		})
	end

	return 
end
NPCAkimboWeaponBase._start_autofire_second = function (self, nr_shots)
	if alive(self._second_gun) and alive(self._setup.user_unit) then
		self._second_gun:base():start_autofire(nr_shots or nil)
	end

	return 
end
NPCAkimboWeaponBase.stop_autofire = function (self)
	NPCAkimboWeaponBase.super.stop_autofire(self)

	if alive(self._second_gun) then
		table.insert(self._fire_callbacks, {
			t = self.get_fire_time(self),
			callback = callback(self, self, "_stop_autofire_second")
		})
	end

	return 
end
NPCAkimboWeaponBase._stop_autofire_second = function (self)
	if alive(self._second_gun) and alive(self._setup.user_unit) then
		self._second_gun:base():stop_autofire()
	end

	return 
end
NPCAkimboWeaponBase.on_enabled = function (self, ...)
	NPCAkimboWeaponBase.super.on_enabled(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():on_enabled(...)
	end

	return 
end
NPCAkimboWeaponBase.on_disabled = function (self, ...)
	NPCAkimboWeaponBase.super.on_disabled(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():on_disabled(...)
	end

	return 
end
NPCAkimboWeaponBase.on_melee_item_shown = function (self)
	if alive(self._second_gun) then
		self._second_gun:base():on_disabled()
	end

	return 
end
NPCAkimboWeaponBase.on_melee_item_hidden = function (self)
	if alive(self._second_gun) then
		self._second_gun:base():on_enabled()

		local active, part_id = self.get_active_gadget(self)

		if part_id then
			self._second_gun:base():set_gadget_on_by_part_id(part_id)

			if self._second_gun:base().gadget_color and self._second_gun:base():gadget_color() then
				self._second_gun:base():set_gadget_color(self._second_gun:base():gadget_color())
			end
		end
	end

	return 
end
NPCAkimboWeaponBase.set_gadget_on = function (self, ...)
	NPCAkimboWeaponBase.super.set_gadget_on(self, ...)

	if alive(self._second_gun) then
		self._second_gun:base():set_gadget_on(...)
	end

	return 
end
NPCAkimboWeaponBase.gadget_color = function (self)
	return self._gadget_color
end
NPCAkimboWeaponBase.set_gadget_color = function (self, color)
	if not self._enabled then
		return 
	end

	if not self._assembly_complete then
		return 
	end

	self._gadget_color = color

	NPCAkimboWeaponBase.super.set_gadget_color(self, color)

	if alive(self._second_gun) then
		self._second_gun:base():set_gadget_color(color)
	end

	return 
end
NPCAkimboWeaponBase.destroy = function (self, ...)
	NPCAkimboWeaponBase.super.destroy(self, ...)

	if alive(self._second_gun) then
		self._second_gun:set_slot(0)
	end

	return 
end
EnemyAkimboWeaponBase = EnemyAkimboWeaponBase or class(NPCRaycastWeaponBase)
EnemyAkimboWeaponBase.AKIMBO = true
EnemyAkimboWeaponBase.init = function (self, ...)
	NPCRaycastWeaponBase.init(self, ...)

	return 
end
EnemyAkimboWeaponBase.create_second_gun = function (self, unit_name)
	local new_unit = World:spawn_unit(unit_name, Vector3(), Rotation())

	if self._cosmetics_id then
		new_unit.base(new_unit):set_cosmetics_data({
			id = self._cosmetics_id,
			quality = self._cosmetics_quality,
			bonus = self._cosmetics_bonus
		})
	end

	self._second_gun = new_unit
	self._second_gun:base().SKIP_AMMO = true
	self._second_gun:base().parent_weapon = self._unit

	new_unit.base(new_unit):setup(self._setup)

	if self._enabled then
		self._second_gun:base():on_enabled()
	else
		self._second_gun:base():on_disabled()
	end

	self._setup.user_unit:link(Idstring("a_weapon_left_front"), self._second_gun, self._second_gun:orientation_object():name())

	self._muzzle_effect_table_second = {
		effect = self._muzzle_effect_table.effect,
		force_synch = self._muzzle_effect_table.force_synch,
		parent = self._second_gun:get_object(Idstring("fire"))
	}

	return 
end
EnemyAkimboWeaponBase._spawn_muzzle_effect = function (self, from_pos, direction)
	World:effect_manager():spawn(self._muzzle_effect_table_second)
	RaycastWeaponBase._spawn_muzzle_effect(self, from_pos, direction)

	return 
end
EnemyAkimboWeaponBase.tweak_data_anim_play = function (self, anim, ...)
	local animations = self.weapon_tweak_data(self).animations

	if animations and animations[anim] then
		self.anim_play(self, animations[anim], ...)

		return RaycastWeaponBase.tweak_data_anim_play(self, anim, ...)
	end

	return RaycastWeaponBase.tweak_data_anim_play(self, anim, ...)
end
EnemyAkimboWeaponBase.anim_play = function (self, anim, speed_multiplier)
	print("anim oookay ", anim)

	if anim then
		local length = self._unit:anim_length(Idstring(anim))
		speed_multiplier = speed_multiplier or 1

		self._second_gun:anim_stop(Idstring(anim))
		self._second_gun:anim_play_to(Idstring(anim), length, speed_multiplier)
	end

	return 
end
EnemyAkimboWeaponBase.tweak_data_anim_stop = function (self, anim, ...)
	local animations = self.weapon_tweak_data(self).animations

	if animations and animations[anim] then
		self.anim_stop(self, self.weapon_tweak_data(self).animations[anim], ...)

		return RaycastWeaponBase.tweak_data_anim_stop(self, anim, ...)
	end

	return RaycastWeaponBase.tweak_data_anim_stop(self, anim, ...)
end
EnemyAkimboWeaponBase.anim_stop = function (self, anim)
	self._second_gun:anim_stop(Idstring(anim))

	return 
end

if _G.IS_VR then
	require("lib/units/weapons/vr/AkimboWeaponBaseVR")
end

return 
