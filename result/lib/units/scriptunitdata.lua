ScriptUnitData = ScriptUnitData or class(CoreScriptUnitData)
ScriptUnitData.init = function (self, unit)
	CoreScriptUnitData.init(self)

	if managers.occlusion and self.skip_occlusion then
		managers.occlusion:remove_occlusion(unit)
	end

	return 
end
ScriptUnitData.destroy = function (self, unit)
	if managers.occlusion and self.skip_occlusion then
		managers.occlusion:add_occlusion(unit)
	end

	if self._destroy_listener_holder then
		self._destroy_listener_holder:call(unit)
	end

	return 
end
ScriptUnitData.add_destroy_listener = function (self, key, clbk)
	self._destroy_listener_holder = self._destroy_listener_holder or ListenerHolder:new()

	self._destroy_listener_holder:add(key, clbk)

	return 
end
ScriptUnitData.remove_destroy_listener = function (self, key)
	self._destroy_listener_holder:remove(key)

	if self._destroy_listener_holder:is_empty() then
		self._destroy_listener_holder = nil
	end

	return 
end

return 
