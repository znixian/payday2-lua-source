CopActionTurn = CopActionTurn or class()
local tmp_rot = Rotation()
local mrot_set_ypr = mrotation.set_yaw_pitch_roll
CopActionTurn.init = function (self, action_desc, common_data)
	self._common_data = common_data
	self._ext_movement = common_data.ext_movement
	self._ext_anim = common_data.ext_anim
	self._ext_base = common_data.ext_base
	self._machine = common_data.machine
	self._action_desc = action_desc
	self._start_pos = mvector3.copy(common_data.pos)

	if not self._ext_anim.idle then
		local redir_res = self._ext_movement:play_redirect("idle")

		if not redir_res then
			debug_pause("[CopActionTurn:init] idle redirect failed in", self._machine:segment_state(Idstring("base")))

			return false
		end
	end

	self.update = self._upd_wait_full_blend

	self._ext_movement:enable_update()
	CopActionAct._create_blocks_table(self, action_desc.blocks)

	return true
end
CopActionTurn.on_exit = function (self)
	self._common_data.unit:set_driving("script")
	self._ext_movement:set_root_blend(true)
	self._ext_movement:set_position(self._start_pos)

	local end_rot = self._common_data.rot

	mrot_set_ypr(tmp_rot, mrotation.yaw(end_rot), 0, 0)
	self._ext_movement:set_rotation(tmp_rot)

	return 
end
CopActionTurn.update = function (self, t)
	if not self._ext_anim.turn and self._ext_anim.idle_full_blend then
		self._expired = true
	end

	self._ext_movement:set_m_rot(self._common_data.unit:rotation())

	return 
end
CopActionTurn._upd_wait_full_blend = function (self, t)
	if self._ext_anim.idle_full_blend then
		local angle = self._action_desc.angle
		local dir_str = (0 < angle and "l") or "r"
		local redir_name = "turn_" .. dir_str
		local redir_res = self._ext_movement:play_redirect(redir_name)

		if redir_res then
			local abs_angle = math.abs(angle)

			if 135 < abs_angle then
				self._machine:set_parameter(redir_res, "angle135", 1)
			elseif 90 < abs_angle then
				local lerp = (abs_angle - 90)/45

				self._machine:set_parameter(redir_res, "angle135", lerp)
				self._machine:set_parameter(redir_res, "angle90", lerp - 1)
			elseif 45 < abs_angle then
				local lerp = (abs_angle - 45)/45

				self._machine:set_parameter(redir_res, "angle90", lerp)
				self._machine:set_parameter(redir_res, "angle45", lerp - 1)
			else
				self._machine:set_parameter(redir_res, "angle45", 1)
			end

			local vis_state = self._ext_base:lod_stage() or 4

			if 1 < vis_state then
				self._machine:set_speed(redir_res, vis_state)
			end

			self._common_data.unit:set_driving("animation")
			self._ext_movement:set_root_blend(false)
			self._ext_base:chk_freeze_anims()

			self.update = nil

			self.update(self, t)
		else
			cat_print("george", "[CopActionTurn:update] ", redir_name, " redirect failed in", self._machine:segment_state(Idstring("base")))

			self._expired = true
		end
	end

	return 
end
CopActionTurn.type = function (self)
	return "turn"
end
CopActionTurn.expired = function (self)
	return self._expired
end
CopActionTurn.need_upd = function (self)
	return true
end

return 
