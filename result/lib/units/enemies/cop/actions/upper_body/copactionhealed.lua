CopActionHealed = CopActionHealed or class()
local tmp_vec1 = Vector3()
CopActionHealed.init = function (self, action_desc, common_data)
	self._common_data = common_data
	self._ext_movement = common_data.ext_movement
	self._ext_inventory = common_data.ext_inventory
	self._ext_anim = common_data.ext_anim
	self._body_part = action_desc.body_part
	self._unit = common_data.unit
	self._machine = common_data.machine
	self._attention = common_data.attention
	self._action_desc = action_desc

	if not self._unit:base():char_tweak().ignore_medic_revive_animation then
		self._ext_movement:play_redirect("use_syringe")
	end

	self._unit:sound():say("hr01")

	self._healed = false

	if self._unit:contour() then
		self._unit:contour():add("medic_heal", true)
		self._unit:contour():flash("medic_heal", 0.2)
	end

	return true
end
CopActionHealed.on_exit = function (self)
	return 
end
CopActionHealed.update = function (self, t)
	if not self._unit:anim_data().heal then
		self._healed = true

		self._ext_movement:play_redirect("idle")
		self._ext_movement:action_request({
			body_part = 2,
			type = "idle"
		})
	end

	return 
end
CopActionHealed.type = function (self)
	return "healed"
end
CopActionHealed.expired = function (self)
	return self._expired
end
CopActionHealed.chk_block = function (self, action_type, t)
	if action_type == "heavy_hurt" or action_type == "hurt" or action_type == "death" then
		return false
	end

	return not self._healed
end
CopActionHealed.on_attention = function (self, attention)
	self._attention = attention

	return 
end
CopActionHealed.body_part = function (self)
	return self._body_part
end
CopActionHealed.need_upd = function (self)
	return true
end
CopActionHealed.save = function (self, save_data)
	for i, k in pairs(self._action_desc) do
		if type_name(k) ~= "Unit" or alive(k) then
			save_data[i] = k
		end
	end

	return 
end

return 
