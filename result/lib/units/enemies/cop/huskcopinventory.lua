HuskCopInventory = HuskCopInventory or class(HuskPlayerInventory)
HuskCopInventory.init = function (self, unit)
	CopInventory.init(self, unit)

	return 
end
HuskCopInventory.set_visibility_state = function (self, state)
	CopInventory.set_visibility_state(self, state)

	return 
end
HuskCopInventory.add_unit_by_name = function (self, new_unit_name, equip)
	local new_unit = World:spawn_unit(new_unit_name, Vector3(), Rotation())

	managers.mutators:modify_value("CopInventory:add_unit_by_name", self)
	CopInventory._chk_spawn_shield(self, new_unit)

	local setup_data = {
		user_unit = self._unit,
		ignore_units = {
			self._unit,
			new_unit,
			self._shield_unit
		},
		expend_ammo = false,
		hit_slotmask = managers.slot:get_mask("bullet_impact_targets_no_AI"),
		hit_player = true,
		user_sound_variant = tweak_data.character[self._unit:base()._tweak_table].weapon_voice
	}

	new_unit.base(new_unit):setup(setup_data)

	if new_unit.base(new_unit).AKIMBO then
		new_unit.base(new_unit):create_second_gun(new_unit_name)
	end

	CopInventory.add_unit(self, new_unit, equip)

	return 
end
HuskCopInventory.get_weapon = function (self)
	CopInventory.get_weapon(self)

	return 
end
HuskCopInventory.drop_weapon = function (self)
	CopInventory.drop_weapon(self)

	return 
end
HuskCopInventory.drop_shield = function (self)
	CopInventory.drop_shield(self)

	return 
end
HuskCopInventory.destroy_all_items = function (self)
	CopInventory.destroy_all_items(self)

	return 
end
HuskCopInventory.add_unit = function (self, new_unit, equip)
	CopInventory.add_unit(self, new_unit, equip)

	return 
end
HuskCopInventory.set_visibility_state = function (self, state)
	CopInventory.set_visibility_state(self, state)

	return 
end

return 
