HuskCopBrain = HuskCopBrain or class()
HuskCopBrain._NET_EVENTS = {
	weapon_laser_off = 2,
	weapon_laser_on = 1
}
HuskCopBrain._ENABLE_LASER_TIME = 3
HuskCopBrain._get_radio_id = CopBrain._get_radio_id
HuskCopBrain.init = function (self, unit)
	self._unit = unit

	return 
end
HuskCopBrain.post_init = function (self)
	self._alert_listen_key = "HuskCopBrain" .. tostring(self._unit:key())
	local alert_listen_filter = managers.groupai:state():get_unit_type_filter("criminal")

	managers.groupai:state():add_alert_listener(self._alert_listen_key, callback(self, self, "on_alert"), alert_listen_filter, {
		aggression = true,
		bullet = true,
		vo_intimidate = true,
		explosion = true,
		footstep = true,
		vo_cbt = true
	}, self._unit:movement():m_head_pos())

	self._last_alert_t = 0

	self._unit:character_damage():add_listener("HuskCopBrain_death" .. tostring(self._unit:key()), {
		"death"
	}, callback(self, self, "clbk_death"))

	self._post_init_complete = true
	self._surrendered = false

	return 
end
HuskCopBrain.interaction_voice = function (self)
	return self._interaction_voice
end
HuskCopBrain.on_intimidated = function (self, amount, aggressor_unit)
	amount = math.clamp(math.ceil(amount*10), 0, 10)

	self._unit:network():send_to_host("long_dis_interaction", amount, aggressor_unit, false)

	return self._interaction_voice
end
HuskCopBrain.clbk_death = function (self, my_unit, damage_info)
	if self._alert_listen_key then
		managers.groupai:state():remove_alert_listener(self._alert_listen_key)

		self._alert_listen_key = nil
	end

	if self._unit:inventory():equipped_unit() then
		self._unit:inventory():equipped_unit():base():set_laser_enabled(false)
	end

	if self._following_hostage_contour_id then
		self._unit:contour():remove_by_id(self._following_hostage_contour_id)

		self._following_hostage_contour_id = nil
	end

	return 
end
HuskCopBrain.set_interaction_voice = function (self, voice)
	self._interaction_voice = voice

	return 
end
HuskCopBrain.load = function (self, load_data)
	local my_load_data = load_data.brain

	self.set_interaction_voice(self, my_load_data.interaction_voice)

	if my_load_data.weapon_laser_on then
		self.sync_net_event(self, self._NET_EVENTS.weapon_laser_on)
	end

	if my_load_data.trade_flee_contour then
		self._unit:contour():add("hostage_trade", nil, nil)
	end

	if my_load_data.following_hostage_contour then
		self._unit:contour():add("friendly", nil, nil)
	end

	self._surrendered = my_load_data.surrendered

	return 
end
HuskCopBrain.on_tied = function (self, aggressor_unit, not_tied, can_flee)
	self._unit:network():send_to_host("unit_tied", aggressor_unit, can_flee)

	return 
end
HuskCopBrain.on_trade = function (self, position, rotation)
	self._unit:network():send_to_host("unit_traded", position, rotation)

	return 
end
HuskCopBrain.on_cool_state_changed = function (self, state)
	return 
end
HuskCopBrain.action_complete_clbk = function (self, action)
	return 
end
HuskCopBrain.on_alert = function (self, alert_data)
	if self._unit:id() == -1 then
		return 
	end

	if TimerManager:game():time() - self._last_alert_t < 5 then
		return 
	end

	if CopLogicBase._chk_alert_obstructed(self._unit:movement():m_head_pos(), alert_data) then
		return 
	end

	self._unit:network():send_to_host("alert", alert_data[5])

	self._last_alert_t = TimerManager:game():time()

	return 
end
HuskCopBrain.sync_surrender = function (self, surrendered)
	self._surrendered = surrendered

	return 
end
HuskCopBrain.surrendered = function (self)
	return self._surrendered
end
HuskCopBrain.on_long_dis_interacted = function (self, amount, aggressor_unit, secondary)
	secondary = secondary or false
	amount = math.clamp(math.ceil(amount*10), 0, 10)

	self._unit:network():send_to_host("long_dis_interaction", amount, aggressor_unit, secondary)

	return 
end
HuskCopBrain.player_ignore = function (self)
	return false
end
HuskCopBrain.on_team_set = function (self, team_data)
	return 
end
HuskCopBrain.update = function (self, unit, t, dt)
	if self._add_laser_t ~= nil and self._post_init_complete then
		self._add_laser_t = self._add_laser_t - dt

		if self._add_laser_t < 0 then
			self.enable_weapon_laser(self)

			self._add_laser_t = nil
		end
	end

	return 
end
HuskCopBrain.sync_net_event = function (self, event_id)
	if event_id == self._NET_EVENTS.weapon_laser_on then
		self._add_laser_t = HuskCopBrain._ENABLE_LASER_TIME
	elseif event_id == self._NET_EVENTS.weapon_laser_off then
		self.disable_weapon_laser(self)
	end

	return 
end
HuskCopBrain.enable_weapon_laser = function (self)
	self._weapon_laser_on = true

	self._unit:inventory():equipped_unit():base():set_laser_enabled(true)
	managers.enemy:_destroy_unit_gfx_lod_data(self._unit:key())

	return 
end
HuskCopBrain.disable_weapon_laser = function (self)
	self._weapon_laser_on = nil

	if self._unit:inventory():equipped_unit() then
		self._unit:inventory():equipped_unit():base():set_laser_enabled(false)
	end

	if not self._unit:character_damage():dead() then
		managers.enemy:_create_unit_gfx_lod_data(self._unit)
	end

	return 
end
HuskCopBrain.pre_destroy = function (self)
	if Network:is_server() then
		self._unit:movement():set_attention()
	else
		self._unit:movement():synch_attention()
	end

	if self._alert_listen_key then
		managers.groupai:state():remove_alert_listener(self._alert_listen_key)

		self._alert_listen_key = nil
	end

	if self._weapon_laser_on then
		self.sync_net_event(self, self._NET_EVENTS.weapon_laser_off)
	end

	return 
end

return 
