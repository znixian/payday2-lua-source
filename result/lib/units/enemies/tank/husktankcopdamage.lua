HuskTankCopDamage = HuskTankCopDamage or class(HuskCopDamage)
HuskTankCopDamage.init = function (self, ...)
	HuskTankCopDamage.super.init(self, ...)

	self._is_halloween = self._unit:name() == Idstring("units/payday2/characters/ene_bulldozer_4/ene_bulldozer_4_husk")

	return 
end
HuskTankCopDamage.damage_bullet = function (self, attack_data, ...)
	if self._is_halloween then
		attack_data.damage = math.min(attack_data.damage, 235)
	end

	return HuskTankCopDamage.super.damage_bullet(self, attack_data, ...)
end
HuskTankCopDamage.seq_clbk_vizor_shatter = function (self)
	TankCopDamage.seq_clbk_vizor_shatter(self)

	return 
end

return 
