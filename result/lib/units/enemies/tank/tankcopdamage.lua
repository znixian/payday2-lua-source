TankCopDamage = TankCopDamage or class(CopDamage)
TankCopDamage.init = function (self, ...)
	TankCopDamage.super.init(self, ...)

	self._is_halloween = self._unit:name() == Idstring("units/payday2/characters/ene_bulldozer_4/ene_bulldozer_4")

	return 
end
TankCopDamage.damage_bullet = function (self, attack_data, ...)
	if self._is_halloween then
		attack_data.damage = math.min(attack_data.damage, 235)
	end

	return TankCopDamage.super.damage_bullet(self, attack_data, ...)
end
TankCopDamage.seq_clbk_vizor_shatter = function (self)
	if not self._unit:character_damage():dead() then
		self._unit:sound():say("visor_lost")
		managers.crime_spree:run_func("OnTankVisorShatter", self._unit)
	end

	return 
end

return 
