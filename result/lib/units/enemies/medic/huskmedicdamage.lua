HuskMedicDamage = HuskMedicDamage or class(HuskCopDamage)
HuskMedicDamage.init = function (self, ...)
	HuskMedicDamage.super.init(self, ...)

	self._heal_cooldown_t = 0
	self._debug_brush = Draw:brush(Color(0.5, 0.5, 0, 1))

	return 
end
HuskMedicDamage.update = function (self, ...)
	MedicDamage.update(self, ...)

	return 
end
HuskMedicDamage.heal_unit = function (self, ...)
	return MedicDamage.heal_unit(self, ...)
end

return 
