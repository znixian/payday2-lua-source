MedicActionHeal = MedicActionHeal or class()
MedicActionHeal.init = function (self, action_desc, common_data)
	self._common_data = common_data
	self._ext_movement = common_data.ext_movement
	self._ext_inventory = common_data.ext_inventory
	self._ext_anim = common_data.ext_anim
	self._body_part = action_desc.body_part
	self._unit = common_data.unit
	self._machine = common_data.machine
	self._attention = common_data.attention
	self._action_desc = action_desc

	self._ext_movement:play_redirect("heal")
	self._unit:sound():say("heal", true)

	self._done = false

	self.check_achievements(self)

	return true
end
MedicActionHeal.on_exit = function (self)
	if self._unit:contour() then
		self._unit:contour():remove("medic_healing", true)
	end

	return 
end
MedicActionHeal.update = function (self, t)
	if not self._unit:anim_data().healing then
		self._done = true

		self._ext_movement:play_redirect("idle")
		self._ext_movement:action_request({
			body_part = 2,
			type = "idle"
		})
	end

	return 
end
MedicActionHeal.type = function (self)
	return "heal"
end
MedicActionHeal.expired = function (self)
	return self._expired
end
MedicActionHeal.chk_block = function (self, action_type, t)
	if action_type == "heavy_hurt" or action_type == "hurt" or action_type == "death" then
		return false
	end

	return not self._done
end
MedicActionHeal.on_attention = function (self, attention)
	self._attention = attention

	return 
end
MedicActionHeal.body_part = function (self)
	return self._body_part
end
MedicActionHeal.need_upd = function (self)
	return true
end
MedicActionHeal.save = function (self, save_data)
	for i, k in pairs(self._action_desc) do
		if type_name(k) ~= "Unit" or alive(k) then
			save_data[i] = k
		end
	end

	return 
end
MedicActionHeal.check_achievements = function (self)
	local total_healed = (managers.job:get_memory("medic_heal_total", true) or 0) + 1

	managers.job:set_memory("medic_heal_total", total_healed, true)

	local all_pass, total_pass = nil
	slot4 = pairs
	slot5 = tweak_data.achievement.medic_heal_achievements or {}

	for achievement, achievement_data in slot4(slot5) do
		total_pass = not achievement_data.total

		if achievement_data.total then
			total_pass = achievement_data.total <= total_healed
		end

		all_pass = total_pass

		if all_pass then
			managers.achievment:award_data(achievement_data)
		end
	end

	return 
end

return 
