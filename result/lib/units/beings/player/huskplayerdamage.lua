HuskPlayerDamage = HuskPlayerDamage or class()
HuskPlayerDamage.init = function (self, unit)
	self._unit = unit
	self._spine2_obj = unit.get_object(unit, Idstring("Spine2"))
	self._listener_holder = EventListenerHolder:new()
	self._mission_damage_blockers = {}
	local level_tweak = tweak_data.levels[managers.job:current_level_id()]

	if level_tweak and level_tweak.is_safehouse and not level_tweak.is_safehouse_combat then
		self.set_mission_damage_blockers(self, "damage_fall_disabled", true)
		self.set_mission_damage_blockers(self, "invulnerable", true)
	end

	return 
end
HuskPlayerDamage._call_listeners = function (self, damage_info)
	CopDamage._call_listeners(self, damage_info)

	return 
end
HuskPlayerDamage.add_listener = function (self, ...)
	CopDamage.add_listener(self, ...)

	return 
end
HuskPlayerDamage.remove_listener = function (self, key)
	CopDamage.remove_listener(self, key)

	return 
end
HuskPlayerDamage.sync_damage_bullet = function (self, attacker_unit, damage, i_body, height_offset)
	local attack_data = {
		attacker_unit = attacker_unit,
		attack_dir = (attacker_unit and attacker_unit.movement(attacker_unit):m_pos() - self._unit:movement():m_pos()) or Vector3(1, 0, 0),
		pos = mvector3.copy(self._unit:movement():m_head_pos()),
		result = {
			variant = "bullet",
			type = "hurt"
		}
	}

	self._call_listeners(self, attack_data)

	return 
end
HuskPlayerDamage.shoot_pos_mid = function (self, m_pos)
	self._spine2_obj:m_position(m_pos)

	return 
end
HuskPlayerDamage.can_attach_projectiles = function (self)
	return false
end
HuskPlayerDamage.set_last_down_time = function (self, down_time)
	self._last_down_time = down_time

	return 
end
HuskPlayerDamage.down_time = function (self)
	return self._last_down_time
end
HuskPlayerDamage.arrested = function (self)
	return self._unit:movement():current_state_name() == "arrested"
end
HuskPlayerDamage.incapacitated = function (self)
	return self._unit:movement():current_state_name() == "incapacitated"
end
HuskPlayerDamage.set_mission_damage_blockers = function (self, type, state)
	self._mission_damage_blockers[type] = state

	return 
end
HuskPlayerDamage.get_mission_blocker = function (self, type)
	return self._mission_damage_blockers[type]
end
HuskPlayerDamage.dead = function (self)
	return 
end
HuskPlayerDamage.damage_bullet = function (self, attack_data)
	if managers.mutators:is_mutator_active(MutatorFriendlyFire) then
		self._send_damage_to_owner(self, attack_data)
	end

	return 
end
HuskPlayerDamage.damage_melee = function (self, attack_data)
	if managers.mutators:is_mutator_active(MutatorFriendlyFire) then
		self._send_damage_to_owner(self, attack_data)
	end

	return 
end
HuskPlayerDamage.damage_fire = function (self, attack_data)
	if managers.mutators:is_mutator_active(MutatorFriendlyFire) then
		attack_data.damage = attack_data.damage*0.2

		self._send_damage_to_owner(self, attack_data)
	end

	return 
end
HuskPlayerDamage._send_damage_to_owner = function (self, attack_data)
	local peer_id = managers.criminals:character_peer_id_by_unit(self._unit)
	local damage = managers.mutators:modify_value("HuskPlayerDamage:FriendlyFireDamage", attack_data.damage)

	managers.network:session():send_to_peers("sync_friendly_fire_damage", peer_id, attack_data.attacker_unit, damage, attack_data.variant)

	if attack_data.attacker_unit == managers.player:player_unit() then
		managers.hud:on_hit_confirmed()
	end

	managers.job:set_memory("trophy_flawless", true, false)

	return 
end

return 
