PlayerInventory = PlayerInventory or class()
PlayerInventory._all_event_types = {
	"add",
	"equip",
	"unequip"
}
PlayerInventory.init = function (self, unit)
	self._unit = unit
	self._available_selections = {}
	self._equipped_selection = nil
	self._latest_addition = nil
	self._selected_primary = nil
	self._use_data_alias = "player"
	self._align_places = {
		right_hand = {
			on_body = false,
			obj3d_name = Idstring("a_weapon_right")
		},
		left_hand = {
			on_body = false,
			obj3d_name = Idstring("a_weapon_left")
		}
	}
	self._listener_id = "PlayerInventory" .. tostring(unit.key(unit))
	self._listener_holder = EventListenerHolder:new()
	self._mask_unit = nil
	self._melee_weapon_unit = nil
	self._melee_weapon_unit_name = nil

	return 
end
PlayerInventory.pre_destroy = function (self, unit)
	if self._weapon_add_clbk then
		if managers.enemy:is_clbk_registered(self._weapon_add_clbk) then
			managers.enemy:remove_delayed_clbk(self._weapon_add_clbk)
		else
			Application:error("[PlayerInventory] Attempted to remove a callback that wasn't registred! " .. tostring(self._weapon_add_clbk))
		end

		self._weapon_add_clbk = nil
	end

	self.destroy_all_items(self)

	return 
end
PlayerInventory.destroy_all_items = function (self)
	for i_sel, selection_data in pairs(self._available_selections) do
		if selection_data.unit and selection_data.unit:base() then
			selection_data.unit:base():remove_destroy_listener(self._listener_id)
			selection_data.unit:base():set_slot(selection_data.unit, 0)
		else
			debug_pause_unit(self._unit, "[PlayerInventory:destroy_all_items] broken inventory unit", selection_data.unit, selection_data.unit:base())
		end
	end

	self._equipped_selection = nil
	self._available_selections = {}

	if alive(self._mask_unit) then
		for _, linked_unit in ipairs(self._mask_unit:children()) do
			linked_unit.unlink(linked_unit)
			World:delete_unit(linked_unit)
		end

		World:delete_unit(self._mask_unit)

		self._mask_unit = nil
	end

	if self._melee_weapon_unit_name then
		managers.dyn_resource:unload(Idstring("unit"), self._melee_weapon_unit_name, DynamicResourceManager.DYN_RESOURCES_PACKAGE, false)

		self._melee_weapon_unit_name = nil
	end

	return 
end
PlayerInventory.equipped_selection = function (self)
	return self._equipped_selection
end
PlayerInventory.equipped_unit = function (self)
	return self._equipped_selection and self._available_selections[self._equipped_selection].unit
end
PlayerInventory.unit_by_selection = function (self, selection)
	return self._available_selections[selection] and self._available_selections[selection].unit
end
PlayerInventory.is_selection_available = function (self, selection_index)
	return (self._available_selections[selection_index] and true) or false
end
PlayerInventory.add_unit = function (self, new_unit, is_equip, equip_is_instant)
	local new_selection = {}
	local use_data = new_unit.base(new_unit):get_use_data(self._use_data_alias)
	new_selection.use_data = use_data
	new_selection.unit = new_unit

	new_unit.base(new_unit):add_destroy_listener(self._listener_id, callback(self, self, "clbk_weapon_unit_destroyed"))

	local selection_index = use_data.selection_index

	if self._available_selections[selection_index] then
		local old_weapon_unit = self._available_selections[selection_index].unit
		is_equip = is_equip or old_weapon_unit == self.equipped_unit(self)

		old_weapon_unit.base(old_weapon_unit):remove_destroy_listener(self._listener_id)
		old_weapon_unit.base(old_weapon_unit):set_slot(old_weapon_unit, 0)
		World:delete_unit(old_weapon_unit)

		if self._equipped_selection == selection_index then
			self._equipped_selection = nil
		end
	end

	self._available_selections[selection_index] = new_selection
	self._latest_addition = selection_index
	self._selected_primary = self._selected_primary or selection_index

	self._call_listeners(self, "add")

	if is_equip then
		self.equip_latest_addition(self, equip_is_instant)
	else
		self._place_selection(self, selection_index, is_equip)
	end

	return 
end
PlayerInventory.clbk_weapon_unit_destroyed = function (self, weap_unit)
	local weapon_key = weap_unit.key(weap_unit)

	for i_sel, sel_data in pairs(self._available_selections) do
		if sel_data.unit:key() == weapon_key then
			if i_sel == self._equipped_selection then
				self._call_listeners(self, "unequip")
			end

			self.remove_selection(self, i_sel, true)

			break
		end
	end

	return 
end
PlayerInventory.get_latest_addition_hud_data = function (self)
	local unit = self._available_selections[self._latest_addition].unit
	local _, _, amount = unit.base(unit):ammo_info()

	return {
		is_equip = self._latest_addition == self._selected_primary,
		amount = amount,
		inventory_index = self._latest_addition,
		unit = unit
	}
end
PlayerInventory.add_unit_by_name = function (self, new_unit_name, equip, instant)
	for _, selection in pairs(self._available_selections) do
		if selection.unit:name() == new_unit_name then
			return 
		end
	end

	local new_unit = World:spawn_unit(new_unit_name, Vector3(), Rotation())
	local setup_data = {
		user_unit = self._unit,
		ignore_units = {
			self._unit,
			new_unit
		},
		expend_ammo = true,
		autoaim = true,
		alert_AI = true,
		alert_filter = self._unit:movement():SO_access()
	}

	new_unit.base(new_unit):setup(setup_data)
	self.add_unit(self, new_unit, equip, instant)

	return 
end
PlayerInventory.add_unit_by_factory_name = function (self, factory_name, equip, instant, blueprint, cosmetics, texture_switches)
	local factory_weapon = tweak_data.weapon.factory[factory_name]
	local ids_unit_name = Idstring(factory_weapon.unit)

	if not managers.dyn_resource:is_resource_ready(Idstring("unit"), ids_unit_name, managers.dyn_resource.DYN_RESOURCES_PACKAGE) then
		managers.dyn_resource:load(Idstring("unit"), ids_unit_name, managers.dyn_resource.DYN_RESOURCES_PACKAGE, nil)
	end

	local new_unit = World:spawn_unit(ids_unit_name, Vector3(), Rotation())

	new_unit.base(new_unit):set_factory_data(factory_name)
	new_unit.base(new_unit):set_cosmetics_data(cosmetics)
	new_unit.base(new_unit):set_texture_switches(texture_switches)

	if blueprint then
		new_unit.base(new_unit):assemble_from_blueprint(factory_name, blueprint)
	else
		new_unit.base(new_unit):assemble(factory_name)
	end

	local setup_data = {
		user_unit = self._unit,
		ignore_units = {
			self._unit,
			new_unit
		},
		expend_ammo = true,
		autoaim = true,
		alert_AI = true,
		alert_filter = self._unit:movement():SO_access(),
		timer = managers.player:player_timer()
	}

	if blueprint then
		setup_data.panic_suppression_skill = (not managers.weapon_factory:has_perk("silencer", factory_name, blueprint) and managers.player:has_category_upgrade("player", "panic_suppression")) or false
	end

	new_unit.base(new_unit):setup(setup_data)
	self.add_unit(self, new_unit, equip, instant)

	if new_unit.base(new_unit).AKIMBO then
		new_unit.base(new_unit):create_second_gun()
	end

	return 
end
PlayerInventory.remove_selection = function (self, selection_index, instant)
	selection_index = selection_index or self._equipped_selection
	local weap_unit = self._available_selections[selection_index].unit

	if alive(weap_unit) then
		weap_unit.base(weap_unit):remove_destroy_listener(self._listener_id)
	end

	self._available_selections[selection_index] = nil

	if self._equipped_selection == selection_index then
		self._equipped_selection = nil
	end

	if selection_index == self._selected_primary then
		self._selected_primary = self._select_new_primary(self)
	end

	return 
end
PlayerInventory.equip_latest_addition = function (self, instant)
	return self.equip_selection(self, self._latest_addition, instant)
end
PlayerInventory.equip_selected_primary = function (self, instant)
	return self.equip_selection(self, self._selected_primary, instant)
end
PlayerInventory.get_next_selection = function (self)
	local i = self._selected_primary

	for i = self._selected_primary, self._selected_primary + 9, 1 do
		local selection = math.mod(i, 10) + 1

		if self._available_selections[selection] then
			return self._available_selections[selection], selection
		end
	end

	return nil
end
PlayerInventory.equip_next = function (self, instant)
	local got, selection = self.get_next_selection(self)

	if got then
		return self.equip_selection(self, selection, instant)
	end

	return false
end
PlayerInventory.get_previous_selection = function (self)
	local i = self._selected_primary

	for i = self._selected_primary, self._selected_primary - 9, -1 do
		local selection = math.mod(i + 8, 10) + 1

		if self._available_selections[selection] then
			return self._available_selections[selection], selection
		end
	end

	return nil
end
PlayerInventory.equip_previous = function (self, instant)
	local got, selection = self.get_previous_selection(self)

	if got then
		return self.equip_selection(self, selection, instant)
	end

	return false
end
PlayerInventory.get_selected = function (self, selection_index)
	return selection_index and selection_index ~= self._equipped_selection and self._available_selections[selection_index]
end
PlayerInventory.equip_selection = function (self, selection_index, instant)
	if selection_index and selection_index ~= self._equipped_selection and self._available_selections[selection_index] then
		if self._equipped_selection then
			self.unequip_selection(self, nil, instant)
		end

		self._equipped_selection = selection_index

		self._place_selection(self, selection_index, true)

		self._selected_primary = selection_index

		self._send_equipped_weapon(self)
		self._call_listeners(self, "equip")

		if self._unit:unit_data().mugshot_id then
			local hud_icon_id = self.equipped_unit(self):base():weapon_tweak_data().hud_icon

			managers.hud:set_mugshot_weapon(self._unit:unit_data().mugshot_id, hud_icon_id, self.equipped_unit(self):base():weapon_tweak_data().use_data.selection_index)
		end

		self.equipped_unit(self):base():set_flashlight_enabled(true)

		return true
	end

	return false
end
PlayerInventory._send_equipped_weapon = function (self)
	local eq_weap_name = self.equipped_unit(self):base()._factory_id or self.equipped_unit(self):name()
	local index = self._get_weapon_sync_index(eq_weap_name)

	if not index then
		debug_pause("[PlayerInventory:_send_equipped_weapon] cannot sync weapon", eq_weap_name, self._unit)

		return 
	end

	local blueprint_string = (self.equipped_unit(self):base()._blueprint and self.equipped_unit(self):base().blueprint_to_string and self.equipped_unit(self):base():blueprint_to_string()) or ""
	local cosmetics_string = ""
	local cosmetics_id = (self.equipped_unit(self):base().get_cosmetics_id and self.equipped_unit(self):base():get_cosmetics_id()) or nil

	if cosmetics_id then
		local cosmetics_quality = (self.equipped_unit(self):base().get_cosmetics_quality and self.equipped_unit(self):base():get_cosmetics_quality()) or nil
		local cosmetics_bonus = (self.equipped_unit(self):base().get_cosmetics_bonus and self.equipped_unit(self):base():get_cosmetics_bonus()) or nil
		local entry = tostring(cosmetics_id)
		local quality = tostring(tweak_data.economy:get_index_from_entry("qualities", cosmetics_quality) or 1)
		local bonus = (cosmetics_bonus and "1") or "0"
		cosmetics_string = entry .. "-" .. quality .. "-" .. bonus
	else
		cosmetics_string = "nil-1-0"
	end

	self._unit:network():send("set_equipped_weapon", index, blueprint_string, cosmetics_string)

	return 
end
PlayerInventory.unequip_selection = function (self, selection_index, instant)
	if not selection_index or selection_index == self._equipped_selection then
		self._call_listeners(self, "unequip")
		self.equipped_unit(self):base():set_flashlight_enabled(false)

		selection_index = selection_index or self._equipped_selection

		self._place_selection(self, selection_index, false)

		self._equipped_selection = nil
	end

	return 
end
PlayerInventory.is_equipped = function (self, index)
	return index == self._equipped_selection
end
PlayerInventory.available_selections = function (self)
	return self._available_selections
end
PlayerInventory.num_selections = function (self)
	return table.size(self._available_selections)
end
PlayerInventory._place_selection = function (self, selection_index, is_equip)
	local selection = self._available_selections[selection_index]
	local unit = selection.unit
	local weap_align_data = selection.use_data[(is_equip and "equip") or "unequip"]
	local align_place = self._align_places[weap_align_data.align_place]

	if align_place then
		if is_equip then
			unit.set_enabled(unit, true)
			unit.base(unit):on_enabled()
		end

		slot7 = self._link_weapon(self, unit, align_place)
	else
		unit.unlink(unit)
		unit.base(unit):set_visibility_state(false)
		unit.set_enabled(unit, false)
		unit.base(unit):on_disabled()

		if unit.base(unit).gadget_on and self._unit:movement().set_cbt_permanent then
			self._unit:movement():set_cbt_permanent(false)
		end
	end

	return 
end
PlayerInventory._link_weapon = function (self, unit, align_place)
	if _G.IS_VR then
		local is_player = managers.player:player_unit() == self._unit

		if is_player then
			return 
		end
	end

	parent_unit = (align_place.on_body and self._unit) or self._unit:camera()._camera_unit
	local res = parent_unit.link(parent_unit, align_place.obj3d_name, unit, unit.orientation_object(unit):name())

	return res
end
PlayerInventory._select_new_primary = function (self)
	for index, use_data in pairs(self._available_selections) do
		return index
	end

	return 
end
PlayerInventory.add_listener = function (self, key, events, clbk)
	events = events or self._all_event_types

	self._listener_holder:add(key, events, clbk)

	return 
end
PlayerInventory.remove_listener = function (self, key)
	self._listener_holder:remove(key)

	return 
end
PlayerInventory._call_listeners = function (self, event)
	self._listener_holder:call(event, self._unit, event)

	return 
end
PlayerInventory.on_death_exit = function (self)
	for i, selection in pairs(self._available_selections) do
		selection.unit:unlink()
	end

	return 
end
PlayerInventory._chk_create_w_factory_indexes = function ()
	if PlayerInventory._weapon_factory_indexed then
		return 
	end

	local weapon_factory_indexed = {}
	PlayerInventory._weapon_factory_indexed = weapon_factory_indexed

	for id, data in pairs(tweak_data.weapon.factory) do
		if id ~= "parts" and data.unit then
			table.insert(weapon_factory_indexed, id)
		end
	end

	table.sort(weapon_factory_indexed, function (a, b)
		return a < b
	end)

	return 
end
PlayerInventory._get_weapon_sync_index = function (wanted_weap_name)
	if type_name(wanted_weap_name) == "Idstring" then
		for i, test_weap_name in ipairs(tweak_data.character.weap_unit_names) do
			if test_weap_name == wanted_weap_name then
				return i
			end
		end
	end

	PlayerInventory._chk_create_w_factory_indexes()

	local start_index = #tweak_data.character.weap_unit_names

	for i, factory_id in ipairs(PlayerInventory._weapon_factory_indexed) do
		if wanted_weap_name == factory_id then
			return start_index + i
		end
	end

	return 
end
PlayerInventory._get_weapon_name_from_sync_index = function (w_index)
	if w_index <= #tweak_data.character.weap_unit_names then
		return tweak_data.character.weap_unit_names[w_index]
	end

	w_index = w_index - #tweak_data.character.weap_unit_names

	PlayerInventory._chk_create_w_factory_indexes()

	return PlayerInventory._weapon_factory_indexed[w_index]
end
PlayerInventory.hide_equipped_unit = function (self)
	local unit = self._equipped_selection and self._available_selections[self._equipped_selection].unit

	if unit and unit.base(unit):enabled() then
		self._was_gadget_on = (unit.base(unit).is_gadget_on and unit.base(unit)._gadget_on) or false

		unit.set_visible(unit, false)
		unit.base(unit):on_disabled()
	end

	return 
end
PlayerInventory.show_equipped_unit = function (self)
	if self._equipped_selection and self._available_selections[self._equipped_selection].unit then
		self._available_selections[self._equipped_selection].unit:set_visible(true)
		self._available_selections[self._equipped_selection].unit:base():on_enabled()

		if self._was_gadget_on then
			self._available_selections[self._equipped_selection].unit:base():set_gadget_on(self._was_gadget_on)

			self._was_gadget_on = nil
		end
	end

	return 
end
PlayerInventory.save = function (self, data)
	if self._equipped_selection then
		local eq_weap_name = self.equipped_unit(self):base()._factory_id or self.equipped_unit(self):name()
		local index = self._get_weapon_sync_index(eq_weap_name)
		data.equipped_weapon_index = index
		data.mask_visibility = self._mask_visibility
		data.blueprint_string = (self.equipped_unit(self):base().blueprint_to_string and self.equipped_unit(self):base():blueprint_to_string()) or nil
		data.gadget_on = self.equipped_unit(self):base().gadget_on and self.equipped_unit(self):base()._gadget_on
		local gadget = self.equipped_unit(self):base().get_active_gadget and self.equipped_unit(self):base():get_active_gadget()

		if gadget and gadget.color then
			data.gadget_color = gadget.color(gadget)
		end

		local cosmetics_string = ""
		local cosmetics_id = (self.equipped_unit(self):base().get_cosmetics_id and self.equipped_unit(self):base():get_cosmetics_id()) or nil

		if cosmetics_id then
			local cosmetics_quality = (self.equipped_unit(self):base().get_cosmetics_quality and self.equipped_unit(self):base():get_cosmetics_quality()) or nil
			local cosmetics_bonus = (self.equipped_unit(self):base().get_cosmetics_bonus and self.equipped_unit(self):base():get_cosmetics_bonus()) or nil
			local entry = tostring(cosmetics_id)
			local quality = tostring(tweak_data.economy:get_index_from_entry("qualities", cosmetics_quality) or 1)
			local bonus = (cosmetics_bonus and "1") or "0"
			cosmetics_string = entry .. "-" .. quality .. "-" .. bonus
		else
			cosmetics_string = "nil-1-0"
		end

		data.cosmetics_string = cosmetics_string
	end

	return 
end
PlayerInventory.cosmetics_string_from_peer = function (self, peer, weapon_name)
	if peer then
		local outfit = peer.blackmarket_outfit(peer)
		local cosmetics = (outfit.primary.factory_id .. "_npc" == weapon_name and outfit.primary.cosmetics) or (outfit.secondary.factory_id .. "_npc" == weapon_name and outfit.secondary.cosmetics)

		if cosmetics then
			local quality = tostring(tweak_data.economy:get_index_from_entry("qualities", cosmetics.quality) or 1)

			return cosmetics.id .. "-" .. quality .. "-" .. ((cosmetics.bonus and "1") or "0")
		else
			return "nil-1-0"
		end
	end

	return 
end
PlayerInventory.load = function (self, data)
	if data.equipped_weapon_index then
		self._weapon_add_clbk = "playerinventory_load"
		local delayed_data = {
			equipped_weapon_index = data.equipped_weapon_index,
			blueprint_string = data.blueprint_string,
			cosmetics_string = data.cosmetics_string,
			gadget_on = data.gadget_on,
			gadget_color = data.gadget_color
		}

		managers.enemy:add_delayed_clbk(self._weapon_add_clbk, callback(self, self, "_clbk_weapon_add", delayed_data), Application:time() + 1)
	end

	self._mask_visibility = (data.mask_visibility and true) or false

	return 
end
PlayerInventory._clbk_weapon_add = function (self, data)
	self._weapon_add_clbk = nil

	if not alive(self._unit) then
		return 
	end

	local eq_weap_name = self._get_weapon_name_from_sync_index(data.equipped_weapon_index)

	if type(eq_weap_name) == "string" then
		if not managers.network:session() then
			return 
		end

		self.add_unit_by_factory_name(self, eq_weap_name, true, true, data.blueprint_string, self.cosmetics_string_from_peer(self, managers.network:session():peer_by_unit(self._unit), eq_weap_name) or data.cosmetics_string)
		self.synch_weapon_gadget_state(self, data.gadget_on)

		if data.gadget_color then
			self.sync_weapon_gadget_color(self, data.gadget_color)
		end
	else
		self._unit:inventory():add_unit_by_name(eq_weap_name, true, true)
	end

	if self._unit:unit_data().mugshot_id then
		local icon = self.equipped_unit(self):base():weapon_tweak_data().hud_icon

		managers.hud:set_mugshot_weapon(self._unit:unit_data().mugshot_id, icon, self.equipped_unit(self):base():weapon_tweak_data().use_data.selection_index)
	end

	return 
end
PlayerInventory.mask_visibility = function (self)
	return self._mask_visibility or false
end
PlayerInventory.set_mask_visibility = function (self, state)
	self._mask_visibility = state

	if self._unit == managers.player:player_unit() then
		return 
	end

	local character_name = managers.criminals:character_name_by_unit(self._unit)

	if not character_name then
		return 
	end

	self._mask_visibility = state

	if alive(self._mask_unit) then
		if not state then
			for _, linked_unit in ipairs(self._mask_unit:children()) do
				linked_unit.unlink(linked_unit)
				World:delete_unit(linked_unit)
			end

			self._mask_unit:unlink()

			local name = self._mask_unit:name()

			World:delete_unit(self._mask_unit)
		end

		return 
	end

	if not state then
		return 
	end

	local mask_unit_name = managers.criminals:character_data_by_name(character_name).mask_obj

	if not managers.dyn_resource:is_resource_ready(Idstring("unit"), mask_unit_name, managers.dyn_resource.DYN_RESOURCES_PACKAGE) then
		return 
	end

	mask_unit_name = mask_unit_name[Global.level_data.level_id] or mask_unit_name.default or mask_unit_name
	local mask_align = self._unit:get_object(Idstring("Head"))
	local mask_unit = World:spawn_unit(Idstring(mask_unit_name), mask_align.position(mask_align), mask_align.rotation(mask_align))

	mask_unit.base(mask_unit):apply_blueprint(managers.criminals:character_data_by_name(character_name).mask_blueprint)
	self._unit:link(mask_align.name(mask_align), mask_unit)

	self._mask_unit = mask_unit
	local mask_id = managers.criminals:character_data_by_name(character_name).mask_id
	local peer = managers.network:session():peer_by_unit(self._unit)
	local mask_data = {
		mask_id = mask_id,
		mask_unit = mask_unit,
		mask_align = mask_align,
		peer_id = peer and peer.id(peer),
		character_name = character_name
	}

	self.update_mask_offset(self, mask_data)

	if not mask_id or not tweak_data.blackmarket.masks[mask_id].type then
		local backside = World:spawn_unit(Idstring("units/payday2/masks/msk_backside/msk_backside"), mask_align.position(mask_align), mask_align.rotation(mask_align))

		self._mask_unit:link(self._mask_unit:orientation_object():name(), backside, backside.orientation_object(backside):name())
	end

	if not mask_id or not tweak_data.blackmarket.masks[mask_id].skip_mask_on_sequence then
		local mask_on_sequence = managers.blackmarket:character_mask_on_sequence_by_character_name(character_name)

		if mask_on_sequence then
			self._unit:damage():run_sequence_simple(mask_on_sequence)
		end
	end

	return 
end
PlayerInventory.update_mask_offset = function (self, mask_data)
	local char = nil

	if mask_data.peer_id then
		char = managers.blackmarket:get_real_character(nil, mask_data.peer_id)
	else
		char = managers.blackmarket:get_real_character(mask_data.character_name, nil)
	end

	local mask_tweak = tweak_data.blackmarket.masks[mask_data.mask_id]

	if mask_tweak and mask_tweak.offsets and mask_tweak.offsets[char] then
		local char_tweak = mask_tweak.offsets[char]

		self.set_mask_offset(self, mask_data.mask_unit, mask_data.mask_align, char_tweak[1] or Vector3(0, 0, 0), char_tweak[2] or Rotation(0, 0, 0))
		self.set_mask_offset(self, mask_data.mask_unit, mask_data.mask_align, char_tweak[1] or Vector3(0, 0, 0), char_tweak[2] or Rotation(0, 0, 0))
	else
		self.set_mask_offset(self, mask_data.mask_unit, mask_data.mask_align, Vector3(0, 0, 0), Rotation(0, 0, 0))
	end

	return 
end
PlayerInventory.set_mask_offset = function (self, mask_unit, mask_align, position, rotation)
	if not alive(mask_unit) then
		return 
	end

	if rotation then
		mask_unit.set_rotation(mask_unit, mask_align.rotation(mask_align)*rotation)
	end

	if position then
		mask_unit.set_position(mask_unit, mask_align.position(mask_align) + mask_unit.rotation(mask_unit):x()*position.x + mask_unit.rotation(mask_unit):z()*position.z + mask_unit.rotation(mask_unit):y()*position.y)
	end

	return 
end
PlayerInventory.set_melee_weapon = function (self, melee_weapon_id, is_npc)
	self._melee_weapon_data = managers.blackmarket:get_melee_weapon_data(melee_weapon_id)

	if is_npc then
		if self._melee_weapon_data.third_unit then
			self._melee_weapon_unit_name = Idstring(self._melee_weapon_data.third_unit)
		end
	elseif self._melee_weapon_data.unit then
		self._melee_weapon_unit_name = Idstring(self._melee_weapon_data.unit)
	end

	if self._melee_weapon_unit_name then
		managers.dyn_resource:load(Idstring("unit"), self._melee_weapon_unit_name, "packages/dyn_resources", false)
	end

	return 
end
PlayerInventory.set_melee_weapon_by_peer = function (self, peer)
	return 
end
PlayerInventory.set_ammo = function (self, ammo)
	for id, weapon in pairs(self._available_selections) do
		weapon.unit:base():set_ammo(ammo)
		managers.hud:set_ammo_amount(id, weapon.unit:base():ammo_info())
	end

	return 
end
PlayerInventory.need_ammo = function (self)
	for _, weapon in pairs(self._available_selections) do
		if not weapon.unit:base():ammo_full() then
			return true
		end
	end

	return false
end
PlayerInventory.all_out_of_ammo = function (self)
	for _, weapon in pairs(self._available_selections) do
		if not weapon.unit:base():out_of_ammo() then
			return false
		end
	end

	return true
end
PlayerInventory.anim_cbk_spawn_character_mask = function (self, unit)
	self.set_mask_visibility(self, true)

	return 
end
PlayerInventory.anim_clbk_equip_exit = function (self, unit)
	self.set_mask_visibility(self, true)

	return 
end
PlayerInventory.set_visibility_state = function (self, state)
	for i, sel_data in pairs(self._available_selections) do
		local enabled = sel_data.unit:enabled()

		sel_data.unit:base():set_visibility_state(enabled and state)
	end

	if alive(self._shield_unit) then
		self._shield_unit:set_visible(state)
	end

	return 
end
PlayerInventory.set_weapon_enabled = function (self, state)
	if self._equipped_selection then
		self.equipped_unit(self):set_enabled(state)
	end

	if alive(self._shield_unit) then
		self._shield_unit:set_enabled(state)
	end

	return 
end

return 
