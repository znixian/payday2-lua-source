PlayerCamera = PlayerCamera or class()
PlayerCamera.IDS_NOTHING = Idstring("")
PlayerCamera.init = function (self, unit)
	self._unit = unit
	self._m_cam_rot = unit.rotation(unit)
	self._m_cam_pos = unit.position(unit) + math.UP*140
	self._m_cam_fwd = self._m_cam_rot:y()
	self._camera_object = World:create_camera()

	self._camera_object:set_near_range(3)
	self._camera_object:set_far_range(250000)
	self._camera_object:set_fov(75)
	self.spawn_camera_unit(self)
	self._setup_sound_listener(self)

	self._sync_dir = {
		pitch = 0,
		yaw = unit.rotation(unit):yaw()
	}
	self._last_sync_t = 0

	self.setup_viewport(self, managers.player:viewport_config())

	return 
end
PlayerCamera.setup_viewport = function (self, data)
	if self._vp then
		self._vp:destroy()
	end

	local dimensions = data.dimensions
	local name = "player" .. tostring(self._id)
	local vp = managers.viewport:new_vp(dimensions.x, dimensions.y, dimensions.w, dimensions.h, name)
	self._director = vp.director(vp)
	self._shaker = self._director:shaker()

	self._shaker:set_timer(managers.player:player_timer())

	if not _G.IS_VR then
		self._camera_controller = self._director:make_camera(self._camera_object, Idstring("fps"))

		self._director:set_camera(self._camera_controller)
		self._director:position_as(self._camera_object)
		self._camera_controller:set_both(self._camera_unit)
		self._camera_controller:set_timer(managers.player:player_timer())
	end

	self._shakers = {}

	if not _G.IS_VR then
		self._shakers.breathing = self._shaker:play("breathing", 0.3)
	end

	self._shakers.headbob = self._shaker:play("headbob", 0)

	vp.set_camera(vp, self._camera_object)

	self._vp = vp

	if false then
		vp.set_width_mul_enabled(vp)
		vp.camera(vp):set_width_multiplier(CoreMath.width_mul(1.7777777777777777))
		self._set_dimensions(self)
	end

	return 
end
PlayerCamera._set_dimensions = function (self)
	self._vp._vp:set_dimensions(0, (RenderSettings.aspect_ratio/1.7777777777777777 - 1)/2, 1, RenderSettings.aspect_ratio/1.7777777777777777)

	return 
end
PlayerCamera.spawn_camera_unit = function (self)
	local lvl_tweak_data = Global.level_data and Global.level_data.level_id and tweak_data.levels[Global.level_data.level_id]
	local unit_folder = "suit"
	self._camera_unit = World:spawn_unit(Idstring("units/payday2/characters/fps_criminals_suit_1/fps_criminals_suit_1"), self._m_cam_pos, self._m_cam_rot)
	self._machine = self._camera_unit:anim_state_machine()

	self._unit:link(self._camera_unit)
	self._camera_unit:base():set_parent_unit(self._unit)
	self._camera_unit:base():reset_properties()
	self._camera_unit:base():set_stance_instant("standard")

	if _G.IS_VR then
		self._camera_unit:set_visible(false)
	end

	return 
end
PlayerCamera.camera_unit = function (self)
	return self._camera_unit
end
PlayerCamera.anim_state_machine = function (self)
	return self._camera_unit:anim_state_machine()
end
PlayerCamera.play_redirect = function (self, redirect_name, speed, offset_time)
	local result = self._camera_unit:base():play_redirect(redirect_name, speed, offset_time)

	return result ~= PlayerCamera.IDS_NOTHING and result
end
PlayerCamera.play_redirect_timeblend = function (self, state, redirect_name, offset_time, t)
	local result = self._camera_unit:base():play_redirect_timeblend(state, redirect_name, offset_time, t)

	return result ~= PlayerCamera.IDS_NOTHING and result
end
PlayerCamera.play_state = function (self, state_name, at_time)
	local result = self._camera_unit:base():play_state(state_name, at_time)

	return result ~= PlayerCamera.IDS_NOTHING and result
end
PlayerCamera.play_raw = function (self, name, params)
	local result = self._camera_unit:base():play_raw(name, params)

	return result ~= PlayerCamera.IDS_NOTHING and result
end
PlayerCamera.set_speed = function (self, state_name, speed)
	self._machine:set_speed(state_name, speed)

	return 
end
PlayerCamera.anim_data = function (self)
	return self._camera_unit:anim_data()
end
PlayerCamera.destroy = function (self)
	Application:stack_dump()
	self._vp:destroy()

	self._unit = nil

	if alive(self._camera_object) then
		World:delete_camera(self._camera_object)
	end

	self._camera_object = nil

	self.remove_sound_listener(self)

	return 
end
PlayerCamera.remove_sound_listener = function (self)
	if not self._listener_id then
		return 
	end

	managers.sound_environment:remove_check_object(self._sound_check_object)
	managers.listener:remove_listener(self._listener_id)
	managers.listener:remove_set("player_camera")

	self._listener_id = nil

	return 
end
PlayerCamera.clbk_fp_enter = function (self, aim_dir)
	if self._camera_manager_mode ~= "first_person" then
		self._camera_manager_mode = "first_person"
	end

	return 
end
PlayerCamera._setup_sound_listener = function (self)
	self._listener_id = managers.listener:add_listener("player_camera", self._camera_object, self._camera_object, nil, false)

	managers.listener:add_set("player_camera", {
		"player_camera"
	})

	self._listener_activation_id = managers.listener:activate_set("main", "player_camera")
	self._sound_check_object = managers.sound_environment:add_check_object({
		primary = true,
		active = true,
		object = self._unit:orientation_object()
	})

	return 
end
PlayerCamera.set_default_listener_object = function (self)
	self.set_listener_object(self, self._camera_object)

	return 
end
PlayerCamera.set_listener_object = function (self, object)
	managers.listener:set_listener(self._listener_id, object, object, nil)

	return 
end
PlayerCamera.position = function (self)
	return self._m_cam_pos
end
PlayerCamera.rotation = function (self)
	return self._m_cam_rot
end
PlayerCamera.forward = function (self)
	return self._m_cam_fwd
end
local camera_mvec = Vector3()
local reticle_mvec = Vector3()
PlayerCamera.position_with_shake = function (self)
	self._camera_object:m_position(camera_mvec)

	return camera_mvec
end
PlayerCamera.forward_with_shake_toward_reticle = function (self, reticle_obj)
	reticle_obj.m_position(reticle_obj, reticle_mvec)
	self._camera_object:m_position(camera_mvec)
	mvector3.subtract(reticle_mvec, camera_mvec)
	mvector3.normalize(reticle_mvec)

	return reticle_mvec
end
PlayerCamera.set_position = function (self, pos)
	if _G.IS_VR then
		self._camera_object:set_position(pos)
		mvector3.set(self._m_cam_pos, pos)

		return 
	end

	self._camera_controller:set_camera(pos)
	mvector3.set(self._m_cam_pos, pos)

	return 
end
PlayerCamera.update_transform = function (self)
	self._camera_object:transform()

	return 
end
local mvec1 = Vector3()
PlayerCamera.set_rotation = function (self, rot)
	if _G.IS_VR then
		self._camera_object:set_rotation(rot)
	end

	mrotation.y(rot, mvec1)
	mvector3.multiply(mvec1, 100000)
	mvector3.add(mvec1, self._m_cam_pos)

	if not _G.IS_VR then
		self._camera_controller:set_target(mvec1)
	end

	mrotation.z(rot, mvec1)

	if not _G.IS_VR then
		self._camera_controller:set_default_up(mvec1)
	end

	mrotation.set_yaw_pitch_roll(self._m_cam_rot, rot.yaw(rot), rot.pitch(rot), rot.roll(rot))
	mrotation.y(self._m_cam_rot, self._m_cam_fwd)

	local t = TimerManager:game():time()
	local sync_dt = t - self._last_sync_t
	local sync_yaw = rot.yaw(rot)
	sync_yaw = sync_yaw%360

	if sync_yaw < 0 then
		sync_yaw = sync_yaw - 360
	end

	sync_yaw = math.floor((sync_yaw*255)/360)
	local sync_pitch = math.clamp(rot.pitch(rot), -85, 85) + 85
	sync_pitch = math.floor((sync_pitch*127)/170)
	local angle_delta = math.abs(self._sync_dir.yaw - sync_yaw) + math.abs(self._sync_dir.pitch - sync_pitch)

	if tweak_data.network then
		local update_network = (tweak_data.network.camera.network_sync_delta_t < sync_dt and 0 < angle_delta) or tweak_data.network.camera.network_angle_delta < angle_delta

		if self._forced_next_sync_t and t < self._forced_next_sync_t then
			update_network = false
		end

		if update_network then
			self._unit:network():send("set_look_dir", sync_yaw, sync_pitch)

			self._sync_dir.yaw = sync_yaw
			self._sync_dir.pitch = sync_pitch
			self._last_sync_t = t
		end
	end

	return 
end
PlayerCamera.set_forced_sync_delay = function (self, t)
	self._forced_next_sync_t = t

	return 
end
PlayerCamera.set_FOV = function (self, fov_value)
	self._camera_object:set_fov(fov_value)

	return 
end
PlayerCamera.viewport = function (self)
	return self._vp
end
PlayerCamera.set_shaker_parameter = function (self, effect, parameter, value)
	if not self._shakers then
		return 
	end

	if self._shakers[effect] then
		self._shaker:set_parameter(self._shakers[effect], parameter, value)
	end

	return 
end
PlayerCamera.play_shaker = function (self, effect, amplitude, frequency, offset)
	if _G.IS_VR then
		return 
	end

	return self._shaker:play(effect, amplitude or 1, frequency or 1, offset or 0)
end
PlayerCamera.stop_shaker = function (self, id)
	self._shaker:stop_immediately(id)

	return 
end
PlayerCamera.shaker = function (self)
	return self._shaker
end

return 
