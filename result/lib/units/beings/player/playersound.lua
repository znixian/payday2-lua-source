PlayerSound = PlayerSound or class()
PlayerSound.init = function (self, unit)
	self._unit = unit

	unit.base(unit):post_init()

	local ss = unit.sound_source(unit)

	ss.set_switch(ss, "robber", "rb3")

	if unit.base(unit).is_local_player then
		ss.set_switch(ss, "int_ext", "first")
	else
		ss.set_switch(ss, "int_ext", "third")
	end

	return 
end
PlayerSound.destroy = function (self, unit)
	if unit.base(unit) then
		unit.base(unit):pre_destroy(unit)
	end

	return 
end
PlayerSound._play = function (self, sound_name, source_name)
	local source = nil

	if source_name then
		source = Idstring(source_name)
	end

	local event = self._unit:sound_source(source):post_event(sound_name, self.sound_callback, self._unit, "marker", "end_of_event")

	return event
end
PlayerSound.sound_callback = function (self, instance, event_type, unit, sound_source, label, identifier, position)
	if not alive(unit) then
		return 
	end

	if event_type == "end_of_event" then
		managers.hud:set_mugshot_talk(unit.unit_data(unit).mugshot_id, false)

		unit.sound(unit)._speaking = nil
	end

	return 
end
PlayerSound.play = function (self, sound_name, source_name, sync)
	local event_id = nil

	if type(sound_name) == "number" then
		event_id = sound_name
		sound_name = nil
	end

	if sync then
		event_id = event_id or SoundDevice:string_to_id(sound_name)
		source_name = source_name or ""

		self._unit:network():send("unit_sound_play", event_id, source_name)
	end

	local event = self._play(self, sound_name or event_id, source_name)

	return event
end
PlayerSound.stop = function (self, source_name)
	local source = nil

	if source_name then
		source = Idstring(source_name)
	end

	self._unit:sound_source(source):stop()

	return 
end
PlayerSound.play_footstep = function (self, foot, material_name)
	if self._last_material ~= material_name then
		self._last_material = material_name
		local material_name = tweak_data.materials[material_name.key(material_name)]

		self._unit:sound_source(Idstring("root")):set_switch("materials", material_name or "no_material")
	end

	self._play(self, (self._unit:movement():running() and "footstep_run") or "footstep_walk")

	return 
end
PlayerSound.play_land = function (self, material_name)
	if self._last_material ~= material_name then
		self._last_material = material_name
		local material_name = tweak_data.materials[material_name.key(material_name)]

		self._unit:sound_source(Idstring("root")):set_switch("materials", material_name or "concrete")
	end

	self._play(self, "footstep_land")

	return 
end
PlayerSound.play_whizby = function (self, params)
	self._play(self, "bullet_whizby_medium")

	return 
end
PlayerSound.say = function (self, sound_name, sync, important_say, ignore_prefix, callback)
	if self._last_speech and self._speaking then
		self._last_speech:stop()

		self._speaking = nil
	end

	local event_id = nil

	if type(sound_name) == "number" then
		event_id = sound_name
		sound_name = nil
	end

	if sync then
		event_id = event_id or SoundDevice:string_to_id(sound_name)

		self._unit:network():send("say", event_id)
	end

	self._last_speech = self._play(self, sound_name or event_id, nil, true, callback)

	if important_say and self._last_speech then
		managers.hud:set_mugshot_talk(self._unit:unit_data().mugshot_id, true)

		self._speaking = true
	end

	return self._last_speech
end
PlayerSound.speaking = function (self)
	return self._speaking
end
PlayerSound.set_voice = function (self, voice)
	self._unit:sound_source():set_switch("robber", voice)

	return 
end

return 
