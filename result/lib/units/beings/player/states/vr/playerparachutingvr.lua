PlayerParachutingVR = PlayerParachuting or Application:error("PlayerParachutingVR needs PlayerParachuting!")
local __enter = PlayerParachuting.enter
PlayerParachutingVR.enter = function (self, ...)
	__enter(self, ...)
	self._camera_unit:base():set_hmd_tracking(false)
	managers.menu:open_menu("zipline")

	return 
end
local __exit = PlayerParachuting.exit
PlayerParachutingVR.exit = function (self, ...)
	__exit(self, ...)
	managers.menu:close_menu("zipline")
	self._camera_unit:base():set_hmd_tracking(true)

	return 
end
PlayerParachutingVR._update_variables = function (self, t, dt)
	self._current_height = self._ext_movement:hmd_position().z

	return 
end
local __update_movement = PlayerParachuting._update_movement
PlayerParachutingVR._update_movement = function (self, t, dt)
	__update_movement(self, t, dt)
	self._unit:movement():set_ghost_position(self._unit:position())

	return 
end

return 
