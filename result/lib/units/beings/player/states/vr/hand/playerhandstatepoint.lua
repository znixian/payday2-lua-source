require("lib/units/beings/player/states/vr/hand/PlayerHandState")

PlayerHandStatePoint = PlayerHandStatePoint or class(PlayerHandState)
PlayerHandStatePoint.init = function (self, hsm, name, hand_unit, sequence)
	PlayerHandStatePoint.super.init(self, name, hsm, hand_unit, sequence)

	return 
end
PlayerHandStatePoint.at_enter = function (self, prev_state)
	PlayerHandStatePoint.super.at_enter(self, prev_state)

	self._timer_t = nil
	self._can_transition = false

	self.hsm(self):enter_controller_state("point")

	return 
end
PlayerHandStatePoint.update = function (self, t, dt)
	if not self._timer_t then
		self._timer_t = t
	end

	if 1.1 < t - self._timer_t and not self._can_transition then
		self._can_transition = true

		self._hsm:change_to_default(nil, true)
	end

	return 
end
PlayerHandStatePoint.default_transition = function (self, next_state)
	if self._can_transition then
		PlayerHandStatePoint.super.default_transition(self, next_state)
	end

	return 
end

return 
