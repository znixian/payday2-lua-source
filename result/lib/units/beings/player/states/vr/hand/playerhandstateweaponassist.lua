require("lib/units/beings/player/states/vr/hand/PlayerHandState")

PlayerHandStateWeaponAssist = PlayerHandStateWeaponAssist or class(PlayerHandState)
PlayerHandStateWeaponAssist.init = function (self, hsm, name, hand_unit, sequence)
	PlayerHandStateWeaponAssist.super.init(self, name, hsm, hand_unit, sequence)

	return 
end
PlayerHandStateWeaponAssist.at_enter = function (self, prev_state)
	local weapon_unit = self.hsm(self):other_hand():current_state()._weapon_unit
	local weapon_tweak = alive(weapon_unit) and tweak_data.vr.weapon_assist.weapons[weapon_unit.base(weapon_unit).name_id]
	local sequence = self._sequence
	local other_hand = self.hsm(self):other_hand():current_state()
	self._assist_position = other_hand.assist_position(other_hand)

	if other_hand.assist_grip(other_hand) then
		sequence = other_hand.assist_grip(other_hand)
	end

	if self._hand_unit and sequence and self._hand_unit:damage():has_sequence(sequence) then
		self._hand_unit:damage():run_sequence_simple(sequence)
	end

	self.hsm(self):enter_controller_state("empty")

	managers.player:player_unit():movement():current_state()._start_intimidate = nil

	return 
end
PlayerHandStateWeaponAssist.update = function (self, t, dt)
	local weapon_unit = self.hsm(self):other_hand():current_state()._weapon_unit

	if alive(weapon_unit) and self._assist_position then
		self._hand_unit:set_position(weapon_unit.position(weapon_unit) + self._assist_position:rotate_with(weapon_unit.rotation(weapon_unit)))
	end

	return 
end

return 
