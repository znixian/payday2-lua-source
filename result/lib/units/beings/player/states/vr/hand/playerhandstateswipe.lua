require("lib/units/beings/player/states/vr/hand/PlayerHandState")

PlayerHandStateSwipe = PlayerHandStateSwipe or class(PlayerHandState)
PlayerHandStateSwipe.init = function (self, hsm, name, hand_unit, sequence)
	PlayerHandStateWeapon.super.init(self, name, hsm, hand_unit, sequence)

	return 
end
PlayerHandStateSwipe.at_enter = function (self, prev_state, params)
	PlayerHandStateSwipe.super.at_enter(self, prev_state)

	self._params = params
	self.prev_state = prev_state.name(prev_state)
	self._flick_callback = params.flick_callback

	self.hsm(self):enter_controller_state("tablet")

	return 
end
PlayerHandStateSwipe.at_exit = function (self, next_state)
	PlayerHandStateSwipe.super.at_exit(self, next_state)

	self._flick_callback = nil

	return 
end
local tmp_vec = Vector3(0, 0, 0)
PlayerHandStateSwipe.update = function (self, t, dt)
	mvector3.set(tmp_vec, Vector3(4, 12, 3.5))
	mvector3.rotate_with(tmp_vec, self._hand_unit:rotation())
	mvector3.add(tmp_vec, self._hand_unit:position())

	local tablet_hand = self._hsm:other_hand():hand_unit()
	local tablet_oobb = tablet_hand.get_object(tablet_hand, Idstring("g_tablet")):oobb()

	tablet_oobb.grow(tablet_oobb, 2)

	if tablet_oobb.point_inside(tablet_oobb, tmp_vec) then
		self._start_swipe = self._start_swipe or mvector3.copy(tmp_vec)
		self._start_t = self._start_t or t
		self._current_swipe = tmp_vec
	elseif self._start_swipe then
		self._start_swipe = nil
		self._start_t = nil
		self._last_flick = nil
	end

	if self._start_swipe and self._current_swipe and self._start_t and self._start_t < t then
		self._check_flick(self, self._start_swipe, self._current_swipe, t - self._start_t)
	end

	return 
end
local dir_vec = Vector3(0, 0, 0)
PlayerHandStateSwipe._check_flick = function (self, start, current, dt)
	if 100 < mvector3.distance_sq(start, current)/dt then
		mvector3.set(dir_vec, current)
		mvector3.subtract(dir_vec, start)

		local inv_rot = self._hand_unit:rotation():inverse()

		mvector3.rotate_with(dir_vec, inv_rot)

		local vel = math.max(mvector3.normalize(dir_vec)/dt/25, 3)
		local dir_string = nil
		local axis = (dir_vec.z < dir_vec.x and "x") or "z"

		if 0 < dir_vec[axis] then
			if axis == "x" then
				dir_string = "left"
			else
				dir_string = "down"
			end
		elseif axis == "x" then
			dir_string = "up"
		else
			dir_string = "right"
		end

		if dir_string == self._last_flick then
			return 
		end

		if self._flick_callback then
			self._flick_callback(dir_string, vel/1)
		end

		self._last_flick = dir_string
	end

	return 
end
PlayerHandStateSwipe.item_transition = function (self, next_state, params)
	params = self._params

	self.default_transition(self, next_state, params)

	return 
end

return 
