require("lib/units/beings/player/states/vr/hand/PlayerHandState")

PlayerHandStateMelee = PlayerHandStateMelee or class(PlayerHandState)
PlayerHandStateMelee.init = function (self, hsm, name, hand_unit, sequence)
	PlayerHandStateWeapon.super.init(self, name, hsm, hand_unit, sequence)

	return 
end
PlayerHandStateMelee._spawn_melee_unit = function (self)
	local melee_entry = managers.blackmarket:equipped_melee_weapon()
	self._melee_entry = melee_entry
	local unit_name = tweak_data.blackmarket.melee_weapons[melee_entry].third_unit

	if unit_name then
		local aligns = tweak_data.blackmarket.melee_weapons[melee_entry].align_objects or {
			"a_weapon_left"
		}
		local graphic_objects = tweak_data.blackmarket.melee_weapons[melee_entry].graphic_objects or {}
		local align = nil

		if 1 < #aligns then
			if self._hsm:hand_id() == 1 then
				align = "a_weapon_left"
			else
				align = "a_weapon_right"
			end

			if not table.contains(aligns, align) then
				Application:error("[PlayerHandStateMelee:_spawn_melee_unit] can't spawn melee weapon in this hand", melee_entry, self._hand_unit)

				return 
			end
		else
			align = aligns[1]
		end

		local align_obj = self._hand_unit:get_object(Idstring("g_glove"))
		self._melee_unit = World:spawn_unit(Idstring(unit_name), align_obj.position(align_obj), align_obj.rotation(align_obj))

		self._hand_unit:link(align_obj.name(align_obj), self._melee_unit, self._melee_unit:orientation_object():name())

		local offset = tweak_data.vr:get_offset_by_id(melee_entry)

		if offset then
			self._melee_unit:set_local_position(offset.position or Vector3())
			self._melee_unit:set_local_rotation(offset.rotation or Rotation())

			local sequence = self._sequence

			if offset.grip then
				sequence = offset.grip
			end

			if self._hand_unit and sequence and self._hand_unit:damage():has_sequence(sequence) then
				self._hand_unit:damage():run_sequence_simple(sequence)
			end
		end

		local align_obj_name = Idstring(align)

		for a_object, g_object in pairs(graphic_objects) do
			self._melee_unit:get_object(Idstring(g_object)):set_visibility(Idstring(a_object) == align_obj_name)
		end

		if alive(self._melee_unit) and self._melee_unit:damage() and self._melee_unit:damage():has_sequence("game") then
			self._melee_unit:damage():run_sequence_simple("game")
		end
	end

	return 
end
PlayerHandStateMelee.at_enter = function (self, prev_state, params)
	PlayerHandStateWeapon.super.at_enter(self, prev_state)
	managers.player:player_unit():movement():current_state():_interupt_action_reload()
	self._spawn_melee_unit(self)
	self._hand_unit:melee():set_melee_unit(self._melee_unit)

	if self._melee_entry == "fists" or self._melee_entry == "fight" then
		self._hand_unit:melee():set_fist(self._melee_entry)
		self._hand_unit:damage():run_sequence_simple("grip")
	end

	self.hsm(self):enter_controller_state("item")
	managers.hud:belt():set_state("melee", "active")

	return 
end
PlayerHandStateMelee.at_exit = function (self, next_state)
	PlayerHandStateMelee.super.at_exit(self, next_state)
	self.hsm(self):exit_controller_state("item")
	self._hand_unit:melee():set_melee_unit()

	if alive(self._melee_unit) then
		self._melee_unit:unlink()
		World:delete_unit(self._melee_unit)
	end

	return 
end
PlayerHandStateMelee.update = function (self, t, dt)
	local controller = managers.vr:hand_state_machine():controller()

	if controller.get_input_pressed(controller, "unequip") then
		managers.hud:belt():set_state("melee", "default")
		self.hsm(self):change_to_default()
	end

	if controller.get_input_pressed(controller, "use_item_vr") then
		self._hand_unit:melee():set_charge_start_t(t)
	elseif controller.get_input_released(controller, "use_item_vr") then
		self._hand_unit:melee():set_charge_start_t(nil)
	elseif controller.get_input_bool(controller, "use_item_vr") and not self._hand_unit:melee():charge_start_t() then
		self._hand_unit:melee():set_charge_start_t(t)
	end

	if self._hand_unit:melee():charge_start_t() then
		local charge_value = managers.player:player_unit():movement():current_state():_get_melee_charge_lerp_value(t)

		managers.controller:get_vr_controller():trigger_haptic_pulse(self.hsm(self):hand_id() - 1, 0, charge_value*1000 + 500)
	end

	return 
end

return 
