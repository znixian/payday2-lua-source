require("lib/units/beings/player/states/vr/hand/PlayerHandState")

PlayerHandStateBelt = PlayerHandStateBelt or class(PlayerHandState)
PlayerHandStateBelt.init = function (self, hsm, name, hand_unit, sequence)
	PlayerHandStateBelt.super.init(self, name, hsm, hand_unit, sequence)

	return 
end
PlayerHandStateBelt.at_enter = function (self, prev_state)
	PlayerHandStateBelt.super.at_enter(self, prev_state)
	self._hsm:enter_controller_state("belt")

	self._belt_button = (self.hsm(self):hand_id() == 1 and "belt_right") or "belt_left"

	return 
end
PlayerHandStateBelt.at_exit = function (self, next_state)
	PlayerHandStateBelt.super.at_exit(self, next_state)
	self._hsm:exit_controller_state("belt")

	if self._belt_state then
		managers.hud:belt():set_selected(self._belt_state, false)
	end

	return 
end
PlayerHandStateBelt.update = function (self, t, dt)
	local belt_state = nil

	for _, state in ipairs(managers.hud:belt():valid_interactions()) do
		if mvector3.distance_sq(self._hand_unit:position(), managers.hud:belt():get_interaction_point(state)) < 100 then
			belt_state = state
		end
	end

	if belt_state ~= self._belt_state then
		if self._belt_state then
			managers.hud:belt():set_selected(self._belt_state, false)
		end

		if belt_state then
			managers.hud:belt():set_selected(belt_state, true)
		end

		if self._belt_state == "throwable" and managers.blackmarket:has_equipped_ability() then
			self._hsm:exit_controller_state("ability")
		elseif belt_state == "throwable" and managers.blackmarket:has_equipped_ability() then
			self._hsm:enter_controller_state("ability")
		end

		self._belt_state = belt_state
	end

	if self._belt_state then
		local player = managers.player:player_unit()

		if self._belt_state == "reload" and self._hsm:default_state_name() == "weapon" and player.movement(player):current_state():can_trigger_reload() then
			player.movement(player):current_state():trigger_reload()
			managers.hud:belt():trigger_reload()
			self.hsm(self):change_to_default()
		end

		if managers.vr:hand_state_machine():controller():get_input_pressed(self._belt_button) then
			if self._belt_state == "weapon" then
				local inv = player.inventory(player)
				local _, wanted_selection = player.inventory(player):get_next_selection()
				local hsm = self.hsm(self)

				player.movement(player):current_state():swap_weapon(self.hsm(self):hand_id(), wanted_selection, function ()
					managers.hud:belt():set_state("weapon", "default")

					return 
				end)
				player.sound(player):play("m4_equip")
				player.inventory(player):equip_selection(wanted_selection, true)
				hsm.set_default_state(hsm, "weapon")
				hsm.other_hand(hsm):set_default_state("idle")
				managers.hud:belt():set_state("weapon", "inactive")
			elseif self._belt_state == "bag" then
				local carry_id = managers.player:get_my_carry_data().carry_id
				local unit_name = tweak_data.carry[carry_id].unit

				if unit_name then
					unit_name = string.match(unit_name, "/([^/]*)$")
					unit_name = "units/pd2_dlc_vr/equipment/" .. unit_name .. "_vr"
				else
					unit_name = "units/pd2_dlc_vr/equipment/gen_pku_lootbag_vr"
				end

				local unit = World:spawn_unit(Idstring(unit_name), self._hand_unit:position(), self._hand_unit:rotation()*Rotation(0, 0, -90))

				self._hsm:change_state_by_name("item", {
					body = "hinge_body_1",
					type = "bag",
					unit = unit,
					offset = Vector3(0, 15, 0),
					prev_state = self._prev_state,
					prompt = {
						text_id = "hud_instruct_throw_bag",
						btn_macros = {
							BTN_USE_ITEM = "use_item"
						}
					}
				})
			elseif self._belt_state == "deployable" or self._belt_state == "deployable_secondary" then
				self._hsm:change_state_by_name("item", {
					type = "deployable",
					prev_state = self._prev_state,
					secondary = self._belt_state == "deployable_secondary"
				})
			elseif self._belt_state == "throwable" then
				local grenade_entry = managers.blackmarket:equipped_grenade()
				local grenade_unit = World:spawn_unit(Idstring(tweak_data.blackmarket.projectiles[grenade_entry].unit_dummy), Vector3(0, 0, 0), Rotation())

				self._hsm:change_state_by_name("item", {
					type = "throwable",
					unit = grenade_unit,
					prev_state = self._prev_state
				})
			elseif self._belt_state == "melee" then
				self._hsm:change_state_by_name("melee", {
					prev_state = self._prev_state
				})
			elseif self._belt_state == "reload" and self._hsm:default_state_name() ~= "weapon" then
				local weap_base = player.inventory(player):equipped_unit():base()
				local mag_name = weap_base.magazine_unit_name(weap_base)

				if mag_name then
					local mag_unit = World:spawn_unit(Idstring(mag_name), Vector3(0, 0, 0), Rotation())
					local second_mag = nil

					if weap_base.akimbo then
						second_mag = World:spawn_unit(Idstring(mag_name), Vector3(-5, 0, 0), Rotation())

						mag_unit.link(mag_unit, mag_unit.orientation_object(mag_unit):name(), second_mag)
					end

					if weap_base.reload_object_name(weap_base) then
						for _, mag in ipairs({
							mag_unit,
							second_mag
						}) do
							local reload_obj = mag.get_object(mag, Idstring(weap_base.reload_object_name(weap_base)))

							reload_obj.set_position(reload_obj, mag.position(mag))
							reload_obj.set_visibility(reload_obj, true)
						end
					end

					self._hsm:change_state_by_name("item", {
						type = "magazine",
						unit = mag_unit,
						prev_state = self._prev_state
					})
					managers.hud:belt():trigger_reload()
					player.movement(player):current_state():grab_mag()
				end
			end
		end
	end

	return 
end

return 
