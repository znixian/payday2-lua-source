require("lib/units/beings/player/states/vr/hand/PlayerHandState")

PlayerHandStateItem = PlayerHandStateItem or class(PlayerHandState)
PlayerHandStateItem.init = function (self, hsm, name, hand_unit, sequence)
	PlayerHandStateItem.super.init(self, name, hsm, hand_unit, sequence)

	return 
end
PlayerHandStateItem._link_item = function (self, item_unit, body, offset)
	self._item_unit = item_unit

	if body then
		self._hand_unit:link(Idstring("g_glove"), item_unit)

		local body_obj = item_unit.body(item_unit, body) or item_unit.body(item_unit, 0)

		body_obj.set_keyframed(body_obj)

		self._body = body_obj

		self.set_bodies_colliding(self, false)
	else
		self._hand_unit:link(Idstring("g_glove"), item_unit, item_unit.orientation_object(item_unit):name())
	end

	if offset then
		self._item_unit:set_local_position(offset)
	end

	self._item_unit:set_visible(true)

	return 
end
PlayerHandStateItem._prompt = function (self, prompt)
	self._prompt_data = prompt

	if prompt.btn_macros then
		prompt.macros = prompt.macros or {}

		for key, macro in pairs(prompt.btn_macros) do
			prompt.macros[key] = managers.localization:btn_macro(macro)
		end
	end

	local text = nil

	if prompt.text then
		text = prompt.text
	elseif prompt.text_id then
		text = managers.localization:to_upper_text(prompt.text_id, prompt.macros)
	end

	local offset = nil
	local hand_id = self.hsm(self):hand_id()

	if self._item_type == "mask" and alive(self._item_unit) then
		offset = Vector3(self._item_unit:oobb():size().x/2*((hand_id == 1 and -1) or 1))
	end

	managers.hud:link_watch_prompt_as_hand(self._hand_unit, hand_id, offset)
	managers.hud:show_interact({
		text = text
	})
	managers.hud:watch_prompt_panel():show()

	return 
end
PlayerHandStateItem.item_type = function (self)
	return self._item_type
end
PlayerHandStateItem.item_unit = function (self)
	return self._item_unit
end
PlayerHandStateItem.switch_hands = function (self)
	self._switching_hands = true

	self.hsm(self):other_hand():change_state_by_name("item", {
		unit = self._item_unit,
		type = self._item_type,
		prompt = self._prompt_data
	})
	self.hsm(self):change_to_default()

	return 
end
PlayerHandStateItem.at_enter = function (self, prev_state, params)
	PlayerHandStateItem.super.at_enter(self, prev_state, params)

	if not params then
		debug_pause("[PlayerHandStateItem:at_enter] Entered item state without params!")
	end

	if params.type ~= "magazine" then
		managers.player:player_unit():movement():current_state():_interupt_action_reload()
	end

	if alive(params.unit) then
		self._link_item(self, params.unit, params.body, params.offset)
	end

	self._item_type = params.type
	self._controller_state = "item"

	if self._item_type == "mask" then
		for _, linked_unit in ipairs(self._item_unit:children()) do
			linked_unit.set_visible(linked_unit, true)
		end

		self._item_unit:set_visible(true)

		local offset = tweak_data.vr:get_offset_by_id(managers.blackmarket:equipped_mask().mask_id)

		if offset then
			self._item_unit:set_local_rotation(offset.rotation or Rotation())
			self._item_unit:set_local_position(offset.position or Vector3())
		end

		self._hand_unit:set_visible(false)

		self._controller_state = "mask"
	elseif self._item_type == "deployable" then
		self._controller_state = "equipment"

		self._hand_unit:damage():run_sequence_simple("ready")

		self._secondary_deployable = params.secondary
	elseif self._item_type == "throwable" then
		local offset = tweak_data.vr:get_offset_by_id(managers.blackmarket:equipped_grenade())

		if offset then
			self._item_unit:set_local_rotation(offset.rotation or Rotation())
			self._item_unit:set_local_position(offset.position or Vector3())

			local sequence = self._sequence

			if offset.grip then
				sequence = offset.grip
			end

			if self._hand_unit and sequence and self._hand_unit:damage():has_sequence(sequence) then
				self._hand_unit:damage():run_sequence_simple(sequence)
			end
		end
	end

	self.hsm(self):enter_controller_state(self._controller_state)

	if params.prompt then
		self._prompt(self, params.prompt)
	end

	if self._item_type == "bag" or self._item_type == "deployable" or self._item_type == "throwable" then
		managers.hud:belt():set_state((self._secondary_deployable and "deployable_secondary") or self._item_type, "active")
	end

	if self._item_type == "deployable" then
		managers.hud:link_watch_prompt_as_hand(self._hand_unit, self.hsm(self):hand_id())
	end

	return 
end
PlayerHandStateItem.at_exit = function (self, next_state, hide_item)
	self.hsm(self):exit_controller_state(self._controller_state)

	if self._item_type == "mask" then
		self._hand_unit:set_visible(true)
	end

	if not self._switching_hands then
		managers.hud:watch_prompt_panel():hide()

		if hide_item or self._item_type == "mask" then
			self._hide_unit(self)
		else
			self._remove_unit(self)
		end
	end

	PlayerHandStateItem.super.at_exit(self, next_state)

	self._switching_hands = false

	return 
end
PlayerHandStateItem._remove_unit = function (self)
	if alive(self._item_unit) then
		for _, linked_unit in ipairs(self._item_unit:children()) do
			linked_unit.unlink(linked_unit)
			World:delete_unit(linked_unit)
		end

		self._item_unit:unlink()
		World:delete_unit(self._item_unit)
	end

	if self._item_type == "deployable" then
		managers.player:player_unit():equipment():on_deploy_interupted()
	end

	return 
end
PlayerHandStateItem._hide_unit = function (self)
	if alive(self._item_unit) then
		for _, linked_unit in ipairs(self._item_unit:children()) do
			linked_unit.set_visible(linked_unit, false)
		end

		self._item_unit:set_visible(false)
	end

	if self._item_type == "deployable" then
		managers.player:player_unit():equipment():on_deploy_interupted()
	end

	return 
end
PlayerHandStateItem.set_warping = function (self, warping)
	if not warping then
		self._wants_dynamic = true

		return 
	end

	if self._body then
		self.set_bodies_dynamic(self, not warping, self._body)
	end

	return 
end
PlayerHandStateItem.set_bodies_dynamic = function (self, dynamic, ignore_body)
	if alive(self._item_unit) then
		for i = 0, self._item_unit:num_bodies() - 1, 1 do
			local body = self._item_unit:body(i)

			if not ignore_body or body ~= ignore_body then
				if dynamic then
					body.set_dynamic(body)
				else
					body.set_keyframed(body)
				end
			end
		end
	end

	return 
end
PlayerHandStateItem.set_bodies_colliding = function (self, colliding)
	if alive(self._item_unit) then
		for i = 0, self._item_unit:num_bodies() - 1, 1 do
			self._item_unit:body(i):set_collisions_enabled(colliding)
		end
	end

	return 
end
PlayerHandStateItem.update = function (self, t, dt)
	if self._wants_dynamic then
		self._dynamic_t = t + 0.05
		self._wants_dynamic = false
	end

	if self._dynamic_t and self._dynamic_t < t then
		self.set_bodies_dynamic(self, true, self._body)

		self._dynamic_t = nil
	end

	local controller = managers.vr:hand_state_machine():controller()

	if controller.get_input_pressed(controller, "use_item_vr") and self._item_type == "throwable" and not managers.player:player_unit():hand():check_hand_through_wall(self.hsm(self):hand_id()) then
		self._remove_unit(self)
		managers.player:player_unit():equipment():throw_projectile(self._hand_unit)
		managers.player:player_unit():movement():current_state():set_throwing_projectile(self.hsm(self):hand_id())
		self.hsm(self):change_to_default()
	end

	if self._item_type ~= "mask" and self._item_type ~= "magazine" and (controller.get_input_pressed(controller, "unequip") or controller.get_input_released(controller, "use_item")) then
		if not self._item_type == "bag" or controller.get_input_pressed(controller, "unequip") then
			managers.hud:belt():set_state((self._secondary_deployable and "deployable_secondary") or self._item_type, "default")
		end

		self.hsm(self):change_to_default()
	end

	if self._item_type == "deployable" then
		local equipment_id = managers.player:equipment_in_slot((self._secondary_deployable and 2) or 1)

		if managers.player:selected_equipment_id() ~= equipment_id then
			managers.player:switch_equipment()
		end

		local valid = managers.player:check_equipment_placement_valid(managers.player:player_unit(), equipment_id)

		if not valid then
			self._hand_unit:damage():run_sequence_simple("ready_warning")
		else
			self._hand_unit:damage():run_sequence_simple("ready")
		end
	elseif self._item_type == "throwable" then
		self._brush = self._brush or Draw:brush(Color(0.15, 0, 0.4, 0))

		self._brush:cylinder(self._hand_unit:position(), self._hand_unit:position() + self._hand_unit:rotation():y()*50, 2)
	elseif self._item_type == "magazine" then
		local player = managers.player:player_unit()
		local weapon = player.inventory(player):equipped_unit()

		if mvector3.distance_sq(self._hand_unit:position(), weapon.position(weapon)) < 400 then
			player.movement(player):current_state():trigger_reload()
			self.hsm(self):change_to_default()
		end
	end

	return 
end
PlayerHandStateItem.swipe_transition = function (self, next_state, params)
	params.unit = alive(self._item_unit) and self._item_unit
	params.type = self._item_type
	params.prompt = self._prompt_data

	self.at_exit(self, next_state, true)
	next_state.at_enter(next_state, self, params)

	return 
end

return 
