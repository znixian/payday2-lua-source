PlayerHandState = PlayerHandState or class()
PlayerHandState.init = function (self, name, hand_state_machine, hand_unit, sequence)
	self._name = name
	self._hsm = hand_state_machine
	self._hand_unit = hand_unit
	self._sequence = sequence

	return 
end
PlayerHandState.destroy = function (self)
	return 
end
PlayerHandState.name = function (self)
	return self._name
end
PlayerHandState.hsm = function (self)
	return self._hsm
end
PlayerHandState.at_enter = function (self, previous_state, params)
	if self._hand_unit and self._sequence and self._hand_unit:damage():has_sequence(self._sequence) then
		self._hand_unit:damage():run_sequence_simple(self._sequence)
	end

	return 
end
PlayerHandState.at_exit = function (self, next_state)
	return 
end
PlayerHandState.default_transition = function (self, next_state, params)
	self.at_exit(self, next_state)
	next_state.at_enter(next_state, self, params)

	return 
end
PlayerHandState.set_controller_enabled = function (self, enabled)
	return 
end

return 
