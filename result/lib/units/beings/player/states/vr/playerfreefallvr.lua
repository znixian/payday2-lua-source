PlayerFreefallVR = PlayerFreefall or Application:error("PlayerFreefallVR needs PlayerFreefall!")
local __enter = PlayerFreefall.enter
PlayerFreefallVR.enter = function (self, ...)
	__enter(self, ...)
	self._camera_unit:base():set_hmd_tracking(false)
	managers.menu:open_menu("zipline")

	return 
end
local __exit = PlayerFreefall.exit
PlayerFreefallVR.exit = function (self, ...)
	__exit(self, ...)
	managers.menu:close_menu("zipline")
	self._camera_unit:base():set_hmd_tracking(true)

	return 
end
PlayerFreefallVR._update_variables = function (self, t, dt)
	self._current_height = self._ext_movement:hmd_position().z

	return 
end
local __update_movement = PlayerFreefall._update_movement
PlayerFreefallVR._update_movement = function (self, t, dt)
	__update_movement(self, t, dt)
	self._unit:movement():set_ghost_position(self._unit:position())

	return 
end

return 
