PlayerEmpty = PlayerEmpty or class(PlayerMovementState)
PlayerEmpty.init = function (self, unit)
	PlayerMovementState.init(self, unit)

	return 
end
PlayerEmpty.enter = function (self, state_data, enter_data)
	PlayerMovementState.enter(self, state_data)

	return 
end
PlayerEmpty.exit = function (self, state_data)
	PlayerMovementState.exit(self, state_data)

	return 
end
PlayerEmpty.update = function (self, t, dt)
	PlayerMovementState.update(self, t, dt)

	return 
end
PlayerEmpty.destroy = function (self)
	return 
end

return 
