PlayerBleedOut = PlayerBleedOut or class(PlayerStandard)
PlayerBleedOut.init = function (self, unit)
	PlayerBleedOut.super.init(self, unit)

	return 
end
PlayerBleedOut.enter = function (self, state_data, enter_data)
	PlayerBleedOut.super.enter(self, state_data, enter_data)

	self._revive_SO_data = {
		unit = self._unit
	}

	self._start_action_bleedout(self, managers.player:player_timer():time())

	self._tilt_wait_t = managers.player:player_timer():time() + 1
	self._old_selection = nil

	if (not managers.player:has_category_upgrade("player", "primary_weapon_when_downed") or self._unit:inventory():equipped_unit():base():weapon_tweak_data().not_allowed_in_bleedout) and self._unit:inventory():equipped_selection() ~= 1 then
		local projectile_entry = managers.blackmarket:equipped_projectile()

		if tweak_data.blackmarket.projectiles[projectile_entry].is_a_grenade then
			self._interupt_action_throw_grenade(self, managers.player:player_timer():time())
		else
			self._interupt_action_throw_projectile(self, managers.player:player_timer():time())
		end

		self._old_selection = self._unit:inventory():equipped_selection()

		self._start_action_unequip_weapon(self, managers.player:player_timer():time(), {
			selection_wanted = 1
		})
		self._unit:inventory():unit_by_selection(1):base():on_reload()
	end

	self._unit:camera():play_shaker("player_bleedout_land")

	local effect_id_world = "world_downed_Peer" .. tostring(managers.network:session():local_peer():id())

	managers.time_speed:play_effect(effect_id_world, tweak_data.timespeed.downed)

	local effect_id_player = "player_downed_Peer" .. tostring(managers.network:session():local_peer():id())

	managers.time_speed:play_effect(effect_id_player, tweak_data.timespeed.downed_player)
	managers.groupai:state():on_criminal_disabled(self._unit)

	if Network:is_server() and self._ext_movement:nav_tracker() then
		self._register_revive_SO(self._revive_SO_data, "revive")
	end

	if self._state_data.in_steelsight then
		self._interupt_action_steelsight(self, managers.player:player_timer():time())
	end

	self._interupt_action_melee(self, managers.player:player_timer():time())
	self._interupt_action_ladder(self, managers.player:player_timer():time())
	managers.groupai:state():report_criminal_downed(self._unit)
	managers.network:session():send_to_peers_synched("sync_contour_state", self._unit, -1, table.index_of(ContourExt.indexed_types, "teammate_downed"), true, 1)

	return 
end
PlayerBleedOut._enter = function (self, enter_data)
	self._unit:base():set_slot(self._unit, 2)

	if Network:is_server() and self._ext_movement:nav_tracker() then
		managers.groupai:state():on_player_weapons_hot()
	end

	local preset = nil

	if managers.groupai:state():whisper_mode() then
		preset = {
			"pl_mask_on_friend_combatant_whisper_mode",
			"pl_mask_on_friend_non_combatant_whisper_mode",
			"pl_mask_on_foe_combatant_whisper_mode_crouch",
			"pl_mask_on_foe_non_combatant_whisper_mode_crouch"
		}
	else
		preset = {
			"pl_friend_combatant_cbt",
			"pl_friend_non_combatant_cbt",
			"pl_foe_combatant_cbt_crouch",
			"pl_foe_non_combatant_cbt_crouch"
		}
	end

	self._ext_movement:set_attention_settings(preset)

	return 
end
PlayerBleedOut.exit = function (self, state_data, new_state_name)
	PlayerBleedOut.super.exit(self, state_data, new_state_name)
	self._end_action_bleedout(self, managers.player:player_timer():time())
	self._unit:camera():camera_unit():base():set_target_tilt(0)

	self._tilt_wait_t = nil
	local exit_data = {
		equip_weapon = self._old_selection
	}

	if Network:is_server() then
		if new_state_name == "fatal" then
			exit_data.revive_SO_data = self._revive_SO_data
			self._revive_SO_data = nil
		else
			self._unregister_revive_SO(self)
		end
	end

	exit_data.skip_equip = true

	if new_state_name == "standard" then
		exit_data.wants_crouch = true
	end

	managers.network:session():send_to_peers_synched("sync_contour_state", self._unit, -1, table.index_of(ContourExt.indexed_types, "teammate_downed"), false, 1)

	return exit_data
end
PlayerBleedOut.interaction_blocked = function (self)
	return true
end
PlayerBleedOut._check_use_item = function (self)
	return false
end
PlayerBleedOut.update = function (self, t, dt)
	PlayerBleedOut.super.update(self, t, dt)

	if self._tilt_wait_t then
		local tilt = math.lerp(35, 0, self._tilt_wait_t - t)

		self._unit:camera():camera_unit():base():set_target_tilt(tilt)

		if self._tilt_wait_t < t then
			self._tilt_wait_t = nil

			self._unit:camera():camera_unit():base():set_target_tilt(35)
		end
	end

	return 
end
PlayerBleedOut._update_check_actions = function (self, t, dt)
	local input = self._get_input(self, t, dt)

	self._unit:camera():set_shaker_parameter("headbob", "amplitude", 0)
	self._update_throw_projectile_timers(self, t, input)
	self._update_reload_timers(self, t, dt, input)
	self._update_equip_weapon_timers(self, t, input)

	if input.btn_stats_screen_press then
		self._unit:base():set_stats_screen_visible(true)
	elseif input.btn_stats_screen_release then
		self._unit:base():set_stats_screen_visible(false)
	end

	self._update_foley(self, t, input)

	local new_action = nil
	new_action = new_action or self._check_action_weapon_gadget(self, t, input)
	new_action = new_action or self._check_action_weapon_firemode(self, t, input)
	new_action = new_action or self._check_action_reload(self, t, input)
	new_action = new_action or self._check_change_weapon(self, t, input)
	new_action = new_action or self._check_action_primary_attack(self, t, input)
	new_action = new_action or self._check_action_throw_projectile(self, t, input)
	new_action = new_action or self._check_action_equip(self, t, input)
	new_action = new_action or self._check_action_interact(self, t, input)
	new_action = new_action or self._check_action_steelsight(self, t, input)
	new_action = new_action or self._check_action_deploy_underbarrel(self, t, input)

	self._check_use_item(self, t, input)

	return 
end
PlayerBleedOut._check_use_item = function (self, t, input)
	local new_action = nil
	local action_wanted = input.btn_use_item_release and self._throw_time and t and t < self._throw_time

	if input.btn_use_item_press then
		self._throw_down = true
		self._throw_time = t + PlayerCarry.throw_limit_t
	end

	if action_wanted then
		local action_forbidden = self._use_item_expire_t or self._changing_weapon(self) or self._interacting(self) or self._ext_movement:has_carry_restriction() or self._is_throwing_projectile(self) or self._on_zipline(self)

		if not action_forbidden then
			managers.player:drop_carry()

			new_action = true
		end
	end

	if self._throw_down and input.btn_use_item_release then
		self._throw_down = false
	end

	return new_action
end
PlayerBleedOut._check_action_interact = function (self, t, input)
	if input.btn_interact_press then
		if _G.IS_VR then
			self._interact_hand = (input.btn_interact_left_press and PlayerHand.LEFT) or PlayerHand.RIGHT
		end

		if not self._intimidate_t or tweak_data.player.movement_state.interaction_delay < t - self._intimidate_t then
			self._intimidate_t = t

			if not PlayerArrested.call_teammate(self, "f11", t) then
				self.call_civilian(self, "f11", t, false, true, self._revive_SO_data)
			end
		end
	end

	return 
end
PlayerBleedOut._check_change_weapon = function (self, ...)
	local primary = self._unit:inventory():unit_by_selection(2)

	if alive(primary) and primary.base(primary):weapon_tweak_data().not_allowed_in_bleedout then
		return false
	end

	if managers.player:has_category_upgrade("player", "primary_weapon_when_downed") then
		return PlayerBleedOut.super._check_change_weapon(self, ...)
	end

	return false
end
PlayerBleedOut._check_action_equip = function (self, ...)
	local primary = self._unit:inventory():unit_by_selection(2)

	if alive(primary) and primary.base(primary):weapon_tweak_data().not_allowed_in_bleedout then
		return false
	end

	if managers.player:has_category_upgrade("player", "primary_weapon_when_downed") then
		return PlayerBleedOut.super._check_action_equip(self, ...)
	end

	return false
end
PlayerBleedOut._check_action_steelsight = function (self, ...)
	if managers.player:has_category_upgrade("player", "steelsight_when_downed") then
		return PlayerBleedOut.super._check_action_steelsight(self, ...)
	end

	return false
end
PlayerBleedOut._start_action_state_standard = function (self, t)
	managers.player:set_player_state("standard")

	return 
end
PlayerBleedOut._register_revive_SO = function (revive_SO_data, variant)
	if revive_SO_data.SO_id or not managers.navigation:is_data_ready() then
		return 
	end

	local followup_objective = {
		scan = true,
		type = "act",
		action = {
			variant = "crouch",
			body_part = 1,
			type = "act",
			blocks = {
				heavy_hurt = -1,
				hurt = -1,
				action = -1,
				aim = -1,
				walk = -1
			}
		}
	}
	local objective = {
		type = "revive",
		called = true,
		scan = true,
		destroy_clbk_key = false,
		follow_unit = revive_SO_data.unit,
		nav_seg = revive_SO_data.unit:movement():nav_tracker():nav_segment(),
		fail_clbk = callback(PlayerBleedOut, PlayerBleedOut, "on_rescue_SO_failed", revive_SO_data),
		complete_clbk = callback(PlayerBleedOut, PlayerBleedOut, "on_rescue_SO_completed", revive_SO_data),
		action_start_clbk = callback(PlayerBleedOut, PlayerBleedOut, "on_rescue_SO_started", revive_SO_data),
		action = {
			align_sync = true,
			type = "act",
			body_part = 1,
			variant = variant,
			blocks = {
				light_hurt = -1,
				hurt = -1,
				action = -1,
				heavy_hurt = -1,
				aim = -1,
				walk = -1
			}
		},
		action_duration = tweak_data.interaction[(variant == "untie" and "free") or variant].timer,
		followup_objective = followup_objective
	}
	local so_descriptor = {
		interval = 0,
		AI_group = "friendlies",
		base_chance = 1,
		chance_inc = 0,
		usage_amount = 1,
		objective = objective,
		search_pos = revive_SO_data.unit:position(),
		admin_clbk = callback(PlayerBleedOut, PlayerBleedOut, "on_rescue_SO_administered", revive_SO_data),
		verification_clbk = callback(PlayerBleedOut, PlayerBleedOut, "rescue_SO_verification", revive_SO_data.unit)
	}
	revive_SO_data.variant = variant
	local so_id = "Playerrevive"
	revive_SO_data.SO_id = so_id

	managers.groupai:state():add_special_objective(so_id, so_descriptor)

	if not revive_SO_data.deathguard_SO_id then
		revive_SO_data.deathguard_SO_id = PlayerBleedOut._register_deathguard_SO(revive_SO_data.unit)
	end

	return 
end
PlayerBleedOut.call_civilian = function (self, line, t, no_gesture, skip_alert, revive_SO_data)
	if not managers.player:has_category_upgrade("player", "civilian_reviver") or (revive_SO_data and revive_SO_data.sympathy_civ) then
		return 
	end

	local detect_only = false
	local voice_type, plural, prime_target = self._get_unit_intimidation_action(self, false, true, false, false, false, 0, true, detect_only)

	if prime_target then
		if detect_only then
			if not prime_target.unit:sound():speaking(t) then
				prime_target.unit:sound():say("_a01x_any", true)
			end
		else
			if not prime_target.unit:sound():speaking(t) then
				prime_target.unit:sound():say("stockholm_syndrome", true)
			end

			local queue_name = line .. "e_plu"

			self._do_action_intimidate(self, t, (not no_gesture and "cmd_come") or nil, queue_name, skip_alert)

			if Network:is_server() and prime_target.unit:brain():is_available_for_assignment({
				type = "revive"
			}) then
				local followup_objective = {
					interrupt_health = 1,
					interrupt_dis = -1,
					type = "free",
					action = {
						sync = true,
						body_part = 1,
						type = "idle"
					}
				}
				local objective = {
					type = "act",
					haste = "run",
					destroy_clbk_key = false,
					nav_seg = self._unit:movement():nav_tracker():nav_segment(),
					pos = self._unit:movement():nav_tracker():field_position(),
					fail_clbk = callback(PlayerBleedOut, PlayerBleedOut, "on_civ_revive_failed", revive_SO_data),
					complete_clbk = callback(PlayerBleedOut, PlayerBleedOut, "on_civ_revive_completed", revive_SO_data),
					action_start_clbk = callback(PlayerBleedOut, PlayerBleedOut, "on_civ_revive_started", revive_SO_data),
					action = {
						align_sync = true,
						type = "act",
						body_part = 1,
						variant = "revive",
						blocks = {
							light_hurt = -1,
							hurt = -1,
							action = -1,
							heavy_hurt = -1,
							aim = -1,
							walk = -1
						}
					},
					action_duration = tweak_data.interaction.revive.timer,
					followup_objective = followup_objective
				}
				revive_SO_data.sympathy_civ = prime_target.unit

				prime_target.unit:brain():set_objective(objective)
			end
		end
	end

	return 
end
PlayerBleedOut._unregister_revive_SO = function (self)
	if not self._revive_SO_data then
		return 
	end

	if self._revive_SO_data.deathguard_SO_id then
		PlayerBleedOut._unregister_deathguard_SO(self._revive_SO_data.deathguard_SO_id)

		self._revive_SO_data.deathguard_SO_id = nil
	end

	if self._revive_SO_data.SO_id then
		managers.groupai:state():remove_special_objective(self._revive_SO_data.SO_id)

		self._revive_SO_data.SO_id = nil
	elseif self._revive_SO_data.rescuer then
		local rescuer = self._revive_SO_data.rescuer
		self._revive_SO_data.rescuer = nil

		if alive(rescuer) then
			rescuer.brain(rescuer):set_objective(nil)
		end
	end

	if self._revive_SO_data.sympathy_civ then
		local sympathy_civ = self._revive_SO_data.sympathy_civ
		self._revive_SO_data.sympathy_civ = nil

		sympathy_civ.brain(sympathy_civ):set_objective(nil)
	end

	return 
end
PlayerBleedOut._register_deathguard_SO = function (my_unit)
	return 
end
PlayerBleedOut._unregister_deathguard_SO = function (so_id)
	managers.groupai:state():remove_special_objective(so_id)

	return 
end
PlayerBleedOut._start_action_bleedout = function (self, t)
	self._interupt_action_running(self, t)

	self._state_data.ducking = true

	self._stance_entered(self)
	self._update_crosshair_offset(self)
	self._unit:kill_mover()
	self._activate_mover(self, Idstring("duck"))

	return 
end
PlayerBleedOut._end_action_bleedout = function (self, t)
	if not self._can_stand(self) then
		return 
	end

	self._state_data.ducking = false

	self._stance_entered(self)
	self._update_crosshair_offset(self)
	self._unit:kill_mover()
	self._activate_mover(self, Idstring("stand"))

	return 
end
PlayerBleedOut._update_movement = function (self, t, dt)
	self._update_network_position(self, t, dt, self._unit:position())

	return 
end
PlayerBleedOut.on_rescue_SO_administered = function (self, revive_SO_data, receiver_unit)
	if revive_SO_data.rescuer then
		debug_pause("[PlayerBleedOut:on_rescue_SO_administered] Already had a rescuer!!!!", receiver_unit, revive_SO_data.rescuer)
	end

	revive_SO_data.rescuer = receiver_unit
	revive_SO_data.SO_id = nil

	if receiver_unit.movement(receiver_unit):carrying_bag() then
		receiver_unit.movement(receiver_unit):throw_bag()
	end

	return 
end
PlayerBleedOut.on_rescue_SO_failed = function (self, revive_SO_data, rescuer)
	if revive_SO_data.rescuer then
		revive_SO_data.rescuer = nil

		PlayerBleedOut._register_revive_SO(revive_SO_data, revive_SO_data.variant)
	end

	return 
end
PlayerBleedOut.on_rescue_SO_completed = function (self, revive_SO_data, rescuer)
	if revive_SO_data.sympathy_civ then
		local objective = {
			interrupt_health = 1,
			interrupt_dis = -1,
			type = "free",
			action = {
				sync = true,
				body_part = 1,
				type = "idle"
			}
		}

		revive_SO_data.sympathy_civ:brain():set_objective(objective)
	end

	revive_SO_data.rescuer = nil

	return 
end
PlayerBleedOut.on_rescue_SO_started = function (self, revive_SO_data, rescuer)
	for c_key, criminal in pairs(managers.groupai:state():all_AI_criminals()) do
		if c_key ~= rescuer.key(rescuer) then
			local obj = criminal.unit:brain():objective()

			if obj and obj.type == "revive" and obj.follow_unit:key() == revive_SO_data.unit:key() then
				criminal.unit:brain():set_objective(nil)
			end
		end
	end

	return 
end
PlayerBleedOut.rescue_SO_verification = function (ignore_this, my_unit, unit)
	return not unit.movement(unit):cool() and not my_unit.movement(my_unit):team().foes[unit.movement(unit):team().id]
end
PlayerBleedOut.on_civ_revive_completed = function (self, revive_SO_data, sympathy_civ)
	if sympathy_civ ~= revive_SO_data.sympathy_civ then
		debug_pause_unit(sympathy_civ, "[PlayerBleedOut:on_civ_revive_completed] idiot thinks he is reviving", sympathy_civ)

		return 
	end

	revive_SO_data.sympathy_civ = nil

	revive_SO_data.unit:character_damage():revive(sympathy_civ)

	if managers.player:has_category_upgrade("player", "civilian_gives_ammo") then
		managers.game_play_central:spawn_pickup({
			name = "ammo",
			position = sympathy_civ.position(sympathy_civ),
			rotation = Rotation()
		})
	end

	return 
end
PlayerBleedOut.on_civ_revive_started = function (self, revive_SO_data, sympathy_civ)
	if sympathy_civ ~= revive_SO_data.sympathy_civ then
		debug_pause_unit(sympathy_civ, "[PlayerBleedOut:on_civ_revive_started] idiot thinks he is reviving", sympathy_civ)

		return 
	end

	revive_SO_data.unit:character_damage():pause_downed_timer()

	if revive_SO_data.SO_id then
		managers.groupai:state():remove_special_objective(revive_SO_data.SO_id)

		revive_SO_data.SO_id = nil
	elseif revive_SO_data.rescuer then
		local rescuer = revive_SO_data.rescuer
		revive_SO_data.rescuer = nil

		if alive(rescuer) then
			rescuer.brain(rescuer):set_objective(nil)
		end
	end

	return 
end
PlayerBleedOut.on_civ_revive_failed = function (self, revive_SO_data, sympathy_civ)
	if revive_SO_data.sympathy_civ then
		if sympathy_civ ~= revive_SO_data.sympathy_civ then
			debug_pause_unit(sympathy_civ, "[PlayerBleedOut:on_civ_revive_failed] idiot thinks he is reviving", sympathy_civ)

			return 
		end

		revive_SO_data.unit:character_damage():unpause_downed_timer()

		revive_SO_data.sympathy_civ = nil
	end

	return 
end
PlayerBleedOut.verif_clbk_is_unit_deathguard = function (self, enemy_unit)
	local char_tweak = tweak_data.character[enemy_unit.base(enemy_unit)._tweak_table]

	return char_tweak.deathguard
end
PlayerBleedOut.clbk_deathguard_administered = function (self, unit)
	unit.movement(unit):set_cool(false)

	return 
end
PlayerBleedOut.pre_destroy = function (self, unit)
	if Network:is_server() then
		self._unregister_revive_SO(self)
	end

	return 
end
PlayerBleedOut.destroy = function (self)
	if Network:is_server() then
		self._unregister_revive_SO(self)
	end

	return 
end

return 
