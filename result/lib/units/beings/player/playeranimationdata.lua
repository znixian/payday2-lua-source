PlayerAnimationData = PlayerAnimationData or class()
PlayerAnimationData.init = function (self, unit)
	self._unit = unit

	return 
end
PlayerAnimationData.anim_clbk_footstep_l = function (self, unit)
	self._footstep = "l"

	unit.base(unit):anim_data_clbk_footstep("left")

	return 
end
PlayerAnimationData.anim_clbk_footstep_r = function (self, unit)
	self._footstep = "r"

	unit.base(unit):anim_data_clbk_footstep("right")

	return 
end
PlayerAnimationData.anim_clbk_startfoot_l = function (self, unit)
	self._footstep = "l"

	return 
end
PlayerAnimationData.anim_clbk_startfoot_r = function (self, unit)
	self._footstep = "r"

	return 
end
PlayerAnimationData.foot = function (self)
	return self._footstep
end
PlayerAnimationData.anim_clbk_upper_body_empty = function (self, unit)
	unit.anim_state_machine(unit):stop_segment(Idstring("upper_body"))

	return 
end
PlayerAnimationData.anim_clbk_base_empty = function (self, unit)
	unit.anim_state_machine(unit):stop_segment(Idstring("base"))

	return 
end
PlayerAnimationData.anim_clbk_death_exit = function (self, unit)
	unit.movement(unit):on_death_exit()
	unit.base(unit):on_death_exit()

	if unit.inventory(unit) then
		unit.inventory(unit):on_death_exit()
	end

	return 
end

return 
