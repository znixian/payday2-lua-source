UnitDamage = UnitDamage or class(CoreUnitDamage)
UnitDamage.COLLISION_SFX_QUITE_TIME = 0.3
UnitDamage.SFX_COLLISION_TAG = Idstring("sfx_only")
UnitDamage.EVENTS = {
	"on_take_damage"
}
UnitDamage.init = function (self, unit, ...)
	CoreUnitDamage.init(self, unit, ...)

	if self.can_play_collision_sfx(self) then
		self._collision_sfx_quite_time = self._collision_sfx_quite_time or UnitDamage.COLLISION_SFX_QUITE_TIME

		self.setup_sfx_collision_body_tags(self)
	end

	self._listener_holder = EventListenerHolder:new()

	if self.report_damage_type then
		self._damage_report = {}

		for i, dmg_type in ipairs(string.split(self.report_damage_type, " ")) do
			table.insert(self._damage_report, dmg_type)
		end
	end

	return 
end
UnitDamage.add_listener = function (self, key, events, clbk)
	self._listener_holder:add(key, events, clbk)

	return 
end
UnitDamage.remove_listener = function (self, key)
	self._listener_holder:remove(key)

	return 
end
UnitDamage.add_damage = function (self, endurance_type, attack_unit, dest_body, normal, position, direction, damage, velocity)
	local result, damage = UnitDamage.super.add_damage(self, endurance_type, attack_unit, dest_body, normal, position, direction, damage, velocity)

	if not self._damage_report or table.contains(self._damage_report, endurance_type) then
		self._listener_holder:call("on_take_damage", self._unit, attack_unit, endurance_type, damage)
	end

	return result, damage
end
UnitDamage.setup_sfx_collision_body_tags = function (self)
	for i = 0, self._unit:num_bodies() - 1, 1 do
		local body = self._unit:body(i)

		if not self._has_body_collision_damage(self, body.name(body)) then
			body.set_collision_script_tag(body, self.SFX_COLLISION_TAG)
		end
	end

	return 
end
UnitDamage._has_body_collision_damage = function (self, body_name)
	for name, data in pairs(self._unit_element._bodies) do
		if Idstring(name) == body_name then
			return (data._first_endurance.collision and true) or false
		end
	end

	return false
end
UnitDamage.can_play_collision_sfx = function (self)
	return self._collision_event ~= nil
end
UnitDamage.set_play_collision_sfx_quite_time = function (self, quite_time)
	if self._collision_sfx_quite_time == nil ~= quite_time == nil and quite_time then
		self.setup_sfx_collision_body_tags(self)
	end

	self._collision_sfx_quite_time = quite_time

	return 
end
UnitDamage.body_collision_callback = function (self, tag, unit, body, other_unit, other_body, position, normal, collision_velocity, velocity, other_velocity)
	if self._collision_sfx_quite_time ~= nil and other_body and body then
		local t = TimerManager:game():time()

		if not self._play_collision_sfx_time or self._play_collision_sfx_time <= t then
			self.play_collision_sfx(self, other_unit, position, normal, collision_velocity)

			self._play_collision_sfx_time = t + self._collision_sfx_quite_time
		end
	end

	if tag ~= self.SFX_COLLISION_TAG then
		CoreUnitDamage.body_collision_callback(self, tag, unit, body, other_unit, other_body, position, normal, collision_velocity, velocity, other_velocity)
	end

	return 
end
UnitDamage.play_collision_sfx = function (self, other_unit, position, normal, collision_velocity)
	local ss = SoundDevice:create_source("collision")

	ss.set_position(ss, position)
	ss.post_event(ss, self._collision_event)

	return 
end
UnitDamage.set_update_callback = function (self, func_name, ...)
	if func_name == "update_proximity_list" and not Network:is_server() and self._unit:id() ~= -1 then
		return 
	end

	UnitDamage.super.set_update_callback(self, func_name, ...)

	return 
end

return 
