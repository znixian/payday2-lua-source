core:import("CoreSubtitlePresenter")

DramaExt = DramaExt or class()
DramaExt.init = function (self, unit)
	self._unit = unit
	self._cue = nil

	return 
end
DramaExt.name = function (self)
	return self.character_name
end
DramaExt.play_sound = function (self, sound, sound_source)
	self._cue = self._cue or {}
	self._cue.sound = sound
	self._cue.sound_source = sound_source
	local playing = self._unit:sound_source(sound_source):post_event(sound, self.sound_callback, self._unit, "marker", "end_of_event")

	if not playing then
		Application:error("[DramaExt:play_cue] Wasn't able to play sound event " .. sound)
		Application:stack_dump()
		self.sound_callback(self, nil, "end_of_event", self._unit, sound_source, nil, nil, nil)
	end

	return 
end
DramaExt.play_subtitle = function (self, string_id, duration)
	self._cue = self._cue or {}
	self._cue.string_id = string_id

	managers.subtitle:set_visible(true)
	managers.subtitle:set_enabled(true)

	if not duration or duration == 0 then
		managers.subtitle:show_subtitle(string_id, 100000)
	else
		managers.subtitle:show_subtitle(string_id, duration)
	end

	return 
end
DramaExt.stop_cue = function (self)
	if self._cue then
		if self._cue.string_id then
			managers.subtitle:set_visible(false)
			managers.subtitle:set_enabled(false)
		end

		if self._cue.sound then
			self._unit:sound_source(self._cue.sound_source):stop()
		end

		self._cue = nil
	end

	return 
end
DramaExt.sound_callback = function (self, instance, event_type, unit, sound_source, label, identifier, position)
	if event_type == "end_of_event" then
		managers.subtitle:set_visible(false)
		managers.subtitle:set_enabled(false)
		managers.dialog:finished()
	elseif event_type == "marker" and sound_source then
		managers.subtitle:set_visible(true)
		managers.subtitle:set_enabled(true)
		managers.subtitle:show_subtitle(sound_source, DramaExt._subtitle_len(DramaExt, sound_source))
	end

	return 
end
DramaExt._subtitle_len = function (self, id)
	local duration = self._length_from_tweak(self, id)

	if duration == nil then
		local text = managers.localization:text(id)
		duration = text.len(text)*tweak_data.dialog.DURATION_PER_CHAR
	end

	if duration < tweak_data.dialog.MINIMUM_DURATION then
		duration = tweak_data.dialog.MINIMUM_DURATION
	end

	return duration
end
DramaExt._length_from_tweak = function (self, id)
	local subtitles_tweak = tweak_data.subtitles.jobs[managers.job:current_real_job_id()]

	if subtitles_tweak and subtitles_tweak[id] then
		return subtitles_tweak[id] + tweak_data.subtitles.additional_time
	end

	return nil
end

return 
