BlinkExt = BlinkExt or class()
BlinkExt.init = function (self, unit)
	self._object_list = {}

	if self._objects then
		for _, object in ipairs(self._objects) do
			table.insert(self._object_list, unit.get_object(unit, Idstring(object)))
		end
	end

	self._effect_list = {}

	if self._effects then
		for _, effect in ipairs(self._effects) do
			table.insert(self._effect_list, unit.effect_spawner(unit, Idstring(effect)))
		end
	end

	if self._state then
		self.set_state(self, self._state, self._delay or 1)
	end

	return 
end
BlinkExt.update = function (self, unit, t, dt)
	if self._delay_current and self._delay_current < t then
		if self._state == "cycle" then
			if self._current_object then
				self._object_list[self._current_object]:set_visibility(false)

				self._current_object = (self._current_object == #self._object_list and 1) or self._current_object + 1

				self._object_list[self._current_object]:set_visibility(true)
			end

			if self._current_effect then
				self._effect_list[self._current_effect]:set_enabled(false)

				self._current_effect = (self._current_effect == #self._effect_list and 1) or self._current_effect + 1

				self._effect_list[self._current_effect]:set_enabled(true)
			end
		elseif self._state == "rand_cycle" then
			if self._current_object then
				self._object_list[self._current_object]:set_visibility(false)

				self._current_object = math.random(#self._object_list)

				self._object_list[self._current_object]:set_visibility(true)
			end

			if self._current_effect then
				self._effect_list[self._current_effect]:set_enabled(false)

				self._current_effect = math.random(#self._effect_list)

				self._effect_list[self._current_effect]:set_enabled(true)
			end
		else
			for _, object in ipairs(self._object_list) do
				object.set_visibility(object, not object.visibility(object))
			end

			for _, effect in ipairs(self._effect_list) do
				effect.set_enabled(effect, not effect.enabled(effect))
			end
		end

		self._delay_current = TimerManager:game():time() + self._delay
	end

	return 
end
BlinkExt.set_state = function (self, state, delay)
	self._state = state
	self._delay = delay

	if state == "twinkle" then
		local swap = true

		for _, object in ipairs(self._object_list) do
			object.set_visibility(object, swap)

			swap = not swap
		end

		swap = true

		for _, effect in ipairs(self._effect_list) do
			effect.set_enabled(effect, swap)

			swap = not swap
		end

		self._delay_current = TimerManager:game():time() + delay
	elseif state == "cycle" then
		for _, object in ipairs(self._object_list) do
			object.set_visibility(object, false)
		end

		for _, effect in ipairs(self._effect_list) do
			effect.set_enabled(effect, false)
		end

		if 0 < #self._object_list then
			self._current_object = 1

			self._object_list[1]:set_visibility(true)
		end

		if 0 < #self._effect_list then
			self._current_effect = 1

			self._effect_list[1]:set_enabled(true)
		end

		self._delay_current = TimerManager:game():time() + delay
	elseif state == "rand_cycle" then
		for _, object in ipairs(self._object_list) do
			object.set_visibility(object, false)
		end

		for _, effect in ipairs(self._effect_list) do
			effect.set_enabled(effect, false)
		end

		if 0 < #self._object_list then
			self._current_object = math.random(#self._object_list)

			self._object_list[self._current_object]:set_visibility(true)
		end

		if 0 < #self._effect_list then
			self._current_effect = math.random(#self._effect_list)

			self._effect_list[self._current_effect]:set_enabled(true)
		end

		self._delay_current = TimerManager:game():time() + delay
	elseif state == "blink" then
		for _, object in ipairs(self._object_list) do
			object.set_visibility(object, true)
		end

		for _, effect in ipairs(self._effect_list) do
			effect.set_enabled(effect, true)
		end

		self._delay_current = TimerManager:game():time() + delay
	elseif state == "disable" then
		for _, object in ipairs(self._object_list) do
			object.set_visibility(object, false)
		end

		for _, effect in ipairs(self._effect_list) do
			effect.set_enabled(effect, false)
		end

		self._delay_current = nil
	elseif state == "enable" then
		for _, object in ipairs(self._object_list) do
			object.set_visibility(object, true)
		end

		for _, effect in ipairs(self._effect_list) do
			effect.set_enabled(effect, true)
		end

		self._delay_current = nil
	end

	return 
end
BlinkExt.save = function (self, data)
	data.BlinkExt = {
		state = self._state,
		delay = self._delay
	}

	return 
end
BlinkExt.load = function (self, data)
	local state = data.BlinkExt
	self._state = data.BlinkExt.state
	self._delay = data.BlinkExt.delay

	self.set_state(self, self._state, self._delay)

	return 
end

return 
