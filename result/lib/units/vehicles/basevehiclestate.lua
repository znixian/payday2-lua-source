BaseVehicleState = BaseVehicleState or class()
BaseVehicleState.init = function (self, unit)
	self._unit = unit

	return 
end
BaseVehicleState.update = function (self, t, dt)
	self._unit:vehicle_driving():_wake_nearby_dynamics()
	self._unit:vehicle_driving():_detect_npc_collisions()
	self._unit:vehicle_driving():_detect_collisions(t, dt)
	self._unit:vehicle_driving():_detect_invalid_possition(t, dt)
	self._unit:vehicle_driving():_play_sound_events(t, dt)

	return 
end
BaseVehicleState.enter = function (self, state_data, enter_data)
	return 
end
BaseVehicleState.exit = function (self, state_data)
	return 
end
BaseVehicleState.get_action_for_interaction = function (self, pos, locator, tweak_data)
	local locator_name = locator.name(locator)

	for _, seat in pairs(tweak_data.seats) do
		if locator_name == Idstring(VehicleDrivingExt.INTERACTION_PREFIX .. seat.name) then
			if seat.driving then
				return VehicleDrivingExt.INTERACT_DRIVE
			else
				return VehicleDrivingExt.INTERACT_ENTER
			end
		end
	end

	for _, loot_point in pairs(tweak_data.loot_points) do
		if locator_name == Idstring(VehicleDrivingExt.INTERACTION_PREFIX .. loot_point.name) then
			return VehicleDrivingExt.INTERACT_LOOT
		end
	end

	if tweak_data.repair_point and locator_name == Idstring(VehicleDrivingExt.INTERACTION_PREFIX .. tweak_data.repair_point) then
		return VehicleDrivingExt.INTERACT_REPAIR
	end

	if tweak_data.trunk_point and locator_name == Idstring(VehicleDrivingExt.INTERACTION_PREFIX .. tweak_data.trunk_point) then
		return VehicleDrivingExt.INTERACT_TRUNK
	end

	return VehicleDrivingExt.INTERACT_INVALID
end
BaseVehicleState.adjust_interactions = function (self)
	if not self._unit:vehicle_driving():is_interaction_allowed() then
		self.disable_interactions(self)
	end

	return 
end
BaseVehicleState.disable_interactions = function (self)
	if self._unit:damage() and self._unit:damage():has_sequence(VehicleDrivingExt.INTERACT_ENTRY_ENABLED) then
		self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_ENTRY_DISABLED)
		self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_LOOT_DISABLED)
		self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_REPAIR_DISABLED)
		self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_INTERACTION_DISABLED)

		self._unit:vehicle_driving()._interaction_enter_vehicle = false
		self._unit:vehicle_driving()._interaction_loot = false
		self._unit:vehicle_driving()._interaction_trunk = false
		self._unit:vehicle_driving()._interaction_repair = false
	end

	return 
end
BaseVehicleState.allow_exit = function (self)
	return true
end
BaseVehicleState.is_vulnerable = function (self)
	return false
end
BaseVehicleState.stop_vehicle = function (self)
	return false
end

return 
