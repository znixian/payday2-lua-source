VehicleStateLocked = VehicleStateLocked or class(BaseVehicleState)
VehicleStateLocked.init = function (self, unit)
	BaseVehicleState.init(self, unit)

	return 
end
VehicleStateLocked.enter = function (self, state_data, enter_data)
	self._unit:vehicle_driving():_stop_engine_sound()
	self._unit:interaction():set_override_timer_value(VehicleDrivingExt.TIME_ENTER)
	self.disable_interactions(self)
	self._unit:vehicle_driving():set_input(0, 0, 1, 1, false, false, 2)

	return 
end
VehicleStateLocked.allow_exit = function (self)
	return true
end
VehicleStateLocked.stop_vehicle = function (self)
	return true
end
VehicleStateLocked.is_vulnerable = function (self)
	return false
end

return 
