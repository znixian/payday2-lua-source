VehicleStateFrozen = VehicleStateFrozen or class(BaseVehicleState)
VehicleStateFrozen.init = function (self, unit)
	BaseVehicleState.init(self, unit)

	return 
end
VehicleStateFrozen.enter = function (self, state_data, enter_data)
	self._unit:vehicle_driving()._hit_soundsource:stop()
	self._unit:vehicle_driving()._slip_soundsource:stop()
	self._unit:vehicle_driving()._bump_soundsource:stop()
	self._unit:interaction():set_override_timer_value(VehicleDrivingExt.TIME_ENTER)

	self._unit:vehicle_driving()._shooting_stance_allowed = false

	self.disable_interactions(self)
	self._unit:vehicle_driving():set_input(0, 0, 1, 1, false, false, 2)

	return 
end
VehicleStateFrozen.allow_exit = function (self)
	return false
end
VehicleStateFrozen.stop_vehicle = function (self)
	return true
end
VehicleStateFrozen.is_vulnerable = function (self)
	return false
end

return 
