VehicleStateParked = VehicleStateParked or class(BaseVehicleState)
VehicleStateParked.init = function (self, unit)
	BaseVehicleState.init(self, unit)

	return 
end
VehicleStateParked.enter = function (self, state_data, enter_data)
	if not self._unit:vehicle():is_active() then
		self._unit:vehicle_driving():activate_vehicle()
		self._unit:vehicle_driving():_start_engine_sound()
	end

	self._unit:interaction():set_override_timer_value(VehicleDrivingExt.TIME_ENTER)
	self.adjust_interactions(self)
	self._unit:vehicle_driving():set_input(0, 0, 1, 1, false, false, 2)

	return 
end
VehicleStateParked.adjust_interactions = function (self)
	VehicleStateParked.super.adjust_interactions(self)

	if self._unit:vehicle_driving():is_interaction_allowed() then
		if self._unit:damage() and self._unit:damage():has_sequence(VehicleDrivingExt.INTERACT_ENTRY_ENABLED) then
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_INTERACTION_ENABLED)
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_ENTRY_ENABLED)
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_LOOT_ENABLED)
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_REPAIR_DISABLED)
		end

		self._unit:vehicle_driving()._interaction_enter_vehicle = true

		if self._unit:vehicle_driving()._has_trunk then
			self._unit:vehicle_driving()._interaction_trunk = true
		else
			self._unit:vehicle_driving()._interaction_loot = true
		end

		self._unit:vehicle_driving()._interaction_repair = false
	end

	return 
end
VehicleStateParked.is_vulnerable = function (self)
	return true
end
VehicleStateParked.stop_vehicle = function (self)
	return true
end

return 
