VehicleStateBlocked = VehicleStateBlocked or class(BaseVehicleState)
VehicleStateBlocked.init = function (self, unit)
	BaseVehicleState.init(self, unit)

	return 
end
VehicleStateBlocked.enter = function (self, state_data, enter_data)
	self._unit:vehicle_driving():_stop_engine_sound()
	self._unit:interaction():set_override_timer_value(VehicleDrivingExt.TIME_ENTER)
	self.adjust_interactions(self)
	self._unit:vehicle_driving():set_input(0, 0, 1, 1, false, false, 2)

	return 
end
VehicleStateBlocked.adjust_interactions = function (self)
	VehicleStateParked.super.adjust_interactions(self)

	if self._unit:vehicle_driving():is_interaction_allowed() then
		if self._unit:damage() and self._unit:damage():has_sequence(VehicleDrivingExt.INTERACT_ENTRY_ENABLED) then
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_INTERACTION_ENABLED)
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_ENTRY_ENABLED)
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_LOOT_ENABLED)
			self._unit:damage():run_sequence_simple(VehicleDrivingExt.INTERACT_REPAIR_DISABLED)
		end

		self._unit:vehicle_driving()._interaction_enter_vehicle = true

		if self._unit:vehicle_driving()._has_trunk then
			self._unit:vehicle_driving()._interaction_trunk = true
		else
			self._unit:vehicle_driving()._interaction_loot = true
		end

		self._unit:vehicle_driving()._interaction_repair = false
	end

	return 
end
VehicleStateBlocked.allow_exit = function (self)
	return true
end
VehicleStateBlocked.stop_vehicle = function (self)
	return true
end
VehicleStateBlocked.is_vulnerable = function (self)
	return false
end

return 
