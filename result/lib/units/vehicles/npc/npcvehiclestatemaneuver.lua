NpcVehicleStateManeuver = NpcVehicleStateManeuver or class(NpcBaseVehicleState)
NpcVehicleStateManeuver.init = function (self, unit)
	NpcBaseVehicleState.init(self, unit)

	return 
end
NpcVehicleStateManeuver.on_enter = function (self, npc_driving_ext)
	local unit_id = npc_driving_ext._unit:unit_data().unit_id

	managers.motion_path:remove_ground_unit_from_path(unit_id)

	return 
end
NpcVehicleStateManeuver.on_exit = function (self, npc_driving_ext)
	local unit_id = npc_driving_ext._unit:unit_data().unit_id
	local path_info = managers.motion_path:find_nearest_ground_path(unit_id)

	if path_info then
		path_info.unit_id = unit_id

		managers.motion_path:put_unit_on_path(path_info)
	end

	return 
end
NpcVehicleStateManeuver.update = function (self, npc_driving_ext, t, dt)
	return 
end
NpcVehicleStateManeuver.name = function (self)
	return NpcVehicleDrivingExt.STATE_MANEUVER
end
NpcVehicleStateManeuver.change_state = function (self, npc_driving_ext)
	return 
end
NpcVehicleStateManeuver.is_maneuvering = function (self)
	return true
end

return 
