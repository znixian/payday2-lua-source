NpcVehicleStatePursuit = NpcVehicleStatePursuit or class(NpcBaseVehicleState)
NpcVehicleStatePursuit.init = function (self, unit)
	NpcBaseVehicleState.init(self, unit)

	self._vehicle = self._unit:vehicle()
	self._next_checkpoint_distance = {
		{
			v_min = 30,
			distance = 1200,
			relative_angle_min = 30,
			relative_angle_max = 60,
			v_max = 40
		},
		{
			v_min = 40,
			distance = 1400,
			relative_angle_min = 30,
			relative_angle_max = 90,
			v_max = 60
		},
		{
			v_min = 60,
			distance = 2000,
			relative_angle_min = 30,
			relative_angle_max = 90,
			v_max = 90
		}
	}
	local cop_position = self._unit:position()
	local delayed_tick = Application:time() + 5
	self._tachograph = {
		distance = 0,
		timeframe = 1,
		tick_at = delayed_tick,
		last_pos = cop_position
	}

	return 
end
NpcVehicleStatePursuit.on_enter = function (self, npc_driving_ext)
	print("Npc state change: ", self.name(self))

	local cop_position = self._unit:position()
	local delayed_tick = Application:time() + 5
	self._tachograph = {
		distance = 0,
		timeframe = 1,
		tick_at = delayed_tick,
		last_pos = cop_position
	}
	self._next_state = nil
	self._desired_direction = 0

	return 
end
NpcVehicleStatePursuit.update = function (self, t, dt)
	return 
end
NpcVehicleStatePursuit.name = function (self)
	return NpcVehicleDrivingExt.STATE_PURSUIT
end
NpcVehicleStatePursuit.calc_steering = function (self, angle)
	self._desired_direction = angle
	local direction = 0
	local normalized_steer = 0
	local scale_steering = 1
	local final_steer = 0
	local FULL_TURN_ANGLE_THRESHOLD = 20

	if 0 <= angle and angle < 90 then
		direction = 1
		normalized_steer = math.clamp(angle/FULL_TURN_ANGLE_THRESHOLD, 0, 1)
		final_steer = normalized_steer*direction
	elseif 90 <= angle and angle < 180 then
		direction = 1
		final_steer = direction
	elseif 180 <= angle and angle < 270 then
		direction = -1
		final_steer = direction
	elseif 270 <= angle then
		direction = -1
		normalized_steer = math.clamp((angle - 270 - 90)/FULL_TURN_ANGLE_THRESHOLD, 0, 1)
		final_steer = normalized_steer*direction
	end

	return final_steer
end
NpcVehicleStatePursuit.calc_distance_threshold = function (self, angle)
	local vehicle_state = self._vehicle:get_state()
	local current_speed = vehicle_state.get_speed(vehicle_state)*3.6
	local threshold = 1000

	for _, data in ipairs(self._next_checkpoint_distance) do
		if data.v_min < current_speed and current_speed <= data.v_max and data.relative_angle_min < angle and angle <= data.relative_angle_max then
			threshold = data.distance
		end
	end

	return threshold
end
NpcVehicleStatePursuit.calc_speed_limit = function (self, path, unit_and_pos)
	local default_speed_limit = path.default_speed_limit or -1
	local retval = default_speed_limit
	local points_in_direction = nil

	if not unit_and_pos.direction or unit_and_pos.direction == "fwd" then
		points_in_direction = path.points
	else
		points_in_direction = path.points_bck
	end

	local target_speed = points_in_direction[unit_and_pos.target_checkpoint].speed

	if target_speed ~= -1 then
		retval = target_speed/27.77
	end

	return retval
end
NpcVehicleStatePursuit.handle_hard_turn = function (self, npc_driving_ext, angle_to_target)
	local vehicle_state = self._vehicle:get_state()
	local current_speed = vehicle_state.get_speed(vehicle_state)*3.6

	if 90 < angle_to_target and 20 < current_speed then
		npc_driving_ext._current_drive_controls = "handbrake"
	elseif not self._last_checkpoint_reached then
		npc_driving_ext._current_drive_controls = "accelerate"
	end

	return 
end
NpcVehicleStatePursuit.evasion_maneuvers = function (self, npc_driving_ext, target_steering)
	return self._loco_unit_proximity(self, npc_driving_ext, target_steering)
end
NpcVehicleStatePursuit._loco_unit_proximity = function (self, npc_driving_ext, target_steering)
	local retval = nil
	local player_unit = npc_driving_ext._get_target_unit(npc_driving_ext)

	if not player_unit then
		return retval
	end

	local player_position = player_unit.position(player_unit)
	local cop_position = self._unit:position()
	local distance_to_player = math.abs(player_position - cop_position:length())/100
	local acceleration = 0
	local brake = 0
	local handbrake = 0

	if npc_driving_ext._drive_mode and npc_driving_ext._drive_mode[npc_driving_ext._current_drive_mode] then
		acceleration = npc_driving_ext._drive_mode[npc_driving_ext._current_drive_mode].acceleration
		brake = npc_driving_ext._drive_mode[npc_driving_ext._current_drive_mode].brake
		handbrake = npc_driving_ext._drive_mode[npc_driving_ext._current_drive_mode].handbrake
	end

	if npc_driving_ext._debug.nav_paths then
		npc_driving_ext._debug.nav_paths.distance_to_player = distance_to_player
	end

	local current_player_proximity_distance = managers.motion_path:get_player_proximity_distance()

	if distance_to_player < current_player_proximity_distance then
		local unit_id = self._unit:unit_data().unit_id

		managers.motion_path:set_player_proximity_distance_for_unit(unit_id, current_player_proximity_distance)
		managers.motion_path:increase_player_proximity_distance()
		npc_driving_ext.set_state(npc_driving_ext, NpcVehicleDrivingExt.STATE_PLAYER_PROXIMITY)

		retval = {
			acceleration = 0,
			handbrake = 1,
			brake = 1,
			steering = target_steering
		}
	end

	return retval
end
NpcVehicleStatePursuit.change_state = function (self, npc_driving_ext)
	if self._next_state then
		npc_driving_ext.set_state(npc_driving_ext, self._next_state)
	end

	return 
end
NpcVehicleStatePursuit.is_maneuvering = function (self)
	return false
end
NpcVehicleStatePursuit.handle_stuck_vehicle = function (self, npc_driving_ext, t, dt)
	if not self._tachograph then
		return 
	end

	if self._tachograph.tick_at < t then
		local cop_position = self._unit:position()
		self._tachograph.tick_at = t + self._tachograph.timeframe
		self._tachograph.distance = cop_position - self._tachograph.last_pos:length()/100
		self._tachograph.last_pos = cop_position

		if self._tachograph.distance <= 1 then
			self._next_state = self._choose_recovery_maneuver(self)
		end
	end

	return 
end
NpcVehicleStatePursuit._choose_recovery_maneuver = function (self)
	local recovery_maneuver = nil

	if 0 <= self._desired_direction and self._desired_direction < 90 then
		recovery_maneuver = NpcVehicleDrivingExt.STATE_MANEUVER_BACK_LEFT
	elseif 90 <= self._desired_direction and self._desired_direction < 180 then
		recovery_maneuver = NpcVehicleDrivingExt.STATE_MANEUVER_BACK_LEFT
	elseif 180 <= self._desired_direction and self._desired_direction < 270 then
		recovery_maneuver = NpcVehicleDrivingExt.STATE_MANEUVER_BACK_RIGHT
	elseif 270 <= self._desired_direction then
		recovery_maneuver = NpcVehicleDrivingExt.STATE_MANEUVER_BACK_RIGHT
	end

	return recovery_maneuver
end

return 
