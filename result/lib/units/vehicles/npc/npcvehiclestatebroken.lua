NpcVehicleStateBroken = NpcVehicleStateBroken or class(NpcBaseVehicleState)
NpcVehicleStateBroken.init = function (self, unit)
	NpcBaseVehicleState.init(self, unit)

	return 
end
NpcVehicleStateBroken.update = function (self, t, dt)
	return 
end
NpcVehicleStateBroken.name = function (self)
	return NpcVehicleDrivingExt.STATE_BROKEN
end
NpcVehicleStateBroken.on_enter = function (self)
	print("NpcVehicleStateBroken:on_enter()")

	if self._unit:damage():has_sequence(VehicleDrivingExt.SEQUENCE_FULL_DAMAGED) then
		self._unit:damage():run_sequence_simple(VehicleDrivingExt.SEQUENCE_FULL_DAMAGED)
	end

	return 
end

return 
