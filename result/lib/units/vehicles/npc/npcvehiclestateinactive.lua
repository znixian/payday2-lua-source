NpcVehicleStateInactive = NpcVehicleStateInactive or class(NpcBaseVehicleState)
NpcVehicleStateInactive.init = function (self, unit)
	NpcBaseVehicleState.init(self, unit)

	return 
end
NpcVehicleStateInactive.update = function (self, t, dt)
	return 
end
NpcVehicleStateInactive.name = function (self)
	return NpcVehicleDrivingExt.STATE_INACTIVE
end

return 
