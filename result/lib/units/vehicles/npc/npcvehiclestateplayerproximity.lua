NpcVehicleStatePlayerProximity = NpcVehicleStatePlayerProximity or class(NpcBaseVehicleState)
NpcVehicleStatePlayerProximity.init = function (self, unit)
	NpcBaseVehicleState.init(self, unit)

	return 
end
NpcVehicleStatePlayerProximity.on_enter = function (self, npc_driving_ext)
	return 
end
NpcVehicleStatePlayerProximity.on_exit = function (self, npc_driving_ext)
	managers.motion_path:reset_player_proximity_distance()

	return 
end
NpcVehicleStatePlayerProximity.update = function (self, t, dt)
	return 
end
NpcVehicleStatePlayerProximity.name = function (self)
	return NpcVehicleDrivingExt.STATE_PLAYER_PROXIMITY
end
NpcVehicleStatePlayerProximity.change_state = function (self, npc_driving_ext)
	local player_unit = npc_driving_ext._get_target_unit(npc_driving_ext)

	if not player_unit then
		return 
	end

	local player_position = player_unit.position(player_unit)
	local cop_position = self._unit:position()
	local distance_to_player = math.abs(player_position - cop_position:length())/100

	if npc_driving_ext._debug.nav_paths then
		npc_driving_ext._debug.nav_paths.distance_to_player = distance_to_player
	end

	local unit_id = self._unit:unit_data().unit_id
	local unit_to_player_proximity_distance = managers.motion_path:get_player_proximity_distance_for_unit(unit_id)

	if unit_to_player_proximity_distance <= distance_to_player then
		npc_driving_ext.set_state(npc_driving_ext, NpcVehicleDrivingExt.STATE_PURSUIT)
	end

	return 
end

return 
