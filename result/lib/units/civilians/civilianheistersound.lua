CivilianHeisterSound = CivilianHeisterSound or class(PlayerSound)
CivilianHeisterSound.init = function (self, unit)
	self._unit = unit

	unit.base(unit):post_init()

	local ss = self._unit:sound_source()

	ss.set_switch(ss, "int_ext", "third")

	return 
end
CivilianHeisterSound.set_room_level = function (self, level)
	local level_switches = {
		"state_01",
		"state_02",
		"state_03"
	}
	local ss = self._unit:sound_source()

	ss.set_switch(ss, "safehouse_state", level_switches[level] or level_switches[1])

	return 
end
CivilianHeisterSound.set_interactor_voice = function (self, interactor_voice)
	local ss = self._unit:sound_source()

	ss.set_switch(ss, "safehouse_toggle_robber", interactor_voice)

	return 
end
CivilianHeisterSound.set_minigame_response = function (self, state)
	local ss = self._unit:sound_source()

	ss.set_switch(ss, "safehouse_minigame", state)

	return 
end
CivilianHeisterSound.anim_clbk_play_sound = function (self, unit, queue_name)
	self._play(self, queue_name)

	return 
end
CivilianHeisterSound.anim_clbk_stop_sound = function (self, unit, source_name)
	if source_name and source_name == Idstring("") then
		self.stop(self)

		return 
	end

	self.stop(self, source_name)

	return 
end
CivilianHeisterSound._play = function (self, sound_name, source_name)
	local source = nil

	if source_name then
		source = Idstring(source_name)
	end

	return self._unit:sound_source(source):post_event(sound_name, self.sound_callback, self._unit, "marker", "end_of_event")
end
CivilianHeisterSound.sound_callback = function (self, instance, event_type, unit, sound_source, label, identifier, position)
	if not alive(unit) then
		return 
	end

	if event_type == "end_of_event" and unit.interaction(unit) and unit.interaction(unit)._reenable_ext then
		unit.interaction(unit):_reenable_ext()
	end

	return 
end

return 
