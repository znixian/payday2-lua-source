require("lib/units/enemies/cop/logics/CopLogicInactive")

CivilianLogicInactive = class(CopLogicInactive)
CivilianLogicInactive.on_enemy_weapons_hot = function (data)
	data.unit:brain():set_attention_settings(nil)

	return 
end
CivilianLogicInactive._register_attention = function (data, my_data)
	if data.unit:character_damage():dead() and managers.groupai:state():whisper_mode() then
		data.unit:brain():set_attention_settings({
			"civ_enemy_corpse_sneak"
		})
	else
		data.unit:brain():set_attention_settings(nil)
	end

	return 
end
CivilianLogicInactive._set_interaction = function (data, my_data)
	if data.unit:character_damage():dead() and not managers.groupai:state():whisper_mode() then
		data.unit:interaction():set_tweak_data("corpse_dispose")
		data.unit:interaction():set_active(true, true, true)
	end

	return 
end

return 
