HuskCivilianDamage = HuskCivilianDamage or class(HuskCopDamage)
HuskCivilianDamage._HEALTH_INIT = CivilianDamage._HEALTH_INIT
HuskCivilianDamage.damage_bullet = CivilianDamage.damage_bullet
HuskCivilianDamage.damage_melee = CivilianDamage.damage_melee
HuskCivilianDamage.no_intimidation_by_dmg = CivilianDamage.no_intimidation_by_dmg
HuskCivilianDamage._on_damage_received = function (self, damage_info)
	CivilianDamage._on_damage_received(self, damage_info)

	return 
end
HuskCivilianDamage._unregister_from_enemy_manager = function (self, damage_info)
	CivilianDamage._unregister_from_enemy_manager(self, damage_info)

	return 
end
HuskCivilianDamage.damage_explosion = function (self, attack_data)
	if attack_data.variant == "explosion" then
		attack_data.damage = 10
	end

	return CopDamage.damage_explosion(self, attack_data)
end
HuskCivilianDamage.damage_fire = function (self, attack_data)
	if attack_data.variant == "fire" then
		attack_data.damage = 10
	end

	return CopDamage.damage_fire(self, attack_data)
end

return 
