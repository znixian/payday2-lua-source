CivilianDamage = CivilianDamage or class(CopDamage)
CivilianDamage.init = function (self, unit)
	CivilianDamage.super.init(self, unit)

	self._pickup = nil

	return 
end
CivilianDamage.die = function (self, variant)
	self._unit:base():set_slot(self._unit, 17)
	self.drop_pickup(self)

	if self._unit:unit_data().mission_element then
		self._unit:unit_data().mission_element:event("death", self._unit)

		if not self._unit:unit_data().alerted_event_called then
			self._unit:unit_data().alerted_event_called = true

			self._unit:unit_data().mission_element:event("alerted", self._unit)
		end
	end

	managers.crime_spree:run_func("OnCivilianKilled", self._unit)

	if alive(managers.interaction:active_unit()) then
		managers.interaction:active_unit():interaction():selected()
	end

	variant = variant or "bullet"
	self._health = 0
	self._health_ratio = 0
	self._dead = true

	self.set_mover_collision_state(self, false)

	return 
end
CivilianDamage._on_damage_received = function (self, damage_info)
	self._call_listeners(self, damage_info)

	if damage_info.result.type == "death" then
		self._unregister_from_enemy_manager(self, damage_info)

		if Network:is_client() then
			self._unit:interaction():set_active(false, false)
		end
	end

	local attacker_unit = damage_info and damage_info.attacker_unit

	if alive(attacker_unit) and attacker_unit.base(attacker_unit) then
		if attacker_unit.base(attacker_unit).thrower_unit then
			attacker_unit = attacker_unit.base(attacker_unit):thrower_unit()
		elseif attacker_unit.base(attacker_unit).sentry_gun then
			attacker_unit = attacker_unit.base(attacker_unit):get_owner()
		end
	end

	if attacker_unit == managers.player:player_unit() and damage_info then
		managers.player:on_damage_dealt(self._unit, damage_info)
	end

	return 
end
CivilianDamage.print = function (self, ...)
	cat_print("civ_damage", ...)

	return 
end
CivilianDamage._unregister_from_enemy_manager = function (self, damage_info)
	managers.enemy:on_civilian_died(self._unit, damage_info)

	return 
end
CivilianDamage.no_intimidation_by_dmg = function (self)
	if self._ignore_intimidation_by_damage then
		return true
	end

	if self._unit and self._unit:anim_data() then
		return self._unit:anim_data().no_intimidation_by_dmg
	end

	return true
end
CivilianDamage.is_friendly_fire = function (self, unit)
	if not unit then
		return false
	end

	if unit.movement(unit):team() == self._unit:movement():team() then
		return true
	end

	local is_player = unit == managers.player:player_unit()

	if not is_player then
		return true
	end

	return false
end
CivilianDamage.damage_bullet = function (self, attack_data)
	if managers.player:has_category_upgrade("player", "civ_harmless_bullets") and self.no_intimidation_by_dmg and not self.no_intimidation_by_dmg(self) and (not self._survive_shot_t or self._survive_shot_t < TimerManager:game():time()) then
		self._survive_shot_t = TimerManager:game():time() + 2.5

		self._unit:brain():on_intimidated(1, attack_data.attacker_unit)

		return 
	end

	attack_data.damage = 10

	return CopDamage.damage_bullet(self, attack_data)
end
CivilianDamage.damage_explosion = function (self, attack_data)
	if attack_data.variant == "explosion" then
		attack_data.damage = 10
	end

	return CopDamage.damage_explosion(self, attack_data)
end
CivilianDamage.damage_fire = function (self, attack_data)
	if attack_data.variant == "fire" then
		attack_data.damage = 10
	end

	return CopDamage.damage_fire(self, attack_data)
end
CivilianDamage.stun_hit = function (self, attack_data)
	if self._dead or self._invulnerable then
		return 
	end

	if not self._lie_down_clbk_id then
		self._lie_down_clbk_id = "lie_down_" .. tostring(self._unit:key())
		local rnd = math.random()
		local t = TimerManager:game():time()

		if not self._char_tweak.is_escort then
			managers.enemy:add_delayed_clbk(self._lie_down_clbk_id, callback(self, self, "_lie_down_clbk", attack_data.attacker_unit), t + rnd)
		end
	end

	return 
end
CivilianDamage._lie_down_clbk = function (self, attacker_unit)
	local params = {
		force_lie_down = true
	}

	self._unit:brain():set_logic("surrender", params)

	self._lie_down_clbk_id = nil

	return 
end
CivilianDamage.damage_melee = function (self, attack_data)
	if managers.player:has_category_upgrade("player", "civ_harmless_melee") and self.no_intimidation_by_dmg and not self.no_intimidation_by_dmg(self) and (not self._survive_shot_t or self._survive_shot_t < TimerManager:game():time()) then
		self._survive_shot_t = TimerManager:game():time() + 2.5

		self._unit:brain():on_intimidated(1, attack_data.attacker_unit)

		return 
	end

	if _G.IS_VR and attack_data.damage == 0 then
		return 
	end

	attack_data.damage = 10

	return CopDamage.damage_melee(self, attack_data)
end
CivilianDamage.damage_tase = function (self, attack_data)
	if managers.player:has_category_upgrade("player", "civ_harmless_melee") and self.no_intimidation_by_dmg and not self.no_intimidation_by_dmg(self) and (not self._survive_shot_t or self._survive_shot_t < TimerManager:game():time()) then
		self._survive_shot_t = TimerManager:game():time() + 2.5

		self._unit:brain():on_intimidated(1, attack_data.attacker_unit)

		return 
	end

	attack_data.damage = 10

	return CopDamage.damage_tase(self, attack_data)
end

return 
