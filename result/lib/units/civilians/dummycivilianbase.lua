DummyCivilianBase = DummyCivilianBase or class()
DummyCivilianBase.init = function (self, unit)
	self._unit = unit

	unit.set_driving(unit, "animation")
	unit.set_animation_lod(unit, 1, 500000, 500, 500000)

	return 
end
DummyCivilianBase.play_state = function (self, state_name, at_time)
	local result = self._unit:play_state(Idstring(state_name), at_time)

	return result ~= Idstring("") and result
end
DummyCivilianBase.anim_clbk_spear_spawn = function (self, unit)
	self._spawn_spear(self)

	return 
end
DummyCivilianBase.anim_clbk_spear_unspawn = function (self, unit)
	self._unspawn_spear(self)

	return 
end
DummyCivilianBase._spawn_spear = function (self)
	if not alive(self._spear) then
		self._spear = World:spawn_unit(Idstring("units/test/beast/weapon/native_spear"), Vector3(), Rotation())

		self._unit:link(Idstring("a_weapon_right_front"), self._spear, self._spear:orientation_object():name())
	end

	return 
end
DummyCivilianBase._unspawn_spear = function (self)
	if alive(self._spear) then
		self._spear:set_slot(0)

		self._spear = nil
	end

	return 
end
DummyCivilianBase.destroy = function (self, unit)
	self._unspawn_spear(self)

	return 
end

return 
