ScriptUnitData = ScriptUnitData or class()
UnitBase = UnitBase or class()
UnitBase.init = function (self, unit, update_enabled)
	self._unit = unit

	if not update_enabled then
		unit.set_extension_update_enabled(unit, Idstring("base"), false)
	end

	return 
end
UnitBase.pre_destroy = function (self, unit)
	self._destroying = true

	return 
end

return 
