local access_type_walk_only = {
	walk = true
}
local access_type_all = {
	acrobatic = true,
	walk = true
}
local ignored_groups = {
	"Phalanx_minion",
	"Phalanx_vip"
}
MutatorEnemyReplacer = MutatorEnemyReplacer or class(BaseMutator)
MutatorEnemyReplacer._type = "MutatorEnemyReplacer"
MutatorEnemyReplacer.name_id = "mutator_specials_override"
MutatorEnemyReplacer.desc_id = "mutator_specials_override_desc"
MutatorEnemyReplacer.has_options = true
MutatorEnemyReplacer.reductions = {
	money = 0.5,
	exp = 0.5
}
MutatorEnemyReplacer.categories = {
	"enemies"
}
MutatorEnemyReplacer.incompatibility_tags = {
	"replaces_units"
}
MutatorEnemyReplacer.icon_coords = {
	6,
	1
}
MutatorEnemyReplacer.register_values = function (self, mutator_manager)
	self.register_value(self, "override_enemy", self.default_override_enemy(self), "oe")

	return 
end
MutatorEnemyReplacer.setup = function (self)
	self._groups = self._groups or {}
	local difficulty = (Global.game_settings and Global.game_settings.difficulty) or "normal"
	local difficulty_index = tweak_data:difficulty_to_index(difficulty)

	self.modify_unit_categories(self, tweak_data.group_ai, difficulty_index)

	return 
end
MutatorEnemyReplacer.name = function (self, lobby_data)
	local name = MutatorEnemyReplacer.super.name(self)

	if self._mutate_name(self, "override_enemy") then
		return string.format("%s - %s", name, managers.localization:text("mutator_specials_override_" .. tostring(self.value(self, "override_enemy"))))
	else
		return name
	end

	return 
end
MutatorEnemyReplacer.get_override_enemy = function (self)
	return self.value(self, "override_enemy")
end
MutatorEnemyReplacer.default_override_enemy = function (self)
	return "tank"
end
MutatorEnemyReplacer.setup_options_gui = function (self, node)
	local params = {
		callback = "_update_mutator_value",
		name = "enemy_selector_choice",
		text_id = "mutator_specials_override_select",
		filter = true,
		update_callback = callback(self, self, "_update_selected_enemy")
	}
	local data_node = {
		{
			value = "tank",
			text_id = "mutator_specials_override_tank",
			_meta = "option"
		},
		{
			value = "taser",
			text_id = "mutator_specials_override_taser",
			_meta = "option"
		},
		{
			value = "shield",
			text_id = "mutator_specials_override_shield",
			_meta = "option"
		},
		{
			value = "spooc",
			text_id = "mutator_specials_override_spooc",
			_meta = "option"
		},
		{
			value = "medic",
			text_id = "mutator_specials_override_medic",
			_meta = "option"
		},
		type = "MenuItemMultiChoice"
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, self.get_override_enemy(self))
	node.add_item(node, new_item)

	self._node = node

	return new_item
end
MutatorEnemyReplacer._update_selected_enemy = function (self, item)
	self.set_value(self, "override_enemy", item.value(item))

	return 
end
MutatorEnemyReplacer.reset_to_default = function (self)
	self.clear_values(self)

	if self._node then
		local slider = self._node:item("enemy_selector_choice")

		if slider then
			slider.set_value(slider, self.default_override_enemy(self))
		end
	end

	return 
end
MutatorEnemyReplacer.modify_unit_categories = function (self, group_ai_tweak, difficulty_index)
	for key, value in pairs(group_ai_tweak.special_unit_spawn_limits) do
		if key == self.get_override_enemy(self) then
			group_ai_tweak.special_unit_spawn_limits[key] = math.huge
		end
	end

	local unit_group = self["_get_unit_group_" .. self.get_override_enemy(self)](self, difficulty_index)

	for group, units in pairs(group_ai_tweak.unit_categories) do
		if not table.contains(ignored_groups, group) then
			print("[Mutators] Replacing unit group:", group)

			group_ai_tweak.unit_categories[group] = unit_group
		else
			print("[Mutators] Ignoring unit group:", group)
		end
	end

	return 
end
MutatorEnemyReplacer._get_unit_group_tank = function (self, difficulty_index)
	if not self._groups.tank then
		if difficulty_index < 6 then
			self._groups.tank = {
				special_type = "tank",
				unit_types = {
					america = {
						Idstring("units/payday2/characters/ene_bulldozer_1/ene_bulldozer_1"),
						Idstring("units/payday2/characters/ene_bulldozer_2/ene_bulldozer_2")
					},
					russia = {
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_saiga/ene_akan_fbi_tank_saiga"),
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_r870/ene_akan_fbi_tank_r870")
					}
				},
				access = access_type_all
			}
		elseif difficulty_index <= 7 then
			self._groups.tank = {
				special_type = "tank",
				unit_types = {
					america = {
						Idstring("units/payday2/characters/ene_bulldozer_2/ene_bulldozer_2"),
						Idstring("units/payday2/characters/ene_bulldozer_3/ene_bulldozer_3")
					},
					russia = {
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_r870/ene_akan_fbi_tank_r870"),
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_rpk_lmg/ene_akan_fbi_tank_rpk_lmg")
					}
				},
				access = access_type_all
			}
		else
			self._groups.tank = {
				special_type = "tank",
				unit_types = {
					america = {
						Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_bulldozer/ene_zeal_bulldozer"),
						Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_bulldozer_2/ene_zeal_bulldozer_2"),
						Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_bulldozer_3/ene_zeal_bulldozer_3")
					},
					russia = {
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_r870/ene_akan_fbi_tank_r870"),
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_saiga/ene_akan_fbi_tank_saiga"),
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_rpk_lmg/ene_akan_fbi_tank_rpk_lmg")
					}
				},
				access = access_type_all
			}
		end
	end

	return self._groups.tank
end
MutatorEnemyReplacer._get_unit_group_shield = function (self, difficulty_index)
	if not self._groups.shield then
		if difficulty_index <= 7 then
			self._groups.shield = {
				special_type = "shield",
				unit_types = {
					america = {
						Idstring("units/payday2/characters/ene_shield_1/ene_shield_1"),
						Idstring("units/payday2/characters/ene_shield_2/ene_shield_2")
					},
					russia = {
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_shield_sr2_smg/ene_akan_fbi_shield_sr2_smg"),
						Idstring("units/pd2_dlc_mad/characters/ene_akan_cs_shield_c45/ene_akan_cs_shield_c45")
					}
				},
				access = access_type_all
			}
		else
			self._groups.shield = {
				special_type = "shield",
				unit_types = {
					america = {
						Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_swat_shield/ene_zeal_swat_shield")
					},
					russia = {
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_shield_dw_sr2_smg/ene_akan_fbi_shield_dw_sr2_smg")
					}
				},
				access = access_type_all
			}
		end
	end

	return self._groups.shield
end
MutatorEnemyReplacer._get_unit_group_taser = function (self, difficulty_index)
	if not self._groups.taser then
		self._groups.taser = {
			special_type = "taser",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/ene_tazer_1/ene_tazer_1")
				},
				russia = {
					Idstring("units/pd2_dlc_mad/characters/ene_akan_cs_tazer_ak47_ass/ene_akan_cs_tazer_ak47_ass")
				}
			},
			access = access_type_all
		}
	end

	return self._groups.taser
end
MutatorEnemyReplacer._get_unit_group_spooc = function (self, difficulty_index)
	if not self._groups.spooc then
		if difficulty_index <= 7 then
			self._groups.spooc = {
				special_type = "spooc",
				unit_types = {
					america = {
						Idstring("units/payday2/characters/ene_spook_1/ene_spook_1")
					},
					russia = {
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_spooc_asval_smg/ene_akan_fbi_spooc_asval_smg")
					}
				},
				access = access_type_all
			}
		else
			self._groups.spooc = {
				special_type = "spooc",
				unit_types = {
					america = {
						Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_cloaker/ene_zeal_cloaker")
					},
					russia = {
						Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_spooc_asval_smg/ene_akan_fbi_spooc_asval_smg")
					}
				},
				access = access_type_all
			}
		end
	end

	return self._groups.spooc
end
MutatorEnemyReplacer._get_unit_group_medic = function (self, difficulty_index)
	if not self._groups.medic then
		self._groups.medic = {
			special_type = "medic",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/ene_medic_r870/ene_medic_r870"),
					Idstring("units/payday2/characters/ene_medic_m4/ene_medic_m4")
				},
				russia = {
					Idstring("units/payday2/characters/ene_medic_r870/ene_medic_r870"),
					Idstring("units/payday2/characters/ene_medic_m4/ene_medic_m4")
				}
			},
			access = access_type_all
		}
	end

	return self._groups.medic
end
MutatorMediDozer = MutatorMediDozer or class(BaseMutator)
MutatorMediDozer._type = "MutatorMediDozer"
MutatorMediDozer.name_id = "mutator_medidozer"
MutatorMediDozer.desc_id = "mutator_medidozer_desc"
MutatorMediDozer.reductions = {
	money = 0.25,
	exp = 0.25
}
MutatorMediDozer.categories = {
	"enemies"
}
MutatorMediDozer.incompatibility_tags = {
	"replaces_units"
}
MutatorMediDozer.icon_coords = {
	8,
	1
}
MutatorMediDozer.setup = function (self)
	self._groups = self._groups or {}
	local difficulty = (Global.game_settings and Global.game_settings.difficulty) or "normal"
	local difficulty_index = tweak_data:difficulty_to_index(difficulty)

	self.modify_unit_categories(self, tweak_data.group_ai, difficulty_index)

	return 
end
MutatorMediDozer.modify_unit_categories = function (self, group_ai_tweak, difficulty_index)
	group_ai_tweak.special_unit_spawn_limits = {
		shield = 0,
		taser = 0,
		spooc = 0,
		tank = math.huge,
		medic = math.huge
	}
	group_ai_tweak.unit_categories.medic_R870 = {
		special_type = "medic",
		unit_types = {
			america = {
				Idstring("units/payday2/characters/ene_medic_m4/ene_medic_m4"),
				Idstring("units/payday2/characters/ene_medic_m4/ene_medic_m4")
			},
			russia = {
				Idstring("units/payday2/characters/ene_medic_m4/ene_medic_m4"),
				Idstring("units/payday2/characters/ene_medic_m4/ene_medic_m4")
			}
		},
		access = access_type_all
	}
	group_ai_tweak.unit_categories.medic_R870 = {
		special_type = "medic",
		unit_types = {
			america = {
				Idstring("units/payday2/characters/ene_medic_r870/ene_medic_r870"),
				Idstring("units/payday2/characters/ene_medic_r870/ene_medic_r870")
			},
			russia = {
				Idstring("units/payday2/characters/ene_medic_r870/ene_medic_r870"),
				Idstring("units/payday2/characters/ene_medic_r870/ene_medic_r870")
			}
		},
		access = access_type_all
	}

	for group, units_data in pairs(group_ai_tweak.unit_categories) do
		if not table.contains(ignored_groups, group) then
			if units_data.special_type then
				if units_data.special_type ~= "tank" and units_data.special_type ~= "medic" then
					group_ai_tweak.unit_categories[group] = group_ai_tweak.unit_categories.FBI_tank
				end
			elseif string.find(group, "r870") then
				group_ai_tweak.unit_categories[group] = group_ai_tweak.unit_categories.medic_R870
			else
				group_ai_tweak.unit_categories[group] = group_ai_tweak.unit_categories.medic_M4
			end
		end
	end

	return 
end
MutatorTitandozers = MutatorTitandozers or class(BaseMutator)
MutatorTitandozers._type = "MutatorTitandozers"
MutatorTitandozers.name_id = "mutator_titandozers"
MutatorTitandozers.desc_id = "mutator_titandozers_desc"
MutatorTitandozers.reductions = {
	money = 0.25,
	exp = 0.25
}
MutatorTitandozers.categories = {
	"enemies"
}
MutatorTitandozers.incompatibility_tags = {}
MutatorTitandozers.icon_coords = {
	1,
	3
}
MutatorTitandozers.load_priority = -10
MutatorTitandozers.setup = function (self)
	self._groups = self._groups or {}
	local difficulty = (Global.game_settings and Global.game_settings.difficulty) or "normal"
	local difficulty_index = tweak_data:difficulty_to_index(difficulty)

	self.modify_unit_categories(self, tweak_data.group_ai, difficulty_index)

	return 
end
MutatorTitandozers.modify_unit_categories = function (self, group_ai_tweak, difficulty_index)
	group_ai_tweak.special_unit_spawn_limits.tank = math.huge
	local unit_group = self._get_unit_group_titandozer(self, difficulty_index)

	for group, units_data in pairs(group_ai_tweak.unit_categories) do
		if not table.contains(ignored_groups, group) and units_data.special_type == "tank" then
			print("[Mutators] Replacing unit group:", group)

			group_ai_tweak.unit_categories[group] = unit_group
		end
	end

	return 
end
MutatorTitandozers._get_unit_group_titandozer = function (self, difficulty_index)
	if not self._groups.tank then
		self._groups.tank = {
			special_type = "tank",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/ene_bulldozer_4/ene_bulldozer_4")
				},
				russia = {
					Idstring("units/payday2/characters/ene_bulldozer_4/ene_bulldozer_4")
				}
			},
			access = access_type_all
		}
	end

	return self._groups.tank
end

return 
