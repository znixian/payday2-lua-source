MutatorFriendlyFire = MutatorFriendlyFire or class(BaseMutator)
MutatorFriendlyFire._type = "MutatorFriendlyFire"
MutatorFriendlyFire.name_id = "mutator_friendly_fire"
MutatorFriendlyFire.desc_id = "mutator_friendly_fire_desc"
MutatorFriendlyFire.has_options = true
MutatorFriendlyFire.reductions = {
	money = 0,
	exp = 0
}
MutatorFriendlyFire.categories = {
	"gameplay"
}
MutatorFriendlyFire.icon_coords = {
	3,
	1
}
MutatorFriendlyFire.register_values = function (self, mutator_manager)
	self.register_value(self, "damage_multiplier", 1, "dm")

	return 
end
MutatorFriendlyFire.name = function (self)
	local name = MutatorFriendlyFire.super.name(self)

	if self._mutate_name(self, "damage_multiplier") then
		return string.format("%s - %.0f%%", name, tonumber(self.value(self, "damage_multiplier"))*100)
	else
		return name
	end

	return 
end
MutatorFriendlyFire.setup = function (self, mutator_manager)
	MutatorFriendlyFire.super.setup(mutator_manager)

	managers.slot._masks.bullet_impact_targets = managers.slot._masks.bullet_impact_targets_ff

	return 
end
MutatorFriendlyFire.get_friendly_fire_damage_multiplier = function (self)
	return self.value(self, "damage_multiplier")
end
MutatorFriendlyFire.modify_value = function (self, id, value)
	if id == "PlayerDamage:FriendlyFire" then
		return false
	elseif id == "HuskPlayerDamage:FriendlyFireDamage" then
		return value*self.get_friendly_fire_damage_multiplier(self)*0.25
	elseif id == "ProjectileBase:create_sweep_data:slot_mask" then
		return value + 3
	end

	return 
end
MutatorFriendlyFire._min_damage = function (self)
	return 0.25
end
MutatorFriendlyFire._max_damage = function (self)
	return 3
end
MutatorFriendlyFire.setup_options_gui = function (self, node)
	local params = {
		name = "ff_damage_slider",
		callback = "_update_mutator_value",
		text_id = "menu_mutator_ff_damage",
		update_callback = callback(self, self, "_update_damage_multiplier")
	}
	local data_node = {
		show_value = true,
		step = 0.05,
		type = "CoreMenuItemSlider.ItemSlider",
		decimal_count = 2,
		min = self._min_damage(self),
		max = self._max_damage(self)
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, self.get_friendly_fire_damage_multiplier(self))
	node.add_item(node, new_item)

	self._node = node

	return new_item
end
MutatorFriendlyFire._update_damage_multiplier = function (self, item)
	self.set_value(self, "damage_multiplier", item.value(item))

	return 
end
MutatorFriendlyFire.reset_to_default = function (self)
	self.clear_values(self)

	if self._node then
		local slider = self._node:item("ff_damage_slider")

		if slider then
			slider.set_value(slider, self.get_friendly_fire_damage_multiplier(self))
		end
	end

	return 
end
MutatorFriendlyFire.options_fill = function (self)
	return self._get_percentage_fill(self, self._min_damage(self), self._max_damage(self), self.get_friendly_fire_damage_multiplier(self))
end

return 
