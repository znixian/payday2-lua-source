MutatorShotgunTweak = MutatorShotgunTweak or class(BaseMutator)
MutatorShotgunTweak._type = "MutatorShotgunTweak"
MutatorShotgunTweak.name_id = "mutator_shotgun_tweak"
MutatorShotgunTweak.desc_id = "mutator_shotgun_tweak_desc"
MutatorShotgunTweak.has_options = true
MutatorShotgunTweak.reductions = {
	money = 0.25,
	exp = 0.25
}
MutatorShotgunTweak.icon_coords = {
	7,
	1
}
MutatorShotgunTweak.register_values = function (self, mutator_manager)
	self.register_value(self, "pull_strength", 3, "ps")
	self.register_value(self, "mothership", false, "ms")

	return 
end
MutatorShotgunTweak.setup = function (self, mutator_manager)
	mutator_manager.register_message(mutator_manager, Message.OnShotgunPush, "ShotgunTweak", callback(self, self, "_on_shotgun_push"))

	self._sound_device = SoundDevice:create_source("MutatorShotgunTweak")

	return 
end
MutatorShotgunTweak.name = function (self)
	local name = MutatorShotgunTweak.super.name(self)

	if self._mutate_name(self, "mothership") then
		return string.format("%s - %s", name, managers.localization:text("mutator_shotgun_tweak_mothership"))
	elseif self._mutate_name(self, "pull_strength") then
		return string.format("%s - %.2fx", name, tonumber(self.value(self, "pull_strength")))
	else
		return name
	end

	return 
end
MutatorShotgunTweak.get_pull_strength = function (self)
	if self.get_to_the_mothership(self) then
		return self.to_the_mothership_strength(self)
	else
		return self.value(self, "pull_strength")
	end

	return 
end
MutatorShotgunTweak.get_to_the_mothership = function (self)
	local value = self.value(self, "mothership")

	if type(value) == "table" then
		return value.current
	else
		return value
	end

	return 
end
MutatorShotgunTweak.to_the_mothership_strength = function (self)
	return 0.01
end
MutatorShotgunTweak._on_shotgun_push = function (self, unit, hit_pos, dir, distance, attacker)
	if alive(unit) and alive(attacker) then
		local str = self.get_pull_strength(self)

		World:play_physic_effect(Idstring("physic_effects/shotgun_wat"), unit.body(unit, "rag_Head"), attacker.body(attacker, "inflict_reciever"), str)
		World:play_physic_effect(Idstring("physic_effects/shotgun_wat"), unit.body(unit, "rag_Hips"), attacker.body(attacker, "inflict_reciever"), str)
		World:play_physic_effect(Idstring("physic_effects/shotgun_wat"), unit.body(unit, "rag_Spine"), attacker.body(attacker, "inflict_reciever"), str)
		World:play_physic_effect(Idstring("physic_effects/shotgun_wat"), unit.body(unit, "rag_Spine1"), attacker.body(attacker, "inflict_reciever"), str)
		World:play_physic_effect(Idstring("physic_effects/shotgun_wat"), unit.body(unit, "rag_Spine2"), attacker.body(attacker, "inflict_reciever"), str)
		World:play_physic_effect(Idstring("physic_effects/shotgun_wat"), unit.body(unit, "rag_RightForeArm"), attacker.body(attacker, "inflict_reciever"), str)
		World:play_physic_effect(Idstring("physic_effects/shotgun_wat"), unit.body(unit, "rag_LeftForeArm"), attacker.body(attacker, "inflict_reciever"), str)

		if attacker == managers.player:player_unit() and self._sound_device then
			self._sound_device:stop()
			self._sound_device:set_position(attacker.position(attacker))
			self._sound_device:set_orientation(attacker.rotation(attacker))
			self._sound_device:post_event("mutators_hfos_01")
		end
	end

	return 
end
MutatorShotgunTweak.modify_value = function (self, id, value)
	if id == "GamePlayCentralManager:get_shotgun_push_range" then
		return math.huge
	elseif id == "EnemyManager:corpse_limit" then
		if self.get_to_the_mothership(self) then
			return 4
		else
			return math.min(value, 16)
		end
	elseif id == "ShotgunBase:_fire_raycast" then
		if value and value.hit_enemy and value.hit_enemy.type == "death" then
			value.type = "death"
		end

		if value and value.variant == "explosion" then
			value.type = "death"
		end

		return value
	end

	return 
end
MutatorShotgunTweak.OnEnemyKilledByExplosion = function (self, unit, was_shotgun)
	if was_shotgun then
		self._on_shotgun_push(self, unit, nil, nil, nil, managers.player:player_unit())
	end

	return 
end
MutatorShotgunTweak._min_strength = function (self)
	return 1
end
MutatorShotgunTweak._max_strength = function (self)
	return 5
end
MutatorShotgunTweak.setup_options_gui = function (self, node)
	local params = {
		name = "pull_strength_slider",
		callback = "_update_mutator_value",
		text_id = "menu_shotgun_tweak",
		update_callback = callback(self, self, "_update_pull_strength")
	}
	local data_node = {
		show_value = true,
		step = 0.1,
		type = "CoreMenuItemSlider.ItemSlider",
		decimal_count = 2,
		min = self._min_strength(self),
		max = self._max_strength(self)
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, self.get_pull_strength(self))
	node.add_item(node, new_item)

	local params = {
		name = "mothership_toggle",
		callback = "_update_mutator_value",
		text_id = "menu_shotgun_tweak_mothership",
		update_callback = callback(self, self, "_update_mothership_toggle")
	}
	local data = {
		{
			w = 24,
			y = 0,
			h = 24,
			s_y = 24,
			value = "on",
			s_w = 24,
			s_h = 24,
			s_x = 24,
			_meta = "option",
			icon = "guis/textures/menu_tickbox",
			x = 24,
			s_icon = "guis/textures/menu_tickbox"
		},
		{
			w = 24,
			y = 0,
			h = 24,
			s_y = 24,
			value = "off",
			s_w = 24,
			s_h = 24,
			s_x = 0,
			_meta = "option",
			icon = "guis/textures/menu_tickbox",
			x = 0,
			s_icon = "guis/textures/menu_tickbox"
		},
		type = "CoreMenuItemToggle.ItemToggle"
	}
	local new_item = node.create_item(node, data, params)

	new_item.set_value(new_item, (self.get_to_the_mothership(self) and "on") or "off")
	node.add_item(node, new_item)

	self._node = node

	return new_item
end
MutatorShotgunTweak._update_pull_strength = function (self, item)
	self.set_value(self, "pull_strength", item.value(item))

	return 
end
MutatorShotgunTweak._update_mothership_toggle = function (self, item)
	local value = (item.value(item) == "on" and true) or false

	self.set_value(self, "mothership", value)

	if self._node then
		local slider = self._node:item("pull_strength_slider")

		if slider then
			if value then
				slider.set_value(slider, self.to_the_mothership_strength(self))
			end

			slider.set_enabled(slider, not value)
		end
	end

	return 
end
MutatorShotgunTweak.reset_to_default = function (self)
	self.clear_values(self)

	if self._node then
		local slider = self._node:item("pull_strength_slider")

		if slider then
			slider.set_value(slider, self.get_pull_strength(self))
			slider.set_enabled(slider, true)
		end

		local toggle = self._node:item("mothership_toggle")

		if toggle then
			toggle.set_value(toggle, (self.get_to_the_mothership(self) and "on") or "off")
		end
	end

	return 
end
MutatorShotgunTweak.options_fill = function (self)
	return self._get_percentage_fill(self, self._min_strength(self), self._max_strength(self), self.get_pull_strength(self))
end

return 
