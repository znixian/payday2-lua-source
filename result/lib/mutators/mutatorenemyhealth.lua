MutatorEnemyHealth = MutatorEnemyHealth or class(BaseMutator)
MutatorEnemyHealth._type = "MutatorEnemyHealth"
MutatorEnemyHealth.name_id = "mutator_enemy_health"
MutatorEnemyHealth.desc_id = "mutator_enemy_health_desc"
MutatorEnemyHealth.has_options = true
MutatorEnemyHealth.reductions = {
	money = 0,
	exp = 0
}
MutatorEnemyHealth.categories = {
	"enemies"
}
MutatorEnemyHealth.icon_coords = {
	4,
	1
}
MutatorEnemyHealth.register_values = function (self, mutator_manager)
	self.register_value(self, "health_multiplier", 2, "hm")

	return 
end
MutatorEnemyHealth.setup = function (self)
	self.modify_character_tweak_data(self, tweak_data.character, self.get_health_multiplier(self))

	return 
end
MutatorEnemyHealth.name = function (self)
	local name = MutatorEnemyHealth.super.name(self)

	if self._mutate_name(self, "health_multiplier") then
		return string.format("%s - %.0f%%", name, tonumber(self.value(self, "health_multiplier"))*100)
	else
		return name
	end

	return 
end
MutatorEnemyHealth.get_health_multiplier = function (self)
	return self.value(self, "health_multiplier")
end
MutatorEnemyHealth.modify_character_tweak_data = function (self, character_tweak, multiplier)
	if character_tweak then
		multiplier = multiplier or self.get_health_multiplier(self)

		print("[Mutators] Mutating character tweak data: ", self.id(self))

		for i, character in ipairs(character_tweak.enemy_list(character_tweak)) do
			if character_tweak[character] then
				print("[Mutators] Mutating health:", character, character_tweak[character].HEALTH_INIT, multiplier, character_tweak[character].HEALTH_INIT*multiplier)

				character_tweak[character].HEALTH_INIT = character_tweak[character].HEALTH_INIT*multiplier
			end
		end
	end

	return 
end
MutatorEnemyHealth._min_health = function (self)
	return 1.01
end
MutatorEnemyHealth._max_health = function (self)
	return 10
end
MutatorEnemyHealth.setup_options_gui = function (self, node)
	local params = {
		name = "enemy_health_slider",
		callback = "_update_mutator_value",
		text_id = "menu_mutator_enemy_health",
		update_callback = callback(self, self, "_update_health_multiplier")
	}
	local data_node = {
		show_value = true,
		step = 0.1,
		type = "CoreMenuItemSlider.ItemSlider",
		decimal_count = 2,
		min = self._min_health(self),
		max = self._max_health(self)
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, self.get_health_multiplier(self))
	node.add_item(node, new_item)

	self._node = node

	return new_item
end
MutatorEnemyHealth._update_health_multiplier = function (self, item)
	self.set_value(self, "health_multiplier", item.value(item))

	return 
end
MutatorEnemyHealth.reset_to_default = function (self)
	self.clear_values(self)

	if self._node then
		local slider = self._node:item("enemy_health_slider")

		if slider then
			slider.set_value(slider, self.get_health_multiplier(self))
		end
	end

	return 
end
MutatorEnemyHealth.options_fill = function (self)
	return self._get_percentage_fill(self, self._min_health(self), self._max_health(self), self.get_health_multiplier(self))
end

return 
