MutatorExplodingEnemies = MutatorExplodingEnemies or class(BaseMutator)
MutatorExplodingEnemies._type = "MutatorExplodingEnemies"
MutatorExplodingEnemies.name_id = "mutator_creeps"
MutatorExplodingEnemies.desc_id = "mutator_creeps_desc"
MutatorExplodingEnemies.has_options = true
MutatorExplodingEnemies.reductions = {
	money = 0.5,
	exp = 0.5
}
MutatorExplodingEnemies.categories = {
	"enemies",
	"gameplay"
}
MutatorExplodingEnemies.icon_coords = {
	2,
	1
}
MutatorExplodingEnemies.register_values = function (self, mutator_manager)
	self.register_value(self, "explosion_size", 4, "es")
	self.register_value(self, "nuclear_dozers", false, "nd")

	return 
end
MutatorExplodingEnemies.setup = function (self, mutator_manager)
	self._explosions = {}

	mutator_manager.register_message(mutator_manager, Message.OnCopDamageDeath, "MutatorExplodingEnemies", callback(self, self, "explode"))

	return 
end
MutatorExplodingEnemies.name = function (self)
	local name = MutatorExplodingEnemies.super.name(self)

	if self._mutate_name(self, "explosion_size") then
		name = string.format("%s - %.1fm", name, tonumber(self.value(self, "explosion_size")))
	end

	if self._mutate_name(self, "nuclear_dozers") then
		name = string.format("%s - %s", name, managers.localization:text("menu_mutator_creeps_nuclear"))
	end

	return name
end
MutatorExplodingEnemies.default_explosion_size = function (self)
	return 4
end
MutatorExplodingEnemies.get_explosion_size = function (self)
	return self.value(self, "explosion_size")
end
MutatorExplodingEnemies.use_nuclear_bulldozers = function (self)
	return self.value(self, "nuclear_dozers")
end
MutatorExplodingEnemies.explosion_delay = function (self)
	return 0
end
MutatorExplodingEnemies._min_explosion_size = function (self)
	return 2
end
MutatorExplodingEnemies._max_explosion_size = function (self)
	return 6
end
MutatorExplodingEnemies.setup_options_gui = function (self, node)
	local params = {
		name = "explosion_slider",
		callback = "_update_mutator_value",
		text_id = "menu_mutator_creeps_scale",
		update_callback = callback(self, self, "_update_explosion_size")
	}
	local data_node = {
		show_value = true,
		step = 0.5,
		type = "CoreMenuItemSlider.ItemSlider",
		decimal_count = 1,
		min = self._min_explosion_size(self),
		max = self._max_explosion_size(self)
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, self.get_explosion_size(self))
	node.add_item(node, new_item)

	local params = {
		name = "nuclear_dozers_toggle",
		callback = "_update_mutator_value",
		text_id = "menu_mutator_creeps_nuclear",
		update_callback = callback(self, self, "_toggle_nuclear_bulldozers")
	}
	local data_node = {
		{
			w = 24,
			y = 0,
			h = 24,
			s_y = 24,
			value = "on",
			s_w = 24,
			s_h = 24,
			s_x = 24,
			_meta = "option",
			icon = "guis/textures/menu_tickbox",
			x = 24,
			s_icon = "guis/textures/menu_tickbox"
		},
		{
			w = 24,
			y = 0,
			h = 24,
			s_y = 24,
			value = "off",
			s_w = 24,
			s_h = 24,
			s_x = 0,
			_meta = "option",
			icon = "guis/textures/menu_tickbox",
			x = 0,
			s_icon = "guis/textures/menu_tickbox"
		},
		type = "CoreMenuItemToggle.ItemToggle"
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, (self.use_nuclear_bulldozers(self) and "on") or "off")
	node.add_item(node, new_item)

	self._node = node

	return new_item
end
MutatorExplodingEnemies._update_explosion_size = function (self, item)
	self.set_value(self, "explosion_size", item.value(item))

	return 
end
MutatorExplodingEnemies._toggle_nuclear_bulldozers = function (self, item)
	self.set_value(self, "nuclear_dozers", item.value(item) == "on")

	return 
end
MutatorExplodingEnemies.reset_to_default = function (self)
	self.clear_values(self)

	if self._node then
		local slider = self._node:item("explosion_slider")

		if slider then
			slider.set_value(slider, self.get_explosion_size(self))
		end

		local toggle = self._node:item("nuclear_dozers_toggle")

		if toggle then
			toggle.set_value(toggle, (self.use_nuclear_bulldozers(self) and "on") or "off")
		end
	end

	return 
end
MutatorExplodingEnemies.options_fill = function (self)
	return self._get_percentage_fill(self, self._min_explosion_size(self), self._max_explosion_size(self), self.get_explosion_size(self))
end
MutatorExplodingEnemies.update = function (self, t, dt)
	for i = #self._explosions, 1, -1 do
		local data = self._explosions[i]
		data.t = data.t - dt

		if data.t < 0 then
			self._detonate(self, data.cop_damage, data.attack_data)
			table.remove(self._explosions, i)
		end
	end

	return 
end
MutatorExplodingEnemies.explode = function (self, cop_damage, attack_data)
	if not Network:is_server() then
		return 
	end

	if self.explosion_delay(self) <= 0 then
		self._detonate(self, cop_damage, attack_data)
	else
		table.insert(self._explosions, {
			cop_damage = cop_damage,
			attack_data = attack_data,
			t = self.explosion_delay(self)
		})
	end

	return 
end
MutatorExplodingEnemies._detonate = function (self, cop_damage, attack_data)
	if Network:is_server() then
		local pos = attack_data.pos

		if (0 < self.explosion_delay(self) or not pos) and alive(cop_damage._unit) then
			pos = cop_damage._unit:get_object(Idstring("Spine2")):position() or pos
		end

		local range = self.get_explosion_size(self)*100
		local damage = attack_data.raw_damage or attack_data.damage or cop_damage._HEALTH_INIT
		local ply_damage = damage*0.5
		local normal = attack_data.attack_dir or math.UP
		local slot_mask = managers.slot:get_mask("explosion_targets")
		local curve_pow = 4
		local unit_tweak = cop_damage._unit:base()._tweak_table

		if unit_tweak and unit_tweak == "tank" and self.use_nuclear_bulldozers(self) then
			range = 2000
			damage = damage*2.5
			ply_damage = damage*0.5
			curve_pow = 6
		end

		local damage_params = {
			no_raycast_check_characters = false,
			hit_pos = pos,
			range = range,
			collision_slotmask = slot_mask,
			curve_pow = curve_pow,
			damage = damage,
			player_damage = ply_damage,
			ignore_unit = cop_damage._unit,
			user = attack_data.attacker_unit
		}
		local effect_params = {
			sound_event = "grenade_explode",
			effect = "effects/payday2/particles/explosions/grenade_explosion",
			camera_shake_max_mul = 4,
			sound_muffle_effect = true,
			feedback_range = range*2
		}

		managers.explosion:give_local_player_dmg(pos, range, ply_damage)
		managers.explosion:play_sound_and_effects(pos, normal, range, effect_params)
		managers.explosion:detect_and_give_dmg(damage_params)
		managers.network:session():send_to_peers_synched("sync_explosion_to_client", attack_data.attacker_unit, pos, normal, ply_damage, range, curve_pow)
	end

	return 
end

return 
