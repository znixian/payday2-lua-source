MutatorEnemyDamage = MutatorEnemyDamage or class(BaseMutator)
MutatorEnemyDamage._type = "MutatorEnemyDamage"
MutatorEnemyDamage.name_id = "mutator_enemy_damage"
MutatorEnemyDamage.desc_id = "mutator_enemy_damage_desc"
MutatorEnemyDamage.has_options = true
MutatorEnemyDamage.reductions = {
	money = 0,
	exp = 0
}
MutatorEnemyDamage.categories = {
	"enemies"
}
MutatorEnemyDamage.icon_coords = {
	5,
	1
}
MutatorEnemyDamage.register_values = function (self, mutator_manager)
	self.register_value(self, "damage_multiplier", 2, "dm")

	return 
end
MutatorEnemyDamage.name = function (self)
	local name = MutatorEnemyHealth.super.name(self)

	if self._mutate_name(self, "damage_multiplier") then
		return string.format("%s - %.0f%%", name, tonumber(self.value(self, "damage_multiplier"))*100)
	else
		return name
	end

	return 
end
MutatorEnemyDamage.get_damage_multiplier = function (self)
	return self.value(self, "damage_multiplier")
end
MutatorEnemyDamage.modify_value = function (self, id, value)
	if id == "PlayerDamage:TakeDamageBullet" then
		return value*self.get_damage_multiplier(self)
	end

	return value
end
MutatorEnemyDamage._min_damage = function (self)
	return 1.01
end
MutatorEnemyDamage._max_damage = function (self)
	return 10
end
MutatorEnemyDamage.setup_options_gui = function (self, node)
	local params = {
		name = "enemy_damage_slider",
		callback = "_update_mutator_value",
		text_id = "menu_mutator_enemy_damage",
		update_callback = callback(self, self, "_update_damage_multiplier")
	}
	local data_node = {
		show_value = true,
		step = 0.1,
		type = "CoreMenuItemSlider.ItemSlider",
		decimal_count = 2,
		min = self._min_damage(self),
		max = self._max_damage(self)
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, self.get_damage_multiplier(self))
	node.add_item(node, new_item)

	self._node = node

	return new_item
end
MutatorEnemyDamage._update_damage_multiplier = function (self, item)
	self.set_value(self, "damage_multiplier", item.value(item))

	return 
end
MutatorEnemyDamage.reset_to_default = function (self)
	self.clear_values(self)

	if self._node then
		local slider = self._node:item("enemy_damage_slider")

		if slider then
			slider.set_value(slider, self.get_damage_multiplier(self))
		end
	end

	return 
end
MutatorEnemyDamage.options_fill = function (self)
	return self._get_percentage_fill(self, self._min_damage(self), self._max_damage(self), self.get_damage_multiplier(self))
end

return 
