MutatorCloakerEffect = MutatorCloakerEffect or class(BaseMutator)
MutatorCloakerEffect._type = "MutatorCloakerEffect"
MutatorCloakerEffect.name_id = "mutator_cloaker_effect"
MutatorCloakerEffect.desc_id = "mutator_cloaker_effect_desc"
MutatorCloakerEffect.has_options = true
MutatorCloakerEffect.reductions = {
	money = 0.25,
	exp = 0.25
}
MutatorCloakerEffect.categories = {
	"enemies"
}
MutatorCloakerEffect.icon_coords = {
	2,
	2
}
MutatorCloakerEffect.register_values = function (self, mutator_manager)
	self.register_value(self, "kick_effect", "explode", "ke")

	return 
end
MutatorCloakerEffect.name = function (self, lobby_data)
	local name = MutatorCloakerEffect.super.name(self)

	if self._mutate_name(self, "kick_effect") then
		return string.format("%s - %s", name, managers.localization:text("menu_mutator_cloaker_effect_" .. tostring(self.value(self, "kick_effect"))))
	else
		return name
	end

	return 
end
MutatorCloakerEffect.kick_effect = function (self)
	return self.value(self, "kick_effect")
end
MutatorCloakerEffect.setup_options_gui = function (self, node)
	local params = {
		callback = "_update_mutator_value",
		name = "effect_selector_choice",
		text_id = "menu_mutator_cloaker_effect",
		filter = true,
		update_callback = callback(self, self, "_update_selected_effect")
	}
	local data_node = {
		{
			value = "explode",
			text_id = "menu_mutator_cloaker_effect_explode",
			_meta = "option"
		},
		{
			value = "fire",
			text_id = "menu_mutator_cloaker_effect_fire",
			_meta = "option"
		},
		{
			value = "smoke",
			text_id = "menu_mutator_cloaker_effect_smoke",
			_meta = "option"
		},
		{
			value = "random",
			text_id = "menu_mutator_cloaker_effect_random",
			_meta = "option"
		},
		type = "MenuItemMultiChoice"
	}
	local new_item = node.create_item(node, data_node, params)

	new_item.set_value(new_item, self.kick_effect(self))
	node.add_item(node, new_item)

	self._node = node

	return new_item
end
MutatorCloakerEffect._update_selected_effect = function (self, item)
	self.set_value(self, "kick_effect", item.value(item))

	return 
end
MutatorCloakerEffect.reset_to_default = function (self)
	self.clear_values(self)

	if self._node then
		local slider = self._node:item("effect_selector_choice")

		if slider then
			slider.set_value(slider, self.kick_effect(self))
		end
	end

	return 
end
MutatorCloakerEffect.OnPlayerCloakerKicked = function (self, cloaker_unit)
	local effect_func = MutatorCloakerEffect["effect_" .. tostring(self.kick_effect(self))]

	if effect_func then
		effect_func(self, cloaker_unit)
	end

	return 
end
MutatorCloakerEffect.effect_smoke = function (self, unit)
	local smoke_grenade = World:spawn_unit(Idstring("units/weapons/smoke_grenade_quick/smoke_grenade_quick"), unit.position(unit), unit.rotation(unit))

	smoke_grenade.base(smoke_grenade):activate_immediately(unit.position(unit), tweak_data.group_ai.smoke_grenade_lifetime)

	return 
end
MutatorCloakerEffect.effect_fire = function (self, unit)
	local position = unit.position(unit)
	local rotation = unit.rotation(unit)
	local data = self.cloaker_fire_large(self)

	if managers.mutators:is_mutator_active(MutatorEnemyReplacer) then
		data = self.cloaker_fire_small(self)
	end

	EnvironmentFire.spawn(position, rotation, data, math.UP, unit, 0, 1)

	return 
end
MutatorCloakerEffect.effect_explode = function (self, unit)
	local foot = unit.get_object(unit, Idstring("RightFoot"))
	local pos = (foot and foot.position(foot)) or unit.position(unit)
	local range = 800
	local damage = 1000
	local ply_damage = 100
	local normal = math.UP
	local slot_mask = managers.slot:get_mask("explosion_targets")
	local curve_pow = 3
	local damage_params = {
		hit_pos = pos,
		range = range,
		collision_slotmask = slot_mask,
		curve_pow = curve_pow,
		damage = damage,
		player_damage = ply_damage
	}
	local effect_params = {
		sound_event = "grenade_explode",
		effect = "effects/payday2/particles/explosions/grenade_explosion",
		camera_shake_max_mul = 4,
		sound_muffle_effect = true,
		feedback_range = range*2
	}

	managers.explosion:give_local_player_dmg(pos, range, ply_damage)
	managers.explosion:play_sound_and_effects(pos, normal, range, effect_params)

	if Network:is_server() then
		managers.explosion:detect_and_give_dmg(damage_params)
		managers.network:session():send_to_peers_synched("sync_explosion_to_client", unit, pos, normal, ply_damage, range, curve_pow)
	end

	return 
end
MutatorCloakerEffect.random_effects = {
	"effect_smoke",
	"effect_fire",
	"effect_explode"
}
MutatorCloakerEffect.effect_random = function (self, unit)
	local len = #self.random_effects
	local rand = math.clamp(unit.id(unit)%len + 1, 1, len)
	local effect = self.random_effects[rand]

	self[effect](self, unit)

	return 
end
MutatorCloakerEffect.cloaker_fire_large = function (self)
	local params = {
		sound_event = "molotov_impact",
		range = 75,
		curve_pow = 3,
		damage = 1,
		fire_alert_radius = 1500,
		hexes = 6,
		sound_event_burning = "burn_loop_gen",
		is_molotov = true,
		player_damage = 2,
		sound_event_impact_duration = 4,
		burn_tick_period = 0.5,
		burn_duration = 15,
		alert_radius = 1500,
		effect_name = "effects/payday2/particles/explosions/molotov_grenade",
		fire_dot_data = {
			dot_trigger_chance = 35,
			dot_damage = 15,
			dot_length = 6,
			dot_trigger_max_distance = 3000,
			dot_tick_period = 0.5
		}
	}

	return params
end
MutatorCloakerEffect.cloaker_fire_small = function (self)
	local params = {
		sound_event = "molotov_impact",
		range = 75,
		curve_pow = 3,
		damage = 1,
		fire_alert_radius = 1500,
		hexes = 2,
		sound_event_burning = "burn_loop_gen",
		is_molotov = true,
		player_damage = 2,
		sound_event_impact_duration = 4,
		burn_tick_period = 0.5,
		burn_duration = 15,
		alert_radius = 1500,
		effect_name = "effects/payday2/particles/explosions/molotov_grenade",
		fire_dot_data = {
			dot_trigger_chance = 35,
			dot_damage = 15,
			dot_length = 6,
			dot_trigger_max_distance = 3000,
			dot_tick_period = 0.5
		}
	}

	return params
end

return 
