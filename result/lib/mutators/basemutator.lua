BaseMutator = BaseMutator or class()
BaseMutator._type = "BaseMutator"
BaseMutator.name_id = ""
BaseMutator.desc_id = ""
BaseMutator.has_options = false
BaseMutator.categories = {}
BaseMutator.incompatiblities = {}
BaseMutator.incompatibility_tags = {}
BaseMutator.icon_coords = {
	1,
	1
}
BaseMutator.load_priority = 0
BaseMutator.reductions = {
	money = 0,
	exp = 0
}
BaseMutator.disables_achievements = true
BaseMutator.init = function (self, mutator_manager)
	self._enabled = false
	self._values = {}

	self.register_values(self)

	local mutator_data = Global.mutators and Global.mutators.mutator_values and Global.mutators.mutator_values[self.id(self)]

	if mutator_data then
		for key, value in pairs(mutator_data.values) do
			self.set_value(self, key, value)
		end

		self._enabled = mutator_data.enabled
	end

	return 
end
BaseMutator.register_values = function (self, mutator_manager)
	return 
end
BaseMutator.setup = function (self, mutator_manager)
	return 
end
BaseMutator._ensure_global_values = function (self)
	Global.mutators.mutator_values[self.id(self)] = Global.mutators.mutator_values[self.id(self)] or {
		values = {},
		enabled = self._enabled
	}

	return 
end
BaseMutator.id = function (self)
	return self._type
end
BaseMutator.name = function (self)
	return managers.localization:text(self.name_id)
end
BaseMutator.desc = function (self)
	return managers.localization:text(self.desc_id)
end
BaseMutator.longdesc = function (self)
	if not self.longdesc_id then
		self.longdesc_id = string.gsub(self.desc_id, "_desc", "_longdesc")
	end

	return managers.localization:text(self.longdesc_id)
end
BaseMutator.icon = function (self)
	local x = self.icon_coords[1]
	local y = self.icon_coords[2]
	local size = MutatorsManager._icon_size

	return MutatorsManager._atlas_file, {
		size*(x - 1),
		size*(y - 1),
		size,
		size
	}
end
BaseMutator.is_compatible_with = function (self, mutator)
	for i, mutator_id in ipairs(self.incompatiblities) do
		if mutator.id(mutator) == mutator_id then
			return false
		end
	end

	for i, tag in ipairs(self.incompatibility_tags) do
		if table.contains(mutator.incompatibility_tags, tag) then
			return false
		end
	end

	return true
end
BaseMutator.is_incompatible_with = function (self, mutator)
	return not self.is_compatible_with(self, mutator) or not mutator.is_compatible_with(mutator, self)
end
BaseMutator.is_enabled = function (self)
	return self._enabled
end
BaseMutator.set_enabled = function (self, enable)
	self._enabled = enable

	self._ensure_global_values(self)

	Global.mutators.mutator_values[self.id(self)].enabled = enable

	return 
end
BaseMutator.is_active = function (self)
	if managers.mutators then
		for i, active_mutator in ipairs(managers.mutators:active_mutators()) do
			if active_mutator.mutator and active_mutator.mutator:id() == self.id(self) then
				return true
			end
		end
	end

	return false
end
BaseMutator.get_cash_reduction = function (self)
	return self.reductions.money
end
BaseMutator.get_experience_reduction = function (self)
	return self.reductions.exp
end
BaseMutator.update = function (self, t, dt)
	return 
end
BaseMutator._mutate_name = function (self, key)
	if Global.game_settings.single_player then
		return self.is_enabled(self) and self.value(self, key)
	elseif Network:is_server() then
		return self.is_enabled(self) and self.value(self, key)
	elseif Network:is_client() then
		return self.value(self, key)
	elseif managers.mutators:crimenet_lobby_data() then
		return self.value(self, key)
	else
		return self.is_enabled(self) and self.value(self, key)
	end

	return 
end
BaseMutator.show_options = function (self)
	return self.has_options
end
BaseMutator.setup_options_gui = function (self, node)
	local params = {
		name = "default_item",
		localize = false,
		text_id = "No options!",
		align = "right",
		disabled_color = tweak_data.screen_colors.important_1
	}
	local data_node = {}
	local new_item = node.create_item(node, data_node, params)

	node.add_item(node, new_item)

	return new_item
end
BaseMutator.reset_to_default = function (self)
	return 
end
BaseMutator.options_fill = function (self)
	return 0
end
BaseMutator._get_percentage_fill = function (self, min, max, current)
	return math.clamp(((current or 0) - min)/(max - min), 0, 1)
end
BaseMutator.clear_values = function (self)
	for id, data in pairs(self._values) do
		data.current = data.default
	end

	return 
end
BaseMutator.values = function (self)
	return self._values
end
BaseMutator.register_value = function (self, key, default, network_key)
	if not network_key then
		network_key = key
		local splits = string.split(key, "[_]")

		if splits then
			network_key = ""

			for _, str in ipairs(splits) do
				network_key = network_key .. tostring(str[1])
			end
		end
	end

	self._values[key] = {
		current = default,
		default = default,
		network_key = network_key
	}

	return 
end
BaseMutator.set_value = function (self, id, value)
	if not self._values[id] then
		Application:error(string.format("Can not set a value for a key that has not been registered! %s: %s", self.id(self), id))

		return 
	end

	self._values[id].current = value

	self._ensure_global_values(self)

	Global.mutators.mutator_values[self.id(self)].values[id] = value

	return 
end
BaseMutator.set_host_value = function (self, id, value)
	if not self._values[id] then
		Application:error(string.format("Can not set a value for a key that has not been registered! %s: %s", self.id(self), id))

		return 
	end

	self._values[id].host = value

	return 
end
BaseMutator.value = function (self, id)
	if not self._values[id] then
		Application:error(string.format("Can not get a value for a key that has not been registered! %s: %s", self.id(self), id))

		return nil
	end

	local value = self._get_value(self, self._values, id)
	local ret_value = value.current

	if Network:is_client() then
		self._apply_host_values(self, managers.mutators:get_mutators_from_lobby_data())

		ret_value = (value.host == nil and value.client) or value.host
	end

	if managers.mutators:crimenet_lobby_data() then
		self._apply_host_values(self, managers.mutators:crimenet_lobby_data())

		ret_value = (value.host == nil and value.client) or value.host
	end

	if ret_value == nil then
		ret_value = value.default
	end

	return ret_value
end
BaseMutator._get_value = function (self, table, id, default)
	local value = table[id]

	if value == nil then
		value = default
	end

	return value
end
BaseMutator._apply_host_values = function (self, host_mutators)
	if host_mutators and host_mutators[self.id(self)] then
		for key, value in pairs(host_mutators[self.id(self)]) do
			self.set_host_value(self, key, value)
		end
	end

	return 
end
BaseMutator.build_matchmaking_key = function (self)
	local matchmaking_key = string.format("%s ", self.id(self))

	if 0 < table.size(self.values(self)) then
		for key, data in pairs(self.values(self)) do
			local value = data.current

			if type(value) == "number" then
				matchmaking_key = matchmaking_key .. string.format("%s %.4f ", data.network_key, value)
			else
				matchmaking_key = matchmaking_key .. string.format("%s %s ", data.network_key, tostring(value))
			end
		end
	end

	return matchmaking_key
end
BaseMutator.get_data_from_attribute_string = function (self, string_table)
	if 0 < #string_table and #string_table%2 ~= 0 then
		Application:error("Warning! Mismatched attribute string table, should have an even amount of elements!", self.id(self))
		print(inspect(string_table))

		return nil
	end

	local data = {}

	for i = 1, #string_table, 2 do
		local key = string_table[i]
		local value = string.trim(string_table[i + 1])

		for id, data in pairs(self._values) do
			if key == data.network_key then
				key = id

				break
			end
		end

		if value == "true" or value == "false" then
			data[key] = toboolean(string_table[i + 1])
		else
			local number = tonumber(string_table[i + 1])

			if number == nil then
				data[key] = string_table[i + 1]
			else
				data[key] = number
			end
		end
	end

	return data
end
BaseMutator.modify_character_tweak_data = function (self, character_tweak)
	return 
end
BaseMutator.modify_tweak_data = function (self, id, value)
	return 
end
BaseMutator.modify_value = function (self, id, value)
	return value
end

return 
