require("core/lib/utils/dev/ews/CoreTableEditorPanel")
require("core/lib/utils/dev/tools/cutscene_editor/CoreCutsceneEditorProject")

CoreCutsceneProjectMappingPanel = CoreCutsceneProjectMappingPanel or class(CoreTableEditorPanel)
CoreCutsceneProjectMappingPanel.init = function (self, parent)
	self.super.init(self, parent)
	self.freeze(self)
	self.add_column(self, "Project")
	self.add_column(self, "Output")
	self.thaw(self)

	return 
end
CoreCutsceneProjectMappingPanel.projects = function (self)
	return managers.database:list_entries_of_type("cutscene_project")
end
CoreCutsceneProjectMappingPanel.mappings = function (self)
	local mappings = {}
	local list_ctrl = self.__list_ctrl
	local row_count = list_ctrl.item_count(list_ctrl)

	for row = 0, row_count - 1, 1 do
		local project = list_ctrl.get_item(list_ctrl, row, 0)
		local output = list_ctrl.get_item(list_ctrl, row, 1)
		mappings[project] = output
	end

	return mappings
end
CoreCutsceneProjectMappingPanel.set_mappings = function (self, mappings, project_sort_func)
	self.freeze(self)
	self.clear(self)

	if mappings then
		for _, project in ipairs(table.map_keys(mappings, project_sort_func)) do
			local output = mappings[project]

			self.add_item(self, project, output)
		end
	end

	self.__list_ctrl:autosize_column(0)
	self.__list_ctrl:autosize_column(1)
	self.thaw(self)

	return 
end
CoreCutsceneProjectMappingPanel._sizer_with_editable_fields = function (self, parent)
	local sizer = EWS:BoxSizer("VERTICAL")
	local project_enabled = self.selected_item(self) ~= nil and not table.empty(self.projects(self))
	local project_label = EWS:StaticText(parent, "Project:")

	project_label.set_enabled(project_label, project_enabled)
	sizer.add(sizer, project_label, 0, 5, "TOP,LEFT,RIGHT")

	local project_dropdown = self._create_project_dropdown(self, parent)

	project_dropdown.set_enabled(project_dropdown, project_enabled)
	sizer.add(sizer, project_dropdown, 0, 5, "ALL,EXPAND")
	self._create_labeled_text_field(self, "Output", parent, sizer)

	if project_enabled then
		project_dropdown.set_focus(project_dropdown)
	end

	return sizer
end
CoreCutsceneProjectMappingPanel._create_project_dropdown = function (self, parent)
	local value = self.selected_item_value(self, "Project")
	local control = EWS:ComboBox(parent, "", "", "CB_DROPDOWN,CB_READONLY,CB_SORT")

	control.freeze(control)

	local first_value = nil

	for _, project in pairs(self.projects(self)) do
		first_value = first_value or project

		control.append(control, project)
	end

	if value then
		control.set_value(control, value)
	end

	control.set_min_size(control, control.get_min_size(control):with_x(0))
	control.connect(control, "EVT_COMMAND_COMBOBOX_SELECTED", self._make_control_edited_callback(self, control, "Project"))
	control.thaw(control)

	return control
end
CoreCutsceneProjectMappingPanel._refresh_buttons_panel = function (self)
	self.super._refresh_buttons_panel(self)
	self.__add_button:set_enabled(not table.empty(self.projects(self)))

	return 
end

return 
