CoreCutsceneClipMetadata = CoreCutsceneClipMetadata or class()
CoreCutsceneClipMetadata.init = function (self, footage, camera)
	self._footage = footage
	self._camera = camera

	return 
end
CoreCutsceneClipMetadata.is_valid = function (self)
	return self.camera_index(self) ~= nil
end
CoreCutsceneClipMetadata.footage = function (self)
	return self._footage
end
CoreCutsceneClipMetadata.camera = function (self)
	return self._camera
end
CoreCutsceneClipMetadata.set_camera = function (self, camera)
	self._camera = camera

	return 
end
CoreCutsceneClipMetadata.camera_index = function (self)
	return (self.footage(self) and table.get_vector_index(self.footage(self):camera_names(), self.camera(self))) or nil
end
CoreCutsceneClipMetadata.camera_icon_image = function (self)
	local icon_index = 0

	if self.footage(self) and self.camera(self) then
		icon_index = self.footage(self):camera_icon_index(self.camera(self))
	end

	return CoreEWS.image_path(string.format("sequencer\\clip_icon_camera_%02i.bmp", icon_index))
end
CoreCutsceneClipMetadata.camera_watermark = function (self)
	if self.footage(self) and self.camera(self) then
		local name_without_prefix = string.match(self.camera(self), "camera_(.+)")
		local as_number = tonumber(name_without_prefix)

		if not as_number or not tostring(as_number) then
			slot3 = string.upper(name_without_prefix or "camera")
		end

		return slot3, 12, "ALIGN_CENTER_HORIZONTAL,ALIGN_CENTER_VERTICAL", Vector3(0, -2)
	end

	return nil
end
CoreCutsceneClipMetadata.prime_cast = function (self, cast)
	self.footage(self):prime_cast(cast)

	return 
end

return 
