require("core/lib/utils/dev/tools/cutscene_editor/CoreCutsceneClipMetadata")
require("core/lib/managers/cutscene/CoreCutsceneKeys")

local CAMERA_ICON_IMAGE_COUNT = 30
CoreCutsceneFootage = CoreCutsceneFootage or class()
CoreCutsceneFootage.init = function (self, cutscene)
	self._cutscene = cutscene

	return 
end
CoreCutsceneFootage.name = function (self)
	return self._cutscene:name()
end
CoreCutsceneFootage.controlled_unit_types = function (self)
	return self._cutscene:controlled_unit_types()
end
CoreCutsceneFootage.camera_names = function (self)
	return self._cutscene:camera_names()
end
CoreCutsceneFootage.keys = function (self)
	return self._cutscene:_all_keys_sorted_by_time()
end
CoreCutsceneFootage.add_clips_to_track = function (self, track, time)
	time = time or 0

	track.freeze(track)

	for start_frame, end_frame, camera in self._camera_cuts(self) do
		track.add_clip(track, self.create_clip(self, start_frame, end_frame, camera))
	end

	track.thaw(track)

	return 
end
CoreCutsceneFootage.add_cameras_to_list_ctrl = function (self, list_ctrl)
	list_ctrl.freeze(list_ctrl)
	list_ctrl.delete_all_items(list_ctrl)

	for _, camera_name in ipairs(self.camera_names(self)) do
		local item = list_ctrl.append_item(list_ctrl, camera_name)
		local icon_index = self.camera_icon_index(self, camera_name, list_ctrl.image_count(list_ctrl))

		list_ctrl.set_item_image(list_ctrl, item, icon_index)
	end

	list_ctrl.thaw(list_ctrl)

	return 
end
CoreCutsceneFootage.create_clip = function (self, start_frame, end_frame, camera)
	local clip = EWS:SequencerCroppedClip()
	local metadata = core_or_local("CutsceneClipMetadata", self, camera)

	clip.set_metadata(clip, metadata)
	clip.set_icon_bitmap(clip, metadata.camera_icon_image(metadata))
	clip.set_watermark(clip, metadata.camera_watermark(metadata))
	clip.set_range(clip, start_frame, end_frame)
	clip.set_uncropped_range(clip, 0, self._cutscene:frame_count())
	clip.set_colour(clip, self.colour(self):unpack())

	return clip
end
CoreCutsceneFootage.frame_count = function (self)
	return self._cutscene:frame_count()
end
CoreCutsceneFootage.colour = function (self)
	if self._colour == nil then
		local precision = 255
		local r = 26
		local g = 52
		local b = 78
		local name = self._cutscene:name()
		local len = string.len(name)

		for i = 1, len, 1 do
			local byte = string.byte(name, i)
			r = math.fmod(r*33 + byte, precision + 1)
			g = math.fmod(g*33 + byte, precision + 1)
			b = math.fmod(b*33 + byte, precision + 1)
		end

		local black_value = 0.7
		local divisor = precision*(black_value - 1)/1
		self._colour = Color(black_value + r/divisor, black_value + g/divisor, black_value + b/divisor)
	end

	return self._colour
end
CoreCutsceneFootage.camera_icon_index = function (self, camera_name, image_count)
	image_count = image_count or CAMERA_ICON_IMAGE_COUNT + 1
	local name_without_prefix = string.match(camera_name, "camera_(.+)")
	local icon_index = tonumber(name_without_prefix) or table.get_vector_index(self.camera_names(self), camera_name) or 0

	if image_count <= icon_index then
		icon_index = 0
	end

	return icon_index
end
CoreCutsceneFootage._camera_cuts = function (self)
	local cuts = self._camera_cut_list(self)
	local index = 0

	return function ()
		index = index + 1

		return unpack(cuts[index] or {})
	end
end
CoreCutsceneFootage._camera_cut_list = function (self)
	if self._camera_cut_cache == nil then
		self._camera_cut_cache = {}

		if self._cutscene:has_cameras() then
			local function add_camera_cut(start_frame, end_frame, camera)
				if start_frame < end_frame then
					table.insert(self._camera_cut_cache, {
						start_frame,
						end_frame,
						camera
					})
				end

				return 
			end

			local previous_key = responder_map({
				frame = 0,
				camera = self._cutscene:default_camera()
			})

			for key in self._cutscene:keys(CoreChangeCameraCutsceneKey.ELEMENT_NAME) do
				add_camera_cut(previous_key.frame(previous_key), key.frame(key), previous_key.camera(previous_key))

				previous_key = key
			end

			add_camera_cut(previous_key.frame(previous_key), self._cutscene:frame_count(), previous_key.camera(previous_key))
		end
	end

	return self._camera_cut_cache
end
CoreCutsceneFootage.prime_cast = function (self, cast)
	cast.prime(cast, self._cutscene)

	return 
end

return 
