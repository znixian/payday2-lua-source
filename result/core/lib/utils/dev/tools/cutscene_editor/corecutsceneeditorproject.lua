CoreCutsceneEditorProject = CoreCutsceneEditorProject or class()
CoreCutsceneEditorProject.VALID_EXPORT_TYPES = {
	"in_game_use",
	"footage_use"
}
CoreCutsceneEditorProject.DEFAULT_EXPORT_TYPE = "in_game_use"
CoreCutsceneEditorProject.path = function (self)
	return self._path
end
CoreCutsceneEditorProject.set_path = function (self, absolute_path)
	self._path = assert(absolute_path, "Must supply a valid path.")
	self._root_node = nil

	return 
end
CoreCutsceneEditorProject.name = function (self)
	return (self.path(self) and managers.database:entry_name(self.path(self))) or "untitled"
end
CoreCutsceneEditorProject.export_type = function (self)
	local settings_node = self.child_node(self, "settings") or responder(function ()
		return 
	end)
	local export_type_node = self.child_node(self, "export_type", settings_node) or responder(nil)

	return export_type_node.parameter(export_type_node, "value") or self.DEFAULT_EXPORT_TYPE
end
CoreCutsceneEditorProject.save = function (self, audio_clips, film_clips, cutscene_keys, settings)
	assert(self.path(self), "A valid path has not been assigned. Call the set_path() method prior to saving.")

	self._root_node = nil

	local function is_valid(member)
		return member ~= nil and member ~= ""
	end

	local project_node = Node("cutscene_project")
	local settings_node = project_node.make_child(project_node, "settings")
	local export_type_node = settings_node.make_child(settings_node, "export_type")

	if type(settings.export_type) == "string" then
		assert(table.contains(self.VALID_EXPORT_TYPES, settings.export_type))
	end

	export_type_node.set_parameter(export_type_node, "value", settings.export_type or self.DEFAULT_EXPORT_TYPE)

	local animation_patches_node = settings_node.make_child(settings_node, "animation_patches")
	slot10 = pairs
	slot11 = settings.animation_patches or {}

	for actor_name, patches in slot10(slot11) do
		for blend_set, animation in pairs(patches) do
			if is_valid(blend_set) and is_valid(animation) then
				local actor_node = self.child_node(self, "actor", animation_patches_node, {
					name = tostring(actor_name)
				}) or animation_patches_node.make_child(animation_patches_node, "actor")

				actor_node.set_parameter(actor_node, "name", tostring(actor_name))

				local override_node = actor_node.make_child(actor_node, "override")

				override_node.set_parameter(override_node, "blend_set", tostring(blend_set))
				override_node.set_parameter(override_node, "animation", tostring(animation))
			end
		end
	end

	local audio_track_node = project_node.make_child(project_node, "audio_track")

	for _, clip in ipairs(audio_clips) do
		if is_valid(clip.sound) then
			local clip_node = audio_track_node.make_child(audio_track_node, "clip")

			clip_node.set_parameter(clip_node, "offset", tostring(clip.offset or 0))
			clip_node.set_parameter(clip_node, "sound", tostring(clip.sound))
		end
	end

	local key_track_node = project_node.make_child(project_node, "key_track")

	for _, cutscene_key in ipairs(cutscene_keys) do
		cutscene_key._save_under(cutscene_key, key_track_node)
	end

	local film_tracks_node = project_node.make_child(project_node, "film_tracks")
	local highest_film_track_index = table.inject(film_clips, 0, function (highest, clip)
		return math.max(highest, clip.track_index or 0)
	end)

	for track_index = 1, highest_film_track_index, 1 do
		local track_node = film_tracks_node.make_child(film_tracks_node, "track")

		for _, clip in ipairs(table.find_all_values(film_clips, function (clip)
			return clip.track_index == track_index
		end)) do
			if is_valid(clip.cutscene) and is_valid(clip.camera) and 0 < clip.to - clip.from then
				local clip_node = track_node.make_child(track_node, "clip")

				clip_node.set_parameter(clip_node, "offset", tostring(clip.offset or 0))
				clip_node.set_parameter(clip_node, "cutscene", tostring(clip.cutscene))
				clip_node.set_parameter(clip_node, "from", tostring(clip.from or 0))
				clip_node.set_parameter(clip_node, "to", tostring(clip.to or 0))
				clip_node.set_parameter(clip_node, "camera", tostring(clip.camera))
			end
		end
	end

	managers.database:save_node(project_node, self.path(self))
	managers.database:recompile()

	return 
end
CoreCutsceneEditorProject.audio_clips = function (self)
	local clips = {}
	local audio_track_node = self.child_node(self, "audio_track") or responder(function ()
		return 
	end)

	for clip_node in audio_track_node.children(audio_track_node) do
		table.insert(clips, {
			offset = tonumber(clip_node.parameter(clip_node, "offset") or "0"),
			sound = clip_node.parameter(clip_node, "sound")
		})
	end

	return clips
end
CoreCutsceneEditorProject.film_clips = function (self)
	local clips = {}
	local film_tracks_node = self.child_node(self, "film_tracks") or responder(0)
	local index = 0
	local track_count = film_tracks_node.num_children(film_tracks_node)

	while index < track_count do
		local track_node = film_tracks_node.child(film_tracks_node, index)

		for clip_node in track_node.children(track_node) do
			table.insert(clips, {
				track_index = index + 1,
				offset = tonumber(clip_node.parameter(clip_node, "offset") or "0"),
				cutscene = clip_node.parameter(clip_node, "cutscene"),
				from = tonumber(clip_node.parameter(clip_node, "from") or "0"),
				to = tonumber(clip_node.parameter(clip_node, "to") or "0"),
				camera = clip_node.parameter(clip_node, "camera")
			})
		end

		index = index + 1
	end

	return clips
end
CoreCutsceneEditorProject.cutscene_keys = function (self, key_collection)
	local cutscene_keys = {}
	local key_track_node = self.child_node(self, "key_track") or responder(function ()
		return 
	end)

	for key_node in key_track_node.children(key_track_node) do
		local cutscene_key = CoreCutsceneKey:create(key_node.name(key_node), key_collection)

		cutscene_key.load(cutscene_key, key_node)
		table.insert(cutscene_keys, cutscene_key)
	end

	return cutscene_keys
end
CoreCutsceneEditorProject.animation_patches = function (self)
	local patches = {}
	local settings_node = self.child_node(self, "settings") or responder(function ()
		return 
	end)
	local animation_patches_node = self.child_node(self, "animation_patches", settings_node) or responder(function ()
		return 
	end)

	for actor_node in animation_patches_node.children(animation_patches_node) do
		local unit_name = actor_node.parameter(actor_node, "name")

		for override_node in actor_node.children(actor_node) do
			local blend_set = override_node.parameter(override_node, "blend_set")
			local animation = override_node.parameter(override_node, "animation")
			patches[unit_name] = patches[unit_name] or {}
			patches[unit_name][blend_set] = animation
		end
	end

	return patches
end
CoreCutsceneEditorProject.root_node = function (self)
	if self._root_node == nil then
		self._root_node = managers.database:load_node(self.path(self))
	end

	return self._root_node
end
CoreCutsceneEditorProject.child_node = function (self, child_name, parent_node, child_properties)
	parent_node = parent_node or self.root_node(self)

	for child_node in parent_node.children(parent_node) do
		if child_node.name(child_node) == child_name and (child_properties == nil or table.true_for_all(child_properties, function (value, key)
			return child_node:parameter(key) == value
		end)) then
			return child_node
		end
	end

	return 
end

return 
