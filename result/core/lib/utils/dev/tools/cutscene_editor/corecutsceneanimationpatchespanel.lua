require("core/lib/utils/dev/ews/CoreTableEditorPanel")
require("core/lib/utils/dev/tools/cutscene_editor/CoreCutsceneEditorProject")

CoreCutsceneAnimationPatchesPanel = CoreCutsceneAnimationPatchesPanel or class(CoreTableEditorPanel)
CoreCutsceneAnimationPatchesPanel.init = function (self, parent)
	self.super.init(self, parent)

	self.__unit_types = {}

	self.freeze(self)
	self.add_column(self, "Unit Name")
	self.add_column(self, "Blend Set")
	self.add_column(self, "Animation")
	self.thaw(self)

	return 
end
CoreCutsceneAnimationPatchesPanel.unit_types = function (self)
	return self.__unit_types or {}
end
CoreCutsceneAnimationPatchesPanel.set_unit_types = function (self, unit_types)
	assert(type(unit_types) == "table" and table.true_for_all(table.map_values(unit_types), function (v)
		return DB:has("unit", tostring(v):id())
	end), "Expected unit_types to be a table mapping actor names to Unit type names.")

	self.__unit_types = unit_types

	self._refresh_fields_panel(self)
	self._refresh_buttons_panel(self)

	return 
end
CoreCutsceneAnimationPatchesPanel.patches = function (self)
	local patches = {}
	local list_ctrl = self.__list_ctrl
	local row_count = list_ctrl.item_count(list_ctrl)

	for row = 0, row_count - 1, 1 do
		local unit_name = list_ctrl.get_item(list_ctrl, row, 0)
		local blend_set = list_ctrl.get_item(list_ctrl, row, 1)
		local animation = list_ctrl.get_item(list_ctrl, row, 2)
		patches[unit_name] = patches[unit_name] or {}
		patches[unit_name][blend_set] = animation
	end

	return patches
end
CoreCutsceneAnimationPatchesPanel.set_patches = function (self, patches)
	self.freeze(self)
	self.clear(self)

	slot2 = pairs
	slot3 = patches or {}

	for unit_name, overrides in slot2(slot3) do
		for blend_set, animation in pairs(overrides) do
			self.add_item(self, unit_name, blend_set, animation)
		end
	end

	self.thaw(self)

	return 
end
CoreCutsceneAnimationPatchesPanel._sizer_with_editable_fields = function (self, parent)
	local sizer = EWS:BoxSizer("VERTICAL")
	local unit_name_enabled = self.selected_item(self) ~= nil and not table.empty(self.unit_types(self))
	local unit_name_label = EWS:StaticText(parent, "Unit Name:")

	unit_name_label.set_enabled(unit_name_label, unit_name_enabled)
	sizer.add(sizer, unit_name_label, 0, 5, "TOP,LEFT,RIGHT")

	local unit_name_dropdown = self._create_unit_name_dropdown(self, parent)

	unit_name_dropdown.set_enabled(unit_name_dropdown, unit_name_enabled)
	sizer.add(sizer, unit_name_dropdown, 0, 5, "ALL,EXPAND")
	self._create_labeled_text_field(self, "Blend Set", parent, sizer)

	local animation_text_field = self._create_animation_text_field(self, parent)

	sizer.add(sizer, animation_text_field, 0, 0, "EXPAND")

	if unit_name_enabled then
		unit_name_dropdown.set_focus(unit_name_dropdown)
	end

	return sizer
end
CoreCutsceneAnimationPatchesPanel._create_unit_name_dropdown = function (self, parent)
	local value = self.selected_item_value(self, "Unit Name")
	local control = EWS:ComboBox(parent, "", "", "CB_DROPDOWN,CB_READONLY,CB_SORT")

	control.freeze(control)

	local first_value = nil

	for unit_name, _ in pairs(self.unit_types(self)) do
		first_value = first_value or unit_name

		control.append(control, unit_name)
	end

	if value then
		control.set_value(control, value)
	end

	control.set_min_size(control, control.get_min_size(control):with_x(0))
	control.connect(control, "EVT_COMMAND_COMBOBOX_SELECTED", self._make_control_edited_callback(self, control, "Unit Name"))
	control.thaw(control)

	return control
end
CoreCutsceneAnimationPatchesPanel._create_animation_text_field = function (self, parent)
	local sizer = EWS:BoxSizer("HORIZONTAL")
	local labeled_text_field_sizer = EWS:BoxSizer("VERTICAL")
	local text_ctrl = self._create_labeled_text_field(self, "Animation", parent, labeled_text_field_sizer)
	local button = EWS:Button(parent, "Browse...")

	button.set_enabled(button, self.selected_item(self) ~= nil)
	button.connect(button, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_browse_for_animation"), text_ctrl)
	sizer.add(sizer, labeled_text_field_sizer, 1, 0, "EXPAND")
	sizer.add(sizer, button, 0, 4, "BOTTOM,RIGHT,ALIGN_BOTTOM")

	return sizer
end
CoreCutsceneAnimationPatchesPanel._refresh_buttons_panel = function (self)
	self.super._refresh_buttons_panel(self)
	self.__add_button:set_enabled(not table.empty(self.unit_types(self)))

	return 
end
CoreCutsceneAnimationPatchesPanel._on_browse_for_animation = function (self, text_ctrl)
	local dir, path = self._absolute_dir_and_path(self, text_ctrl.get_value(text_ctrl))
	dir = dir or self.__default_dir or managers.database:base_path() .. "data"
	path = path or ""
	local dialog = EWS:FileDialog(self._top_level_window(self), "Select an Animation File", dir, path, "Diesel Animation (*.diesel)|*.diesel", "OPEN,FILE_MUST_EXIST")

	if dialog.show_modal(dialog) then
		self.__default_dir = dialog.get_directory(dialog)
		local relative_path = string.match(dialog.get_path(dialog), "^" .. managers.database:base_path() .. "(.*)")

		text_ctrl.set_value(text_ctrl, relative_path)
	end

	return 
end
CoreCutsceneAnimationPatchesPanel._absolute_dir_and_path = function (self, relative_path)
	relative_path = Application:nice_path(relative_path or "", false)

	if string.begins(relative_path, "data\\") then
		local absolute_path = managers.database:base_path() .. relative_path
		local absolute_dir = string.match(absolute_path, "^(.+)\\")

		return absolute_dir, absolute_path
	else
		return nil, nil
	end

	return 
end

return 
