function set_widget_help(widget, help_text)
	widget.set_tool_tip(widget, help_text)

	return 
end

function set_widget_box_help(widget, help_header, help_text, view)
	local function on_enter(t, evt)
		t.set_own_font_weight(t, "FONTWEIGHT_BOLD")
		t.refresh(t)
		evt.skip(evt)

		return 
	end

	local function on_leave(t, evt)
		t.set_own_font_weight(t, "FONTWEIGHT_NORMAL")
		t.refresh(t)
		evt.skip(evt)

		return 
	end

	local function on_left_down(t, evt)
		t[1]:set_box_help(t[2], t[3])

		return 
	end

	widget.set_tool_tip(widget, "(click to get help text in textbox)\n" .. help_text)
	widget.connect(widget, "EVT_ENTER_WINDOW", on_enter, widget)
	widget.connect(widget, "EVT_LEAVE_WINDOW", on_leave, widget)
	widget.connect(widget, "EVT_LEFT_DOWN", on_left_down, {
		view,
		help_header,
		help_text
	})

	return 
end

function base_path(n)
	local bs = n.reverse(n):find("\\")

	if bs then
		return n.sub(n, #n - bs + 2)
	else
		return n
	end

	return 
end

function dir_name(n)
	local bs = n.reverse(n):find("\\")

	if bs then
		return n.sub(n, 1, #n - bs + 1)
	else
		return n
	end

	return 
end

return 
