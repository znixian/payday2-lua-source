CoreMaterialEditor = CoreMaterialEditor or class()
CoreMaterialEditor.live_update_parameter = function (self, name, param_type, param_ui_type, value)
	if alive(self._selected_unit) then
		table.insert(self._live_update_parameter_list, {
			_name = name,
			_param_type = param_type,
			_param_ui_type = param_ui_type,
			_value = value
		})
	end

	return 
end
CoreMaterialEditor._get_node = function (self, node, name)
	for n in node.children(node) do
		if n.name(n) == name then
			return n
		end
	end

	return 
end
CoreMaterialEditor._get_all_nodes = function (self, node, name)
	local t = {}

	for n in node.children(node) do
		if n.name(n) == name then
			table.insert(t, n)
		end
	end

	return t
end
CoreMaterialEditor._find_node = function (self, node, name, key, value)
	for n in node.children(node) do
		if n.parameter(n, key) == value and (not name or n.name(n) == name) then
			return n
		end
	end

	return 
end
CoreMaterialEditor._find_all_nodes = function (self, node, name, key, value)
	local t = {}

	for n in node.children(node) do
		if n.parameter(n, key) == value and (not name or n.name(n) == name) then
			table.insert(t, n)
		end
	end

	return t
end
CoreMaterialEditor._read_config = function (self)
	self._global_material_config_name = self.PROJECT_GLOBAL_GONFIG_NAME

	if not managers.database:has(managers.database:base_path() .. self._global_material_config_name) then
		self._global_material_config_name = self.CORE_GLOBAL_GONFIG_NAME

		if not managers.database:has(managers.database:base_path() .. self._global_material_config_name) then
			error("Could not find the global material config file!")
		end
	end

	local settings = managers.database:load_node(managers.database:base_path() .. self.SETTINGS_FILE)

	if settings then
		for setting in settings.children(settings) do
			if setting.name(setting) == "remote" and setting.parameter(setting, "host") then
				self._remote_host = setting.parameter(setting, "host")
			end
		end
	end

	return 
end
CoreMaterialEditor._write_config = function (self)
	local file = assert(SystemFS:open(managers.database:base_path() .. self.SETTINGS_FILE, "w"))
	local str = "<material_editor>\n"

	if self._remote_host then
		str = string.format("%s\t<remote host=\"%s\"/>\n", str, self._remote_host)
	end

	file.write(file, string.format("%s</material_editor>", str))
	file.close(file)

	return 
end
CoreMaterialEditor._freeze_frame = function (self)
	self._main_frame:freeze()

	return 
end
CoreMaterialEditor._unfreeze_frame = function (self)
	self._main_frame:layout()
	self._main_frame:thaw()
	self._main_frame:refresh()

	return 
end
CoreMaterialEditor._freeze_output = function (self)
	self._lock_output = true

	return 
end
CoreMaterialEditor._unfreeze_output = function (self)
	self._lock_output = nil

	return 
end
CoreMaterialEditor._remot_compile = function (self)
	local defines = nil

	for k, v in pairs(self._shader_defines) do
		if v._checked then
			defines = (defines and defines .. " " .. k) or k
		end
	end

	local cmd = string.format("start /D \"%score\\utils\\shader_server\" lua5.1.exe client.lua %s %s %s %s", managers.database:base_path(), self._remote_host, Application:short_game_name(), self._compilable_shader_combo_box:get_value(), defines)

	assert(os.execute(cmd) == 0)

	return 
end
CoreMaterialEditor._create_make_file = function (self, rebuild)
	local make_params, temp_params = self._get_make_params(self)
	local file = SystemFS:open(managers.database:base_path() .. self.TEMP_PATH .. "make.xml", "w")

	file.write(file, "<make>\n")
	file.write(file, "\t<silent_fail/>\n")

	if rebuild then
		file.write(file, "\t<rebuild/>\n")
	else
		file.write(file, "\t<compile shader=\"" .. self._compilable_shader_combo_box:get_value() .. "\" defines=\"")

		local defines = nil

		for k, v in pairs(self._shader_defines) do
			if v._checked then
				if not defines then
					defines = k
				else
					defines = defines .. " " .. k
				end
			end
		end

		file.write(file, (defines or "") .. "\"/>\n")
	end

	file.write(file, "\t<file_io\n")

	for k, v in pairs(make_params) do
		file.write(file, "\t\t" .. k .. "=\"" .. string.gsub(v, "/", "\\") .. "\"\n")
	end

	file.write(file, "\t/>\n</make>\n")
	file.close(file)

	return make_params, temp_params
end
CoreMaterialEditor._run_compiler = function (self)
	local cmd = Application:nice_path(managers.database:root_path() .. "aux_assets\\engine\\bin\\shaderdev\\", true) .. "shaderdev -m \"" .. Application:nice_path(managers.database:base_path() .. self.TEMP_PATH .. "make.xml", false) .. "\" > \"" .. Application:nice_path(managers.database:base_path() .. self.TEMP_PATH .. "compile_log.txt", false) .. "\""
	local ret = os.execute(cmd)
	local file = SystemFS:open(managers.database:base_path() .. self.TEMP_PATH .. "compile_log.txt", "r")
	local log = file.read(file)

	file.close(file)
	CoreEWS.show_log(self._main_frame, log, (ret == 0 and "Shader compiled OK!") or "Shader ERROR!")

	return ret == 0
end
CoreMaterialEditor._get_make_params = function (self)
	local shader = self._compilable_shaders[self._compilable_shader_combo_box:get_value()]
	local srcpath = managers.database:base_path() .. self.SHADER_PATH .. managers.database:entry_name(shader._entry)
	local tmppath = managers.database:base_path() .. self.TEMP_PATH
	local make_params = {}
	local temp_params = {}
	make_params.source = managers.database:base_path() .. shader._entry .. ".shader_source"
	make_params.working_directory = tmppath
	make_params.render_templates = srcpath .. ".render_template_database"
	make_params.win32d3d9 = tmppath .. managers.database:entry_name(shader._entry) .. ".d3d9.win32.shaders"
	make_params.win32d3d10 = tmppath .. managers.database:entry_name(shader._entry) .. ".d3d10.win32.shaders"
	make_params.ps3 = tmppath .. managers.database:entry_name(shader._entry) .. ".ps3.shaders"
	make_params.x360d3d9 = tmppath .. managers.database:entry_name(shader._entry) .. ".x360.shaders"
	make_params.lrb = tmppath .. managers.database:entry_name(shader._entry) .. ".lrb.shaders"
	temp_params.render_templates = tmppath .. managers.database:entry_name(shader._entry) .. ".render_template_database"
	temp_params.win32d3d9 = tmppath .. managers.database:entry_name(shader._entry) .. ".d3d9.win32.shaders"
	temp_params.win32d3d10 = tmppath .. managers.database:entry_name(shader._entry) .. ".d3d10.win32.shaders"
	temp_params.ps3 = tmppath .. managers.database:entry_name(shader._entry) .. ".ps3.shaders"
	temp_params.x360d3d9 = tmppath .. managers.database:entry_name(shader._entry) .. ".x360.shaders"
	temp_params.lrb = tmppath .. managers.database:entry_name(shader._entry) .. ".lrb.shaders"

	return make_params, temp_params
end
CoreMaterialEditor._cleanup_temp_files = function (self, temp_params)
	for k, v in pairs(temp_params) do
		os.remove(v)
	end

	os.remove(Application:nice_path(managers.database:base_path() .. self.TEMP_PATH .. "make.xml", false))
	os.remove(Application:nice_path(managers.database:base_path() .. self.TEMP_PATH .. "compile_log.txt", false))

	return 
end
CoreMaterialEditor._insert_libs_in_database = function (self, temp_params, make_params)
	assert(SystemFS:copy_file(temp_params.render_templates, make_params.render_templates), string.format("Could not copy %s -> %s", temp_params.render_templates, make_params.render_templates))
	self._cleanup_temp_files(self, temp_params)
	managers.database:recompile()

	return 
end
CoreMaterialEditor._copy_to_remote_client = function (self)
	return 
end
CoreMaterialEditor._find_unit_material = function (self, unit)
	local path = unit.material_config(unit):s()
	local node = DB:has("material_config", path) and DB:load_node("material_config", path)

	if node then
		return node, managers.database:entry_expanded_path("material_config", path)
	end

	return 
end
CoreMaterialEditor._find_selected_unit = function (self)
	if managers.editor and managers.editor:selected_unit() and managers.editor:selected_unit() ~= self._selected_unit then
		self._selected_unit = managers.editor:selected_unit()
		self._live_update_parameter_list = {}
		local unit_material_node, unit_material_path = self._find_unit_material(self, self._selected_unit)

		if unit_material_node and (not self._material_config_path or unit_material_path ~= self._material_config_path) and unit_material_node.parameter(unit_material_node, "version") == self.MATERIAL_CONFIG_VERSION_TAG and EWS:message_box(self._main_frame, "Do you want to open: " .. unit_material_path, "Open", "OK,CANCEL", Vector3(-1, -1, -1)) == "OK" then
			self._save_current(self)
			self._load_node(self, unit_material_path)
			self._start_dialog:end_modal()
		end
	end

	return 
end
CoreMaterialEditor._get_material = function (self)
	local units_in_world = World:find_units_quick("all")

	for _, unit_in_world in ipairs(units_in_world) do
		local material = unit_in_world.material(unit_in_world, Idstring(self._current_material_node:parameter("name")))

		if material then
			return material
		end
	end

	return 
end
CoreMaterialEditor._create_rt_name = function (self, rt)
	table.sort(rt)

	local rt_str = "generic"

	for _, option in ipairs(rt) do
		rt_str = rt_str .. ":" .. option
	end

	return rt_str
end
CoreMaterialEditor._try_convert_parameter = function (self, mat, child, rt)
	if child.name(child) == "diffuse_texture" then
		table.insert(rt, "DIFFUSE_TEXTURE")
	elseif child.name(child) == "bump_normal_texture" then
		table.insert(rt, "NORMALMAP")
	else
		mat.remove_child_at(mat, mat.index_of_child(mat, child))
	end

	return 
end
CoreMaterialEditor._version_error = function (self, mat)
	local res = EWS:message_box(self._main_frame, "This material is not of the expected version! Do you want to convert it?", "Version", "YES_NO", Vector3(-1, -1, -1))

	if res == "YES" then
		local rt = {}

		mat.set_parameter(mat, "version", self.MATERIAL_VERSION_TAG)
		mat.clear_parameter(mat, "src")

		for child in mat.children(mat) do
			self._try_convert_parameter(self, mat, child, rt)
		end

		mat.set_parameter(mat, "render_template", self._create_rt_name(self, rt))

		return true
	else
		return false
	end

	return 
end
CoreMaterialEditor._update_material = function (self, param)
	local material = self._get_material(self)

	if material then
		if param._param_type == "texture" then
			local name_ids = Idstring(param._name)

			Application:set_material_texture(material, name_ids, Idstring(param._value), material.texture_type(material, name_ids), 0)
		elseif param._param_type == "vector3" or param._param_type == "scalar" then
			if param._name == "diffuse_color" then
				material.set_diffuse_color(material, param._value)
			elseif param._param_ui_type == "intensity" then
				material.set_variable(material, Idstring(param._name), LightIntensityDB:lookup(Idstring(param._value)))
			else
				material.set_variable(material, Idstring(param._name), param._value)
			end
		end
	end

	return 
end
CoreMaterialEditor._live_update = function (self)
	if alive(self._selected_unit) then
		for _, param in ipairs(self._live_update_parameter_list) do
			self._update_material(self, param)
		end

		self._live_update_parameter_list = {}
	end

	return 
end
CoreMaterialEditor._check_valid_xml_on_save = function (self, node)
	local str = nil

	for mat in node.children(node) do
		for var in mat.children(mat) do
			if var.parameter(var, "file") == "[NONE]" then
				str = (str or "") .. var.name(var) .. "\n"
			end
		end
	end

	return str == nil, str
end
CoreMaterialEditor._set_channels_default_texture = function (self, node)
	for mat in node.children(node) do
		for var in mat.children(mat) do
			if var.parameter(var, "file") == "[NONE]" then
				var.set_parameter(var, "file", self.DEFAULT_TEXTURE)
			end
		end
	end

	return 
end

return 
