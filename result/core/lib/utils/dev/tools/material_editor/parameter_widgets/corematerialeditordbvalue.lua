require("core/lib/utils/dev/tools/material_editor/CoreSmartNode")

local CoreMaterialEditorParameter = require("core/lib/utils/dev/tools/material_editor/parameter_widgets/CoreMaterialEditorParameter")
local CoreMaterialEditorDBValue = CoreMaterialEditorDBValue or class(CoreMaterialEditorParameter)
CoreMaterialEditorDBValue.init = function (self, parent, editor, parameter_info, parameter_node)
	CoreMaterialEditorParameter.init(self, parent, editor, parameter_info, parameter_node)

	self._combobox = EWS:ComboBox(self._right_panel, self._value, "", "CB_READONLY")

	self._fill_combobox(self)
	self._combobox:set_value(self._value)
	self._combobox:connect("", "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "_on_combobox_changed"), "")
	self._right_box:add(self._combobox, 0, 4, "ALL,EXPAND")

	return 
end
CoreMaterialEditorDBValue.update = function (self, t, dt)
	CoreMaterialEditorParameter.update(self, t, dt)

	return 
end
CoreMaterialEditorDBValue.destroy = function (self)
	CoreMaterialEditorParameter.destroy(self)

	return 
end
CoreMaterialEditorDBValue.on_toggle_customize = function (self)
	self._customize = not self._customize

	self._load_value(self)
	self._editor:_update_output()
	self._right_panel:set_enabled(self._customize)
	self._combobox:set_value(self._value)
	self.update_live(self)

	return 
end
CoreMaterialEditorDBValue._on_combobox_changed = function (self)
	self._value = self._combobox:get_value()

	self._parameter_node:set_parameter("value", tostring(self._value))
	self.update_live(self)

	return 
end
CoreMaterialEditorDBValue._fill_combobox = function (self)
	for _, v in ipairs(LightIntensityDB:list()) do
		self._combobox:append(v.s(v))
	end

	return 
end

return CoreMaterialEditorDBValue
