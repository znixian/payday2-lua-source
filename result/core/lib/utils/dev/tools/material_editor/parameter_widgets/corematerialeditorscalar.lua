require("core/lib/utils/dev/tools/material_editor/CoreSmartNode")

local CoreMaterialEditorParameter = require("core/lib/utils/dev/tools/material_editor/parameter_widgets/CoreMaterialEditorParameter")
local CoreMaterialEditorScalar = CoreMaterialEditorScalar or class(CoreMaterialEditorParameter)
CoreMaterialEditorScalar.init = function (self, parent, editor, parameter_info, parameter_node)
	CoreMaterialEditorParameter.init(self, parent, editor, parameter_info, parameter_node)

	self._slider = EWS:Slider(self._right_panel, self.to_slider_range(self, self._value, parameter_info.min, parameter_info.step), 0, self.to_slider_range(self, parameter_info.max, parameter_info.min, parameter_info.step), "", "")

	self._slider:connect("", "EVT_SCROLL_THUMBTRACK", self._on_slider, self)
	self._slider:connect("", "EVT_SCROLL_CHANGED", self._on_slider, self)
	self._right_box:add(self._slider, 1, 4, "ALL,EXPAND")

	self._text_ctrl = EWS:TextCtrl(self._right_panel, tostring(self._value), "", "TE_PROCESS_ENTER")

	self._text_ctrl:connect("", "EVT_COMMAND_TEXT_ENTER", self._on_text_ctrl, self)
	self._text_ctrl:set_min_size(Vector3(40, -1, -1))
	self._right_box:add(self._text_ctrl, 0, 4, "ALL,EXPAND")

	return 
end
CoreMaterialEditorScalar.update = function (self, t, dt)
	CoreMaterialEditorParameter.update(self, t, dt)

	return 
end
CoreMaterialEditorScalar.destroy = function (self)
	CoreMaterialEditorParameter.destroy(self)

	return 
end
CoreMaterialEditorScalar.on_toggle_customize = function (self)
	self._customize = not self._customize

	self._load_value(self)
	self._editor:_update_output()
	self._right_panel:set_enabled(self._customize)
	self._text_ctrl:set_value(string.format("%.3f", self._value))
	self._slider:set_value(self.to_slider_range(self, self._value, self._parameter_info.min, self._parameter_info.step))
	self.update_live(self)

	return 
end
CoreMaterialEditorScalar._on_slider = function (self)
	self._value = self.from_slider_range(self, self._slider:get_value(), self._parameter_info.min, self._parameter_info.step)

	self._parameter_node:set_parameter("value", tostring(self._value))
	self._text_ctrl:set_value(string.format("%.3f", self._value))
	self.update_live(self)

	return 
end
CoreMaterialEditorScalar._on_text_ctrl = function (self)
	self._value = tonumber(self._text_ctrl:get_value()) or 0

	self._parameter_node:set_parameter("value", tostring(self._value))
	self._slider:set_value(self.to_slider_range(self, self._value, self._parameter_info.min, self._parameter_info.step))
	self._editor:_update_output()
	self.update_live(self)

	return 
end

return CoreMaterialEditorScalar
