require("core/lib/utils/dev/tools/material_editor/CoreSmartNode")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreEws")
core:import("CoreColorPickerPanel")

local CoreMaterialEditorParameter = require("core/lib/utils/dev/tools/material_editor/parameter_widgets/CoreMaterialEditorParameter")
local CoreMaterialEditorColor3 = CoreMaterialEditorColor3 or class(CoreMaterialEditorParameter)
CoreMaterialEditorColor3.init = function (self, parent, editor, parameter_info, parameter_node)
	CoreMaterialEditorParameter.init(self, parent, editor, parameter_info, parameter_node)

	self._picker_panel = CoreColorPickerPanel.ColorPickerPanel:new(self._right_panel, false, "HORIZONTAL")

	self._picker_panel:set_color(Color(self._value.x, self._value.y, self._value.z))
	self._picker_panel:connect("EVT_COLOR_UPDATED", CoreEvent.callback(self, self, "_on_color"), self._picker_panel)
	self._right_box:add(self._picker_panel:panel(), 1, 0, "ALL,EXPAND")

	return 
end
CoreMaterialEditorColor3.update = function (self, t, dt)
	CoreMaterialEditorParameter.update(self, t, dt)
	self._picker_panel:update(t, dt)

	return 
end
CoreMaterialEditorColor3.destroy = function (self)
	CoreMaterialEditorParameter.destroy(self)

	return 
end
CoreMaterialEditorColor3.on_toggle_customize = function (self)
	self._customize = not self._customize

	self._load_value(self)
	self._editor:_update_output()
	self._right_panel:set_enabled(self._customize)
	self._picker_panel:set_color(Color(self._value.x, self._value.y, self._value.z))
	self.update_live(self)

	return 
end
CoreMaterialEditorColor3._on_color = function (self, sender, color)
	self._value = Vector3(color.r, color.g, color.b)

	self._parameter_node:set_parameter("value", math.vector_to_string(self._value))
	self.update_live(self)

	return 
end

return CoreMaterialEditorColor3
