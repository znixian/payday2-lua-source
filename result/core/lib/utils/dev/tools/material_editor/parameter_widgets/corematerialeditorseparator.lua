require("core/lib/utils/dev/tools/material_editor/CoreSmartNode")

local CoreMaterialEditorParameter = require("core/lib/utils/dev/tools/material_editor/parameter_widgets/CoreMaterialEditorParameter")
local CoreMaterialEditorSeparator = CoreMaterialEditorSeparator or class(CoreMaterialEditorParameter)
CoreMaterialEditorSeparator.init = function (self, parent)
	self._panel = EWS:Panel(parent, "", "")
	self._box = EWS:BoxSizer("HORIZONTAL")
	self._line = EWS:StaticLine(self._panel, "", "")

	self._box:add(self._line, 1, 4, "TOP,EXPAND")
	self._panel:set_sizer(self._box)

	return 
end
CoreMaterialEditorSeparator.update = function (self, t, dt)
	return 
end
CoreMaterialEditorSeparator.destroy = function (self)
	CoreMaterialEditorParameter.destroy(self)

	return 
end

return CoreMaterialEditorSeparator
