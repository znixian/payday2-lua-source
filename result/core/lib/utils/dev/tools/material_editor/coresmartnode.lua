CoreSmartNode = CoreSmartNode or class()
CoreSmartNode.init = function (self, node)
	self._parameters = {}
	self._children = {}

	if type(node) == "string" then
		self._name = node
	else
		self._name = node.name(node)

		for k, v in pairs(node.parameters(node)) do
			self._parameters[k] = v
		end

		for child in node.children(node) do
			table.insert(self._children, CoreSmartNode:new(child))
		end
	end

	return 
end
CoreSmartNode.children = function (self)
	local count = table.getn(self._children)
	local i = 0

	return function ()
		i = i + 1

		if i <= count then
			return self._children[i]
		end

		return 
	end
end
CoreSmartNode.parameters = function (self)
	return self._parameters
end
CoreSmartNode.name = function (self)
	return self._name
end
CoreSmartNode.num_children = function (self)
	return #self._children
end
CoreSmartNode.parameter = function (self, k)
	return self._parameters[k]
end
CoreSmartNode.set_parameter = function (self, k, v)
	self._parameters[k] = v

	return 
end
CoreSmartNode.clear_parameter = function (self, k)
	self._parameters[k] = nil

	return 
end
CoreSmartNode.make_child = function (self, name)
	local node = CoreSmartNode:new(name)

	table.insert(self._children, node)

	return node
end
CoreSmartNode.add_child = function (self, n)
	local node = CoreSmartNode:new(n)

	table.insert(self._children, node)

	return node
end
CoreSmartNode.index_of_child = function (self, c)
	local i = 0

	for child in self.children(self) do
		if child == c then
			return i
		end

		i = i + 1
	end

	return -1
end
CoreSmartNode.remove_child_at = function (self, index)
	local i = index + 1
	self._children[i] = self._children[#self._children]
	self._children[#self._children] = nil

	return 
end
CoreSmartNode.to_real_node = function (self)
	local node = Node(self._name)

	for k, v in pairs(self.parameters(self)) do
		node.set_parameter(node, k, v)
	end

	for child in self.children(self) do
		node.add_child(node, child.to_real_node(child))
	end

	return node
end
CoreSmartNode.to_xml = function (self)
	return self.to_real_node(self):to_xml()
end

return 
