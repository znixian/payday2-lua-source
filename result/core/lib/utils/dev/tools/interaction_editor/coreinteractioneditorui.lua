core:module("CoreInteractionEditorUI")
core:import("CoreClass")
core:import("CoreCode")
core:import("CoreInteractionEditorConfig")
core:import("CoreInteractionEditorPropUI")

InteractionEditorUI = InteractionEditorUI or CoreClass.class()
InteractionEditorUI.init = function (self, owner)
	self._main_frame = EWS:Frame(CoreInteractionEditorConfig.EDITOR_TITLE, Vector3(-1, -1, -1), Vector3(1000, 800, 0), "", Global.frame)
	local menu_bar = EWS:MenuBar()
	self._file_menu = EWS:Menu("")

	self._file_menu:append_item("NEW", "New\tCtrl-N", "")
	self._file_menu:append_item("OPEN", "Open\tCtrl-O", "")
	self._file_menu:append_separator()
	self._file_menu:append_item("SAVE", "Save\tCtrl-S", "")
	self._file_menu:set_enabled("SAVE", false)
	self._file_menu:append_item("SAVE_AS", "Save As", "")
	self._file_menu:set_enabled("SAVE_AS", false)
	self._file_menu:append_item("SAVE_ALL", "Save All", "")
	self._file_menu:set_enabled("SAVE_ALL", false)
	self._file_menu:append_separator()
	self._file_menu:append_item("CLOSE", "Close\tCtrl-W", "")
	self._file_menu:set_enabled("CLOSE", false)
	self._file_menu:append_separator()
	self._file_menu:append_item("EXIT", "Exit", "")
	menu_bar.append(menu_bar, self._file_menu, "File")

	local edit_menu = EWS:Menu("")

	edit_menu.append_item(edit_menu, "UNDO", "Undo\tCtrl-Z", "")
	self._file_menu:set_enabled("UNDO", false)
	edit_menu.append_item(edit_menu, "REDO", "Redo\tCtrl-Y", "")
	self._file_menu:set_enabled("REDO", false)
	menu_bar.append(menu_bar, edit_menu, "Edit")
	self._main_frame:set_menu_bar(menu_bar)

	self._tool_bar = EWS:ToolBar(self._main_frame, "", "TB_FLAT,TB_NOALIGN")

	self._tool_bar:add_tool("NEW", "New", CoreInteractionEditorConfig.NEW_ICON, "")
	self._tool_bar:add_tool("OPEN", "Open", CoreInteractionEditorConfig.OPEN_ICON, "")
	self._tool_bar:add_separator()
	self._tool_bar:add_tool("SAVE", "Save", CoreInteractionEditorConfig.SAVE_ICON, "")
	self._tool_bar:set_tool_enabled("SAVE", false)
	self._tool_bar:add_tool("SAVE_ALL", "Save All", CoreInteractionEditorConfig.SAVE_ALL_ICON, "")
	self._tool_bar:set_tool_enabled("SAVE_ALL", false)
	self._tool_bar:add_separator()
	self._tool_bar:add_tool("CLOSE", "Close", CoreInteractionEditorConfig.CLOSE_ICON, "")
	self._tool_bar:set_tool_enabled("CLOSE", false)
	self._main_frame:set_tool_bar(self._tool_bar)
	self._tool_bar:realize()

	self._status_bar = EWS:StatusBar(self._main_frame, "", "")

	self._main_frame:set_status_bar(self._status_bar)

	local main_box = EWS:BoxSizer("HORIZONTAL")
	self._main_splitter = EWS:SplitterWindow(self._main_frame, "", "")
	self._main_notebook = EWS:Notebook(self._main_splitter, "", "")
	self._prop_panel = CoreInteractionEditorPropUI.InteractionEditorPropUI:new(self._main_splitter, owner)

	self._main_splitter:split_vertically(self._main_notebook, self._prop_panel:window(), -1)
	main_box.add(main_box, self._main_splitter, 1, 0, "EXPAND")
	self._main_frame:set_sizer(main_box)
	self._main_splitter:set_minimum_pane_size(200)
	self._main_splitter:set_sash_gravity(1)
	self._main_splitter:update_size()
	self._main_frame:set_visible(true)

	self._owner = owner

	self.connect_events(self)

	return 
end
InteractionEditorUI.frame = function (self)
	return self._main_frame
end
InteractionEditorUI.set_position = function (self, pos)
	self._main_frame:set_position(pos)

	return 
end
InteractionEditorUI.set_title = function (self, text)
	self._main_frame:set_title((text and CoreInteractionEditorConfig.EDITOR_TITLE .. " - " .. text) or CoreInteractionEditorConfig.EDITOR_TITLE)

	return 
end
InteractionEditorUI.connect_events = function (self)
	self._main_frame:connect("NEW", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_new"), nil)
	self._main_frame:connect("OPEN", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_open"), nil)
	self._main_frame:connect("SAVE", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_save"), nil)
	self._main_frame:connect("SAVE_AS", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_save_as"), nil)
	self._main_frame:connect("SAVE_ALL", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_save_all"), nil)
	self._main_frame:connect("CLOSE", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_close_system"), nil)
	self._main_frame:connect("EXIT", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_close"), nil)
	self._main_frame:connect("UNDO", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_undo"), nil)
	self._main_frame:connect("REDO", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_redo"), nil)
	self._main_frame:connect("", "EVT_CLOSE_WINDOW", callback(self._owner, self._owner, "on_close"), nil)
	self._main_frame:connect("", "EVT_COMMAND_NOTEBOOK_PAGE_CHANGED", callback(self._owner, self._owner, "on_notebook_changing"), nil)

	return 
end
InteractionEditorUI.create_graph_context_menu = function (self, system)
	system.graph(system):connect("", "EVT_RIGHT_UP", callback(self._owner, self._owner, "on_show_graph_context_menu"), system)

	local add_menu = EWS:Menu("")

	for _, v in ipairs(InteractionDescription:node_types()) do
		add_menu.append_item(add_menu, "ADD_NODE_" .. string.upper(v), string.upper(v), "")
		system.graph(system):window():connect("ADD_NODE_" .. string.upper(v), "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_add_node"), function ()
			system:add_node(v)

			return 
		end)
	end

	local menu = EWS:Menu("")

	menu.append_menu(menu, "ADD_NODE", "Add Node", add_menu, "")
	menu.append_separator(menu)
	menu.append_item(menu, "DELETE_NODE", "Delete Node", "")
	system.graph(system):window():connect("DELETE_NODE", "EVT_COMMAND_MENU_SELECTED", callback(self._owner, self._owner, "on_remove_node"), function ()
		for _, n in ipairs(system:selected_nodes()) do
			system:remove_node(n)
		end

		return 
	end)

	return menu
end
InteractionEditorUI.show_graph_context_menu = function (self, system)
	system.context_menu(system):set_enabled("DELETE_NODE", false)
	system.graph(system):window():popup_menu(system.context_menu(system), Vector3(-1, -1, 0))

	return 
end
InteractionEditorUI.destroy = function (self)
	if CoreCode.alive(self._main_frame) then
		self._main_frame:destroy()

		self._main_frame = nil
	end

	return 
end
InteractionEditorUI.clean_prop_panel = function (self)
	self._prop_panel:clean()

	return 
end
InteractionEditorUI.rebuild_prop_panel = function (self, desc, node)
	self._prop_panel:rebuild(desc, node)

	return 
end
InteractionEditorUI.create_nb_page = function (self, caption, select)
	local panel = EWS:Panel(self._main_notebook, "", "")

	return panel, self._main_notebook:add_page(panel, caption, select)
end
InteractionEditorUI.destroy_nb_page = function (self, id)
	self._main_notebook:freeze()

	local newid = math.clamp(self.set_nb_page(self, 0), 0, math.clamp(self.get_nb_page_count(self) - 2, 0, math.huge))

	self._main_notebook:delete_page(id)
	self.set_nb_page(self, newid)
	self._main_notebook:thaw()
	self._main_notebook:refresh()

	return 
end
InteractionEditorUI.current_nb_page = function (self)
	return self._main_notebook:get_current_page()
end
InteractionEditorUI.get_nb_page_count = function (self)
	return self._main_notebook:get_page_count()
end
InteractionEditorUI.set_nb_page = function (self, id)
	return self._main_notebook:set_page(id)
end
InteractionEditorUI.get_nb_page = function (self, id)
	return self._main_notebook:get_page(id)
end
InteractionEditorUI.update_nb_page_caption = function (self, id, text)
	self._main_notebook:set_page_text(id, text)

	return 
end
InteractionEditorUI.get_nb_page_by_caption = function (self, text)
	for i = 0, self._main_notebook:get_page_count() - 1, 1 do
		if self._main_notebook:get_page_text(i) == text then
			return i
		end
	end

	return 
end
InteractionEditorUI.get_nb_page_id = function (self, panel)
	for i = 0, self._main_notebook:get_page_count() - 1, 1 do
		if self._main_notebook:get_page(i) == panel then
			return i
		end
	end

	return 
end
InteractionEditorUI.set_save_close_option_enabled = function (self, b)
	self._file_menu:set_enabled("SAVE", b)
	self._tool_bar:set_tool_enabled("SAVE", b)
	self._file_menu:set_enabled("SAVE_AS", b)
	self._file_menu:set_enabled("SAVE_ALL", b)
	self._tool_bar:set_tool_enabled("SAVE_ALL", b)
	self._file_menu:set_enabled("CLOSE", b)
	self._tool_bar:set_tool_enabled("CLOSE", b)

	return 
end
InteractionEditorUI.want_to_save = function (self, path)
	return EWS:message_box(self._main_frame, path .. " has changed.\nDo you want to save it?", "Save Changes", "ICON_WARNING,YES_DEFAULT,YES_NO,CANCEL", Vector3(-1, -1, -1))
end

return 
