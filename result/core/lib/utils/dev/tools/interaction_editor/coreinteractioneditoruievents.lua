core:module("CoreInteractionEditorUIEvents")
core:import("CoreClass")
core:import("CoreCode")
core:import("CoreMath")
core:import("CoreInteractionEditorConfig")

InteractionEditorUIEvents = InteractionEditorUIEvents or CoreClass.class()
InteractionEditorUIEvents.on_close = function (self)
	managers.toolhub:close(CoreInteractionEditorConfig.EDITOR_TITLE)

	return 
end
InteractionEditorUIEvents.on_new = function (self)
	self.open_system(self)

	return 
end
InteractionEditorUIEvents.on_close_system = function (self)
	self.close_system(self)

	return 
end
InteractionEditorUIEvents.on_notebook_changing = function (self, data, event)
	self.activate_system(self, self.ui(self):get_nb_page(event.get_selection(event)))

	return 
end
InteractionEditorUIEvents.on_show_graph_context_menu = function (self, system)
	self.ui(self):show_graph_context_menu(system)

	return 
end
InteractionEditorUIEvents.on_add_node = function (self, func)
	func()

	return 
end
InteractionEditorUIEvents.on_remove_node = function (self, func)
	func()

	return 
end
InteractionEditorUIEvents.on_save = function (self)
	self.do_save(self)

	return 
end
InteractionEditorUIEvents.on_save_as = function (self)
	self.do_save_as(self)

	return 
end
InteractionEditorUIEvents.on_save_all = function (self)
	self.do_save_all(self)

	return 
end
InteractionEditorUIEvents.on_open = function (self)
	local path, dir = managers.database:open_file_dialog(self.ui(self):frame(), "*.interaction_project")

	if path and managers.database:has(path) then
		self.open_system(self, path)
	end

	return 
end
InteractionEditorUIEvents.on_undo = function (self)
	return 
end
InteractionEditorUIEvents.on_redo = function (self)
	return 
end

return 
