core:module("CoreInteractionEditorSystemEvents")
core:import("CoreClass")
core:import("CoreCode")
core:import("CoreInteractionEditorOpStack")

InteractionEditorSystemEvents = InteractionEditorSystemEvents or CoreClass.class()
InteractionEditorSystemEvents.setup_stack = function (self)
	local stack = CoreInteractionEditorOpStack.InteractionEditorOpStack:new()

	return stack
end
InteractionEditorSystemEvents.undo_add_node = function (self, node_id, node_type)
	return 
end
InteractionEditorSystemEvents.redo_add_node = function (self, node_id, node_type)
	return 
end
InteractionEditorSystemEvents.undo_remove_node = function (self, node_id)
	return 
end
InteractionEditorSystemEvents.redo_remove_node = function (self, node_id)
	return 
end
InteractionEditorSystemEvents.on_delete_node = function (self, data, event)
	self.remove_node(self, event.node(event))
	event.skip(event)

	return 
end
InteractionEditorSystemEvents.on_select_node = function (self, data, event)
	self.ui(self):clean_prop_panel()

	local nodes = event.selected_nodes(event)

	if #nodes == 1 then
		self.ui(self):rebuild_prop_panel(self.desc(self), nodes[1]:metadata())
	end

	event.skip(event)

	return 
end
InteractionEditorSystemEvents.on_connect_node = function (self, data, event)
	local md_src = event.source(event):metadata()
	local output = event.outslot(event)
	local md_dest = event.dest(event):metadata()
	local input = event.inslot(event)

	assert(md_src ~= "" and output ~= "" and md_dest ~= "", input ~= "")

	local src_type = self._desc:transput_type(md_src, output)
	local dest_type = self._desc:transput_type(md_dest, input)

	if src_type == "undefined" or dest_type == "undefined" or src_type == dest_type then
		self._desc:transition_add(md_src, output, md_dest, input)
		event.skip(event)
	end

	return 
end
InteractionEditorSystemEvents.on_disconnect_node = function (self, data, event)
	local md_src = event.source(event):metadata()
	local output = event.outslot(event)
	local md_dest = event.dest(event):metadata()
	local input = event.inslot(event)

	assert(md_src ~= "" and output ~= "" and md_dest ~= "", input ~= "")
	self._desc:transition_remove(md_src, output, md_dest, input)
	event.skip(event)

	return 
end

return 
