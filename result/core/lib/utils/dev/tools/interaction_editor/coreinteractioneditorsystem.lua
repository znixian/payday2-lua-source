core:module("CoreInteractionEditorSystem")
core:import("CoreClass")
core:import("CoreCode")
core:import("CoreScriptGraph")
core:import("CoreInteractionEditorConfig")
core:import("CoreInteractionEditorSystemEvents")

new_counter = new_counter or 1
node_id = node_id or 1
InteractionEditorSystem = InteractionEditorSystem or CoreClass.class(CoreInteractionEditorSystemEvents.InteractionEditorSystemEvents)
InteractionEditorSystem.init = function (self, ui, path)
	self._ui = ui
	self._path = path
	self._node_id_map = {}
	self._pattern_data = {}
	self._is_new = path == nil
	self._caption = (path and managers.database:entry_name(path)) or "New" .. tostring(new_counter)
	self._op_stack = CoreInteractionEditorSystemEvents.InteractionEditorSystemEvents.setup_stack(self)
	self._panel, self._id = ui.create_nb_page(ui, self._caption, true)
	local main_box = EWS:BoxSizer("VERTICAL")
	self._graph = CoreScriptGraph.ScriptGraph:new(self._panel, "", "FLOW")

	self._graph:connect("", "EVT_NODE_DELETE", callback(self, self, "on_delete_node"), nil)
	self._graph:connect("", "EVT_NODE_SELECTED", callback(self, self, "on_select_node"), nil)
	self._graph:connect("", "EVT_NODE_CONNECT", callback(self, self, "on_connect_node"), nil)
	self._graph:connect("", "EVT_NODE_DISCONNECT", callback(self, self, "on_disconnect_node"), nil)

	self._context_menu = ui.create_graph_context_menu(ui, self)

	main_box.add(main_box, self._graph:window(), 1, 0, "EXPAND")

	self._desc = InteractionDescription()

	self._panel:set_sizer(main_box)

	new_counter = new_counter + 1

	self.activate(self)

	self._node_id_map = (path and self._graph:load(assert(managers.database:load_node(path)))) or {}
	local md = self._graph:graph_metadata()

	if md then
		for child in md.children(md) do
			if child.name(child) == "interaction" then
				self._desc:from_xml(child)

				break
			end
		end

		for child in md.children(md) do
			if child.name(child) == "patterns" then
				self._load_patterns(self, self._desc, self._pattern_data, child)

				break
			end
		end
	end

	return 
end
InteractionEditorSystem.caption = function (self)
	return self._caption
end
InteractionEditorSystem.desc = function (self)
	return self._desc
end
InteractionEditorSystem.ui = function (self)
	return self._ui
end
InteractionEditorSystem.is_new = function (self)
	return self._is_new
end
InteractionEditorSystem.path = function (self)
	return self._path
end
InteractionEditorSystem.update = function (self, t, dt)
	if self._active and self._graph then
		self._graph:update(t, dt)
	end

	return 
end
InteractionEditorSystem.graph = function (self)
	return self._graph
end
InteractionEditorSystem.graph_node = function (self, id)
	return self._node_id_map[id]
end
InteractionEditorSystem.selected_nodes = function (self)
	return self._graph:selected_nodes()
end
InteractionEditorSystem.add_pattern_data = function (self, node, pat, ptype, name, full_name)
	local nd = self._pattern_data[node] or {}
	local key = full_name .. " - " .. ptype

	assert(not nd[key])

	nd[key] = {
		node,
		pat,
		ptype,
		name,
		full_name
	}
	self._pattern_data[node] = nd

	return 
end
InteractionEditorSystem.pattern_data = function (self, node, full_name_and_type)
	return unpack(assert(self._pattern_data[node])[full_name_and_type])
end
InteractionEditorSystem.remove_pattern_data = function (self, node, full_name_and_type)
	assert(self._pattern_data[node])[full_name_and_type] = nil

	return 
end
InteractionEditorSystem.add_node = function (self, node_type, skip_stack)
	local id = node_type .. tostring(node_id)

	assert(self._desc:node_add(id, node_type))

	local node = EWS:FlowNode(node_type, self._desc:node_inputs(id), self._desc:node_outputs(id), 0, 0)

	node.set_metadata(node, id)
	self._graph:add_node(node)

	self._node_id_map[id] = node
	node_id = node_id + 1

	self.set_node_colors(self, node, id)

	skip_stack = skip_stack or skip_stack

	return 
end
InteractionEditorSystem.remove_node = function (self, node, skip_stack)
	local id = node.metadata(node)

	assert(self._desc:node_remove(id))

	self._node_id_map[id] = nil
	skip_stack = skip_stack or skip_stack

	return 
end
InteractionEditorSystem.panel = function (self)
	return self._panel
end
InteractionEditorSystem.context_menu = function (self)
	return self._context_menu
end
InteractionEditorSystem.close = function (self)
	self.deactivate(self)
	self._panel:destroy_children()
	self._ui:destroy_nb_page(self._ui:get_nb_page_by_caption(self._caption))

	self._graph = nil

	return 
end
InteractionEditorSystem.active = function (self)
	return self._active
end
InteractionEditorSystem.activate = function (self)
	self._ui:set_title(not self._is_new and self._path)
	self._ui:set_save_close_option_enabled(true)

	local selected = self._graph:selected_nodes()

	if #selected == 1 then
		self._ui:rebuild_prop_panel(self._desc, selected[1]:metadata())
	end

	self._active = true

	return 
end
InteractionEditorSystem.deactivate = function (self)
	self._ui:set_title()
	self._ui:set_save_close_option_enabled(false)
	self._ui:clean_prop_panel()

	self._active = false

	return 
end
InteractionEditorSystem.save = function (self, path)
	local md_node = Node("graph_metadata")

	md_node.add_child(md_node, self._desc:to_xml())

	local pat_node = md_node.make_child(md_node, "patterns")

	self._save_patterns(self, self._pattern_data, pat_node)
	self._graph:set_graph_metadata(md_node)
	managers.database:save_node(assert(self._graph:save(self._node_id_map)), path)

	self._is_new = false
	self._path = path
	local page_id = self._ui:get_nb_page_by_caption(self._caption)
	self._caption = managers.database:entry_name(path)

	self._ui:set_title(self._path)
	self._ui:update_nb_page_caption(page_id, self._caption)
	self._op_stack:mark_save()

	return 
end
InteractionEditorSystem.undo = function (self)
	self._op_stack:undo()

	return 
end
InteractionEditorSystem.redo = function (self)
	self._op_stack:redo()

	return 
end
InteractionEditorSystem.has_unsaved_changes = function (self)
	return self._op_stack:has_unsaved_changes()
end
InteractionEditorSystem.set_node_colors = function (self, node, id)
	for _, trans in ipairs(self._desc:node_inputs(id)) do
		local color = assert(self._slot_color(self, self._desc:transput_type(id, trans)))

		node.set_input_colour(node, trans, color.r, color.g, color.b)
	end

	for _, trans in ipairs(self._desc:node_outputs(id)) do
		local color = assert(self._slot_color(self, self._desc:transput_type(id, trans)))

		node.set_output_colour(node, trans, color.r, color.g, color.b)
	end

	return 
end
InteractionEditorSystem._slot_color = function (self, t)
	for i, v in ipairs(CoreInteractionEditorConfig.NODE_TYPES) do
		if v == t then
			return CoreInteractionEditorConfig.NODE_COLORS[i]
		end
	end

	return 
end
InteractionEditorSystem._save_patterns = function (self, pattern_data, cfg_node)
	for node, data in pairs(pattern_data) do
		for _, params in pairs(data) do
			local inst = cfg_node.make_child(cfg_node, "instance")

			inst.set_parameter(inst, "node", node)
			inst.set_parameter(inst, "pat", params[2])
			inst.set_parameter(inst, "ptype", params[3])
			inst.set_parameter(inst, "name", params[4])
			inst.set_parameter(inst, "full_name", params[5])
		end
	end

	return 
end
InteractionEditorSystem._load_patterns = function (self, desc, pattern_data, cfg_node)
	for inst in cfg_node.children(cfg_node) do
		local node = inst.parameter(inst, "node")
		local pat = inst.parameter(inst, "pat")
		local ptype = inst.parameter(inst, "ptype")
		local name = inst.parameter(inst, "name")
		local full_name = inst.parameter(inst, "full_name")

		self.add_pattern_data(self, node, pat, ptype, name, full_name)
	end

	return 
end

return 
