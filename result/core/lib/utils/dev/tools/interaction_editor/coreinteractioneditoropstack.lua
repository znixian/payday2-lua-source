core:module("CoreInteractionEditorOpStack")
core:import("CoreClass")
core:import("CoreCode")

InteractionEditorOpStack = InteractionEditorOpStack or CoreClass.class()
InteractionEditorOpStack.init = function (self)
	self._stack = {}
	self._redo_stack = {}
	self._ops = {
		save = {
			name = "save"
		}
	}

	return 
end
InteractionEditorOpStack.has_unsaved_changes = function (self)
	local size = #self._stack

	return 0 < size and self._stack[size].op.name ~= "save"
end
InteractionEditorOpStack.new_op_type = function (self, name, undo_cb, redo_cb)
	self._ops[name] = {
		name = name,
		undo_cb = undo_cb,
		redo_cb = redo_cb
	}

	return 
end
InteractionEditorOpStack.mark_save = function (self)
	self.new_op(self, "save")

	return 
end
InteractionEditorOpStack.new_op = function (self, name, ...)
	table.insert(self._stack, {
		op = assert(self._ops[name]),
		params = {
			...
		}
	})

	self._redo_stack = {}

	return 
end
InteractionEditorOpStack.undo = function (self)
	local size = #self._stack

	if 0 < size then
		local op_data = self._stack[size]

		table.insert(self._redo_stack, op_data)
		table.remove(self._stack, size)

		if op_data.op.name ~= "save" then
			op_data.op.undo_cb(op.params)
		else
			self.undo(self)
		end
	end

	return 
end
InteractionEditorOpStack.redo = function (self)
	local size = #self._redo_stack

	if 0 < size then
		local op_data = self._redo_stack[size]

		table.insert(self._stack, op_data)
		table.remove(self._redo_stack, size)

		if op_data.op.name ~= "save" then
			op_data.op.redo_cb(op.params)
		else
			self.redo(self)
		end
	end

	return 
end

return 
