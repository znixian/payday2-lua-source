core:module("CoreLuaProfilerTreeBox")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreKeywordArguments")

local parse_kwargs = CoreKeywordArguments.parse_kwargs
local DEFAULT = Vector3(1, 1, 1)
local HIGHLIGHTED = Vector3(0.8, 0.8, 1)
local PERCENT = 0
local SECONDS = 1
local PLAIN = 2
local DEFAULT_FORMAT = PERCENT
local DEFAULT_INFOKEY = "total_time"
LuaProfilerTreeBox = LuaProfilerTreeBox or CoreClass.class()
LuaProfilerTreeBox.init = function (self, ...)
	local args = CoreKeywordArguments.KeywordArguments:new(...)
	self._ews_parent = args.mandatory_object(args, "parent")

	args.assert_all_consumed(args)

	self._lpd = nil
	self.panel = EWS:Panel(self._ews_parent, "", "")
	self._treectrl = EWS:TreeCtrl(self.panel, "", "TR_HAS_BUTTONS")
	self._displayformat = DEFAULT_FORMAT
	self.__on_item_expanded_cb = CoreEvent.callback(self, self, "_on_item_expanded")

	self._treectrl:connect("EVT_COMMAND_TREE_SEL_CHANGED", CoreEvent.callback(self, self, "_on_select"))
	self._treectrl:connect("EVT_COMMAND_TREE_ITEM_EXPANDED", self.__on_item_expanded_cb)

	self.box_sizer = EWS:BoxSizer("HORIZONTAL")

	self.box_sizer:add(self._treectrl, 1, 1, "EXPAND")
	self.panel:set_sizer(self.box_sizer)

	return 
end
LuaProfilerTreeBox.set_gridview = function (self, ...)
	self._gridview = parse_kwargs({
		...
	}, "table:gridview")

	return 
end
LuaProfilerTreeBox.destroy = function (self)
	self._lpd = nil

	self._treectrl:clear()
	self._treectrl:destroy()

	self._treectrl = nil
	self._gridview = nil

	return 
end
LuaProfilerTreeBox.set_profilerdata = function (self, ...)
	self._lpd, self._displayformat = parse_kwargs({
		...
	}, "userdata:lpd", "number:displayformat")

	self._redraw(self)

	return 
end
LuaProfilerTreeBox.set_displayformat = function (self, ...)
	self._displayformat = parse_kwargs({
		...
	}, "number:displayformat")

	if self._lpd then
		function relabel(cnid)
			if self._lpd:cn_treenodeid(cnid) ~= -1 then
				local label = self:_makelabel(cnid)
				local tnid = self._lpd:cn_treenodeid(cnid)

				self._treectrl:set_item_text(tnid, label)
				self._treectrl:set_item_text_colour(tnid, Vector3(0, 0, 0))

				for i = 0, self._lpd:cn_numchildren(cnid) - 1, 1 do
					relabel(self._lpd:cn_child(cnid, i))
				end

				for i = 0, self._lpd:cn_numexpensivechildren(cnid) - 1, 1 do
					local echild = self._lpd:cn_expensivechild(cnid, i)
					local tnid = self._lpd:cn_treenodeid(echild)

					if tnid ~= -1 then
						self._treectrl:set_item_text_colour(tnid, Vector3(1, 0, 0))
					end
				end
			end

			return 
		end

		relabel(self._lpd:rootcallnode())
	end

	return 
end
LuaProfilerTreeBox._redraw = function (self)
	if self._lpd then
		self._clear(self)
		self._populate_rootnode(self)
		self._populate_plus2(self, self._lpd:rootcallnode())

		local treenodeid = self._lpd:cn_treenodeid(self._lpd:rootcallnode())

		self._treectrl:expand(treenodeid)
	end

	return 
end
LuaProfilerTreeBox._clear = function (self)
	local function clear_treenodeid(cnid)
		if self._lpd:cn_treenodeid(cnid) ~= -1 then
			self._lpd:cn_settreenodeid(cnid, -1)

			for i = 0, self._lpd:cn_numchildren(cnid) - 1, 1 do
				clear_treenodeid(self._lpd:cn_child(cnid, i))
			end
		end

		return 
	end

	clear_treenodeid(self._lpd:rootcallnode())
	self._treectrl:clear()

	return 
end
LuaProfilerTreeBox._populate_rootnode = function (self)
	local rnid = self._lpd:rootcallnode()
	local label = self._makelabel(self, rnid)
	local treerootid = self._treectrl:append_root(label)

	self._lpd:cn_settreenodeid(rnid, treerootid)

	return 
end
LuaProfilerTreeBox._populate_plus2 = function (self, cnid, plus1)
	local populated_children = false

	for i = 0, self._lpd:cn_numchildren(cnid) - 1, 1 do
		local child_id = self._lpd:cn_child(cnid, i)

		if self._lpd:cn_treenodeid(child_id) == -1 then
			populated_children = true
			local label = self._makelabel(self, child_id)
			local tnid = self._treectrl:append(self._lpd:cn_treenodeid(cnid), label)

			self._lpd:cn_settreenodeid(child_id, tnid)
		end

		if not plus1 then
			self._populate_plus2(self, child_id, true)
		end
	end

	if populated_children then
		for i = 0, self._lpd:cn_numexpensivechildren(cnid) - 1, 1 do
			local echild = self._lpd:cn_expensivechild(cnid, i)
			local tnid = self._lpd:cn_treenodeid(echild)

			self._treectrl:set_item_text_colour(tnid, Vector3(1, 0, 0))
		end
	end

	return 
end
LuaProfilerTreeBox._populate_path = function (self, cnid)
	local nodes2expand = {}

	while cnid ~= self._lpd:rootcallnode() do
		cnid = self._lpd:cn_parent(cnid)

		table.insert(nodes2expand, cnid)
	end

	for i = #nodes2expand, 1, -1 do
		self._populate_plus2(self, nodes2expand[i])
	end

	return 
end
LuaProfilerTreeBox._on_item_expanded = function (self, _, tree_event)
	local tnid = tree_event.get_item(tree_event)
	local cnid = self._lpd:find_callnode(tnid)

	self._populate_plus2(self, cnid)

	return 
end
LuaProfilerTreeBox._makelabel = function (self, cnid)
	local frametime = self._lpd:frametime()
	local label = ""

	if cnid == self._lpd:rootcallnode() then
		if self._displayformat == PERCENT then
			label = label .. string.format("%6.2f%% LUA", (self._lpd:cn_value(cnid)*100)/frametime)
		elseif self._displayformat == SECONDS then
			label = label .. string.format("%6.2fms LUA", self._lpd:cn_value(cnid)*1000)
		elseif self._displayformat == PLAIN then
			label = label .. string.format("LUA")
		end
	else
		if self._displayformat == PERCENT then
			label = label .. string.format("%6.3f%%  ", (self._lpd:cn_value(cnid)*100)/frametime)
		elseif self._displayformat == SECONDS then
			label = label .. string.format("%6.3fms  ", self._lpd:cn_value(cnid)*1000)
		elseif self._displayformat == PLAIN then
			label = label .. string.format("%s    ", self._lpd:cn_value(cnid))
		end

		local fnid = self._lpd:cn_funcnode(cnid)
		label = label .. string.format("%s (%s/%s)", self._lpd:fn_func(fnid), self._lpd:fn_file(fnid), self._lpd:fn_line(fnid))

		if 1 < self._lpd:cn_num_acc_nodes(cnid) then
			label = label .. string.format(" x%d", self._lpd:cn_num_acc_nodes(cnid))
		end
	end

	return label
end
LuaProfilerTreeBox.deselect_and_expand = function (self, ...)
	local fnid = parse_kwargs({
		...
	}, "number:fnid")

	self._treectrl:disconnect("EVT_COMMAND_TREE_ITEM_EXPANDED", self.__on_item_expanded_cb)
	self._collapse_all(self)
	self._clear_highlights(self)
	self._treectrl:unselect_all_items()

	for i = 0, self._lpd:fn_numcallnodes(fnid) - 1, 1 do
		self._populate_path(self, self._lpd:fn_callnode(fnid, i))
	end

	for i = 0, self._lpd:fn_numcallnodes(fnid) - 1, 1 do
		self._expand_path(self, self._lpd:fn_callnode(fnid, i))
		self._highlight_callnode(self, self._lpd:fn_callnode(fnid, i))
	end

	self._treectrl:connect("EVT_COMMAND_TREE_ITEM_EXPANDED", self.__on_item_expanded_cb)

	return 
end
LuaProfilerTreeBox._collapse_all = function (self)
	function collapse(cnid)
		tnid = self._lpd:cn_treenodeid(cnid)

		if tnid ~= -1 then
			self._treectrl:collapse(tnid)

			for i = 0, self._lpd:cn_numchildren(cnid) - 1, 1 do
				local child_id = self._lpd:cn_child(cnid, i)

				collapse(child_id)
			end
		end

		return 
	end

	collapse(self._lpd:rootcallnode())

	return 
end
LuaProfilerTreeBox._expand_path = function (self, cnid)
	while cnid ~= self._lpd:rootcallnode() do
		cnid = self._lpd:cn_parent(cnid)

		self._treectrl:expand(self._lpd:cn_treenodeid(cnid))
	end

	return 
end
LuaProfilerTreeBox.deselect_and_highlight = function (self, ...)
	local fnid = parse_kwargs({
		...
	}, "number:fnid")

	self._treectrl:unselect_all_items()
	self._highlight_funcnode(self, fnid)

	return 
end
LuaProfilerTreeBox._clear_highlights = function (self)
	function clear_highlight(cnid)
		local tnid = self._lpd:cn_treenodeid(cnid)

		if tnid ~= -1 then
			self._treectrl:set_item_background_colour(tnid, Vector3(1, 1, 1))

			for i = 0, self._lpd:cn_numchildren(cnid) - 1, 1 do
				local child_id = self._lpd:cn_child(cnid, i)

				clear_highlight(child_id)
			end
		end

		return 
	end

	clear_highlight(self._lpd:rootcallnode())

	return 
end
LuaProfilerTreeBox._highlight_funcnode = function (self, fnid)
	self._clear_highlights(self)

	for i = 0, self._lpd:fn_numcallnodes(fnid) - 1, 1 do
		local cnid = self._lpd:fn_callnode(fnid, i)

		self._highlight_callnode(self, cnid)
	end

	return 
end
LuaProfilerTreeBox._highlight_callnode = function (self, cnid)
	local tnid = self._lpd:cn_treenodeid(cnid)

	if tnid ~= -1 then
		self._treectrl:set_item_background_colour(tnid, HIGHLIGHTED)
	end

	return 
end
LuaProfilerTreeBox._on_select = function (self)
	local tnid = self._treectrl:selected_item()

	if tnid ~= -1 then
		local cnid = self._lpd:find_callnode(tnid)
		local fnid = self._lpd:cn_funcnode(cnid)

		self._highlight_funcnode(self, fnid)
		self._gridview:deselect_and_highlight({
			fnid = fnid
		})
	end

	return 
end

return 
