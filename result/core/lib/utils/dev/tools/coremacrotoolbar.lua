CoreMacroToolbar = CoreMacroToolbar or class()
CoreMacroToolbar.init = function (self, toolbar_panel, toolbar_box, default_icon, macro_file, self_str)
	self._toolbar_panel = toolbar_panel
	self._toolbar_box = toolbar_box
	self._default_icon = default_icon
	self._macro_file = macro_file
	self._self_str = self_str

	self.reload_macros(self)

	return 
end
CoreMacroToolbar.reload_macros = function (self)
	if self._macros then
		for name, macro in pairs(self._macros) do
			macro._ews:destroy()
		end
	end

	self._macros = {}
	local node = File:parse_xml(self._macro_file)

	if node then
		for macro in node.children(node) do
			local icon_path = macro.parameter(macro, "icon")

			if icon_path == "DEFAULT" or icon_path == "" then
				icon_path = self._default_icon
			end

			local name = macro.parameter(macro, "name")
			self._macros[name] = {
				_ews = EWS:BitmapButton(self._toolbar_panel, icon_path, "", "")
			}

			self._macros[name]._ews:set_tool_tip(name)

			self._macros[name]._func = loadstring("local self = " .. self._self_str .. "; " .. macro.parameter(macro, "code"))

			self._macros[name]._ews:connect("EVT_COMMAND_BUTTON_CLICKED", self._macros[name]._func, "")

			self._macros[name]._event = macro.parameter(macro, "event")

			self._toolbar_box:add(self._macros[name]._ews, 0, 2, "ALL")
		end

		self._toolbar_panel:layout()
	end

	return 
end
CoreMacroToolbar.trigger_event = function (self, event_name)
	if self._macros then
		for _, macro in pairs(self._macros) do
			if macro._event == event_name then
				macro._func()
			end
		end
	end

	return 
end

return 
