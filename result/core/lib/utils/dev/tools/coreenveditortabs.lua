CoreEnvEditor = CoreEnvEditor or class()
CoreEnvEditor.create_interface = function (self)
	local gui = self.add_sky_param(self, "sun_ray_color", EnvEdColorBox:new(self, self.get_tab(self, "Global illumination"), "Sun color"))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_sky_param(self, "sun_ray_color_scale", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Sun intensity", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "fog_start_color", EnvEdColorBox:new(self, self.get_tab(self, "Global illumination"), "Fog start color"))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "fog_far_low_color", EnvEdColorBox:new(self, self.get_tab(self, "Global illumination"), "Fog far low color"))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "fog_min_range", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Fog min range", nil, 0, 5000, 1, 1))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "fog_max_range", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Fog max range", nil, 0, 500000, 1, 1))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "fog_max_density", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Fog max density", nil, 0, 1000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "sky_top_color", EnvEdColorBox:new(self, self.get_tab(self, "Global illumination"), "Ambient top color"))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "sky_top_color_scale", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Ambient top scale", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "sky_bottom_color", EnvEdColorBox:new(self, self.get_tab(self, "Global illumination"), "Ambient bottom color"))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "sky_bottom_color_scale", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Ambient bottom scale", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "ambient_color", EnvEdColorBox:new(self, self.get_tab(self, "Global illumination"), "Ambient color"))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "ambient_color_scale", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Ambient color scale", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "ambient_scale", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Ambient scale", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "ambient_falloff_scale", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Ambient falloff scale", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_post_processors_param(self, "deferred", "deferred_lighting", "apply_ambient", "effect_light_scale", SingelSlider:new(self, self.get_tab(self, "Global illumination"), "Effect lighting scale", nil, 0, 1000, 1000, 1000))

	self.add_gui_element(self, gui, "Global illumination", "Global lighting")

	gui = self.add_underlay_param(self, "sky", "color0", EnvEdColorBox:new(self, self.get_tab(self, "Skydome"), "Color top"))

	self.add_gui_element(self, gui, "Skydome", "Sky")

	gui = self.add_underlay_param(self, "sky", "color0_scale", SingelSlider:new(self, self.get_tab(self, "Skydome"), "Color top scale", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Skydome", "Sky")

	gui = self.add_underlay_param(self, "sky", "color2", EnvEdColorBox:new(self, self.get_tab(self, "Skydome"), "Color low"))

	self.add_gui_element(self, gui, "Skydome", "Sky")

	gui = self.add_underlay_param(self, "sky", "color2_scale", SingelSlider:new(self, self.get_tab(self, "Skydome"), "Color low scale", nil, 0, 10000, 1000, 1000))

	self.add_gui_element(self, gui, "Skydome", "Sky")

	local gui = self.add_sky_param(self, "underlay", PathBox:new(self, self.get_tab(self, "Skydome"), "Underlay"))

	self.add_gui_element(self, gui, "Skydome", "Sky")

	gui = self.add_sky_param(self, "global_texture", DBPickDialog:new(self, self.get_tab(self, "Skydome"), "Global cubemap", "texture"))

	self.add_gui_element(self, gui, "Skydome", "Sky")

	gui = self.add_sky_param(self, "global_world_overlay_texture", DBPickDialog:new(self, self.get_tab(self, "Global textures"), "Global world overlay texture", "texture"))

	self.add_gui_element(self, gui, "Global textures", "World")

	gui = self.add_sky_param(self, "global_world_overlay_mask_texture", DBPickDialog:new(self, self.get_tab(self, "Global textures"), "Global world overlay mask texture", "texture"))

	self.add_gui_element(self, gui, "Global textures", "World")

	return 
end
CoreEnvEditor.create_simple_interface = function (self)
	return 
end

return 
