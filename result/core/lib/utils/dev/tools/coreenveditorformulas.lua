CoreEnvEditor = CoreEnvEditor or class()
CoreEnvEditor.lerp_formula = function (value, v)
	return math.lerp(value, Vector3(1, 1, 1), v or 0.5)
end

return 
