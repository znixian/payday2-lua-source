require("core/lib/utils/dev/ews/tree_control/CoreFilteredTreeControl")

CoreFilteredTreePanel = CoreFilteredTreePanel or class()
CoreFilteredTreePanel.init = function (self, parent_frame)
	self._create_panel(self, parent_frame)

	self._tree_refresh_timeout = 0

	return 
end
CoreFilteredTreePanel.add_to_sizer = function (self, sizer, proportion, border, flags)
	return sizer.add(sizer, self._panel, proportion, border, flags)
end
CoreFilteredTreePanel.connect = function (self, event_type, script_callback, script_data)
	return self._tree_control(self):connect(event_type, script_callback, script_data)
end
CoreFilteredTreePanel.update = function (self, time, delta_time)
	if 0 < self._tree_refresh_timeout then
		self._tree_refresh_timeout = self._tree_refresh_timeout - delta_time

		if self._tree_refresh_timeout <= 0 and self._tree_control(self) then
			self._tree_refresh_timeout = 0

			self._tree_control(self):refresh_tree()
		end
	end

	return 
end
CoreFilteredTreePanel._tree_control = function (self)
	return self._filtered_tree_control
end
CoreFilteredTreePanel._create_panel = function (self, parent_frame)
	self._panel = EWS:Panel(parent_frame, "", "")
	local panel_sizer = EWS:BoxSizer("VERTICAL")

	self._panel:set_sizer(panel_sizer)

	local filter_panel, filter_text_ctrl = self._create_filter_bar_panel(self, self._panel)

	panel_sizer.add(panel_sizer, filter_panel, 0, 0, "EXPAND")

	self._filtered_tree_control = CoreFilteredTreeControl:new(self._panel)

	self._filtered_tree_control:add_to_sizer(panel_sizer, 1, 0, "EXPAND")
	self._filtered_tree_control:add_filter(function (node)
		return string.find(node.path(node), filter_text_ctrl:get_value(), 1, true)
	end)
	filter_text_ctrl.connect(filter_text_ctrl, "EVT_COMMAND_TEXT_UPDATED", callback(self, self, "_on_filter_text_updated"))
	filter_text_ctrl.connect(filter_text_ctrl, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "_on_filter_enter_pressed"))

	return 
end
CoreFilteredTreePanel._create_filter_bar_panel = function (self, parent_frame)
	local panel = EWS:Panel(parent_frame, "", "")
	local panel_sizer = EWS:BoxSizer("HORIZONTAL")

	panel.set_sizer(panel, panel_sizer)

	local image = EWS:BitmapButton(panel, CoreEWS.image_path("magnifying_glass_32x32.png"), "", "NO_BORDER")

	panel_sizer.add(panel_sizer, image, 0, 5, "TOP,BOTTOM,LEFT,EXPAND")

	local filter_text_ctrl = EWS:TextCtrl(panel, "", "", "")

	panel_sizer.add(panel_sizer, filter_text_ctrl, 1, 5, "LEFT,RIGHT,ALIGN_CENTER_VERTICAL")

	return panel, filter_text_ctrl
end
CoreFilteredTreePanel._on_filter_text_updated = function (self)
	self._tree_refresh_timeout = 0.25

	return 
end
CoreFilteredTreePanel._on_filter_enter_pressed = function (self)
	if 0 < self._tree_refresh_timeout then
		self._tree_refresh_timeout = 0

		self._tree_control(self):refresh_tree()
	end

	self._tree_control(self):expand(true)

	return 
end

return 
