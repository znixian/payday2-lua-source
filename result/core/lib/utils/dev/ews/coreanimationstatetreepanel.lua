require("core/lib/utils/dev/ews/CoreFilteredTreePanel")

CoreAnimationStateTreePanel = CoreAnimationStateTreePanel or class(CoreFilteredTreePanel)
CoreAnimationStateTreePanel.init = function (self, parent_frame, unit)
	self.super.init(self, parent_frame)
	self.set_unit(self, unit)

	return 
end
CoreAnimationStateTreePanel.unit = function (self)
	return self._unit
end
CoreAnimationStateTreePanel.set_unit = function (self, unit)
	self._unit = unit

	self._refresh_tree(self)

	return 
end
CoreAnimationStateTreePanel._refresh_tree = function (self)
	self._tree_control(self):freeze()
	self._tree_control(self):clear()

	if self.unit(self) then
		local state_machine = self.unit(self):anim_state_machine()

		if state_machine then
			local sorted_states = table.sorted_copy(state_machine.config(state_machine):states(), function (a, b)
				return a.name(a):s() < b.name(b):s()
			end)

			for _, state in ipairs(sorted_states) do
				self._tree_control(self):append_path(state.name(state):s())
			end
		end
	end

	self._tree_control(self):thaw()

	return 
end

return 
