CoreXMLTextCtrl = CoreXMLTextCtrl or class()
CoreXMLTextCtrl.init = function (self, parent, value, id, syntax_colors, style)
	if syntax_colors then
		self._syntax_colors = syntax_colors
	else
		self._syntax_colors = {
			NODE = Vector3(50, 50, 255),
			KEY = Vector3(250, 50, 50),
			VALUE = Vector3(0, 0, 0),
			NORMAL = Vector3(0, 0, 0)
		}
	end

	self._text_ctrl = EWS:TextCtrl(parent, value or "", id or "", style or "TE_MULTILINE,TE_RICH2,TE_DONTWRAP")

	self._text_ctrl:set_default_style_font_family("FONTFAMILY_TELETYPE")
	self._text_ctrl:set_default_style_font_size(10)

	return 
end
CoreXMLTextCtrl.update = function (self, t)
	self._skip_event = nil

	if self._check_syntax then
		if self._last_syntax_check then
			if 3 < t - self._last_syntax_check then
				local text = self._text_ctrl:get_value()
				self._last_syntax_check = nil
				self._check_syntax = nil
				self._skip_event = true

				if Node("node"):try_read_xml(text) then
					local node = Node("node")

					node.read_xml(node, "<n>\n" .. text .. "</n>\n")

					if 0 < node.num_children(node) then
						self._text_ctrl:freeze()
						self._text_ctrl:set_value("")
						self._draw_text_with_color(self, "", node)
						self._set_tc_color(self, "NORMAL")
						self._text_ctrl:thaw()
						self._text_ctrl:set_insertion_point(0)
						self._text_ctrl:show_position(0)
						self._text_ctrl:update()
					end

					self._xml_ok = true
				else
					self._xml_ok = nil
				end
			end
		else
			self._last_syntax_check = t
		end
	end

	return 
end
CoreXMLTextCtrl.text_ctrl = function (self)
	return self._text_ctrl
end
CoreXMLTextCtrl.xml_ok = function (self)
	return self._xml_ok == true
end
CoreXMLTextCtrl.check = function (self)
	self._check_syntax = true
	self._last_syntax_check = 0

	return 
end
CoreXMLTextCtrl.set_value = function (self, value)
	self._text_ctrl:set_value(value)
	self.check(self)
	self.update(self, math.huge)

	return 
end
CoreXMLTextCtrl._on_text_change = function (self)
	if not self._skip_event then
		self._check_syntax = true
		self._last_syntax_check = nil
	end

	return 
end
CoreXMLTextCtrl._draw_text_with_color = function (self, level, node)
	for child in node.children(node) do
		self._set_tc_color(self, "NODE")
		self._text_ctrl:append(level .. "<" .. child.name(child))

		for k, v in pairs(child.parameters(child)) do
			self._set_tc_color(self, "KEY")
			self._text_ctrl:append(" " .. k .. "=")
			self._set_tc_color(self, "VALUE")
			self._text_ctrl:append("\"" .. v .. "\"")
		end

		self._set_tc_color(self, "NODE")

		if child.num_children(child) == 0 then
			self._text_ctrl:append("/>\n")
		else
			self._text_ctrl:append(">\n")
			self._draw_text_with_color(self, level .. "    ", child)
			self._text_ctrl:append(level .. "</" .. child.name(child) .. ">\n")
		end
	end

	return 
end
CoreXMLTextCtrl._set_tc_color = function (self, color)
	self._text_ctrl:set_default_style_colour(self._syntax_colors[color])

	return 
end

return 
