require("core/lib/utils/dev/ews/tree_control/CoreEWSTreeCtrlTreeNode")

CoreManagedTreeControl = CoreManagedTreeControl or class()
CoreManagedTreeControl.CHECKBOX_STYLE_STR = "TR_HAS_CHECKBOX"
CoreManagedTreeControl.CHECKBOX_UPDATED_EVENT_NAME = "EVT_COMMAND_TREE_CHECKBOX_UPDATED"
CoreManagedTreeControl.CHECKBOX_STATE0_ICON = CoreEWS.image_path("tree_checkbox_unchecked_16x16.png")
CoreManagedTreeControl.CHECKBOX_STATE1_ICON = CoreEWS.image_path("tree_checkbox_checked_16x16.png")
CoreManagedTreeControl.init = function (self, parent_frame, styles)
	self._styles, self._checkbox_style = self._eat_checkbox_style(self, styles or "TR_HAS_BUTTONS,TR_HIDE_ROOT,TR_LINES_AT_ROOT,TR_SINGLE")
	self._tree_ctrl = EWS:TreeCtrl(parent_frame, "", self._styles)

	self._init_checkbox_icons(self)

	if self._checkbox_style then
		self._tree_ctrl:connect("", "EVT_LEFT_DOWN", callback(self, self, "_change_state"), "")
		self._tree_ctrl:connect("", "EVT_COMMAND_TREE_ITEM_GETTOOLTIP", callback(self, self, "_tooltip"), "")
	end

	self._visible_root_node = CoreEWSTreeCtrlTreeNode:new(self._tree_ctrl, self._tree_ctrl:append_root("hidden_root"), self._checkbox_style)
	self._tree_event_wrapper_map = {}
	self._custom_callbacks = {}
	self._tooltips = {}

	return 
end
CoreManagedTreeControl._eat_checkbox_style = function (self, styles)
	local checkbox_style = false
	local ret = ""

	for style in string.gmatch(styles, "[%w_]+") do
		if style == self.CHECKBOX_STYLE_STR then
			checkbox_style = true
		else
			ret = ret .. "," .. style
		end
	end

	return ret, checkbox_style
end
CoreManagedTreeControl._init_checkbox_icons = function (self)
	if self._checkbox_style then
		self.set_image_list(self, EWS:ImageList(16, 16, false))
		self._image_list:add(self.CHECKBOX_STATE0_ICON)
		self._image_list:add(self.CHECKBOX_STATE1_ICON)
	end

	return 
end
CoreManagedTreeControl._change_state = function (self, data, event)
	local id, hit = self._tree_ctrl:hit_test()

	if -1 < id then
		if hit == "ONITEMICON" then
			self._tree_ctrl:freeze()

			local state = self._tree_ctrl:get_item_image(id, "NORMAL")

			if state == 0 then
				state = 1
			elseif state == 1 then
				state = 0
			else
				self._tree_ctrl:thaw()
				self._tree_ctrl:update()

				return 
			end

			self._tree_ctrl:set_item_image(id, state, "NORMAL")
			self._tree_ctrl:thaw()
			self._tree_ctrl:update()
			self._find_and_do_custom_callback(self, self.CHECKBOX_UPDATED_EVENT_NAME, {
				_state = state,
				_id = id
			})

			return 
		elseif hit == "ONITEMBUTTON" then
			if self._tree_ctrl:is_expanded(id) then
				self._tree_ctrl:collapse(id)
			else
				self._tree_ctrl:expand(id)
			end

			return 
		end
	end

	event.skip(event)

	return 
end
CoreManagedTreeControl._tooltip = function (self, data, event)
	local id, hit = self._tree_ctrl:hit_test()

	if -1 < id and (hit == "ONITEMICON" or hit == "ONITEMLABEL") then
		event.set_tool_tip(event, self._tooltips[tostring(id)] or "")
	end

	return 
end
CoreManagedTreeControl._find_and_do_custom_callback = function (self, cb_type, data)
	for k, v in pairs(self._custom_callbacks) do
		if v._event_type == cb_type then
			k(v._script_data, data)
		end
	end

	return 
end
CoreManagedTreeControl._view_tree_root = function (self)
	return self._visible_root_node
end
CoreManagedTreeControl._tree_root = function (self)
	return self._visible_root_node
end
CoreManagedTreeControl.set_tooltip = function (self, node, text)
	self._tooltips[tostring(node._item_id)] = text

	return 
end
CoreManagedTreeControl.set_image_list = function (self, list)
	self._image_list = list

	self._tree_ctrl:set_image_list(list)

	return 
end
CoreManagedTreeControl.get_image_list = function (self)
	return self._image_list
end
CoreManagedTreeControl.clear = function (self)
	self._tree_root(self):remove_children()

	for k, v in pairs(self._custom_callbacks) do
		if v._event_type == self.CHECKBOX_UPDATED_EVENT_NAME then
			self._custom_callbacks[k] = nil
		end
	end

	return 
end
CoreManagedTreeControl.set_size = function (self, size)
	self._tree_ctrl:set_size(size)

	return 
end
CoreManagedTreeControl.set_min_size = function (self, size)
	self._tree_ctrl:set_min_size(size)

	return 
end
CoreManagedTreeControl.add_to_sizer = function (self, sizer, proportion, border, flags)
	return sizer.add(sizer, self._tree_ctrl, proportion, border, flags)
end
CoreManagedTreeControl.detach_from_sizer = function (self, sizer)
	return sizer.detach(sizer, self._tree_ctrl)
end
CoreManagedTreeControl.freeze = function (self)
	return self._tree_ctrl:freeze()
end
CoreManagedTreeControl.thaw = function (self)
	return self._tree_ctrl:thaw()
end
CoreManagedTreeControl.append = function (self, item_text)
	return self._tree_root(self):append(item_text)
end
CoreManagedTreeControl.append_path = function (self, path)
	return self._tree_root(self):append_path(path)
end
CoreManagedTreeControl.append_copy_of_node = function (self, node, recurse)
	return self._tree_root(self):append_copy_of_node(node, recurse)
end
CoreManagedTreeControl.expand = function (self, recurse)
	self._view_tree_root(self):expand(recurse)

	return 
end
CoreManagedTreeControl.collapse = function (self, recurse)
	self._view_tree_root(self):collapse(recurse)

	return 
end
CoreManagedTreeControl.root_nodes = function (self)
	return self._tree_root(self):children()
end
CoreManagedTreeControl.root_node = function (self)
	return self._tree_root(self)
end
CoreManagedTreeControl.connect = function (self, event_type, script_callback, script_data)
	if event_type == self.CHECKBOX_UPDATED_EVENT_NAME then
		self._custom_callbacks[script_callback] = {
			_event_type = event_type,
			_script_data = script_data
		}
	elseif string.begins(event_type, "EVT_COMMAND_TREE_") then
		local function tree_event_wrapper(data, event, ...)
			local event_metatable = getmetatable(event) or {}
			local wrapped_event = setmetatable({}, {
				__index = event_metatable.__index
			})
			local item_id = event.get_item(event)
			local old_item_id = event.get_old_item(event)
			wrapped_event.get_item = function ()
				return (item_id ~= -1 and CoreEWSTreeCtrlTreeNode:new(self._tree_ctrl, item_id, self._checkbox_style)) or nil
			end
			wrapped_event.get_old_item = function ()
				return (old_item_id ~= -1 and CoreEWSTreeCtrlTreeNode:new(self._tree_ctrl, old_item_id, self._checkbox_style)) or nil
			end

			return script_callback(data, wrapped_event, ...)
		end

		if not self._tree_event_wrapper_map[script_callback] then
			self._tree_event_wrapper_map[script_callback] = tree_event_wrapper

			self._tree_ctrl:connect(event_type, tree_event_wrapper, script_data)
		end
	else
		self._tree_ctrl:connect(event_type, script_callback, script_data)
	end

	return 
end
CoreManagedTreeControl.disconnect = function (self, event_type, script_callback)
	if event_type == self.CHECKBOX_UPDATED_EVENT_NAME then
		self._custom_callbacks[script_callback] = nil
	elseif string.begins(event_type, "EVT_COMMAND_TREE_") then
		local resolved_wrapper = self._tree_event_wrapper_map[script_callback]

		if resolved_wrapper then
			self._tree_ctrl:disconnect(event_type, resolved_wrapper)

			self._tree_event_wrapper_map[script_callback] = nil
		end
	else
		self._tree_ctrl:disconnect(event_type, script_callback)
	end

	return 
end

return 
