require("core/lib/utils/dev/ews/tree_control/CoreManagedTreeControl")
require("core/lib/utils/dev/ews/tree_control/CoreTreeNode")

CoreFilteredTreeControl = CoreFilteredTreeControl or class(CoreManagedTreeControl)
CoreFilteredTreeControl.init = function (self, parent_frame, styles)
	self.super.init(self, parent_frame, styles)

	self._virtual_root_node = CoreTreeNode:new()

	self._virtual_root_node:set_callback("on_append", callback(self, self, "_on_node_appended"))
	self._virtual_root_node:set_callback("on_remove", callback(self, self, "_on_node_removed"))

	self._filters = {}
	self._freeze_count = 0

	return 
end
CoreFilteredTreeControl.add_filter = function (self, predicate)
	table.insert(self._filters, predicate)
	self.refresh_tree(self)

	return predicate
end
CoreFilteredTreeControl.remove_filter = function (self, predicate)
	table.delete(self._filters, predicate)
	self.refresh_tree(self)

	return 
end
CoreFilteredTreeControl.refresh_tree = function (self)
	if self._freeze_count ~= 0 then
		return 
	end

	self.freeze(self)
	self._view_tree_root(self):remove_children()

	local function append_to_visible_tree(child)
		if self:_node_passes_filters(child) then
			self:_view_tree_root():append_path(child.path(child))
		end

		return true
	end

	self._tree_root(self):for_each_child(append_to_visible_tree, true)
	self.thaw(self, true)

	return 
end
CoreFilteredTreeControl._node_passes_filters = function (self, node)
	for _, predicate in ipairs(self._filters) do
		if not predicate(node) then
			return false
		end
	end

	return true
end
CoreFilteredTreeControl._on_node_appended = function (self, new_node)
	local visible_parent_node = self._view_tree_root(self)

	if new_node.parent(new_node) then
		visible_parent_node = self._view_tree_root(self):child_at_path(new_node.parent(new_node):path())
	end

	if visible_parent_node and self._node_passes_filters(self, new_node) then
		visible_parent_node.append_copy_of_node(visible_parent_node, new_node)
	end

	return 
end
CoreFilteredTreeControl._on_node_removed = function (self, removed_node)
	local visible_node = self._view_tree_root(self):child_at_path(removed_node.path(removed_node))

	if visible_node then
		visible_node.remove(visible_node)
	end

	return 
end
CoreFilteredTreeControl.clear = function (self)
	self.super.clear(self)
	self.refresh_tree(self)

	return 
end
CoreFilteredTreeControl._tree_root = function (self)
	return self._virtual_root_node
end
CoreFilteredTreeControl.freeze = function (self)
	if self._freeze_count == 0 then
		self.super.freeze(self)
	end

	self._freeze_count = self._freeze_count + 1

	return 
end
CoreFilteredTreeControl.thaw = function (self, already_refreshed)
	self._freeze_count = self._freeze_count - 1

	if self._freeze_count == 0 then
		if not already_refreshed then
			self.refresh_tree(self)
		end

		self.super.thaw(self)
	end

	return 
end

return 
