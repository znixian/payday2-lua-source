CoreBaseTreeNode = CoreBaseTreeNode or class()
CoreBaseTreeNode.path = function (self, separator)
	separator = separator or "/"
	local parent = self.parent(self)

	if parent then
		return parent.path(parent) .. separator .. self.text(self)
	end

	return self.text(self)
end
CoreBaseTreeNode.has_children = function (self)
	return 0 < table.getn(self.children(self))
end
CoreBaseTreeNode.child = function (self, text, separator)
	for _, child in ipairs(self.children(self)) do
		if child.text(child) == text then
			return child
		end
	end

	return 
end
CoreBaseTreeNode.child_at_path = function (self, path, separator)
	separator = separator or "/"
	local first_path_component, remaining_path = string.split(path, separator, false, 1)
	local child = self.child(self, first_path_component)

	if child and remaining_path then
		return child.child_at_path(child, remaining_path, separator)
	end

	return child
end
CoreBaseTreeNode.append_path = function (self, path, separator)
	separator = separator or "/"
	local first_path_component, remaining_path = unpack(string.split(path, separator, false, 1))
	local node = self.child(self, first_path_component) or self.append(self, first_path_component)

	if remaining_path then
		return node.append_path(node, remaining_path, separator)
	end

	return node
end
CoreBaseTreeNode.append_copy_of_node = function (self, node, recurse)
	local new_node = self.append(self, node.text(node))

	if recurse then
		for _, child in ipairs(node.children(node)) do
			new_node.append_copy_of_node(new_node, child, true)
		end
	end

	return new_node
end
CoreBaseTreeNode.for_each_child = function (self, func, recurse)
	for _, child in ipairs(table.list_copy(self.children(self))) do
		if not func(child) then
			break
		end

		if recurse then
			child.for_each_child(child, func, true)
		end
	end

	return 
end
CoreBaseTreeNode.remove_children = function (self)
	for _, child in ipairs(table.list_copy(self.children(self))) do
		child.remove(child)
	end

	return 
end

return 
