require("core/lib/utils/dev/ews/tree_control/CoreBaseTreeNode")

CoreEWSTreeCtrlTreeNode = CoreEWSTreeCtrlTreeNode or class(CoreBaseTreeNode)
CoreEWSTreeCtrlTreeNode.init = function (self, ews_tree_ctrl, item_id, checkbox_style)
	self._checkbox_style = checkbox_style
	self._tree_ctrl = assert(ews_tree_ctrl, "nil argument supplied as ews_tree_ctrl")
	self._item_id = assert(item_id, "nil argument supplied as item_id")

	self.set_state(self, 0)

	return 
end
CoreEWSTreeCtrlTreeNode.id = function (self)
	return self._item_id
end
CoreEWSTreeCtrlTreeNode.expand = function (self, recurse)
	self._tree_ctrl:expand(self._item_id)

	if recurse then
		for _, child in ipairs(self.children(self)) do
			child.expand(child, true)
		end
	end

	return 
end
CoreEWSTreeCtrlTreeNode.collapse = function (self, recurse)
	self._tree_ctrl:collapse(self._item_id)

	if recurse then
		for _, child in ipairs(self.children(self)) do
			child.collapse(child, true)
		end
	end

	return 
end
CoreEWSTreeCtrlTreeNode.set_selected = function (self, state)
	if state == nil then
		state = true
	end

	self._tree_ctrl:select_item(self._item_id, state)

	return 
end
CoreEWSTreeCtrlTreeNode.state = function (self, state)
	if self._checkbox_style then
		return self._tree_ctrl:get_item_image(self._item_id, "NORMAL")
	else
		return 0
	end

	return 
end
CoreEWSTreeCtrlTreeNode.set_state = function (self, state)
	if self._checkbox_style then
		self._change_state(self, state)
	end

	return 
end
CoreEWSTreeCtrlTreeNode.checkbox_style = function (self)
	return self._checkbox_style
end
CoreEWSTreeCtrlTreeNode.set_checkbox_style = function (self, style)
	self._checkbox_style = style

	return 
end
CoreEWSTreeCtrlTreeNode.set_image = function (self, image, item_state)
	self._tree_ctrl:set_item_image(self._item_id, image, item_state or "NORMAL")

	return 
end
CoreEWSTreeCtrlTreeNode.get_image = function (self, item_state)
	return self._tree_ctrl:get_item_image(self._item_id, item_state or "NORMAL")
end
CoreEWSTreeCtrlTreeNode._change_state = function (self, state)
	self._tree_ctrl:set_item_image(self._item_id, state, "NORMAL")

	return 
end
CoreEWSTreeCtrlTreeNode.text = function (self)
	return self._tree_ctrl:get_item_text(self._item_id)
end
CoreEWSTreeCtrlTreeNode.parent = function (self)
	local parent_id = self._tree_ctrl:get_parent(self._item_id)

	if parent_id ~= -1 and self._tree_ctrl:get_parent(parent_id) ~= -1 then
		return CoreEWSTreeCtrlTreeNode:new(self._tree_ctrl, parent_id, self._checkbox_style)
	end

	return 
end
CoreEWSTreeCtrlTreeNode.children = function (self)
	return table.collect(self._tree_ctrl:get_children(self._item_id), function (child_id)
		return CoreEWSTreeCtrlTreeNode:new(self._tree_ctrl, child_id, self._checkbox_style)
	end)
end
CoreEWSTreeCtrlTreeNode.append = function (self, text)
	return CoreEWSTreeCtrlTreeNode:new(self._tree_ctrl, self._tree_ctrl:append(self._item_id, text), self._checkbox_style)
end
CoreEWSTreeCtrlTreeNode.remove = function (self)
	self._tree_ctrl:remove(self._item_id)

	return 
end
CoreEWSTreeCtrlTreeNode.has_children = function (self)
	return 0 < table.getn(self._tree_ctrl:get_children(self._item_id))
end

return 
