require("core/lib/utils/dev/ews/tree_control/CoreBaseTreeNode")

CoreTreeNode = CoreTreeNode or class(CoreBaseTreeNode)
CoreTreeNode.init = function (self, text)
	self._text = text or ""
	self._parent = nil
	self._children = {}

	return 
end
CoreTreeNode.set_callback = function (self, event_name, func)
	self._callbacks = self._callbacks or {}
	self._callbacks[event_name] = func

	return 
end
CoreTreeNode._invoke_callback = function (self, event_name, ...)
	local callback_table = self._callbacks

	if not callback_table then
		return 
	end

	local callback = callback_table[event_name]

	if callback then
		callback(...)
	end

	return 
end
CoreTreeNode.text = function (self)
	return self._text
end
CoreTreeNode.parent = function (self)
	if self._parent and self._parent._parent then
		return self._parent
	end

	return 
end
CoreTreeNode.remove_children = function (self)
	return self.super.remove_children(self)
end
CoreTreeNode.children = function (self)
	return self._children
end
CoreTreeNode.append = function (self, text)
	local new_node = CoreTreeNode:new(text)
	new_node._callbacks = self._callbacks
	new_node._parent = self

	table.insert(self._children, new_node)
	self._invoke_callback(self, "on_append", new_node)

	return new_node
end
CoreTreeNode.remove = function (self)
	self._invoke_callback(self, "on_remove", self)

	if self._parent then
		table.delete(self._parent._children, self)
	end

	self._text = ""
	self._parent = nil
	self._children = {}

	return 
end

return 
