core:module("CoreColorPickerDialog")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreEws")
core:import("CoreColorPickerPanel")

ColorPickerDialog = ColorPickerDialog or CoreClass.mixin(CoreClass.class(), CoreEvent.BasicEventHandling)
ColorPickerDialog.EDITOR_TITLE = "Color Picker"
ColorPickerDialog.init = function (self, parent_frame, enable_alpha, orientation, enable_value)
	orientation = orientation or "HORIZONTAL"

	assert(orientation == "HORIZONTAL" or orientation == "VERTICAL", "Invalid orientation.")

	local frame_size = (orientation == "HORIZONTAL" and Vector3(366, 166)) or Vector3(186, 300, 0)
	self._window = EWS:Frame(ColorPickerDialog.EDITOR_TITLE, Vector3(-1, -1, 0), frame_size, "SYSTEM_MENU,CAPTION,CLOSE_BOX,CLIP_CHILDREN", parent_frame)
	local sizer = EWS:BoxSizer("HORIZONTAL")

	self._window:set_sizer(sizer)
	self._window:set_icon(CoreEws.image_path("toolbar/color_16x16.png"))
	self._window:connect("", "EVT_CLOSE_WINDOW", CoreEvent.callback(self, self, "_on_close"), "")

	self._picker_panel = CoreColorPickerPanel.ColorPickerPanel:new(self._window, enable_alpha, orientation, enable_value)

	self._picker_panel:connect("EVT_COLOR_UPDATED", CoreEvent.callback(self, self, "_on_color_updated"), self._picker_panel)
	self._picker_panel:connect("EVT_COLOR_CHANGED", CoreEvent.callback(self, self, "_on_color_changed"), self._picker_panel)
	sizer.add(sizer, self._picker_panel:panel(), 0, 0, "EXPAND")
	self.set_visible(self, true)

	return 
end
ColorPickerDialog.update = function (self, time, delta_time)
	self._picker_panel:update(time, delta_time)

	return 
end
ColorPickerDialog.color = function (self)
	return self._picker_panel:color()
end
ColorPickerDialog.set_color = function (self, color)
	self._picker_panel:set_color(color)

	return 
end
ColorPickerDialog.set_position = function (self, newpos)
	self._window:set_position(newpos)

	return 
end
ColorPickerDialog.set_visible = function (self, visible)
	self._window:set_visible(visible)

	return 
end
ColorPickerDialog.center = function (self, window)
	self._window:set_position((window.get_position(window) + window.get_size(window)*0.5) - self._window:get_size()*0.5)

	return 
end
ColorPickerDialog.close = function (self)
	self._window:destroy()

	return 
end
ColorPickerDialog._on_color_updated = function (self, sender, color)
	self._send_event(self, "EVT_COLOR_UPDATED", color)

	return 
end
ColorPickerDialog._on_color_changed = function (self, sender, color)
	self._send_event(self, "EVT_COLOR_CHANGED", color)

	return 
end
ColorPickerDialog._on_close = function (self)
	self._window:set_visible(false)
	self._send_event(self, "EVT_CLOSE_WINDOW", self._window)
	managers.toolhub:close(ColorPickerDialog.EDITOR_TITLE)

	return 
end

return 
