core:module("CoreColorPickerDraggables")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreMath")

ColorPickerDraggables = ColorPickerDraggables or CoreClass.mixin(CoreClass.class(), CoreEvent.BasicEventHandling)
ColorPickerDraggables.init = function (self, parent_frame, enable_alpha, enable_value)
	self._create_panel(self, parent_frame, enable_alpha, enable_value)
	self.set_color(self, Color.white)

	return 
end
ColorPickerDraggables.update = function (self, time, delta_time)
	local current_mouse_event = EWS:MouseEvent("EVT_MOTION")
	self._previous_mouse_event = self._previous_mouse_event or current_mouse_event

	if self._dragged_control and current_mouse_event.get_position(current_mouse_event) ~= self._previous_mouse_event:get_position() then
		self._on_dragging(self, self._dragged_control, current_mouse_event)
	end

	if self._dragged_control and current_mouse_event.left_is_down(current_mouse_event) == false and self._previous_mouse_event:left_is_down() == true then
		self._on_drag_stop(self, self._dragged_control, current_mouse_event)
	end

	self._previous_mouse_event = current_mouse_event

	return 
end
ColorPickerDraggables.panel = function (self)
	return self._panel
end
ColorPickerDraggables.color = function (self)
	return self._alpha_slider or self._value_slider or self._spectrum:color()
end
ColorPickerDraggables.set_color = function (self, color)
	hue, saturation, value = CoreMath.rgb_to_hsv(color.red, color.green, color.blue)

	self._spectrum:set_hue(hue)
	self._spectrum:set_saturation(saturation)
	self._spectrum:set_value(value)

	if self._value_slider then
		self._value_slider:set_value(value)
	end

	if self._alpha_slider then
		self._alpha_slider:set_value(color.alpha)
	end

	self._update_ui_except(self, self._spectrum)

	return 
end
ColorPickerDraggables._create_panel = function (self, parent_frame, enable_alpha, enable_value)
	if enable_alpha == nil then
		enable_alpha = true
	end

	if enable_value == nil then
		enable_value = true
	end

	self._panel = EWS:Panel(parent_frame)

	self._panel:set_min_size(Vector3(180, 134, 0))

	local panel_sizer = EWS:BoxSizer("HORIZONTAL")

	self._panel:set_sizer(panel_sizer)

	local slider_width = 20
	local slider_margin = 3
	self._spectrum = EWS:ColorSpectrum(self._panel, "")

	panel_sizer.add(panel_sizer, self._spectrum, 0, slider_margin, "ALL")
	self._spectrum:connect("EVT_LEFT_DOWN", CoreEvent.callback(self, self, "_on_drag_start"), self._spectrum)

	local spectrum_size = self._spectrum:get_min_size()

	if enable_value then
		self._value_slider = EWS:ColorSlider(self._panel, "")

		panel_sizer.add(panel_sizer, self._value_slider, 0, 0, "EXPAND")
		self._value_slider:connect("EVT_LEFT_DOWN", CoreEvent.callback(self, self, "_on_drag_start"), self._value_slider)
	else
		spectrum_size = spectrum_size.with_x(spectrum_size, spectrum_size.x + slider_width + slider_margin)
	end

	if enable_alpha then
		self._alpha_slider = EWS:ColorSlider(self._panel, "")

		panel_sizer.add(panel_sizer, self._alpha_slider, 0, slider_margin, "LEFT,RIGHT,EXPAND")
		self._alpha_slider:connect("EVT_LEFT_DOWN", CoreEvent.callback(self, self, "_on_drag_start"), self._alpha_slider)
	else
		spectrum_size = spectrum_size.with_x(spectrum_size, spectrum_size.x + slider_width + slider_margin)
	end

	self._spectrum:set_min_size(spectrum_size)

	return 
end
ColorPickerDraggables._update_ui_except = function (self, sender)
	if self._value_slider ~= nil then
		if sender ~= self._spectrum then
			self._spectrum:set_value(self._value_slider:value())
		end

		if sender ~= self._value_slider then
			self._value_slider:set_top_color(self._spectrum:unscaled_color())
		end
	end

	if sender ~= self._alpha_slider and self._alpha_slider ~= nil then
		local opaque_color = self._value_slider or self._spectrum:color()

		self._alpha_slider:set_top_color(opaque_color)
		self._alpha_slider:set_bottom_color(opaque_color.with_alpha(opaque_color, 0))
	end

	return 
end
ColorPickerDraggables._process_color_update_event = function (self, sender, event)
	local event_position_in_sender = event.get_position(event, sender)

	if sender == self._spectrum then
		sender.set_hue(sender, sender.point_to_hue(sender, event_position_in_sender))
		sender.set_saturation(sender, sender.point_to_saturation(sender, event_position_in_sender))
	else
		sender.set_value(sender, sender.point_to_value(sender, event_position_in_sender))
	end

	self._update_ui_except(self, sender)
	self._send_event(self, "EVT_COLOR_UPDATED", self.color(self))

	return 
end
ColorPickerDraggables._process_color_change_event = function (self, sender, event)
	self._process_color_update_event(self, sender, event)
	self._send_event(self, "EVT_COLOR_CHANGED", self.color(self))

	return 
end
ColorPickerDraggables._on_drag_start = function (self, sender, event)
	self._previous_mouse_event = EWS:MouseEvent("EVT_LEFT_DOWN")
	self._dragged_control = sender

	self._process_color_update_event(self, sender, event)

	return 
end
ColorPickerDraggables._on_dragging = function (self, sender, event)
	self._process_color_update_event(self, sender, event)

	return 
end
ColorPickerDraggables._on_drag_stop = function (self, sender, event)
	self._process_color_change_event(self, sender, event)

	self._dragged_control = nil

	return 
end

return 
