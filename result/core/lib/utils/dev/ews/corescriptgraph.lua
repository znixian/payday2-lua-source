core:module("CoreScriptGraph")
core:import("CoreClass")
core:import("CoreEvent")

ScriptGraph = ScriptGraph or CoreClass.class()
CONFIG_VERSION = "1"
ScriptGraph.init = function (self, parent, id, style)
	assert(style == "FLOW" or not style, "[ScriptGraph] Currently the only supported graph view type is 'FLOW'.")
	self._create_panel(self, parent, id, style)

	return 
end
ScriptGraph.update = function (self, t, dt)
	self._graph_view:update_graph(dt)

	return 
end
ScriptGraph.window = function (self)
	return self._graph_view
end
ScriptGraph.destroy = function (self)
	self._graph_view:destroy()

	return 
end
ScriptGraph.selected_nodes = function (self)
	local t = {}

	for n in self._graph_view:selected_nodes() do
		table.insert(t, n)
	end

	return t
end
ScriptGraph.last_selected_node = function (self)
	return self._graph_view:last_selected_node()
end
ScriptGraph.nodes = function (self)
	return self._graph_view:nodes()
end
ScriptGraph.graph_metadata = function (self)
	return self._graph_metadata
end
ScriptGraph.set_graph_metadata = function (self, data)
	self._graph_metadata = data

	return 
end
ScriptGraph.add_node = function (self, node)
	assert(node.type(node) == "FlowNode", "[ScriptGraph] The only supported node type is 'flow_node'.")
	self._graph:add_node(node)

	return 
end
ScriptGraph.connect = function (self, id, event_type, cb, data)
	self._graph_view:connect(id, event_type, cb, data)

	return 
end
ScriptGraph.save = function (self, id_map)
	local new_id_map = id_map or {}
	local config = Node("graph")

	config.set_parameter(config, "name", self._name or "")
	config.set_parameter(config, "version", CONFIG_VERSION)

	if self._graph_metadata then
		local gm = Node("metadata")

		gm.add_child(gm, self._graph_metadata)
		config.add_child(config, gm)
	end

	if id_map then
		for id, node in pairs(id_map) do
			local cfg_node = Node("node")

			cfg_node.set_parameter(cfg_node, "name", node.name(node))
			cfg_node.set_parameter(cfg_node, "type", node.type(node))
			cfg_node.set_parameter(cfg_node, "id", id)
			cfg_node.set_parameter(cfg_node, "x", node.x(node))
			cfg_node.set_parameter(cfg_node, "y", node.y(node))
			self._write_slots(self, cfg_node, node, id_map)
			self._write_icon(self, cfg_node, node)
			self._write_node_color(self, cfg_node, node)
			config.add_child(config, cfg_node)
		end
	else
		for node in self._graph_view:nodes() do
			local cfg_node = Node("node")

			cfg_node.set_parameter(cfg_node, "name", node.name(node))
			cfg_node.set_parameter(cfg_node, "type", node.type(node))

			local id = tostring(node)
			new_id_map[id] = node

			cfg_node.set_parameter(cfg_node, "id", id)
			cfg_node.set_parameter(cfg_node, "x", node.x(node))
			cfg_node.set_parameter(cfg_node, "y", node.y(node))
			self._write_slots(self, cfg_node, node)
			self._write_icon(self, cfg_node, node)
			self._write_node_color(self, cfg_node, node)
			config.add_child(config, cfg_node)
		end
	end

	return config, new_id_map
end
ScriptGraph.load = function (self, config_root)
	assert(config_root, "[ScriptGraph] Could not open: " .. tostring(config_root))
	assert(config_root.parameter(config_root, "version") == CONFIG_VERSION, "[ScriptGraph] Bad version!")

	self._name = assert(config_root.parameter(config_root, "name"))
	self._nodes = {}
	self._id_map = {}

	for node in config_root.children(config_root) do
		if node.name(node) == "metadata" then
			self._graph_metadata = node.child(node, 0)

			break
		end
	end

	for node in config_root.children(config_root) do
		if node.name(node) == "node" then
			local node_info = self._load_node_info(self, node)
			local ewsnode = EWS:FlowNode(assert(node.parameter(node, "name")), node_info.in_slot_names, node_info.out_slot_names, tonumber(assert(node.parameter(node, "x"))), tonumber(assert(node.parameter(node, "y"))))
			self._nodes[assert(node.parameter(node, "id"))] = {
				node_type = assert(node.parameter(node, "type")),
				info = node_info,
				cnode = ewsnode
			}

			ewsnode.set_metadata(ewsnode, node.parameter(node, "id"))

			self._id_map[node.parameter(node, "id")] = ewsnode
		end
	end

	self._graph:clear()

	for _, node in pairs(self._nodes) do
		self._graph:add_node(node.cnode)

		if node.info.icon then
			node.cnode:set_icon(CoreEWS.image_path(node.info.icon:s()))
		end

		if node.info.color then
			node.cnode:set_colour(unpack(node.info.color))
		end
	end

	for _, node in pairs(self._nodes) do
		if node.info.out_slots then
			for slot_name, slot in pairs(node.info.out_slots) do
				for _, con in pairs(slot.con) do
					local dest_node = self._find_node_with_id(self, self._nodes, con.id)

					node.cnode:set_target(slot_name, dest_node, con.slot, con.desc)
				end

				if slot.col then
					node.cnode:set_output_colour(slot_name, unpack(slot.col))
				end
			end
		end

		if node.info.in_slots then
			for slot_name, slot in pairs(node.info.in_slots) do
				if slot.col then
					node.cnode:set_input_colour(slot_name, unpack(slot.col))
				end
			end
		end
	end

	self._graph_view:refresh()

	return self._id_map
end
ScriptGraph._load_node_info = function (self, node)
	local info = {}

	for node_info in node.children(node) do
		if node_info.name(node_info) == "slot" then
			if assert(node_info.parameter(node_info, "type")) == "in" then
				info.in_slots = info.in_slots or {}
				info.in_slot_names = info.in_slot_names or {}
				local color = nil
				local name = assert(node_info.parameter(node_info, "name"))

				for inf in node_info.children(node_info) do
					if inf.name(inf) == "color" then
						color = {
							tonumber(assert(inf.parameter(inf, "r"))),
							tonumber(assert(inf.parameter(inf, "g"))),
							tonumber(assert(inf.parameter(inf, "b")))
						}
					end
				end

				table.insert(info.in_slot_names, name)

				info.in_slots[name] = {
					col = color
				}
			else
				info.out_slots = info.out_slots or {}
				info.out_slot_names = info.out_slot_names or {}
				local connection = {}
				local color = nil
				local name = assert(node_info.parameter(node_info, "name"))

				for inf in node_info.children(node_info) do
					if inf.name(inf) == "connection" then
						table.insert(connection, {
							id = assert(inf.parameter(inf, "id")),
							slot = assert(inf.parameter(inf, "slot")),
							desc = assert(inf.parameter(inf, "desc"))
						})
					elseif inf.name(inf) == "color" then
						color = {
							tonumber(assert(inf.parameter(inf, "r"))),
							tonumber(assert(inf.parameter(inf, "g"))),
							tonumber(assert(inf.parameter(inf, "b")))
						}
					end
				end

				table.insert(info.out_slot_names, name)

				info.out_slots[name] = {
					con = connection,
					col = color
				}
			end
		elseif node_info.name(node_info) == "icon" then
			info.icon = assert(Idstring(node_info.parameter(node_info, "name")))
		elseif node_info.name(node_info) == "color" then
			info.color = {
				tonumber(assert(node_info.parameter(node_info, "r"))),
				tonumber(assert(node_info.parameter(node_info, "g"))),
				tonumber(assert(node_info.parameter(node_info, "b")))
			}
		end
	end

	return info
end
ScriptGraph._create_panel = function (self, parent, id, style, window_style)
	self._graph = EWS:Graph()
	self._graph_view = EWS:GraphView(parent, id or "", self._graph, "FLOW")

	self._graph_view:set_clipping(false)
	self._graph_view:toggle_style(window_style or "SUNKEN_BORDER")

	return 
end
ScriptGraph._find_node_with_id = function (self, nodes, id)
	local node = assert(nodes[id], "[ScriptGraph] The graph is corrupt! Can't find node id: " .. tostring(id))

	return node.cnode
end
ScriptGraph._find_id_with_node = function (self, id_map, node)
	for id, n in pairs(id_map) do
		if n == node then
			return id
		end
	end

	error("[ScriptGraph] Could not find node: " .. tostring(node))

	return 
end
ScriptGraph._write_icon = function (self, cfg_node, node)
	if node.icon(node) ~= "" then
		local icon = Node("icon")
		local path_len = string.len(managers.database:base_path() .. "core\\lib\\utils\\dev\\ews\\images\\")

		icon.set_parameter(icon, "name", string.sub(node.icon(node), path_len + 1))
		cfg_node.add_child(cfg_node, icon)
	end

	return 
end
ScriptGraph._write_connections = function (self, slot_node, slot, node, id_map)
	local con_info = node.connection_info(node, slot)

	for _, inf in pairs(con_info) do
		local dest = inf.node
		local dest_slots = inf.slots
		local id = (id_map and self._find_id_with_node(self, id_map, dest)) or tostring(dest)

		for _, dest_slot in ipairs(dest_slots) do
			local con_node = Node("connection")

			con_node.set_parameter(con_node, "id", id)
			con_node.set_parameter(con_node, "slot", dest_slot)
			con_node.set_parameter(con_node, "desc", "")
			slot_node.add_child(slot_node, con_node)
		end
	end

	return 
end
ScriptGraph._write_output_color = function (self, slot_node, slot, node)
	local r, g, b = node.output_colour(node, slot)

	self._write_output_color_to_node(self, slot_node, Node("color"), r, g, b)

	return 
end
ScriptGraph._write_input_color = function (self, slot_node, slot, node)
	return 
end
ScriptGraph._write_output_color_to_node = function (self, slot_node, color_node, r, g, b)
	color_node.set_parameter(color_node, "r", tostring(assert(r)))
	color_node.set_parameter(color_node, "g", tostring(assert(g)))
	color_node.set_parameter(color_node, "b", tostring(assert(b)))
	slot_node.add_child(slot_node, color_node)

	return 
end
ScriptGraph._write_node_color = function (self, cfg_node, node)
	local color_node = Node("color")

	color_node.set_parameter(color_node, "r", tostring(assert(node.r(node))))
	color_node.set_parameter(color_node, "g", tostring(assert(node.g(node))))
	color_node.set_parameter(color_node, "b", tostring(assert(node.b(node))))
	cfg_node.add_child(cfg_node, color_node)

	return 
end
ScriptGraph._write_slots = function (self, cfg_node, node, id_map)
	if node.type(node) == "FlowNode" then
		for _, slot in ipairs(node.inputs(node)) do
			local slot_node = Node("slot")

			slot_node.set_parameter(slot_node, "name", slot)
			slot_node.set_parameter(slot_node, "type", "in")
			self._write_connections(self, slot_node, slot, node, id_map)
			self._write_input_color(self, slot_node, slot, node)
			cfg_node.add_child(cfg_node, slot_node)
		end

		for _, slot in ipairs(node.outputs(node)) do
			local slot_node = Node("slot")

			slot_node.set_parameter(slot_node, "name", slot)
			slot_node.set_parameter(slot_node, "type", "out")
			self._write_connections(self, slot_node, slot, node, id_map)
			self._write_output_color(self, slot_node, slot, node)
			cfg_node.add_child(cfg_node, slot_node)
		end
	end

	return 
end

return 
