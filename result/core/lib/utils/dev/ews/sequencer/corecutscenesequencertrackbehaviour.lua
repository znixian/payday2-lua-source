TrackBehaviour = TrackBehaviour or class()
EditableTrackBehaviour = EditableTrackBehaviour or class(TrackBehaviour)
ClipDragTrackBehaviour = ClipDragTrackBehaviour or class(TrackBehaviour)
MovePlayheadTrackBehaviour = MovePlayheadTrackBehaviour or class(TrackBehaviour)
BoxSelectionTrackBehaviour = BoxSelectionTrackBehaviour or class(TrackBehaviour)
TrackBehaviour.init = function (self)
	self._default_behaviour = self

	return 
end
TrackBehaviour.set_delegate = function (self, track_behaviour_delegate)
	self._delegate = track_behaviour_delegate

	return self
end
TrackBehaviour.set_default_behaviour = function (self, behaviour)
	self._default_behaviour = behaviour

	return self
end
TrackBehaviour.populate_from = function (self, behaviour)
	self._default_behaviour = behaviour._default_behaviour
	self._delegate = behaviour._delegate

	return self
end
TrackBehaviour.change_behaviour = function (self, new_behaviour)
	self._invoke_on_delegate(self, "change_track_behaviour", new_behaviour.populate_from(new_behaviour, self))

	return self
end
TrackBehaviour.restore_default_behaviour = function (self)
	self.change_behaviour(self, self._default_behaviour)

	return self
end
TrackBehaviour._delegate_supports = function (self, method_name)
	return self._delegate and type(self._delegate[method_name]) == "function"
end
TrackBehaviour._invoke_on_delegate = function (self, method_name, ...)
	if self._delegate == nil then
		error("Failed to call required delegate method \"" .. method_name .. "\" - no delegate object has been assigned")
	else
		local func = self._delegate[method_name]

		if type(func) == "function" then
			return func(self._delegate, ...)
		else
			error("Failed to call required delegate method \"" .. method_name .. "\" - method not defined")
		end
	end

	return 
end
EditableTrackBehaviour.CLIP_EDGE_HANDLE_WIDTH = 6
EditableTrackBehaviour.init = function (self)
	self.super.init(self)

	return 
end
EditableTrackBehaviour.on_mouse_motion = function (self, sender, track, event)
	if sender.clips and sender == track then
		local clip_below_cursor = sender.clip_at_event(sender, event)

		if clip_below_cursor then
			local drag_mode = self._drag_mode(self, clip_below_cursor, event.get_position(event, sender))

			if drag_mode == "LEFT_EDGE" or drag_mode == "RIGHT_EDGE" then
				return EWS:set_cursor("SIZEWE")
			end
		end

		EWS:set_cursor(nil)
	end

	return 
end
EditableTrackBehaviour.on_mouse_left_down = function (self, sender, track, event)
	if not sender.clips then
		self.change_behaviour(self, MovePlayheadTrackBehaviour:new())
	elseif sender == track then
		local item_below_cursor = sender.clip_at_event(sender, event)

		if item_below_cursor then
			if item_below_cursor.selected(item_below_cursor) then
				if event.control_down(event) then
					self._set_item_selected(self, item_below_cursor, false)
				end
			else
				if not event.control_down(event) then
					self._invoke_on_delegate(self, "deselect_all_items")
				end

				self._set_item_selected(self, item_below_cursor, true)
			end

			if item_below_cursor.selected(item_below_cursor) then
				local drag_mode = self._drag_mode(self, item_below_cursor, event.get_position(event, sender))

				if self._delegate_supports(self, "_signal_drag") then
					self._invoke_on_delegate(self, "_signal_drag", item_below_cursor, drag_mode)
				end

				self.change_behaviour(self, ClipDragTrackBehaviour:new(item_below_cursor, event.get_position(event, sender), drag_mode))
			end
		else
			if not event.control_down(event) then
				self._invoke_on_delegate(self, "deselect_all_items")
			end

			self._invoke_on_delegate(self, "_on_start_box_selection", event)
			self.change_behaviour(self, BoxSelectionTrackBehaviour:new())
		end
	end

	event.skip(event)

	return 
end
EditableTrackBehaviour._set_item_selected = function (self, item, selected)
	if self._delegate_supports(self, "set_item_selected") then
		self._invoke_on_delegate(self, "set_item_selected", item, selected)
	else
		item.set_selected(item, selected)
	end

	return 
end
EditableTrackBehaviour._drag_mode = function (self, clip, position)
	if clip.point_is_near_edge(clip, position, "LEFT_EDGE", -EditableTrackBehaviour.CLIP_EDGE_HANDLE_WIDTH) then
		return "LEFT_EDGE"
	elseif clip.point_is_near_edge(clip, position, "RIGHT_EDGE", -EditableTrackBehaviour.CLIP_EDGE_HANDLE_WIDTH) then
		return "RIGHT_EDGE"
	else
		return "CLIP"
	end

	return 
end
ClipDragTrackBehaviour.SNAP_RADIUS = 10
ClipDragTrackBehaviour.init = function (self, clip, drag_start, mode)
	self.super.init(self)

	self._clip = clip
	self._clip_initial_start_time = self._clip:start_time()
	self._clip_initial_end_time = self._clip:end_time()
	self._mode = mode or "CLIP"
	self._drag_start = drag_start
	self._previous_mouse_position = self._drag_start
	self._snapped_edge = "LEFT"

	if self._mode == "RIGHT_EDGE" then
		self._snapped_edge = "RIGHT"
	end

	return 
end
ClipDragTrackBehaviour.on_mouse_motion = function (self, sender, track, event)
	self._determine_snapped_edge(self, event.get_position(event, sender))

	local time_displacement = self._time_displacement(self, sender, event)
	slot5 = ipairs
	slot6 = (track.selected_clips and track.selected_clips(track)) or {}

	for _, clip in slot5(slot6) do
		clip.drag(clip, time_displacement, self._mode)

		local metadata = clip.metadata(clip)

		if metadata and metadata.on_gui_representation_changed then
			metadata.on_gui_representation_changed(metadata, sender, clip)
		end
	end

	if sender == track and self._delegate_supports(self, "_signal_drag") then
		self._invoke_on_delegate(self, "_signal_drag", self._clip, self._mode)
	end

	self._previous_mouse_position = event.get_position(event, sender)

	event.skip(event)

	return 
end
ClipDragTrackBehaviour.on_mouse_left_up = function (self, sender, track, event)
	slot4 = ipairs
	slot5 = (track.selected_clips and track.selected_clips(track)) or {}

	for _, clip in slot4(slot5) do
		clip.drag_commit(clip)
	end

	self.restore_default_behaviour(self)
	event.skip(event)

	return 
end
ClipDragTrackBehaviour._determine_snapped_edge = function (self, position)
	if self._mode == "CLIP" then
		local drag_delta_x = position.x - self._previous_mouse_position.x

		if 0 < drag_delta_x then
			self._snapped_edge = "RIGHT"
		elseif drag_delta_x < 0 then
			self._snapped_edge = "LEFT"
		end
	end

	return 
end
ClipDragTrackBehaviour._time_displacement = function (self, track, event)
	local time_displacement = track.pixels_to_units(track, event.get_position(event, track).x - self._drag_start.x)

	if event.shift_down(event) then
		return time_displacement
	else
		local playhead_position = (self._delegate_supports(self, "playhead_position") and self._invoke_on_delegate(self, "playhead_position")) or nil
		local snapped_to_grid = self._snap_to_grid(self, track, time_displacement)
		local snapped_to_clips = self._snap_to_clips(self, track, time_displacement) or snapped_to_grid
		local snapped_to_playhead = (playhead_position and self._snap_to_time(self, playhead_position)) or snapped_to_clips
		local unsnapped_time = self._drag_start.x + time_displacement
		local snap_radius = track.pixels_to_units(track, ClipDragTrackBehaviour.SNAP_RADIUS)

		if math.abs((self._drag_start.x + snapped_to_playhead) - unsnapped_time) < snap_radius then
			return snapped_to_playhead
		elseif math.abs((self._drag_start.x + snapped_to_clips) - unsnapped_time) < snap_radius then
			return snapped_to_clips
		elseif math.abs((self._drag_start.x + snapped_to_grid) - unsnapped_time) < snap_radius then
			return snapped_to_grid
		else
			return time_displacement
		end
	end

	return 
end
ClipDragTrackBehaviour._snap_to_time = function (self, time)
	return time - self._dragged_clip_edge_time(self)
end
ClipDragTrackBehaviour._snap_to_grid = function (self, track, time_displacement)
	local unsnapped_time = self._dragged_clip_edge_time(self) + time_displacement

	return self._snap_to_time(self, self._closest_grid_line_time(self, unsnapped_time))
end
ClipDragTrackBehaviour._closest_grid_line_time = function (self, time)
	return math.max(0, math.round(time, 100))
end
ClipDragTrackBehaviour._dragged_clip_edge_time = function (self)
	if self._snapped_edge == "LEFT" then
		return self._clip_initial_start_time
	elseif self._snapped_edge == "RIGHT" then
		return self._clip_initial_end_time
	end

	return 
end
ClipDragTrackBehaviour._snap_to_clips = function (self, track, time_displacement)
	if not track.clips then
		return nil
	end

	local closest_snapped_displacement = nil

	local function update_closest_snapped_displacement(initial_time, displacement, clip_edge)
		local unsnapped_time = initial_time + displacement
		local snapped_displacement = clip_edge - initial_time

		if closest_snapped_displacement == nil then
			closest_snapped_displacement = snapped_displacement
		else
			local snapped_time = initial_time + snapped_displacement
			local snapped_displacement_diff = math.abs(snapped_time - unsnapped_time)
			local closest_snapped_time = initial_time + closest_snapped_displacement
			local closest_snapped_displacement_diff = math.abs(closest_snapped_time - unsnapped_time)

			if snapped_displacement_diff < closest_snapped_displacement_diff then
				closest_snapped_displacement = snapped_displacement
			end
		end

		return 
	end

	for _, clip in ipairs(track.clips(track)) do
		if not clip.selected(clip) then
			if self._snapped_edge == "LEFT" then
				update_closest_snapped_displacement(self._clip_initial_start_time, time_displacement, clip.end_time(clip))
			elseif self._snapped_edge == "RIGHT" then
				update_closest_snapped_displacement(self._clip_initial_end_time, time_displacement, clip.start_time(clip))
			end
		end
	end

	return closest_snapped_displacement
end
MovePlayheadTrackBehaviour.on_mouse_motion = function (self, sender, track, event)
	if sender == track then
		local pos = event.get_position(event)
		local time = sender.pixels_to_units(sender, event.get_position(event, sender).x)

		self._invoke_on_delegate(self, "set_playhead_position", time)
	end

	event.skip(event)

	return 
end
MovePlayheadTrackBehaviour.on_mouse_left_up = function (self, sender, track, event)
	if sender == track then
		self.on_mouse_motion(self, sender, track, event)
		self.restore_default_behaviour(self)
	end

	return 
end
BoxSelectionTrackBehaviour.on_mouse_motion = function (self, sender, track, event)
	if sender == track then
		self._invoke_on_delegate(self, "_on_drag_box_selection", event)
	end

	event.skip(event)

	return 
end
BoxSelectionTrackBehaviour.on_mouse_left_up = function (self, sender, track, event)
	if sender == track then
		self.on_mouse_motion(self, sender, track, event)
		self._invoke_on_delegate(self, "_on_commit_box_selection", event)
		self.restore_default_behaviour(self)
	end

	return 
end

return 
