CoreTableEditorPanel = CoreTableEditorPanel or class()
CoreTableEditorPanel.init = function (self, parent)
	self.__column_names = {}
	self.__panel = EWS:Panel(parent)
	local panel_sizer = EWS:BoxSizer("VERTICAL")

	self.__panel:set_sizer(panel_sizer)
	panel_sizer.add(panel_sizer, self._create_list_ctrl(self, self.__panel), 1, 5, "TOP,LEFT,RIGHT,EXPAND")
	panel_sizer.add(panel_sizer, self._create_buttons_panel(self, self.__panel), 0, 4, "ALL,EXPAND")
	panel_sizer.add(panel_sizer, self._create_fields_panel(self, self.__panel), 0, 0, "EXPAND")

	return 
end
CoreTableEditorPanel.destroy = function (self)
	self.__panel:destroy()

	self.__panel = nil

	return 
end
CoreTableEditorPanel.clear = function (self)
	self.freeze(self)
	self.__list_ctrl:delete_all_items()
	self.set_selected_item(self, nil)
	self.thaw(self)

	return 
end
CoreTableEditorPanel.add_to_sizer = function (self, sizer, proportion, border, flags)
	sizer.add(sizer, self.__panel, proportion, border, flags)

	return 
end
CoreTableEditorPanel.detach_from_sizer = function (self, sizer)
	sizer.detach(sizer, self.__panel)

	return 
end
CoreTableEditorPanel.freeze = function (self)
	self.__frozen = true

	self.__panel:freeze()

	return 
end
CoreTableEditorPanel.thaw = function (self)
	self.__frozen = nil

	self.__panel:thaw()

	if self.__needs_refresh then
		self.__needs_refresh = nil

		self._refresh_fields_panel(self)
		self._refresh_buttons_panel(self)
	end

	return 
end
CoreTableEditorPanel.add_column = function (self, heading, format_style)
	table.insert(self.__column_names, heading)
	self.__list_ctrl:append_column(heading, format_style or "LIST_FORMAT_LEFT")
	self._refresh_fields_panel(self)

	return 
end
CoreTableEditorPanel.add_item = function (self, ...)
	local values = {
		...
	}
	local item_index = self.__list_ctrl:append_item(tostring(values[1]))

	for i = 2, #values, 1 do
		self.__list_ctrl:set_item(item_index, i - 1, tostring(values[i]))
	end

	self.set_selected_item(self, item_index)

	return item_index
end
CoreTableEditorPanel.remove_item = function (self, item_index)
	self.__list_ctrl:delete_item(item_index)
	self.set_selected_item(self, nil)

	return 
end
CoreTableEditorPanel.item_value = function (self, item_index, column_name)
	return self._string_to_value(self, self.__list_ctrl:get_item(item_index, self._column_index(self, column_name)), column_name)
end
CoreTableEditorPanel.set_item_value = function (self, item_index, column_name, value)
	self.__list_ctrl:set_item(item_index, self._column_index(self, column_name), self._value_to_string(self, value, column_name))

	return 
end
CoreTableEditorPanel.selected_item = function (self)
	local item_index = self.__list_ctrl:selected_item()

	return (0 <= item_index and item_index) or nil
end
CoreTableEditorPanel.set_selected_item = function (self, item_index)
	self.__list_ctrl:set_item_selected(item_index or -1, true)
	self._refresh_fields_panel(self)
	self._refresh_buttons_panel(self)

	return 
end
CoreTableEditorPanel.selected_item_value = function (self, column_name)
	local selected_item_index = self.selected_item(self)

	return selected_item_index and self.item_value(self, selected_item_index, column_name)
end
CoreTableEditorPanel.set_selected_item_value = function (self, column_name, value)
	local selected_item_index = self.selected_item(self)

	if selected_item_index then
		self.set_item_value(self, selected_item_index, column_name, value)
	end

	return 
end
CoreTableEditorPanel._create_list_ctrl = function (self, parent)
	self.__list_ctrl = EWS:ListCtrl(parent, "", "LC_REPORT,LC_SINGLE_SEL")

	self.__list_ctrl:connect("EVT_COMMAND_LIST_ITEM_SELECTED", callback(self, self, "_on_selection_changed"), self.__list_ctrl)
	self.__list_ctrl:connect("EVT_COMMAND_LIST_ITEM_DESELECTED", callback(self, self, "_on_selection_changed"), self.__list_ctrl)

	return self.__list_ctrl
end
CoreTableEditorPanel._create_buttons_panel = function (self, parent)
	self.__buttons_panel = EWS:Panel(parent)
	local panel_sizer = EWS:BoxSizer("HORIZONTAL")

	self.__buttons_panel:set_sizer(panel_sizer)

	self.__add_button = EWS:Button(self.__buttons_panel, "Add")

	self.__add_button:connect("EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_add_button_clicked"), self.__add_button)

	self.__remove_button = EWS:Button(self.__buttons_panel, "Remove")

	self.__remove_button:connect("EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_remove_button_clicked"), self.__add_button)
	panel_sizer.add(panel_sizer, self.__add_button, 1, 1, "RIGHT,EXPAND")
	panel_sizer.add(panel_sizer, self.__remove_button, 1, 2, "LEFT,EXPAND")

	return self.__buttons_panel
end
CoreTableEditorPanel._create_fields_panel = function (self, parent)
	self.__fields_panel = EWS:Panel(parent)

	self._refresh_fields_panel(self)

	return self.__fields_panel
end
CoreTableEditorPanel._refresh_buttons_panel = function (self)
	self.__remove_button:set_enabled(self.selected_item(self) ~= nil)

	return 
end
CoreTableEditorPanel._refresh_fields_panel = function (self)
	if self.__frozen then
		self.__needs_refresh = true
	else
		self.__fields_panel:freeze()
		self.__fields_panel:destroy_children()

		local new_sizer = self._sizer_with_editable_fields(self, self.__fields_panel)

		if new_sizer then
			self.__fields_panel:set_sizer(new_sizer)
			self.__fields_panel:fit_inside()
		end

		self.__fields_panel:thaw()
	end

	return 
end
CoreTableEditorPanel._sizer_with_editable_fields = function (self, parent)
	local sizer = EWS:BoxSizer("VERTICAL")
	local first_control = nil

	for _, column_name in ipairs(self.__column_names) do
		local control = self._create_labeled_text_field(self, column_name, parent, sizer)
		first_control = first_control or control
	end

	if first_control and self.selected_item(self) ~= nil then
		first_control.set_selection(first_control, -1, -1)
		first_control.set_focus(first_control)
	end

	return sizer
end
CoreTableEditorPanel._create_labeled_text_field = function (self, column_name, parent, sizer)
	local enabled = self.selected_item(self) ~= nil
	local label = EWS:StaticText(parent, string.pretty(column_name, true) .. ":")
	local control = EWS:TextCtrl(parent, self.selected_item_value(self, column_name))
	local callback_func = self._make_control_edited_callback(self, control, column_name)

	label.set_enabled(label, enabled)
	control.set_enabled(control, enabled)
	control.set_min_size(control, control.get_min_size(control):with_x(0))
	control.connect(control, "EVT_COMMAND_TEXT_UPDATED", callback_func)
	sizer.add(sizer, label, 0, 5, "TOP,LEFT,RIGHT")
	sizer.add(sizer, control, 0, 5, "ALL,EXPAND")

	return control
end
CoreTableEditorPanel._column_index = function (self, column_name)
	local column_index = (column_name and table.get_vector_index(self.__column_names, column_name)) or assert(nil, string.format("Column \"%s\" does not exist.", tostring(column_name)))

	return column_index - 1
end
CoreTableEditorPanel._string_to_value = function (self, str, column_name)
	return str or ""
end
CoreTableEditorPanel._value_to_string = function (self, value, column_name)
	return (value == nil and "") or tostring(value)
end
CoreTableEditorPanel._make_control_edited_callback = function (self, control, column_name, value_method_name)
	return function ()
		self:_on_control_edited(control, column_name, value_method_name)

		return 
	end
end
CoreTableEditorPanel._top_level_window = function (self, window)
	window = window or self.__panel

	return ((type_name(window) == "EWSFrame" or type_name(window) == "EWSDialog") and window) or self._top_level_window(self, assert(window.parent(window)))
end
CoreTableEditorPanel._on_selection_changed = function (self, sender)
	self._refresh_fields_panel(self)
	self._refresh_buttons_panel(self)

	return 
end
CoreTableEditorPanel._on_add_button_clicked = function (self, sender)
	self.add_item(self, "<New Entry>")

	return 
end
CoreTableEditorPanel._on_remove_button_clicked = function (self, sender)
	self.remove_item(self, self.selected_item(self))

	return 
end
CoreTableEditorPanel._on_control_edited = function (self, control, column_name, value_method_name)
	value_method_name = value_method_name or "get_value"
	local value = control[value_method_name](control)

	self.set_selected_item_value(self, column_name, value)

	return 
end

return 
