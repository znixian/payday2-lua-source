CoreCollapseBox = CoreCollapseBox or class()
CoreCollapseBox.init = function (self, parent, orientation, caption, expanded_size, expand, style)
	self._caption = caption or ""
	self._expand = not expand
	self._parent = parent
	self._expanded_size = expanded_size
	self._panel = EWS:Panel(self._parent, "", "")
	self._box = (style == "NO_BORDER" and EWS:BoxSizer("VERTICAL")) or EWS:StaticBoxSizer(self._panel, "VERTICAL", "")

	self._panel:set_sizer(self._box)

	self._btn = EWS:Button(self._panel, "", "", "NO_BORDER")

	self._btn:set_font_weight("FONTWEIGHT_BOLD")
	self.connect(self, "", "EVT_COMMAND_BUTTON_CLICKED", self._cb, self)
	self._box:add(self._btn, 0, 0, "EXPAND")

	self._lower_panel = EWS:Panel(self._panel, "", (style == "NO_BORDER" and "SIMPLE_BORDER") or "")

	if self._expanded_size then
		self._lower_panel:set_min_size(self._expanded_size)
	end

	self._lower_box = EWS:BoxSizer(orientation)

	self._lower_panel:set_sizer(self._lower_box)
	self._box:add(self._lower_panel, 1, 4, (style == "NO_BORDER" and "LEFT,RIGHT,EXPAND") or "EXPAND")
	self._cb(self)

	return 
end
CoreCollapseBox.connect = function (self, id, event, cb, data)
	self._btn:connect(id, event, cb, data)

	return 
end
CoreCollapseBox.panel = function (self)
	return self._panel
end
CoreCollapseBox.lower_panel = function (self)
	return self._lower_panel
end
CoreCollapseBox.box = function (self)
	return self._lower_box
end
CoreCollapseBox.expanded = function (self)
	return self._expand
end
CoreCollapseBox.expanded_size = function (self)
	return self._expanded_size
end
CoreCollapseBox.set_expand = function (self, b)
	self._expand = not expand

	self._cb(self)

	return 
end
CoreCollapseBox.set_expanded_size = function (self, v)
	self._parent:freeze()

	self._expanded_size = v

	self._lower_panel:set_sizer(v)
	self._parent:layout()
	self._parent:thaw()
	self._parent:refresh()

	return 
end
CoreCollapseBox._cb = function (self)
	self._expand = not self._expand

	self._parent:freeze()

	local icon = (self._expand and "[-]") or "[+]"

	self._lower_panel:set_visible(self._expand)
	self._btn:set_caption(icon .. " " .. self._caption)
	self._parent:layout()
	self._parent:thaw()
	self._parent:refresh()

	return 
end

return 
