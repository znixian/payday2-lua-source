core:module("CoreVectorSliderBox")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreEws")
core:import("CoreDebug")

VectorSliderBox = VectorSliderBox or CoreClass.mixin(CoreClass.class(), CoreEvent.BasicEventHandling)
VectorSliderBox.init = function (self, parent_frame, title, min, max, step)
	assert(min < max)

	self._parent_frame = parent_frame
	self._step = step
	self._min = min
	self._max = max
	self._slider_max = (max - min)/step
	self._box = EWS:StaticBoxSizer(self._parent_frame, "VERTICAL", title)

	return 
end
VectorSliderBox.box = function (self)
	return self._box
end
VectorSliderBox._on_slider_movement = function (self)
	self._update_text(self)

	return 
end
VectorSliderBox._on_slider_changed = function (self)
	self._update_values(self)

	return 
end
VectorSliderBox._on_mute_toggle = function (self)
	if self.__mute_toggle:get_value() == true then
		self._set_enabled_all(self, false)
	else
		self._set_enabled_all(self, true)
	end

	return 
end
VectorSliderBox._create_slider = function (self)
	local new_slider = EWS:Slider(self._parent_frame, 0, 0, self._slider_max)

	new_slider.connect(new_slider, "", "EVT_SCROLL_THUMBTRACK", CoreEvent.callback(self, self, "_on_slider_movement"), "")
	new_slider.connect(new_slider, "", "EVT_SCROLL_CHANGED", CoreEvent.callback(self, self, "_on_slider_changed"), "")

	return new_slider
end
VectorSliderBox._create_mute_button = function (self)
	local box, mute_button = nil
	box = EWS:BoxSizer("HORIZONTAL")
	mute_button = EWS:ToggleButton(self._parent_frame, "Feed Zero", "", "BU_EXACTFIT,NO_BORDER")

	mute_button.connect(mute_button, "", "EVT_COMMAND_TOGGLEBUTTON_CLICKED", CoreEvent.callback(self, self, "_on_mute_toggle"), "")
	box.add(box, mute_button, 0, 0, "EXPAND,ALIGN_LEFT")
	self._box:add(box, 1, 0, "EXPAND")

	return mute_button
end
VectorSliderBox._create_sizer = function (self)
	local box, text_ctrl, slider = nil
	box = EWS:BoxSizer("HORIZONTAL")
	slider = self._create_slider(self)
	text_ctrl = EWS:TextCtrl(self._parent_frame, string.format("%.3f", self._min), "", "TE_PROCESS_ENTER")

	text_ctrl.connect(text_ctrl, "", "EVT_COMMAND_TEXT_ENTER", CoreEvent.callback(self, self, "_on_update_textctrl"), "")
	box.add(box, slider, 5, 0, "EXPAND")
	box.add(box, text_ctrl, 1, 0, "EXPAND")
	self._box:add(box, 1, 0, "EXPAND")

	return slider, text_ctrl
end
VectorSliderBox.get_value = function (self)
	if self._mute_toggle:get_value() == true then
		return Vector3(0, 0, 0)
	else
		return Vector3(self._slider_x:get_value(), self._slider_y:get_value(), self._slider_z:get_value())
	end

	return 
end
VectorSliderBox.set_value = function (self, vector)
	self.__slider_x:set_value(self._actual_to_slider(self, vector.x))
	self.__slider_y:set_value(self._actual_to_slider(self, vector.y))
	self.__slider_z:set_value(self._actual_to_slider(self, vector.z))
	self._set_text(self, vector.x, vector.y, vector.z)

	if self._vector3 then
		self._send_event(self, "EVT_VALUE_CHANGED", vector)
	else
		self._send_event(self, "EVT_VALUE_CHANGED", vector.with_z(vector, 0))
	end

	return 
end
VectorSliderBox._actual_to_slider = function (self, value)
	return (value - self._min)/self._step
end
VectorSliderBox._slider_to_actual = function (self, value)
	return value*self._step + self._min
end
VectorSliderBox._update_values = function (self)
	local x, y, z = nil
	x = self._slider_to_actual(self, self.__slider_x:get_value())
	y = self._slider_to_actual(self, self.__slider_y:get_value())
	z = self._slider_to_actual(self, self.__slider_z:get_value())

	self.set_value(self, Vector3(x, y, z))
	self._set_text(self, x, y, z)

	return 
end
VectorSliderBox._update_text = function (self)
	local x, y, z = nil
	x = self._slider_to_actual(self, self.__slider_x:get_value())
	y = self._slider_to_actual(self, self.__slider_y:get_value())
	z = self._slider_to_actual(self, self.__slider_z:get_value())

	self._set_text(self, x, y, z)

	return 
end
VectorSliderBox._set_text = function (self, x, y, z)
	self.__slider_x_textctrl:set_value(string.format("%.3f", x))
	self.__slider_y_textctrl:set_value(string.format("%.3f", y))
	self.__slider_z_textctrl:set_value(string.format("%.3f", z))

	return 
end
VectorSliderBox._on_update_textctrl = function (self)
	local x, y, z = nil
	x = self._check_input(self, self.__slider_x_textctrl:get_value())
	y = self._check_input(self, self.__slider_y_textctrl:get_value())
	z = self._check_input(self, self.__slider_z_textctrl:get_value())

	self.set_value(self, Vector3(x, y, z))

	return 
end
VectorSliderBox._check_input = function (self, input)
	local value = tonumber(input)

	if type(value) ~= "number" then
		value = self._min
	elseif value < self._min or self._max < value then
		value = self._min
	end

	return value
end
VectorSliderBox._set_enabled_all = function (self, value)
	self.__slider_x:set_enabled(value)
	self.__slider_y:set_enabled(value)
	self.__slider_x_textctrl:set_enabled(value)
	self.__slider_y_textctrl:set_enabled(value)
	self.__slider_z:set_enabled(value)
	self.__slider_z_textctrl:set_enabled(value)

	return 
end
Vector2SliderBox = Vector2SliderBox or CoreClass.class(VectorSliderBox)
Vector2SliderBox.init = function (self, parent_frame, title, min, max, step)
	self.super:init(parent_frame, title, min, max, step)

	self._vector3 = false
	self.__mute_toggle = self._create_mute_button(self)
	self.__slider_x, self.__slider_x_textctrl = self._create_sizer(self)
	self.__slider_y, self.__slider_y_textctrl = self._create_sizer(self)
	self.__slider_z = EWS:Slider(self._parent_frame, 0, 0, 0)
	self.__slider_z_textctrl = EWS:TextCtrl(self._parent_frame, 0, "", "")

	return 
end
Vector3SliderBox = Vector3SliderBox or CoreClass.class(VectorSliderBox)
Vector3SliderBox.init = function (self, parent_frame, title, min, max, step)
	self.super.init(self, parent_frame, title, min, max, step)

	self._vector3 = true
	self.__mute_toggle = self._create_mute_button(self)
	self.__slider_x, self.__slider_x_textctrl = self._create_sizer(self)
	self.__slider_y, self.__slider_y_textctrl = self._create_sizer(self)
	self.__slider_z, self.__slider_z_textctrl = self._create_sizer(self)

	return 
end

return 
