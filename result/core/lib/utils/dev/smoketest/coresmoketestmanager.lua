core:module("CoreSmoketestManager")
core:import("CoreClass")
core:import("CoreEngineAccess")
core:import("CoreSmoketestReporter")
core:import("CoreSmoketestLoadLevelSuite")
core:import("CoreSmoketestEditorSuite")

Manager = Manager or CoreClass.class()
Manager.init = function (self, session_state)
	self._session_state = session_state
	self._smoketestsuites = {}
	self._reporter = CoreSmoketestReporter.Reporter:new()

	self.register(self, "editor", CoreSmoketestEditorSuite.EditorSuite:new())
	self.register(self, "load_level", CoreSmoketestLoadLevelSuite.LoadLevelSuite:new())

	return 
end
Manager.destroy = function (self)
	return 
end
Manager.register = function (self, name, smoketestsuite)
	self._smoketestsuites[name] = smoketestsuite

	return 
end
Manager.post_init = function (self)
	self._parse_arguments(self, Application:argv())

	return 
end
Manager._parse_arguments = function (self, args)
	local suite_arguments = {}

	for i, arg in ipairs(args) do
		if arg.find(arg, "-smoketest:") then
			local smoketest_class = arg.sub(arg, 12, -1)

			assert(not self._suite, "Only one smoketest suite can be run at a time")
			assert(self._smoketestsuites[smoketest_class], "Smoketest '" .. smoketest_class .. "' does't exist")

			self._suite = self._smoketestsuites[smoketest_class]
		elseif arg.find(arg, "-smoketestarg:") then
			local subarg = arg.sub(arg, 15, -1)
			local separator_index = subarg.find(subarg, "=")

			assert(separator_index, "smoketestargs must be on the form name=value! found this " .. subarg)

			local name = subarg.sub(subarg, 1, separator_index - 1)
			local value = subarg.sub(subarg, separator_index + 1, -1)
			suite_arguments[name] = value
		end
	end

	if self._suite then
		self._suite:start(self._session_state, self._reporter, suite_arguments)
	end

	return 
end
Manager.update = function (self, t, dt)
	if self._suite then
		self._suite:update(t, dt)

		if self._suite:is_done() then
			if SystemInfo:platform() == Idstring("WIN32") then
				self._reporter:begin_substep("shutdown")
				CoreEngineAccess._quit()
			else
				self._reporter:tests_done()
			end

			self._suite = nil
		end
	end

	return 
end

return 
