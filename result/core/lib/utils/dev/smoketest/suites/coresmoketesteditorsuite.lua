core:module("CoreSmoketestEditorSuite")
core:import("CoreClass")
core:import("CoreSmoketestCommonSuite")

EditorSuite = EditorSuite or class(CoreSmoketestCommonSuite.CommonSuite)
EditorSuite.init = function (self)
	EditorSuite.super.init(self)
	self.add_step(self, "load_editor", CoreSmoketestCommonSuite.WaitEventSubstep, CoreSmoketestCommonSuite.WaitEventSubstep.step_arguments(Idstring("game_state_editor")))
	self.add_step(self, "load_level", CoreSmoketestCommonSuite.CallAndDoneSubstep, CoreSmoketestCommonSuite.CallAndDoneSubstep.step_arguments(callback(self, self, "load_level")))

	return 
end
EditorSuite.load_level = function (self)
	managers.editor:load_level(self.get_argument(self, "editor_dir"), self.get_argument(self, "editor_level"))

	return 
end
EditorSuite.run_mission_simulation = function (self)
	managers.editor:run_simulation_callback(true)

	return 
end
EditorSuite.stop_mission_simulation = function (self)
	managers.editor:run_simulation_callback(true)

	return 
end
EditorSuite.environment_editor = function (self)
	managers.toolhub:open("Environment Editor")
	managers.toolhub:close("Environment Editor")

	return 
end

return 
