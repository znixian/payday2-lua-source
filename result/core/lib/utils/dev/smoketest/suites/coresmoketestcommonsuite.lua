core:module("CoreSmoketestCommonSuite")
core:import("CoreClass")
core:import("CoreSmoketestSuite")

Substep = Substep or CoreClass.class()
Substep.init = function (self, suite, step_arguments)
	self._suite = suite
	self._step_arguments = step_arguments

	self.start(self)

	return 
end
Substep.has_failed = function (self)
	return self._fail == true
end
Substep.is_done = function (self)
	return self._done
end
Substep._set_done = function (self)
	self._done = true
	self._fail = false

	return 
end
Substep._set_fail = function (self)
	self._fail = true

	return 
end
Substep.start = function (self)
	assert(false, "Not implemented")

	return 
end
Substep.update = function (self, t, dt)
	return 
end
CallAndDoneSubstep = CallAndDoneSubstep or CoreClass.class(Substep)
CallAndDoneSubstep.step_arguments = function (callback)
	local step_arguments = {
		callback = callback
	}

	return step_arguments
end
CallAndDoneSubstep.start = function (self)
	self._step_arguments.callback()
	self._set_done(self)

	return 
end
WaitEventSubstep = WaitEventSubstep or CoreClass.class(Substep)
WaitEventSubstep.step_arguments = function (event_id)
	local step_arguments = {
		event_id = event_id
	}

	return step_arguments
end
WaitEventSubstep.start = function (self)
	self._event_listener = EventManager:register_listener(self._step_arguments.event_id, function ()
		self:_set_done()

		return 
	end, nil)

	return 
end
WaitEventSubstep.destroy = function (self)
	EventManager:unregister_listener(self._event_listener)

	return 
end
CallAndWaitEventSubstep = CallAndWaitEventSubstep or CoreClass.class(Substep)
CallAndWaitEventSubstep.step_arguments = function (callback, event_id)
	local step_arguments = {
		callback = callback,
		event_id = event_id
	}

	return step_arguments
end
CallAndWaitEventSubstep.start = function (self)
	self._event_listener = EventManager:register_listener(self._step_arguments.event_id, function ()
		self:_set_done()

		return 
	end, nil)

	self._step_arguments.callback()

	return 
end
CallAndWaitEventSubstep.destroy = function (self)
	EventManager:unregister_listener(self._event_listener)

	return 
end
DelaySubstep = DelaySubstep or CoreClass.class(Substep)
DelaySubstep.step_arguments = function (seconds)
	local step_arguments = {
		seconds = seconds
	}

	return step_arguments
end
DelaySubstep.start = function (self)
	self._seconds_left = self._step_arguments.seconds

	return 
end
DelaySubstep.update = function (self, t, dt)
	self._seconds_left = self._seconds_left - dt

	if self._seconds_left <= 0 then
		self._set_done(self)
	end

	return 
end
CommonSuite = CommonSuite or CoreClass.class(CoreSmoketestSuite.Suite)
CommonSuite.init = function (self)
	self._step_list = {}

	return 
end
CommonSuite.add_step = function (self, name, class, params)
	local step_entry = {
		name = name,
		class = class,
		params = params
	}

	table.insert(self._step_list, step_entry)

	return 
end
CommonSuite.start = function (self, session_state, reporter, suite_arguments)
	self._suite_arguments = suite_arguments
	self._session_state = session_state
	self._reporter = reporter
	self._is_done = false
	self._current_step_id = 0

	self.next_step(self)

	return 
end
CommonSuite.is_done = function (self)
	return self._is_done
end
CommonSuite.update = function (self, t, dt)
	if self._current_step then
		self._current_step:update(t, dt)

		if self._current_step:is_done() then
			if self._current_step:has_failed() then
				self._reporter:fail_substep(self._step_list[self._current_step_id].name)
			else
				self._reporter:end_substep(self._step_list[self._current_step_id].name)
			end

			if not self.next_step(self) then
				self._is_done = true
			end
		end
	end

	return 
end
CommonSuite.next_step = function (self)
	if self._current_step then
		if self._current_step.destroy then
			self._current_step:destroy()
		end

		self._current_step = nil
	end

	self._current_step_id = self._current_step_id + 1

	if self._current_step_id <= #self._step_list then
		local step_entry = self._step_list[self._current_step_id]

		self._reporter:begin_substep(step_entry.name)

		self._current_step = step_entry.class:new(self, step_entry.params)

		return true
	else
		return false
	end

	return 
end
CommonSuite.get_argument = function (self, name)
	assert(self._suite_arguments[name], "Suite argument '" .. name .. "' was not defined")

	return self._suite_arguments[name]
end

return 
