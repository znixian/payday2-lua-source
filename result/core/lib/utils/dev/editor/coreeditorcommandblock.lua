core:module("CoreEditorCommandBlock")
core:import("CoreEditorCommand")

CoreEditorCommandBlock = CoreEditorCommandBlock or class()
CoreEditorCommandBlock.init = function (self)
	self._actions = {}

	return 
end
CoreEditorCommandBlock.execute = function (self)
	for i = 1, #self._actions, 1 do
		self._actions[i]:execute()
	end

	return 
end
CoreEditorCommandBlock.undo = function (self)
	local sorted_actions = _G.clone(self._actions)

	for i, action in ipairs(sorted_actions) do
		action.__order = i + (action.__type and (action.__type.__priority or 0))
	end

	table.sort(sorted_actions, function (a, b)
		return a.__order < b.__order
	end)

	for i = #sorted_actions, 1, -1 do
		if managers.editor:undo_debug() then
			print("[Undo] performing undo on ", i, sorted_actions[i])
		end

		sorted_actions[i]:undo(i == 1)
	end

	return 
end
CoreEditorCommandBlock.add_command = function (self, action)
	table.insert(self._actions, action)

	return 
end
CoreEditorCommandBlock.__tostring = function (self)
	return string.format("[CoreEditorCommandBlock actions: %s]", tostring(#self._actions))
end

return 
