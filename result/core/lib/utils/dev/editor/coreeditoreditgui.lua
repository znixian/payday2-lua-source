core:import("CoreEngineAccess")
core:import("CoreEditorUtils")

EditGui = EditGui or class()
EditGui.init = function (self, parent, toolbar, btn, name)
	self._panel = EWS:Panel(parent, "", "TAB_TRAVERSAL")
	self._main_sizer = EWS:StaticBoxSizer(self._panel, "HORIZONTAL", name)

	self._panel:set_sizer(self._main_sizer)

	self._toolbar = toolbar
	self._btn = btn
	self._ctrls = {
		unit = nil
	}

	self.set_visible(self, false)

	return 
end
EditGui.has = function (self, unit)
	if not alive(unit) or false then
		self.disable(self)

		return false
	end

	return 
end
EditGui.disable = function (self)
	self._ctrls.unit = nil

	self._toolbar:set_tool_enabled(self._btn, false)
	self._toolbar:set_tool_state(self._btn, false)
	self.set_visible(self, false)

	return 
end
EditGui.set_visible = function (self, vis)
	self._visible = vis

	self._panel:set_visible(vis)
	self._panel:layout()

	return 
end
EditGui.visible = function (self)
	return self._visible
end
EditGui.get_panel = function (self)
	return self._panel
end

return 
