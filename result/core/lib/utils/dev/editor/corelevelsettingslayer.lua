core:module("CoreLevelSettingsLayer")
core:import("CoreLayer")
core:import("CoreEws")

LevelSettingsLayer = LevelSettingsLayer or class(CoreLayer.Layer)
LevelSettingsLayer.init = function (self, owner)
	LevelSettingsLayer.super.init(self, owner, "level_settings")

	self._settings = {}
	self._settings_ctrlrs = {}

	return 
end
LevelSettingsLayer.get_layer_name = function (self)
	return "Level Settings"
end
LevelSettingsLayer.get_setting = function (self, setting)
	return self._settings[setting]
end
LevelSettingsLayer.get_mission_filter = function (self)
	local t = {}

	for i = 1, 5, 1 do
		if self.get_setting(self, "mission_filter_" .. i) then
			table.insert(t, i)
		end
	end

	return t
end
LevelSettingsLayer.load = function (self, world_holder, offset)
	self._settings = world_holder.create_world(world_holder, "world", self._save_name, offset)

	for id, setting in pairs(self._settings_ctrlrs) do
		if setting.type == "combobox" then
			CoreEws.change_combobox_value(setting.params, self._settings[id])
		end
	end

	return 
end
LevelSettingsLayer.save = function (self, save_params)
	local t = {
		single_data_block = true,
		entry = self._save_name,
		data = {
			settings = self._settings
		}
	}

	self._add_project_save_data(self, t.data)
	managers.editor:add_save_data(t)

	return 
end
LevelSettingsLayer.update = function (self, t, dt)
	return 
end
LevelSettingsLayer.build_panel = function (self, notebook)
	cat_print("editor", "LevelSettingsLayer:build_panel")

	self._ews_panel = EWS:Panel(notebook, "", "TAB_TRAVERSAL")
	self._main_sizer = EWS:BoxSizer("HORIZONTAL")

	self._ews_panel:set_sizer(self._main_sizer)

	self._sizer = EWS:BoxSizer("VERTICAL")

	self._add_chunk_name(self, self._ews_panel, self._sizer)
	self._add_simulation_level_id(self, self._sizer)
	self._add_simulation_mission_filter(self, self._sizer)
	self._main_sizer:add(self._sizer, 1, 0, "EXPAND")

	return self._ews_panel
end
LevelSettingsLayer._add_simulation_level_id = function (self, sizer)
	local id = "simulation_level_id"
	local params = {
		default = "none",
		name = "Simulation level id:",
		value = "none",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Select a level id to use when simulating the level.",
		sorted = true,
		panel = self._ews_panel,
		sizer = sizer,
		options = rawget(_G, "tweak_data").levels:get_level_index()
	}
	local ctrlr = CoreEws.combobox(params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "_set_data"), {
		ctrlr = ctrlr,
		value = id
	})

	self._settings_ctrlrs[id] = {
		default = "none",
		type = "combobox",
		params = params,
		ctrlr = ctrlr
	}

	return 
end
LevelSettingsLayer._add_simulation_mission_filter = function (self, sizer)
	for i = 1, 5, 1 do
		local id = "mission_filter_" .. i
		local ctrlr = EWS:CheckBox(self._ews_panel, "Mission filter " .. i, "")

		ctrlr.set_value(ctrlr, false)
		ctrlr.connect(ctrlr, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "_set_data"), {
			ctrlr = ctrlr,
			value = id
		})
		sizer.add(sizer, ctrlr, 0, 0, "EXPAND")

		self._settings_ctrlrs[id] = {
			default = false,
			type = "checkbox",
			ctrlr = ctrlr
		}
	end

	return 
end
LevelSettingsLayer._add_chunk_name = function (self, panel, sizer)
	local id = "chunk_name"
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 0, 1, "EXPAND,LEFT")

	local options = {
		"default",
		"init"
	}
	local combobox_params = {
		name = "Chunk Name",
		sizer_proportions = 1,
		name_proportions = 1,
		tooltip = "Select an option from the combobox",
		sorted = false,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = horizontal_sizer,
		options = options,
		value = self._settings.chunk_name or options[1]
	}
	local ctrlr = CoreEws.combobox(combobox_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "_set_data"), {
		ctrlr = ctrlr,
		value = id
	})

	self._settings_ctrlrs[id] = {
		type = "combobox",
		params = combobox_params,
		ctrlr = ctrlr,
		default = options[1]
	}

	return 
end
LevelSettingsLayer._set_data = function (self, data)
	self._settings[data.value] = data.ctrlr:get_value()
	self._settings[data.value] = tonumber(self._settings[data.value]) or self._settings[data.value]

	return 
end
LevelSettingsLayer.add_triggers = function (self)
	LevelSettingsLayer.super.add_triggers(self)

	local vc = self._editor_data.virtual_controller

	return 
end
LevelSettingsLayer.activate = function (self)
	LevelSettingsLayer.super.activate(self)

	return 
end
LevelSettingsLayer.deactivate = function (self)
	LevelSettingsLayer.super.deactivate(self)

	return 
end
LevelSettingsLayer.clear = function (self)
	LevelSettingsLayer.super.clear(self)

	for id, setting in pairs(self._settings_ctrlrs) do
		if setting.type == "combobox" then
			CoreEws.change_combobox_value(setting.params, setting.default)
		elseif setting.type == "checkbox" then
			setting.ctrlr:set_value(setting.default)
		end
	end

	self._settings = {}

	return 
end

return 
