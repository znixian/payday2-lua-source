core:module("CoreEditorCommand")

EditorCommand = EditorCommand or class()
EditorCommand.__type = EditorCommand
EditorCommand.UnitValues = {}
EditorCommand.init = function (self, layer)
	self._layer = layer
	self._values = {}

	return 
end
EditorCommand.__tostring = function (self)
	return "[Command base]"
end
EditorCommand.execute = function (self)
	print("Execute not implemented for EditorCommand")

	return 
end
EditorCommand.undo = function (self)
	print("Undo not implemented for EditorCommand")

	return 
end
EditorCommand.layer = function (self)
	return self._layer
end
EditorCommand.value = function (self, val, default)
	self._values = self._values or {}
	local v = self._values[val]

	if v ~= nil then
		return v
	else
		return default
	end

	return 
end
ReferenceUnitCommand = ReferenceUnitCommand or class(EditorCommand)
ReferenceUnitCommand.__type = ReferenceUnitCommand
ReferenceUnitCommand.UnitValues = {
	"reference_unit",
	"selected_units"
}
ReferenceUnitCommand.execute = function (self)
	if not self.are_unit_values_set(self) then
		self.save_unit_values(self)
	end

	return 
end
ReferenceUnitCommand.are_unit_values_set = function (self)
	return self._values.__set
end
ReferenceUnitCommand.save_unit_values = function (self)
	self._values.reference_unit = self._layer._selected_unit:unit_data().unit_id
	self._values.selected_units = {}

	for _, unit in ipairs(self._layer._selected_units) do
		table.insert(self._values.selected_units, unit.unit_data(unit).unit_id)
	end

	self._values.__set = true

	return 
end
ReferenceUnitCommand.get_saved_units = function (self)
	local reference_unit = managers.editor:unit_with_id(self.value(self, "reference_unit")) or managers.editor:get_special_unit_with_id(self.value(self, "reference_unit"))
	local units = {}

	for _, id in ipairs(self.value(self, "selected_units")) do
		local unit = managers.editor:unit_with_id(id)

		if unit then
			table.insert(units, unit)
		end
	end

	return reference_unit, units
end
MoveUnitCommand = MoveUnitCommand or class(ReferenceUnitCommand)
MoveUnitCommand.__type = MoveUnitCommand
MoveUnitCommand.UnitValues = {
	"reference_unit",
	"selected_units"
}
MoveUnitCommand.init = function (self, layer, command)
	MoveUnitCommand.super.init(self, layer)

	if command and command.are_unit_values_set(command) then
		self._values = command._values
	end

	return 
end
MoveUnitCommand.save_unit_values = function (self)
	MoveUnitCommand.super.save_unit_values(self)

	local reference_unit, units = self.get_saved_units(self)

	if alive(reference_unit) then
		self._values.original_pos = reference_unit.unit_data(reference_unit).world_pos
	end

	return 
end
MoveUnitCommand.execute = function (self, pos)
	MoveUnitCommand.super.execute(self)

	self._values.target_pos = pos or self._values.target_pos

	self.perform_move(self, self.value(self, "target_pos"), self.get_saved_units(self))

	return 
end
MoveUnitCommand.undo = function (self)
	self.perform_move(self, self.value(self, "original_pos"), self.get_saved_units(self))

	return 
end
MoveUnitCommand.perform_move = function (self, pos, reference, units)
	local reselect = false

	for _, unit in ipairs(self.layer(self):selected_units()) do
		if unit ~= reference and not table.contains(units, unit) then
			reselect = true
		end
	end

	if reselect then
		local select_units = {
			reference
		}

		for _, unit in ipairs(units) do
			table.insert(select_units, unit)
		end

		managers.editor:select_units(select_units)
	end

	for _, unit in ipairs(units) do
		if unit ~= reference then
			self.layer(self):set_unit_position(unit, pos, reference.rotation(reference))
		end
	end

	if alive(reference) then
		reference.set_position(reference, pos)

		reference.unit_data(reference).world_pos = pos

		self.layer(self):_on_unit_moved(reference, pos)
	end

	return 
end
MoveUnitCommand.__tostring = function (self)
	return string.format("[Command MoveUnit target: %s]", tostring(self.value(self, "target_pos")))
end
RotateUnitCommand = RotateUnitCommand or class(ReferenceUnitCommand)
RotateUnitCommand.__type = RotateUnitCommand
RotateUnitCommand.UnitValues = {
	"reference_unit",
	"selected_units"
}
RotateUnitCommand.init = function (self, layer, command)
	RotateUnitCommand.super.init(self, layer)

	if command and command.are_unit_values_set(command) then
		self._values = _G.clone(command._values)
	end

	return 
end
RotateUnitCommand.save_unit_values = function (self)
	MoveUnitCommand.super.save_unit_values(self)

	self._values.rot_add = Rotation()

	return 
end
RotateUnitCommand.execute = function (self, rot)
	RotateUnitCommand.super.execute(self)

	self._values.rot_add = (rot and self._values.rot_add*rot) or self._values.rot_add
	rot = rot or self._values.rot_add

	self.perform_rotation(self, rot or self._values.rot_add, self.get_saved_units(self))

	return 
end
RotateUnitCommand.undo = function (self)
	self.perform_rotation(self, self.value(self, "rot_add"):inverse(), self.get_saved_units(self))

	return 
end
RotateUnitCommand.perform_rotation = function (self, rot, reference, units)
	local rot = rot*reference.rotation(reference)

	reference.set_rotation(reference, rot)
	self.layer(self):_on_unit_rotated(reference, rot)

	for _, unit in ipairs(units) do
		if unit ~= reference then
			self.layer(self):set_unit_position(unit, reference.position(reference), rot)
			self.layer(self):set_unit_rotation(unit, rot)
		end
	end

	return 
end
RotateUnitCommand.__tostring = function (self)
	return string.format("[Command RotateUnit target: %s]", tostring(self.value(self, "rot_add")))
end
HideUnitsCommand = HideUnitsCommand or class(EditorCommand)
HideUnitsCommand.__type = HideUnitsCommand
HideUnitsCommand.UnitValues = {
	"units"
}
HideUnitsCommand.execute = function (self, units, hidden)
	if not self._values.units then
		self._values.units = {}

		for _, unit in ipairs(units) do
			table.insert(self._values.units, unit.unit_data(unit).unit_id)
		end

		self._values.hide = hidden
	end

	if hidden == nil then
		hidden = self.value(self, "hide")
	end

	self.hide_units(self, self.value(self, "units"), hidden)

	return 
end
HideUnitsCommand.undo = function (self)
	self.hide_units(self, self.value(self, "units"), not self.value(self, "hide"))

	return 
end
HideUnitsCommand.hide_units = function (self, units, hidden)
	for _, unit_id in ipairs(units) do
		local unit = managers.editor:unit_with_id(unit_id)

		if alive(unit) and unit.enabled(unit) then
			managers.editor:set_unit_visible(unit, not hidden)
		end
	end

	return 
end
HideUnitsCommand.__tostring = function (self)
	return string.format("[Command HideUnits hidden: %s]", tostring(self.value(self, "hide")))
end
SpawnUnitCommand = SpawnUnitCommand or class(EditorCommand)
SpawnUnitCommand.__type = SpawnUnitCommand
SpawnUnitCommand.UnitValues = {
	"spawned_unit"
}
SpawnUnitCommand.execute = function (self, name, pos, rot, to_continent_name, prefered_id)
	if name and pos and rot then
		self._values.args = {
			name,
			pos,
			rot,
			to_continent_name,
			prefered_id
		}
	end

	local unit = self.layer(self):create_unit(unpack(self._values.args))

	table.insert(self.layer(self)._created_units, unit)

	self.layer(self)._created_units_pairs[unit.unit_data(unit).unit_id] = unit

	self.layer(self):set_select_unit(unit)

	self._values.spawned_unit = unit.unit_data(unit).unit_id

	return unit
end
SpawnUnitCommand.undo = function (self)
	local unit = managers.editor:unit_with_id(self.value(self, "spawned_unit"))

	if alive(unit) then
		self.layer(self):delete_unit(unit, true)
	end

	return 
end
SpawnUnitCommand.__tostring = function (self)
	return string.format("[Command SpawnUnit %s]", tostring(self._values.args[1]))
end
DeleteStaticUnitCommand = DeleteStaticUnitCommand or class(EditorCommand)
DeleteStaticUnitCommand.__type = DeleteStaticUnitCommand
DeleteStaticUnitCommand.__priority = 1000000
DeleteStaticUnitCommand.UnitValues = {
	"unit"
}
DeleteStaticUnitCommand.IgnoredRestoreKeys = {
	"id",
	"name_id",
	"continent"
}
DeleteStaticUnitCommand.execute = function (self, unit)
	unit = unit or managers.editor:unit_with_id(self.value(self, "id"))

	self.save_unit_values(self, unit)

	if self.layer(self):selected_unit() == unit then
		self.layer(self):set_reference_unit(nil)
		self.layer(self):update_unit_settings()
	end

	table.delete(self.layer(self)._created_units, unit)

	self.layer(self)._created_units_pairs[unit.unit_data(unit).unit_id] = nil

	self.layer(self):remove_unit(unit)

	return 
end
DeleteStaticUnitCommand.undo = function (self)
	local unit = self.layer(self):do_spawn_unit(self.value(self, "name"), self.value(self, "pos"), self.value(self, "rot"), self.value(self, "continent"), true, self.value(self, "id"))

	self.restore_unit_values(self, unit)
	self.layer(self):set_name_id(unit, self._values.unit_data.name_id)
	self.layer(self):clone_edited_values(unit, unit)

	return 
end
DeleteStaticUnitCommand.save_unit_values = function (self, unit)
	self._values.id = unit.unit_data(unit).unit_id
	self._values.name = unit.name(unit)
	self._values.pos = unit.position(unit)
	self._values.rot = unit.rotation(unit)
	self._values.continent = unit.unit_data(unit).continent
	self._values.unit_data = unit.unit_data(unit)

	return 
end
DeleteStaticUnitCommand.restore_unit_values = function (self, unit)
	for key, value in pairs(self._values.unit_data) do
		if not table.contains(self.IgnoredRestoreKeys, key) then
			unit.unit_data(unit)[key] = value
		end
	end

	return 
end
DeleteStaticUnitCommand.__tostring = function (self)
	return string.format("[Command DeleteStaticUnit %s(%s)]", tostring(self._values.name), tostring(self._values.id))
end
MissionElementEditorCommand = MissionElementEditorCommand or class(EditorCommand)
MissionElementEditorCommand.__type = MissionElementEditorCommand
MissionElementEditorCommand.init = function (self, mission_element)
	MissionElementEditorCommand.super.init(self, managers.editor:layer("Mission"))

	self._values.element_id = mission_element._unit:unit_data().unit_id

	return 
end
MissionElementEditorCommand.get_self_mission_element = function (self)
	return managers.editor:unit_with_id(self._values.element_id):mission_element()
end
DeleteMissionElementCommand = DeleteMissionElementCommand or class(MissionElementEditorCommand)
DeleteMissionElementCommand.__type = DeleteMissionElementCommand
DeleteMissionElementCommand.__priority = 100000
DeleteMissionElementCommand.execute = function (self, mission_element)
	mission_element = mission_element or self.get_self_mission_element(self)

	for idx, key in ipairs(mission_element._save_values) do
		self._values[key] = mission_element._hed[key]
	end

	return 
end
DeleteMissionElementCommand.undo = function (self, is_final_action)
	local mission_element = self.get_self_mission_element(self)

	for idx, key in ipairs(mission_element._save_values) do
		mission_element._hed[key] = self._values[key]
	end

	if is_final_action then
		mission_element.destroy_panel(mission_element)
		mission_element.panel(mission_element)
		managers.editor:layer("Mission"):update_unit_settings()
	end

	return 
end
DeleteMissionElementCommand.__tostring = function (self)
	return string.format("[Command DeleteMissionElement %s]", tostring(self._values.element_id))
end
MissionElementAddOnExecutedCommand = MissionElementAddOnExecutedCommand or class(MissionElementEditorCommand)
MissionElementAddOnExecutedCommand.__type = MissionElementAddOnExecutedCommand
MissionElementAddOnExecutedCommand.execute = function (self, mission_element, unit, existing_params)
	mission_element = mission_element or self.get_self_mission_element(self)
	unit = unit or managers.editor:unit_with_id(self._values.params.id)
	local params = existing_params or self._values.params or {}
	params.id = unit.unit_data(unit).unit_id
	params.delay = params.delay or 0
	params.alternative = params.alternative or (mission_element.ON_EXECUTED_ALTERNATIVES and mission_element.ON_EXECUTED_ALTERNATIVES[1]) or nil

	table.insert(mission_element._on_executed_units, unit)
	table.insert(mission_element._hed.on_executed, params)

	if mission_element._timeline then
		mission_element._timeline:add_element(unit, params)
	end

	mission_element.append_elements_sorted(mission_element)
	mission_element.set_on_executed_element(mission_element, unit)

	self._values.params = params

	return 
end
MissionElementAddOnExecutedCommand.undo = function (self)
	local mission_element = self.get_self_mission_element(self)
	local remove_unit = managers.editor:unit_with_id(self._values.params.id)
	local command = MissionElementRemoveOnExecutedCommand:new(mission_element)

	command.execute(command, mission_element, remove_unit)

	return 
end
MissionElementAddOnExecutedCommand.__tostring = function (self)
	return string.format("[Command AddOnExecuted %s]", tostring(self._values.element_id))
end
MissionElementRemoveOnExecutedCommand = MissionElementRemoveOnExecutedCommand or class(MissionElementEditorCommand)
MissionElementRemoveOnExecutedCommand.__type = MissionElementRemoveOnExecutedCommand
MissionElementRemoveOnExecutedCommand.execute = function (self, mission_element, unit)
	mission_element = mission_element or self.get_self_mission_element(self)
	unit = unit or managers.editor:unit_with_id(self._values.unit_id)
	self._values.unit_id = unit.unit_data(unit).unit_id

	for _, on_executed in ipairs(mission_element._hed.on_executed) do
		if on_executed.id == unit.unit_data(unit).unit_id then
			self._values.removed_on_executed = on_executed

			if mission_element._timeline then
				mission_element._timeline:remove_element(on_executed)
			end

			table.delete(mission_element._hed.on_executed, on_executed)
			table.delete(mission_element._on_executed_units, unit)
			mission_element.append_elements_sorted(mission_element)

			return true
		end
	end

	return false
end
MissionElementRemoveOnExecutedCommand.undo = function (self)
	local mission_element = self.get_self_mission_element(self)
	local add_unit = managers.editor:unit_with_id(self._values.removed_on_executed.id)
	local command = MissionElementAddOnExecutedCommand:new(mission_element)

	command.execute(command, mission_element, add_unit, self._values.removed_on_executed)

	return 
end
MissionElementRemoveOnExecutedCommand.__tostring = function (self)
	return string.format("[Command RemoveOnExecuted %s]", tostring(self._values.element_id))
end
MissionElementAddLinkElementCommand = MissionElementAddLinkElementCommand or class(MissionElementEditorCommand)
MissionElementAddLinkElementCommand.__type = MissionElementAddLinkElementCommand
MissionElementAddLinkElementCommand.execute = function (self, mission_element, link_name, unit_id)
	mission_element = mission_element or self.get_self_mission_element(self)
	link_name = link_name or self._values.link_name
	unit_id = unit_id or self._values.unit_id
	self._values.link_name = link_name
	self._values.unit_id = unit_id

	if mission_element._hed[link_name] and not table.contains(mission_element._hed[link_name], unit_id) then
		table.insert(mission_element._hed[link_name], unit_id)
		mission_element.on_added_link_element(mission_element, link_name, unit_id)

		return true
	else
		return false
	end

	return 
end
MissionElementAddLinkElementCommand.undo = function (self)
	local mission_element = self.get_self_mission_element(self)
	local command = MissionElementRemoveLinkElementCommand:new(mission_element)

	command.execute(command, mission_element, self.value(self, "link_name"), self.value(self, "unit_id"))

	return 
end
MissionElementAddLinkElementCommand.__tostring = function (self)
	return string.format("[Command AddLinkElement [%s %s]]", tostring(self.value(self, "link_name")), tostring(self.value(self, "unit_id")))
end
MissionElementRemoveLinkElementCommand = MissionElementRemoveLinkElementCommand or class(MissionElementEditorCommand)
MissionElementRemoveLinkElementCommand.__type = MissionElementRemoveLinkElementCommand
MissionElementRemoveLinkElementCommand.execute = function (self, mission_element, link_name, unit_id)
	mission_element = mission_element or self.get_self_mission_element(self)
	link_name = link_name or self._values.link_name
	unit_id = unit_id or self._values.unit_id
	self._values.link_name = link_name
	self._values.unit_id = unit_id

	if table.contains(mission_element._hed[link_name], unit_id) then
		table.delete(mission_element._hed[link_name], unit_id)
		mission_element.on_removed_link_element(mission_element, link_name, unit_id)

		return true
	else
		return false
	end

	return 
end
MissionElementRemoveLinkElementCommand.undo = function (self)
	local mission_element = self.get_self_mission_element(self)
	local command = MissionElementAddLinkElementCommand:new(mission_element)

	command.execute(command, mission_element, self.value(self, "link_name"), self.value(self, "unit_id"))

	return 
end
MissionElementRemoveLinkElementCommand.__tostring = function (self)
	return string.format("[Command RemoveLinkElement [%s %s]]", tostring(self.value(self, "link_name")), tostring(self.value(self, "unit_id")))
end

return 
