core:import("CoreEditorUtils")

CoreEditorGroups = CoreEditorGroups or class()
CoreEditorGroups.init = function (self)
	self._groups = {}
	self._group_names = {}

	return 
end
CoreEditorGroups.groups = function (self)
	return self._groups
end
CoreEditorGroups.group_names = function (self)
	return self._group_names
end
CoreEditorGroups.update = function (self, t, dt)
	for _, group in pairs(self._groups) do
		group.draw(group, t, dt)
	end

	return 
end
CoreEditorGroups.create = function (self, name, reference, units)
	if not table.contains(self._group_names, name) then
		table.insert(self._group_names, name)
	end

	local group = CoreEditorGroup:new(name, reference, units)
	self._groups[name] = group

	return group
end
CoreEditorGroups.add = function (self, name, units)
	local group = self._groups[name]

	group.add(group, units)

	return 
end
CoreEditorGroups.remove = function (self, name)
	table.delete(self._group_names, name)
	self._groups[name]:remove()

	self._groups[name] = nil

	return 
end
CoreEditorGroups.clear = function (self)
	return 
end
CoreEditorGroups.group_name = function (self)
	local name = EWS:get_text_from_user(Global.frame_panel, "Enter name for the new group:", "Create Group", self.new_group_name(self), Vector3(-1, -1, 0), true)

	if name and name ~= "" then
		if self._groups[name] then
			self.group_name(self)
		else
			return name
		end
	end

	return nil
end
CoreEditorGroups.new_group_name = function (self)
	local s = "Group0"
	local i = 1

	while self._groups[s .. i] do
		i = i + 1

		if 9 < i then
			s = "Group"
		end
	end

	return s .. i
end
CoreEditorGroups.save = function (self)
	for _, name in ipairs(self._group_names) do
		local group = self._groups[name]

		if group then
			local units = {}

			for _, unit in ipairs(group.units(group)) do
				table.insert(units, unit.unit_data(unit).unit_id)
			end

			local t = {
				entry = "editor_groups",
				continent = group.continent(group) and group.continent_name(group),
				data = {
					name = group.name(group),
					reference = group.reference(group):unit_data().unit_id,
					continent = group.continent(group) and group.continent_name(group),
					units = units
				}
			}

			managers.editor:add_save_data(t)
		end
	end

	return 
end
CoreEditorGroups.load = function (self, world_holder, offset)
	local load_data = world_holder.create_world(world_holder, "world", "editor_groups", offset)
	local group_names = load_data.group_names
	local groups = {}

	for name, data in pairs(load_data.groups) do
		if 0 < #data.units then
			local reference = managers.worlddefinition:get_unit(data.reference)
			local units = {}

			for _, unit in ipairs(data.units) do
				table.insert(units, managers.worlddefinition:get_unit(unit))
			end

			local continent = nil

			if data.continent then
				continent = managers.editor:continent(data.continent)
			end

			if not table.contains(units, reference) then
				reference = units[1]

				cat_error("editor", "Changed reference for group,", name, ".")
			end

			groups[name] = {
				reference = reference,
				units = units,
				continent = continent
			}
		else
			table.delete(group_names, name)
			cat_error("editor", "Removed old group", name, "since it didnt contain any units.")
		end
	end

	for _, name in ipairs(group_names) do
		if not groups[name].reference then
			managers.editor:output_error("Reference unit is nil since there are no units left in group. Will remove, " .. name .. ".")
		else
			local group = self.create(self, name, groups[name].reference, groups[name].units)

			group.set_continent(group, groups[name].continent)
		end
	end

	return 
end
CoreEditorGroups.load_group = function (self)
	local path = managers.database:open_file_dialog(Global.frame, "XML-file (*.xml)|*.xml")

	if path then
		self.load_group_file(self, path)
	end

	return 
end
CoreEditorGroups.load_group_file = function (self, path)
	local name = self.group_name(self)

	if not name then
		return 
	end

	local node = SystemFS:parse_xml(path)
	local layer_name = "Statics"

	if node.has_parameter(node, "layer") then
		layer_name = node.parameter(node, "layer")
	end

	local layer = managers.editor:layer(layer_name)
	local pos = managers.editor:current_layer():current_pos()

	managers.editor:change_layer_notebook(layer_name)

	local reference = nil
	local units = {}

	if pos then
		for unit in node.children(node) do
			local rot, new_unit = nil

			if unit.name(unit) == "ref_unit" then
				reference = layer.do_spawn_unit(layer, unit.parameter(unit, "name"), pos)
				new_unit = reference
			else
				local pos = pos + math.string_to_vector(unit.parameter(unit, "local_pos"))
				local rot = math.string_to_rotation(unit.parameter(unit, "local_rot"))
				new_unit = layer.do_spawn_unit(layer, unit.parameter(unit, "name"), pos, rot)
			end

			for setting in unit.children(unit) do
				if setting.name(setting) == "light" then
					self.parse_light(self, new_unit, setting)
				elseif setting.name(setting) == "variation" then
					self.parse_variation(self, new_unit, setting)
				elseif setting.name(setting) == "material_variation" then
					self.parse_material_variation(self, new_unit, setting)
				elseif setting.name(setting) == "editable_gui" then
					self.parse_editable_gui(self, new_unit, setting)
				end
			end

			table.insert(units, new_unit)
		end

		self.create(self, name, reference, units)
		layer.select_group(layer, self._groups[name])
	end

	return 
end
CoreEditorGroups.parse_light = function (self, unit, node)
	local light = unit.get_object(unit, Idstring(node.parameter(node, "name")))

	if not light then
		return 
	end

	light.set_enable(light, toboolean(node.parameter(node, "enabled")))
	light.set_far_range(light, tonumber(node.parameter(node, "far_range")))
	light.set_color(light, math.string_to_vector(node.parameter(node, "color")))
	light.set_spot_angle_start(light, tonumber(node.parameter(node, "angle_start")))
	light.set_spot_angle_end(light, tonumber(node.parameter(node, "angle_end")))
	light.set_multiplier(light, LightIntensityDB:lookup(Idstring(node.parameter(node, "multiplier"))))

	if node.has_parameter(node, "falloff_exponent") then
		light.set_falloff_exponent(light, tonumber(node.parameter(node, "falloff_exponent")))
	end

	return 
end
CoreEditorGroups.parse_variation = function (self, unit, node)
	local variation = node.parameter(node, "value")

	if variation ~= "default" then
		unit.unit_data(unit).mesh_variation = variation

		managers.sequence:run_sequence_simple2(unit.unit_data(unit).mesh_variation, "change_state", unit)
	end

	return 
end
CoreEditorGroups.parse_material_variation = function (self, unit, node)
	local material_variation = node.parameter(node, "value")

	if material_variation ~= "default" then
		unit.unit_data(unit).material = material_variation

		unit.set_material_config(unit, unit.unit_data(unit).material, true)
	end

	return 
end
CoreEditorGroups.parse_editable_gui = function (self, unit, node)
	unit.editable_gui(unit):set_text(node.parameter(node, "text"))
	unit.editable_gui(unit):set_font_color(math.string_to_vector(node.parameter(node, "font_color")))
	unit.editable_gui(unit):set_font_size(tonumber(node.parameter(node, "font_size")))
	unit.editable_gui(unit):set_font(node.parameter(node, "font"))
	unit.editable_gui(unit):set_align(node.parameter(node, "align"))
	unit.editable_gui(unit):set_vertical(node.parameter(node, "vertical"))
	unit.editable_gui(unit):set_blend_mode(node.parameter(node, "blend_mode"))
	unit.editable_gui(unit):set_render_template(node.parameter(node, "render_template"))
	unit.editable_gui(unit):set_wrap(node.parameter(node, "wrap") == "true")
	unit.editable_gui(unit):set_word_wrap(node.parameter(node, "word_wrap") == "true")
	unit.editable_gui(unit):set_alpha(tonumber(node.parameter(node, "alpha")))
	unit.editable_gui(unit):set_shape(string.split(node.parameter(node, "shape"), " "))

	return 
end
CoreEditorGroup = CoreEditorGroup or class()
CoreEditorGroup.init = function (self, name, reference, units)
	self._name = name
	self._reference = reference
	self._units = {}
	self._continent = managers.editor:current_continent()

	for _, unit in ipairs(units) do
		self.add_unit(self, unit)
	end

	self._closed = true

	return 
end
CoreEditorGroup.closed = function (self)
	return self._closed
end
CoreEditorGroup.set_closed = function (self, closed)
	self._closed = closed

	return 
end
CoreEditorGroup.name = function (self)
	return self._name
end
CoreEditorGroup.units = function (self)
	return self._units
end
CoreEditorGroup.continent = function (self)
	return self._continent
end
CoreEditorGroup.set_continent = function (self, continent)
	self._continent = continent

	return 
end
CoreEditorGroup.continent_name = function (self)
	return tostring(self._continent and self._continent:name())
end
CoreEditorGroup.add = function (self, units)
	return 
end
CoreEditorGroup.add_unit = function (self, unit)
	if not unit then
		return 
	end

	table.insert(self._units, unit)

	unit.unit_data(unit).editor_groups = unit.unit_data(unit).editor_groups or {}

	table.insert(unit.unit_data(unit).editor_groups, self)

	return 
end
CoreEditorGroup.remove_unit = function (self, unit)
	table.delete(self._units, unit)
	table.delete(unit.unit_data(unit).editor_groups, self)

	if unit == self._reference then
		if 0 < #self._units then
			self._reference = self._units[1]
		else
			managers.editor:remove_group(self._name)
		end
	end

	return 
end
CoreEditorGroup.remove = function (self)
	for _, unit in ipairs(self._units) do
		table.delete(unit.unit_data(unit).editor_groups, self)
	end

	return 
end
CoreEditorGroup.reference = function (self)
	return self._reference
end
CoreEditorGroup.set_reference = function (self, reference)
	self._reference = reference

	return 
end
CoreEditorGroup.save_to_file = function (self)
	local path = managers.database:save_file_dialog(Global.frame, true, "XML-file (*.xml)|*.xml", self.name(self))

	if path then
		local f = SystemFS:open(path, "w")

		f.puts(f, "<group name=\"" .. self.name(self) .. "\" layer=\"" .. managers.editor:current_layer_name() .. "\">")
		f.puts(f, "\t<ref_unit name=\"" .. self._reference:name():s() .. "\">")
		self.save_edited_settings(self, f, "\t\t", self._reference)
		f.puts(f, "\t</ref_unit>")

		for _, unit in ipairs(self._units) do
			if unit ~= self._reference then
				local name = unit.name(unit):s()
				local local_pos = unit.unit_data(unit).local_pos
				local local_rot = unit.unit_data(unit).local_rot
				local x = string.format("%.4f", local_pos.x)
				local y = string.format("%.4f", local_pos.y)
				local z = string.format("%.4f", local_pos.z)
				local_pos = "" .. x .. " " .. y .. " " .. z
				local yaw = string.format("%.4f", local_rot.yaw(local_rot))
				local pitch = string.format("%.4f", local_rot.pitch(local_rot))
				local roll = string.format("%.4f", local_rot.roll(local_rot))
				local_rot = "" .. yaw .. " " .. pitch .. " " .. roll

				f.puts(f, "\t<unit name=\"" .. name .. "\" local_pos=\"" .. local_pos .. "\" local_rot=\"" .. local_rot .. "\">")
				self.save_edited_settings(self, f, "\t\t", unit)
				f.puts(f, "\t</unit>")
			end
		end

		f.puts(f, "</group>")
		SystemFS:close(f)
	end

	return 
end
CoreEditorGroup.save_edited_settings = function (self, ...)
	self.save_lights(self, ...)
	self.save_variation(self, ...)
	self.save_editable_gui(self, ...)

	return 
end
CoreEditorGroup.save_lights = function (self, file, t, unit, data_table)
	local lights = CoreEditorUtils.get_editable_lights(unit) or {}

	for _, light in ipairs(lights) do
		local c_s = "" .. light.color(light).x .. " " .. light.color(light).y .. " " .. light.color(light).z
		local as = light.spot_angle_start(light)
		local ae = light.spot_angle_end(light)
		local multiplier = CoreEditorUtils.get_intensity_preset(light.multiplier(light)):s()
		local falloff_exponent = light.falloff_exponent(light)

		file.puts(file, t .. "<light name=\"" .. light.name(light):s() .. "\" enabled=\"" .. tostring(light.enable(light)) .. "\" far_range=\"" .. light.far_range(light) .. "\" color=\"" .. c_s .. "\" angle_start=\"" .. as .. "\" angle_end=\"" .. ae .. "\" multiplier=\"" .. multiplier .. "\" falloff_exponent=\"" .. falloff_exponent .. "\"/>")
	end

	return 
end
CoreEditorGroup.save_variation = function (self, file, t, unit, data_table)
	if unit.unit_data(unit).mesh_variation and 0 < #managers.sequence:get_editable_state_sequence_list(unit.name(unit)) then
		file.puts(file, t .. "<variation value=\"" .. unit.unit_data(unit).mesh_variation .. "\"/>")
	end

	if unit.unit_data(unit).material and unit.unit_data(unit).material ~= "default" then
		file.puts(file, t .. "<material_variation value=\"" .. unit.unit_data(unit).material .. "\"/>")
	end

	return 
end
CoreEditorGroup.save_editable_gui = function (self, file, t, unit, data_table)
	if unit.editable_gui(unit) then
		local text = unit.editable_gui(unit):text()
		local font_color = unit.editable_gui(unit):font_color()
		local font_size = unit.editable_gui(unit):font_size()
		local font = unit.editable_gui(unit):font()
		local align = unit.editable_gui(unit):align()
		local vertical = unit.editable_gui(unit):vertical()
		local blend_mode = unit.editable_gui(unit):blend_mode()
		local render_template = unit.editable_gui(unit):render_template()
		local wrap = unit.editable_gui(unit):wrap()
		local word_wrap = unit.editable_gui(unit):word_wrap()
		local alpha = unit.editable_gui(unit):alpha()
		local shape = unit.editable_gui(unit):shape()

		file.puts(file, t .. "<editable_gui text=\"" .. text .. "\" font_size=\"" .. font_size .. "\" font_color=\"" .. math.vector_to_string(font_color) .. "\" font=\"" .. font .. "\" align=\"" .. align .. "\" vertical=\"" .. vertical .. "\" blend_mode=\"" .. blend_mode .. "\" render_template=\"" .. render_template .. "\" wrap=\"" .. tostring(wrap) .. "\" word_wrap=\"" .. tostring(word_wrap) .. "\" alpha=\"" .. alpha .. "\" shape=\"" .. string.format("%s %s %s %s", unpack(shape)) .. "\"/>")
	end

	return 
end
CoreEditorGroup.draw = function (self, t, dt)
	local i = 0.25

	if managers.editor:using_groups() then
		if self._continent ~= managers.editor:current_continent() then
			return 
		end

		i = 0.65
	elseif not managers.editor:debug_draw_groups() then
		return 
	end

	for _, unit in ipairs(self._units) do
		Application:draw(unit, i*1, i*1, i*1)
	end

	Application:draw(self._reference, 0, i*1, 0)

	return 
end
GroupPresetsDialog = GroupPresetsDialog or class(CoreEditorEwsDialog)
GroupPresetsDialog.init = function (self, files, path)
	self._path = path

	CoreEditorEwsDialog.init(self, nil, "Group Presets", "", Vector3(300, 150, 0), Vector3(200, 300, 0), "DEFAULT_DIALOG_STYLE,RESIZE_BORDER,STAY_ON_TOP")
	self.create_panel(self, "VERTICAL")

	self._hide_on_create = true
	local option_sizer = EWS:BoxSizer("VERTICAL")
	local hide = EWS:CheckBox(self._panel, "Hide on create", "")

	hide.set_value(hide, self._hide_on_create)
	hide.connect(hide, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "hide_on_create"), hide)
	hide.connect(hide, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	option_sizer.add(option_sizer, hide, 0, 2, "RIGHT,LEFT")
	self._panel_sizer:add(option_sizer, 0, 0, "ALIGN_RIGHT")

	self._list = EWS:ListBox(self._panel, "", "LB_SINGLE,LB_HSCROLL,LB_NEEDED_SB,LB_SORT")

	self._panel_sizer:add(self._list, 1, 0, "EXPAND")

	for _, file in ipairs(files) do
		self._list:append(file)
	end

	self._list:connect("EVT_COMMAND_LISTBOX_SELECTED", callback(self, self, "select_group"), nil)
	self._list:connect("EVT_COMMAND_LISTBOX_DOUBLECLICKED", callback(self, self, "create_group"), nil)
	self._list:connect("EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local button_sizer = EWS:BoxSizer("HORIZONTAL")
	local create_btn = EWS:Button(self._panel, "Create", "", "")

	button_sizer.add(button_sizer, create_btn, 0, 2, "RIGHT,LEFT")
	create_btn.connect(create_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "create_group"), "")
	create_btn.connect(create_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local cancel_btn = EWS:Button(self._panel, "Cancel", "", "")

	button_sizer.add(button_sizer, cancel_btn, 0, 2, "RIGHT,LEFT")
	cancel_btn.connect(cancel_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_cancel"), "")
	cancel_btn.connect(cancel_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	self._panel_sizer:add(button_sizer, 0, 0, "ALIGN_RIGHT")
	self._dialog_sizer:add(self._panel, 1, 0, "EXPAND")
	self._dialog:set_visible(true)

	return 
end
GroupPresetsDialog.select_group = function (self)
	local i = self._list:selected_index()

	if -1 < i then
		local name = self._list:get_string(i)
		self._file = managers.database:base_path() .. self._path .. "\\" .. name
	end

	return 
end
GroupPresetsDialog.create_group = function (self)
	if self._file then
		if self._hide_on_create then
			self._dialog:set_visible(false)
		end

		managers.editor:groups():load_group_file(self._file)
	end

	return 
end
GroupPresetsDialog.hide_on_create = function (self, hide)
	self._hide_on_create = hide.get_value(hide)

	return 
end

return 
