core:module("CoreLayer")
core:import("CoreEngineAccess")
core:import("CoreEditorSave")
core:import("CoreEditorUtils")
core:import("CoreEditorWidgets")
core:import("CoreEvent")
core:import("CoreClass")
core:import("CoreCode")
core:import("CoreInput")
core:import("CoreTable")
core:import("CoreStack")
core:import("CoreUnit")
core:import("CoreEditorCommand")
core:import("CoreEditorCommandBlock")

Layer = Layer or CoreClass.class()
Layer.init = function (self, owner, save_name)
	if not owner then
		Application:error("Layer:init was called without parameters owner and save_name")
	end

	self._owner = owner or self._owner
	self._save_name = save_name
	self._confirmed_unit_types = {}
	self._unit_map = {}
	self._unit_types = {}
	self._created_units = {}
	self._created_units_pairs = {}
	self._selected_units = {}
	self._name_ids = {}
	self._notebook_units_lists = {}
	self._editor_data = self._owner._editor_data
	self._ctrl = self._editor_data.virtual_controller
	self._move_widget = CoreEditorWidgets.MoveWidget:new(self)
	self._rotate_widget = CoreEditorWidgets.RotationWidget:new(self)
	self._layer_enabled = true
	self._will_use_widgets = false
	self._using_widget = false
	self._ignore_global_select = false
	self._marker_sphere_size = 25
	self._uses_continents = false

	self._init_unit_highlighter(self)

	return 
end
Layer._init_unit_highlighter = function (self)
	self._unit_highlighter = World:unit_manager():unit_highlighter()

	self._unit_highlighter:add_config("highlight", "highlight", "highlight_skinned")
	self._unit_highlighter:set_config_name_filter("highlight", "g_*", "gfx_*")

	self._highlighted_units = {}

	return 
end
Layer.created_units = function (self)
	return self._created_units
end
Layer.created_units_pairs = function (self)
	return self._created_units_pairs
end
Layer.selected_units = function (self)
	return self._selected_units
end
Layer.current_pos = function (self)
	return self._current_pos
end
Layer.uses_continents = function (self)
	return self._uses_continents
end
Layer.owner = function (self)
	return self._owner
end
Layer.load = function (self, world_holder, offset)
	local world_units = world_holder.create_world(world_holder, "world", self._save_name, offset)

	if world_units then
		for _, unit in ipairs(world_units) do
			self.add_unit_to_created_units(self, unit)
		end
	end

	return world_units
end
Layer.post_load = function (self)
	if not self._post_register_units then
		return 
	end

	for _, unit in ipairs(self._post_register_units) do
		local previous_id = unit.unit_data(unit).unit_id
		unit.unit_data(unit).unit_id = self._owner:get_unit_id(unit)
		local msg = "A unit, " .. unit.name(unit):s() .. " in layer " .. self._save_name .. ", had duplicate unit id. The unit id was changed from " .. previous_id .. " to " .. unit.unit_data(unit).unit_id .. ".\n\nPlease verify that no references to the unit is broken."

		EWS:message_box(Global.frame_panel, msg, self._save_name, "OK,ICON_ERROR", Vector3(-1, -1, 0))
		managers.editor:output(msg)
		self.add_unit_to_created_units(self, unit, true)
	end

	self._post_register_units = nil

	return 
end
Layer.add_unit_to_created_units = function (self, unit, skip_register)
	if not skip_register and not self._owner:register_unit_id(unit) then
		self._post_register_units = self._post_register_units or {}

		table.insert(self._post_register_units, unit)

		return 
	end

	self.set_up_name_id(self, unit)
	table.insert(self._created_units, unit)

	self._created_units_pairs[unit.unit_data(unit).unit_id] = unit

	return 
end
Layer.set_up_name_id = function (self, unit)
	if unit.unit_data(unit).name_id == "none" then
		unit.unit_data(unit).name_id = self.get_name_id(self, unit)
	else
		self.insert_name_id(self, unit)
	end

	return 
end
Layer.insert_name_id = function (self, unit)
	local name = unit.name(unit):s()
	self._name_ids[name] = self._name_ids[name] or {}
	local name_id = unit.unit_data(unit).name_id
	self._name_ids[name][name_id] = (self._name_ids[name][name_id] or 0) + 1

	return 
end
Layer.get_name_id = function (self, unit, name)
	local u_name = unit.name(unit):s()
	local start_number = 1

	if name then
		local sub_name = name

		for i = string.len(name), 0, -1 do
			local sub = string.sub(name, i, string.len(name))
			sub_name = string.sub(name, 0, i)

			if tonumber(sub) then
				start_number = tonumber(sub)
			else
				break
			end
		end

		name = sub_name
	else
		local reverse = string.reverse(u_name)
		local i = string.find(reverse, "/")
		name = string.reverse(string.sub(reverse, 0, i - 1))
		name = name .. "_"
	end

	self._name_ids[u_name] = self._name_ids[u_name] or {}
	local t = self._name_ids[u_name]

	for i = start_number, 10000, 1 do
		i = ((i < 10 and "00") or (i < 100 and "0") or "") .. i
		local name_id = name .. i

		if not t[name_id] then
			t[name_id] = 1

			return name_id
		end
	end

	return 
end
Layer.remove_name_id = function (self, unit)
	local unit_name = unit.name(unit):s()

	if self._name_ids[unit_name] then
		local name_id = unit.unit_data(unit).name_id
		self._name_ids[unit_name][name_id] = self._name_ids[unit_name][name_id] - 1

		if self._name_ids[unit_name][name_id] == 0 then
			self._name_ids[unit_name][name_id] = nil
		end
	end

	return 
end
Layer.set_name_id = function (self, unit, name_id)
	local unit_name = unit.name(unit):s()

	if self._name_ids[unit_name] then
		self.remove_name_id(self, unit)

		self._name_ids[unit_name][name_id] = (self._name_ids[unit_name][name_id] or 0) + 1
		unit.unit_data(unit).name_id = name_id

		managers.editor:unit_name_changed(unit)
	end

	return 
end
Layer.widget_affect_object = function (self)
	return self._selected_unit
end
Layer.use_widget_position = function (self, pos)
	self.set_unit_positions(self, pos)

	return 
end
Layer.use_widget_rotation = function (self, rot)
	self.set_unit_rotations(self, rot*self.widget_affect_object(self):rotation():inverse())

	return 
end
Layer.update = function (self, t, dt)
	self._update_widget_affect_object(self, t, dt)
	self._update_drag_select(self, t, dt)
	self._update_draw_unit_trigger_sequences(self, t, dt)

	return 
end
Layer._update_widget_affect_object = function (self, t, dt)
	if alive(self.widget_affect_object(self)) then
		local widget_pos = managers.editor:world_to_screen(self.widget_affect_object(self):position())

		if 100 < widget_pos.z then
			widget_pos = widget_pos.with_z(widget_pos, 0)
			local widget_screen_pos = widget_pos
			widget_pos = managers.editor:screen_to_world(widget_pos, 1000)
			local widget_rot = self.widget_rot(self)

			if self._using_widget then
				if self._move_widget:enabled() then
					local result_pos = self._move_widget:calculate(self.widget_affect_object(self), widget_rot, widget_pos, widget_screen_pos)

					if self._last_pos ~= result_pos then
						self.use_widget_position(self, result_pos)
					end

					self._last_pos = result_pos
				end

				if self._rotate_widget:enabled() then
					local result_rot = self._rotate_widget:calculate(self.widget_affect_object(self), widget_rot, widget_pos, widget_screen_pos)

					if self._last_rot ~= result_rot then
						self.use_widget_rotation(self, result_rot)
					end

					self._last_rot = result_rot
				end
			end

			if not self._using_widget and (self._move_widget:enabled() or self._rotate_widget:enabled()) then
				if self._move_widget:enabled() and self._last_pos ~= nil then
					self.use_widget_position(self, self._last_pos)

					self._last_pos = nil
				end

				if self._rotate_widget:enabled() and self._last_rot ~= nil then
					self.use_widget_rotation(self, self._last_rot)

					self._last_rot = nil
				end
			end

			if self._move_widget:enabled() then
				self._move_widget:set_position(widget_pos)
				self._move_widget:set_rotation(widget_rot)
				self._move_widget:update(t, dt)
			end

			if self._rotate_widget:enabled() then
				self._rotate_widget:set_position(widget_pos)
				self._rotate_widget:set_rotation(widget_rot)
				self._rotate_widget:update(t, dt)
			end
		end
	end

	return 
end
Layer._update_drag_select = function (self, t, dt)
	if not self._drag_select then
		return 
	end

	local end_pos = managers.editor:cursor_pos()

	if self._polyline then
		local p1 = managers.editor:screen_pos(self._drag_start_pos)
		local p3 = managers.editor:screen_pos(end_pos)
		local p2 = Vector3(p3.x, p1.y, 0)
		local p4 = Vector3(p1.x, p3.y, 0)

		self._polyline:set_points({
			p1,
			p2,
			p3,
			p4
		})
	end

	local len = self._drag_start_pos - end_pos:length()

	if 0.05 < len then
		local top_left = self._drag_start_pos
		local bottom_right = end_pos

		if (bottom_right.y < top_left.y and top_left.x < bottom_right.x) or (top_left.y < bottom_right.y and bottom_right.x < top_left.x) then
			top_left = Vector3(self._drag_start_pos.x, end_pos.y, 0)
			bottom_right = Vector3(end_pos.x, self._drag_start_pos.y, 0)
		end

		local units = World:find_units("camera_frustum", managers.editor:camera(), top_left, bottom_right, 500000, self._slot_mask)
		self._drag_units = {}
		local r = 1
		local g = 1
		local b = 1
		local brush = Draw:brush()

		if CoreInput.alt() then
			b = 0
			g = 0
			r = 1
		end

		if CoreInput.ctrl() then
			b = 0
			g = 1
			r = 0
		end

		brush.set_color(brush, Color(0.15, r*0.5, g*0.5, b*0.5))

		for _, unit in ipairs(units) do
			if self.authorised_unit_type(self, unit) and managers.editor:select_unit_ok_conditions(unit, self) then
				table.insert(self._drag_units, unit)
				brush.draw(brush, unit)
				Application:draw(unit, r*0.75, g*0.75, b*0.75)
			end
		end
	end

	return 
end
Layer._update_draw_unit_trigger_sequences = function (self, t, dt)
	if alive(self._selected_unit) and self._selected_unit:damage() and not self._selected_unit:mission_element() then
		local trigger_name_list = self._selected_unit:damage():get_trigger_name_list()

		if trigger_name_list then
			for _, trigger_name in ipairs(trigger_name_list) do
				local trigger_data = self._selected_unit:damage():get_trigger_data_list(trigger_name)

				if trigger_data and 0 < #trigger_data then
					for _, data in ipairs(trigger_data) do
						if alive(data.notify_unit) then
							Application:draw_line(self._selected_unit:position(), data.notify_unit:position(), 0, 1, 1)
							Application:draw_sphere(data.notify_unit:position(), 50, 0, 1, 1)
							Application:draw(data.notify_unit, 0, 1, 1)
						end
					end
				end
			end
		end
	end

	return 
end
Layer.authorised_unit_type = function (self, unit)
	local name = unit.name(unit):s()

	if self._confirmed_unit_types[name] ~= nil then
		return self._confirmed_unit_types[name]
	end

	local u_type = unit.type(unit):s()

	for _, type in ipairs(self._unit_types) do
		if u_type == type then
			self._confirmed_unit_types[name] = true

			return true
		end
	end

	self._confirmed_unit_types[name] = false

	return false
end
Layer.draw_grid = function (self, t, dt)
	if not managers.editor:layer_draw_grid() then
		return 
	end

	local rot = Rotation(0, 0, 0)

	if alive(self._selected_unit) and self.local_rot(self) then
		rot = self._selected_unit:rotation()
	end

	for i = -5, 5, 1 do
		local from_x = (self._current_pos + rot.x(rot)*i*self.grid_size(self)) - rot.y(rot)*self.grid_size(self)*6
		local to_x = self._current_pos + rot.x(rot)*i*self.grid_size(self) + rot.y(rot)*self.grid_size(self)*6

		Application:draw_line(from_x, to_x, 0, 0.5, 0)

		local from_y = (self._current_pos + rot.y(rot)*i*self.grid_size(self)) - rot.x(rot)*self.grid_size(self)*6
		local to_y = self._current_pos + rot.y(rot)*i*self.grid_size(self) + rot.x(rot)*self.grid_size(self)*6

		Application:draw_line(from_y, to_y, 0, 0.5, 0)
	end

	return 
end
Layer.update_always = function (self, t, dt)
	if not self._layer_enabled then
		for _, unit in ipairs(self._created_units) do
			Application:draw(unit, 0.75, 0.75, 0.75)
		end
	end

	return 
end
Layer.local_rot = function (self)
	return managers.editor:is_coordinate_system("Local")
end
Layer.surface_move = function (self)
	return managers.editor:use_surface_move()
end
Layer.use_snappoints = function (self)
	return managers.editor:use_snappoints()
end
Layer.grid_size = function (self)
	return managers.editor:grid_size()
end
Layer.snap_rotation = function (self)
	return managers.editor:snap_rotation()
end
Layer.snap_rotation_axis = function (self)
	return managers.editor:snap_rotation_axis()
end
Layer.rotation_speed = function (self)
	return managers.editor:rotation_speed()
end
Layer.grid_altitude = function (self)
	return managers.editor:grid_altitude()
end
Layer.build_units = function (self, params)
	params = params or {}
	local style = params.style or "LC_REPORT,LC_NO_HEADER,LC_SORT_ASCENDING,LC_SINGLE_SEL"
	local unit_events = params.unit_events or {}
	local notebook_sizer = EWS:BoxSizer("VERTICAL")
	self._notebook = EWS:Notebook(self._ews_panel, "", "NB_TOP,NB_MULTILINE")

	if params and params.units_notebook_min_size then
		self._notebook:set_min_size(params.units_notebook_min_size)
	end

	notebook_sizer.add(notebook_sizer, self._notebook, 1, 0, "EXPAND")

	for _, c in ipairs(table.map_keys(self._category_map)) do
		local names = self._category_map[c]
		local panel = EWS:Panel(self._notebook, "", "TAB_TRAVERSAL")
		local units_sizer = EWS:BoxSizer("VERTICAL")

		panel.set_sizer(panel, units_sizer)

		local short_name = EWS:CheckBox(panel, "Short name", "", "ALIGN_LEFT")

		short_name.set_value(short_name, true)
		units_sizer.add(units_sizer, short_name, 0, 0, "EXPAND")
		units_sizer.add(units_sizer, EWS:StaticText(panel, "Filter", 0, ""), 0, 0, "ALIGN_CENTER_HORIZONTAL")

		local unit_filter = EWS:TextCtrl(panel, "", "", "TE_CENTRE")

		units_sizer.add(units_sizer, unit_filter, 0, 0, "EXPAND")

		local units = EWS:ListCtrl(panel, "", style)

		units.clear_all(units)
		units.append_column(units, "Name")

		for name, _ in pairs(names) do
			local i = units.append_item(units, self._stripped_unit_name(self, name))

			units.set_item_data(units, i, name)
		end

		units.autosize_column(units, 0)
		units_sizer.add(units_sizer, units, 1, 0, "EXPAND")
		short_name.connect(short_name, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "toggle_short_name"), {
			filter = unit_filter,
			units = units,
			category = c,
			short_name = short_name
		})
		units.connect(units, "EVT_COMMAND_LIST_ITEM_SELECTED", callback(self, self, "set_unit_name"), units)

		for _, event in ipairs(unit_events) do
			units.connect(units, event, callback(self, self, "set_unit_name"), units)
		end

		unit_filter.connect(unit_filter, "EVT_COMMAND_TEXT_UPDATED", callback(self, self, "update_filter"), {
			filter = unit_filter,
			units = units,
			category = c,
			short_name = short_name
		})

		local page_name = managers.editor:category_name(c)
		self._notebook_units_lists[page_name] = {
			units = units,
			filter = unit_filter
		}

		self._notebook:add_page(panel, page_name, true)
	end

	return notebook_sizer
end
Layer._stripped_unit_name = function (self, name)
	local reverse = string.reverse(name)
	local i = string.find(reverse, "/")
	name = string.reverse(string.sub(reverse, 0, i - 1))

	return name
end
Layer.repopulate_units = function (self)
	self.load_unit_map_from_vector(self)

	for c, names in pairs(self._category_map) do
		local data = self._notebook_units_lists[managers.editor:category_name(c)]

		data.units:clear()

		for name, _ in pairs(names) do
			data.units:append(name)
		end
	end

	return 
end
Layer.units_notebook = function (self)
	return self._notebook
end
Layer.notebook_unit_list = function (self, name)
	return self._notebook_units_lists[name]
end
Layer.toggle_short_name = function (self, data)
	self.update_filter(self, data)

	return 
end
Layer.update_filter = function (self, data)
	local filter = data.filter:get_value()

	data.units:delete_all_items()

	local unit_map = self._unit_map

	if data.category then
		unit_map = self._category_map[data.category]
	end

	for name, _ in pairs(unit_map) do
		local stripped_name = (data.short_name:get_value() and self._stripped_unit_name(self, name)) or name

		if string.find(stripped_name, filter, 1, true) then
			local i = data.units:append_item(stripped_name)

			data.units:set_item_data(i, name)
		end
	end

	data.units:autosize_column(0)

	return 
end
Layer.build_name_id = function (self)
	local sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, EWS:StaticText(self._ews_panel, "Name:", 0, ""), 0, 2, "ALIGN_CENTER,RIGHT")

	self._name_id = EWS:TextCtrl(self._ews_panel, "none", "", "TE_CENTRE")

	sizer.add(sizer, self._name_id, 1, 0, "EXPAND")
	self._name_id:connect("EVT_COMMAND_TEXT_UPDATED", callback(self, self, "update_name_id"), self._name_id)
	self._sizer:add(sizer, 0, 4, "EXPAND,TOP")

	return 
end
Layer.update_name_id = function (self, name_id)
	if self._block_name_id_event then
		self._block_name_id_event = false

		return 
	end

	if alive(self._selected_unit) then
		self.set_name_id(self, self._selected_unit, name_id.get_value(name_id))
	end

	return 
end
Layer.cb_toogle = function (self, data)
	self[data.value] = data.cb:get_value()

	return 
end
Layer.cb_toogle_trg = function (self, data)
	data.cb:set_value(not data.cb:get_value())

	self[data.value] = data.cb:get_value()

	return 
end
Layer.change_combo_box = function (self, data)
	self[data.value] = tonumber(data.combobox:get_value())

	return 
end
Layer.change_combo_box_trg = function (self, data)
	local next_i = nil

	for i = 1, #self[data.t], 1 do
		if self[data.value] == self[data.t][i] then
			if self.ctrl(self) then
				if i == 1 then
					next_i = #self[data.t]
				else
					next_i = 1
				end
			elseif self.shift(self) then
				if i == 1 then
					next_i = #self[data.t]
				else
					next_i = i - 1
				end
			elseif i == #self[data.t] then
				next_i = 1
			else
				next_i = i + 1
			end
		end
	end

	data.combobox:set_value(self[data.t][next_i])

	self[data.value] = tonumber(data.combobox:get_value())

	return 
end
Layer.use_move_widget = function (self, value)
	if self._will_use_widgets then
		self._move_widget:set_use(value)
		self._move_widget:set_enabled(alive(self.widget_affect_object(self)))
		self._rotate_widget:set_enabled(alive(self.widget_affect_object(self)))
	end

	return 
end
Layer.use_rotate_widget = function (self, value)
	if self._will_use_widgets then
		self._rotate_widget:set_use(value)
		self._move_widget:set_enabled(alive(self.widget_affect_object(self)))
		self._rotate_widget:set_enabled(alive(self.widget_affect_object(self)))
	end

	return 
end
Layer.unit_types = function (self)
	return self._unit_types
end
Layer.load_unit_map_from_vector = function (self, which)
	self._unit_types = which or self._unit_types
	self._unit_map = {}
	self._category_map = {}

	for _, t in pairs(self._unit_types) do
		self._category_map[t] = {}

		for _, unit_name in ipairs(managers.database:list_units_of_type(t)) do
			local unit_data = CoreEngineAccess._editor_unit_data(unit_name.id(unit_name))
			self._unit_map[unit_name] = unit_data
			self._category_map[t][unit_name] = unit_data
		end
	end

	return 
end
Layer.set_unit_map = function (self, map)
	self._unit_map = map

	return 
end
Layer.get_unit_map = function (self)
	return self._unit_map
end
Layer.category_map = function (self)
	return self._category_map
end
Layer.get_layer_name = function (self)
	return nil
end
Layer.cancel_all = function (self, ctrlr, event)
	event.skip(event)

	if EWS:name_to_key_code("K_ESCAPE") == event.key_code(event) then
		self.ews_replace_unit(self)
	end

	return 
end
Layer.deselect = function (self)
	if not self.condition(self) then
		self.set_select_unit(self, nil)
		self.update_unit_settings(self)
	end

	return 
end
Layer.force_editor_state = function (self)
	self._owner:force_editor_state()

	return 
end
Layer.update_unit_settings = function (self)
	managers.editor:unit_output(self._selected_unit)
	managers.editor:has_editables(self._selected_unit, self._selected_units)
	self._move_widget:set_enabled(alive(self.widget_affect_object(self)))
	self._rotate_widget:set_enabled(alive(self.widget_affect_object(self)))

	if self._name_id then
		self._block_name_id_event = true

		if alive(self._selected_unit) then
			self._name_id:set_value(self._selected_unit:unit_data().name_id)
			self._name_id:set_enabled(true)
		else
			self._name_id:set_enabled(false)
			self._name_id:set_value("-")
		end
	end

	self.set_reference_unit(self, self._selected_unit)

	return 
end
Layer.activate = function (self)
	self.update_unit_settings(self)

	if alive(self._selected_unit) then
		managers.editor:set_grid_altitude(self._selected_unit:position().z)
	end

	self.use_move_widget(self, managers.editor:using_move_widget())
	self.use_rotate_widget(self, managers.editor:using_rotate_widget())
	self.recalc_all_locals(self)

	return 
end
Layer.deactivate = function (self)
	self._drag_units = nil

	self.select_release(self)
	self._move_widget:set_enabled(false)

	return 
end
Layer.build_panel = function (self)
	return nil
end
Layer.widget_rot = function (self)
	local widget_rot = Rotation()

	if self.local_rot(self) then
		widget_rot = self.widget_affect_object(self):rotation()
	end

	return widget_rot
end
Layer.click_widget = function (self)
	if not self.widget_affect_object(self) or not alive(self.widget_affect_object(self)) then
		return 
	end

	local from = managers.editor:get_cursor_look_point(0)
	local to = managers.editor:get_cursor_look_point(100000)

	if self._move_widget:enabled() then
		local ray = World:raycast("ray", from, to, "ray_type", "widget", "target_unit", self._move_widget:widget())

		if ray and ray.body then
			if self.shift(self) then
				self.clone(self)
			end

			self._move_widget:add_move_widget_axis(ray.body:name():s())

			self._grab = true
			self._grab_info = CoreEditorUtils.GrabInfo:new(self.widget_affect_object(self))
			self._using_widget = true

			self._move_widget:set_move_widget_offset(self.widget_affect_object(self), self.widget_rot(self))
		end
	end

	if self._rotate_widget:enabled() then
		local ray = World:raycast("ray", from, to, "ray_type", "widget", "target_unit", self._rotate_widget:widget())

		if ray and ray.body then
			if self.shift(self) then
				self.clone(self)
			end

			self._rotate_widget:set_rotate_widget_axis(ray.body:name():s())
			managers.editor:set_value_info_visibility(true)

			self._grab = true
			self._grab_info = CoreEditorUtils.GrabInfo:new(self.widget_affect_object(self))
			self._using_widget = true

			self._rotate_widget:set_world_dir(ray.position)
			self._rotate_widget:set_rotate_widget_start_screen_position(managers.editor:world_to_screen(ray.position):with_z(0))
			self._rotate_widget:set_rotate_widget_unit_rot(self.widget_affect_object(self):rotation())
		end
	end

	return 
end
Layer.release_widget = function (self)
	if self._using_widget then
		self.cloned_group(self)

		self._grab = false

		if self._selected_unit then
			managers.editor:set_grid_altitude(self._selected_unit:position().z)
		end

		self.reset_widget_values(self)
	end

	return 
end
Layer.cloned_group = function (self)
	if self._clone_create_group then
		self._clone_create_group = false

		managers.editor:group()
	end

	return 
end
Layer.using_widget = function (self)
	return self._using_widget
end
Layer.reset_widget_values = function (self)
	self._using_widget = false

	self._move_widget:reset_values()
	self._rotate_widget:reset_values()
	managers.editor:set_value_info_visibility(false)

	return 
end
Layer.prepare_replace = function (self, names, rules)
	rules = rules or {}
	local data = {}
	local units = {}

	for _, name in ipairs(names) do
		local slot = CoreEngineAccess._editor_unit_data(name.id(name)):slot()

		for _, unit in ipairs(World:find_units_quick("disabled", "all", slot)) do
			if unit.name(unit) == name.id(name) and managers.editor:unit_in_layer(unit) == self then
				local continent = unit.unit_data(unit).continent

				if not rules.only_current_continent or not continent or managers.editor:current_continent() == continent then
					local unit_params = {
						name = unit.name(unit),
						continent = continent,
						position = unit.position(unit),
						rotation = unit.rotation(unit),
						groups = unit.unit_data(unit).editor_groups
					}

					if unit == self._selected_unit then
						unit_params.reference_unit = true
					elseif table.contains(self._selected_units, unit) then
						unit_params.selected = true
					end

					table.insert(data, unit_params)
					table.insert(units, unit)
				end
			end
		end
	end

	for _, unit in ipairs(units) do
		self.delete_unit(self, unit)
	end

	return data
end
Layer.recreate_units = function (self, name, data)
	local units_to_select = {}
	local reference_unit = nil
	self._continent_locked_picked = true

	for _, params in ipairs(data) do
		local unit_name = name or params.name:id()
		local continent = params.continent
		local pos = params.position
		local rot = params.rotation
		local new_unit = self.do_spawn_unit(self, unit_name, pos, rot)

		if continent and new_unit.unit_data(new_unit).continent ~= continent then
			managers.editor:change_continent_for_unit(new_unit, continent)
		end

		if params.groups then
			for _, group in ipairs(params.groups) do
				group.add_unit(group, new_unit)
			end
		end

		if params.reference_unit then
			reference_unit = new_unit
		elseif params.selected then
			table.insert(units_to_select, new_unit)
		end
	end

	self._continent_locked_picked = false

	self.set_select_unit(self, nil)

	self._replacing_units = true

	self.set_select_unit(self, reference_unit)

	for _, unit in ipairs(units_to_select) do
		self.set_select_unit(self, unit)
	end

	self._replacing_units = false

	return 
end
Layer.replace_unit = function (self, name, all)
	local replace_units = {}

	if all then
		for _, unit in ipairs(World:find_units_quick("all", self._selected_unit:slot())) do
			if unit.name(unit) == self._selected_unit:name() and unit ~= self._selected_unit then
				table.insert(replace_units, unit)
			end
		end
	else
		for _, unit in ipairs(self._selected_units) do
			if unit ~= self._selected_unit then
				table.insert(replace_units, unit)
			end
		end
	end

	table.insert(replace_units, self._selected_unit)
	self._replace_units(self, name, replace_units)

	return 
end
Layer._replace_units = function (self, name, replace_units)
	local selected_units = CoreTable.clone(self._selected_units)
	local reference_unit = self._selected_unit
	local units_to_select = {}

	for _, unit in ipairs(replace_units) do
		local continent = unit.unit_data(unit).continent

		if not continent or managers.editor:current_continent() == continent then
			local pos = unit.position(unit)
			local rot = unit.rotation(unit)
			local unit_name = name or unit.name(unit):s()
			local new_unit = self.do_spawn_unit(self, unit_name, pos, rot)

			if continent and new_unit.unit_data(new_unit).continent ~= continent then
				managers.editor:change_continent_for_unit(new_unit, continent)
			end

			if unit.unit_data(unit).editor_groups then
				for _, group in ipairs(unit.unit_data(unit).editor_groups) do
					group.add_unit(group, new_unit)
				end
			end

			if unit == reference_unit then
				reference_unit = new_unit
			elseif table.contains(selected_units, unit) then
				table.insert(units_to_select, new_unit)
			end

			self.delete_unit(self, unit)
		end
	end

	self.set_select_unit(self, nil)

	self._replacing_units = true

	self.set_select_unit(self, reference_unit)

	for _, unit in ipairs(units_to_select) do
		self.set_select_unit(self, unit)
	end

	self._replacing_units = false

	return 
end
Layer.use_grab_info = function (self)
	self.reset_widget_values(self)

	return 
end
Layer.unit_sampler = function (self)
	if not self._grab and not self.condition(self) then
		local data = {
			ray_type = "body editor",
			sample = true,
			mask = managers.slot:get_mask("editor_all")
		}
		local ray = managers.editor:unit_by_raycast(data)

		if ray and ray.unit then
			local unit_name = ray.unit:name()
			local s = managers.editor:select_unit_name(unit_name)

			managers.editor:output(s)
		end
	end

	return 
end
Layer.ignore_global_select = function (self)
	return self._ignore_global_select
end
Layer.select_unit_authorised = function (self, unit)
	return true
end
Layer.click_select_unit = function (self)
	self.set_drag_select(self)
	managers.editor:click_select_unit(self)

	return 
end
Layer.set_drag_select = function (self)
	if self.condition(self) or self._grab then
		return 
	end

	self._drag_select = true
	self._polyline = managers.editor._gui:polyline({
		color = Color(0.5, 1, 1, 1)
	})

	self._polyline:set_closed(true)

	self._drag_start_pos = managers.editor:cursor_pos()

	return 
end
Layer.remove_polyline = function (self)
	if self._polyline then
		managers.editor._gui:remove(self._polyline)

		self._polyline = nil
	end

	return 
end
Layer.adding_units = function (self)
	return CoreInput.ctrl()
end
Layer.removing_units = function (self)
	return CoreInput.alt()
end
Layer.adding_or_removing_units = function (self)
	return CoreInput.ctrl() or CoreInput.alt()
end
Layer.select_release = function (self)
	self._drag_select = false

	self.remove_polyline(self)

	if self._drag_units then
		self._selecting_many_units = true

		for _, unit in ipairs(self._drag_units) do
			self.set_select_unit(self, unit)
		end

		self._selecting_many_units = false

		self.check_referens_exists(self)
		managers.editor:selected_units(self._selected_units)
		self.update_unit_settings(self)
	end

	self._drag_units = nil

	return 
end
Layer.add_highlighted_unit = function (self, unit, config)
	if not unit then
		return 
	end

	return 
end
Layer.remove_highlighted_unit = function (self, unit)
	return 
end
Layer.clear_highlighted_units = function (self)
	for _, unit in ipairs(self._selected_units) do
		self.remove_highlighted_unit(self, unit)
	end

	return 
end
Layer.clear_selected_units_table = function (self)
	self.clear_highlighted_units(self)

	self._selected_units = {}

	return 
end
Layer.clear_selected_units = function (self)
	self.clear_selected_units_table(self)
	self.set_reference_unit(self, nil)
	self.update_unit_settings(self)

	return 
end
Layer.set_selected_units = function (self, units)
	self.clear_selected_units(self)

	self._selecting_many_units = true
	local id = Profiler:start("call_set_select_unit")

	for _, unit in ipairs(units) do
		self.set_select_unit(self, unit)
	end

	Profiler:stop(id)
	Profiler:counter_time("call_set_select_unit")
	managers.editor:selected_units(self._selected_units)
	self.update_unit_settings(self)

	self._selecting_many_units = false

	return 
end
Layer.select_group = function (self, group)
	self.clear_highlighted_units(self)
	self.set_reference_unit(self, group.reference(group))

	self._selected_units = CoreTable.clone(group.units(group))

	for _, unit in ipairs(self._selected_units) do
		self.add_highlighted_unit(self, unit, "highlight")
	end

	managers.editor:group_selected(group)
	self.recalc_all_locals(self)
	self.update_unit_settings(self)

	return 
end
Layer.current_group = function (self)
	return self.unit_in_group(self, self._selected_unit)
end
Layer.unit_in_group = function (self, unit)
	if alive(unit) then
		local groups = unit.unit_data(unit).editor_groups

		if groups then
			for i = #groups, 1, -1 do
				local group = groups[i]

				if group and group.closed(group) and group.continent(group) == managers.editor:current_continent() then
					return group
				end
			end
		end
	end

	return nil
end
Layer.set_select_group = function (self, unit)
	if managers.editor:using_groups() and not self._clone_create_group then
		local group = self.unit_in_group(self, unit)
		local current_group = self.current_group(self)

		if group then
			local reference = group.reference(group)

			if CoreInput.alt() then
				if current_group and current_group == group then
					current_group.remove_unit(current_group, unit)
					self.remove_select_unit(self, unit)
				end
			elseif CoreInput.shift() then
				group.set_reference(group, unit)
			elseif CoreInput.ctrl() then
				if current_group then
					if self.current_group(self) == group then
						current_group.remove_unit(current_group, unit)
						self.remove_select_unit(self, unit)
					else
						current_group.add_unit(current_group, unit)
						self.add_select_unit(self, unit)
					end
				end
			else
				self.select_group(self, group)
			end

			if reference ~= group.reference(group) then
				self.select_group(self, group)
			end
		elseif CoreInput.ctrl() then
			if current_group then
				current_group.add_unit(current_group, unit)
				self.add_select_unit(self, unit)
			end
		elseif not self._selecting_many_units then
			self.clear_selected_units(self)
		end

		return true
	end

	return false
end
Layer.set_select_unit = function (self, unit)
	if managers.editor:loading() then
		return 
	end

	if self.set_select_group(self, unit) then
		return 
	end

	if self.alt(self) then
		self.remove_select_unit(self, unit)
	elseif self.shift(self) then
		if table.contains(self._selected_units, unit) then
			self.set_reference_unit(self, unit)
			self.recalc_all_locals(self)
		else
			self.set_reference_unit(self, self._selected_unit or unit)
			self.add_select_unit(self, unit)
		end
	elseif (self.ctrl(self) or self._selecting_many_units or self._replacing_units) and 0 < #self._selected_units then
		self.add_select_unit(self, unit)
	else
		self.clear_selected_units_table(self)
		self.set_reference_unit(self, unit)
		self.add_highlighted_unit(self, unit, "highlight")
		table.insert(self._selected_units, unit)
	end

	if not self._selecting_many_units then
		self.check_referens_exists(self)
	end

	if not self._selecting_many_units then
		managers.editor:on_selected_unit(unit)
		managers.editor:selected_units(self._selected_units)
		self.update_unit_settings(self)
	end

	return 
end
Layer.add_select_unit = function (self, unit)
	if unit then
		if not table.contains(self._selected_units, unit) then
			table.insert(self._selected_units, unit)
			self.add_highlighted_unit(self, unit, "highlight")

			if self._selected_unit then
				self.recalc_locals(self, unit, self._selected_unit)
			end
		elseif not self._selecting_many_units then
			table.delete(self._selected_units, unit)
			self.remove_highlighted_unit(self, unit)
		end
	end

	return 
end
Layer.remove_select_unit = function (self, unit)
	if table.contains(self._selected_units, unit) then
		table.delete(self._selected_units, unit)
		self.remove_highlighted_unit(self, unit)
	end

	return 
end
Layer.check_referens_exists = function (self)
	if 0 < #self._selected_units then
		if not table.contains(self._selected_units, self._selected_unit) then
			self.set_reference_unit(self, self._selected_units[1])
			self.recalc_all_locals(self)
		end
	else
		self.set_reference_unit(self, nil)
	end

	return 
end
Layer.set_reference_unit = function (self, unit)
	if alive(self._selected_unit) and (not alive(unit) or unit ~= self._selected_unit) then
		self._on_reference_unit_unselected(self, self._selected_unit)
	end

	self._selected_unit = unit

	managers.editor:on_reference_unit(self._selected_unit)

	return 
end
Layer._on_reference_unit_unselected = function (self, unit)
	return 
end
Layer.recalc_all_locals = function (self)
	if 0 < #self._selected_units and not table.contains(self._selected_units, self._selected_unit) then
		self.set_reference_unit(self, self._selected_units[1])
	end

	if alive(self._selected_unit) then
		local reference = self._selected_unit
		reference.unit_data(reference).local_pos = Vector3(0, 0, 0)
		reference.unit_data(reference).local_rot = Rotation(0, 0, 0)

		for _, unit in ipairs(self._selected_units) do
			if unit ~= reference then
				self.recalc_locals(self, unit, reference)
			end
		end
	end

	return 
end
Layer.recalc_locals = function (self, unit, reference)
	local pos = reference.position(reference)
	local rot = reference.rotation(reference)
	unit.unit_data(unit).local_pos = unit.unit_data(unit).world_pos - pos:rotate_with(rot.inverse(rot))
	unit.unit_data(unit).local_rot = rot.inverse(rot)*unit.rotation(unit)

	return 
end
Layer.selected_unit = function (self)
	return self._selected_unit
end
Layer.verify_selected_unit = function (self)
	return alive(self._selected_unit)
end
Layer.verify_selected_units = function (self)
	for i = #self._selected_units, 1, -1 do
		if not alive(self._selected_units[i]) then
			table.remove(self._selected_units, i)
		end
	end

	local i = 1

	while not alive(self._selected_unit) and i < #self._selected_units do
		self._selected_unit = self._selected_units[i]
		i = i + 1
	end

	return alive(self._selected_unit)
end
Layer.create_unit = function (self, name, pos, rot, to_continent_name, prefered_id)
	local unit = CoreUnit.safe_spawn_unit(name, pos, rot)

	if self.uses_continents(self) then
		local continent = (to_continent_name and managers.editor:continent(to_continent_name)) or managers.editor:current_continent()

		if continent then
			continent.add_unit(continent, unit)
		end
	end

	unit.unit_data(unit).world_pos = pos
	unit.unit_data(unit).unit_id = self._owner:get_unit_id(unit, prefered_id)
	unit.unit_data(unit).name_id = self.get_name_id(self, unit)

	managers.editor:spawned_unit(unit)
	self._on_unit_created(self, unit)

	return unit
end
Layer.do_spawn_unit = function (self, name, pos, rot, to_continent_name, prevent_undo, prefered_id)
	local continent = (to_continent_name and managers.editor:continent(to_continent_name)) or managers.editor:current_continent()

	if continent.value(continent, "locked") and not self._continent_locked_picked then
		managers.editor:output_warning("Can't create units in continent " .. managers.editor:current_continent():name() .. " because it is locked!")

		return 
	end

	if name.s(name) ~= "" then
		if prevent_undo == nil and managers.editor:undo_debug() then
			Application:stack_dump()
			print("[Undo] WARNING: Called do_spawn_unit without setting 'prevent_undo'! This can create unit delete-spawn recursion, so fix it!")
		end

		pos = pos or self.current_pos(self)
		rot = rot or Rotation(Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1))
		local command = CoreEditorCommand.SpawnUnitCommand:new(self)
		local unit = command.execute(command, name, pos, rot, to_continent_name, prefered_id)

		if not prevent_undo then
			managers.editor:register_undo_command(command)
		end

		return unit
	end

	return 
end
Layer.remove_unit = function (self, unit)
	table.delete(self._selected_units, unit)
	self.remove_highlighted_unit(self, unit)
	self.remove_name_id(self, unit)
	managers.editor:remove_unit_id(unit)
	managers.editor:deleted_unit(unit)
	managers.portal:delete_unit(unit)

	if unit.unit_data(unit).continent then
		unit.unit_data(unit).continent:remove_unit(unit)
	end

	World:delete_unit(unit)

	return 
end
Layer.delete_unit = function (self, unit, prevent_undo)
	local command = CoreEditorCommand.DeleteStaticUnitCommand:new(self)

	command.execute(command, unit)

	if not prevent_undo then
		managers.editor:register_undo_command(command)
	end

	return 
end
Layer._on_unit_created = function (self, unit)
	return 
end
Layer.show_replace_units = function (self)
	if self.ctrl(self) or self.shift(self) or self.alt(self) then
		return 
	end

	if self._selected_unit then
		managers.editor:show_layer_replace_dialog(self)
	end

	return 
end
Layer.get_created_unit_by_pattern = function (self, patterns)
	local units = {}

	for _, unit in ipairs(self._created_units) do
		for _, pattern in ipairs(patterns) do
			if string.find(unit.name(unit):s(), pattern, 1, true) then
				table.insert(units, unit)
			end
		end
	end

	return units
end
Layer.add_triggers = function (self)
	local vc = self._editor_data.virtual_controller

	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "click_widget"))
	vc.add_release_trigger(vc, Idstring("lmb"), callback(self, self, "release_widget"))
	vc.add_trigger(vc, Idstring("deselect"), callback(self, self, "deselect"))
	vc.add_trigger(vc, Idstring("unit_sampler"), callback(self, self, "unit_sampler"))
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "click_select_unit"))
	vc.add_release_trigger(vc, Idstring("lmb"), callback(self, self, "select_release"))
	vc.add_trigger(vc, Idstring("clone_edited_values"), callback(self, self, "on_clone_edited_values"))
	vc.add_trigger(vc, Idstring("center_view_on_selected_unit"), callback(self, self, "on_center_view_on_selected_unit"))

	return 
end
Layer.clear_triggers = function (self)
	self._editor_data.virtual_controller:clear_triggers()

	return 
end
Layer.get_help = function (self, text)
	return text .. "No help Available"
end
Layer.clone = function (self)
	cat_debug("editor", "No clone implemented in current layer")

	return 
end
Layer._cloning_done = function (self)
	return 
end
Layer.on_center_view_on_selected_unit = function (self)
	managers.editor:center_view_on_unit(self.selected_unit(self))

	return 
end
Layer.on_clone_edited_values = function (self)
	if self.ctrl(self) then
		return 
	end

	if self._selected_unit then
		local ray = self._owner:select_unit_by_raycast(managers.slot:get_mask("editor_all"), "body editor")

		if ray and ray.unit then
			if self.shift(self) then
				self.clone_edited_values(self, self._selected_unit, ray.unit)
			else
				self.clone_edited_values(self, ray.unit, self._selected_unit)
			end

			self.update_unit_settings(self)
		end
	end

	return 
end
Layer.clone_edited_values = function (self, unit, source)
	if unit.name(unit) ~= source.name(source) then
		return 
	end

	local lights = CoreEditorUtils.get_editable_lights(source) or {}

	for _, light in ipairs(lights) do
		local new_light = unit.get_object(unit, light.name(light))

		new_light.set_near_range(new_light, light.near_range(light))
		new_light.set_far_range(new_light, light.far_range(light))
		new_light.set_enable(new_light, light.enable(light))
		new_light.set_color(new_light, light.color(light))
		new_light.set_multiplier(new_light, light.multiplier(light))
		new_light.set_falloff_exponent(new_light, light.falloff_exponent(light))
		new_light.set_spot_angle_start(new_light, light.spot_angle_start(light))
		new_light.set_spot_angle_end(new_light, light.spot_angle_end(light))
		new_light.set_clipping_values(new_light, light.clipping_values(light))

		local projection_texture = source.unit_data(source).projection_textures and source.unit_data(source).projection_textures[light.name(light):s()]

		if projection_texture then
			local is_projection = CoreEditorUtils.is_projection_light(source, light, "projection")
			local is_spot = (not string.match(light.properties(light), "omni") or false) and true

			if is_projection and is_spot then
				new_light.set_projection_texture(new_light, Idstring(projection_texture), false, false)

				unit.unit_data(unit).projection_textures = unit.unit_data(unit).projection_textures or {}
				unit.unit_data(unit).projection_textures[light.name(light):s()] = projection_texture
			end
		end
	end

	unit.unit_data(unit).mesh_variation = source.unit_data(source).mesh_variation

	if unit.unit_data(unit).mesh_variation and unit.unit_data(unit).mesh_variation ~= "default" then
		managers.sequence:run_sequence_simple2(unit.unit_data(unit).mesh_variation, "change_state", unit)
	end

	unit.unit_data(unit).material = source.unit_data(source).material

	if unit.unit_data(unit).material and unit.unit_data(unit).material ~= "default" then
		unit.set_material_config(unit, unit.unit_data(unit).material, true)
	end

	unit.unit_data(unit).disable_shadows = source.unit_data(source).disable_shadows

	unit.set_shadows_disabled(unit, unit.unit_data(unit).disable_shadows)

	unit.unit_data(unit).disable_collision = source.unit_data(source).disable_collision
	local collision_enabled = not unit.unit_data(unit).disable_collision

	for index = 0, unit.num_bodies(unit) - 1, 1 do
		local body = unit.body(unit, index)

		if body then
			body.set_collisions_enabled(body, collision_enabled)
			body.set_collides_with_mover(body, collision_enabled)
		end
	end

	unit.unit_data(unit).delayed_load = source.unit_data(source).delayed_load
	unit.unit_data(unit).hide_on_projection_light = source.unit_data(source).hide_on_projection_light
	unit.unit_data(unit).disable_on_ai_graph = source.unit_data(source).disable_on_ai_graph

	if unit.editable_gui(unit) then
		unit.editable_gui(unit):set_text(source.editable_gui(source):text())
		unit.editable_gui(unit):set_font_size(source.editable_gui(source):font_size())
		unit.editable_gui(unit):set_font_color(source.editable_gui(source):font_color())
		unit.editable_gui(unit):set_font(source.editable_gui(source):font())
		unit.editable_gui(unit):set_align(source.editable_gui(source):align())
		unit.editable_gui(unit):set_vertical(source.editable_gui(source):vertical())
		unit.editable_gui(unit):set_blend_mode(source.editable_gui(source):blend_mode())
		unit.editable_gui(unit):set_render_template(source.editable_gui(source):render_template())
		unit.editable_gui(unit):set_wrap(source.editable_gui(source):wrap())
		unit.editable_gui(unit):set_word_wrap(source.editable_gui(source):word_wrap())
		unit.editable_gui(unit):set_alpha(source.editable_gui(source):alpha())
		unit.editable_gui(unit):set_shape(source.editable_gui(source):shape())
	end

	if unit.ladder(unit) then
		unit.ladder(unit):set_width(source.ladder(source):width())
		unit.ladder(unit):set_height(source.ladder(source):height())
	end

	return 
end
Layer._continent_locked = function (self, unit)
	local continent = unit.unit_data(unit).continent

	if not continent then
		return false
	end

	return unit.unit_data(unit).continent:value("locked")
end
Layer.set_enabled = function (self, enabled)
	self._layer_enabled = enabled

	for _, unit in ipairs(self._created_units) do
		if not self._continent_locked(self, unit) then
			unit.set_enabled(unit, enabled)
		end
	end

	return true
end
Layer.hide_all = function (self)
	self._hide_units(self, self._created_units, true)

	return 
end
Layer.unhide_all = function (self)
	self._hide_units(self, self._created_units, false)

	return 
end
Layer.on_hide_selected = function (self)
	self._hide_units(self, _G.clone(self.selected_units(self)), true)

	return 
end
Layer._hide_units = function (self, units, hide)
	local hide_command = CoreEditorCommand.HideUnitsCommand:new(self)

	hide_command.execute(hide_command, units, hide)
	managers.editor:register_undo_command(hide_command)

	return 
end
Layer.clear = function (self)
	for _, unit in ipairs(self._created_units) do
		if alive(unit) then
			self.remove_unit(self, unit)
		end
	end

	self._move_widget:set_enabled(false)
	self.set_reference_unit(self, nil)
	self.clear_selected_units_table(self)

	self._created_units = {}
	self._created_units_pairs = {}
	self._name_ids = {}

	if self._name_id then
		self._name_id:set_value("-")
		self._name_id:set_enabled(false)
	end

	return 
end
Layer.set_unit_name = function (self, units)
	local i = units.selected_item(units)

	if i ~= -1 then
		local name = self.get_real_name(self, units.get_item_data(units, i))

		if not CoreEngineAccess._editor_unit_data(name.id(name)):model_script_data() then
			managers.editor:output("Unit " .. name .. " doesnt have a model or diesel file.")
			units.deselect_index(units, i)

			self._unit_name = ""

			return 
		end

		self._unit_name = name
	end

	return 
end
Layer.get_real_name = function (self, name)
	local fs = " %*"

	if string.find(name, fs) then
		local e = string.find(name, fs)
		name = string.sub(name, 1, e - 1)
	end

	return name
end
Layer.condition = function (self)
	return managers.editor:conditions() or self._using_widget
end
Layer.grab = function (self)
	return self._grab
end
Layer.create_marker = function (self)
	return 
end
Layer.use_marker = function (self)
	return 
end
Layer.on_continent_changed = function (self)
	return 
end
Layer.set_unit_rotations = function (self, rot, finalize)
	return 
end
Layer.set_unit_positions = function (self, pos, finalize)
	return 
end
Layer._add_project_save_data = function (self, data)
	return 
end
Layer._add_project_unit_save_data = function (self, unit, data)
	return 
end
Layer.selected_amount_string = function (self)
	return "Selected " .. self._save_name .. ": " .. #self._selected_units
end
local idstring_wpn = Idstring("wpn")
Layer.save = function (self)
	for _, unit in ipairs(self._created_units) do
		local unit_data = unit.unit_data(unit)

		if not unit_data.instance then
			local t = {
				entry = self._save_name,
				continent = unit_data.continent and unit_data.continent:name(),
				data = {
					unit_data = CoreEditorSave.save_data_table(unit)
				}
			}

			self._add_project_unit_save_data(self, unit, t.data)
			managers.editor:add_save_data(t)

			if unit.type(unit) ~= idstring_wpn then
				managers.editor:add_to_world_package({
					category = "units",
					name = unit.name(unit):s(),
					continent = unit_data.continent
				})
			end
		end
	end

	return 
end
Layer.test_spawn = function (self, type)
	local pos = Vector3()
	local rot = Rotation()
	local i = 0
	local prow = 40
	local y_pos = 0
	local c_rad = 0
	local row_units = {}
	local max_rad = 0
	local removed = {}

	for name, unit_data in pairs(self._unit_map) do
		if not type or unit_data.type(unit_data) == Idstring(type) then
			print("Spawning:", name)

			local unit = self.do_spawn_unit(self, name, pos, rot)
			local bsr = unit.bounding_sphere_radius(unit)*2

			if 20000 < bsr then
				table.insert(removed, name)
				print("  Removing:", name)
				self.remove_unit(self, unit)
			else
				max_rad = math.max(max_rad, bsr)
				i = i + 1

				self.set_unit_position(self, unit, unit.position(unit) + Vector3(bsr/2, y_pos, 0), Rotation())

				pos = pos + Vector3(bsr, 0, 0)

				table.insert(row_units, unit)
			end

			if math.mod(i, prow) == 0 then
				c_rad = max_rad*1

				for slot21, slot22 in ipairs(row_units) do
				end

				max_rad = 0
				y_pos = y_pos + c_rad
				pos = Vector3()
				row_units = {}
			end
		end
	end

	print("\n")

	for _, name in ipairs(removed) do
		print("Removed", name)
	end

	print("DONE")

	return 
end
Layer.shift = function (self)
	return CoreInput.shift()
end
Layer.ctrl = function (self)
	return CoreInput.ctrl()
end
Layer.alt = function (self)
	return CoreInput.alt()
end

return 
