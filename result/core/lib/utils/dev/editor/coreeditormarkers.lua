CoreEditor.build_marker_panel = function (self)
	self._marker_panel = EWS:Panel(self._ews_editor_frame, "Markers", "TAB_TRAVERSAL")
	local marker_sizer = EWS:BoxSizer("VERTICAL")

	self._marker_panel:set_sizer(marker_sizer)

	local btn_sizer = EWS:BoxSizer("HORIZONTAL")
	local create_marker_btn = EWS:Button(self._marker_panel, "Create Marker", "_create_marker", "BU_EXACTFIT,NO_BORDER")

	create_marker_btn.connect(create_marker_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_make_marker"), nil)
	btn_sizer.add(btn_sizer, create_marker_btn, 0, 2, "RIGHT")

	local use_marker_btn = EWS:Button(self._marker_panel, "Use Marker", "_use_marker", "BU_EXACTFIT,NO_BORDER")

	use_marker_btn.connect(use_marker_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_use_marker"), nil)
	btn_sizer.add(btn_sizer, use_marker_btn, 0, 2, "RIGHT")

	local delete_marker_btn = EWS:Button(self._marker_panel, "Del. Marker", "_delete_marker", "BU_EXACTFIT,NO_BORDER")

	delete_marker_btn.connect(delete_marker_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_delete_marker"), nil)
	btn_sizer.add(btn_sizer, delete_marker_btn, 0, 2, "RIGHT")
	marker_sizer.add(marker_sizer, btn_sizer, 0, 0, "EXPAND")

	local marker_list_sizer = EWS:BoxSizer("VERTICAL")
	self._ews_markers = EWS:ListBox(self._marker_panel, "_markers", "LB_SINGLE,LB_HSCROLL,LB_NEEDED_SB,LB_SORT")

	marker_list_sizer.add(marker_list_sizer, self._ews_markers, 0, 0, "EXPAND")
	marker_sizer.add(marker_sizer, marker_list_sizer, 0, 0, "EXPAND")

	return self._marker_panel
end
CoreEditor.marker_name = function (self)
	local i = 1

	while self._markers["marker" .. i] do
		i = i + 1
	end

	return "marker" .. i
end
CoreEditor.on_make_marker = function (self)
	local name = EWS:get_text_from_user(Global.frame_panel, "Enter name for the marker:", "Create Marker", self.marker_name(self), Vector3(-1, -1, 0), true)

	if name and name ~= "" then
		if self._markers[name] then
			self.on_make_marker(self)
		else
			self.make_marker(self, name)
		end
	end

	return 
end
CoreEditor.make_marker = function (self, name)
	if self._current_layer then
		local m = Marker:new(name)

		if self._current_layer:create_marker(m) then
			self._ews_markers:append(name)

			self._markers[name] = m
		end
	end

	return 
end
CoreEditor.on_use_marker = function (self)
	local s = self.get_marker_string(self)

	if s then
		self.use_marker(self, s)
	end

	return 
end
CoreEditor.use_marker = function (self, name)
	if self._current_layer and self._markers[name] then
		self._current_layer:use_marker(self._markers[name])
	end

	return 
end
CoreEditor.on_delete_marker = function (self)
	local s = self.get_marker_string(self)

	if s then
		self.delete_marker(self, s)
	end

	return 
end
CoreEditor.delete_marker = function (self, name)
	if self._markers[name] then
		self._markers[name] = nil
	end

	self.remove_marker_from_list(self, name)

	return 
end
CoreEditor.get_marker_string = function (self)
	if 0 < self._ews_markers:nr_items() then
		local i = self._ews_markers:selected_index()

		if 0 <= i then
			return self._ews_markers:get_string(i)
		end
	end

	return nil
end
CoreEditor.remove_marker_from_list = function (self, s)
	local i = 0
	local size = self._ews_markers:nr_items()

	while i < size do
		if s == self._ews_markers:get_string(i) then
			self._ews_markers:remove(i)

			break
		end

		i = i + 1
	end

	return 
end
CoreEditor.create_marker = function (self, name, pos, rot)
	self._markers[name] = Marker:new(name, pos, rot)

	return 
end
CoreEditor.get_marker = function (self, name)
	if self._markers[name] then
		return self._markers[name]
	end

	return nil
end
CoreEditor.clear_markers = function (self)
	self._ews_markers:clear()

	self._markers = {}

	return 
end
Marker = Marker or class()
Marker.init = function (self, name, pos, rot)
	self._name = name
	self._pos = pos
	self._rot = rot

	return 
end
Marker.set_pos = function (self, pos)
	self._pos = pos

	return 
end
Marker.set_rot = function (self, rot)
	self._rot = rot

	return 
end
Marker.draw = function (self)
	local l = 2000

	Application:draw_line(self._pos, self._pos + self._rot:x()*l, 0.5, 0, 0)
	Application:draw_line(self._pos, self._pos + self._rot:y()*l, 0, 0.5, 0)
	Application:draw_line(self._pos, self._pos + self._rot:z()*l, 0, 0, 0.5)

	return 
end
Marker.save = function (self, file, t)
	t = t .. "\t"

	file.puts(file, t .. "<marker name=\"" .. self._name .. "\" pos=\"" .. self._pos.x .. " " .. self._pos.y .. " " .. self._pos.z .. "\" rot=\"" .. self._rot:yaw() .. " " .. self._rot:pitch() .. " " .. self._rot:roll() .. "\"/>")

	return 
end

return 
