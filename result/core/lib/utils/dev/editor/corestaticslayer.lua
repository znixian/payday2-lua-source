core:module("CoreStaticsLayer")
core:import("CoreStaticLayer")
core:import("CoreEditorUtils")
core:import("CoreEws")

StaticsLayer = StaticsLayer or class(CoreStaticLayer.StaticLayer)
StaticsLayer.init = function (self, owner)
	local types = CoreEditorUtils.layer_type("statics")

	StaticsLayer.super.init(self, owner, "statics", types, "statics_layer")

	self._uses_continents = true

	return 
end
StaticsLayer.build_panel = function (self, notebook)
	StaticsLayer.super.build_panel(self, notebook)

	return self._ews_panel, true
end
StaticsLayer.add_btns_to_toolbar = function (self, ...)
	StaticsLayer.super.add_btns_to_toolbar(self, ...)
	self._btn_toolbar:add_tool("MOVE_TO_CONTINENT", "Move to continent", CoreEws.image_path("toolbar\\copy_folder_16x16.png"), "Move to continent")
	self._btn_toolbar:connect("MOVE_TO_CONTINENT", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_on_gui_move_to_continent"), nil)

	return 
end
StaticsLayer._on_gui_move_to_continent = function (self)
	if #self._selected_units == 0 then
		return 
	end

	local continent = nil
	local continents = table.map_keys(managers.editor:continents())

	table.delete(continents, managers.editor:current_continent_name())

	if 1 < #continents then
		local dialog = EWS:SingleChoiceDialog(Global.frame_panel, "Select which continent to move units:", "Continents", continents, "")

		dialog.show_modal(dialog)

		continent = dialog.get_string_selection(dialog)

		if not continent or continent == "" then
			return nil
		end
	else
		continent = continents[1]
	end

	self.move_to_continent(self, continent)

	return 
end
StaticsLayer.set_enabled = function (self, enabled)
	if not enabled then
		managers.editor:output_warning("Don't want to disable Statics layer since it would cause all dynamics to fall.")
	end

	return false
end

return 
