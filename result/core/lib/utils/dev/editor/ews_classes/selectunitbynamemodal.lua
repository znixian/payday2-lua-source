SelectUnitByNameModal = SelectUnitByNameModal or class(UnitByName)
SelectUnitByNameModal.init = function (self, name, unit_filter_function, ...)
	UnitByName.init(self, name, unit_filter_function, ...)
	self.show_modal(self)

	return 
end
SelectUnitByNameModal._build_buttons = function (self, panel, sizer)
	local select_btn = EWS:Button(panel, "Select", "", "BU_BOTTOM")

	sizer.add(sizer, select_btn, 0, 2, "RIGHT,LEFT")
	select_btn.connect(select_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_select_unit"), "")
	select_btn.connect(select_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	UnitByName._build_buttons(self, panel, sizer)

	return 
end
SelectUnitByNameModal._on_select_unit = function (self)
	self.end_modal(self)

	return 
end
SelectUnitByNameModal.on_cancel = function (self)
	self._cancelled = true

	self.end_modal(self)

	return 
end
SelectUnitByNameModal.end_modal = function (self)
	self._dialog:end_modal("hello")

	return 
end
SelectUnitByNameModal.cancelled = function (self)
	return self._cancelled
end
SelectUnitByNameModal.selected_units = function (self)
	return self._selected_item_units(self)
end
SingleSelectUnitByNameModal = SingleSelectUnitByNameModal or class(SelectUnitByNameModal)
SingleSelectUnitByNameModal.STYLE = "LC_REPORT,LC_NO_HEADER,LC_SORT_ASCENDING,LC_SINGLE_SEL"

return 
