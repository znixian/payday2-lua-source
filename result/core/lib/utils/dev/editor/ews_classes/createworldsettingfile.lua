CreateWorldSettingFile = CreateWorldSettingFile or class(CoreEditorEwsDialog)
CreateWorldSettingFile.init = function (self, params)
	CoreEditorEwsDialog.init(self, nil, "World Setting", "", Vector3(300, 150, 0), Vector3(200, 400, 0), "DEFAULT_DIALOG_STYLE,RESIZE_BORDER,STAY_ON_TOP")
	self.create_panel(self, "VERTICAL")
	self._dialog_sizer:add(self._panel, 1, 0, "EXPAND")

	local button_sizer = EWS:BoxSizer("HORIZONTAL")

	if params.path then
		local t = self._parse_file(self, managers.database:entry_path(params.path))

		if not t then
			return 
		end

		self._path = params.path

		self._add_continent_cbs(self, t)

		local save_btn = EWS:Button(self._panel, "Save", "", "")

		button_sizer.add(button_sizer, save_btn, 0, 2, "RIGHT,LEFT")
		save_btn.connect(save_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_save"), "")
	else
		self._name = params.name
		self._dir = params.dir
		self._path = params.dir .. "/" .. params.name .. ".world_setting"

		self._add_continent_cbs(self)

		local create_btn = EWS:Button(self._panel, "Create", "", "")

		button_sizer.add(button_sizer, create_btn, 0, 2, "RIGHT,LEFT")
		create_btn.connect(create_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_create"), "")
	end

	local cancel_btn = EWS:Button(self._panel, "Cancel", "", "")

	button_sizer.add(button_sizer, cancel_btn, 0, 2, "RIGHT,LEFT")
	cancel_btn.connect(cancel_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_cancel"), "")
	cancel_btn.connect(cancel_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	self._panel_sizer:add(button_sizer, 0, 0, "ALIGN_RIGHT")
	self._dialog:show_modal()

	return 
end
CreateWorldSettingFile._add_continent_cbs = function (self, params)
	self._cbs = {}
	local sizer = EWS:StaticBoxSizer(self._panel, "VERTICAL", "Exclude continents")
	slot3 = pairs
	slot4 = params or managers.editor:continents()

	for name, _ in slot3(slot4) do
		local cb = EWS:CheckBox(self._panel, name, "", "ALIGN_LEFT")

		sizer.add(sizer, cb, 0, 0, "EXPAND")
		cb.set_value(cb, (params and params[name]) or false)

		self._cbs[name] = cb
	end

	self._panel_sizer:add(sizer, 1, 0, "EXPAND")

	return 
end
CreateWorldSettingFile.on_create = function (self)
	local t = {}

	for name, cb in pairs(self._cbs) do
		t[name] = cb.get_value(cb)
	end

	local settings = SystemFS:open(self._path, "w")

	settings.puts(settings, ScriptSerializer:to_generic_xml(t))
	SystemFS:close(settings)
	self.end_modal(self)
	self._compile(self, self._path)

	return 
end
CreateWorldSettingFile._compile = function (self, path)
	local t = {
		target_db_name = "all",
		send_idstrings = false,
		preprocessor_definitions = "preprocessor_definitions",
		verbose = false,
		platform = string.lower(SystemInfo:platform():s()),
		source_root = managers.database:root_path() .. "/assets",
		target_db_root = Application:base_path() .. "assets",
		source_files = {
			managers.database:entry_path_with_properties(path)
		}
	}

	Application:data_compile(t)
	DB:reload()
	managers.database:clear_all_cached_indices()

	return 
end
CreateWorldSettingFile.on_save = function (self)
	self.on_create(self)

	return 
end
CreateWorldSettingFile._serialize_to_script = function (self, type, name)
	return PackageManager:editor_load_script_data(type.id(type), name.id(name))
end
CreateWorldSettingFile._parse_file = function (self, path)
	if not DB:has("world_setting", path) then
		return 
	end

	local settings = SystemFS:parse_xml(managers.database:entry_expanded_path("world_setting", path))

	if settings.name(settings) == "settings" then
		local t = {}

		for continent in settings.children(settings) do
			t[continent.parameter(continent, "name")] = toboolean(continent.parameter(continent, "exclude"))
		end

		return t
	else
		return self._serialize_to_script(self, "world_setting", path)
	end

	return 
end
CreateWorldSettingFile.on_cancel = function (self)
	self.end_modal(self)

	return 
end

return 
