SelectGroupByName = SelectGroupByName or class(CoreEditorEwsDialog)
SelectGroupByName.init = function (self, ...)
	CoreEditorEwsDialog.init(self, nil, "Select group by name", "", Vector3(300, 150, 0), Vector3(350, 500, 0), "DEFAULT_DIALOG_STYLE,RESIZE_BORDER,STAY_ON_TOP", ...)
	self.create_panel(self, "VERTICAL")

	local horizontal_ctrlr_sizer = EWS:BoxSizer("HORIZONTAL")
	local list_sizer = EWS:BoxSizer("VERTICAL")

	list_sizer.add(list_sizer, EWS:StaticText(self._panel, "Filter", 0, ""), 0, 0, "ALIGN_CENTER_HORIZONTAL")

	self._filter = EWS:TextCtrl(self._panel, "", "", "TE_CENTRE")

	list_sizer.add(list_sizer, self._filter, 0, 0, "EXPAND")
	self._filter:connect("EVT_COMMAND_TEXT_UPDATED", callback(self, self, "update_filter"), nil)

	self._list = EWS:ListCtrl(self._panel, "", "LC_REPORT,LC_NO_HEADER,LC_SORT_ASCENDING")

	self._list:clear_all()
	self._list:append_column("Name")
	list_sizer.add(list_sizer, self._list, 1, 0, "EXPAND")
	horizontal_ctrlr_sizer.add(horizontal_ctrlr_sizer, list_sizer, 3, 0, "EXPAND")
	self._panel_sizer:add(horizontal_ctrlr_sizer, 1, 0, "EXPAND")
	self._list:connect("EVT_COMMAND_LIST_ITEM_SELECTED", callback(self, self, "on_mark_group"), nil)
	self._list:connect("EVT_COMMAND_LIST_ITEM_ACTIVATED", callback(self, self, "on_select_group"), nil)
	self._list:connect("EVT_KEY_DOWN", callback(self, self, "key_delete"), "")
	self._list:connect("EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local button_sizer = EWS:BoxSizer("HORIZONTAL")
	local select_btn = EWS:Button(self._panel, "Select", "", "BU_BOTTOM")

	button_sizer.add(button_sizer, select_btn, 0, 2, "RIGHT,LEFT")
	select_btn.connect(select_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_select_group"), "")
	select_btn.connect(select_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local find_btn = EWS:Button(self._panel, "Ungroup", "", "BU_BOTTOM")

	button_sizer.add(button_sizer, find_btn, 0, 2, "RIGHT,LEFT")
	find_btn.connect(find_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_ungroup"), "")
	find_btn.connect(find_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local delete_btn = EWS:Button(self._panel, "Delete", "", "BU_BOTTOM")

	button_sizer.add(button_sizer, delete_btn, 0, 2, "RIGHT,LEFT")
	delete_btn.connect(delete_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_delete"), "")
	delete_btn.connect(delete_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local cancel_btn = EWS:Button(self._panel, "Cancel", "", "")

	button_sizer.add(button_sizer, cancel_btn, 0, 2, "RIGHT,LEFT")
	cancel_btn.connect(cancel_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_cancel"), "")
	cancel_btn.connect(cancel_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	self._panel_sizer:add(button_sizer, 0, 0, "ALIGN_RIGHT")
	self._dialog_sizer:add(self._panel, 1, 0, "EXPAND")
	self.fill_group_list(self)
	self._dialog:set_visible(true)

	return 
end
SelectGroupByName.key_delete = function (self, ctrlr, event)
	event.skip(event)

	if EWS:name_to_key_code("K_DELETE") == event.key_code(event) then
		self.on_delete(self)
	end

	return 
end
SelectGroupByName.key_cancel = function (self, ctrlr, event)
	event.skip(event)

	if EWS:name_to_key_code("K_ESCAPE") == event.key_code(event) then
		self.on_cancel(self)
	end

	return 
end
SelectGroupByName.on_delete = function (self)
	managers.editor:freeze_gui_lists()

	local groups = self._selected_item_groups(self)

	for _, group in ipairs(groups) do
		for _, unit in ipairs(clone(group.units(group))) do
			managers.editor:delete_unit(unit)
		end
	end

	managers.editor:thaw_gui_lists()

	return 
end
SelectGroupByName.on_mark_group = function (self)
	return 
end
SelectGroupByName.on_ungroup = function (self)
	local groups = self._selected_item_groups(self)

	for _, group in ipairs(groups) do
		managers.editor:remove_group(group.name(group))
	end

	return 
end
SelectGroupByName.on_select_group = function (self)
	local group = self._selected_item_group(self)

	if not group then
		return 
	end

	local ref = group.reference(group)

	managers.editor:change_layer_based_on_unit(ref)
	managers.editor:freeze_gui_lists()
	managers.editor:select_group(group)
	managers.editor:thaw_gui_lists()

	return 
end
SelectGroupByName._selected_item_groups = function (self)
	local groups = {}

	for _, i in ipairs(self._list:selected_items()) do
		local group = self._groups[self._list:get_item_data(i)]

		table.insert(groups, group)
	end

	return groups
end
SelectGroupByName._selected_item_group = function (self)
	local index = self._list:selected_item()

	if index ~= -1 then
		return self._groups[self._list:get_item_data(index)]
	end

	return 
end
SelectGroupByName.group_removed = function (self, group)
	for i = 0, self._list:item_count() - 1, 1 do
		if self._groups[self._list:get_item_data(i)] == group then
			self._list:delete_item(i)

			return 
		end
	end

	return 
end
SelectGroupByName.group_created = function (self, group)
	local i = self._list:append_item(group.name(group))
	local j = #self._groups + 1
	self._groups[j] = group

	self._list:set_item_data(i, j)

	return 
end
SelectGroupByName.group_selected = function (self, group)
	for _, i in ipairs(self._list:selected_items()) do
		self._list:set_item_selected(i, false)
	end

	for i = 0, self._list:item_count() - 1, 1 do
		if self._groups[self._list:get_item_data(i)] == group then
			self._list:set_item_selected(i, true)
			self._list:ensure_visible(i)

			return 
		end
	end

	return 
end
SelectGroupByName.update_filter = function (self)
	self.fill_group_list(self)

	return 
end
SelectGroupByName.fill_group_list = function (self)
	self._list:delete_all_items()

	local groups = managers.editor:groups():groups()
	local j = 1
	local filter = self._filter:get_value()
	self._groups = {}

	self._list:freeze()

	for name, group in pairs(groups) do
		if string.find(name, filter, 1, true) then
			local i = self._list:append_item(name)
			self._groups[j] = group

			self._list:set_item_data(i, j)

			j = j + 1
		end
	end

	self._list:thaw()
	self._list:autosize_column(0)

	return 
end
SelectGroupByName.reset = function (self)
	self.fill_group_list(self)

	return 
end
SelectGroupByName.freeze = function (self)
	self._list:freeze()

	return 
end
SelectGroupByName.thaw = function (self)
	self._list:thaw()

	return 
end

return 
