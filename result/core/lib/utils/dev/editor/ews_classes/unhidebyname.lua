UnhideByName = UnhideByName or class(CoreEditorEwsDialog)
UnhideByName.init = function (self, ...)
	CoreEditorEwsDialog.init(self, nil, self.TITLE or "Unhide by name", "", Vector3(300, 150, 0), Vector3(350, 500, 0), "DEFAULT_DIALOG_STYLE,RESIZE_BORDER", ...)
	self.create_panel(self, "VERTICAL")

	local horizontal_ctrlr_sizer = EWS:BoxSizer("HORIZONTAL")
	local list_sizer = EWS:BoxSizer("VERTICAL")

	list_sizer.add(list_sizer, EWS:StaticText(self._panel, "Filter", 0, ""), 0, 0, "ALIGN_CENTER_HORIZONTAL")

	self._filter = EWS:TextCtrl(self._panel, "", "", "TE_CENTRE")

	list_sizer.add(list_sizer, self._filter, 0, 0, "EXPAND")
	self._filter:connect("EVT_COMMAND_TEXT_UPDATED", callback(self, self, "update_filter"), nil)

	self._list = EWS:ListCtrl(self._panel, "", "LC_REPORT,LC_NO_HEADER,LC_SORT_ASCENDING")

	self._list:clear_all()
	self._list:append_column("Name")
	list_sizer.add(list_sizer, self._list, 1, 0, "EXPAND")
	horizontal_ctrlr_sizer.add(horizontal_ctrlr_sizer, list_sizer, 3, 0, "EXPAND")

	local list_ctrlrs = EWS:BoxSizer("VERTICAL")
	self._layer_cbs = {}
	local layers_sizer = EWS:StaticBoxSizer(self._panel, "VERTICAL", "List Layers")
	local layers = managers.editor:layers()
	local names_layers = {}

	for name, layer in pairs(layers) do
		table.insert(names_layers, name)
	end

	table.sort(names_layers)

	for _, name in ipairs(names_layers) do
		local cb = EWS:CheckBox(self._panel, name, "")

		cb.set_value(cb, true)

		self._layer_cbs[name] = cb

		cb.connect(cb, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "on_layer_cb"), {
			cb = cb,
			name = name
		})
		cb.connect(cb, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
		layers_sizer.add(layers_sizer, cb, 0, 2, "EXPAND,TOP")
	end

	local layer_buttons_sizer = EWS:BoxSizer("HORIZONTAL")
	local all_btn = EWS:Button(self._panel, "All", "", "BU_EXACTFIT,NO_BORDER")

	layer_buttons_sizer.add(layer_buttons_sizer, all_btn, 0, 2, "TOP,BOTTOM")
	all_btn.connect(all_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_all_layers"), "")
	all_btn.connect(all_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local none_btn = EWS:Button(self._panel, "None", "", "BU_EXACTFIT,NO_BORDER")

	layer_buttons_sizer.add(layer_buttons_sizer, none_btn, 0, 2, "TOP,BOTTOM")
	none_btn.connect(none_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_none_layers"), "")
	none_btn.connect(none_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local invert_btn = EWS:Button(self._panel, "Invert", "", "BU_EXACTFIT,NO_BORDER")

	layer_buttons_sizer.add(layer_buttons_sizer, invert_btn, 0, 2, "TOP,BOTTOM")
	invert_btn.connect(invert_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_invert_layers"), "")
	invert_btn.connect(invert_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	layers_sizer.add(layers_sizer, layer_buttons_sizer, 0, 2, "TOP,BOTTOM")
	list_ctrlrs.add(list_ctrlrs, layers_sizer, 0, 30, "EXPAND,TOP")

	local continents_sizer = EWS:StaticBoxSizer(self._panel, "VERTICAL", "Continents")
	self._continents_sizer = EWS:BoxSizer("VERTICAL")

	self.build_continent_cbs(self)
	continents_sizer.add(continents_sizer, self._continents_sizer, 0, 2, "TOP,BOTTOM")
	list_ctrlrs.add(list_ctrlrs, continents_sizer, 0, 5, "EXPAND,TOP")

	local continent_buttons_sizer = EWS:BoxSizer("HORIZONTAL")
	local continent_all_btn = EWS:Button(self._panel, "All", "", "BU_EXACTFIT,NO_BORDER")

	continent_buttons_sizer.add(continent_buttons_sizer, continent_all_btn, 0, 2, "TOP,BOTTOM")
	continent_all_btn.connect(continent_all_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_all_continents"), "")
	continent_all_btn.connect(continent_all_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local continent_none_btn = EWS:Button(self._panel, "None", "", "BU_EXACTFIT,NO_BORDER")

	continent_buttons_sizer.add(continent_buttons_sizer, continent_none_btn, 0, 2, "TOP,BOTTOM")
	continent_none_btn.connect(continent_none_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_none_continents"), "")
	continent_none_btn.connect(continent_none_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local continent_invert_btn = EWS:Button(self._panel, "Invert", "", "BU_EXACTFIT,NO_BORDER")

	continent_buttons_sizer.add(continent_buttons_sizer, continent_invert_btn, 0, 2, "TOP,BOTTOM")
	continent_invert_btn.connect(continent_invert_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_invert_continents"), "")
	continent_invert_btn.connect(continent_invert_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	continents_sizer.add(continents_sizer, continent_buttons_sizer, 0, 2, "TOP,BOTTOM")
	horizontal_ctrlr_sizer.add(horizontal_ctrlr_sizer, list_ctrlrs, 2, 0, "EXPAND")
	self._panel_sizer:add(horizontal_ctrlr_sizer, 1, 0, "EXPAND")
	self._list:connect("EVT_COMMAND_LIST_ITEM_SELECTED", callback(self, self, "on_mark_unit"), nil)
	self._list:connect("EVT_COMMAND_LIST_ITEM_ACTIVATED", callback(self, self, "on_unhide"), nil)
	self._list:connect("EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local button_sizer = EWS:BoxSizer("HORIZONTAL")
	local unhide_btn = EWS:Button(self._panel, self.BTN_NAME or "Unhide", "", "BU_BOTTOM")

	button_sizer.add(button_sizer, unhide_btn, 0, 2, "RIGHT,LEFT")
	unhide_btn.connect(unhide_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_unhide"), "")
	unhide_btn.connect(unhide_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local cancel_btn = EWS:Button(self._panel, "Cancel", "", "")

	button_sizer.add(button_sizer, cancel_btn, 0, 2, "RIGHT,LEFT")
	cancel_btn.connect(cancel_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_cancel"), "")
	cancel_btn.connect(cancel_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	self._panel_sizer:add(button_sizer, 0, 0, "ALIGN_RIGHT")
	self._dialog_sizer:add(self._panel, 1, 0, "EXPAND")
	self.fill_unit_list(self)
	self._dialog:set_visible(true)

	return 
end
UnhideByName.build_continent_cbs = function (self)
	self._continents_cbs = {}
	local continents = managers.editor:continents()
	self._continent_names = {}

	for name, continent in pairs(continents) do
		table.insert(self._continent_names, name)
	end

	table.sort(self._continent_names)

	for _, name in ipairs(self._continent_names) do
		local cb = EWS:CheckBox(self._panel, name, "")

		cb.set_value(cb, true)

		self._continents_cbs[name] = cb

		cb.connect(cb, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "on_continent_cb"), {
			cb = cb,
			name = name
		})
		cb.connect(cb, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
		self._continents_sizer:add(cb, 0, 2, "EXPAND,TOP")
	end

	return 
end
UnhideByName.on_continent_cb = function (self)
	self.fill_unit_list(self)

	return 
end
UnhideByName.on_all_layers = function (self)
	for name, cb in pairs(self._layer_cbs) do
		cb.set_value(cb, true)
	end

	self.fill_unit_list(self)

	return 
end
UnhideByName.on_none_layers = function (self)
	for name, cb in pairs(self._layer_cbs) do
		cb.set_value(cb, false)
	end

	self.fill_unit_list(self)

	return 
end
UnhideByName.on_invert_layers = function (self)
	for name, cb in pairs(self._layer_cbs) do
		cb.set_value(cb, not cb.get_value(cb))
	end

	self.fill_unit_list(self)

	return 
end
UnhideByName.on_all_continents = function (self)
	for name, cb in pairs(self._continents_cbs) do
		cb.set_value(cb, true)
	end

	self.fill_unit_list(self)

	return 
end
UnhideByName.on_none_continents = function (self)
	for name, cb in pairs(self._continents_cbs) do
		cb.set_value(cb, false)
	end

	self.fill_unit_list(self)

	return 
end
UnhideByName.on_invert_continents = function (self)
	for name, cb in pairs(self._continents_cbs) do
		cb.set_value(cb, not cb.get_value(cb))
	end

	self.fill_unit_list(self)

	return 
end
UnhideByName.key_cancel = function (self, ctrlr, event)
	event.skip(event)

	if EWS:name_to_key_code("K_ESCAPE") == event.key_code(event) then
		self.on_cancel(self)
	end

	return 
end
UnhideByName.on_layer_cb = function (self, data)
	self.fill_unit_list(self)

	return 
end
UnhideByName.on_cancel = function (self)
	self._dialog:set_visible(false)

	return 
end
UnhideByName.on_unhide = function (self)
	managers.editor:freeze_gui_lists()

	for _, unit in ipairs(self._selected_item_units(self)) do
		if self.IS_HIDE_BY_NAME then
			managers.editor:set_unit_visible(unit, false)
		else
			managers.editor:set_unit_visible(unit, true)
		end
	end

	managers.editor:thaw_gui_lists()

	return 
end
UnhideByName.on_delete = function (self)
	managers.editor:freeze_gui_lists()

	for _, unit in ipairs(self._selected_item_units(self)) do
		managers.editor:delete_unit(unit)
	end

	managers.editor:thaw_gui_lists()

	return 
end
UnhideByName.on_mark_unit = function (self)
	return 
end
UnhideByName._selected_item_units = function (self)
	local units = {}

	for _, i in ipairs(self._list:selected_items()) do
		local unit = self._units[self._list:get_item_data(i)]

		table.insert(units, unit)
	end

	return units
end
UnhideByName._selected_item_unit = function (self)
	local index = self._list:selected_item()

	if index ~= -1 then
		return self._units[self._list:get_item_data(index)]
	end

	return 
end
UnhideByName.select_unit = function (self, unit)
	managers.editor:select_unit(unit)

	return 
end
UnhideByName.hid_unit = function (self, unit)
	self._append_unit_to_list(self, unit)

	return 
end
UnhideByName._append_unit_to_list = function (self, unit)
	local i = self._list:append_item(unit.unit_data(unit).name_id)
	local j = #self._units + 1
	self._units[j] = unit

	self._list:set_item_data(i, j)

	return 
end
UnhideByName.unhid_unit = function (self, unit)
	self._remove_unit_from_list(self, unit)

	return 
end
UnhideByName._remove_unit_from_list = function (self, unit)
	for i = 0, self._list:item_count() - 1, 1 do
		if self._units[self._list:get_item_data(i)] == unit then
			self._list:delete_item(i)

			return 
		end
	end

	return 
end
UnhideByName.unit_name_changed = function (self, unit)
	for i = 0, self._list:item_count() - 1, 1 do
		if self._units[self._list:get_item_data(i)] == unit then
			self._list:set_item(i, 0, unit.unit_data(unit).name_id)

			local sort = false

			if 0 <= i - 1 then
				local over = self._units[self._list:get_item_data(i - 1)]:unit_data().name_id
				sort = sort or unit.unit_data(unit).name_id < over
			end

			if i + 1 < self._list:item_count() then
				local under = self._units[self._list:get_item_data(i + 1)]:unit_data().name_id
				sort = sort or under < unit.unit_data(unit).name_id
			end

			if sort then
				self.fill_unit_list(self)

				for i = 0, self._list:item_count() - 1, 1 do
					if self._units[self._list:get_item_data(i)] == unit then
						self._list:set_item_selected(i, true)
						self._list:ensure_visible(i)

						break
					end
				end
			end

			break
		end
	end

	return 
end
UnhideByName.update_filter = function (self)
	self.fill_unit_list(self)

	return 
end
UnhideByName.fill_unit_list = function (self)
	self._list:freeze()
	self._list:delete_all_items()

	local layers = managers.editor:layers()
	local j = 1
	local filter = self._filter:get_value()
	self._units = {}
	local hidden = {}

	for _, unit in ipairs(managers.editor:hidden_units()) do
		hidden[unit.key(unit)] = unit
	end

	for name, layer in pairs(layers) do
		if self._layer_cbs[name]:get_value() then
			for _, unit in ipairs(layer.created_units(layer)) do
				if self._continent_ok(self, unit) and ((not self.IS_HIDE_BY_NAME and hidden[unit.key(unit)]) or (self.IS_HIDE_BY_NAME and not hidden[unit.key(unit)])) and string.find(unit.unit_data(unit).name_id, filter, 1, true) then
					local i = self._list:append_item(unit.unit_data(unit).name_id)
					self._units[j] = unit

					self._list:set_item_data(i, j)

					j = j + 1
				end
			end
		end
	end

	self._list:autosize_column(0)
	self._list:thaw()

	return 
end
UnhideByName._continent_ok = function (self, unit)
	local continent = unit.unit_data(unit).continent

	if not continent then
		return true
	end

	return self._continents_cbs[continent.name(continent)] and self._continents_cbs[continent.name(continent)]:get_value()
end
UnhideByName.reset = function (self)
	self.fill_unit_list(self)

	return 
end
UnhideByName.freeze = function (self)
	self._list:freeze()

	return 
end
UnhideByName.thaw = function (self)
	self._list:thaw()

	return 
end
UnhideByName.recreate = function (self)
	for name, cb in pairs(self._continents_cbs) do
		self._continents_sizer:detach(cb)
		cb.destroy(cb)
	end

	self.build_continent_cbs(self)
	self.fill_unit_list(self)
	self._panel:layout()

	return 
end
HideByName = HideByName or class(UnhideByName)
HideByName.TITLE = "Hide by Name"
HideByName.BTN_NAME = "Hide"
HideByName.IS_HIDE_BY_NAME = true
HideByName.init = function (self, ...)
	HideByName.super.init(self, ...)

	return 
end
HideByName.hid_unit = function (self, unit)
	self._remove_unit_from_list(self, unit)

	return 
end
HideByName.unhid_unit = function (self, unit)
	self._append_unit_to_list(self, unit)

	return 
end
HideByName.spawned_unit = function (self, unit)
	self._append_unit_to_list(self, unit)

	return 
end
HideByName.deleted_unit = function (self, unit)
	self._remove_unit_from_list(self, unit)

	return 
end

return 
