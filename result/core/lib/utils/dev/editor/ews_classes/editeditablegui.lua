core:import("CoreEditorUtils")
core:import("CoreEws")

EditUnitEditableGui = EditUnitEditableGui or class(EditUnitBase)
EditUnitEditableGui.init = function (self, editor)
	local panel, sizer = editor or managers.editor:add_unit_edit_page({
		name = "Gui Text",
		class = self
	})
	self._panel = panel
	self._ctrls = {}
	self._element_guis = {}
	self._fonts = {
		"core/fonts/diesel",
		"fonts/font_medium_shadow_mf",
		"fonts/font_small_shadow_mf",
		"fonts/font_eroded",
		"fonts/font_large_mf",
		"fonts/font_medium_mf",
		"fonts/font_small_mf"
	}
	self._aligns = {
		horizontal = {
			"left",
			"center",
			"right",
			"justified"
		},
		vertical = {
			"top",
			"center",
			"bottom"
		}
	}
	self._blend_modes = {
		"normal",
		"add",
		"mul",
		"mulx2",
		"sub",
		"darken",
		"lighten"
	}
	self._render_templates = {
		"diffuse_vc_decal",
		"Text",
		"TextDistanceField",
		"diffuse_vc_decal_distance_field"
	}
	local ctrlrs_sizer = EWS:BoxSizer("VERTICAL")

	self._create_color_button(self, panel, ctrlrs_sizer)
	self._create_text_box(self, panel, ctrlrs_sizer)
	self._create_font_combobox(self, panel, ctrlrs_sizer)
	self._create_font_size_slider(self, panel, ctrlrs_sizer)
	self._create_text_aligns_combobox(self, panel, ctrlrs_sizer)
	self._create_text_wrap_checkbox(self, panel, ctrlrs_sizer)
	self._create_render_template_blend_mode_combobox(self, panel, ctrlrs_sizer)
	self._create_alpha_slider(self, panel, ctrlrs_sizer)
	self._create_shape_sliders(self, panel, ctrlrs_sizer)
	sizer.add(sizer, ctrlrs_sizer, 1, 1, "EXPAND")
	panel.layout(panel)
	panel.set_enabled(panel, false)

	return 
end
EditUnitEditableGui._create_color_button = function (self, panel, sizer)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 0, 0, "")
	horizontal_sizer.add(horizontal_sizer, EWS:StaticText(panel, "Color:", 0, ""), 0.5, 1, "EXPAND")

	local color_button = EWS:Button(panel, "", "", "BU_EXACTFIT")

	color_button.set_min_size(color_button, Vector3(18, 23, 0))
	color_button.connect(color_button, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "show_color_dialog"), "")
	horizontal_sizer.add(horizontal_sizer, color_button, 0, 0, "LEFT")

	self._ctrls.color_button = color_button

	return 
end
EditUnitEditableGui._create_text_box = function (self, panel, sizer)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 1, 1, "EXPAND,BOTTOM")
	horizontal_sizer.add(horizontal_sizer, EWS:StaticText(panel, "Text:", 0, ""), 0.5, 1, "EXPAND")

	local gui_text = EWS:TextCtrl(panel, "none", "", "TE_MULTILINE,TE_WORDWRAP")

	gui_text.connect(gui_text, "EVT_COMMAND_TEXT_UPDATED", callback(self, self, "update_gui_text"), gui_text)
	horizontal_sizer.add(horizontal_sizer, gui_text, 1, 1, "EXPAND")

	self._ctrls.gui_text = gui_text

	return 
end
EditUnitEditableGui._create_font_size_slider = function (self, panel, sizer)
	local horizontal_sizer = sizer
	self._font_size_params = {
		name_proportions = 1,
		name = "Font size:",
		ctrlr_proportions = 3,
		slider_ctrlr_proportions = 4,
		value = 1,
		tooltip = "Set the font size using the slider",
		min = 0.1,
		floats = 2,
		max = 10,
		panel = panel,
		sizer = horizontal_sizer
	}

	CoreEws.slider_and_number_controller(self._font_size_params)
	self._font_size_params.slider_ctrlr:connect("EVT_SCROLL_CHANGED", callback(self, self, "update_font_size"), nil)
	self._font_size_params.slider_ctrlr:connect("EVT_SCROLL_THUMBTRACK", callback(self, self, "update_font_size"), nil)
	self._font_size_params.number_ctrlr:connect("EVT_COMMAND_TEXT_ENTER", callback(self, self, "update_font_size"), nil)
	self._font_size_params.number_ctrlr:connect("EVT_KILL_FOCUS", callback(self, self, "update_font_size"), nil)

	return 
end
EditUnitEditableGui._create_font_combobox = function (self, panel, sizer)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 0, 1, "EXPAND")

	self._font_params = {
		name = "Font:",
		sizer_proportions = 1,
		name_proportions = 1,
		tooltip = "Select a font from the combobox",
		sorted = false,
		ctrlr_proportions = 1,
		panel = panel,
		sizer = horizontal_sizer,
		options = self._fonts,
		value = self._fonts[1]
	}
	local ctrlr = CoreEws.combobox(self._font_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "update_font"), nil)

	self._ctrls.font_list = ctrlr

	return 
end
EditUnitEditableGui._create_text_aligns_combobox = function (self, panel, sizer)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 0, 1, "EXPAND")

	self._aligns_horizontal_params = {
		name = "Horizontal:",
		sizer_proportions = 1,
		name_proportions = 1,
		tooltip = "Select an align from the combobox",
		sorted = false,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = horizontal_sizer,
		options = self._aligns.horizontal,
		value = self._aligns.horizontal[1]
	}
	local ctrlr = CoreEws.combobox(self._aligns_horizontal_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "update_align"), nil)

	self._ctrls.align_horizontal = ctrlr
	self._aligns_vertical_params = {
		name = " Vertical:",
		sizer_proportions = 1,
		name_proportions = 1,
		tooltip = "Select an align from the combobox",
		sorted = false,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = horizontal_sizer,
		options = self._aligns.vertical,
		value = self._aligns.vertical[1]
	}
	local ctrlr = CoreEws.combobox(self._aligns_vertical_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "update_vertical"), nil)

	self._ctrls.align_vertical = ctrlr

	return 
end
EditUnitEditableGui._create_text_wrap_checkbox = function (self, panel, sizer)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 0, 0, "TOP,BOTTOM,RIGHT")

	local checkbox = EWS:CheckBox(panel, "Text Wrap ", "")

	checkbox.set_value(checkbox, false)
	checkbox.set_tool_tip(checkbox, "Click to toggle")
	checkbox.connect(checkbox, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "update_text_wrap"), nil)
	horizontal_sizer.add(horizontal_sizer, checkbox, 0, 1, "EXPAND,LEFT")

	self._ctrls.text_wrap = checkbox
	local checkbox = EWS:CheckBox(panel, "Text Word Wrap", "")

	checkbox.set_value(checkbox, false)
	checkbox.set_tool_tip(checkbox, "Click to toggle")
	checkbox.set_enabled(checkbox, false)
	checkbox.connect(checkbox, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "update_text_word_wrap"), nil)
	horizontal_sizer.add(horizontal_sizer, checkbox, 0, 1, "EXPAND,LEFT")

	self._ctrls.text_word_wrap = checkbox

	horizontal_sizer.add_spacer(horizontal_sizer, 2, 0, 1, "")

	local checkbox = EWS:CheckBox(panel, "Debug", "")

	checkbox.set_value(checkbox, false)
	checkbox.set_tool_tip(checkbox, "Click to toggle")
	checkbox.connect(checkbox, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "update_debug"), nil)
	horizontal_sizer.add(horizontal_sizer, checkbox, 1, 1, "EXPAND,LEFT")

	self._ctrls.debug = checkbox

	return 
end
EditUnitEditableGui._create_render_template_blend_mode_combobox = function (self, panel, sizer)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 0, 1, "EXPAND")

	self._render_template_params = {
		name = "Render Template:",
		sizer_proportions = 1,
		name_proportions = 1,
		tooltip = "Select a Render Template from the combobox",
		sorted = false,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = horizontal_sizer,
		options = self._render_templates,
		value = self._render_templates[1]
	}
	local ctrlr = CoreEws.combobox(self._render_template_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "update_render_template"), nil)

	self._ctrls.render_list = ctrlr
	self._blend_mode_params = {
		name = " Blend Mode:",
		enabled = false,
		sizer_proportions = 1,
		name_proportions = 1,
		tooltip = "Select a Blend Mode from the combobox",
		sorted = false,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = horizontal_sizer,
		options = self._blend_modes,
		value = self._blend_modes[1]
	}
	local ctrlr = CoreEws.combobox(self._blend_mode_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "update_blend_mode"), nil)

	self._ctrls.blend_list = ctrlr

	return 
end
EditUnitEditableGui._create_alpha_slider = function (self, panel, sizer)
	local horizontal_sizer = sizer
	self._alpha_params = {
		name_proportions = 1,
		name = "Alpha:",
		ctrlr_proportions = 3,
		slider_ctrlr_proportions = 4,
		value = 1,
		tooltip = "Set the alpha using the slider",
		min = 0,
		floats = 2,
		max = 1,
		panel = panel,
		sizer = horizontal_sizer
	}

	CoreEws.slider_and_number_controller(self._alpha_params)
	self._alpha_params.slider_ctrlr:connect("EVT_SCROLL_CHANGED", callback(self, self, "update_alpha"), nil)
	self._alpha_params.slider_ctrlr:connect("EVT_SCROLL_THUMBTRACK", callback(self, self, "update_alpha"), nil)
	self._alpha_params.number_ctrlr:connect("EVT_COMMAND_TEXT_ENTER", callback(self, self, "update_alpha"), nil)
	self._alpha_params.number_ctrlr:connect("EVT_KILL_FOCUS", callback(self, self, "update_alpha"), nil)

	return 
end
EditUnitEditableGui._create_shape_sliders = function (self, panel, sizer)
	local horizontal_sizer = sizer

	sizer.add_spacer(sizer, 0, 2, 0, "TOP")

	self._shape_params = {}

	for i, shape in ipairs({
		"x",
		"y",
		"w",
		"h"
	}) do
		self._shape_params[i] = {
			name_proportions = 1,
			ctrlr_proportions = 3,
			slider_ctrlr_proportions = 4,
			value = 1,
			tooltip = "Set shape using the slider",
			min = 0,
			floats = 2,
			max = 1,
			name = "Shape " .. shape .. ":",
			panel = panel,
			sizer = horizontal_sizer
		}

		CoreEws.slider_and_number_controller(self._shape_params[i])
		self._shape_params[i].slider_ctrlr:connect("EVT_SCROLL_CHANGED", callback(self, self, "update_shape"), nil)
		self._shape_params[i].slider_ctrlr:connect("EVT_SCROLL_THUMBTRACK", callback(self, self, "update_shape"), nil)
		self._shape_params[i].number_ctrlr:connect("EVT_COMMAND_TEXT_ENTER", callback(self, self, "update_shape"), nil)
		self._shape_params[i].number_ctrlr:connect("EVT_KILL_FOCUS", callback(self, self, "update_shape"), nil)
	end

	return 
end
EditUnitEditableGui.show_color_dialog = function (self)
	local colordlg = EWS:ColourDialog(Global.frame, true, self._ctrls.color_button:background_colour()/255)

	if colordlg.show_modal(colordlg) then
		self._ctrls.color_button:set_background_colour(colordlg.get_colour(colordlg).x*255, colordlg.get_colour(colordlg).y*255, colordlg.get_colour(colordlg).z*255)

		for _, unit in ipairs(self._ctrls.units) do
			if alive(unit) and unit.editable_gui(unit) then
				unit.editable_gui(unit):set_font_color(Vector3(colordlg.get_colour(colordlg).x, colordlg.get_colour(colordlg).y, colordlg.get_colour(colordlg).z))
			end
		end
	end

	return 
end
EditUnitEditableGui.update_debug = function (self)
	if self._no_event or not Application:production_build() then
		return 
	end

	local enabled = self._ctrls.debug:get_value()

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_debug(enabled)
		end
	end

	return 
end
EditUnitEditableGui.update_shape = function (self)
	if self._no_event then
		return 
	end

	local shape = {
		self._shape_params[1].value,
		self._shape_params[2].value,
		self._shape_params[3].value,
		self._shape_params[4].value
	}

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_shape(shape)
		end
	end

	return 
end
EditUnitEditableGui.update_alpha = function (self)
	if self._no_event then
		return 
	end

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_alpha(self._alpha_params.value)
		end
	end

	return 
end
EditUnitEditableGui.update_render_template = function (self)
	if self._no_event then
		return 
	end

	local render_template = self._render_template_params.value

	self._ctrls.blend_list:set_enabled(render_template == "Text")

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_render_template(render_template)
		end
	end

	return 
end
EditUnitEditableGui.update_blend_mode = function (self)
	if self._no_event then
		return 
	end

	local blend_mode = self._blend_mode_params.value

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_blend_mode(blend_mode)
		end
	end

	return 
end
EditUnitEditableGui.update_text_wrap = function (self)
	if self._no_event then
		return 
	end

	local enabled = self._ctrls.text_wrap:get_value()

	self._ctrls.text_word_wrap:set_enabled(enabled)

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_wrap(enabled)
		end
	end

	return 
end
EditUnitEditableGui.update_text_word_wrap = function (self)
	if self._no_event then
		return 
	end

	local enabled = self._ctrls.text_word_wrap:get_value()

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_word_wrap(enabled)
		end
	end

	return 
end
EditUnitEditableGui.update_align = function (self)
	if self._no_event then
		return 
	end

	local align = self._aligns_horizontal_params.value

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_align(align)
		end
	end

	return 
end
EditUnitEditableGui.update_vertical = function (self)
	if self._no_event then
		return 
	end

	local align = self._aligns_vertical_params.value

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_vertical(align)
		end
	end

	return 
end
EditUnitEditableGui.update_font = function (self)
	if self._no_event then
		return 
	end

	local font = self._font_params.value

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_font(font)
		end
	end

	return 
end
EditUnitEditableGui.update_gui_text = function (self, gui_text)
	if self._no_event then
		return 
	end

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_text(utf8.from_latin1(gui_text.get_value(gui_text)))
		end
	end

	return 
end
EditUnitEditableGui.update_font_size = function (self)
	if self._no_event then
		return 
	end

	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) and unit.editable_gui(unit) then
			unit.editable_gui(unit):set_font_size(self._font_size_params.value)
		end
	end

	return 
end
EditUnitEditableGui.is_editable = function (self, unit, units)
	if alive(unit) and unit.editable_gui(unit) then
		self._ctrls.unit = unit
		self._ctrls.units = units
		local font_options = clone(self._fonts)
		local default_font = self._ctrls.unit:editable_gui():default_font()

		if not table.contains(font_options, default_font) then
			table.insert(font_options, default_font)
		end

		CoreEws.update_combobox_options(self._font_params, font_options)

		self._no_event = true

		self._ctrls.gui_text:set_value(utf8.to_latin1(self._ctrls.unit:editable_gui():text()))

		local font_color = self._ctrls.unit:editable_gui():font_color()

		self._ctrls.color_button:set_background_colour(font_color.x*255, font_color.y*255, font_color.z*255)
		CoreEws.change_slider_and_number_value(self._font_size_params, self._ctrls.unit:editable_gui():font_size())
		CoreEws.change_combobox_value(self._font_params, self._ctrls.unit:editable_gui():font())
		CoreEws.change_combobox_value(self._aligns_horizontal_params, self._ctrls.unit:editable_gui():align())
		CoreEws.change_combobox_value(self._aligns_vertical_params, self._ctrls.unit:editable_gui():vertical())
		CoreEws.change_combobox_value(self._blend_mode_params, self._ctrls.unit:editable_gui():blend_mode())
		CoreEws.change_combobox_value(self._render_template_params, self._ctrls.unit:editable_gui():render_template())
		CoreEws.change_slider_and_number_value(self._alpha_params, self._ctrls.unit:editable_gui():alpha())

		local shape = self._ctrls.unit:editable_gui():shape()

		for i, value in ipairs(shape) do
			CoreEws.change_slider_and_number_value(self._shape_params[i], value)
		end

		self._ctrls.text_wrap:set_value(self._ctrls.unit:editable_gui():wrap())
		self._ctrls.text_word_wrap:set_value(self._ctrls.unit:editable_gui():word_wrap())
		self._ctrls.text_word_wrap:set_enabled(self._ctrls.unit:editable_gui():wrap())
		self._ctrls.blend_list:set_enabled(self._ctrls.unit:editable_gui():render_template() == "Text")

		self._no_event = false

		return true
	end

	return false
end

return 
