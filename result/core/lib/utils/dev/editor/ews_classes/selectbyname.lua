SelectByName = SelectByName or class(UnitByName)
SelectByName.init = function (self, ...)
	UnitByName.init(self, "Select by name", nil, ...)

	return 
end
SelectByName._build_buttons = function (self, panel, sizer)
	local find_btn = EWS:Button(panel, "Find", "", "BU_BOTTOM")

	sizer.add(sizer, find_btn, 0, 2, "RIGHT,LEFT")
	find_btn.connect(find_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_find_unit"), "")
	find_btn.connect(find_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local select_btn = EWS:Button(panel, "Select", "", "BU_BOTTOM")

	sizer.add(sizer, select_btn, 0, 2, "RIGHT,LEFT")
	select_btn.connect(select_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_select_unit"), "")
	select_btn.connect(select_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local delete_btn = EWS:Button(panel, "Delete", "", "BU_BOTTOM")

	sizer.add(sizer, delete_btn, 0, 2, "RIGHT,LEFT")
	delete_btn.connect(delete_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_delete"), "")
	delete_btn.connect(delete_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")
	UnitByName._build_buttons(self, panel, sizer)

	return 
end
SelectByName._on_delete = function (self)
	local confirm = EWS:message_box(self._dialog, "Delete selected unit(s)?", self._dialog_name, "YES_NO,ICON_QUESTION", Vector3(-1, -1, 0))

	if confirm == "NO" then
		return 
	end

	managers.editor:freeze_gui_lists()

	for _, unit in ipairs(self._selected_item_units(self)) do
		managers.editor:delete_unit(unit)
	end

	managers.editor:thaw_gui_lists()

	return 
end
SelectByName._on_find_unit = function (self)
	self._on_select_unit(self)
	managers.editor:center_view_on_unit(self._selected_item_unit(self))

	return 
end
SelectByName._on_select_unit = function (self)
	managers.editor:change_layer_based_on_unit(self._selected_item_unit(self))
	managers.editor:freeze_gui_lists()

	self._blocked = true

	managers.editor:select_units(self._selected_item_units(self))
	managers.editor:thaw_gui_lists()

	self._blocked = false

	return 
end

return 
