core:import("CoreEditorUtils")
core:import("CoreEws")

EditUnitVariation = EditUnitVariation or class(EditUnitBase)
EditUnitVariation.init = function (self, editor)
	local panel, sizer = editor or managers.editor:add_unit_edit_page({
		name = "Variations",
		class = self
	})
	self._panel = panel
	self._ctrls = {}
	self._element_guis = {}
	local all_variations_sizer = EWS:BoxSizer("VERTICAL")
	self._mesh_params = {
		default = "default",
		name = "Mesh:",
		ctrlr_proportions = 3,
		name_proportions = 1,
		tooltip = "Select a mesh variation from the combobox",
		sorted = true,
		sizer_proportions = 2,
		panel = panel,
		sizer = all_variations_sizer,
		options = {}
	}

	CoreEws.combobox(self._mesh_params)
	self._mesh_params.ctrlr:connect("EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "change_variation"), nil)

	self._material_params = {
		default = "default",
		name = "Material:",
		ctrlr_proportions = 3,
		name_proportions = 1,
		tooltip = "Select a material variation from the combobox",
		sorted = true,
		sizer_proportions = 2,
		panel = panel,
		sizer = all_variations_sizer,
		options = {}
	}

	CoreEws.combobox(self._material_params)
	self._material_params.ctrlr:connect("EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "change_material"), nil)
	sizer.add(sizer, all_variations_sizer, 0, 0, "EXPAND")

	self._avalible_material_groups = {}

	panel.layout(panel)
	panel.set_enabled(panel, false)

	return 
end
EditUnitVariation.change_variation = function (self)
	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) then
			local mesh_variation = self._mesh_params.ctrlr:get_value()
			local reset = managers.sequence:get_reset_editable_state_sequence_list(unit.name(unit))[1]

			if reset then
				managers.sequence:run_sequence_simple2(reset, "change_state", unit)
			end

			local variations = managers.sequence:get_editable_state_sequence_list(unit.name(unit))

			if 0 < #variations then
				if mesh_variation == "default" then
					unit.unit_data(unit).mesh_variation = "default"
				elseif table.contains(variations, mesh_variation) then
					managers.sequence:run_sequence_simple2(mesh_variation, "change_state", unit)

					unit.unit_data(unit).mesh_variation = mesh_variation
				end
			end
		end
	end

	return 
end
EditUnitVariation.change_material = function (self)
	for _, unit in ipairs(self._ctrls.units) do
		if alive(unit) then
			local material = self._material_params.ctrlr:get_value()
			local materials = self.get_material_configs_from_meta(self, unit.name(unit))

			if table.contains(materials, material) then
				if material ~= "default" then
					unit.set_material_config(unit, Idstring(material), true)
				end

				unit.unit_data(unit).material = material
			end
		end
	end

	return 
end
EditUnitVariation.is_editable = function (self, unit, units)
	if alive(unit) then
		local variations = managers.sequence:get_editable_state_sequence_list(unit.name(unit))
		local materials = self.get_material_configs_from_meta(self, unit.name(unit))

		if 0 < #variations or 0 < #materials then
			self._ctrls.unit = unit
			self._ctrls.units = units

			CoreEws.update_combobox_options(self._mesh_params, variations)
			CoreEws.change_combobox_value(self._mesh_params, self._ctrls.unit:unit_data().mesh_variation)
			self._mesh_params.ctrlr:set_enabled(0 < #variations)
			CoreEws.update_combobox_options(self._material_params, materials)
			CoreEws.change_combobox_value(self._material_params, self._ctrls.unit:unit_data().material)
			self._material_params.ctrlr:set_enabled(0 < #materials)

			return true
		end
	end

	return false
end
EditUnitVariation.get_material_configs_from_meta = function (self, unit_name)
	self._avalible_material_groups = self._avalible_material_groups or {}

	if self._avalible_material_groups[unit_name.key(unit_name)] then
		return self._avalible_material_groups[unit_name.key(unit_name)]
	end

	local node = CoreEngineAccess._editor_unit_data(unit_name.id(unit_name)):model_script_data()
	local available_groups = {}
	local groups = {}

	for child in node.children(node) do
		if child.name(child) == "metadata" and child.parameter(child, "material_config_group") ~= "" then
			table.insert(groups, child.parameter(child, "material_config_group"))
		end
	end

	if 0 < #groups then
		for _, entry in ipairs(managers.database:list_entries_of_type("material_config")) do
			local node = DB:load_node("material_config", entry)

			for _, group in ipairs(groups) do
				local group_name = node.has_parameter(node, "group") and node.parameter(node, "group")

				if group_name == group and not table.contains(available_groups, entry) then
					table.insert(available_groups, entry)
				end
			end
		end
	end

	self._avalible_material_groups[unit_name.key(unit_name)] = available_groups

	return available_groups
end

return 
