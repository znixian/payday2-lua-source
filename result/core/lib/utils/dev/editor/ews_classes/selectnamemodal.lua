SelectNameModal = SelectNameModal or class(CoreEditorEwsDialog)
SelectNameModal.init = function (self, name, assets_list, settings, ...)
	Global.world_editor = Global.world_editor or {}
	Global.world_editor.filter = Global.world_editor.filter or ""
	self._dialog_name = self._dialog_name or name or "Assets"
	self._cancelled = true
	self._assets_list = assets_list

	CoreEditorEwsDialog.init(self, nil, self._dialog_name, "", Vector3(300, 150, 0), Vector3(350, 500, 0), "DEFAULT_DIALOG_STYLE,RESIZE_BORDER,STAY_ON_TOP", ...)
	self.create_panel(self, "VERTICAL")

	local panel = self._panel
	local panel_sizer = self._panel_sizer

	panel.set_sizer(panel, panel_sizer)

	local horizontal_ctrlr_sizer = EWS:BoxSizer("HORIZONTAL")
	local list_sizer = EWS:BoxSizer("VERTICAL")

	list_sizer.add(list_sizer, EWS:StaticText(panel, "Filter", 0, ""), 0, 0, "ALIGN_CENTER_HORIZONTAL")

	self._filter = EWS:TextCtrl(panel, Global.world_editor.filter, "", "TE_CENTRE")

	list_sizer.add(list_sizer, self._filter, 0, 0, "EXPAND")
	self._filter:connect("EVT_COMMAND_TEXT_UPDATED", callback(self, self, "update_filter"), nil)

	local list_style = (settings and settings.list_style) or "LC_SINGLE_SEL,LC_REPORT,LC_NO_HEADER,LC_SORT_ASCENDING"
	self._list = EWS:ListCtrl(panel, "", list_style)

	self._list:clear_all()
	self._list:append_column("Name")
	list_sizer.add(list_sizer, self._list, 1, 0, "EXPAND")
	horizontal_ctrlr_sizer.add(horizontal_ctrlr_sizer, list_sizer, 3, 0, "EXPAND")
	panel_sizer.add(panel_sizer, horizontal_ctrlr_sizer, 1, 0, "EXPAND")
	self._list:connect("EVT_COMMAND_LIST_ITEM_SELECTED", callback(self, self, "_on_mark_assett"), nil)
	self._list:connect("EVT_COMMAND_LIST_ITEM_ACTIVATED", callback(self, self, "_on_select_asset"), nil)
	self._list:connect("EVT_CHAR", callback(self, self, "key_delete"), "")
	self._list:connect("EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local button_sizer = EWS:BoxSizer("HORIZONTAL")

	self._build_buttons(self, panel, button_sizer)
	panel_sizer.add(panel_sizer, button_sizer, 0, 0, "ALIGN_RIGHT")
	self._dialog_sizer:add(self._panel, 1, 0, "EXPAND")
	self.fill_asset_list(self)
	self._dialog:set_visible(true)
	self.show_modal(self)

	return 
end
SelectNameModal._on_mark_assett = function (self)
	return 
end
SelectNameModal._on_select_asset = function (self)
	return 
end
SelectNameModal._build_buttons = function (self, panel, sizer)
	local select_btn = EWS:Button(panel, "Select", "", "BU_BOTTOM")

	sizer.add(sizer, select_btn, 0, 2, "RIGHT,LEFT")
	select_btn.connect(select_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "_on_select_asset"), "")
	select_btn.connect(select_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	local cancel_btn = EWS:Button(panel, "Cancel", "", "")

	sizer.add(sizer, cancel_btn, 0, 2, "RIGHT,LEFT")
	cancel_btn.connect(cancel_btn, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "on_cancel"), "")
	cancel_btn.connect(cancel_btn, "EVT_KEY_DOWN", callback(self, self, "key_cancel"), "")

	return 
end
SelectNameModal._on_select_asset = function (self)
	self._cancelled = false

	self._dialog:end_modal("hello")

	return 
end
SelectNameModal.on_cancel = function (self)
	self._dialog:end_modal("hello")

	return 
end
SelectNameModal.selected_assets = function (self)
	return self._selected_item_assets(self)
end
SelectNameModal.update_filter = function (self)
	Global.world_editor.filter = self._filter:get_value()

	self.fill_asset_list(self)

	return 
end
SelectNameModal.cancelled = function (self)
	return self._cancelled
end
SelectNameModal.fill_asset_list = function (self)
	self._list:delete_all_items()

	local filter = self._filter:get_value()
	filter = utf8.to_lower(utf8.from_latin1(filter))
	local j = 1
	self._assets = {}

	self._list:freeze()

	for _, asset in pairs(self._assets_list) do
		local l_asset = utf8.to_lower(utf8.from_latin1(asset))

		if string.find(l_asset, filter, 1, true) then
			local i = self._list:append_item(asset)
			self._assets[j] = asset

			self._list:set_item_data(i, j)

			j = j + 1
		end
	end

	self._list:thaw()
	self._list:autosize_column(0)

	return 
end
SelectNameModal.key_delete = function (self, ctrlr, event)
	event.skip(event)

	if EWS:name_to_key_code("K_DELETE") == event.key_code(event) then
		self._on_delete(self)
	end

	return 
end
SelectNameModal.key_cancel = function (self, ctrlr, event)
	event.skip(event)

	if EWS:name_to_key_code("K_ESCAPE") == event.key_code(event) then
		self.on_cancel(self)
	end

	return 
end
SelectNameModal._on_delete = function (self)
	return 
end
SelectNameModal._selected_item_assets = function (self)
	local assets = {}

	for _, i in ipairs(self._list:selected_items()) do
		local asset = self._list:get_item(i, 0)

		table.insert(assets, asset)
	end

	return assets
end

return 
