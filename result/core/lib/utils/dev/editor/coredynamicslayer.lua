core:module("CoreDynamicsLayer")
core:import("CoreDynamicLayer")
core:import("CoreEditorUtils")

DynamicsLayer = DynamicsLayer or class(CoreDynamicLayer.DynamicLayer)
DynamicsLayer.init = function (self, owner)
	local types = CoreEditorUtils.layer_type("dynamics")

	DynamicsLayer.super.init(self, owner, "dynamics", types, "dynamics_layer")

	self._uses_continents = true

	return 
end

return 
