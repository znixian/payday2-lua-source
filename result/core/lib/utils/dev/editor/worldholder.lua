core:import("CoreWorldDefinition")
core:import("CoreEditorUtils")
core:import("CoreEngineAccess")
core:import("CoreUnit")

local sky_orientation_data_key = Idstring("sky_orientation/rotation"):key()
CoreOldWorldDefinition = CoreOldWorldDefinition or class()
CoreMissionElementUnit = CoreMissionElementUnit or class()
WorldHolder = WorldHolder or class()
WorldHolder.get_world_file = function (self)
	Application:error("FIXME: Either unused or broken.")

	return nil
end
WorldHolder.init = function (self, params)
	if type_name(params) ~= "table" then
		Application:throw_exception("WorldHolder:init needs a table as param (was of type " .. type_name(params) .. "). Check wiki for documentation.")

		return 
	end

	local file_path = params.file_path
	local file_type = params.file_type
	self._worlds = {}

	if file_path then
		self._worldfile_generation = self._worldfile_generation(self, file_type, file_path)

		if self._worldfile_generation == "level" then
			Application:throw_exception("Level format no longer supported, use type world with resaved data. (" .. file_path .. ")")

			return 
		end

		local reverse = string.reverse(file_path)
		local i = string.find(reverse, "/")
		self._world_dir = string.reverse(string.sub(reverse, i))

		if not DB:has(file_type, file_path) then
			if not Application:editor() then
				assert(false, file_path .. "." .. file_type .. " is not in the database!")
			end

			return 
		end

		if self._worldfile_generation == "new" then
			params.world_dir = self._world_dir
			self._definition = CoreWorldDefinition.WorldDefinition:new(params)
		elseif self._worldfile_generation == "old" then
			self._error(self, "World " .. file_path .. "." .. file_type .. " is old format! Will soon result in crash, please resave.")

			local t = {
				world_dir = self._world_dir,
				world_setting = params.world_setting
			}
			local WorldDefinitionClass = rawget(_G, "WorldDefinition") or rawget(_G, "CoreOldWorldDefinition")
			local path = managers.database:entry_expanded_path(file_type, file_path)
			local node = SystemFS:parse_xml(path)

			for world in node.children(node) do
				if world.name(world) == "world" then
					t.node = world
					self._worlds[world.parameter(world, "name")] = WorldDefinitionClass.new(WorldDefinitionClass, t)
				end
			end
		end
	end

	return 
end
WorldHolder._error = function (self, msg)
	if Application:editor() then
		managers.editor:output_error(msg)
	end

	Application:error(msg)

	return 
end
WorldHolder._worldfile_generation = function (self, file_type, file_path)
	if file_type == "level" then
		return "level"
	end

	if not Application:editor() then
		return "new"
	end

	local path = managers.database:entry_expanded_path(file_type, file_path)
	local node = SystemFS:parse_xml(path)

	if not node then
		return "missing"
	end

	if node.name(node) == "worlds" then
		return "old"
	end

	if node.name(node) == "generic_scriptdata" then
		return "new"
	end

	return "unknown"
end
WorldHolder.status = function (self)
	if self._worldfile_generation == "new" then
		return "ok"
	end

	return self._worldfile_generation
end
WorldHolder.create_world = function (self, world, layer, offset)
	if self._definition then
		local return_data = self._definition:create(layer, offset)

		if not Application:editor() and (layer == "statics" or layer == "all") and not Global.running_slave then
			World:occlusion_manager():merge_occluders(5)
		end

		return return_data
	end

	local c_world = self._worlds[world]

	if c_world then
		local return_data = c_world.create(c_world, layer, offset)

		if not Application:editor() and (layer == "statics" or layer == "all") and not Global.running_slave then
			World:culling_octree():build_tree()
			World:occlusion_manager():merge_occluders(5)
		end

		if not Application:editor() and layer == "all" then
			c_world.clear_definitions(c_world)
		end

		return return_data
	else
		Application:error("WorldHolder:create_world :: Could not create world", world, "for layer", layer)
	end

	return 
end
WorldHolder.get_player_data = function (self, world, layer, offset)
	local c_world = self._worlds[world]

	if c_world then
		return c_world.get_player_data(c_world, offset)
	else
		Application:error("WorldHolder:create_world :: Could not create world", world, "for layer", layer)
	end

	return 
end
WorldHolder.get_max_id = function (self, world)
	if self._definition then
		return self._definition:get_max_id()
	end

	local c_world = self._worlds[world]

	if c_world then
		return c_world.get_max_id(c_world)
	else
		Application:error("WorldHolder:create_world :: Could not return max id", world)
	end

	return 
end
WorldHolder.get_level_name = function (self, world)
	local c_world = self._worlds[world]

	if c_world then
		return c_world.get_level_name(c_world)
	else
		Application:error("WorldHolder:create_world :: Could not return level name", world)
	end

	return 
end
CoreOldWorldDefinition.init = function (self, params)
	managers.worlddefinition = self
	self._max_id = 0
	self._level_name = "none"
	self._definitions = {}
	self._world_dir = params.world_dir

	self._load_world_package(self)
	managers.sequence:preload()

	self._old_groups = {
		groups = {},
		group_names = {}
	}
	self._portal_slot_mask = World:make_slot_mask(1)
	self._massunit_replace_names = {}
	self._replace_names = {}
	self._replace_units_path = "assets/lib/utils/dev/editor/xml/replace_units"

	self.parse_replace_unit(self)

	self._excluded_continents = {}

	self._parse_world_setting(self, params.world_setting)

	local node = params.node
	local level = params.level

	if node then
		if node.has_parameter(node, "max_id") then
			self._max_id = tonumber(node.parameter(node, "max_id"))
		end

		if node.has_parameter(node, "level_name") then
			self._level_name = node.parameter(node, "level_name")
		end

		self.parse_definitions(self, node)
	elseif level then
		self._level_file = level
		self._max_id = self._level_file:data(Idstring("world")).max_id
		self._level_name = self._level_file:data(Idstring("world")).level_name
	end

	self._definitions.editor_groups = self._definitions.editor_groups or {
		groups = self._old_groups.groups,
		group_names = self._old_groups.group_names
	}
	self._all_units = {}
	self._stage_depended_units = {}
	self._trigger_units = {}
	self._use_unit_callbacks = {}
	self._mission_element_units = {}

	return 
end
CoreOldWorldDefinition._load_node = function (self, type, path)
	local path = managers.database:entry_expanded_path(type, path)

	return SystemFS:parse_xml(path)
end
CoreOldWorldDefinition._load_world_package = function (self)
	if Application:editor() then
		return 
	end

	local package = self._world_dir .. "world"

	if not DB:has("package", package) then
		Application:throw_exception("No world.package file found in " .. self._world_dir .. ", please resave level")

		return 
	end

	if not PackageManager:loaded(package) then
		PackageManager:load(package)

		self._current_world_package = package
	end

	return 
end
CoreOldWorldDefinition._load_continent_package = function (self, path)
	if Application:editor() then
		return 
	end

	if not DB:has("package", path) then
		Application:error("Missing package for a continent(" .. path .. "), resave level " .. self._world_dir .. ".")

		return 
	end

	self._continent_packages = self._continent_packages or {}

	if not PackageManager:loaded(path) then
		PackageManager:load(path)
		table.insert(self._continent_packages, path)
		managers.sequence:preload()
	end

	return 
end
CoreOldWorldDefinition.unload_packages = function (self)
	if self._current_world_package and PackageManager:loaded(self._current_world_package) then
		PackageManager:unload(self._current_world_package)
	end

	for _, package in ipairs(self._continent_packages) do
		PackageManager:unload(package)
	end

	return 
end
CoreOldWorldDefinition.nice_path = function (self, path)
	path = string.match(string.gsub(path, ".*assets[/\\]", ""), "([^.]*)")
	path = string.gsub(path, "\\", "/")

	return path
end
CoreOldWorldDefinition._parse_world_setting = function (self, world_setting)
	if not world_setting then
		return 
	end

	local path = self.world_dir(self) .. world_setting

	if not DB:has("world_setting", path) then
		Application:error("There is no world_setting file " .. world_setting .. " at path " .. path)

		return 
	end

	local settings = DB:load_node("world_setting", path)

	for continent in settings.children(settings) do
		self._excluded_continents[continent.parameter(continent, "name")] = toboolean(continent.parameter(continent, "exclude"))
	end

	return 
end
CoreOldWorldDefinition.parse_definitions = function (self, node)
	local num_children = node.num_children(node)
	local num_progress = 0

	for type in node.children(node) do
		local name = type.name(type)
		self._definitions[name] = self._definitions[name] or {}

		if managers.editor then
			num_progress = num_progress + num_children/50

			managers.editor:update_load_progress(num_progress, "Parse layer: " .. name)
		end

		if self["parse_" .. name] then
			self["parse_" .. name](self, type, self._definitions[name])
		else
			Application:error("CoreOldWorldDefinition:No parse function for type/layer", name)
		end
	end

	return 
end
CoreOldWorldDefinition.world_dir = function (self)
	return self._world_dir
end
CoreOldWorldDefinition.get_max_id = function (self)
	return self._max_id
end
CoreOldWorldDefinition.get_level_name = function (self)
	return self._level_name
end
CoreOldWorldDefinition.parse_continents = function (self, node, t)
	local path = node.parameter(node, "file")

	if not DB:has("continents", path) then
		path = self.world_dir(self) .. path
	end

	if not DB:has("continents", path) then
		Application:error("Continent file didn't exist " .. path .. ").")

		return 
	end

	local continents = self._load_node(self, "continents", path)

	for continent in continents.children(continents) do
		local data = parse_values_node(continent)
		data = data._values or {}

		if not self._continent_editor_only(self, data) then
			local name = continent.parameter(continent, "name")

			if not self._excluded_continents[name] then
				local path = self.world_dir(self) .. name .. "/" .. name

				self._load_continent_package(self, path)

				data.base_id = data.base_id or tonumber(continent.parameter(continent, "base_id"))

				if DB:has("continent", path) then
					local node = self._load_node(self, "continent", path)

					for world in node.children(node) do
						data.level_name = world.parameter(world, "level_name")
						t[name] = data

						self.parse_definitions(self, world)
					end
				else
					Application:error("Continent file " .. path .. ".continent doesnt exist.")
				end
			end
		end
	end

	return 
end
CoreOldWorldDefinition._continent_editor_only = function (self, data)
	return not Application:editor() and data.editor_only
end
CoreOldWorldDefinition.parse_values = function (self, node, t)
	for child in node.children(node) do
		local name, value = parse_value_node(child)
		t[name] = value
	end

	return 
end
CoreOldWorldDefinition.parse_markers = function (self, node, t)
	for child in node.children(node) do
		table.insert(t, LoadedMarker:new(child))
	end

	return 
end
CoreOldWorldDefinition.parse_groups = function (self, node, t)
	for child in node.children(node) do
		local name = child.parameter(child, "name")
		local reference = tonumber(child.parameter(child, "reference_unit_id"))

		if reference ~= 0 then
			self.add_editor_group(self, name, reference)
		else
			cat_error("Removed empty group", name, "when converting from old GroupHandler to new.")
		end
	end

	return 
end
CoreOldWorldDefinition.parse_editor_groups = function (self, node, t)
	local groups = self._old_groups.groups
	local group_names = self._old_groups.group_names

	for group in node.children(node) do
		local name = group.parameter(group, "name")

		if not groups[name] then
			local reference = tonumber(group.parameter(group, "reference_id"))
			local continent = nil

			if group.has_parameter(group, "continent") and group.parameter(group, "continent") ~= "nil" then
				continent = group.parameter(group, "continent")
			end

			local units = {}

			for unit in group.children(group) do
				table.insert(units, tonumber(unit.parameter(unit, "id")))
			end

			groups[name] = {
				reference = reference,
				continent = continent,
				units = units
			}

			table.insert(group_names, name)
		end
	end

	t.groups = groups
	t.group_names = group_names

	return 
end
CoreOldWorldDefinition.add_editor_group = function (self, name, reference)
	table.insert(self._old_groups.group_names, name)

	self._old_groups.groups[name] = {
		reference = reference
	}
	self._old_groups.groups[name].units = self._old_groups.groups[name].units or {}

	return 
end
CoreOldWorldDefinition.add_editor_group_unit = function (self, name, id)
	self._old_groups.groups[name].units = self._old_groups.groups[name].units or {}

	table.insert(self._old_groups.groups[name].units, id)

	return 
end
CoreOldWorldDefinition.parse_brush = function (self, node)
	if node.has_parameter(node, "path") then
		self._massunit_path = node.parameter(node, "path")
	elseif node.has_parameter(node, "file") then
		self._massunit_path = node.parameter(node, "file")

		if not DB:has("massunit", self._massunit_path) then
			self._massunit_path = self.world_dir(self) .. self._massunit_path
		end
	end

	return 
end
CoreOldWorldDefinition.parse_sounds = function (self, node, t)
	local path = nil

	if node.has_parameter(node, "path") then
		path = node.parameter(node, "path")
	elseif node.has_parameter(node, "file") then
		path = node.parameter(node, "file")

		if not DB:has("world_sounds", path) then
			path = self.world_dir(self) .. path
		end
	end

	if not DB:has("world_sounds", path) then
		Application:error("The specified sound file '" .. path .. ".world_sounds' was not found for this level! ", path, "No sound will be loaded!")

		return 
	end

	local node = self._load_node(self, "world_sounds", path)
	self._sounds = CoreWDSoundEnvironment:new(node)

	return 
end
CoreOldWorldDefinition.parse_mission_scripts = function (self, node, t)
	if not Application:editor() then
		return 
	end

	t.scripts = t.scripts or {}
	local values = parse_values_node(node)

	for name, data in pairs(values._scripts) do
		t.scripts[name] = data
	end

	return 
end
CoreOldWorldDefinition.parse_mission = function (self, node, t)
	if Application:editor() then
		local MissionClass = rawget(_G, "MissionElementUnit") or rawget(_G, "CoreMissionElementUnit")

		for child in node.children(node) do
			table.insert(t, MissionClass.new(MissionClass, child))
		end
	end

	return 
end
CoreOldWorldDefinition.parse_environment = function (self, node)
	self._environment = CoreEnvironment:new(node)

	return 
end
CoreOldWorldDefinition.parse_world_camera = function (self, node)
	if node.has_parameter(node, "path") then
		self._world_camera_path = node.parameter(node, "path")
	elseif node.has_parameter(node, "file") then
		self._world_camera_path = node.parameter(node, "file")

		if not DB:has("world_cameras", self._world_camera_path) then
			self._world_camera_path = self.world_dir(self) .. self._world_camera_path
		end
	end

	return 
end
CoreOldWorldDefinition.parse_portal = function (self, node)
	self._portal = CorePortal:new(node)

	return 
end
CoreOldWorldDefinition.parse_wires = function (self, node, t)
	for child in node.children(node) do
		table.insert(t, CoreWire:new(child))
	end

	return 
end
CoreOldWorldDefinition.parse_statics = function (self, node, t)
	for child in node.children(node) do
		table.insert(t, CoreStaticUnit:new(child))
	end

	return 
end
CoreOldWorldDefinition.parse_dynamics = function (self, node, t)
	for child in node.children(node) do
		table.insert(t, CoreDynamicUnit:new(child))
	end

	return 
end
CoreOldWorldDefinition.release_sky_orientation_modifier = function (self)
	if self._environment then
		self._environment:release_sky_orientation_modifier()
	end

	return 
end
CoreOldWorldDefinition.clear_definitions = function (self)
	self._definitions = nil

	return 
end
CoreOldWorldDefinition.create = function (self, layer, offset)
	if self._level_file then
		self.create_from_level_file(self, {
			layer = layer,
			level_file = self._level_file,
			offset = offset
		})
		self._create_continent_level(self, layer, offset)
		self._level_file:destroy()

		return true
	else
		return self.create_units(self, layer, offset)
	end

	return 
end
CoreOldWorldDefinition._create_continent_level = function (self, layer, offset)
	if not self._level_file:data(Idstring("continents")) then
		Application:error("No continent data saved to level file, please resave.")

		return 
	end

	local path = self.world_dir(self) .. self._level_file:data(Idstring("continents")).file

	if not DB:has("continents", path) then
		Application:error("Continent file didn't exist " .. path .. ").")

		return 
	end

	local continents = DB:load_node("continents", path)

	for continent in continents.children(continents) do
		local data = parse_values_node(continent)
		data = data._values or {}

		if not self._continent_editor_only(self, data) then
			local name = continent.parameter(continent, "name")

			if not self._excluded_continents[name] then
				local path = self.world_dir(self) .. name .. "/" .. name

				self._load_continent_package(self, path)

				if DB:has("level", path) then
					local level_file = Level:load(path)

					self.create_from_level_file(self, {
						layer = layer,
						level_file = level_file,
						offset = offset
					})
					level_file.destroy(level_file)
				else
					Application:error("Continent file " .. path .. ".continent doesnt exist.")
				end
			end
		end
	end

	return 
end
CoreOldWorldDefinition.create_units = function (self, layer, offset)
	if layer ~= "all" and not self._definitions[layer] then
		return {}
	end

	local return_data = {}

	if layer == "markers" then
		return_data = self._definitions.markers
	end

	if layer == "values" then
		return_data = self._definitions.values
	end

	if layer == "editor_groups" then
		return_data = self._definitions.editor_groups
	end

	if layer == "continents" then
		return_data = self._definitions.continents
	end

	if (layer == "portal" or layer == "all") and self._portal then
		self._portal:create(offset)

		return_data = self._portal
	end

	if (layer == "sounds" or layer == "all") and self._sounds then
		self._sounds:create()

		return_data = self._sounds
	end

	if layer == "mission_scripts" then
		return_data = self._definitions.mission_scripts
	end

	if layer == "mission" then
		for _, unit in ipairs(self._definitions.mission) do
			table.insert(return_data, unit.create_unit(unit, offset))
		end
	end

	if (layer == "brush" or layer == "all") and self._massunit_path then
		self.load_massunit(self, self._massunit_path, offset)
	end

	if (layer == "environment" or layer == "all") and self._environment then
		self._environment:create(offset)

		return_data = self._environment
	end

	if (layer == "world_camera" or layer == "all") and self._world_camera_path then
		managers.worldcamera:load(self._world_camera_path, offset)
	end

	if (layer == "wires" or layer == "all") and self._definitions.wires then
		for _, unit in ipairs(self._definitions.wires) do
			table.insert(return_data, unit.create_unit(unit, offset))
		end
	end

	if layer == "statics" or layer == "all" then
		for _, unit in ipairs(self._definitions.statics) do
			table.insert(return_data, unit.create_unit(unit, offset))
		end
	end

	if layer == "dynamics" or layer == "all" then
		for _, unit in ipairs(self._definitions.dynamics) do
			table.insert(return_data, unit.create_unit(unit, offset))
		end
	end

	return return_data
end
CoreOldWorldDefinition.create_from_level_file = function (self, params)
	local layer = params.layer
	local offset = params.offset
	local level_file = params.level_file

	if (layer == "portal" or layer == "all") and not Application:editor() and level_file.data(level_file, Idstring("portal")) then
		self.create_portals(self, level_file.data(level_file, Idstring("portal")).portals, offset)
		self.create_portal_unit_groups(self, level_file.data(level_file, Idstring("portal")).unit_groups, offset)
	end

	if (layer == "sounds" or layer == "all") and level_file.data(level_file, Idstring("sounds")) then
		if level_file.data(level_file, Idstring("sounds")).path then
			self.create_sounds(self, level_file.data(level_file, Idstring("sounds")).path, offset)
		elseif level_file.data(level_file, Idstring("sounds")).file then
			self.create_sounds(self, self.world_dir(self) .. level_file.data(level_file, Idstring("sounds")).file, offset)
		end
	end

	if (layer == "brush" or layer == "all") and level_file.data(level_file, Idstring("brush")) then
		if level_file.data(level_file, Idstring("brush")).path then
			self.load_massunit(self, level_file.data(level_file, Idstring("brush")).path, offset)
		elseif level_file.data(level_file, Idstring("brush")).file then
			self.load_massunit(self, self.world_dir(self) .. level_file.data(level_file, Idstring("brush")).file, offset)
		end
	end

	if (layer == "environment" or layer == "all") and level_file.data(level_file, Idstring("environment")) then
		self.create_environment(self, level_file.data(level_file, Idstring("environment")), offset)
	end

	if (layer == "world_camera" or layer == "all") and level_file.data(level_file, Idstring("world_camera")) then
		if level_file.data(level_file, Idstring("world_camera")).path then
			managers.worldcamera:load(level_file.data(level_file, Idstring("world_camera")).path, offset)
		elseif level_file.data(level_file, Idstring("world_camera")).file then
			managers.worldcamera:load(self.world_dir(self) .. level_file.data(level_file, Idstring("world_camera")).file, offset)
		end
	end

	if layer == "wires" or layer == "all" then
		local t = self.create_level_units(self, {
			layer = "wires",
			offset = offset,
			level_file = level_file
		})

		for _, d in ipairs(t) do
			local unit = d[1]
			local data = d[2]
			local wire = data.wire
			unit.wire_data(unit).slack = wire.slack
			local target = unit.get_object(unit, Idstring("a_target"))

			target.set_position(target, wire.target_pos)
			target.set_rotation(target, wire.target_rot)
			wire_set_midpoint(unit, Idstring("rp"), Idstring("a_target"), Idstring("a_bender"))
			unit.set_moving(unit)
		end
	end

	if layer == "statics" or layer == "all" then
		self.create_level_units(self, {
			layer = "statics",
			offset = offset,
			level_file = level_file
		})
	end

	if layer == "dynamics" or layer == "all" then
		self.create_level_units(self, {
			layer = "dynamics",
			offset = offset,
			level_file = level_file
		})
	end

	return 
end
CoreOldWorldDefinition.create_level_units = function (self, params)
	local layer = params.layer
	local offset = params.offset
	local level_file = params.level_file
	local t = level_file.create(level_file, layer)

	for _, d in ipairs(t) do
		local unit = d[1]
		local data = d[2]
		local generic_data = self.make_generic_data(self, data)

		self.assign_unit_data(self, unit, generic_data)
	end

	return t
end
CoreOldWorldDefinition.create_portals = function (self, portals, offset)
	for _, portal in ipairs(portals) do
		local t = {}

		for _, point in ipairs(portal.points) do
			table.insert(t, point.position + offset)
		end

		local top = portal.top
		local bottom = portal.bottom

		if top == 0 and bottom == 0 then
			top, bottom = nil
		end

		managers.portal:add_portal(t, bottom, top)
	end

	return 
end
CoreOldWorldDefinition.create_portal_unit_groups = function (self, unit_groups, offset)
	if not unit_groups then
		return 
	end

	for name, shapes in pairs(unit_groups) do
		local group = managers.portal:add_unit_group(name)

		for _, shape in ipairs(shapes) do
			group.add_shape(group, shape)
		end
	end

	return 
end
CoreOldWorldDefinition.create_sounds = function (self, path)
	local sounds_level = Level:load(path)
	local sounds = sounds_level.data(sounds_level, Idstring("sounds"))

	managers.sound_environment:set_default_environment(sounds.environment)
	managers.sound_environment:set_default_ambience(sounds.ambience, sounds.ambience_soundbank)
	managers.sound_environment:set_ambience_enabled(sounds.ambience_enabled)

	for _, sound_environment in ipairs(sounds.sound_environments) do
		managers.sound_environment:add_area(sound_environment)
	end

	for _, sound_emitter in ipairs(sounds.sound_emitters) do
		managers.sound_environment:add_emitter(sound_emitter)
	end

	if sounds.sound_area_emitters then
		for _, sound_area_emitter in ipairs(sounds.sound_area_emitters) do
			managers.sound_environment:add_area_emitter(sound_area_emitter)
		end
	end

	sounds_level.destroy(sounds_level)

	return 
end
CoreOldWorldDefinition.create_environment = function (self, data, offset)
	managers.viewport:set_default_environment(data.environment, nil, nil)

	self._environment_modifier_id = managers.viewport:create_global_environment_modifier(sky_orientation_data_key, true, function ()
		return data.sky_rot
	end)
	local wind = data.wind

	Wind:set_direction(wind.angle, wind.angle_var, 5)
	Wind:set_tilt(wind.tilt, wind.tilt_var, 5)
	Wind:set_speed_m_s(wind.speed or 6, wind.speed_variation or 1, 5)
	Wind:set_enabled(true)

	if not Application:editor() then
		for _, effect in ipairs(data.effects) do
			local name = Idstring(effect.name)

			if DB:has("effect", name) then
				managers.portal:add_effect({
					effect = name,
					position = effect.position,
					rotation = effect.rotation
				})
			end
		end
	end

	for _, environment_area in ipairs(data.environment_areas) do
		managers.environment_area:add_area(environment_area)
	end

	return 
end
CoreOldWorldDefinition.load_massunit = function (self, path, offset)
	if Application:editor() then
		local l = MassUnitManager:list(path.id(path))

		for _, name in ipairs(l) do
			if DB:has(Idstring("unit"), name.id(name)) then
				CoreUnit.editor_load_unit(name)
			elseif not table.has(self._massunit_replace_names, name.s(name)) then
				managers.editor:output("Unit " .. name.s(name) .. " does not exist")

				local old_name = name.s(name)
				name = managers.editor:show_replace_massunit()

				if name and DB:has(Idstring("unit"), name.id(name)) then
					CoreUnit.editor_load_unit(name)
				end

				self._massunit_replace_names[old_name] = name or ""

				managers.editor:output("Unit " .. old_name .. " changed to " .. tostring(name))
			end
		end
	end

	MassUnitManager:delete_all_units()
	MassUnitManager:load(path.id(path), offset, Rotation(), self._massunit_replace_names)

	return 
end
CoreOldWorldDefinition.parse_replace_unit = function (self)
	local is_editor = Application:editor()

	if DB:has("xml", self._replace_units_path) then
		local node = DB:load_node("xml", self._replace_units_path)

		for unit in node.children(node) do
			local old_name = unit.name(unit)
			local replace_with = unit.parameter(unit, "replace_with")
			self._replace_names[old_name] = replace_with

			if is_editor then
				managers.editor:output_info("Unit " .. old_name .. " will be replaced with " .. replace_with)
			end
		end
	end

	return 
end
CoreOldWorldDefinition.preload_unit = function (self, name)
	local is_editor = Application:editor()

	if table.has(self._replace_names, name) then
		name = self._replace_names[name]
	elseif is_editor and (not DB:has(Idstring("unit"), name.id(name)) or CoreEngineAccess._editor_unit_data(name.id(name)):type():id() == Idstring("deleteme")) then
		if not DB:has(Idstring("unit"), name.id(name)) then
			managers.editor:output_info("Unit " .. name .. " does not exist")
		else
			managers.editor:output_info("Unit " .. name .. " is of type " .. CoreEngineAccess._editor_unit_data(name.id(name)):type():t())
		end

		local old_name = name
		name = managers.editor:show_replace_unit()
		self._replace_names[old_name] = name

		managers.editor:output_info("Unit " .. old_name .. " changed to " .. tostring(name))
	end

	if is_editor and name then
		CoreUnit.editor_load_unit(name)
	end

	return 
end
CoreOldWorldDefinition.make_unit = function (self, name, data, offset)
	local is_editor = Application:editor()

	if table.has(self._replace_names, name) then
		name = self._replace_names[name]
	end

	local unit = nil

	if name then
		if MassUnitManager:can_spawn_unit(Idstring(name)) and not is_editor then
			unit = MassUnitManager:spawn_unit(Idstring(name), data._position + offset, data._rotation)
		else
			unit = safe_spawn_unit(name, data._position + offset, data._rotation)
		end

		if unit then
			self.assign_unit_data(self, unit, data)
		elseif is_editor then
			local s = "Failed creating unit " .. tostring(name)

			Application:throw_exception(s)
		end
	end

	return unit
end
CoreOldWorldDefinition.assign_unit_data = function (self, unit, data)
	local is_editor = Application:editor()

	if not unit.unit_data(unit) then
		Application:error("The unit " .. unit.name(unit) .. " (" .. unit.author(unit) .. ") does not have the required extension unit_data (ScriptUnitData)")
	end

	if unit.unit_data(unit).only_exists_in_editor and not is_editor then
		unit.set_slot(unit, 0)

		return 
	end

	if data._unit_id then
		unit.unit_data(unit).unit_id = data._unit_id

		unit.set_editor_id(unit, unit.unit_data(unit).unit_id)

		self._all_units[unit.unit_data(unit).unit_id] = unit

		self.use_me(self, unit, is_editor)
	end

	if is_editor then
		unit.unit_data(unit).name_id = data._name_id
		unit.unit_data(unit).world_pos = unit.position(unit)
	end

	if data._group_name and is_editor and not self._level_file and data._group_name ~= "none" then
		self.add_editor_group_unit(self, data._group_name, unit.unit_data(unit).unit_id)
	end

	if data._continent and is_editor then
		managers.editor:add_unit_to_continent(data._continent, unit)
	end

	for _, l in ipairs(data._lights) do
		local light = unit.get_object(unit, Idstring(l.name))

		if light then
			light.set_enable(light, l.enable)
			light.set_far_range(light, l.far_range)
			light.set_color(light, l.color)

			if l.angle_start then
				light.set_spot_angle_start(light, l.angle_start)
				light.set_spot_angle_end(light, l.angle_end)
			end

			if l.multiplier then
				if tonumber(l.multiplier) then
					l.multiplier = CoreEditorUtils.get_intensity_preset(tonumber(l.multiplier))
				end

				if type_name(l.multiplier) == "string" then
					l.multiplier = Idstring(l.multiplier)
				end

				light.set_multiplier(light, LightIntensityDB:lookup(l.multiplier))
				light.set_specular_multiplier(light, LightIntensityDB:lookup_specular_multiplier(l.multiplier))
			end

			if l.falloff_exponent then
				light.set_falloff_exponent(light, l.falloff_exponent)
			end
		end
	end

	if data._variation and data._variation ~= "default" then
		unit.unit_data(unit).mesh_variation = data._variation

		managers.sequence:run_sequence_simple2(unit.unit_data(unit).mesh_variation, "change_state", unit)
	end

	if data._material_variation and data._material_variation ~= "default" then
		unit.unit_data(unit).material = data._material_variation

		unit.set_material_config(unit, unit.unit_data(unit).material, true)
	end

	if data._editable_gui then
		unit.editable_gui(unit):set_text(data._editable_gui.text)
		unit.editable_gui(unit):set_font_color(data._editable_gui.font_color)
		unit.editable_gui(unit):set_font_size(data._editable_gui.font_size)
		unit.editable_gui(unit):set_font(data._editable_gui.font)
		unit.editable_gui(unit):set_align(data._editable_gui.align)
		unit.editable_gui(unit):set_vertical(data._editable_gui.vertical)
		unit.editable_gui(unit):set_blend_mode(data._editable_gui.blend_mode)
		unit.editable_gui(unit):set_render_template(data._editable_gui.render_template)
		unit.editable_gui(unit):set_wrap(data._editable_gui.wrap)
		unit.editable_gui(unit):set_word_wrap(data._editable_gui.word_wrap)
		unit.editable_gui(unit):set_alpha(data._editable_gui.alpha)
		unit.editable_gui(unit):set_shape(data._editable_gui.shape)

		if not is_editor then
			unit.editable_gui(unit):lock_gui()
		end
	end

	self.add_trigger_sequence(self, unit, data._triggers)

	if not table.empty(data._exists_in_stages) then
		local t = clone(CoreScriptUnitData.exists_in_stages)

		for i, value in pairs(data._exists_in_stages) do
			t[i] = value
		end

		unit.unit_data(unit).exists_in_stages = t

		table.insert(self._stage_depended_units, unit)
	end

	if unit.unit_data(unit).only_visible_in_editor and not is_editor then
		unit.set_visible(unit, false)
	end

	if data.cutscene_actor then
		unit.unit_data(unit).cutscene_actor = data.cutscene_actor

		managers.cutscene:register_cutscene_actor(unit)
	end

	if data.disable_shadows then
		if is_editor then
			unit.unit_data(unit).disable_shadows = data.disable_shadows
		end

		unit.set_shadows_disabled(unit, data.disable_shadows)
	end

	if data.delayed_load then
		unit.unit_data(unit).delayed_load = data.delayed_load
	end

	if not is_editor and self._portal_slot_mask and unit.in_slot(unit, self._portal_slot_mask) and not unit.unit_data(unit).only_visible_in_editor then
		managers.portal:add_unit(unit)
	end

	return 
end
CoreOldWorldDefinition.add_trigger_sequence = function (self, unit, triggers)
	local is_editor = Application:editor()

	for _, trigger in ipairs(triggers) do
		if is_editor and Global.running_simulation then
			local notify_unit = managers.editor:unit_with_id(trigger.notify_unit_id)

			unit.damage(unit):add_trigger_sequence(trigger.name, trigger.notify_unit_sequence, notify_unit, trigger.time, nil, nil, is_editor)
		elseif self._all_units[trigger.notify_unit_id] then
			unit.damage(unit):add_trigger_sequence(trigger.name, trigger.notify_unit_sequence, self._all_units[trigger.notify_unit_id], trigger.time, nil, nil, is_editor)
		elseif self._trigger_units[trigger.notify_unit_id] then
			table.insert(self._trigger_units[trigger.notify_unit_id], {
				unit = unit,
				trigger = trigger
			})
		else
			self._trigger_units[trigger.notify_unit_id] = {
				{
					unit = unit,
					trigger = trigger
				}
			}
		end
	end

	return 
end
CoreOldWorldDefinition.use_me = function (self, unit, is_editor)
	local id = unit.unit_data(unit).unit_id

	if self._trigger_units[id] then
		for _, t in ipairs(self._trigger_units[id]) do
			t.unit:damage():add_trigger_sequence(t.trigger.name, t.trigger.notify_unit_sequence, unit, t.trigger.time, nil, nil, is_editor)
		end
	end

	if self._use_unit_callbacks[id] then
		for _, call in ipairs(self._use_unit_callbacks[id]) do
			call(unit)
		end
	end

	return 
end
CoreOldWorldDefinition.get_unit_on_load = function (self, id, call)
	if self._all_units[id] then
		return self._all_units[id]
	end

	if self._use_unit_callbacks[id] then
		table.insert(self._use_unit_callbacks[id], call)
	else
		self._use_unit_callbacks[id] = {
			call
		}
	end

	return nil
end
CoreOldWorldDefinition.check_stage_depended_units = function (self, stage)
	for _, unit in ipairs(self._stage_depended_units) do
		for i, value in ipairs(unit.unit_data(unit).exists_in_stages) do
			if stage == "stage" .. i and not value then
				World:delete_unit(unit)
			end
		end
	end

	return 
end
CoreOldWorldDefinition.get_unit = function (self, id)
	return self._all_units[id]
end
CoreOldWorldDefinition.add_mission_element_unit = function (self, unit)
	self._mission_element_units[unit.unit_data(unit).unit_id] = unit

	return 
end
CoreOldWorldDefinition.get_mission_element_unit = function (self, id)
	return self._mission_element_units[id]
end
CoreOldWorldDefinition.get_hub_element_unit = function (self, id)
	Application:stack_dump_error("CoreOldWorldDefinition:get_hub_element_unit is deprecated, use CoreOldWorldDefinition:get_mission_element_unit instead.")

	return self._mission_element_units[id]
end
CoreOldWorldDefinition.get_soundbank = function (self)
	return self._soundbank
end
LoadedMarker = LoadedMarker or class()
LoadedMarker.init = function (self, node)
	self._name = node.parameter(node, "name")
	self._pos = math.string_to_vector(node.parameter(node, "pos"))
	self._rot = math.string_to_vector(node.parameter(node, "rot"))
	self._rot = Rotation(self._rot.x, self._rot.y, self._rot.z)

	return 
end
CoreWDSoundEnvironment = CoreWDSoundEnvironment or class()
CoreWDSoundEnvironment.init = function (self, node)
	self._sound_environments = {}
	self._sound_emitters = {}
	self._sound_area_emitters = {}

	node.for_each(node, "default", callback(self, self, "parse_default"))
	node.for_each(node, "ambience", callback(self, self, "parse_ambience"))
	node.for_each(node, "sound_environment", callback(self, self, "parse_sound_environment"))
	node.for_each(node, "sound_emitter", callback(self, self, "parse_sound_emitter"))
	node.for_each(node, "sound_area_emitter", callback(self, self, "parse_sound_area_emitter"))

	return 
end
CoreWDSoundEnvironment.parse_default = function (self, node)
	self._default_ambience_soundbank = node.parameter(node, "ambience_soundbank")
	self._default_environment = node.parameter(node, "environment")
	self._default_ambience = node.parameter(node, "ambience")

	return 
end
CoreWDSoundEnvironment.parse_ambience = function (self, node)
	self._ambience_enabled = toboolean(node.parameter(node, "enabled"))

	return 
end
CoreWDSoundEnvironment.parse_sound_environment = function (self, node)
	local t = {
		environment = node.parameter(node, "environment"),
		ambience_event = node.parameter(node, "ambience_event"),
		ambience_soundbank = node.parameter(node, "ambience_soundbank"),
		position = math.string_to_vector(node.parameter(node, "position")),
		rotation = math.string_to_rotation(node.parameter(node, "rotation")),
		width = tonumber(node.parameter(node, "width")),
		depth = tonumber(node.parameter(node, "depth")),
		height = tonumber(node.parameter(node, "height")),
		name = node.parameter(node, "name")
	}

	table.insert(self._sound_environments, t)

	return 
end
CoreWDSoundEnvironment.parse_sound_emitter = function (self, node)
	for emitter in node.children(node) do
		table.insert(self._sound_emitters, parse_values_node(emitter))
	end

	return 
end
CoreWDSoundEnvironment.parse_sound_area_emitter = function (self, node)
	local t = {}

	for shape in node.children(node) do
		for value in shape.children(shape) do
			local name, vt = parse_value_node(value)
			t = vt
		end

		t.position = math.string_to_vector(shape.parameter(shape, "position"))
		t.rotation = math.string_to_rotation(shape.parameter(shape, "rotation"))
	end

	table.insert(self._sound_area_emitters, t)

	return 
end
CoreWDSoundEnvironment.create = function (self)
	managers.sound_environment:set_default_environment(self._default_environment)
	managers.sound_environment:set_default_ambience(self._default_ambience, self._default_ambience_soundbank)
	managers.sound_environment:set_ambience_enabled(self._ambience_enabled)

	for _, sound_environment in ipairs(self._sound_environments) do
		managers.sound_environment:add_area(sound_environment)
	end

	for _, sound_emitter in ipairs(self._sound_emitters) do
		managers.sound_environment:add_emitter(sound_emitter)
	end

	for _, sound_area_emitter in ipairs(self._sound_area_emitters) do
		managers.sound_environment:add_area_emitter(sound_area_emitter)
	end

	return 
end
CoreEnvironment = CoreEnvironment or class()
CoreEnvironment.init = function (self, node)
	self._values = {}

	if node.has_parameter(node, "environment") then
		self._values.environment = node.parameter(node, "environment")
	end

	if node.has_parameter(node, "sky_rot") then
		self._values.sky_rot = tonumber(node.parameter(node, "sky_rot"))
	end

	node.for_each(node, "value", callback(self, self, "parse_value"))
	node.for_each(node, "wind", callback(self, self, "parse_wind"))

	self._unit_effects = {}

	node.for_each(node, "unit_effect", callback(self, self, "parse_unit_effect"))

	self._environment_areas = {}

	node.for_each(node, "environment_area", callback(self, self, "parse_environment_area"))

	self._units_data = {}
	self._units = {}

	node.for_each(node, "unit", callback(self, self, "parse_unit"))

	return 
end
CoreEnvironment.release_sky_orientation_modifier = function (self)
	if self._environment_modifier_id then
		managers.viewport:destroy_global_environment_modifier(self._environment_modifier_id)

		self._environment_modifier_id = nil
	end

	return 
end
CoreEnvironment.parse_value = function (self, node)
	self._values[node.parameter(node, "name")] = string_to_value(node.parameter(node, "type"), node.parameter(node, "value"))

	return 
end
CoreEnvironment.parse_wind = function (self, node)
	self._wind = {
		wind_angle = tonumber(node.parameter(node, "angle")),
		wind_dir_var = tonumber(node.parameter(node, "angle_var")),
		wind_tilt = tonumber(node.parameter(node, "tilt")),
		wind_tilt_var = tonumber(node.parameter(node, "tilt_var"))
	}

	if node.has_parameter(node, "speed") then
		self._wind.wind_speed = tonumber(node.parameter(node, "speed"))
	end

	if node.has_parameter(node, "speed_variation") then
		self._wind.wind_speed_variation = tonumber(node.parameter(node, "speed_variation"))
	end

	return 
end
CoreEnvironment.parse_unit_effect = function (self, node)
	local pos, rot = nil

	for o in node.children(node) do
		pos = math.string_to_vector(o.parameter(o, "pos"))
		rot = math.string_to_rotation(o.parameter(o, "rot"))
	end

	local name = node.parameter(node, "name")
	local t = {
		pos = pos,
		rot = rot,
		name = name
	}

	table.insert(self._unit_effects, t)

	return 
end
CoreEnvironment.parse_environment_area = function (self, node)
	local t = {}

	for shape in node.children(node) do
		t = managers.shape:parse(shape)
	end

	table.insert(self._environment_areas, t)

	return 
end
CoreEnvironment.parse_unit = function (self, node)
	if not Application:editor() then
		return 
	end

	local t = {
		name = node.parameter(node, "name"),
		generic = Generic:new(node)
	}

	table.insert(self._units_data, t)

	return 
end
CoreEnvironment.sky_rotation_modifier = function (self)
	return self._values.sky_rot
end
CoreEnvironment.create = function (self, offset)
	if self._values.environment ~= "none" then
		managers.viewport:set_default_environment(self._values.environment, nil, nil)
	end

	if not Application:editor() then
		self._environment_modifier_id = self._environment_modifier_id or managers.viewport:create_global_environment_modifier(sky_orientation_data_key, true, function ()
			return self:sky_rotation_modifier()
		end)
	end

	if self._wind then
		Wind:set_direction(self._wind.wind_angle, self._wind.wind_dir_var, 5)
		Wind:set_tilt(self._wind.wind_tilt, self._wind.wind_tilt_var, 5)
		Wind:set_speed_m_s(self._wind.wind_speed or 6, self._wind.wind_speed_variation or 1, 5)
		Wind:set_enabled(true)
	end

	if not Application:editor() then
		for _, unit_effect in ipairs(self._unit_effects) do
			local name = Idstring(unit_effect.name)

			if DB:has("effect", name) then
				managers.portal:add_effect({
					effect = name,
					position = unit_effect.pos,
					rotation = unit_effect.rot
				})
			end
		end
	end

	for _, environment_area in ipairs(self._environment_areas) do
		managers.environment_area:add_area(environment_area)
	end

	for _, data in ipairs(self._units_data) do
		local unit = managers.worlddefinition:make_unit(data.name, data.generic, offset)

		table.insert(self._units, unit)
	end

	return 
end
CorePortal = CorePortal or class()
CorePortal.init = function (self, node)
	managers.worlddefinition:preload_unit("core/units/portal_point/portal_point")

	self._portal_shapes = {}
	self._unit_groups = {}

	node.for_each(node, "portal_list", callback(self, self, "parse_portal_list"))
	node.for_each(node, "unit_group", callback(self, self, "parse_unit_group"))

	return 
end
CorePortal.parse_portal_list = function (self, node)
	local name = node.parameter(node, "name")
	local top = tonumber(node.parameter(node, "top")) or 0
	local bottom = tonumber(node.parameter(node, "bottom")) or 0
	local draw_base = tonumber(node.parameter(node, "draw_base")) or 0
	self._portal_shapes[name] = {
		portal = {},
		top = top,
		bottom = bottom,
		draw_base = draw_base
	}
	local portal = self._portal_shapes[name].portal

	for o in node.children(node) do
		local p = math.string_to_vector(o.parameter(o, "pos"))

		table.insert(portal, {
			pos = p
		})
	end

	return 
end
CorePortal.parse_unit_group = function (self, node)
	local name = node.parameter(node, "name")
	local shapes = {}

	for shape in node.children(node) do
		table.insert(shapes, managers.shape:parse(shape))
	end

	self._unit_groups[name] = shapes

	return 
end
CorePortal.create = function (self, offset)
	if not Application:editor() then
		for name, portal in pairs(self._portal_shapes) do
			local t = {}

			for _, data in ipairs(portal.portal) do
				table.insert(t, data.pos + offset)
			end

			local top = portal.top
			local bottom = portal.bottom

			if top == 0 and bottom == 0 then
				top, bottom = nil
			end

			managers.portal:add_portal(t, bottom, top)
		end
	end

	for name, shapes in pairs(self._unit_groups) do
		local group = managers.portal:add_unit_group(name)

		for _, shape in ipairs(shapes) do
			group.add_shape(group, shape)
		end
	end

	return 
end
CoreWire = CoreWire or class()
CoreWire.init = function (self, node)
	self._unit_name = node.parameter(node, "name")

	managers.worlddefinition:preload_unit(self._unit_name)

	self._generic = Generic:new(node)

	node.for_each(node, "wire", callback(self, self, "parse_wire"))

	return 
end
CoreWire.parse_wire = function (self, node)
	self._target_pos = math.string_to_vector(node.parameter(node, "target_pos"))
	local rot = math.string_to_vector(node.parameter(node, "target_rot"))
	self._target_rot = Rotation(rot.x, rot.y, rot.z)
	self._slack = tonumber(node.parameter(node, "slack"))

	return 
end
CoreWire.create_unit = function (self, offset)
	self._unit = managers.worlddefinition:make_unit(self._unit_name, self._generic, offset)

	if self._unit then
		self._unit:wire_data().slack = self._slack
		local target = self._unit:get_object(Idstring("a_target"))

		target.set_position(target, self._target_pos)
		target.set_rotation(target, self._target_rot)
		wire_set_midpoint(self._unit, self._unit:orientation_object():name(), Idstring("a_target"), Idstring("a_bender"))
		self._unit:set_moving()
	end

	return self._unit
end
CoreStaticUnit = CoreStaticUnit or class()
CoreStaticUnit.init = function (self, node)
	self._unit_name = node.parameter(node, "name")

	managers.worlddefinition:preload_unit(self._unit_name)

	self._generic = Generic:new(node)

	self._generic:continent_upgrade_nil_to_world()

	return 
end
CoreStaticUnit.create_unit = function (self, offset)
	self._unit = managers.worlddefinition:make_unit(self._unit_name, self._generic, offset)

	return self._unit
end
CoreDynamicUnit = CoreDynamicUnit or class()
CoreDynamicUnit.init = function (self, node)
	self._unit_name = node.parameter(node, "name")

	managers.worlddefinition:preload_unit(self._unit_name)

	self._generic = Generic:new(node)

	self._generic:continent_upgrade_nil_to_world()

	return 
end
CoreDynamicUnit.create_unit = function (self, offset)
	self._unit = managers.worlddefinition:make_unit(self._unit_name, self._generic, offset)

	return self._unit
end
CoreMissionElementUnit.init = function (self, node)
	self._unit_name = node.parameter(node, "name")

	managers.worlddefinition:preload_unit(self._unit_name)

	if node.has_parameter(node, "script") then
		self._script = node.parameter(node, "script")
	end

	self._generic = Generic:new(node)

	self._generic:continent_upgrade_nil_to_world()
	node.for_each(node, "values", callback(self, self, "parse_values"))

	return 
end
CoreMissionElementUnit.parse_values = function (self, node)
	self._values = MissionElementValues:new(node)

	return 
end
CoreMissionElementUnit.create_unit = function (self, offset)
	self._unit = managers.worlddefinition:make_unit(self._unit_name, self._generic, offset)

	if self._unit then
		self._unit:mission_element_data().script = self._script

		managers.worlddefinition:add_mission_element_unit(self._unit)

		if self._type then
			self._type:make_unit(self._unit)
		end

		if self._values then
			self._values:set_values(self._unit)
		end
	end

	return self._unit
end
MissionElementValues = MissionElementValues or class()
MissionElementValues.init = function (self, node)
	self._values = parse_values_node(node)

	return 
end
MissionElementValues.set_values = function (self, unit)
	for name, value in pairs(self._values) do
		unit.mission_element_data(unit)[name] = value
	end

	return 
end
CoreOldWorldDefinition.make_generic_data = function (self, in_data)
	local data = {
		_name_id = "none",
		_lights = {},
		_triggers = {},
		_exists_in_stages = {}
	}
	local generic = in_data.generic
	local lights = in_data.lights
	local variation = in_data.variation
	local material_variation = in_data.material_variation
	local triggers = in_data.triggers
	local cutscene_actor = in_data.cutscene_actor
	local disable_shadows = in_data.disable_shadows
	local disable_collision = in_data.disable_collision
	local delayed_load = in_data.delayed_load

	if generic then
		data._unit_id = generic.unit_id
		data._name_id = generic.name_id
		data._group_name = generic.group_name
	end

	for _, light in ipairs(lights) do
		table.insert(data._lights, light)
	end

	if variation then
		data._variation = variation.value
	end

	if material_variation then
		data._material_variation = material_variation.value
	end

	if triggers then
		for _, trigger in ipairs(triggers) do
			table.insert(data._triggers, trigger)
		end
	end

	if cutscene_actor then
		data.cutscene_actor = cutscene_actor.name
	end

	if disable_shadows then
		data.disable_shadows = disable_shadows.value
	end

	if disable_collision then
		data.disable_collision = disable_collision.value
	end

	if delayed_load then
		data.delayed_load = delayed_load.value
	end

	data._editable_gui = in_data.editable_gui

	return data
end
Generic = Generic or class()
Generic.init = function (self, node)
	self._name_id = "none"
	self._lights = {}
	self._triggers = {}
	self._exists_in_stages = {}

	node.for_each(node, "generic", callback(self, self, "parse_generic"))
	node.for_each(node, "orientation", callback(self, self, "parse_orientation"))
	node.for_each(node, "light", callback(self, self, "parse_light"))
	node.for_each(node, "variation", callback(self, self, "parse_variation"))
	node.for_each(node, "material_variation", callback(self, self, "parse_material_variation"))
	node.for_each(node, "trigger", callback(self, self, "parse_trigger"))
	node.for_each(node, "editable_gui", callback(self, self, "parse_editable_gui"))
	node.for_each(node, "settings", callback(self, self, "parse_settings"))
	node.for_each(node, "legend_settings", callback(self, self, "parse_legend_settings"))
	node.for_each(node, "exists_in_stage", callback(self, self, "parse_exists_in_stage"))
	node.for_each(node, "cutscene_actor", callback(self, self, "cutscene_actor_settings"))
	node.for_each(node, "disable_shadows", callback(self, self, "parse_disable_shadows"))
	node.for_each(node, "disable_collision", callback(self, self, "parse_disable_collision"))
	node.for_each(node, "delayed_load", callback(self, self, "parse_delayed_load"))

	return 
end
Generic.parse_orientation = function (self, node)
	self._position = math.string_to_vector(node.parameter(node, "pos"))
	local rot = math.string_to_vector(node.parameter(node, "rot"))
	self._rotation = Rotation(rot.x, rot.y, rot.z)

	return 
end
Generic.parse_generic = function (self, node)
	if node.has_parameter(node, "unit_id") then
		self._unit_id = tonumber(node.parameter(node, "unit_id"))
	end

	if node.has_parameter(node, "name_id") then
		self._name_id = node.parameter(node, "name_id")
	end

	if node.has_parameter(node, "group_name") then
		self._group_name = node.parameter(node, "group_name")
	end

	if node.has_parameter(node, "continent") then
		local c = node.parameter(node, "continent")

		if c ~= "nil" then
			self._continent = c
		end
	end

	return 
end
Generic.continent_upgrade_nil_to_world = function (self)
	if not self._continent then
		self._continent = "world"
	end

	return 
end
Generic.parse_light = function (self, node)
	local name = node.parameter(node, "name")
	local far_range = tonumber(node.parameter(node, "far_range"))
	local enable = toboolean(node.parameter(node, "enabled"))
	local color = math.string_to_vector(node.parameter(node, "color"))
	local angle_start, angle_end, multiplier, falloff_exponent = nil

	if node.has_parameter(node, "angle_start") then
		angle_start = tonumber(node.parameter(node, "angle_start"))
		angle_end = tonumber(node.parameter(node, "angle_end"))
	end

	if node.has_parameter(node, "multiplier") then
		multiplier = node.parameter(node, "multiplier")
	end

	if node.has_parameter(node, "falloff_exponent") then
		falloff_exponent = tonumber(node.parameter(node, "falloff_exponent"))
	end

	table.insert(self._lights, {
		name = name,
		far_range = far_range,
		enable = enable,
		color = color,
		angle_start = angle_start,
		angle_end = angle_end,
		multiplier = multiplier,
		falloff_exponent = falloff_exponent
	})

	return 
end
Generic.parse_variation = function (self, node)
	self._variation = node.parameter(node, "value")

	return 
end
Generic.parse_material_variation = function (self, node)
	self._material_variation = node.parameter(node, "value")

	return 
end
Generic.parse_settings = function (self, node)
	self._unique_item = toboolean(node.parameter(node, "unique_item"))

	return 
end
Generic.parse_legend_settings = function (self, node)
	self._legend_name = node.parameter(node, "legend_name")

	return 
end
Generic.cutscene_actor_settings = function (self, node)
	self.cutscene_actor = node.parameter(node, "name")

	return 
end
Generic.parse_disable_shadows = function (self, node)
	self.disable_shadows = toboolean(node.parameter(node, "value"))

	return 
end
Generic.parse_disable_collision = function (self, node)
	self.disable_collision = toboolean(node.parameter(node, "value"))

	return 
end
Generic.parse_delayed_load = function (self, node)
	self.delayed_load = toboolean(node.parameter(node, "value"))

	return 
end
Generic.parse_exists_in_stage = function (self, node)
	self._exists_in_stages[tonumber(node.parameter(node, "stage"))] = toboolean(node.parameter(node, "value"))

	return 
end
Generic.parse_trigger = function (self, node)
	local trigger = {
		name = node.parameter(node, "name"),
		id = tonumber(node.parameter(node, "id")),
		notify_unit_id = tonumber(node.parameter(node, "notify_unit_id")),
		time = tonumber(node.parameter(node, "time")),
		notify_unit_sequence = node.parameter(node, "notify_unit_sequence")
	}

	table.insert(self._triggers, trigger)

	return 
end
Generic.parse_editable_gui = function (self, node)
	local text = node.parameter(node, "text")
	local font_color = math.string_to_vector(node.parameter(node, "font_color"))
	local font_size = tonumber(node.parameter(node, "font_size"))
	local align = node.parameter(node, "align")
	local vertical = node.parameter(node, "vertical")
	local blend_mode = node.parameter(node, "blend_mode")
	local render_template = node.parameter(node, "render_template")
	local wrap = node.parameter(node, "wrap") == "on"
	local word_wrap = node.parameter(node, "word_wrap") == "on"
	local alpha = tonumber(node.parameter(node, "alpha"))
	local shape = string.split(node.parameter(node, "shape"), " ")
	self._editable_gui = {
		text = text,
		font_color = font_color,
		font_size = font_size,
		align = align,
		vertical = vertical,
		blend_mode = blend_mode,
		render_template = render_template,
		wrap = wrap,
		word_wrap = word_wrap,
		alpha = alpha,
		shape = shape
	}

	return 
end

return 
