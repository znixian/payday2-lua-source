core:module("CorePointPicker")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreDebug")

PointPicker = PointPicker or CoreClass.mixin(CoreClass.class(), CoreEvent.BasicEventHandling)
PointPicker.init = function (self, viewport, slotmask)
	self.__viewport = viewport
	self.__enabled = false
	self.__slotmask = slotmask or World:make_slot_mask(1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 22, 23, 24, 26, 27, 30, 34, 37, 38, 39)

	return 
end
PointPicker.update = function (self, time, delta_time)
	if self.__enabled then
		local camera = self.__viewport:camera()
		local start_position = managers.editor:cursor_pos():with_z(camera.near_range(camera))
		local end_position = start_position.with_z(start_position, camera.far_range(camera))
		local ray_start = camera.screen_to_world(camera, start_position)
		local ray_end = camera.screen_to_world(camera, end_position)
		local raycast = World:raycast("ray", ray_start, ray_end, "slot_mask", self.__slotmask)
		local mouse_event = EWS:MouseEvent("EVT_MOTION")

		if mouse_event then
			if mouse_event.left_is_down(mouse_event) then
				self._mouse_left_down(self, raycast)
			else
				self._mouse_moved(self, raycast)
			end
		end
	end

	return 
end
PointPicker.start_picking = function (self)
	self.__enabled = true

	return 
end
PointPicker.stop_picking = function (self)
	self.__enabled = false

	return 
end
PointPicker._mouse_moved = function (self, raycast)
	self._send_event(self, "EVT_PICKING", raycast)

	return 
end
PointPicker._mouse_left_down = function (self, raycast)
	self._send_event(self, "EVT_FINISHED_PICKING", raycast)
	self.stop_picking(self)

	return 
end

return 
