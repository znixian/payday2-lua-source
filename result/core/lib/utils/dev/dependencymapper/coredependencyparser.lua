core:module("CoreDependencyParser")
core:import("CoreDependencyNode")
core:import("CoreGameDn")
core:import("CoreLevelDn")
core:import("CoreUnitDn")
core:import("CoreObjectDn")
core:import("CoreModelDn")
core:import("CoreMaterialfileDn")
core:import("CoreMaterialconfigDn")
core:import("CoreTextureDn")
core:import("CoreCutsceneDn")
core:import("CoreEffectDn")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreKeywordArguments")
core:import("CoreWorkbook")
core:import("CoreWorksheet")
core:import("CoreSsRow")
core:import("CoreDebug")

local parse_kwargs = CoreKeywordArguments.parse_kwargs
GAME = CoreDependencyNode.GAME
LEVEL = CoreDependencyNode.LEVEL
UNIT = CoreDependencyNode.UNIT
OBJECT = CoreDependencyNode.OBJECT
MODEL = CoreDependencyNode.MODEL
MATERIALS_FILE = CoreDependencyNode.MATERIALS_FILE
MATERIAL_CONFIG = CoreDependencyNode.MATERIAL_CONFIG
TEXTURE = CoreDependencyNode.TEXTURE
CUTSCENE = CoreDependencyNode.CUTSCENE
EFFECT = CoreDependencyNode.EFFECT
DependencyParser = DependencyParser or CoreClass.class()
DependencyParser.init = function (self, db)
	self._database = db or Database
	self._dn_cb = CoreEvent.callback(self, self, "_dn")
	self._key2node = {}

	self._make_game_node(self)
	self._make_level_nodes(self)
	self._make_materialfile_node(self)
	self._make_nodes_from_db(self, CoreUnitDn.UnitDependencyNode, "unit")
	self._make_nodes_from_db(self, CoreObjectDn.ObjectDependencyNode, "object")
	self._make_nodes_from_db(self, CoreModelDn.ModelDependencyNode, "model")
	self._make_nodes_from_db(self, CoreMaterialconfigDn.Material_configDependencyNode, "material_config")
	self._make_nodes_from_db(self, CoreTextureDn.TextureDependencyNode, "texture")
	self._make_nodes_from_db(self, CoreCutsceneDn.CutsceneDependencyNode, "cutscene")
	self._make_nodes_from_db(self, CoreEffectDn.EffectDependencyNode, "effect")

	return 
end
DependencyParser._key = function (self, type_, name)
	return string.format("%s:%s", type_, name)
end
DependencyParser._dn = function (self, ...)
	local name, type_ = parse_kwargs({
		...
	}, "string:name", "number:type_")
	local key = self._key(self, type_, name)

	return self._key2node[key]
end
DependencyParser._make_game_node = function (self)
	local key = self._key(self, GAME, "Game")
	self._key2node[key] = CoreGameDn.GameDependencyNode:new("Game", self._dn_cb, self._database)

	return 
end
DependencyParser._make_level_nodes = function (self)
	for _, dir in ipairs(File:list(CoreLevelDn.LEVEL_BASE, true)) do
		local file = string.format(CoreLevelDn.LEVEL_FILE, dir)

		if File:exists(file) then
			local key = self._key(self, LEVEL, dir)
			self._key2node[key] = CoreLevelDn.LevelDependencyNode:new(dir, self._dn_cb, self._database)
		end
	end

	return 
end
DependencyParser._make_materialfile_node = function (self)
	local key = self._key(self, MATERIALS_FILE, "Materialsfile")
	self._key2node[key] = CoreMaterialfileDn.MaterialsfileDependencyNode:new("Materialsfile", self._dn_cb, self._database)

	return 
end
DependencyParser._make_nodes_from_db = function (self, dn_class, db_type)
	for _, name in ipairs(managers.database:list_entries_of_type(db_type)) do
		local dn = dn_class.new(dn_class, name, self._dn_cb, self._database)
		local key = self._key(self, dn.type_(dn), name)
		self._key2node[key] = dn
	end

	return 
end
DependencyParser.nodes = function (self, pattern)
	local dn_list = {}

	for _, dn in pairs(self._key2node) do
		if dn.match(dn, pattern) then
			table.insert(dn_list, dn)
		end
	end

	return dn_list
end
DependencyParser.complement = function (self, dn_list, pattern)
	local all_dn = self.nodes(self)
	local list = set_difference(all_dn, dn_list)

	return filter(list, pattern)
end
DependencyParser.reached = function (self, start_dn_list, pattern)
	local reached_dn = {}

	for _, start_dn in ipairs(start_dn_list) do
		reached_dn = union(reached_dn, start_dn.reached(start_dn))
	end

	return filter(reached_dn, pattern)
end
DependencyParser.not_reached = function (self, start_dn_list, pattern)
	local reached_dn = self.reached(self, start_dn_list)
	local not_reached_dn = self.complement(self, reached_dn)

	return filter(not_reached_dn, pattern)
end

function _set2list(set)
	local list = {}

	for n, _ in pairs(set) do
		table.insert(list, n)
	end

	return list
end

function _list2set(list)
	local set = {}

	for _, n in ipairs(list) do
		set[n] = n
	end

	return set
end

function union(A_list, B_list)
	local set = {}

	for _, dn in ipairs(A_list) do
		set[dn] = dn
	end

	for _, dn in ipairs(B_list) do
		set[dn] = dn
	end

	return _set2list(set)
end

function intersect(A_list, B_list)
	local b_set = _list2set(B_list)
	local c_set = {}

	for _, dn in ipairs(A_list) do
		if b_set[dn] ~= nil then
			c_set[dn] = dn
		end
	end

	return _set2list(c_set)
end

function set_difference(A_list, B_list)
	local set = _list2set(A_list)

	for _, n in ipairs(B_list) do
		set[n] = nil
	end

	return _set2list(set)
end

function names(A_list)
	local names = {}

	for _, dn in ipairs(A_list) do
		table.insert(names, dn.name(dn))
	end

	table.sort(names)

	return names
end

function filter(dn_list, pattern)
	res_list = {}

	for _, dn in ipairs(dn_list) do
		if dn.match(dn, pattern) then
			table.insert(res_list, dn)
		end
	end

	return res_list
end

function generate_report(filepath, protected_list, dp)
	local workbook = CoreWorkbook.Workbook:new()
	local dp = dp or DependencyParser:new(ProjectDatabase)
	local game = dp.nodes(dp, GAME)[1]
	local reached = game.reached(game)

	if protected_list ~= nil then
		reached = union(reached, dp.reached(dp, protected_list))
	end

	local not_reached = dp.complement(dp, reached)

	function make_first_worksheet()
		local ws = CoreWorksheet.Worksheet:new("README")

		ws.add_row(ws, CoreSsRow.Header1Row:new("Candidates for Removal Report"))
		ws.add_row(ws, CoreSsRow.Row:new(""))
		ws.add_row(ws, CoreSsRow.Header2Row:new("About"))
		ws.add_row(ws, CoreSsRow.Row:new("This is an automatically generated report suggesting different"))
		ws.add_row(ws, CoreSsRow.Row:new("assets that (maybe) can be removed from the game because they"))
		ws.add_row(ws, CoreSsRow.Row:new("are (probably) not used. Each worksheet contains a list of candidate"))
		ws.add_row(ws, CoreSsRow.Row:new("assets of a given type. There is also a sheet with a list of assets"))
		ws.add_row(ws, CoreSsRow.Row:new("that are consider protected."))
		ws.add_row(ws, CoreSsRow.Row:new(""))
		ws.add_row(ws, CoreSsRow.Header2Row:new("Warning!"))
		ws.add_row(ws, CoreSsRow.Row:new("Unfortunately, the report is not exact: there are many assets listed"))
		ws.add_row(ws, CoreSsRow.Row:new("in this report that is actually still part of the game. One reason for"))
		ws.add_row(ws, CoreSsRow.Row:new("this is that assets can be invoked through Lua script code, another is"))
		ws.add_row(ws, CoreSsRow.Row:new("that the dependency chains between assets are not always easily tracable."))
		ws.add_row(ws, CoreSsRow.Row:new("This means the report is only a starting point for the hard, tiresome,"))
		ws.add_row(ws, CoreSsRow.Row:new("manual, and error prone labour of removing assets..."))
		ws.add_row(ws, CoreSsRow.Row:new(""))
		ws.add_row(ws, CoreSsRow.Header2Row:new("Protected Assets"))
		ws.add_row(ws, CoreSsRow.Row:new("Because of the issues listed above it is possible to add a list of"))
		ws.add_row(ws, CoreSsRow.Row:new("protected assets. By listing a asset in the protected list it (including"))
		ws.add_row(ws, CoreSsRow.Row:new("the assets it depends on) is removed from the list of candidates to"))
		ws.add_row(ws, CoreSsRow.Row:new("be removed. The protected assets are listed in the last worksheet."))

		return ws
	end

	function make_worksheet(type_, name)
		local ws = CoreWorksheet.Worksheet:new(name)

		ws.add_row(ws, CoreSsRow.Header1Row:new(string.format("%s, Candidates to be Removed:", name)))

		local node_names = names(filter(not_reached, type_))

		CoreDebug.cat_print("debug", name, #node_names)

		for _, n in ipairs(node_names) do
			ws.add_row(ws, CoreSsRow.Row:new(n))
		end

		collectgarbage()

		return ws
	end

	function make_protected_worksheet()
		local ws = CoreWorksheet.Worksheet:new("Protected Assets")

		ws.add_row(ws, CoreSsRow.Header1Row:new("Proteted Assets (by type):"))
		ws.add_row(ws, CoreSsRow.Header2Row:new("Levels", "Units", "Objects", "Model", "Material Configs", "Textures", "Cutscenes", "Effects"))

		local levels = names(filter(protected_list, LEVEL))
		local units = names(filter(protected_list, UNIT))
		local objects = names(filter(protected_list, OBJECT))
		local models = names(filter(protected_list, MODEL))
		local mtrlcfgs = names(filter(protected_list, MATERIAL_CONFIG))
		local textures = names(filter(protected_list, TEXTURE))
		local cutscenes = names(filter(protected_list, CUTSCENE))
		local effects = names(filter(protected_list, EFFECT))
		local size = math.max(#levels, #units, #objects, #models, #mtrlcfgs, #textures, #cutscenes, #effects)

		for i = 1, size, 1 do
			ws.add_row(ws, CoreSsRow.Row:new(levels[i] or "", units[i] or "", objects[i] or "", models[i] or "", mtrlcfgs[i] or "", textures[i] or "", cutscenes[i] or "", effects[i] or ""))
		end

		collectgarbage()

		return ws
	end

	workbook.add_worksheet(workbook, make_first_worksheet())
	workbook.add_worksheet(workbook, make_worksheet(LEVEL, "Levels"))
	workbook.add_worksheet(workbook, make_worksheet(UNIT, "Units"))
	workbook.add_worksheet(workbook, make_worksheet(OBJECT, "Objects"))
	workbook.add_worksheet(workbook, make_worksheet(MODEL, "Models"))
	workbook.add_worksheet(workbook, make_worksheet(MATERIAL_CONFIG, "Material Configs"))
	workbook.add_worksheet(workbook, make_worksheet(TEXTURE, "Textures"))
	workbook.add_worksheet(workbook, make_worksheet(CUTSCENE, "Cutscenes"))
	workbook.add_worksheet(workbook, make_worksheet(EFFECT, "Effects"))
	workbook.add_worksheet(workbook, make_protected_worksheet())
	collectgarbage()

	f = io.open(filepath, "w")

	if f == nil then
		local cause = "Unable to open file " .. filepath .. " (open in other program?)"

		Application:error(cause)

		return cause
	else
		workbook.to_xml(workbook, f)
		f:close()
	end

	return 
end

function generate_BC_report(filepath)
	local dp = dp or DependencyParser:new(ProjectDatabase)
	local units = dp.nodes(dp, UNIT)
	local levels = dp.nodes(dp, LEVEL)
	local textures = dp.nodes(dp, TEXTURE)
	local effects = dp.nodes(dp, EFFECT)
	local prot = {}
	prot = union(prot, filter(units, "terracotta.*"))
	prot = union(prot, filter(units, "mp_.*"))
	prot = union(prot, filter(units, "multiplayer.*"))
	prot = union(prot, filter(levels, "mp_.*"))
	prot = union(prot, filter(textures, "concept_.*"))
	prot = union(prot, filter(textures, "credits_.*"))
	prot = union(prot, filter(textures, "mp_.*"))
	prot = union(prot, filter(textures, "hud_.*"))
	prot = union(prot, filter(textures, "gui_.*"))
	prot = union(prot, filter(textures, "security_camera_.*"))
	prot = union(prot, effects)

	generate_report(filepath, prot, dp)

	return 
end

function generate_FAITH_report(filepath)
	local dp = dp or DependencyParser:new(ProjectDatabase)
	local levels = dp.nodes(dp, LEVEL)
	local units = dp.nodes(dp, UNIT)
	local objects = dp.nodes(dp, OBJECT)
	local models = dp.nodes(dp, MODEL)
	local mtrlcfgs = dp.nodes(dp, MATERIAL_CONFIG)
	local textures = dp.nodes(dp, TEXTURE)
	local cutscenes = dp.nodes(dp, CUTSCENE)
	local effects = dp.nodes(dp, EFFECT)
	local prot = {}

	generate_report(filepath, prot, dp)

	return 
end

return 
