core:module("CoreModelDn")
core:import("CoreClass")
core:import("CoreDependencyNode")

MODEL = CoreDependencyNode.MODEL
ModelDependencyNode = ModelDependencyNode or CoreClass.class(CoreDependencyNode.DependencyNodeBase)
ModelDependencyNode.init = function (self, name, get_dn_cb, database)
	self.super.init(self, MODEL, "model", name, get_dn_cb, database)

	return 
end
ModelDependencyNode._parse = function (self)
	return {}
end

return 
