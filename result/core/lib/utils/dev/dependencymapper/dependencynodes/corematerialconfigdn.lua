core:module("CoreMaterialconfigDn")
core:import("CoreClass")
core:import("CoreDependencyNode")

MATERIAL_CONFIG = CoreDependencyNode.MATERIAL_CONFIG
MATERIALS_FILE = CoreDependencyNode.MATERIALS_FILE
TEXTURE = CoreDependencyNode.TEXTURE
Material_configDependencyNode = Material_configDependencyNode or CoreClass.class(CoreDependencyNode.DependencyNodeBase)
Material_configDependencyNode.init = function (self, name, get_dn_cb, database)
	self.super.init(self, MATERIAL_CONFIG, "material_config", name, get_dn_cb, database)

	return 
end
Material_configDependencyNode._walkxml2dependencies = function (self, xmlnode, deps)
	local mf = self._get_dn({
		name = "Materialsfile",
		type_ = MATERIALS_FILE
	})

	deps.add(deps, mf)

	local node_name = xmlnode.name(xmlnode)

	if node_name ~= "material" then
		local texture_name = xmlnode.parameter(xmlnode, "file")

		if texture_name ~= nil then
			local dn = self._get_dn({
				name = texture_name,
				type_ = TEXTURE
			})

			deps.add(deps, dn)

			if dn == nil then
				Application:error("When parsing material: " .. self._name .. ", can not locate texture: " .. texture_name)
			end
		end
	end

	return 
end

return 
