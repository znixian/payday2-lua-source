core:module("CoreTextureDn")
core:import("CoreClass")
core:import("CoreDependencyNode")

TEXTURE = CoreDependencyNode.TEXTURE
TextureDependencyNode = TextureDependencyNode or CoreClass.class(CoreDependencyNode.DependencyNodeBase)
TextureDependencyNode.init = function (self, name, get_dn_cb, database)
	self.super.init(self, TEXTURE, "texture", name, get_dn_cb, database)

	return 
end
TextureDependencyNode._parse = function (self)
	return {}
end

return 
