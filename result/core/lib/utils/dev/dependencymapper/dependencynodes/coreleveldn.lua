core:module("CoreLevelDn")
core:import("CoreClass")
core:import("CoreDependencyNode")

LEVEL = CoreDependencyNode.LEVEL
UNIT = CoreDependencyNode.UNIT
LEVEL_BASE = "./data/levels/"
LEVEL_FILE = "./data/levels/%s/world.xml"
MISSION_FILE = "./data/levels/%s/mission.xml"
LEVEL_CONVERT = {
	player_start = "player"
}
LEVEL_SKIP = {
	unit_sequence = true,
	hub = true,
	environment_effect = true,
	worldcamera_trigger = true,
	cubemap_gizmo = true,
	music = true,
	music_mode = true,
	he_rumble = true,
	play_effect = true,
	physicspush = true,
	worldcamera = true,
	unit_sequence_trigger = true,
	area = true
}
LevelDependencyNode = LevelDependencyNode or CoreClass.class(CoreDependencyNode.DependencyNodeBase)
LevelDependencyNode.init = function (self, name, get_dn_cb, database)
	self.super.init(self, LEVEL, nil, name, get_dn_cb, database)

	return 
end
LevelDependencyNode._parse = function (self)
	local file = string.format(LEVEL_FILE, self._name)
	local f = File:open(file, "r")
	local level_node = Node.from_xml(f.read(f))

	f.close(f)

	local file = string.format(MISSION_FILE, self._name)
	local f = File:open(file, "r")
	local mission_node = Node.from_xml(f.read(f))

	f.close(f)

	return {
		level_node,
		mission_node
	}
end
LevelDependencyNode._walkxml2dependencies = function (self, xmlnode, deps)
	local node_name = xmlnode.name(xmlnode)

	if node_name == "unit" then
		local unit_name = xmlnode.parameter(xmlnode, "name")

		if unit_name ~= nil and not LEVEL_SKIP[unit_name] then
			unit_name = LEVEL_CONVERT[unit_name] or unit_name
			local dn = self._get_dn({
				name = unit_name,
				type_ = UNIT
			})

			deps.add(deps, dn)

			if dn == nil then
				Application:error("When parsing Level: " .. self._name .. ", can not locate Unit: " .. unit_name)
			end
		end
	elseif node_name == "enemy" then
		local unit_name = xmlnode.parameter(xmlnode, "name")

		if unit_name ~= nil then
			local dn = self._get_dn({
				name = unit_name,
				type_ = UNIT
			})

			deps.add(deps, dn)

			if dn == nil then
				Application:error("When parsing Level: " .. self._name .. ", can not locate enemy-unit: " .. unit_name)
			end
		end
	end

	return 
end

return 
