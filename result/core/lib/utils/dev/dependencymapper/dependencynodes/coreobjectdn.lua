core:module("CoreObjectDn")
core:import("CoreClass")
core:import("CoreDependencyNode")

OBJECT = CoreDependencyNode.OBJECT
MODEL = CoreDependencyNode.MODEL
MATERIAL_CONFIG = CoreDependencyNode.MATERIAL_CONFIG
ObjectDependencyNode = ObjectDependencyNode or CoreClass.class(CoreDependencyNode.DependencyNodeBase)
ObjectDependencyNode.init = function (self, name, get_dn_cb, database)
	self.super.init(self, OBJECT, "object", name, get_dn_cb, database)

	return 
end
ObjectDependencyNode._walkxml2dependencies = function (self, xmlnode, deps)
	local node_name = xmlnode.name(xmlnode)

	if node_name == "diesel" then
		local materials_name = xmlnode.parameter(xmlnode, "materials")

		if materials_name ~= nil then
			local dn = self._get_dn({
				name = materials_name,
				type_ = MATERIAL_CONFIG
			})

			deps.add(deps, dn)

			if dn == nil then
				Application:error("When parsing object: " .. self._name .. ", can not locate material_config: " .. materials_name)
			end
		end

		local diesel_file = xmlnode.parameter(xmlnode, "file")

		if diesel_file ~= nil then
			local dn = self._get_dn({
				name = diesel_file,
				type_ = MODEL
			})

			deps.add(deps, dn)

			if dn == nil then
				Application:error("When parsing object: " .. self._name .. ", can not locate model: " .. diesel_file)
			end
		end
	end

	return 
end

return 
