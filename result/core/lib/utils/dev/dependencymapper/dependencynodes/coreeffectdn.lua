core:module("CoreEffectDn")
core:import("CoreClass")
core:import("CoreDependencyNode")

TEXTURE = CoreDependencyNode.TEXTURE
EFFECT = CoreDependencyNode.EFFECT
EffectDependencyNode = EffectDependencyNode or CoreClass.class(CoreDependencyNode.DependencyNodeBase)
EffectDependencyNode.init = function (self, name, get_dn_cb, database)
	self.super.init(self, EFFECT, "effect", name, get_dn_cb, database)

	return 
end
EffectDependencyNode._walkxml2dependencies = function (self, xmlnode, deps)
	local texture_name = xmlnode.parameter(xmlnode, "texture")

	if texture_name ~= nil then
		local dn = self._get_dn({
			name = texture_name,
			type_ = TEXTURE
		})

		deps.add(deps, dn)

		if dn == nil then
			Application:error("When parsing effect: " .. self._name .. ", can not locate texture: " .. texture_name)
		end
	end

	return 
end

return 
