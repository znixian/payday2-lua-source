core:module("CoreDependencyNode")
core:import("CoreClass")

GAME = 0
LEVEL = 1
UNIT = 2
OBJECT = 3
MATERIAL_CONFIG = 4
TEXTURE = 6
CUTSCENE = 7
EFFECT = 8
MATERIALS_FILE = 9
MODEL = 10
DependencyNodeBase = DependencyNodeBase or CoreClass.class()
DependencyNodeBase.init = function (self, type_, db_type, name, get_dn_cb, database)
	assert(type(type_) == "number")
	assert(type(name) == "string")
	assert(type(get_dn_cb) == "function")
	assert(type(database) == "userdata")

	self._type = type_
	self._db_type = db_type
	self._name = name
	self._get_dn = get_dn_cb
	self._database = database
	self._parsed = false
	self._depends_on = {}

	return 
end
DependencyNodeBase.isdependencynode = function (self)
	return true
end
DependencyNodeBase.type_ = function (self)
	return self._type
end
DependencyNodeBase.name = function (self)
	return self._name
end
DependencyNodeBase.match = function (self, pattern)
	if pattern == nil then
		return true
	elseif type(pattern) == type(GAME) then
		return pattern == self.type_(self)
	elseif type(pattern) == "string" then
		return string.match(self.name(self), string.format("^%s$", pattern)) ~= nil
	elseif pattern.isdependencynode then
		return pattern == self
	elseif type(pattern) == "table" then
		for _, f in ipairs(pattern) do
			if f == self then
				return true
			end
		end

		return false
	else
		error(string.format("Filter '%s' not supported", pattern))
	end

	return 
end
DependencyNodeBase.get_dependencies = function (self)
	if not self._parsed then
		for _, xmlnode in ipairs(self._parse(self)) do
			self._walkxml(self, xmlnode)
		end

		self._parsed = true
	end

	local dn_list = {}

	for dn, _ in pairs(self._depends_on) do
		table.insert(dn_list, dn)
	end

	return dn_list
end
DependencyNodeBase.reached = function (self, pattern)
	local found = {}

	self._reached(self, pattern, {}, found)

	return found
end
DependencyNodeBase._reached = function (self, pattern, traversed, found)
	if traversed[self] then
		return 
	else
		traversed[self] = true

		if self.match(self, pattern) then
			table.insert(found, self)
		end

		for _, dn in ipairs(self.get_dependencies(self)) do
			dn._reached(dn, pattern, traversed, found)
		end
	end

	return 
end
DependencyNodeBase._parse = function (self)
	local entry = self._database:lookup(self._db_type, self._name)

	assert(entry.valid(entry))

	local xmlnode = self._database:load_node(entry)

	return {
		xmlnode
	}
end
DependencyNodeBase._walkxml = function (self, xmlnode)
	local deps = _Deps:new()

	self._walkxml2dependencies(self, xmlnode, deps)

	for _, dn in deps.get_pairs(deps) do
		self._depends_on[dn] = true
	end

	for child in xmlnode.children(xmlnode) do
		self._walkxml(self, child)
	end

	return 
end
DependencyNodeBase._walkxml2dependencies = function (self, xmlnode, deps)
	error("Not Implemented")

	return 
end
_Deps = _Deps or CoreClass.class()
_Deps.init = function (self)
	self._dnlist = {}

	return 
end
_Deps.add = function (self, dn)
	table.insert(self._dnlist, dn)

	return 
end
_Deps.get_pairs = function (self)
	return ipairs(self._dnlist)
end

return 
