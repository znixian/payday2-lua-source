core:module("CoreFreeFlightAction")

FreeFlightAction = FreeFlightAction or class()
FreeFlightAction.init = function (self, name, callback)
	self._name = assert(name)
	self._callback = assert(callback)

	return 
end
FreeFlightAction.do_action = function (self)
	self._callback()

	return 
end
FreeFlightAction.reset = function (self)
	return 
end
FreeFlightAction.name = function (self)
	return self._name
end
FreeFlightActionToggle = FreeFlightActionToggle or class()
FreeFlightActionToggle.init = function (self, name1, name2, callback1, callback2)
	self._name1 = assert(name1)
	self._name2 = assert(name2)
	self._callback1 = assert(callback1)
	self._callback2 = assert(callback2)
	self._toggle = 1

	return 
end
FreeFlightActionToggle.do_action = function (self)
	if self._toggle == 1 then
		self._toggle = 2

		self._callback1()
	else
		self._toggle = 1

		self._callback2()
	end

	return 
end
FreeFlightActionToggle.reset = function (self)
	if self._toggle == 2 then
		self.do_action(self)
	end

	return 
end
FreeFlightActionToggle.name = function (self)
	if self._toggle == 1 then
		return self._name1
	else
		return self._name2
	end

	return 
end

return 
