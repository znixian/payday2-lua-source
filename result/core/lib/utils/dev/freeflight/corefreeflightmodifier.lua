core:module("CoreFreeFlightModifier")

FreeFlightModifier = FreeFlightModifier or class()
FreeFlightModifier.init = function (self, name, values, index, callback)
	self._name = assert(name)
	self._values = assert(values)
	self._index = assert(index)
	self._callback = callback

	return 
end
FreeFlightModifier.step_up = function (self)
	self._index = math.clamp(self._index + 1, 1, #self._values)

	if self._callback then
		self._callback(self.value(self))
	end

	return 
end
FreeFlightModifier.step_down = function (self)
	self._index = math.clamp(self._index - 1, 1, #self._values)

	if self._callback then
		self._callback(self.value(self))
	end

	return 
end
FreeFlightModifier.name_value = function (self)
	return string.format("%10.10s %7.2f", self._name, self._values[self._index])
end
FreeFlightModifier.value = function (self)
	return self._values[self._index]
end

return 
