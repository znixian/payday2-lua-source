core:module("CoreWorkbook")
core:import("CoreClass")

local EMPTY_WORKBOOK_XML1 = [[
<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="header1">
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="header2">
   <Font x:Family="Swiss" ss:Bold="1" ss:Italic="1"/>
  </Style>
 </Styles>]]
local EMPTY_WORKBOOK_XML2 = "</Workbook> "
Workbook = Workbook or CoreClass.class()
Workbook.init = function (self)
	self._worksheets = {}

	return 
end
Workbook.add_worksheet = function (self, worksheet)
	table.insert(self._worksheets, worksheet)

	return 
end
Workbook.to_xml = function (self, f)
	f.write(f, EMPTY_WORKBOOK_XML1)

	local ws_xml = ""

	for _, ws in ipairs(self._worksheets) do
		f.write(f, "\n")
		ws.to_xml(ws, f)
	end

	f.write(f, EMPTY_WORKBOOK_XML2)

	return 
end

return 
