core:module("CoreWorksheet")
core:import("CoreClass")

local EMPTY_WORKSHEET_XML1 = " <Worksheet ss:Name=\"%s\">\n  <Table> "
local EMPTY_WORKSHEET_XML2 = "\n  </Table>\n </Worksheet> "
Worksheet = Worksheet or CoreClass.class()
Worksheet.init = function (self, name)
	self._name = name
	self._rows = {}

	return 
end
Worksheet.add_row = function (self, row)
	table.insert(self._rows, row)

	return 
end
Worksheet.to_xml = function (self, f)
	f.write(f, string.format(EMPTY_WORKSHEET_XML1, self._name))

	for _, r in ipairs(self._rows) do
		f.write(f, "\n")
		r.to_xml(r, f)
	end

	f.write(f, EMPTY_WORKSHEET_XML2)

	return 
end

return 
