core:module("CorePointerDraw")
core:import("CoreClass")
core:import("CoreEvent")
core:import("CoreDebug")

PointerDraw = PointerDraw or CoreClass.class()
PointerDraw.init = function (self, color, size, position)
	self.__shape = shape or "sphere"
	self.__color = color or Color("ff0000")
	self.__position = position
	self.__size = size or 30

	return 
end
PointerDraw.update = function (self, time, delta_time)
	if self.__position then
		local pen = Draw:pen()

		pen.set(pen, "no_z")
		pen.set(pen, self.__color)
		pen.sphere(pen, self.__position, self.__size)
	end

	return 
end
PointerDraw.set_position = function (self, position)
	self.__position = position

	return 
end

return 
