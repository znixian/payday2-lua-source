core:module("CoreSwitchStateMachine")
core:import("CoreFiniteStateMachine")

SwitchStateMachine = SwitchStateMachine or class(CoreFiniteStateMachine.FiniteStateMachine)
SwitchStateMachine.init = function (self, object_name, object)
	assert(object_name ~= nil)

	self._object_name = object_name
	self._object = object

	return 
end
SwitchStateMachine.clear = function (self)
	self._destroy_current_state(self)

	return 
end
SwitchStateMachine.switch_state = function (self, state_class, ...)
	assert(state_class, "You must specify a valid state class to switch to")

	if self._state_class == state_class then
		return 
	end

	self._set_state(self, state_class, ...)

	return 
end

return 
