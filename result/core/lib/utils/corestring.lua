core:module("CoreString")

utf8.find_char = function (text, char)
	for i, c in ipairs(utf8.characters(text)) do
		if c == char then
			return i
		end
	end

	return 
end
string.begins = function (s, beginning)
	if s and beginning then
		return s.sub(s, 1, #beginning) == beginning
	end

	return false
end
string.ends = function (s, ending)
	if s and ending then
		return #ending == 0 or s.sub(s, -(#ending)) == ending
	end

	return false
end
string.case_insensitive_compare = function (a, b)
	return string.lower(a) < string.lower(b)
end
string.split = function (s, separator_pattern, keep_empty, max_splits)
	local result = {}
	local pattern = "(.-)" .. separator_pattern .. "()"
	local count = 0
	local final_match_end_index = 0

	for part, end_index in string.gmatch(s, pattern) do
		final_match_end_index = end_index

		if keep_empty or part ~= "" then
			count = count + 1
			result[count] = part

			if count == max_splits then
				break
			end
		end
	end

	local remainder = string.sub(s, final_match_end_index)
	result[count + 1] = ((keep_empty or remainder ~= "") and remainder) or nil

	return result
end
string.join = function (separator, elements, keep_empty)
	local strings = table.collect(elements, function (element)
		local as_string = tostring(element)

		if as_string ~= "" or keep_empty then
			return as_string
		end

		return 
	end)

	return table.concat(strings, separator)
end
string.trim = function (s, pattern)
	pattern = pattern or "%s*"

	return string.match(s, "^" .. pattern .. "(.-)" .. pattern .. "$")
end
string.capitalize = function (s)
	return string.gsub(s, "(%w)(%w*)", function (first_letter, remaining_letters)
		return string.upper(first_letter) .. string.lower(remaining_letters)
	end)
end
string.pretty = function (s, capitalize)
	local pretty = string.gsub(s, "%W", " ")

	return (capitalize and string.capitalize(pretty)) or pretty
end
string.rep = function (self, n)
	local out = ""

	for i = 1, n, 1 do
		out = out .. self
	end

	return out
end
string.left = function (self, n)
	return self .. " ":rep(n - self.len(self))
end

return 
