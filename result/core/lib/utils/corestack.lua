core:module("CoreStack")

Stack = Stack or class()
Stack.init = function (self)
	self.clear(self)

	return 
end
Stack.push = function (self, value)
	self._last = self._last + 1
	self._table[self._last] = value

	return 
end
Stack.pop = function (self)
	if self.is_empty(self) then
		error("Stack is empty")
	end

	local value = self._table[self._last]
	self._table[self._last] = nil
	self._last = self._last - 1

	return value
end
Stack.top = function (self)
	if self.is_empty(self) then
		error("Stack is empty")
	end

	return self._table[self._last]
end
Stack.is_empty = function (self)
	return self._last == 0
end
Stack.size = function (self)
	return self._last
end
Stack.clear = function (self)
	self._last = 0
	self._table = {}

	return 
end
Stack.stack_table = function (self)
	return self._table
end

return 
