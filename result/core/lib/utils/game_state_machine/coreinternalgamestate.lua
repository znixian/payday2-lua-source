core:module("CoreInternalGameState")

GameState = GameState or class()
GameState.init = function (self, name, game_state_machine)
	self._name = name
	self._gsm = game_state_machine

	return 
end
GameState.destroy = function (self)
	return 
end
GameState.name = function (self)
	return self._name
end
GameState.gsm = function (self)
	return self._gsm
end
GameState.at_enter = function (self, previous_state)
	return 
end
GameState.at_exit = function (self, next_state)
	return 
end
GameState.default_transition = function (self, next_state)
	self.at_exit(self, next_state)
	next_state.at_enter(next_state, self)

	return 
end
GameState.force_editor_state = function (self)
	self._gsm:change_state_by_name("editor")

	return 
end
GameState.allow_world_camera_sequence = function (self)
	return false
end
GameState.play_world_camera_sequence = function (self, name, sequence)
	error("NotImplemented")

	return 
end
GameState.allow_freeflight = function (self)
	return true
end
GameState.freeflight_drop_player = function (self, pos, rot, velocity)
	Application:error("[FreeFlight] Drop player not implemented for state '" .. self.name(self) .. "'")

	return 
end

return 
