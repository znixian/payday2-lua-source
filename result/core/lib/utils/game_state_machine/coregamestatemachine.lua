core:module("CoreGameStateMachine")
core:import("CoreInitState")

GameStateMachine = GameStateMachine or class()
GameStateMachine.init = function (self, start_state)
	self._states = {}
	self._transitions = {}
	local init = CoreInitState._InitState:new(self)
	self._states[init.name(init)] = init
	self._transitions[init] = self._transitions[init] or {}
	self._transitions[init][start_state] = init.default_transition
	self._current_state = init
	self._queued_transitions = {
		{
			start_state
		}
	}

	self._do_state_change(self)

	return 
end
GameStateMachine.destroy = function (self)
	for _, state in pairs(self._states) do
		state.destroy(state)
	end

	self._states = {}
	self._transitions = {}

	return 
end
GameStateMachine.add_transition = function (self, from, to, trans_func)
	self._states[from.name(from)] = from
	self._states[to.name(to)] = to
	self._transitions[from] = self._transitions[from] or {}
	self._transitions[from][to] = trans_func

	return 
end
GameStateMachine.current_state = function (self)
	return self._current_state
end
GameStateMachine.can_change_state = function (self, state)
	local state_from = (self._queued_transitions and self._queued_transitions[#self._queued_transitions][1]) or self._current_state
	local valid_transitions = self._transitions[state_from]

	return valid_transitions and valid_transitions[state] ~= nil
end
GameStateMachine.change_state = function (self, state, params)
	if self._doing_state_change then
		Application:error("[GameStateMachine:change_state] State change during transition!")
		Application:stack_dump()
	end

	local transition_debug_string = string.format("'%s' --> '%s'", tostring(self.last_queued_state_name(self)), tostring(state.name(state)))

	cat_print("game_state_machine", "[GameStateMachine] Requested state change " .. transition_debug_string)

	if self.can_change_state(self, state) or false then
		self._queued_transitions = self._queued_transitions or {}

		table.insert(self._queued_transitions, {
			state,
			params
		})
	end

	return 
end
GameStateMachine.current_state_name = function (self)
	return self._current_state:name()
end
GameStateMachine.can_change_state_by_name = function (self, state_name)
	local state = assert(self._states[state_name], "[GameStateMachine] Name '" .. tostring(state_name) .. "' does not correspond to a valid state.")

	return self.can_change_state(self, state)
end
GameStateMachine.change_state_by_name = function (self, state_name, params)
	local state = assert(self._states[state_name], "[GameStateMachine] Name '" .. tostring(state_name) .. "' does not correspond to a valid state.")

	self.change_state(self, state, params)

	return 
end
GameStateMachine.update = function (self, t, dt)
	if self._current_state.update then
		self._current_state:update(t, dt)
	end

	return 
end
GameStateMachine.paused_update = function (self, t, dt)
	if self._current_state.paused_update then
		self._current_state:paused_update(t, dt)
	end

	return 
end
GameStateMachine.end_update = function (self, t, dt)
	if self._queued_transitions then
		self._do_state_change(self)
	end

	return 
end
GameStateMachine._do_state_change = function (self)
	if not self._queued_transitions then
		return 
	end

	self._doing_state_change = true

	for i_transition, transition in ipairs(self._queued_transitions) do
		local new_state = transition[1]
		local params = transition[2]
		local old_state = self._current_state
		local trans_func = self._transitions[old_state][new_state]

		cat_print("game_state_machine", "[GameStateMachine] Executing state change '" .. tostring(old_state.name(old_state)) .. "' --> '" .. tostring(new_state.name(new_state)) .. "'")

		self._current_state = new_state

		trans_func(old_state, new_state, params)
	end

	self._queued_transitions = nil
	self._doing_state_change = false

	return 
end
GameStateMachine.last_queued_state_name = function (self)
	if self._queued_transitions then
		return self._queued_transitions[#self._queued_transitions][1]:name()
	else
		return self.current_state_name(self)
	end

	return 
end

return 
