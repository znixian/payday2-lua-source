core:module("CoreInitState")
core:import("CoreInternalGameState")

_InitState = _InitState or class(CoreInternalGameState.GameState)
_InitState.init = function (self, game_state_machine)
	CoreInternalGameState.GameState.init(self, "init", game_state_machine)

	return 
end
_InitState.at_enter = function (self)
	error("[GameStateMachine] ERROR, you are not allowed to enter the init state")

	return 
end

return 
