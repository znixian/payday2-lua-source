core:module("CoreFiniteStateMachine")
core:import("CoreDebug")

FiniteStateMachine = FiniteStateMachine or class()
FiniteStateMachine.init = function (self, state_class, object_name, object)
	self._object = object
	self._object_name = object_name

	if state_class then
		self._set_state(self, state_class)
	end

	self._debug = true

	return 
end
FiniteStateMachine.load = function (self, data)
	local class = _G[data.class_name]

	self._set_state(class)

	return 
end
FiniteStateMachine.save = function (self, data)
	data.class_name = class_name(self._state_class)

	return 
end
FiniteStateMachine.set_debug = function (self, debug_enabled)
	self._debug = debug_enabled

	return 
end
FiniteStateMachine.destroy = function (self)
	self._destroy_current_state(self)

	return 
end
FiniteStateMachine.transition = function (self)
	assert(self._state)
	assert(self._state.transition, "You must at least have a transition method")

	local new_state_class, arg = self._state:transition()

	if new_state_class then
		self._set_state(self, new_state_class, arg)
	end

	return 
end
FiniteStateMachine.state = function (self)
	assert(self._state)

	return self._state
end
FiniteStateMachine._class_name = function (self, state_class)
	return CoreDebug.full_class_name(state_class)
end
FiniteStateMachine._destroy_current_state = function (self)
	if self._state and self._state.destroy then
		self._state:destroy()

		self._state = nil
	end

	return 
end
FiniteStateMachine._set_state = function (self, new_state_class, ...)
	if self._debug then
		cat_print("debug", "transitions from '" .. self._class_name(self, self._state_class) .. "' to '" .. self._class_name(self, new_state_class) .. "'")
	end

	self._destroy_current_state(self)

	local init_function = new_state_class.init
	new_state_class.init = function ()
		return 
	end
	self._state = new_state_class.new(new_state_class)

	assert(self._state ~= nil)

	new_state_class.init = init_function
	self._state[self._object_name] = self._object
	self._state_class = new_state_class

	if init_function then
		self._state:init(...)
	end

	return 
end

return 
