CoreEditableGui = CoreEditableGui or class()
CoreEditableGui.init = function (self, unit)
	self._unit = unit
	self._text = self._text or "Default Text"
	self._cull_distance = self._cull_distance or 5000
	self._sides = self._sides or 1
	self._gui_movie = self._gui_movie or "default_text"
	self._gui_object = self._gui_object or "gui_name"
	self._font = self._font or "core/fonts/diesel"
	self._blend_mode = self._blend_mode or "normal"
	self._wrap = self._wrap or false
	self._word_wrap = self._word_wrap or false
	self._render_template = self._render_template or "diffuse_vc_decal"
	self._alpha = self._alpha or 1
	self._shape = {
		self._x or 0,
		self._y or 0,
		self._w or self.width or 1,
		self._h or self.height or 1
	}
	self._vertical = self._vertical or "center"
	self._align = self._align or "left"
	self._gui = World:newgui()
	self._default_font = self._font
	self._guis = {}

	if self._sides == 1 then
		self.add_workspace(self, self._unit:get_object(Idstring(self._gui_object)))
	else
		for i = 1, self._sides, 1 do
			self.add_workspace(self, self._unit:get_object(Idstring(self._gui_object .. i)))
		end
	end

	local text_object = self._guis[1].gui:child("std_text")
	self._font_size = text_object.font_size(text_object)

	self.set_font_size(self, self._font_size)

	self._font_color = Vector3(text_object.color(text_object).red, text_object.color(text_object).green, text_object.color(text_object).blue)

	return 
end
CoreEditableGui.add_workspace = function (self, gui_object)
	local ws = self._gui:create_object_workspace(0, 0, gui_object, Vector3(0, 0, 0))
	local gui = ws.panel(ws):gui(Idstring("core/guis/core_editable_gui"))
	local panel = gui.panel(gui)

	gui.child(gui, "std_text"):set_wrap(self._wrap)
	gui.child(gui, "std_text"):set_word_wrap(self._word_wrap)
	gui.child(gui, "std_text"):set_blend_mode(self._blend_mode)
	gui.child(gui, "std_text"):set_font(Idstring(self._font))
	gui.child(gui, "std_text"):set_text(self._text)
	gui.child(gui, "std_text"):set_render_template(Idstring(self._render_template))
	gui.child(gui, "std_text"):set_align(self._align)
	gui.child(gui, "std_text"):set_vertical(self._vertical)
	gui.child(gui, "std_text"):set_alpha(self._alpha)
	gui.child(gui, "std_text"):set_shape(self._shape[1]*gui.w(gui), self._shape[2]*gui.h(gui), self._shape[3]*gui.w(gui), self._shape[4]*gui.h(gui))
	table.insert(self._guis, {
		workspace = ws,
		gui = gui,
		panel = panel
	})

	return 
end
CoreEditableGui.text = function (self)
	return self._text
end
CoreEditableGui.set_text = function (self, text)
	text = text or ""
	self._text = text

	if managers.localization then
		text = managers.localization:format_text(text)
	end

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_text(text)
	end

	return 
end
CoreEditableGui.default_font = function (self)
	return self._default_font
end
CoreEditableGui.font = function (self)
	return self._font
end
CoreEditableGui.set_font = function (self, font)
	self._font = font or self._font

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_font(Idstring(self._font))
	end

	self.set_font_size(self, self._font_size)

	return 
end
CoreEditableGui.font_size = function (self)
	return self._font_size
end
CoreEditableGui.set_font_size = function (self, font_size)
	self._font_size = font_size or self._font_size

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_font_size(self._font_size*(gui.gui:child("std_text"):height()*10)/100)
	end

	return 
end
CoreEditableGui.font_color = function (self)
	return self._font_color
end
CoreEditableGui.set_font_color = function (self, font_color)
	self._font_color = font_color or self._font_color

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_color(Color(1, self._font_color.x, self._font_color.y, self._font_color.z))
	end

	return 
end
CoreEditableGui.align = function (self)
	return self._align
end
CoreEditableGui.set_align = function (self, align)
	self._align = align or self._align

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_align(self._align)
	end

	return 
end
CoreEditableGui.vertical = function (self)
	return self._vertical
end
CoreEditableGui.set_vertical = function (self, vertical)
	self._vertical = vertical or self._vertical

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_vertical(self._vertical)
	end

	return 
end
CoreEditableGui.set_blend_mode = function (self, blend_mode)
	self._blend_mode = blend_mode or self._blend_mode

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_blend_mode(self._blend_mode)
	end

	return 
end
CoreEditableGui.blend_mode = function (self)
	return self._blend_mode
end
CoreEditableGui.set_render_template = function (self, render_template)
	self._render_template = render_template or self._render_template

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_render_template(Idstring(self._render_template))
	end

	return 
end
CoreEditableGui.render_template = function (self)
	return self._render_template
end
CoreEditableGui.set_wrap = function (self, wrap)
	if wrap == nil then
		return 
	end

	self._wrap = wrap

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_wrap(self._wrap)
	end

	return 
end
CoreEditableGui.wrap = function (self)
	return self._wrap
end
CoreEditableGui.set_word_wrap = function (self, word_wrap)
	if word_wrap == nil then
		return 
	end

	self._word_wrap = word_wrap

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_word_wrap(self._word_wrap)
	end

	return 
end
CoreEditableGui.word_wrap = function (self)
	return self._word_wrap
end
CoreEditableGui.set_alpha = function (self, alpha)
	self._alpha = alpha or self._alpha

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_alpha(self._alpha)
	end

	return 
end
CoreEditableGui.alpha = function (self)
	return self._alpha
end
CoreEditableGui.set_shape = function (self, shape)
	self._shape = shape or self._shape

	for _, gui in ipairs(self._guis) do
		gui.gui:child("std_text"):set_shape(self._shape[1]*gui.gui:w(), self._shape[2]*gui.gui:h(), self._shape[3]*gui.gui:w(), self._shape[4]*gui.gui:h())
	end

	self.set_font_size(self)

	return 
end
CoreEditableGui.shape = function (self)
	return self._shape
end
CoreEditableGui.set_debug = function (self, enabled)
	return 
end
CoreEditableGui.on_unit_set_enabled = function (self, enabled)
	for _, gui in ipairs(self._guis) do
		if enabled then
			gui.workspace:show()
		else
			gui.workspace:hide()
		end
	end

	return 
end
CoreEditableGui.lock_gui = function (self)
	for _, gui in ipairs(self._guis) do
		gui.workspace:set_cull_distance(self._cull_distance)
		gui.workspace:set_frozen(true)
	end

	return 
end
CoreEditableGui.destroy = function (self)
	for _, gui in ipairs(self._guis) do
		if alive(self._gui) and alive(gui.workspace) then
			self._gui:destroy_workspace(gui.workspace)
		end
	end

	self._guis = nil

	return 
end

return 
