CoreHubElement = CoreHubElement or class()
HubElement = HubElement or class(CoreHubElement)
HubElement.init = function (self, ...)
	CoreHubElement.init(self, ...)

	return 
end
CoreHubElement.init = function (self, unit)
	self._unit = unit
	self._hed = self._unit:hub_element_data()
	self._ud = self._unit:unit_data()

	self._unit:anim_play(1)

	self._save_values = {}
	self._mission_trigger_values = {}
	self._update_selected_on = false
	self._parent_panel = managers.editor:hub_element_panel()
	self._parent_sizer = managers.editor:hub_element_sizer()
	self._panels = {}
	self._arrow_brush = Draw:brush()

	self._createicon(self)

	return 
end
CoreHubElement._createicon = function (self)
	local iconsize = 128

	if Global.iconsize then
		iconsize = Global.iconsize
	end

	if self._icon == nil then
		return 
	end

	local root = self._unit:get_object(Idstring("c_hub_element_sphere"))

	if root == nil then
		return 
	end

	if self._iconcolor == nil then
		self._iconcolor = "fff"
	end

	self._icon_gui = World:newgui()

	self._icon_gui:preload("core/guis/core_edit_icon")

	local pos = self._unit:position() - Vector3(iconsize/2, iconsize/2, 0)
	self._icon_ws = self._icon_gui:create_linked_workspace(64, 64, root, pos, Vector3(iconsize, 0, 0), Vector3(0, iconsize, 0))

	self._icon_ws:set_billboard(self._icon_ws.BILLBOARD_BOTH)
	self._icon_ws:panel():gui("core/guis/core_edit_icon")

	self._icon_script = self._icon_ws:panel():gui("core/guis/core_edit_icon"):script()

	self._icon_script:seticon(self._icon, tostring(self._iconcolor))

	return 
end
CoreHubElement._create_panel = function (self)
	if self._panel then
		return 
	end

	self._panel, self._panel_sizer = self._add_panel(self, self._parent_panel, self._parent_sizer)

	return 
end
CoreHubElement._build_panel = function (self)
	self._panel = nil

	return 
end
CoreHubElement.panel = function (self, id, parent, parent_sizer)
	if id then
		if self._panels[id] then
			return self._panels[id]
		end

		local panel, panel_sizer = self._add_panel(self, parent, parent_sizer)

		self._build_panel(self, panel, panel_sizer)

		self._panels[id] = panel

		return self._panels[id]
	end

	if not self._panel then
		self._build_panel(self)
	end

	return self._panel
end
CoreHubElement._add_panel = function (self, parent, parent_sizer)
	local panel = EWS:Panel(parent, "", "TAB_TRAVERSAL")
	local panel_sizer = EWS:BoxSizer("VERTICAL")

	panel.set_sizer(panel, panel_sizer)
	panel_sizer.add(panel_sizer, EWS:StaticText(panel, managers.editor:category_name(self._unit:name()), 0, ""), 0, 0, "ALIGN_CENTER_HORIZONTAL")
	panel_sizer.add(panel_sizer, EWS:StaticLine(panel, "", "LI_HORIZONTAL"), 0, 0, "EXPAND")
	parent_sizer.add(parent_sizer, panel, 1, 0, "EXPAND")
	panel.set_visible(panel, false)
	panel.set_extension(panel, {
		alive = true
	})

	return panel, panel_sizer
end
CoreHubElement.add_help_text = function (self, data)
	if data.panel and data.sizer then
		data.sizer:add(EWS:TextCtrl(data.panel, data.text, 0, "TE_MULTILINE,TE_READONLY,TE_WORDWRAP,TE_CENTRE"), 0, 5, "EXPAND,TOP,BOTTOM")
	end

	return 
end
CoreHubElement.set_element_data = function (self, data)
	if data.callback then
		local he = self._unit:hub_element()

		he[data.callback](he, data.ctrlr, data.params)
	end

	if data.value then
		self._hed[data.value] = data.ctrlr:get_value()
		self._hed[data.value] = tonumber(self._hed[data.value]) or self._hed[data.value]
	end

	return 
end
CoreHubElement.selected = function (self)
	return 
end
CoreHubElement.update_selected = function (self)
	return 
end
CoreHubElement.update_unselected = function (self)
	return 
end
CoreHubElement.begin_editing = function (self)
	return 
end
CoreHubElement.end_editing = function (self)
	return 
end
CoreHubElement.clone_data = function (self)
	return 
end
CoreHubElement.layer_finished = function (self)
	return 
end
CoreHubElement.action_type = function (self)
	return self._action_type or self._type
end
CoreHubElement.trigger_type = function (self)
	return self._trigger_type or self._type
end
CoreHubElement.save_mission_action = function (self, file, t, hub, dont_save_values)
	local type = self.action_type(self)

	if type then
		local ha = hub.hub_element(hub):get_hub_action(self._unit)

		file.puts(file, t .. "<action type=\"" .. type .. "\" name=\"" .. self.name(self) .. "\" mode=\"" .. ha.type .. "\" start_time=\"" .. ha.action_delay .. "\">")

		if not dont_save_values then
			for _, name in ipairs(self._save_values) do
				self.save_value(self, file, t, name)
			end
		end

		file.puts(file, t .. "</action>")
	end

	return 
end
CoreHubElement.save_mission_action_enemy = function (self, file, t, hub)
	local ha = hub.hub_element(hub):get_hub_action(self._unit)
	local pos = self._unit:position()
	local rot = self._unit:rotation()

	file.puts(file, t .. "<action type=\"" .. self.action_type(self) .. "\" name=\"" .. self.name(self) .. "\" mode=\"" .. ha.type .. "\" start_time=\"" .. ha.action_delay .. "\">")

	if ha.type == "" or ha.type == "create" then
		file.puts(file, t .. "\t<enemy name=\"" .. self._hed.enemy_name .. "\">")

		for _, name in ipairs(self._save_values) do
			self.save_value(self, file, t .. "\t", name)
		end

		file.puts(file, t .. "\t</enemy>")
	end

	file.puts(file, t .. "</action>")

	return 
end
CoreHubElement.save_data = function (self, file, t)
	self.save_values(self, file, t)

	return 
end
CoreHubElement.save_values = function (self, file, t)
	t = t .. "\t"

	file.puts(file, t .. "<values>")

	for _, name in ipairs(self._save_values) do
		self.save_value(self, file, t, name)
	end

	file.puts(file, t .. "</values>")

	return 
end
CoreHubElement.save_value = function (self, file, t, name)
	t = t .. "\t"

	file.puts(file, save_value_string(self._hed, name, t, self._unit))

	return 
end
CoreHubElement.save_mission_trigger = function (self, file, t, hub)
	if 0 < #self._mission_trigger_values then
		local type = self.trigger_type(self)

		if type then
			local ht = hub.hub_element(hub):get_hub_trigger(self._unit)

			file.puts(file, t .. "<trigger type=\"" .. type .. "\" name=\"" .. self.name(self) .. "\" mode=\"" .. ht.type .. "\">")

			for _, name in ipairs(self._mission_trigger_values) do
				self.save_value(self, file, t, name)
			end

			file.puts(file, t .. "</trigger>")
		end
	end

	return 
end
CoreHubElement.name = function (self)
	return self._unit:name() .. self._ud.unit_id
end
CoreHubElement.load_data = function (self, data)
	return 
end
CoreHubElement.get_color = function (self, type)
	if type then
		if type == "activate" or type == "enable" then
			return 0, 1, 0
		elseif type == "deactivate" or type == "disable" then
			return 1, 0, 0
		end
	end

	return 0, 1, 0
end
CoreHubElement.draw_connections_selected = function (self)
	for _, hub in ipairs(self._hed.hubs) do
		local r = 1
		local g = 0.6
		local b = 0.2

		self.draw_arrow(self, self._unit, hub, r, g, b, true)
	end

	return 
end
CoreHubElement.draw_connections_unselected = function (self)
	return 
end
CoreHubElement.draw_arrow = function (self, from, to, r, g, b, thick)
	self._arrow_brush:set_color(Color(r, g, b))

	local mul = 1.2
	r = math.clamp(r*mul, 0, 1)
	g = math.clamp(g*mul, 0, 1)
	b = math.clamp(b*mul, 0, 1)
	from = from.position(from)
	to = to.position(to)
	local len = from - to:length()
	local dir = to - from:normalized()
	len = len - 50

	if thick then
		self._arrow_brush:cylinder(from, from + dir*len, 10)
		Application:draw_cylinder(from, from + dir*len, 10, r, g, b)
	else
		Application:draw_line(from, to, r, g, b)
	end

	self._arrow_brush:cone(to, to + from - to:normalized()*150, 48)
	Application:draw_cone(to, to + from - to:normalized()*150, 48, r, g, b)

	return 
end
CoreHubElement.clear = function (self)
	return 
end
CoreHubElement.action_types = function (self)
	return self._action_types
end
CoreHubElement.timeline_color = function (self)
	return self._timeline_color
end
CoreHubElement.add_triggers = function (self)
	return 
end
CoreHubElement.clear_triggers = function (self)
	return 
end
CoreHubElement.widget_affect_object = function (self)
	return nil
end
CoreHubElement.use_widget_position = function (self)
	return nil
end
CoreHubElement.set_enabled = function (self)
	return 
end
CoreHubElement.set_disabled = function (self)
	return 
end
CoreHubElement.set_update_selected_on = function (self, value)
	self._update_selected_on = value

	return 
end
CoreHubElement.update_selected_on = function (self)
	return self._update_selected_on
end
CoreHubElement.destroy = function (self)
	if self._panel then
		self._panel:extension().alive = false

		self._panel:destroy()
	end

	if self._icon_ws then
		self._icon_gui:destroy_workspace(self._icon_ws)

		self._icon_ws = nil
	end

	return 
end

return 
