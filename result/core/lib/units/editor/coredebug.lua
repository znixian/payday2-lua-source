CoreDebugUnitElement = CoreDebugUnitElement or class(MissionElement)
DebugUnitElement = DebugUnitElement or class(CoreDebugUnitElement)
DebugUnitElement.SAVE_UNIT_POSITION = false
DebugUnitElement.SAVE_UNIT_ROTATION = false
DebugUnitElement.init = function (self, ...)
	CoreDebugUnitElement.init(self, ...)

	return 
end
CoreDebugUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.debug_string = "none"
	self._hed.as_subtitle = false
	self._hed.show_instigator = false
	self._hed.color = nil

	table.insert(self._save_values, "debug_string")
	table.insert(self._save_values, "as_subtitle")
	table.insert(self._save_values, "show_instigator")
	table.insert(self._save_values, "color")

	return 
end
CoreDebugUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local debug = EWS:TextCtrl(panel, self._hed.debug_string, "", "TE_PROCESS_ENTER")

	panel_sizer.add(panel_sizer, debug, 0, 0, "EXPAND")
	debug.connect(debug, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		value = "debug_string",
		ctrlr = debug
	})
	debug.connect(debug, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		value = "debug_string",
		ctrlr = debug
	})
	self._build_value_checkbox(self, panel, panel_sizer, "as_subtitle", "Show as subtitle")
	self._build_value_checkbox(self, panel, panel_sizer, "show_instigator", "Show instigator")

	return 
end

return 
