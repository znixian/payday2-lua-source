CoreLogicChanceUnitElement = CoreLogicChanceUnitElement or class(MissionElement)
LogicChanceUnitElement = LogicChanceUnitElement or class(CoreLogicChanceUnitElement)
LogicChanceUnitElement.init = function (self, ...)
	CoreLogicChanceUnitElement.init(self, ...)

	return 
end
CoreLogicChanceUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.chance = 100

	table.insert(self._save_values, "chance")

	return 
end
CoreLogicChanceUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_number(self, panel, panel_sizer, "chance", {
		min = 0,
		floats = 0,
		max = 100
	}, "Specifies chance that this element will call its on executed elements (in percent)")

	return 
end
CoreLogicChanceOperatorUnitElement = CoreLogicChanceOperatorUnitElement or class(MissionElement)
CoreLogicChanceOperatorUnitElement.LINK_ELEMENTS = {
	"elements"
}
LogicChanceOperatorUnitElement = LogicChanceOperatorUnitElement or class(CoreLogicChanceOperatorUnitElement)
LogicChanceOperatorUnitElement.init = function (self, ...)
	LogicChanceOperatorUnitElement.super.init(self, ...)

	return 
end
CoreLogicChanceOperatorUnitElement.init = function (self, unit)
	CoreLogicChanceOperatorUnitElement.super.init(self, unit)

	self._hed.operation = "none"
	self._hed.chance = 0
	self._hed.elements = {}

	table.insert(self._save_values, "operation")
	table.insert(self._save_values, "chance")
	table.insert(self._save_values, "elements")

	return 
end
CoreLogicChanceOperatorUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	CoreLogicChanceOperatorUnitElement.super.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.75,
				b = 0.25,
				r = 0.75,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
CoreLogicChanceOperatorUnitElement.get_links_to_unit = function (self, ...)
	CoreLogicChanceOperatorUnitElement.super.get_links_to_unit(self, ...)
	self._get_links_of_type_from_elements(self, self._hed.elements, "operator", ...)

	return 
end
CoreLogicChanceOperatorUnitElement.update_editing = function (self)
	return 
end
CoreLogicChanceOperatorUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and ray.unit:name() == Idstring("core/units/mission_elements/logic_chance/logic_chance") then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
CoreLogicChanceOperatorUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CoreLogicChanceOperatorUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"logic_chance/logic_chance"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)
	self._build_value_combobox(self, panel, panel_sizer, "operation", {
		"none",
		"add_chance",
		"subtract_chance",
		"reset",
		"set_chance"
	}, "Select an operation for the selected elements")
	self._build_value_number(self, panel, panel_sizer, "chance", {
		min = 0,
		floats = 0,
		max = 100
	}, "Amount of chance to add, subtract or set to the logic chance elements.")
	self._add_help_text(self, "This element can modify logic_chance element. Select logic chance elements to modify using insert and clicking on the elements.")

	return 
end
CoreLogicChanceTriggerUnitElement = CoreLogicChanceTriggerUnitElement or class(MissionElement)
CoreLogicChanceTriggerUnitElement.LINK_ELEMENTS = {
	"elements"
}
LogicChanceTriggerUnitElement = LogicChanceTriggerUnitElement or class(CoreLogicChanceTriggerUnitElement)
LogicChanceTriggerUnitElement.init = function (self, ...)
	LogicChanceTriggerUnitElement.super.init(self, ...)

	return 
end
CoreLogicChanceTriggerUnitElement.init = function (self, unit)
	CoreLogicChanceTriggerUnitElement.super.init(self, unit)

	self._hed.outcome = "fail"
	self._hed.elements = {}

	table.insert(self._save_values, "outcome")
	table.insert(self._save_values, "elements")

	return 
end
CoreLogicChanceTriggerUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	CoreLogicChanceTriggerUnitElement.super.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.85,
				b = 0.25,
				r = 0.85,
				from_unit = unit,
				to_unit = self._unit
			})
		end
	end

	return 
end
CoreLogicChanceTriggerUnitElement.get_links_to_unit = function (self, ...)
	CoreLogicChanceTriggerUnitElement.super.get_links_to_unit(self, ...)
	self._get_links_of_type_from_elements(self, self._hed.elements, "trigger", ...)

	return 
end
CoreLogicChanceTriggerUnitElement.update_editing = function (self)
	return 
end
CoreLogicChanceTriggerUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and ray.unit:name() == Idstring("core/units/mission_elements/logic_chance/logic_chance") then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
CoreLogicChanceTriggerUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CoreLogicChanceTriggerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"logic_chance/logic_chance"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)
	self._build_value_combobox(self, panel, panel_sizer, "outcome", {
		"fail",
		"success"
	}, "Select an outcome to trigger on")
	self._add_help_text(self, "This element is a trigger to logic_chance element.")

	return 
end

return 
