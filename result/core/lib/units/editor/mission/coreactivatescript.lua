CoreActivateScriptUnitElement = CoreActivateScriptUnitElement or class(MissionElement)
ActivateScriptUnitElement = ActivateScriptUnitElement or class(CoreActivateScriptUnitElement)
ActivateScriptUnitElement.init = function (self, ...)
	CoreActivateScriptUnitElement.init(self, ...)

	return 
end
CoreActivateScriptUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.activate_script = "none"

	table.insert(self._save_values, "activate_script")

	return 
end
CoreActivateScriptUnitElement.selected = function (self)
	MissionElement.selected(self)
	CoreEWS.update_combobox_options(self._script_params, self._scripts(self))

	if self._hed.activate_script ~= "none" and not table.contains(self._scripts(self), self._hed.activate_script) then
		self._hed.activate_script = "none"
	end

	CoreEWS.change_combobox_value(self._script_params, self._hed.activate_script)

	return 
end
CoreActivateScriptUnitElement._scripts = function (self)
	return managers.editor:layer("Mission"):script_names()
end
CoreActivateScriptUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	self._script_params = {
		default = "none",
		name = "Script:",
		ctrlr_proportions = 2,
		name_proportions = 1,
		tooltip = "Select a script from the combobox",
		sorted = true,
		panel = panel,
		sizer = panel_sizer,
		options = self._scripts(self),
		value = self._hed.activate_script
	}
	local scripts = CoreEWS.combobox(self._script_params)

	scripts.connect(scripts, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		value = "activate_script",
		ctrlr = scripts
	})

	return 
end

return 
