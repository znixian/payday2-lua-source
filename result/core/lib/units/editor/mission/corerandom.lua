CoreRandomUnitElement = CoreRandomUnitElement or class(MissionElement)
CoreRandomUnitElement.SAVE_UNIT_POSITION = false
CoreRandomUnitElement.SAVE_UNIT_ROTATION = false
RandomUnitElement = RandomUnitElement or class(CoreRandomUnitElement)
RandomUnitElement.init = function (self, ...)
	CoreRandomUnitElement.init(self, ...)

	return 
end
CoreRandomUnitElement.init = function (self, unit)
	CoreRandomUnitElement.super.init(self, unit)

	self._hed.amount = 1
	self._hed.amount_random = 0
	self._hed.ignore_disabled = true
	self._hed.counter_id = nil

	table.insert(self._save_values, "amount")
	table.insert(self._save_values, "amount_random")
	table.insert(self._save_values, "ignore_disabled")
	table.insert(self._save_values, "counter_id")

	return 
end
CoreRandomUnitElement.update_editing = function (self)
	return 
end
CoreRandomUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	CoreRandomUnitElement.super.draw_links(self, t, dt, selected_unit)

	if self._hed.counter_id then
		local unit = all_units[self._hed.counter_id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.85,
				b = 0.25,
				r = 0.85,
				from_unit = unit,
				to_unit = self._unit
			})
		end
	end

	return 
end
CoreRandomUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and ray.unit:name() == Idstring("core/units/mission_elements/logic_counter/logic_counter") then
		local id = ray.unit:unit_data().unit_id

		if self._hed.counter_id == id then
			self._hed.counter_id = nil
		else
			self._hed.counter_id = id
		end
	end

	return 
end
CoreRandomUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CoreRandomUnitElement.remove_links = function (self, unit)
	if self._hed.counter_id and self._hed.counter_id == unit.unit_data(unit).unit_id then
		self._hed.counter_id = nil
	end

	return 
end
CoreRandomUnitElement._add_counter_filter = function (self, unit)
	return unit.name(unit) == Idstring("core/units/mission_elements/logic_counter/logic_counter")
end
CoreRandomUnitElement._set_counter_id = function (self, unit)
	self._hed.counter_id = unit.unit_data(unit).unit_id

	return 
end
CoreRandomUnitElement._remove_counter_filter = function (self, unit)
	return self._hed.counter_id == unit.unit_data(unit).unit_id
end
CoreRandomUnitElement._remove_counter_id = function (self, unit)
	self._hed.counter_id = nil

	return 
end
CoreRandomUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_add_remove_static_unit_from_list(self, panel, panel_sizer, {
		single = true,
		add_filter = callback(self, self, "_add_counter_filter"),
		add_result = callback(self, self, "_set_counter_id"),
		remove_filter = callback(self, self, "_remove_counter_filter"),
		remove_result = callback(self, self, "_remove_counter_id")
	})
	self._build_value_number(self, panel, panel_sizer, "amount", {
		floats = 0,
		min = 1
	}, "Specifies the amount of elements to be executed")
	self._build_value_number(self, panel, panel_sizer, "amount_random", {
		floats = 0,
		min = 0
	}, "Add a random amount to amount")
	self._build_value_checkbox(self, panel, panel_sizer, "ignore_disabled")
	self._add_help_text(self, "Use 'Amount' only to specify an exact amount of elements to execute. Use 'Amount Random' to add a random amount to 'Amount' ('Amount' + random('Amount Random').")

	return 
end

return 
