CorePlaySoundUnitElement = CorePlaySoundUnitElement or class(MissionElement)
CorePlaySoundUnitElement.LINK_ELEMENTS = {
	"elements"
}
PlaySoundUnitElement = PlaySoundUnitElement or class(CorePlaySoundUnitElement)
PlaySoundUnitElement.init = function (self, ...)
	PlaySoundUnitElement.super.init(self, ...)

	return 
end
CorePlaySoundUnitElement.init = function (self, unit)
	CorePlaySoundUnitElement.super.init(self, unit)

	self._hed.elements = {}
	self._hed.append_prefix = false
	self._hed.use_instigator = false
	self._hed.interrupt = true

	table.insert(self._save_values, "sound_event")
	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "append_prefix")
	table.insert(self._save_values, "use_instigator")
	table.insert(self._save_values, "interrupt")

	return 
end
CorePlaySoundUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	MissionElement.draw_links(self, t, dt, selected_unit, all_units)

	return 
end
CorePlaySoundUnitElement.update_editing = function (self)
	return 
end
CorePlaySoundUnitElement.update_selected = function (self, t, dt, selected_unit, all_units)
	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0,
				b = 0,
				r = 0.75,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
CorePlaySoundUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and (string.find(ray.unit:name():s(), "ai_spawn_enemy", 1, true) or string.find(ray.unit:name():s(), "ai_spawn_civilian", 1, true)) then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
CorePlaySoundUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CorePlaySoundUnitElement.post_init = function (self, ...)
	CorePlaySoundUnitElement.super.post_init(self, ...)
	self._add_soundbank(self)

	return 
end
CorePlaySoundUnitElement.test_element = function (self)
	if self._hed.sound_event then
		managers.editor:set_wanted_mute(false)
		managers.editor:set_listener_enabled(true)

		if self._ss then
			self._ss:stop()
		end

		self._ss = SoundDevice:create_source(self._unit:unit_data().name_id)

		self._ss:set_position(self._unit:position())
		self._ss:set_orientation(self._unit:rotation())
		self._ss:post_event(self._hed.sound_event)
	end

	return 
end
CorePlaySoundUnitElement.stop_test_element = function (self)
	managers.editor:set_wanted_mute(true)
	managers.editor:set_listener_enabled(false)

	if self._ss then
		self._ss:stop()
	end

	return 
end
CorePlaySoundUnitElement.set_category = function (self, params)
	local value = params.value

	CoreEws.update_combobox_options(self._sound_params, managers.sound_environment:scene_events(value))
	CoreEws.change_combobox_value(self._sound_params, managers.sound_environment:scene_events(value)[1])

	self._hed.sound_event = self._sound_params.value

	self._add_soundbank(self)

	return 
end
CorePlaySoundUnitElement._add_soundbank = function (self)
	self.stop_test_element(self)
	managers.sound_environment:add_soundbank(managers.sound_environment:scene_soundbank(self._hed.sound_event))

	return 
end
CorePlaySoundUnitElement.set_element_data = function (self, params, ...)
	CorePlaySoundUnitElement.super.set_element_data(self, params, ...)

	if params.value == "sound_event" then
		self._add_soundbank(self)
	end

	return 
end
CorePlaySoundUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = {
		"ai_spawn_enemy",
		"ai_spawn_civilian"
	}

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)

	local paths = managers.sound_environment:scene_paths()

	if #paths <= 0 then
		local help = {
			panel = panel,
			sizer = panel_sizer,
			text = "No scene sounds available in project!"
		}

		self.add_help_text(self, help)

		return 
	end

	self._hed.sound_event = self._hed.sound_event or managers.sound_environment:scene_events(paths[1])[1]

	self._add_soundbank(self)

	local path_value = managers.sound_environment:scene_path(self._hed.sound_event)
	local _, _ = CoreEws.combobox_and_list({
		name = "Category:",
		panel = panel,
		sizer = panel_sizer,
		options = paths,
		value = path_value,
		value_changed_cb = callback(self, self, "set_category")
	})
	local _, sound_params = self._build_value_combobox(self, panel, panel_sizer, "sound_event", managers.sound_environment:scene_events(path_value), "Select a sound event")
	self._sound_params = sound_params

	self._build_value_checkbox(self, panel, panel_sizer, "append_prefix", "Append unit prefix")
	self._build_value_checkbox(self, panel, panel_sizer, "use_instigator", "Play on instigator")
	self._build_value_checkbox(self, panel, panel_sizer, "interrupt", "Interrupt existing sound")

	return 
end
CorePlaySoundUnitElement.add_to_mission_package = function (self)
	managers.editor:add_to_sound_package({
		category = "soundbanks",
		name = managers.sound_environment:scene_soundbank(self._hed.sound_event)
	})

	return 
end
CorePlaySoundUnitElement.destroy = function (self)
	self.stop_test_element(self)
	CorePlaySoundUnitElement.super.destroy(self)

	return 
end

return 
