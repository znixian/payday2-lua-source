CoreWorldCameraUnitElement = CoreWorldCameraUnitElement or class(MissionElement)
WorldCameraUnitElement = WorldCameraUnitElement or class(CoreWorldCameraUnitElement)
WorldCameraUnitElement.init = function (self, ...)
	CoreWorldCameraUnitElement.init(self, ...)

	return 
end
CoreWorldCameraUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.worldcamera = "none"
	self._hed.worldcamera_sequence = "none"

	table.insert(self._save_values, "worldcamera")
	table.insert(self._save_values, "worldcamera_sequence")

	return 
end
CoreWorldCameraUnitElement.test_element = function (self)
	if self._hed.worldcamera_sequence ~= "none" then
		managers.worldcamera:play_world_camera_sequence(self._hed.worldcamera_sequence)
	elseif self._hed.worldcamera ~= "none" then
		managers.worldcamera:play_world_camera(self._hed.worldcamera)
	end

	return 
end
CoreWorldCameraUnitElement.selected = function (self)
	MissionElement.selected(self)
	self._populate_worldcameras(self)

	if not managers.worldcamera:all_world_cameras()[self._hed.worldcamera] then
		self._hed.worldcamera = "none"

		self._worldcameras:set_value(self._hed.worldcamera)
	end

	self._populate_sequences(self)

	if not managers.worldcamera:all_world_camera_sequences()[self._hed.worldcamera_sequence] then
		self._hed.worldcamera_sequence = "none"

		self._sequences:set_value(self._hed.worldcamera_sequence)
	end

	return 
end
CoreWorldCameraUnitElement._populate_worldcameras = function (self)
	self._worldcameras:clear()
	self._worldcameras:append("none")

	for _, name in ipairs(self._sorted_worldcameras(self)) do
		self._worldcameras:append(name)
	end

	self._worldcameras:set_value(self._hed.worldcamera)

	return 
end
CoreWorldCameraUnitElement._populate_sequences = function (self)
	self._sequences:clear()
	self._sequences:append("none")

	for _, name in ipairs(self._sorted_worldcamera_sequences(self)) do
		self._sequences:append(name)
	end

	self._sequences:set_value(self._hed.worldcamera_sequence)

	return 
end
CoreWorldCameraUnitElement._sorted_worldcameras = function (self)
	local t = {}

	for name, _ in pairs(managers.worldcamera:all_world_cameras()) do
		table.insert(t, name)
	end

	table.sort(t)

	return t
end
CoreWorldCameraUnitElement._sorted_worldcamera_sequences = function (self)
	local t = {}

	for name, _ in pairs(managers.worldcamera:all_world_camera_sequences()) do
		table.insert(t, name)
	end

	table.sort(t)

	return t
end
CoreWorldCameraUnitElement.select_camera_btn = function (self)
	local dialog = SelectNameModal:new("Select camera", self._sorted_worldcameras(self))

	if dialog.cancelled(dialog) then
		return 
	end

	for _, worldcamera in ipairs(dialog._selected_item_assets(dialog)) do
		self._hed.worldcamera = worldcamera

		self._worldcameras:set_value(self._hed.worldcamera)
	end

	return 
end
CoreWorldCameraUnitElement.select_sequence_btn = function (self)
	local dialog = SelectNameModal:new("Select sequence", self._sorted_worldcamera_sequences(self))

	if dialog.cancelled(dialog) then
		return 
	end

	for _, worldcamera_sequence in ipairs(dialog._selected_item_assets(dialog)) do
		self._hed.worldcamera_sequence = worldcamera_sequence

		self._sequences:set_value(self._hed.worldcamera_sequence)
	end

	return 
end
CoreWorldCameraUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local sequence_sizer = EWS:BoxSizer("HORIZONTAL")

	sequence_sizer.add(sequence_sizer, EWS:StaticText(self._panel, "Sequence:", 0, ""), 1, 0, "ALIGN_CENTER_VERTICAL")

	self._sequences = EWS:ComboBox(self._panel, "", "", "CB_DROPDOWN,CB_READONLY")

	self._populate_sequences(self)
	self._sequences:set_value(self._hed.worldcamera_sequence)
	self._sequences:connect("EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		value = "worldcamera_sequence",
		ctrlr = self._sequences
	})
	sequence_sizer.add(sequence_sizer, self._sequences, 3, 0, "EXPAND")

	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "SELECT_EFFECT", "Select sequence", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "SELECT_EFFECT", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "select_sequence_btn"), nil)
	toolbar.realize(toolbar)
	sequence_sizer.add(sequence_sizer, toolbar, 0, 1, "EXPAND,LEFT")
	self._panel_sizer:add(sequence_sizer, 0, 0, "EXPAND")

	local worldcamera_sizer = EWS:BoxSizer("HORIZONTAL")

	worldcamera_sizer.add(worldcamera_sizer, EWS:StaticText(self._panel, "Camera:", 0, ""), 1, 0, "ALIGN_CENTER_VERTICAL")

	self._worldcameras = EWS:ComboBox(self._panel, "", "", "CB_DROPDOWN,CB_READONLY")

	self._populate_worldcameras(self)
	self._worldcameras:set_value(self._hed.worldcamera)
	self._worldcameras:connect("EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		value = "worldcamera",
		ctrlr = self._worldcameras
	})
	worldcamera_sizer.add(worldcamera_sizer, self._worldcameras, 3, 0, "EXPAND")

	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "SELECT_EFFECT", "Select camera", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "SELECT_EFFECT", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "select_camera_btn"), nil)
	toolbar.realize(toolbar)
	worldcamera_sizer.add(worldcamera_sizer, toolbar, 0, 1, "EXPAND,LEFT")
	self._panel_sizer:add(worldcamera_sizer, 0, 0, "EXPAND")

	return 
end

return 
