CoreExecuteInOtherMissionUnitElement = CoreExecuteInOtherMissionUnitElement or class(MissionElement)
ExecuteInOtherMissionUnitElement = ExecuteInOtherMissionUnitElement or class(CoreExecuteInOtherMissionUnitElement)
ExecuteInOtherMissionUnitElement.init = function (self, ...)
	CoreExecuteInOtherMissionUnitElement.init(self, ...)

	return 
end
CoreExecuteInOtherMissionUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	return 
end
CoreExecuteInOtherMissionUnitElement.selected = function (self)
	MissionElement.selected(self)

	return 
end
CoreExecuteInOtherMissionUnitElement.add_unit_list_btn = function (self)
	local function f(unit)
		return unit.type(unit) == Idstring("mission_element") and unit ~= self._unit
	end

	local dialog = SelectUnitByNameModal:new("Add other mission unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		self.add_on_executed(self, unit)
	end

	return 
end
CoreExecuteInOtherMissionUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	self._btn_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._btn_toolbar:add_tool("ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	self._btn_toolbar:connect("ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "add_unit_list_btn"), nil)
	self._btn_toolbar:realize()
	panel_sizer.add(panel_sizer, self._btn_toolbar, 0, 1, "EXPAND,LEFT")

	return 
end

return 
