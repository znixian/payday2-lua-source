core:import("CoreShapeManager")

CoreShapeUnitElement = CoreShapeUnitElement or class(MissionElement)
ShapeUnitElement = ShapeUnitElement or class(CoreShapeUnitElement)
ShapeUnitElement.init = function (self, ...)
	CoreShapeUnitElement.init(self, ...)

	return 
end
CoreShapeUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._timeline_color = Vector3(1, 1, 0)
	self._brush = Draw:brush()
	self._hed.trigger_times = 0
	self._hed.shape_type = "box"
	self._hed.width = 500
	self._hed.depth = 500
	self._hed.height = 500
	self._hed.radius = 250

	table.insert(self._save_values, "shape_type")
	table.insert(self._save_values, "width")
	table.insert(self._save_values, "depth")
	table.insert(self._save_values, "height")
	table.insert(self._save_values, "radius")

	return 
end
CoreShapeUnitElement.update_selected = function (self, t, dt, selected_unit, all_units)
	local shape = self.get_shape(self)

	if shape then
		shape.draw(shape, t, dt, 1, 1, 1)
	end

	return 
end
CoreShapeUnitElement.get_shape = function (self)
	if not self._shape then
		self._create_shapes(self)
	end

	return (self._hed.shape_type == "box" and self._shape) or (self._hed.shape_type == "cylinder" and self._cylinder_shape)
end
CoreShapeUnitElement.set_shape_property = function (self, params)
	self._shape:set_property(params.property, self._hed[params.value])
	self._cylinder_shape:set_property(params.property, self._hed[params.value])

	return 
end
CoreShapeUnitElement._set_shape_type = function (self)
	local is_box = self._hed.shape_type == "box"
	local is_cylinder = self._hed.shape_type == "cylinder"

	self._depth_params.number_ctrlr:set_enabled(is_box)
	self._width_params.number_ctrlr:set_enabled(is_box)
	self._height_params.number_ctrlr:set_enabled(is_box or is_cylinder)
	self._radius_params.number_ctrlr:set_enabled(is_cylinder)
	self._sliders.depth:set_enabled(is_box)
	self._sliders.width:set_enabled(is_box)
	self._sliders.height:set_enabled(is_box or is_cylinder)
	self._sliders.radius:set_enabled(is_cylinder)

	return 
end
CoreShapeUnitElement._create_shapes = function (self)
	self._shape = CoreShapeManager.ShapeBoxMiddle:new({
		width = self._hed.width,
		depth = self._hed.depth,
		height = self._hed.height
	})

	self._shape:set_unit(self._unit)

	self._cylinder_shape = CoreShapeManager.ShapeCylinderMiddle:new({
		radius = self._hed.radius,
		height = self._hed.height
	})

	self._cylinder_shape:set_unit(self._unit)

	return 
end
CoreShapeUnitElement.set_element_data = function (self, params, ...)
	CoreShapeUnitElement.super.set_element_data(self, params, ...)

	if params.value == "shape_type" then
		self._set_shape_type(self)
	end

	return 
end
CoreShapeUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "shape_type", {
		"box",
		"cylinder"
	}, "Select shape for area")

	if not self._shape then
		self._create_shapes(self)
	end

	local width, width_params = self._build_value_number(self, panel, panel_sizer, "width", {
		floats = 0,
		min = 0
	}, "Set the width for the shape")

	width_params.name_ctrlr:set_label("Width[cm]:")

	self._width_params = width_params

	width.connect(width, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_shape_property"), {
		value = "width",
		property = "width"
	})
	width.connect(width, "EVT_KILL_FOCUS", callback(self, self, "set_shape_property"), {
		value = "width",
		property = "width"
	})

	local depth, depth_params = self._build_value_number(self, panel, panel_sizer, "depth", {
		floats = 0,
		min = 0
	}, "Set the depth for the shape")

	depth_params.name_ctrlr:set_label("Depth[cm]:")

	self._depth_params = depth_params

	depth.connect(depth, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_shape_property"), {
		value = "depth",
		property = "depth"
	})
	depth.connect(depth, "EVT_KILL_FOCUS", callback(self, self, "set_shape_property"), {
		value = "depth",
		property = "depth"
	})

	local height, height_params = self._build_value_number(self, panel, panel_sizer, "height", {
		floats = 0,
		min = 0
	}, "Set the height for the shape")

	height_params.name_ctrlr:set_label("Height[cm]:")

	self._height_params = height_params

	height.connect(height, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_shape_property"), {
		value = "height",
		property = "height"
	})
	height.connect(height, "EVT_KILL_FOCUS", callback(self, self, "set_shape_property"), {
		value = "height",
		property = "height"
	})

	local radius, radius_params = self._build_value_number(self, panel, panel_sizer, "radius", {
		floats = 0,
		min = 0
	}, "Set the radius for the shape")

	radius_params.name_ctrlr:set_label("Radius[cm]:")

	self._radius_params = radius_params

	radius.connect(radius, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_shape_property"), {
		value = "radius",
		property = "radius"
	})
	radius.connect(radius, "EVT_KILL_FOCUS", callback(self, self, "set_shape_property"), {
		value = "radius",
		property = "radius"
	})
	self.scale_slider(self, panel, panel_sizer, width_params, "width", "Width scale:")
	self.scale_slider(self, panel, panel_sizer, depth_params, "depth", "Depth scale:")
	self.scale_slider(self, panel, panel_sizer, height_params, "height", "Height scale:")
	self.scale_slider(self, panel, panel_sizer, radius_params, "radius", "Radius scale:")
	self._set_shape_type(self)

	return 
end
CoreShapeUnitElement.scale_slider = function (self, panel, sizer, number_ctrlr_params, value, name)
	local slider_sizer = EWS:BoxSizer("HORIZONTAL")

	slider_sizer.add(slider_sizer, EWS:StaticText(panel, name, "", "ALIGN_LEFT"), 1, 0, "ALIGN_CENTER_VERTICAL")

	local slider = EWS:Slider(panel, 100, 1, 200, "", "")

	slider_sizer.add(slider_sizer, slider, 2, 0, "EXPAND")
	slider.connect(slider, "EVT_SCROLL_CHANGED", callback(self, self, "set_size"), {
		ctrlr = slider,
		number_ctrlr_params = number_ctrlr_params,
		value = value
	})
	slider.connect(slider, "EVT_SCROLL_THUMBTRACK", callback(self, self, "set_size"), {
		ctrlr = slider,
		number_ctrlr_params = number_ctrlr_params,
		value = value
	})
	slider.connect(slider, "EVT_SCROLL_CHANGED", callback(self, self, "size_release"), {
		ctrlr = slider,
		number_ctrlr_params = number_ctrlr_params,
		value = value
	})
	slider.connect(slider, "EVT_SCROLL_THUMBRELEASE", callback(self, self, "size_release"), {
		ctrlr = slider,
		number_ctrlr_params = number_ctrlr_params,
		value = value
	})
	sizer.add(sizer, slider_sizer, 0, 0, "EXPAND")

	self._sliders = self._sliders or {}
	self._sliders[value] = slider

	return 
end
CoreShapeUnitElement.set_size = function (self, params)
	local value = (self._hed[params.value]*params.ctrlr:get_value())/100

	self._shape:set_property(params.value, value)
	self._cylinder_shape:set_property(params.value, value)
	CoreEWS.change_entered_number(params.number_ctrlr_params, value)

	return 
end
CoreShapeUnitElement.size_release = function (self, params)
	self._hed[params.value] = params.number_ctrlr_params.value

	params.ctrlr:set_value(100)

	return 
end

return 
