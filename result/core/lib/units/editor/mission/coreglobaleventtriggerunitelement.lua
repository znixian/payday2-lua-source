CoreGlobalEventTriggerUnitElement = CoreGlobalEventTriggerUnitElement or class(MissionElement)
GlobalEventTriggerUnitElement = GlobalEventTriggerUnitElement or class(CoreGlobalEventTriggerUnitElement)
GlobalEventTriggerUnitElement.init = function (self, ...)
	GlobalEventTriggerUnitElement.super.init(self, ...)

	return 
end
CoreGlobalEventTriggerUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.trigger_times = 1
	self._hed.global_event = "none"

	table.insert(self._save_values, "global_event")

	return 
end
CoreGlobalEventTriggerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "global_event", table.list_add({
		"none"
	}, managers.mission:get_global_event_list()), "Select a global event from the combobox")

	return 
end

return 
