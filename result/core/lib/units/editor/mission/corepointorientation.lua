CorePointOrientationUnitElement = CorePointOrientationUnitElement or class(MissionElement)
PointOrientationUnitElement = PointOrientationUnitElement or class(CorePointOrientationUnitElement)
PointOrientationUnitElement.init = function (self, ...)
	PointOrientationUnitElement.super.init(self, ...)

	return 
end
CorePointOrientationUnitElement.init = function (self, unit)
	CorePointOrientationUnitElement.super.init(self, unit)

	return 
end
CorePointOrientationUnitElement.update_selected = function (self, ...)
	CorePointOrientationUnitElement.super.update_selected(self, ...)
	self._draw_orientation(self)

	return 
end
CorePointOrientationUnitElement.update_unselected = function (self, ...)
	CorePointOrientationUnitElement.super.update_unselected(self, ...)
	self._draw_orientation(self)

	return 
end
CorePointOrientationUnitElement._draw_orientation = function (self)
	local len = 25
	local scale = 0.05

	Application:draw_arrow(self._unit:position(), self._unit:position() + self._unit:rotation():x()*len, 1, 0, 0, scale)
	Application:draw_arrow(self._unit:position(), self._unit:position() + self._unit:rotation():y()*len, 0, 1, 0, scale)
	Application:draw_arrow(self._unit:position(), self._unit:position() + self._unit:rotation():z()*len, 0, 0, 1, scale)

	return 
end

return 
