CoreMotionPathOperatorUnitElement = CoreMotionPathOperatorUnitElement or class(MissionElement)
CoreMotionPathOperatorUnitElement.LINK_ELEMENTS = {
	"elements"
}
MotionPathOperatorUnitElement = MotionPathOperatorUnitElement or class(CoreMotionPathOperatorUnitElement)
MotionPathOperatorUnitElement.init = function (self, ...)
	MotionPathOperatorUnitElement.super.init(self, ...)

	return 
end
CoreMotionPathOperatorUnitElement.init = function (self, unit)
	CoreMotionPathOperatorUnitElement.super.init(self, unit)

	self._hed.operation = "none"
	self._hed.marker = nil
	self._hed.elements = {}
	self._hed.marker_ids = {}

	table.insert(self._save_values, "operation")
	table.insert(self._save_values, "marker")
	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "marker_ids")

	return 
end
CoreMotionPathOperatorUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	CoreMotionPathOperatorUnitElement.super.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.25,
				b = 0.25,
				r = 0.75,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	if self._hed.marker then
		local unit = all_units[self._hed.marker_ids[self._hed.marker]]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw and alive(unit) and alive(self._unit) then
			self._draw_link(self, {
				g = 0.55,
				b = 0.05,
				r = 0.95,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
CoreMotionPathOperatorUnitElement.get_links_to_unit = function (self, ...)
	CoreMotionPathOperatorUnitElement.super.get_links_to_unit(self, ...)
	self._get_links_of_type_from_elements(self, self._hed.elements, "operator", ...)

	return 
end
CoreMotionPathOperatorUnitElement.update_editing = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		sample = true,
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit then
		Application:draw(ray.unit, 0, 1, 0)
	end

	return 
end
CoreMotionPathOperatorUnitElement.update_selected = function (self)
	Application:draw_cone(self._unit:position(), self._unit:position() + self._unit:rotation():y()*75, 35, 1, 1, 1)

	return 
end
CoreMotionPathOperatorUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit and ray.unit:name() == Idstring("units/dev_tools/mission_elements/motion_path_marker/motion_path_marker") then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
CoreMotionPathOperatorUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CoreMotionPathOperatorUnitElement._motion_path_markers = function (self)
	self._hed.marker_ids = {}
	local motion_path_markers = {
		"none"
	}
	local mission_elements = managers.worlddefinition._mission_element_units

	for _, me in pairs(mission_elements) do
		if alive(me) and me.name(me) == Idstring("units/dev_tools/mission_elements/motion_path_marker/motion_path_marker") then
			table.insert_sorted(motion_path_markers, me.unit_data(me).name_id)

			self._hed.marker_ids[me.unit_data(me).name_id] = me.unit_data(me).unit_id
		end
	end

	return motion_path_markers
end
CoreMotionPathOperatorUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "operation", {
		"none",
		"goto_marker",
		"teleport",
		"move",
		"wait",
		"rotate",
		"activate_bridge",
		"deactivate_bridge"
	}, "Select an operation for the selected elements.")

	local markers_combo = self._build_value_combobox(self, panel, panel_sizer, "marker", self._motion_path_markers(self), "Select motion path marker.")

	Application:debug("CoreMotionPathOperatorUnitElement:_build_panel( panel, panel_sizer ): ", markers_combo)
	markers_combo.connect(markers_combo, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "on_executed_marker_selected"), nil)
	self._add_help_text(self, "This element can modify motion path marker elements. Select motion path marker elements to modify using insert and clicking on the elements.")

	return 
end
CoreMotionPathOperatorUnitElement.on_executed_marker_selected = function (self)
	Application:debug("CoreMotionPathOperatorUnitElement:_build_panel( panel, panel_sizer ): ", self._hed.marker, self._hed.marker_ids[self._hed.marker])

	if self._hed.marker == "none" then
		self._hed.marker = nil
	end

	return 
end
CoreMotionPathTriggerUnitElement = CoreMotionPathTriggerUnitElement or class(MissionElement)
CoreMotionPathTriggerUnitElement.LINK_ELEMENTS = {
	"elements"
}
MotionPathTriggerUnitElement = MotionPathTriggerUnitElement or class(CoreMotionPathTriggerUnitElement)
MotionPathTriggerUnitElement.init = function (self, ...)
	MotionPathTriggerUnitElement.super.init(self, ...)

	return 
end
CoreMotionPathTriggerUnitElement.init = function (self, unit)
	CoreMotionPathTriggerUnitElement.super.init(self, unit)

	self._hed.outcome = "marker_reached"
	self._hed.elements = {}

	table.insert(self._save_values, "outcome")
	table.insert(self._save_values, "elements")

	return 
end
CoreMotionPathTriggerUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	CoreMotionPathTriggerUnitElement.super.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0.85,
				b = 0.25,
				r = 0.85,
				from_unit = unit,
				to_unit = self._unit
			})
		end
	end

	return 
end
CoreMotionPathTriggerUnitElement.get_links_to_unit = function (self, ...)
	CoreMotionPathTriggerUnitElement.super.get_links_to_unit(self, ...)
	self._get_links_of_type_from_elements(self, self._hed.elements, "trigger", ...)

	return 
end
CoreMotionPathTriggerUnitElement.update_editing = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		sample = true,
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit then
		Application:draw(ray.unit, 0, 1, 0)
	end

	return 
end
CoreMotionPathTriggerUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "body editor",
		mask = managers.slot:get_mask("all")
	})

	if ray and ray.unit and ray.unit:name() == Idstring("units/dev_tools/mission_elements/motion_path_marker/motion_path_marker") then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
CoreMotionPathTriggerUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CoreMotionPathTriggerUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer

	self._build_value_combobox(self, panel, panel_sizer, "outcome", {
		"marker_reached"
	}, "Select an outcome to trigger on")
	self._add_help_text(self, "This element is a trigger on motion path marker element.")

	return 
end

return 
