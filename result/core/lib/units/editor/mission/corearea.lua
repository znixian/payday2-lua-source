CoreAreaUnitElement = CoreAreaUnitElement or class(MissionElement)
AreaUnitElement = AreaUnitElement or class(CoreAreaUnitElement)
AreaUnitElement.init = function (self, ...)
	CoreAreaUnitElement.init(self, ...)

	return 
end
CoreAreaUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._timeline_color = Vector3(1, 1, 0)
	self._shapes_types = {
		"box",
		"sphere",
		"plane"
	}
	self._shape_type = "box"
	self._grid_size = 1
	self._kb_move_grid_size = 25
	self._move_shape_rep = RepKey:new({
		"up",
		"down",
		"right",
		"left",
		"num 4",
		"num 6"
	})
	self._brush = Draw:brush()
	self._hed.area_interval = managers.area:default_interval()
	self._hed.area_type = "on_enter"
	self._hed.shapes = {}

	table.insert(self._save_values, "area_interval")
	table.insert(self._save_values, "area_type")
	table.insert(self._save_values, "shapes")
	table.insert(self._hed.action_types, "activate")
	table.insert(self._hed.action_types, "deactivate")

	return 
end
CoreAreaUnitElement.populate_shapes_list = function (self)
	self._shapes_list:clear()

	for name, shape in pairs(self._hed.shapes) do
		self._shapes_list:append(name)
	end

	return 
end
CoreAreaUnitElement.set_shape_type = function (self, types)
	self._shape_type = types.get_value(types)

	return 
end
CoreAreaUnitElement.selected_shape = function (self, shapes)
	local i = shapes.selected_index(shapes)

	if -1 < i then
		self._current_shape = shapes.get_string(shapes, i)

		self.set_shape_values(self)
	end

	return 
end
CoreAreaUnitElement.set_start_altitude = function (self)
	if self._current_shape then
		self._hed.shapes[self._current_shape].position = self._hed.shapes[self._current_shape].position:with_z(self._start_altitude:get_value())
	end

	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement.set_altitude_text = function (self, data)
	if not self._current_shape then
		return 
	end

	local value = tonumber(data.ctrl:get_value())

	if data.type == "start" then
		self._hed.shapes[self._current_shape].position = self._hed.shapes[self._current_shape].position:with_z(value)
	elseif data.type == "height" then
		self._hed.shapes[self._current_shape].height = value
	end

	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement.set_altitude_spin = function (self, data)
	if not self._current_shape then
		return 
	end

	if data.type == "start" then
		local z = self._hed.shapes[self._current_shape].position.z + data.step
		self._hed.shapes[self._current_shape].position = self._hed.shapes[self._current_shape].position:with_z(z)
	elseif data.type == "height" then
		self._hed.shapes[self._current_shape].height = self._hed.shapes[self._current_shape].height + data.step
	end

	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement.set_height = function (self)
	if self._current_shape then
		self._hed.shapes[self._current_shape].height = self._height:get_value()
	end

	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement.set_2d = function (self)
	if self._current_shape then
		self._hed.shapes[self._current_shape].position = self._hed.shapes[self._current_shape].position:with_z(0)
		self._hed.shapes[self._current_shape].height = 0
	end

	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement.set_size = function (self)
	if self._current_shape then
		self._hed.shapes[self._current_shape].size_mul = math.pow(self._size:get_value()/10, 2)
	end

	return 
end
CoreAreaUnitElement.size_release = function (self)
	local current_shape = self._hed.shapes[self._current_shape]

	if current_shape then
		if current_shape.type == "sphere" then
			current_shape.radious = current_shape.radious*current_shape.size_mul
		elseif current_shape.type == "box" then
			current_shape.width = current_shape.width*current_shape.size_mul
			current_shape.length = current_shape.length*current_shape.size_mul
		elseif current_shape.type == "plane" then
			current_shape.width = current_shape.width*current_shape.size_mul
		end

		current_shape.size_mul = 1

		self._size:set_value(10)
	end

	return 
end
CoreAreaUnitElement.set_shape_values = function (self)
	local current_shape = self._hed.shapes[self._current_shape]

	if current_shape then
		self._start_altitude:set_enabled(true)
		self._start_altitude:set_value(current_shape.position.z)
		self._start_altitude_text:set_enabled(true)
		self._start_altitude_text:change_value(current_shape.position.z)
		self._start_altitude_spin:set_enabled(true)
		self._height:set_enabled(true)
		self._height:set_value(current_shape.height)
		self._height_altitude_text:set_enabled(true)
		self._height_altitude_text:change_value(current_shape.height)
		self._height_altitude_spin:set_enabled(true)
		self._size:set_enabled(true)
		self._size:set_value(math.sqrt(current_shape.size_mul)*10)
	else
		self._start_altitude:set_value(0)
		self._start_altitude:set_enabled(false)
		self._start_altitude_text:change_value(0)
		self._start_altitude_text:set_enabled(false)
		self._start_altitude_spin:set_enabled(false)
		self._height:set_value(0)
		self._height:set_enabled(false)
		self._height_altitude_text:change_value(0)
		self._height_altitude_text:set_enabled(false)
		self._height_altitude_spin:set_enabled(false)
		self._size:set_value(0)
		self._size:set_enabled(false)
	end

	return 
end
CoreAreaUnitElement.update_selected = function (self, time)
	for _, shape in pairs(self._hed.shapes) do
		self.draw(self, shape, 0, (math.sin(time*100) + 1)*0.5*0.5 + 0.5, 0)
	end

	if self._current_shape then
		self.draw(self, self._hed.shapes[self._current_shape], 0, 1, 1)
	end

	return 
end
CoreAreaUnitElement.draw = function (self, shape, r, g, b)
	local start_z = -20000
	local height = 40000
	local position = shape.position
	local rotation = shape.rotation

	if shape.height ~= 0 or position.z ~= 0 then
		height = shape.height
		start_z = 0
	end

	self._brush:set_color(Color(0.75, r, g, b))

	if shape.type == "sphere" then
		self._brush:cylinder(position + Vector3(0, 0, start_z), position + Vector3(0, 0, height + start_z), shape.radious*shape.size_mul)
		Application:draw_cylinder(position + Vector3(0, 0, start_z), position + Vector3(0, 0, height + start_z), shape.radious*shape.size_mul, r, g, b)
	elseif shape.type == "box" then
		local start = position + Vector3(0, 0, start_z)
		local center = (start + start + Vector3(shape.width*shape.size_mul, shape.length*shape.size_mul, height))/2

		self._brush:box(center, Vector3((shape.width*shape.size_mul)/2, 0, 0), Vector3(0, (shape.length*shape.size_mul)/2, 0), Vector3(0, 0, height/2))
		Application:draw_box(position + Vector3(0, 0, start_z), position + Vector3(shape.width*shape.size_mul, shape.length*shape.size_mul, height + start_z), r, g, b)
	elseif shape.type == "plane" then
		local start = position + Vector3(0, 0, start_z)
		local width = shape.width*shape.size_mul
		local pos1 = position + Vector3(0, 0, start_z)
		local pos2 = position + shape.rotation:x()*width + Vector3(0, 0, start_z)
		local pos3 = position + shape.rotation:x()*width + Vector3(0, 0, height + start_z)
		local pos4 = position + Vector3(0, 0, height + start_z)

		self._brush:quad(pos1, pos2, pos3, pos4)
		Application:draw_rotation(position, shape.rotation)
		Application:draw_line(pos1, pos2, r, g, b)
		Application:draw_line(pos2, pos3, r, g, b)
		Application:draw_line(pos3, pos4, r, g, b)
		Application:draw_line(pos4, pos1, r, g, b)

		local step = math.clamp(height/20, 250, 10000)

		for i = start_z, height + start_z, step do
			local pos = position + Vector3(0, 0, i)

			Application:draw_line(pos, pos + shape.rotation:y()*500, 0, 1, 0)
			Application:draw_line(pos + shape.rotation:x()*width, pos + shape.rotation:x()*width + shape.rotation:y()*500, 0, 1, 0)
			Application:draw_line(pos + (shape.rotation:x()*width)/2, pos + (shape.rotation:x()*width)/2 + shape.rotation:y()*500, 0, 1, 0)
			Application:draw_line(pos, pos + shape.rotation:y()*-500, 1, 0, 0)
			Application:draw_line(pos + shape.rotation:x()*width, pos + shape.rotation:x()*width + shape.rotation:y()*-500, 1, 0, 0)
			Application:draw_line(pos + (shape.rotation:x()*width)/2, pos + (shape.rotation:x()*width)/2 + shape.rotation:y()*-500, 1, 0, 0)
		end
	end

	return 
end
CoreAreaUnitElement.update_editing = function (self, t, dt)
	local p1 = managers.editor:get_cursor_look_point(0)
	local p2 = managers.editor:get_cursor_look_point(100)
	local current_shape = self._hed.shapes[self._current_shape]
	local t = p1.z/(p1.z - p2.z)
	self._current_pos = p1 + (p2 - p1)*t

	if 0 < self._grid_size then
		self._current_pos = self.round_position(self, self._current_pos)
	end

	if self._creating and self._current_shape then
		self._hed.shapes[self._current_shape] = self.set_shape_properties(self, self._shape_type, self._start_pos, self._current_pos)
	end

	if self._grab_shape then
		if self._move_all_shapes then
			for name, shape in pairs(self._hed.shapes) do
				shape.position = self._current_pos + shape.move_offset
			end
		elseif current_shape then
			current_shape.position = self._current_pos + current_shape.move_offset
		end
	end

	local kb = Input:keyboard()
	local mov_vec = nil

	if self._move_shape_rep:update(t, dt) then
		if kb.down(kb, "up") then
			mov_vec = Vector3(0, 1, 0)
		elseif kb.down(kb, "down") then
			mov_vec = Vector3(0, 1, 0)*-1
		elseif kb.down(kb, "right") then
			mov_vec = Vector3(1, 0, 0)
		elseif kb.down(kb, "left") then
			mov_vec = Vector3(1, 0, 0)*-1
		end

		if mov_vec then
			if shift() then
				for name, shape in pairs(self._hed.shapes) do
					shape.position = shape.position + mov_vec*self._kb_move_grid_size
				end
			elseif current_shape then
				current_shape.position = current_shape.position + mov_vec*self._kb_move_grid_size
			end
		end
	end

	if current_shape then
		local rot_axis = nil

		if kb.down(kb, "num 4") then
			rot_axis = Vector3(0, 0, 1)
		elseif kb.down(kb, "num 6") then
			rot_axis = Vector3(0, 0, -1)
		end

		if rot_axis then
			current_shape.rotation = Rotation(rot_axis, dt*100)*current_shape.rotation
		end
	end

	Application:draw_rotation(self._current_pos, self._unit:rotation())

	return 
end
CoreAreaUnitElement.load_data = function (self, data)
	if not data then
		return 
	end

	self._hed.area_type = data._area_type or self._hed.area_type

	for _, shape in ipairs(data._shapes) do
		self._current_shape = self.new_shape_name(self)
		local properties = nil
		local s_pos = shape._generic._position

		if shape._type == "sphere" then
			local e_pos = shape._generic._position + Vector3(shape._radious, 0, shape._height)
			properties = self.set_shape_properties(self, shape._type, s_pos, e_pos)
		elseif shape._type == "box" then
			local e_pos = shape._generic._position + Vector3(shape._width, shape._length, shape._height)
			properties = self.set_shape_properties(self, shape._type, s_pos, e_pos)
		elseif shape._type == "plane" then
			local e_pos = shape._generic._position + shape._generic._rotation:x()*shape._width
			properties = self.set_shape_properties(self, shape._type, s_pos, e_pos)
			properties.height = shape._height
		end

		self._hed.shapes[self._current_shape] = properties
	end

	return 
end
CoreAreaUnitElement.round_position = function (self, p)
	return Vector3(math.round(p.x/self._grid_size)*self._grid_size, math.round(p.y/self._grid_size)*self._grid_size, 0)
end
CoreAreaUnitElement.create_shape = function (self)
	local p1 = managers.editor:get_cursor_look_point(0)
	local p2 = managers.editor:get_cursor_look_point(100)
	local t = p1.z/(p1.z - p2.z)
	self._start_pos = p1 + (p2 - p1)*t

	if 0 < self._grid_size then
		self._start_pos = self.round_position(self, self._start_pos)
	end

	self._current_shape = self.new_shape_name(self)
	local properties = self.set_shape_properties(self, self._shape_type, self._start_pos, self._start_pos)
	self._hed.shapes[self._current_shape] = properties

	self._shapes_list:append(self._current_shape)

	self._creating = true

	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement.new_shape_name = function (self)
	for i = 1, 100, 1 do
		local name = "shape" .. i

		if not self._hed.shapes[name] then
			return name
		end
	end

	return 
end
CoreAreaUnitElement.set_shape_properties = function (self, type, pos, end_pos)
	local t = {
		type = type,
		position = pos,
		rotation = Rotation(),
		size_mul = 1,
		height = end_pos.z - t.position.z
	}

	if type == "sphere" then
		t.radious = Vector3(t.position.x, t.position.y, 0) - Vector3(end_pos.x, end_pos.y, 0):length()
	elseif type == "box" then
		t.length = end_pos.y - t.position.y
		t.width = end_pos.x - t.position.x
	elseif type == "plane" then
		local x = end_pos - t.position:normalized()
		local z = Vector3(0, 0, 1)
		local y = z.cross(z, x)
		t.rotation = Rotation(x, y, z)
		t.width = end_pos - t.position:length()
	end

	return t
end
CoreAreaUnitElement.delete = function (self)
	if self._current_shape then
		self._hed.shapes[self._current_shape] = nil
		self._current_shape = nil
	end

	self.populate_shapes_list(self)
	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement.create_shape_release = function (self)
	self._creating = false

	return 
end
CoreAreaUnitElement.move_shape = function (self)
	self._grab_shape = true

	for name, shape in pairs(self._hed.shapes) do
		shape.move_offset = shape.position - self._current_pos
	end

	self._move_all_shapes = shift()

	return 
end
CoreAreaUnitElement.release_shape = function (self)
	self._grab_shape = false
	self._move_all_shapes = false

	for name, shape in pairs(self._hed.shapes) do
		shape.move_offset = nil
	end

	return 
end
CoreAreaUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, "lmb", callback(self, self, "create_shape"))
	vc.add_release_trigger(vc, "lmb", callback(self, self, "create_shape_release"))
	vc.add_trigger(vc, "emb", callback(self, self, "move_shape"))
	vc.add_release_trigger(vc, "emb", callback(self, self, "release_shape"))
	vc.add_trigger(vc, "destroy", callback(self, self, "delete"))

	return 
end
CoreAreaUnitElement.save_mission_trigger = function (self, file, tab)
	file.puts(file, tab .. "<trigger type=\"UnitInArea\" name=\"area" .. self._unit:unit_data().unit_id .. "\"/>")

	return 
end
CoreAreaUnitElement.set_interval = function (self, data)
	local value = tonumber(data.ctrlr:get_value())
	value = math.clamp(value, 0, 1000000)
	self._hed.area_interval = value

	data.ctrlr:change_value(string.format("%.2f", self._hed.area_interval))

	return 
end
CoreAreaUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local interval_sizer = EWS:BoxSizer("HORIZONTAL")

	interval_sizer.add(interval_sizer, EWS:StaticText(panel, "Check Interval:", 0, ""), 2, 0, "ALIGN_CENTER_VERTICAL")

	local interval_ctrlr = EWS:TextCtrl(self._panel, string.format("%.2f", self._hed.area_interval), "", "TE_PROCESS_ENTER")

	interval_ctrlr.set_tool_tip(interval_ctrlr, "Set the check interval in seconds (set to 0 for every frame).")
	interval_ctrlr.connect(interval_ctrlr, "EVT_CHAR", callback(nil, _G, "verify_number"), interval_ctrlr)
	interval_ctrlr.connect(interval_ctrlr, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_interval"), {
		ctrlr = interval_ctrlr
	})
	interval_ctrlr.connect(interval_ctrlr, "EVT_KILL_FOCUS", callback(self, self, "set_interval"), {
		ctrlr = interval_ctrlr
	})
	interval_sizer.add(interval_sizer, interval_ctrlr, 3, 0, "EXPAND")
	panel_sizer.add(panel_sizer, interval_sizer, 0, 0, "EXPAND")

	local types_sizer = EWS:BoxSizer("HORIZONTAL")

	types_sizer.add(types_sizer, EWS:StaticText(panel, "Types:", 0, ""), 2, 0, "ALIGN_CENTER_VERTICAL")

	local area_type = EWS:ComboBox(panel, "", "", "CB_DROPDOWN,CB_READONLY")

	for _, type in ipairs({
		"on_enter",
		"on_exit",
		"toggle"
	}) do
		area_type.append(area_type, type)
	end

	area_type.set_value(area_type, self._hed.area_type)
	types_sizer.add(types_sizer, area_type, 3, 0, "EXPAND")
	area_type.connect(area_type, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		value = "area_type",
		ctrlr = area_type
	})
	panel_sizer.add(panel_sizer, types_sizer, 0, 0, "EXPAND")

	local shape_names_sizer = EWS:BoxSizer("HORIZONTAL")

	shape_names_sizer.add(shape_names_sizer, EWS:StaticText(panel, "Shape Types:", 0, ""), 2, 0, "ALIGN_CENTER_VERTICAL")

	local shape_names = EWS:ComboBox(panel, "", "", "CB_DROPDOWN,CB_READONLY")

	for _, type in ipairs(self._shapes_types) do
		shape_names.append(shape_names, type)
	end

	shape_names.set_value(shape_names, self._shape_type)
	shape_names_sizer.add(shape_names_sizer, shape_names, 3, 0, "EXPAND")
	shape_names.connect(shape_names, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_shape_type"), shape_names)
	panel_sizer.add(panel_sizer, shape_names_sizer, 0, 0, "EXPAND")

	local shapes_sizer = EWS:StaticBoxSizer(panel, "VERTICAL", "Shapes")
	self._shapes_list = EWS:ListBox(panel, "", "LB_SINGLE,LB_HSCROLL,LB_NEEDED_SB,LB_SORT")

	shapes_sizer.add(shapes_sizer, self._shapes_list, 0, 0, "EXPAND")
	self._shapes_list:connect("EVT_COMMAND_LISTBOX_SELECTED", callback(self, self, "selected_shape"), self._shapes_list)
	self.populate_shapes_list(self)

	local delete_shape = EWS:Button(panel, "Delete Shape", "", "BU_EXACTFIT,NO_BORDER")

	shapes_sizer.add(shapes_sizer, delete_shape, 0, 0, "ALIGN_RIGHT")
	delete_shape.connect(delete_shape, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "delete"), nil)
	panel_sizer.add(panel_sizer, shapes_sizer, 0, 0, "EXPAND")

	local altitude_sizer = EWS:StaticBoxSizer(panel, "VERTICAL", "Altitude")
	self._start_altitude = EWS:Slider(panel, 0, -20000, 20000, "", "")
	self._height = EWS:Slider(panel, 0, 0, 30000, "", "")
	self._start_altitude_text, self._start_altitude_spin = self._altitude_ctrlr(self, panel, "Start Altitude:", 0, "start", altitude_sizer)

	altitude_sizer.add(altitude_sizer, self._start_altitude, 1, 10, "EXPAND")

	self._height_altitude_text, self._height_altitude_spin = self._altitude_ctrlr(self, panel, "Height:", 0, "height", altitude_sizer)

	altitude_sizer.add(altitude_sizer, self._height, 1, 0, "EXPAND")
	self._start_altitude:connect("EVT_SCROLL_CHANGED", callback(self, self, "set_start_altitude"), nil)
	self._start_altitude:connect("EVT_SCROLL_THUMBTRACK", callback(self, self, "set_start_altitude"), nil)
	self._height:connect("EVT_SCROLL_CHANGED", callback(self, self, "set_height"), nil)
	self._height:connect("EVT_SCROLL_THUMBTRACK", callback(self, self, "set_height"), nil)

	local set_2d = EWS:Button(panel, "Set to 2D", "_set_2d", "BU_EXACTFIT,NO_BORDER")

	altitude_sizer.add(altitude_sizer, set_2d, 1, 0, "EXPAND")
	set_2d.connect(set_2d, "EVT_COMMAND_BUTTON_CLICKED", callback(self, self, "set_2d"), nil)
	panel_sizer.add(panel_sizer, altitude_sizer, 1, 0, "EXPAND")

	local size_sizer = EWS:StaticBoxSizer(panel, "VERTICAL", "Size")
	self._size = EWS:Slider(panel, 10, 1, 40, "", "")

	size_sizer.add(size_sizer, self._size, 1, 0, "EXPAND")
	self._size:connect("EVT_SCROLL_CHANGED", callback(self, self, "set_size"), nil)
	self._size:connect("EVT_SCROLL_CHANGED", callback(self, self, "size_release"), nil)
	self._size:connect("EVT_SCROLL_THUMBTRACK", callback(self, self, "set_size"), nil)
	self._size:connect("EVT_SCROLL_THUMBRELEASE", callback(self, self, "size_release"), nil)
	panel_sizer.add(panel_sizer, size_sizer, 0, 0, "EXPAND")
	self.set_shape_values(self)

	return 
end
CoreAreaUnitElement._altitude_ctrlr = function (self, panel, name, value, type, sizer)
	local ctrl_sizer = EWS:BoxSizer("HORIZONTAL")

	ctrl_sizer.add(ctrl_sizer, EWS:StaticText(panel, name, "", "ALIGN_LEFT"), 1, 0, "ALIGN_CENTER_VERTICAL")

	local ctrl = EWS:TextCtrl(self._panel, value, "", "TE_PROCESS_ENTER")

	ctrl.connect(ctrl, "EVT_CHAR", callback(nil, _G, "verify_number"), ctrl)
	ctrl.connect(ctrl, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_altitude_text"), {
		ctrl = ctrl,
		type = type
	})
	ctrl.connect(ctrl, "EVT_KILL_FOCUS", callback(self, self, "set_altitude_text"), {
		ctrl = ctrl,
		type = type
	})
	ctrl_sizer.add(ctrl_sizer, ctrl, 1, 0, "EXPAND")

	local spin = EWS:SpinButton(self._panel, "", "SP_VERTICAL")

	spin.set_min_size(spin, Vector3(-1, 10, 0))
	spin.connect(spin, "EVT_SCROLL_LINEUP", callback(self, self, "set_altitude_spin"), {
		step = 1,
		ctrl = ctrl,
		type = type
	})
	spin.connect(spin, "EVT_SCROLL_LINEDOWN", callback(self, self, "set_altitude_spin"), {
		step = -1,
		ctrl = ctrl,
		type = type
	})
	ctrl_sizer.add(ctrl_sizer, spin, 0, 0, "EXPAND")
	sizer.add(sizer, ctrl_sizer, 1, 10, "EXPAND,LEFT,RIGHT")

	return ctrl, spin
end
CoreAreaUnitElement.destroy = function (self)
	return 
end

return 
