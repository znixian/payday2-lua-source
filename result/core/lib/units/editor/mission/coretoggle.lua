CoreToggleUnitElement = CoreToggleUnitElement or class(MissionElement)
CoreToggleUnitElement.SAVE_UNIT_POSITION = false
CoreToggleUnitElement.SAVE_UNIT_ROTATION = false
CoreToggleUnitElement.LINK_ELEMENTS = {
	"elements"
}
ToggleUnitElement = ToggleUnitElement or class(CoreToggleUnitElement)
ToggleUnitElement.init = function (self, ...)
	CoreToggleUnitElement.init(self, ...)

	return 
end
CoreToggleUnitElement.init = function (self, unit)
	MissionElement.init(self, unit)

	self._hed.toggle = "on"
	self._hed.set_trigger_times = -1
	self._hed.elements = {}

	table.insert(self._save_values, "toggle")
	table.insert(self._save_values, "elements")
	table.insert(self._save_values, "set_trigger_times")

	return 
end
CoreToggleUnitElement.draw_links = function (self, t, dt, selected_unit, all_units)
	MissionElement.draw_links(self, t, dt, selected_unit)

	for _, id in ipairs(self._hed.elements) do
		local unit = all_units[id]
		local draw = not selected_unit or unit == selected_unit or self._unit == selected_unit

		if draw then
			self._draw_link(self, {
				g = 0,
				b = 0,
				r = 0.75,
				from_unit = self._unit,
				to_unit = unit
			})
		end
	end

	return 
end
CoreToggleUnitElement.get_links_to_unit = function (self, ...)
	CoreToggleUnitElement.super.get_links_to_unit(self, ...)
	self._get_links_of_type_from_elements(self, self._hed.elements, "operator", ...)

	return 
end
CoreToggleUnitElement.update_editing = function (self)
	return 
end
CoreToggleUnitElement.add_element = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit then
		local id = ray.unit:unit_data().unit_id

		if table.contains(self._hed.elements, id) then
			table.delete(self._hed.elements, id)
		else
			table.insert(self._hed.elements, id)
		end
	end

	return 
end
CoreToggleUnitElement.add_triggers = function (self, vc)
	vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "add_element"))

	return 
end
CoreToggleUnitElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local names = nil

	self._build_add_remove_unit_from_list(self, panel, panel_sizer, self._hed.elements, names)
	self._build_value_combobox(self, panel, panel_sizer, "toggle", {
		"on",
		"off",
		"toggle"
	}, "Select how you want to toggle an element")
	self._build_value_number(self, panel, panel_sizer, "set_trigger_times", {
		floats = 0,
		min = -1
	}, "Sets the elements trigger times when toggle on (-1 means do not use)")

	return 
end

return 
