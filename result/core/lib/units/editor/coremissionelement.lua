CoreMissionElement = CoreMissionElement or class()
MissionElement = MissionElement or class(CoreMissionElement)
MissionElement.SAVE_UNIT_POSITION = true
MissionElement.SAVE_UNIT_ROTATION = true
MissionElement.RANDOMS = nil
MissionElement.LINK_ELEMENTS = {}
MissionElement.init = function (self, ...)
	CoreMissionElement.init(self, ...)

	return 
end
CoreMissionElement.init = function (self, unit)
	if not CoreMissionElement.editor_link_brush then
		local brush = Draw:brush()

		brush.set_font(brush, Idstring("core/fonts/nice_editor_font"), 10)
		brush.set_render_template(brush, Idstring("OverlayVertexColorTextured"))

		CoreMissionElement.editor_link_brush = brush
	end

	self._unit = unit
	self._hed = self._unit:mission_element_data()
	self._ud = self._unit:unit_data()

	self._unit:anim_play(1)

	self._save_values = {}
	self._update_selected_on = false

	self._add_default_saves(self)

	if self.USES_POINT_ORIENTATION then
		self.base_update_editing = callback(self, self, "__update_editing")
	end

	self._parent_panel = managers.editor:mission_element_panel()
	self._parent_sizer = managers.editor:mission_element_sizer()
	self._panels = {}
	self._on_executed_units = {}
	self._arrow_brush = Draw:brush()

	self._createicon(self)

	return 
end
CoreMissionElement.post_init = function (self)
	if self.RANDOMS then
		for _, value_name in ipairs(self.RANDOMS) do
			if tonumber(self._hed[value_name]) then
				self._hed[value_name] = {
					self._hed[value_name],
					0
				}
			end
		end
	end

	return 
end
CoreMissionElement._createicon = function (self)
	local iconsize = 32

	if Global.iconsize then
		iconsize = Global.iconsize
	end

	if self._icon == nil and self._icon_x == nil then
		return 
	end

	local root = self._unit:orientation_object()

	if root == nil then
		return 
	end

	if self._iconcolor_type then
		if self._iconcolor_type == "trigger" then
			self._iconcolor = "ff81bffc"
		elseif self._iconcolor_type == "logic" then
			self._iconcolor = "ffffffd9"
		elseif self._iconcolor_type == "operator" then
			self._iconcolor = "fffcbc7c"
		elseif self._iconcolor_type == "filter" then
			self._iconcolor = "ff65ad67"
		end
	end

	if self._iconcolor == nil then
		self._iconcolor = "fff"
	end

	self._iconcolor_c = Color(self._iconcolor)
	self._icon_gui = World:newgui()
	local pos = self._unit:position() - Vector3(iconsize/2, iconsize/2, 0)
	self._icon_ws = self._icon_gui:create_linked_workspace(64, 64, root, pos, Vector3(iconsize, 0, 0), Vector3(0, iconsize, 0))

	self._icon_ws:set_billboard(self._icon_ws.BILLBOARD_BOTH)
	self._icon_ws:panel():gui(Idstring("core/guis/core_edit_icon"))

	self._icon_script = self._icon_ws:panel():gui(Idstring("core/guis/core_edit_icon")):script()

	if self._icon then
		self._icon_script:seticon(self._icon, tostring(self._iconcolor))
	elseif self._icon_x then
		self._icon_script:seticon_texture_rect(self._icon_x, self._icon_y, self._icon_w, self._icon_h, tostring(self._iconcolor))
	end

	return 
end
CoreMissionElement.set_iconsize = function (self, size)
	if not self._icon_ws then
		return 
	end

	local root = self._unit:orientation_object()
	local pos = self._unit:position() - Vector3(size/2, size/2, 0)

	self._icon_ws:set_linked(64, 64, root, pos, Vector3(size, 0, 0), Vector3(0, size, 0))

	return 
end
CoreMissionElement._add_default_saves = function (self)
	self._hed.enabled = true
	self._hed.debug = nil
	self._hed.execute_on_startup = false
	self._hed.execute_on_restart = nil
	self._hed.base_delay = 0
	self._hed.base_delay_rand = nil
	self._hed.trigger_times = 0
	self._hed.on_executed = {}

	if self.USES_POINT_ORIENTATION then
		self._hed.orientation_elements = nil
		self._hed.use_orientation_sequenced = nil
		self._hed.disable_orientation_on_use = nil
	end

	if self.USES_INSTIGATOR_RULES then
		self._hed.rules_elements = nil
	end

	if self.INSTANCE_VAR_NAMES then
		self._hed.instance_var_names = nil
	end

	table.insert(self._save_values, "unit:position")
	table.insert(self._save_values, "unit:rotation")
	table.insert(self._save_values, "enabled")
	table.insert(self._save_values, "execute_on_startup")
	table.insert(self._save_values, "base_delay")
	table.insert(self._save_values, "trigger_times")
	table.insert(self._save_values, "on_executed")
	table.insert(self._save_values, "orientation_elements")
	table.insert(self._save_values, "use_orientation_sequenced")
	table.insert(self._save_values, "disable_orientation_on_use")
	table.insert(self._save_values, "rules_elements")
	table.insert(self._save_values, "instance_var_names")

	return 
end
CoreMissionElement.build_default_gui = function (self, panel, sizer)
	self._build_value_checkbox(self, panel, sizer, "enabled")
	self._build_value_checkbox(self, panel, sizer, "execute_on_startup")
	self._build_value_number(self, panel, sizer, "trigger_times", {
		floats = 0,
		min = 0
	}, "Specifies how many time this element can be executed (0 mean unlimited times)")

	local base_delay_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, base_delay_sizer, 0, 0, "EXPAND,LEFT")

	local base_delay_ctrlr = self._build_value_number(self, panel, base_delay_sizer, "base_delay", {
		sizer_proportions = 2,
		min = 0,
		floats = 2,
		name_proportions = 1,
		ctrlr_proportions = 1
	}, "Specifies a base delay that is added to each on executed delay")
	local base_delay_rand_ctrlr = self._build_value_number(self, panel, base_delay_sizer, "base_delay_rand", {
		sizer_proportions = 1,
		min = 0,
		floats = 2,
		name_proportions = 0,
		ctrlr_proportions = 1
	}, "Specifies an additional random time to be added to base delay (delay + rand)", "  random")
	local on_executed_sizer = EWS:StaticBoxSizer(panel, "VERTICAL", "On Executed")
	local element_sizer = EWS:BoxSizer("HORIZONTAL")

	on_executed_sizer.add(on_executed_sizer, element_sizer, 0, 1, "EXPAND,LEFT")

	self._elements_params = {
		name = "Element:",
		name_proportions = 1,
		tooltip = "Select an element from the combobox",
		sorted = true,
		sizer_proportions = 1,
		ctrlr_proportions = 2,
		panel = panel,
		sizer = element_sizer,
		options = {}
	}
	local elements = CoreEWS.combobox(self._elements_params)

	elements.connect(elements, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "on_executed_element_selected"), nil)

	self._add_elements_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._add_elements_toolbar:add_tool("ADD_ELEMENT", "Add an element", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	self._add_elements_toolbar:connect("ADD_ELEMENT", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_on_toolbar_add_element"), nil)
	self._add_elements_toolbar:realize()
	element_sizer.add(element_sizer, self._add_elements_toolbar, 0, 1, "EXPAND,LEFT")

	self._elements_toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	self._elements_toolbar:add_tool("DELETE_SELECTED", "Remove selected element", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	self._elements_toolbar:connect("DELETE_SELECTED", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_on_toolbar_remove"), nil)
	self._elements_toolbar:realize()
	element_sizer.add(element_sizer, self._elements_toolbar, 0, 1, "EXPAND,LEFT")

	if not self.ON_EXECUTED_ALTERNATIVES and self._create_dynamic_on_executed_alternatives then
		self._create_dynamic_on_executed_alternatives(self)
	end

	if self.ON_EXECUTED_ALTERNATIVES then
		local on_executed_alternatives_params = {
			name = "Alternative:",
			name_proportions = 1,
			tooltip = "Select an alternative on executed from the combobox",
			sorted = false,
			ctrlr_proportions = 2,
			panel = panel,
			sizer = on_executed_sizer,
			options = self.ON_EXECUTED_ALTERNATIVES,
			value = self.ON_EXECUTED_ALTERNATIVES[1]
		}
		local on_executed_alternatives_types = CoreEws.combobox(on_executed_alternatives_params)

		on_executed_alternatives_types.connect(on_executed_alternatives_types, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "on_executed_alternatives_types"), nil)

		self._on_executed_alternatives_params = on_executed_alternatives_params
	end

	local delay_sizer = EWS:BoxSizer("HORIZONTAL")

	on_executed_sizer.add(on_executed_sizer, delay_sizer, 0, 0, "EXPAND,LEFT")

	self._element_delay_params = {
		value = 0,
		name = "Delay:",
		ctrlr_proportions = 1,
		name_proportions = 1,
		tooltip = "Sets the delay time for the selected on executed element",
		min = 0,
		floats = 2,
		sizer_proportions = 2,
		panel = panel,
		sizer = delay_sizer
	}
	local element_delay = CoreEws.number_controller(self._element_delay_params)

	element_delay.connect(element_delay, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "on_executed_element_delay"), nil)
	element_delay.connect(element_delay, "EVT_KILL_FOCUS", callback(self, self, "on_executed_element_delay"), nil)

	self._element_delay_rand_params = {
		value = 0,
		name = "  Random:",
		ctrlr_proportions = 1,
		name_proportions = 0,
		tooltip = "Specifies an additional random time to be added to delay (delay + rand)",
		min = 0,
		floats = 2,
		sizer_proportions = 1,
		panel = panel,
		sizer = delay_sizer
	}
	local element_delay_rand = CoreEws.number_controller(self._element_delay_rand_params)

	element_delay_rand.connect(element_delay_rand, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "on_executed_element_delay_rand"), nil)
	element_delay_rand.connect(element_delay_rand, "EVT_KILL_FOCUS", callback(self, self, "on_executed_element_delay_rand"), nil)
	sizer.add(sizer, on_executed_sizer, 0, 0, "EXPAND")

	if self.USES_POINT_ORIENTATION then
		sizer.add(sizer, self._build_point_orientation(self, panel), 0, 0, "EXPAND")
	end

	if self.INSTANCE_VAR_NAMES then
		sizer.add(sizer, self._build_instance_var_names(self, panel), 0, 0, "EXPAND")
	end

	sizer.add(sizer, EWS:StaticLine(panel, "", "LI_HORIZONTAL"), 0, 5, "EXPAND,TOP,BOTTOM")
	self.append_elements_sorted(self)
	self.set_on_executed_element(self)

	local function refresh_list_flow_cbk(ctrlr)
		local function f()
			managers.editor:layer("Mission"):refresh_list_flow()

			return 
		end

		ctrlr.connect(ctrlr, "EVT_COMMAND_TEXT_ENTER", f, nil)
		ctrlr.connect(ctrlr, "EVT_KILL_FOCUS", f, nil)

		return 
	end

	refresh_list_flow_cbk(base_delay_ctrlr)
	refresh_list_flow_cbk(base_delay_rand_ctrlr)
	refresh_list_flow_cbk(element_delay)
	refresh_list_flow_cbk(element_delay_rand)

	return 
end
CoreMissionElement._build_point_orientation = function (self, panel)
	local sizer = EWS:StaticBoxSizer(panel, "HORIZONTAL", "Point orientation")
	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "ADD_ELEMENT", "Add an element", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "ADD_ELEMENT", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_add_unit_to_orientation_elements"), nil)
	toolbar.add_tool(toolbar, "DELETE_ELEMENT", "Remove selected element", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	toolbar.connect(toolbar, "DELETE_ELEMENT", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_remove_unit_from_orientation_elements"), nil)
	toolbar.realize(toolbar)
	sizer.add(sizer, toolbar, 0, 1, "EXPAND,LEFT")

	local use_orientation_sequenced = EWS:CheckBox(panel, "Use sequenced", "")

	use_orientation_sequenced.set_value(use_orientation_sequenced, self._hed.use_orientation_sequenced)
	use_orientation_sequenced.connect(use_orientation_sequenced, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "use_orientation_sequenced",
		ctrlr = use_orientation_sequenced
	})
	sizer.add(sizer, use_orientation_sequenced, 0, 4, "EXPAND,LEFT")

	local disable_orientation_on_use = EWS:CheckBox(panel, "Disable on use", "")

	disable_orientation_on_use.set_value(disable_orientation_on_use, self._hed.disable_orientation_on_use)
	disable_orientation_on_use.connect(disable_orientation_on_use, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		value = "disable_orientation_on_use",
		ctrlr = disable_orientation_on_use
	})
	sizer.add(sizer, disable_orientation_on_use, 0, 4, "EXPAND,LEFT")

	return sizer
end
CoreMissionElement._add_unit_to_orientation_elements = function (self)
	local script = self._unit:mission_element_data().script

	local function f(unit)
		if not string.find(unit.name(unit):s(), "point_orientation", 1, true) then
			return 
		end

		if not unit.mission_element_data(unit) or unit.mission_element_data(unit).script ~= script then
			return 
		end

		local id = unit.unit_data(unit).unit_id

		if self._hed.orientation_elements and table.contains(self._hed.orientation_elements, id) then
			return false
		end

		return managers.editor:layer("Mission"):category_map()[unit.type(unit):s()]
	end

	local dialog = SelectUnitByNameModal:new("Add Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		self._add_orientation_unit_id(self, unit.unit_data(unit).unit_id)
	end

	return 
end
CoreMissionElement._remove_unit_from_orientation_elements = function (self)
	if not self._hed.orientation_elements then
		return 
	end

	local function f(unit)
		return table.contains(self._hed.orientation_elements, unit.unit_data(unit).unit_id)
	end

	local dialog = SelectUnitByNameModal:new("Remove Unit", f)

	if dialog.cancelled(dialog) then
		return 
	end

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		self._remove_orientation_unit_id(self, unit.unit_data(unit).unit_id)
	end

	return 
end
CoreMissionElement._build_instance_var_names = function (self, panel)
	local sizer = EWS:StaticBoxSizer(panel, "VERTICAL", "Instance variables")
	local options = {}
	local func_instance_params_units = managers.editor:layer("Mission"):get_created_unit_by_pattern({
		"func_instance_params"
	})

	for _, unit in ipairs(func_instance_params_units) do
		for _, param in ipairs(unit.mission_element_data(unit).params) do
			options[param.type] = options[param.type] or {}

			table.insert(options[param.type], param.var_name)
		end
	end

	for _, data in ipairs(self.INSTANCE_VAR_NAMES) do
		local params = {
			default = "not_used",
			ctrlr_proportions = 2,
			name_proportions = 1,
			sizer_proportions = 1,
			tooltip = "Select a value",
			sorted = true,
			name = string.pretty(data.value, true) .. ":",
			panel = panel,
			sizer = sizer,
			options = options[data.type] or {},
			value = (self._hed.instance_var_names and self._hed.instance_var_names[data.value]) or "not_used"
		}
		local ctrlr = CoreEws.combobox(params)

		ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "_set_instance_var_name"), {
			ctrlr = ctrlr,
			data = data
		})
	end

	return sizer
end
CoreMissionElement._set_instance_var_name = function (self, params)
	local value = params.ctrlr:get_value()
	value = (value ~= "not_used" and value) or nil
	self._hed.instance_var_names = self._hed.instance_var_names or {}
	self._hed.instance_var_names[params.data.value] = value
	self._hed.instance_var_names = (next(self._hed.instance_var_names) and self._hed.instance_var_names) or nil

	return 
end
CoreMissionElement._create_panel = function (self)
	if self._panel then
		return 
	end

	self._panel, self._panel_sizer = self._add_panel(self, self._parent_panel, self._parent_sizer)

	return 
end
CoreMissionElement._build_panel = function (self)
	self._create_panel(self)

	return 
end
CoreMissionElement.panel = function (self, id, parent, parent_sizer)
	if id then
		if self._panels[id] then
			return self._panels[id]
		end

		local panel, panel_sizer = self._add_panel(self, parent, parent_sizer)

		self._build_panel(self, panel, panel_sizer)

		self._panels[id] = panel

		return self._panels[id]
	end

	if not self._panel then
		self._build_panel(self)
	end

	return self._panel
end
CoreMissionElement._add_panel = function (self, parent, parent_sizer)
	local panel = EWS:ScrolledWindow(parent, "", "VSCROLL,TAB_TRAVERSAL")

	panel.set_scroll_rate(panel, Vector3(0, 20, 0))
	panel.set_virtual_size_hints(panel, Vector3(0, 0, 0), Vector3(1, -1, -1))

	local panel_sizer = EWS:BoxSizer("VERTICAL")

	panel.set_sizer(panel, panel_sizer)
	parent_sizer.add(parent_sizer, panel, 1, 0, "EXPAND")
	panel.set_visible(panel, false)
	panel.set_extension(panel, {
		alive = true
	})
	self.build_default_gui(self, panel, panel_sizer)

	return panel, panel_sizer
end
CoreMissionElement.add_help_text = function (self, data)
	if data.panel and data.sizer then
		local text = EWS:TextCtrl(data.panel, data.text, 0, "TE_MULTILINE,TE_READONLY,TE_WORDWRAP,TE_CENTRE")

		data.sizer:add(text, 0, 5, "EXPAND,TOP,BOTTOM")

		return text
	end

	return 
end
CoreMissionElement._add_help_text = function (self, text)
	local help = {
		panel = self._panel,
		sizer = self._panel_sizer,
		text = text
	}

	return self.add_help_text(self, help)
end
CoreMissionElement._on_toolbar_add_element = function (self)
	local function f(unit)
		return unit.type(unit) == Idstring("mission_element") and unit ~= self._unit
	end

	local dialog = SelectUnitByNameModal:new("Add/Remove element", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		self.add_on_executed(self, unit)
	end

	return 
end
CoreMissionElement._on_toolbar_remove = function (self)
	self.remove_on_execute(self, self._current_element_unit(self))

	return 
end
CoreMissionElement.set_element_data = function (self, data)
	if data.callback then
		local he = self._unit:mission_element()

		he[data.callback](he, data.ctrlr, data.params)
	end

	if data.value then
		self._hed[data.value] = data.ctrlr:get_value()
		self._hed[data.value] = tonumber(self._hed[data.value]) or self._hed[data.value]

		if data.value == "base_delay_rand" then
			self._hed[data.value] = (0 < self._hed[data.value] and self._hed[data.value]) or nil
		end

		self.check_apply_value_to_all_elements(self, data)
	end

	return 
end
CoreMissionElement.check_apply_value_to_all_elements = function (self, data)
	if EWS:get_key_state("K_CONTROL") then
		local value = tonumber(self._hed[data.value]) or self._hed[data.value]

		for _, unit in ipairs(managers.editor:layer("Mission"):selected_units()) do
			if unit ~= self._unit and unit.mission_element_data(unit) then
				unit.mission_element_data(unit)[data.value] = value

				unit.mission_element(unit):set_panel_dirty()
			end
		end
	end

	return 
end
CoreMissionElement.check_apply_func_to_all_elements = function (self, func_name, data)
	if EWS:get_key_state("K_CONTROL") then
		for _, unit in ipairs(managers.editor:layer("Mission"):selected_units()) do
			if unit ~= self._unit then
				local mission_element = unit.mission_element(unit)

				if mission_element and mission_element[func_name] then
					mission_element[func_name](mission_element, data)
					mission_element.set_panel_dirty(mission_element)
				end
			end
		end
	end

	return 
end
CoreMissionElement.set_panel_dirty = function (self)
	if not alive(self._panel) then
		return 
	end

	self._panel:destroy()

	self._panel = nil

	return 
end
CoreMissionElement.selected = function (self)
	self.append_elements_sorted(self)

	return 
end
CoreMissionElement.update_selected = function (self)
	return 
end
CoreMissionElement.update_unselected = function (self)
	return 
end
CoreMissionElement.can_edit = function (self)
	return self.update_editing or self.base_update_editing
end
CoreMissionElement.begin_editing = function (self)
	return 
end
CoreMissionElement.end_editing = function (self)
	return 
end
CoreMissionElement.clone_data = function (self, all_units)
	for _, data in ipairs(self._hed.on_executed) do
		table.insert(self._on_executed_units, all_units[data.id])
	end

	return 
end
CoreMissionElement.layer_finished = function (self)
	for _, data in ipairs(self._hed.on_executed) do
		local unit = managers.worlddefinition:get_mission_element_unit(data.id)

		table.insert(self._on_executed_units, unit)
	end

	return 
end
CoreMissionElement.save_data = function (self, file, t)
	self.save_values(self, file, t)

	return 
end
CoreMissionElement.save_values = function (self, file, t)
	t = t .. "\t"

	file.puts(file, t .. "<values>")

	for _, name in ipairs(self._save_values) do
		self.save_value(self, file, t, name)
	end

	file.puts(file, t .. "</values>")

	return 
end
CoreMissionElement.save_value = function (self, file, t, name)
	t = t .. "\t"

	file.puts(file, save_value_string(self._hed, name, t, self._unit))

	return 
end
CoreMissionElement.new_save_values = function (self)
	local t = {
		position = (self.SAVE_UNIT_POSITION and self._unit:position()) or nil,
		rotation = (self.SAVE_UNIT_ROTATION and self._unit:rotation()) or nil
	}

	for _, value in ipairs(self._save_values) do
		t[value] = self._hed[value]
	end

	t.base_delay_rand = (self._hed.base_delay_rand and 0 < self._hed.base_delay_rand and self._hed.base_delay_rand) or nil

	if self.save then
		self.save(self, t)
	end

	if self.RANDOMS then
		for _, value_name in ipairs(self.RANDOMS) do
			if t[value_name][2] == 0 then
				t[value_name] = t[value_name][1]
			end
		end
	end

	return t
end
CoreMissionElement.name = function (self)
	return self._unit:name() .. self._ud.unit_id
end
CoreMissionElement.add_to_mission_package = function (self)
	return 
end
CoreMissionElement.get_color = function (self, type)
	if type then
		if type == "activate" or type == "enable" then
			return 0, 1, 0
		elseif type == "deactivate" or type == "disable" then
			return 1, 0, 0
		end
	end

	return 0, 1, 0
end
CoreMissionElement.get_element_color = function (self)
	local r = 1
	local g = 1
	local b = 1

	if self._iconcolor and managers.editor:layer("Mission"):use_colored_links() then
		r = self._iconcolor_c.r
		g = self._iconcolor_c.g
		b = self._iconcolor_c.b
	end

	return r, g, b
end
CoreMissionElement.draw_links_selected = function (self, t, dt, selected_unit)
	local unit = self._current_element_unit(self)

	if alive(unit) then
		local r = 1
		local g = 1
		local b = 1

		if self._iconcolor and managers.editor:layer("Mission"):use_colored_links() then
			r = self._iconcolor_c.r
			g = self._iconcolor_c.g
			b = self._iconcolor_c.b
		end

		self._draw_link(self, {
			thick = true,
			from_unit = self._unit,
			to_unit = unit,
			r = r,
			g = g,
			b = b
		})
	end

	return 
end
CoreMissionElement._draw_link = function (self, params)
	params.draw_flow = managers.editor:layer("Mission"):visualize_flow()

	Application:draw_link(params)

	return 
end
CoreMissionElement.draw_links_unselected = function (self)
	return 
end
CoreMissionElement.clear = function (self)
	return 
end
CoreMissionElement.action_types = function (self)
	return self._action_types
end
CoreMissionElement.timeline_color = function (self)
	return self._timeline_color
end
CoreMissionElement.add_triggers = function (self, vc)
	return 
end
CoreMissionElement.base_add_triggers = function (self, vc)
	if self.USES_POINT_ORIENTATION then
		vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "_on_use_point_orientation"))
	end

	if self.USES_INSTIGATOR_RULES then
		vc.add_trigger(vc, Idstring("lmb"), callback(self, self, "_on_use_instigator_rule"))
	end

	return 
end
CoreMissionElement._on_use_point_orientation = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and string.find(ray.unit:name():s(), "point_orientation", 1, true) then
		local id = ray.unit:unit_data().unit_id

		if self._hed.orientation_elements and table.contains(self._hed.orientation_elements, id) then
			self._remove_orientation_unit_id(self, id)
		else
			self._add_orientation_unit_id(self, id)
		end
	end

	return 
end
CoreMissionElement._add_orientation_unit_id = function (self, id)
	self._hed.orientation_elements = self._hed.orientation_elements or {}

	table.insert(self._hed.orientation_elements, id)

	return 
end
CoreMissionElement._remove_orientation_unit_id = function (self, id)
	table.delete(self._hed.orientation_elements, id)

	self._hed.orientation_elements = (0 < #self._hed.orientation_elements and self._hed.orientation_elements) or nil

	return 
end
CoreMissionElement._on_use_instigator_rule = function (self)
	local ray = managers.editor:unit_by_raycast({
		ray_type = "editor",
		mask = 10
	})

	if ray and ray.unit and string.find(ray.unit:name():s(), "data_instigator_rule", 1, true) then
		local id = ray.unit:unit_data().unit_id

		if self._hed.rules_elements and table.contains(self._hed.rules_elements, id) then
			self._remove_instigator_rule_unit_id(self, id)
		else
			self._add_instigator_rule_unit_id(self, id)
		end
	end

	return 
end
CoreMissionElement._add_instigator_rule_unit_id = function (self, id)
	self._hed.rules_elements = self._hed.rules_elements or {}

	table.insert(self._hed.rules_elements, id)

	return 
end
CoreMissionElement._remove_instigator_rule_unit_id = function (self, id)
	table.delete(self._hed.rules_elements, id)

	self._hed.rules_elements = (0 < #self._hed.rules_elements and self._hed.rules_elements) or nil

	return 
end
CoreMissionElement.__update_editing = function (self, _, t, dt, current_pos)
	return 
end
CoreMissionElement.clear_triggers = function (self)
	return 
end
CoreMissionElement.widget_affect_object = function (self)
	return nil
end
CoreMissionElement.use_widget_position = function (self)
	return nil
end
CoreMissionElement.set_enabled = function (self)
	if self._icon_ws then
		self._icon_ws:show()
	end

	return 
end
CoreMissionElement.set_disabled = function (self)
	if self._icon_ws then
		self._icon_ws:hide()
	end

	return 
end
CoreMissionElement.on_set_visible = function (self, visible)
	if self._icon_ws then
		if visible then
			self._icon_ws:show()
		else
			self._icon_ws:hide()
		end
	end

	return 
end
CoreMissionElement.set_update_selected_on = function (self, value)
	self._update_selected_on = value

	return 
end
CoreMissionElement.update_selected_on = function (self)
	return self._update_selected_on
end
CoreMissionElement.destroy_panel = function (self)
	if self._panel then
		self._panel:extension().alive = false

		self._panel:destroy()

		self._panel = nil
	end

	return 
end
CoreMissionElement.destroy = function (self)
	if self._timeline then
		self._timeline:destroy()
	end

	if self._panel then
		self._panel:extension().alive = false

		self._panel:destroy()
	end

	if self._icon_ws then
		self._icon_gui:destroy_workspace(self._icon_ws)

		self._icon_ws = nil
	end

	return 
end
CoreMissionElement.draw_links = function (self, t, dt, selected_unit, all_units)
	self._base_check_removed_units(self, all_units)
	self.draw_link_on_executed(self, t, dt, selected_unit)
	self._draw_elements(self, t, dt, self._hed.orientation_elements, selected_unit, all_units)
	self._draw_elements(self, t, dt, self._hed.rules_elements, selected_unit, all_units)

	return 
end
CoreMissionElement._base_check_removed_units = function (self, all_units)
	if self._hed.orientation_elements then
		for _, id in ipairs(clone(self._hed.orientation_elements)) do
			local unit = all_units[id]

			if not alive(unit) then
				self._remove_orientation_unit_id(self, id)
			end
		end
	end

	if self._hed.rules_elements then
		for _, id in ipairs(clone(self._hed.rules_elements)) do
			local unit = all_units[id]

			if not alive(unit) then
				self._remove_instigator_rule_unit_id(self, id)
			end
		end
	end

	return 
end
CoreMissionElement._draw_elements = function (self, t, dt, elements, selected_unit, all_units)
	if not elements then
		return 
	end

	for _, id in ipairs(elements) do
		local unit = all_units[id]

		if self._should_draw_link(self, selected_unit, unit) then
			local r, g, b = unit.mission_element(unit):get_link_color()

			self._draw_link(self, {
				from_unit = unit,
				to_unit = self._unit,
				r = r,
				g = g,
				b = b
			})
		end
	end

	return 
end
CoreMissionElement._should_draw_link = function (self, selected_unit, unit)
	return not selected_unit or unit == selected_unit or self._unit == selected_unit
end
CoreMissionElement.get_link_color = function (self, unit)
	local r = 1
	local g = 1
	local b = 1

	if self._iconcolor and managers.editor:layer("Mission"):use_colored_links() then
		r = self._iconcolor_c.r
		g = self._iconcolor_c.g
		b = self._iconcolor_c.b
	end

	return r, g, b
end
CoreMissionElement.draw_link_on_executed = function (self, t, dt, selected_unit)
	local unit_sel = self._unit == selected_unit

	CoreMissionElement.editor_link_brush:set_color((unit_sel and Color.green) or Color.white)

	for _, unit in ipairs(self._on_executed_units) do
		if alive(unit) then
			if not selected_unit or unit_sel or unit == selected_unit then
				local dir = mvector3.copy(unit.position(unit))

				mvector3.subtract(dir, self._unit:position())

				local vec_len = mvector3.normalize(dir)
				local offset = math.min(50, vec_len)

				mvector3.multiply(dir, offset)

				if self._distance_to_camera < 1000000 then
					local text = self._get_delay_string(self, unit.unit_data(unit).unit_id)
					local alternative = self._get_on_executed(self, unit.unit_data(unit).unit_id).alternative

					if alternative then
						text = text .. " - " .. alternative .. ""
					end

					CoreMissionElement.editor_link_brush:center_text(self._unit:position() + dir, text, managers.editor:camera_rotation():x(), -managers.editor:camera_rotation():z())
				end

				local r, g, b = self.get_link_color(self)

				self._draw_link(self, {
					from_unit = self._unit,
					to_unit = unit,
					r = r*0.75,
					g = g*0.75,
					b = b*0.75
				})
			end
		else
			table.delete(self._on_executed_units, unit)
		end
	end

	return 
end
CoreMissionElement._get_delay_string = function (self, unit_id)
	local delay = self._hed.base_delay + self._get_on_executed(self, unit_id).delay
	local text = string.format("%.2f", delay)

	if self._hed.base_delay_rand or self._get_on_executed(self, unit_id).delay_rand then
		local delay_max = delay + (self._get_on_executed(self, unit_id).delay_rand or 0)
		delay_max = delay_max + ((self._hed.base_delay_rand and self._hed.base_delay_rand) or 0)
		text = text .. "-" .. string.format("%.2f", delay_max) .. ""
	end

	return text
end
CoreMissionElement.add_on_executed = function (self, unit, prevent_undo)
	if self.remove_on_execute(self, unit, true) then
		return 
	end

	local command = CoreEditorCommand.MissionElementAddOnExecutedCommand:new(self)

	command.execute(command, self, unit)

	if not prevent_undo then
		managers.editor:register_undo_command(command)
	end

	return 
end
CoreMissionElement.remove_on_execute = function (self, unit, prevent_undo)
	local command = CoreEditorCommand.MissionElementRemoveOnExecutedCommand:new(self)
	local result = command.execute(command, self, unit)

	if result and not prevent_undo then
		managers.editor:register_undo_command(command)
	end

	return result
end
CoreMissionElement.add_link_element = function (self, element_name, unit_id, prevent_undo)
	local command = CoreEditorCommand.MissionElementAddLinkElementCommand:new(self)
	local result = command.execute(command, self, element_name, unit_id)

	if result and not prevent_undo then
		managers.editor:register_undo_command(command)
	end

	return 
end
CoreMissionElement.on_added_link_element = function (self, element_name, unit_id)
	return 
end
CoreMissionElement.remove_link_element = function (self, element_name, unit_id, prevent_undo)
	local command = CoreEditorCommand.MissionElementRemoveLinkElementCommand:new(self)
	local result = command.execute(command, self, element_name, unit_id)

	if result and not prevent_undo then
		managers.editor:register_undo_command(command)
	end

	return 
end
CoreMissionElement.on_removed_link_element = function (self, element_name, unit_id)
	return 
end
CoreMissionElement.remove_links = function (self, unit)
	if not self.LINK_ELEMENTS then
		return 
	end

	local id = nil

	for _, element_name in ipairs(self.LINK_ELEMENTS) do
		if self._hed[element_name] then
			for i = #self._hed[element_name], 1, -1 do
				id = self._hed[element_name][i]

				if id == unit.unit_data(unit).unit_id then
					self.remove_link_element(self, element_name, id)
				end
			end
		end
	end

	return 
end
CoreMissionElement.remove_all_links = function (self)
	if not self.LINK_ELEMENTS then
		return 
	end

	local id = nil

	for _, element_name in ipairs(self.LINK_ELEMENTS) do
		if self._hed[element_name] then
			for i = #self._hed[element_name], 1, -1 do
				self.remove_link_element(self, element_name, self._hed[element_name][i])
			end
		end
	end

	return 
end
CoreMissionElement.delete_unit = function (self, units)
	for i = #self._on_executed_units, 1, -1 do
		self.remove_on_execute(self, self._on_executed_units[i])
	end

	for _, unit in ipairs(units) do
		unit.mission_element(unit):remove_on_execute(self._unit)
		unit.mission_element(unit):remove_links(self._unit)
	end

	self.remove_all_links(self)

	local command = CoreEditorCommand.DeleteMissionElementCommand:new(self)

	command.execute(command, self)
	managers.editor:register_undo_command(command)

	return 
end
CoreMissionElement.set_on_executed_element = function (self, unit, id)
	unit = unit or self.on_execute_unit_by_id(self, id)

	if not alive(unit) then
		self._set_on_execute_ctrlrs_enabled(self, false)
		self._set_first_executed_element(self)

		return 
	end

	self._set_on_execute_ctrlrs_enabled(self, true)

	if self._elements_params then
		local name = self.combobox_name(self, unit)

		CoreEWS.change_combobox_value(self._elements_params, name)
		self.set_on_executed_data(self)
	end

	return 
end
CoreMissionElement.set_on_executed_data = function (self)
	local id = self.combobox_id(self, self._elements_params.value)
	local params = self._get_on_executed(self, id)

	CoreEWS.change_entered_number(self._element_delay_params, params.delay)
	CoreEWS.change_entered_number(self._element_delay_rand_params, params.delay_rand or 0)

	if self._on_executed_alternatives_params then
		CoreEWS.change_combobox_value(self._on_executed_alternatives_params, params.alternative)
	end

	if self._timeline then
		self._timeline:select_element(params)
	end

	return 
end
CoreMissionElement._set_first_executed_element = function (self)
	if 0 < #self._hed.on_executed then
		local unit = self.on_execute_unit_by_id(self, self._hed.on_executed[1].id)

		if alive(unit) then
			self.set_on_executed_element(self, unit)
		else
			print("could not set first executed element! ", self._hed.on_executed[1].id, unit)
		end
	end

	return 
end
CoreMissionElement._set_on_execute_ctrlrs_enabled = function (self, enabled)
	if not self._elements_params then
		return 
	end

	self._elements_params.ctrlr:set_enabled(enabled)
	self._element_delay_params.number_ctrlr:set_enabled(enabled)
	self._element_delay_rand_params.number_ctrlr:set_enabled(enabled)
	self._elements_toolbar:set_enabled(enabled)

	if self._on_executed_alternatives_params then
		self._on_executed_alternatives_params.ctrlr:set_enabled(enabled)
	end

	return 
end
CoreMissionElement.on_executed_element_selected = function (self)
	self.set_on_executed_data(self)

	return 
end
CoreMissionElement._get_on_executed = function (self, id)
	for _, params in ipairs(self._hed.on_executed) do
		if params.id == id then
			return params
		end
	end

	return 
end
CoreMissionElement._current_element_id = function (self)
	if not self._elements_params or not self._elements_params.value then
		return nil
	end

	return self.combobox_id(self, self._elements_params.value)
end
CoreMissionElement._current_element_unit = function (self)
	local id = self._current_element_id(self)

	if not id then
		return nil
	end

	local unit = self.on_execute_unit_by_id(self, id)

	if not alive(unit) then
		return nil
	end

	return unit
end
CoreMissionElement.on_executed_element_delay = function (self)
	local id = self.combobox_id(self, self._elements_params.value)
	local params = self._get_on_executed(self, id)
	params.delay = self._element_delay_params.value

	if self._timeline then
		self._timeline:delay_updated(params)
	end

	return 
end
CoreMissionElement.on_executed_element_delay_rand = function (self)
	local id = self.combobox_id(self, self._elements_params.value)
	local params = self._get_on_executed(self, id)
	params.delay_rand = (0 < self._element_delay_rand_params.value and self._element_delay_rand_params.value) or nil

	return 
end
CoreMissionElement.on_executed_alternatives_types = function (self)
	local id = self.combobox_id(self, self._elements_params.value)
	local params = self._get_on_executed(self, id)

	print("self._on_executed_alternatives_params.value", self._on_executed_alternatives_params.value)

	params.alternative = self._on_executed_alternatives_params.value

	return 
end
CoreMissionElement.append_elements_sorted = function (self)
	if not self._elements_params then
		return 
	end

	local id = self._current_element_id(self)
	local found = false

	for _, data in ipairs(self._hed.on_executed) do
		if data.id == id then
			found = true

			break
		end
	end

	if not found and 0 < #self._hed.on_executed then
		id = self._hed.on_executed[1].id
	end

	CoreEWS.update_combobox_options(self._elements_params, self._combobox_names_names(self, self._on_executed_units))

	if #self._hed.on_executed < 1 then
		return 
	end

	self.set_on_executed_element(self, nil, id)

	return 
end
CoreMissionElement.combobox_name = function (self, unit)
	return unit.unit_data(unit).name_id .. " (" .. unit.unit_data(unit).unit_id .. ")"
end
CoreMissionElement.combobox_id = function (self, name)
	local s = nil
	local e = string.len(name) - 1

	for i = string.len(name), 0, -1 do
		local t = string.sub(name, i, i)

		if t == "(" then
			s = i + 1

			break
		end
	end

	return tonumber(string.sub(name, s, e))
end
CoreMissionElement.on_execute_unit_by_id = function (self, id)
	for _, unit in ipairs(self._on_executed_units) do
		if unit.unit_data(unit).unit_id == id then
			return unit
		end
	end

	return nil
end
CoreMissionElement._combobox_names_names = function (self, units)
	local names = {}

	for _, unit in ipairs(units) do
		table.insert(names, self.combobox_name(self, unit))
	end

	return names
end
CoreMissionElement.on_timeline = function (self)
	if not self._timeline then
		self._timeline = MissionElementTimeline:new(self._unit:unit_data().name_id)

		self._timeline:set_mission_unit(self._unit)
	else
		self._timeline:set_visible(true)
	end

	return 
end
CoreMissionElement._build_value_combobox = function (self, panel, sizer, value_name, options, tooltip, custom_name, params)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, (params and params.horizontal_sizer_proportions) or 0, 1, "EXPAND,LEFT")

	local combobox_params = {
		sizer_proportions = 1,
		name_proportions = 1,
		sorted = false,
		ctrlr_proportions = 2,
		name = string.pretty(custom_name or value_name, true) .. ":",
		panel = panel,
		sizer = horizontal_sizer,
		options = options,
		value = self._hed[value_name],
		tooltip = tooltip or "Select an option from the combobox"
	}
	local ctrlr = CoreEws.combobox(combobox_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_COMBOBOX_SELECTED", callback(self, self, "set_element_data"), {
		ctrlr = ctrlr,
		value = value_name
	})

	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "SELECT_NAME_LIST", "Select from list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "SELECT_NAME_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_on_gui_value_combobox_toolbar_select_dialog"), {
		combobox_params = combobox_params,
		value_name = value_name
	})
	toolbar.realize(toolbar)
	horizontal_sizer.add(horizontal_sizer, toolbar, 0, 1, "EXPAND,LEFT")

	return ctrlr, combobox_params
end
CoreMissionElement._on_gui_value_combobox_toolbar_select_dialog = function (self, params)
	local dialog = SelectNameModal:new("Select name", params.combobox_params.options)

	if dialog.cancelled(dialog) then
		return 
	end

	for _, name in ipairs(dialog._selected_item_assets(dialog)) do
		CoreEws.change_combobox_value(params.combobox_params, name)
		self.set_element_data(self, {
			ctrlr = params.combobox_params.ctrlr,
			value = params.value_name
		})
	end

	return 
end
CoreMissionElement._build_value_number = function (self, panel, sizer, value_name, options, tooltip, custom_name)
	local number_params = {
		name = string.pretty(custom_name or value_name, true) .. ":",
		panel = panel,
		sizer = sizer,
		value = self._hed[value_name],
		floats = options.floats,
		tooltip = tooltip or "Set a number value",
		min = options.min,
		max = options.max,
		name_proportions = options.name_proportions or 1,
		ctrlr_proportions = options.ctrlr_proportions or 2,
		sizer_proportions = options.sizer_proportions
	}
	local ctrlr = CoreEws.number_controller(number_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "set_element_data"), {
		ctrlr = ctrlr,
		value = value_name
	})
	ctrlr.connect(ctrlr, "EVT_KILL_FOCUS", callback(self, self, "set_element_data"), {
		ctrlr = ctrlr,
		value = value_name
	})

	return ctrlr, number_params
end
CoreMissionElement._build_value_checkbox = function (self, panel, sizer, value_name, tooltip, custom_name)
	local checkbox = EWS:CheckBox(panel, custom_name or string.pretty(value_name, true), "")

	checkbox.set_value(checkbox, self._hed[value_name])
	checkbox.set_tool_tip(checkbox, tooltip or "Click to toggle")
	checkbox.connect(checkbox, "EVT_COMMAND_CHECKBOX_CLICKED", callback(self, self, "set_element_data"), {
		ctrlr = checkbox,
		value = value_name
	})
	sizer.add(sizer, checkbox, 0, 0, "EXPAND")

	return checkbox
end
CoreMissionElement._build_value_random_number = function (self, panel, sizer, value_name, options, tooltip, custom_name)
	local horizontal_sizer = EWS:BoxSizer("HORIZONTAL")

	sizer.add(sizer, horizontal_sizer, 0, 0, "EXPAND,LEFT")

	local number_params = {
		name = string.pretty(custom_name or value_name, true) .. ":",
		panel = panel,
		sizer = horizontal_sizer,
		value = self._hed[value_name][1],
		floats = options.floats,
		tooltip = tooltip or "Set a number value",
		min = options.min,
		max = options.max,
		name_proportions = options.name_proportions or 2,
		ctrlr_proportions = options.ctrlr_proportions or 2,
		sizer_proportions = options.sizer_proportions or 2
	}
	local ctrlr = CoreEws.number_controller(number_params)

	ctrlr.connect(ctrlr, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "_set_random_number_element_data"), {
		index = 1,
		ctrlr = ctrlr,
		value = value_name
	})
	ctrlr.connect(ctrlr, "EVT_KILL_FOCUS", callback(self, self, "_set_random_number_element_data"), {
		index = 1,
		ctrlr = ctrlr,
		value = value_name
	})

	local number2_params = {
		name = "+ random:",
		tooltip = "Add a random amount",
		panel = panel,
		sizer = horizontal_sizer,
		value = self._hed[value_name][2],
		floats = options.floats,
		min = options.min,
		max = options.max,
		name_proportions = options.name_proportions or 1,
		ctrlr_proportions = options.ctrlr_proportions or 2,
		sizer_proportions = options.sizer_proportions or 1
	}
	local ctrlr2 = CoreEws.number_controller(number2_params)

	ctrlr2.connect(ctrlr2, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "_set_random_number_element_data"), {
		index = 2,
		ctrlr = ctrlr2,
		value = value_name
	})
	ctrlr2.connect(ctrlr2, "EVT_KILL_FOCUS", callback(self, self, "_set_random_number_element_data"), {
		index = 2,
		ctrlr = ctrlr2,
		value = value_name
	})

	return ctrlr, number_params
end
CoreMissionElement._set_random_number_element_data = function (self, data)
	print("_set_random_number_element_data", inspect(data))

	local value = data.ctrlr:get_value()
	value = tonumber(value)

	print("data.ctrlr:get_value()", value, type(value))
	print("self._hed[ data.value ]", inspect(self._hed[data.value]))

	self._hed[data.value][data.index] = value

	return 
end
CoreMissionElement._build_add_remove_unit_from_list = function (self, panel, sizer, elements, names, exact_names)
	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_add_unit_list_btn"), {
		elements = elements,
		names = names,
		exact_names = exact_names
	})
	toolbar.add_tool(toolbar, "REMOVE_UNIT_LIST", "Remove unit from unit list", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	toolbar.connect(toolbar, "REMOVE_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_remove_unit_list_btn"), {
		elements = elements
	})
	toolbar.realize(toolbar)
	sizer.add(sizer, toolbar, 0, 1, "EXPAND,LEFT")

	return 
end
CoreMissionElement._add_unit_list_btn = function (self, params)
	local elements = params.elements or {}
	local script = self._unit:mission_element_data().script

	local function f_correct_unit(unit)
		if not params.names and not params.exact_names then
			return true
		end

		local u_name = unit.name(unit):s()

		if params.exact_names then
			for _, name in ipairs(params.exact_names) do
				if u_name == name then
					return true
				end
			end
		end

		if params.names then
			for _, name in ipairs(params.names) do
				if string.find(u_name, name, 1, true) then
					return true
				end
			end
		end

		return false
	end

	local function f(unit)
		if not unit.mission_element_data(unit) or unit.mission_element_data(unit).script ~= script then
			return 
		end

		local id = unit.unit_data(unit).unit_id

		if table.contains(elements, id) then
			return false
		end

		if f_correct_unit(unit) then
			return true
		end

		return false
	end

	local dialog = SelectUnitByNameModal:new("Add Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		table.insert(elements, id)
	end

	return 
end
CoreMissionElement._remove_unit_list_btn = function (self, params)
	local elements = params.elements

	local function f(unit)
		return table.contains(elements, unit.unit_data(unit).unit_id)
	end

	local dialog = SelectUnitByNameModal:new("Remove Unit", f)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		table.delete(elements, id)
	end

	return 
end
CoreMissionElement._build_add_remove_static_unit_from_list = function (self, panel, sizer, params)
	local toolbar = EWS:ToolBar(panel, "", "TB_FLAT,TB_NODIVIDER")

	toolbar.add_tool(toolbar, "ADD_UNIT_LIST", "Add unit from unit list", CoreEws.image_path("world_editor\\unit_by_name_list.png"), nil)
	toolbar.connect(toolbar, "ADD_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_add_static_unit_list_btn"), params)
	toolbar.add_tool(toolbar, "REMOVE_UNIT_LIST", "Remove unit from unit list", CoreEws.image_path("toolbar\\delete_16x16.png"), nil)
	toolbar.connect(toolbar, "REMOVE_UNIT_LIST", "EVT_COMMAND_MENU_SELECTED", callback(self, self, "_remove_static_unit_list_btn"), params)
	toolbar.realize(toolbar)
	sizer.add(sizer, toolbar, 0, 1, "EXPAND,LEFT")

	return 
end
CoreMissionElement._add_static_unit_list_btn = function (self, params)
	local dialog = (params.single and SingleSelectUnitByNameModal) or SelectUnitByNameModal:new("Add Unit", params.add_filter)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		local id = unit.unit_data(unit).unit_id

		params.add_result(unit)
	end

	return 
end
CoreMissionElement._remove_static_unit_list_btn = function (self, params)
	local dialog = (params.single and SingleSelectUnitByNameModal) or SelectUnitByNameModal:new("Remove Unit", params.remove_filter)

	for _, unit in ipairs(dialog.selected_units(dialog)) do
		params.remove_result(unit)
	end

	return 
end
CoreMissionElement.get_links_to_unit = function (self, to_unit, links, all_units)
	if to_unit == self._unit then
		for _, data in ipairs(self._hed.on_executed) do
			local on_executed_unit = all_units[data.id]
			local delay = self._get_delay_string(self, on_executed_unit.unit_data(on_executed_unit).unit_id)
			local type = "on_executed" .. ((data.alternative and " " .. data.alternative) or "")

			table.insert(links.on_executed, {
				unit = on_executed_unit,
				alternative = type,
				delay = delay
			})
		end
	end

	for _, data in ipairs(self._hed.on_executed) do
		local unit = all_units[data.id]

		if unit == to_unit then
			local delay = self._get_delay_string(self, unit.unit_data(unit).unit_id)
			local type = "on_executed" .. ((data.alternative and " " .. data.alternative) or "")

			table.insert(links.executers, {
				unit = self._unit,
				alternative = type,
				delay = delay
			})
		end
	end

	return 
end
CoreMissionElement._get_links_of_type_from_elements = function (self, elements, type, to_unit, links, all_units)
	local links1 = (type == "operator" and links.on_executed) or (type == "trigger" and links.executers) or (type == "filter" and links.executers) or links.on_executed
	local links2 = (type == "operator" and links.executers) or (type == "trigger" and links.on_executed) or (type == "filter" and links.on_executed) or links.executers
	local to_unit_id = to_unit.unit_data(to_unit).unit_id

	for _, id in ipairs(elements) do
		if to_unit == self._unit then
			table.insert(links1, {
				unit = all_units[id],
				alternative = type
			})
		elseif id == to_unit_id then
			table.insert(links2, {
				unit = self._unit,
				alternative = type
			})
		end
	end

	return 
end

return 
