CoreDeprecatedHubElement = CoreDeprecatedHubElement or class(HubElement)
DeprecatedHubElement = DeprecatedHubElement or class(CoreDeprecatedHubElement)
DeprecatedHubElement.init = function (self, ...)
	CoreDeprecatedHubElement.init(self, ...)

	return 
end
CoreDeprecatedHubElement.init = function (self, unit)
	HubElement.init(self, unit)

	return 
end
CoreDeprecatedHubElement._build_panel = function (self, panel, panel_sizer)
	self._create_panel(self)

	panel = panel or self._panel
	panel_sizer = panel_sizer or self._panel_sizer
	local deprecated_sizer = EWS:BoxSizer("VERTICAL")

	deprecated_sizer.add(deprecated_sizer, EWS:StaticText(panel, "This hub element is deprecated", 0, ""), 1, 0, "ALIGN_CENTER")
	deprecated_sizer.add(deprecated_sizer, EWS:StaticText(panel, "Please remove", 0, ""), 1, 0, "ALIGN_CENTER")
	deprecated_sizer.add(deprecated_sizer, EWS:StaticText(panel, "", 0, ""), 1, 0, "ALIGN_CENTER")
	deprecated_sizer.add(deprecated_sizer, EWS:StaticText(panel, "Have a nice day!", 0, ""), 1, 0, "ALIGN_CENTER")
	panel_sizer.add(panel_sizer, deprecated_sizer, 0, 0, "EXPAND")

	return 
end

return 
