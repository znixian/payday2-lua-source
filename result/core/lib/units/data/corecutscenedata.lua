CoreCutsceneData = CoreCutsceneData or class()
CutsceneData = CoreCutsceneData
CoreCutsceneData.init = function (self, unit)
	self.__unit = assert(unit, "Unit not supplied to CoreCutsceneData.")
	self.__cutscene_name = self._cutscene_name(self, unit.name(unit))

	self.cutscene_player(self, true, true)
	managers.cutscene:register_unit_with_cutscene_data_extension(self.__unit)

	return 
end
CoreCutsceneData.destroy = function (self)
	self.destroy_cutscene_player(self)
	managers.cutscene:unregister_unit_with_cutscene_data_extension(self.__unit)

	self.__unit = nil
	self.__cutscene_name = nil

	return 
end
CoreCutsceneData.cutscene_player = function (self, __skip_stall_warning, __skip_priming)
	if self.__cutscene_player == nil then
		local cutscene = managers.cutscene:get_cutscene(self.__cutscene_name)

		if not __skip_stall_warning then
			cat_print("spam", "[CoreCutsceneData] The cutscene \"" .. cutscene.name(cutscene) .. "\" has been cleaned up. Call CoreCutsceneData:reset_cutscene_player() before attempting to replay it.")
		end

		self.__cutscene_player = core_or_local("CutscenePlayer", cutscene)

		self.__cutscene_player:add_keys()

		if not __skip_priming then
			self.__cutscene_player:prime()
		end

		local actual_destroy_func = self.__cutscene_player.destroy
		self.__cutscene_player.destroy = function (instance)
			assert(instance == self.__cutscene_player)

			self.__cutscene_player = nil

			actual_destroy_func(instance)

			return 
		end
	end

	return self.__cutscene_player
end
CoreCutsceneData.destroy_cutscene_player = function (self)
	if self.__cutscene_player then
		self.__cutscene_player:destroy()
		assert(self.__cutscene_player == nil)
	end

	return 
end
CoreCutsceneData.reset_cutscene_player = function (self)
	self.destroy_cutscene_player(self)
	self.cutscene_player(self, true)

	return 
end
CoreCutsceneData._cutscene_name = function (self, unit_type_name)
	return string.match(unit_type_name, "cutscene_(.+)")
end

return 
