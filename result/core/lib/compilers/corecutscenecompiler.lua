require("core/lib/compilers/CoreCompilerSystem")
require("core/lib/compilers/CoreCutsceneOptimizer")
require("core/lib/utils/dev/tools/cutscene_editor/CoreCutsceneEditorProject")
require("core/lib/utils/dev/tools/cutscene_editor/CoreCutsceneFootage")

CoreCutsceneCompiler = CoreCutsceneCompiler or class()
CoreCutsceneCompiler.compile = function (self, file, dest, force_recompile)
	if file.type ~= "cutscene" then
		return false
	end

	if not force_recompile and dest.up_to_date(dest, file.path, "cutscene", file.name, file.properties) then
		dest.skip_update(dest, "cutscene", file.name, file.properties)

		return true
	end

	cat_print("spam", "Compiling " .. file.path)

	local project = assert(self._load_project(self, file.path), string.format("Failed to load cutscene \"%s\".", file.path))
	local optimizer = self._create_optimizer_for_project(self, project)

	if optimizer.is_valid(optimizer) then
		front.optimizer:export_to_compile_destination(dest, file.name)
		front.optimizer:free_cached_animations()
	else
		local error_msg = string.format("Cutscene \"%s\" is invalid:", file.path)

		for _, problem in ipairs(optimizer.problems(optimizer)) do
			error_msg = error_msg .. "\t" .. problem
		end

		Application:error(error_msg)
	end

	return true
end
CoreCutsceneCompiler._load_project = function (self, path)
	if managers.database:has(path) then
		local project = CoreCutsceneEditorProject:new()

		project.set_path(project, path)

		return project
	end

	return nil
end
CoreCutsceneCompiler._create_optimizer_for_project = function (self, project)
	local optimizer = CoreCutsceneOptimizer:new()

	optimizer.set_compression_enabled(optimizer, "win32", project.export_type(project) == "in_game_use")

	local exported_clip_descriptors = table.find_all_values(project.film_clips(project), function (clip)
		return clip.track_index == 1
	end)

	for _, clip_descriptor in ipairs(exported_clip_descriptors) do
		local clip = self._create_clip(self, clip_descriptor)

		optimizer.add_clip(optimizer, clip)
	end

	for _, key in ipairs(project.cutscene_keys(project)) do
		optimizer.add_key(optimizer, key)
	end

	for unit_name, patches in pairs(project.animation_patches(project)) do
		slot9 = pairs
		slot10 = patches or {}

		for blend_set, animation in slot9(slot10) do
			optimizer.add_animation_patch(optimizer, unit_name, blend_set, animation)
		end
	end

	return optimizer
end
CoreCutsceneCompiler._create_clip = function (self, clip_descriptor)
	local cutscene = managers.cutscene:get_cutscene(clip_descriptor.cutscene)
	local footage = assert(CoreCutsceneFootage:new(cutscene), "Cutscene \"" .. clip_descriptor.cutscene .. "\" does not exist.")
	local clip = footage.create_clip(footage, clip_descriptor.from, clip_descriptor.to, clip_descriptor.camera)

	clip.offset_by(clip, clip_descriptor.offset - clip_descriptor.from)

	return clip
end

return 
