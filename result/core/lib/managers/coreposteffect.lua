CorePostEffectModifier = CorePostEffectModifier or class()
CorePostEffectModifier.init = function (self)
	self._params = {}

	return 
end
CorePostEffectModifier.add = function (self, from)
	for key, value in pairs(from._params) do
		if self._params[key] then
			self._params[key] = self._params[key] + from._params[key]
		elseif type(from._params[key]) ~= "number" then
			self._params[key] = Vector3(from._params[key].x, from._params[key].y, from._params[key].z)
		else
			self._params[key] = from._params[key]
		end
	end

	return 
end
CorePostEffectModifier.scale = function (self, scale)
	for key, value in pairs(self._params) do
		self._params[key] = self._params[key]*scale
	end

	return 
end
CorePostEffectModifier.copy = function (self, from)
	for key, value in pairs(from._params) do
		if type(value) ~= "number" then
			self._params[key] = Vector3(value.x, value.y, value.z)
		else
			self._params[key] = value
		end
	end

	return 
end
CorePostEffectModifier.interpolate = function (self, postfx, with, scale)
	for key, value in pairs(postfx._params) do
		if type(value) ~= "string" then
			if not with._params[key] then
				self._params[key] = nil
			else
				local invscale = scale - 1
				self._params[key] = postfx._params[key]*invscale + with._params[key]*scale
			end
		end
	end

	return 
end
CorePostEffectModifier.interpolate_value = function (self, postfx, with, key, scale)
	if not with._params[key] or not postfx._params[key] then
		return 
	else
		local invscale = scale - 1
		self._params[key] = postfx._params[key]*invscale + with._params[key]*scale
	end

	return 
end
CorePostEffectModifier.parse = function (self, xml_node)
	self._params = {}

	for child in xml_node.children(xml_node) do
		local key = child.parameter(child, "key")
		local value = child.parameter(child, "value")

		if child.name(child) == "param" and key and key ~= "" and value and value ~= "" then
			if math.string_is_good_vector(value) then
				self._params[key] = math.string_to_vector(value)
			elseif tonumber(value) then
				self._params[key] = tonumber(value)
			elseif string.sub(value, 1, 1) == "#" then
				self._params[key] = self.database_lookup(self, string.sub(value, 2))
			else
				self._params[key] = tostring(value)
			end
		end
	end

	return 
end
CorePostEffectModifier.set_value = function (self, key, value)
	self._params[key] = value

	return 
end
CorePostEffectModifier.value = function (self, key)
	return self._params[key]
end
CorePostEffectModifier.database_lookup = function (self, str)
	local i = string.find(str, "#")
	local db_key = string.sub(str, 1, i - 1)
	local value_key = string.sub(str, i + 1)

	assert(db_key == "LightIntensityDB")

	local value = LightIntensityDB:lookup(value_key)

	assert(value)

	return value
end
CorePostProcessor = CorePostProcessor or class()
CorePostProcessor.init = function (self)
	self._modifiers = {}

	return 
end
CorePostProcessor.add = function (self, from)
	for name, modifier in pairs(from._modifiers) do
		if not self._modifiers[name] then
			self._modifiers[name] = CorePostEffectModifier:new()
		end

		modifier.add(modifier, from._modifiers[name])
	end

	return 
end
CorePostProcessor.scale = function (self, scale)
	for name, modifier in pairs(self._modifiers) do
		modifier.scale(modifier, scale)
	end

	return 
end
CorePostProcessor.copy = function (self, from)
	for name, modifier in pairs(from._modifiers) do
		if not self._modifiers[name] then
			self._modifiers[name] = CorePostEffectModifier:new()
		end

		self._modifiers[name]:copy(modifier)
	end

	return 
end
CorePostProcessor.interpolate = function (self, postfx, with, scale)
	for name, modifier in pairs(postfx._modifiers) do
		if not with._modifiers[name] then
			with._modifiers[name] = CorePostEffectModifier:new()
		end

		if not self._modifiers[name] then
			self._modifiers[name] = CorePostEffectModifier:new()
		end
	end

	for name, modifier in pairs(with._modifiers) do
		if not postfx._modifiers[name] then
			postfx._modifiers[name] = CorePostEffectModifier:new()
		end

		if not self._modifiers[name] then
			self._modifiers[name] = CorePostEffectModifier:new()
		end
	end

	for name, modifier in pairs(self._modifiers) do
		modifier.interpolate(modifier, postfx._modifiers[name], with._modifiers[name], scale)
	end

	return 
end
CorePostProcessor.interpolate_value = function (self, postfx, with, modifier, key, scale)
	if not with._modifiers[modifier] or not postfx._modifiers[modifier] then
		return 
	end

	if not self._modifiers[modifier] then
		self._modifiers[modifier] = CorePostEffectModifier:new()
	end

	self._modifiers[modifier]:interpolate_value(postfx._modifiers[modifier], with._modifiers[modifier], key, scale)

	return 
end
CorePostProcessor.parse = function (self, xml_node)
	for child in xml_node.children(xml_node) do
		local name = child.parameter(child, "name")

		if child.name(child) == "modifier" and name and name ~= "" then
			if not self._modifiers[name] then
				self._modifiers[name] = CorePostEffectModifier:new()
			end

			self._modifiers[name]:parse(child)
		end
	end

	return 
end
CorePostProcessor.set_value = function (self, modifier, key, value)
	if not self._modifiers[modifier] then
		self._modifiers[modifier] = CorePostEffectModifier:new()
	end

	self._modifiers[modifier]:set_value(key, value)

	return 
end
CorePostProcessor.value = function (self, modifier, key)
	if self._modifiers[modifier] then
		return self._modifiers[modifier]:value(key)
	else
		return nil
	end

	return 
end
CorePostEffect = CorePostEffect or class()
CorePostEffect.init = function (self)
	self.set_default(self)

	return 
end
CorePostEffect.set_name = function (self, name)
	self._name = name

	return 
end
CorePostEffect.set_default = function (self)
	self._post_processors = {}
	self._name = "default"

	return 
end
CorePostEffect.add = function (self, from)
	for name, processor in pairs(from._post_processors) do
		if not self._post_processors[name] then
			self._post_processors[name] = CorePostProcessor:new()
			self._post_processors[name]._effect = processor._effect
		end

		processor.add(processor, from._post_processors[name])
	end

	return 
end
CorePostEffect.scale = function (self, scale)
	for name, processor in pairs(self._post_processors) do
		processor.scale(processor, scale)
	end

	return 
end
CorePostEffect.copy = function (self, from)
	for name, processor in pairs(from._post_processors) do
		if not self._post_processors[name] then
			self._post_processors[name] = CorePostProcessor:new()
			self._post_processors[name]._effect = processor._effect
		end

		self._post_processors[name]:copy(processor)
	end

	self._name = from._name

	return 
end
CorePostEffect.interpolate = function (self, postfx, with, scale)
	for name, processor in pairs(postfx._post_processors) do
		if not with._post_processors[name] then
			with._post_processors[name] = CorePostProcessor:new()
			with._post_processors[name]._effect = processor._effect
		end

		if not self._post_processors[name] then
			self._post_processors[name] = CorePostProcessor:new()
			self._post_processors[name]._effect = processor._effect
		end
	end

	for name, processor in pairs(with._post_processors) do
		if not postfx._post_processors[name] then
			postfx._post_processors[name] = CorePostProcessor:new()
			postfx._post_processors[name]._effect = processor._effect
		end

		if not self._post_processors[name] then
			self._post_processors[name] = CorePostProcessor:new()
			self._post_processors[name]._effect = processor._effect
		end
	end

	for name, processor in pairs(self._post_processors) do
		processor.interpolate(processor, postfx._post_processors[name], with._post_processors[name], scale)
	end

	self._name = postfx._name

	return 
end
CorePostEffect.interpolate_value = function (self, postfx, with, processor, modifier, key, scale)
	if not with._post_processors[processor] or not postfx._post_processors[processor] then
		return 
	end

	if not self._post_processors[processor] then
		self._post_processors[processor] = CorePostProcessor:new()
		self._post_processors[processor]._effect = processor._effect
	end

	self._name = postfx._name

	self._post_processors[processor]:interpolate_value(postfx._post_processors[processor], with._post_processors[processor], modifier, key, scale)

	return 
end
CorePostEffect.parse = function (self, xml_node)
	for child in xml_node.children(xml_node) do
		local name = child.parameter(child, "name")
		local effect = child.parameter(child, "effect")

		if child.name(child) == "post_processor" and name and name ~= "" and effect and effect ~= "" then
			if not self._post_processors[name] then
				self._post_processors[name] = CorePostProcessor:new()
			end

			self._post_processors[name]._effect = effect

			self._post_processors[name]:parse(child)
		end
	end

	return 
end
CorePostEffect.set_value = function (self, processor, modifier, key, value)
	if not self._post_processors[processor] then
		self._post_processors[processor] = CorePostProcessor:new()
	end

	self._post_processors[processor]:set_value(modifier, key, value)

	return 
end
CorePostEffect.value = function (self, processor, modifier, key)
	if self._post_processors[processor] then
		return self._post_processors[processor]:value(modifier, key)
	else
		return nil
	end

	return 
end

return 
