core:module("CoreEnvironmentAreaManager")
core:import("CoreShapeManager")
core:import("CoreEnvironmentFeeder")

EnvironmentAreaManager = EnvironmentAreaManager or class()
EnvironmentAreaManager.POSITION_OFFSET = 50
EnvironmentAreaManager.init = function (self)
	self._areas = {}
	self._blocks = 0

	self.set_default_transition_time(self, 0.1)
	self.set_default_bezier_curve(self, {
		0,
		0,
		1,
		1
	})

	local skip_default_list = {
		[CoreEnvironmentFeeder.UnderlayPathFeeder.FILTER_CATEGORY] = true,
		[CoreEnvironmentFeeder.CubeMapTextureFeeder.FILTER_CATEGORY] = true,
		[CoreEnvironmentFeeder.WorldOverlayTextureFeeder.FILTER_CATEGORY] = true,
		[CoreEnvironmentFeeder.WorldOverlayMaskTextureFeeder.FILTER_CATEGORY] = true,
		[CoreEnvironmentFeeder.PostShadowSlice0Feeder.FILTER_CATEGORY] = true
	}
	local default_filter_list = {}

	for name, data_path_key_list in pairs(managers.viewport:get_predefined_environment_filter_map()) do
		if not skip_default_list[name] then
			for _, data_path_key in ipairs(data_path_key_list) do
				table.insert(default_filter_list, data_path_key)
			end
		end
	end

	self.set_default_filter_list(self, default_filter_list)

	return 
end
EnvironmentAreaManager.set_default_transition_time = function (self, time)
	self._default_transition_time = time

	return 
end
EnvironmentAreaManager.default_transition_time = function (self)
	return self._default_transition_time
end
EnvironmentAreaManager.set_default_bezier_curve = function (self, bezier_curve)
	self._default_bezier_curve = bezier_curve

	return 
end
EnvironmentAreaManager.default_bezier_curve = function (self)
	return self._default_bezier_curve
end
EnvironmentAreaManager.set_default_filter_list = function (self, filter_list)
	self._default_filter_list = filter_list

	return 
end
EnvironmentAreaManager.default_filter_list = function (self)
	if self._default_filter_list then
		return table.list_copy(self._default_filter_list)
	else
		return nil
	end

	return 
end
EnvironmentAreaManager.default_prio = function (self)
	return 100
end
EnvironmentAreaManager.areas = function (self)
	return self._areas
end
EnvironmentAreaManager.add_area = function (self, area_params)
	local area = EnvironmentArea:new(area_params)

	table.insert(self._areas, area)
	self.prio_order_areas(self)

	return area
end
EnvironmentAreaManager.prio_order_areas = function (self)
	table.sort(self._areas, function (a, b)
		return a.is_higher_prio(a, b.prio(b))
	end)

	return 
end
EnvironmentAreaManager.remove_area = function (self, area)
	for _, vp in ipairs(managers.viewport:viewports()) do
		vp.on_environment_area_removed(vp, area)
	end

	table.delete(self._areas, area)

	return 
end
EnvironmentAreaManager.update = function (self, t, dt)
	local vps = managers.viewport:all_really_active_viewports()

	for _, vp in ipairs(vps) do
		if 0 < self._blocks then
			return 
		end

		vp.update_environment_area(vp, self._areas, self.POSITION_OFFSET)
	end

	return 
end
EnvironmentAreaManager.environment_at_position = function (self, pos)
	local environment = managers.viewport:default_environment()

	for _, area in ipairs(self._areas) do
		if area.is_inside(area, pos) then
			environment = area.environment(area)

			break
		end
	end

	return environment
end
EnvironmentAreaManager.add_block = function (self)
	self._blocks = self._blocks + 1

	return 
end
EnvironmentAreaManager.remove_block = function (self)
	self._blocks = self._blocks - 1

	return 
end
EnvironmentArea = EnvironmentArea or class(CoreShapeManager.ShapeBox)
EnvironmentArea.init = function (self, params)
	params.type = "box"

	EnvironmentArea.super.init(self, params)

	self._properties.name = params.name
	self._properties.environment = params.environment or managers.viewport:game_default_environment()
	self._properties.permanent = params.permanent or false
	self._properties.transition_time = params.transition_time or managers.environment_area:default_transition_time()
	self._properties.bezier_curve = params.bezier_curve or managers.environment_area:default_bezier_curve()
	self._properties.filter_list = params.filter_list or managers.environment_area:default_filter_list()
	self._properties.prio = params.prio or managers.environment_area:default_prio()

	self._generate_id(self)

	return 
end
EnvironmentArea._generate_id = function (self)
	local filter_list_id = ""

	if self._properties.filter_list then
		for _, data_path_key in pairs(self._properties.filter_list) do
			filter_list_id = filter_list_id .. "," .. data_path_key
		end
	end

	self._id = self._properties.environment .. filter_list_id:key()

	return 
end
EnvironmentArea.save_level_data = function (self)
	local unit = self.unit(self)

	if unit then
		self._properties.name = self._unit:unit_data().name_id
	end

	return EnvironmentArea.super.save_level_data(self)
end
EnvironmentArea.set_unit = function (self, unit)
	EnvironmentArea.super.set_unit(self, unit)

	if unit and self._properties.name then
		return self._properties.name
	else
		return nil
	end

	return 
end
EnvironmentArea.id = function (self)
	return self._id
end
EnvironmentArea.environment = function (self)
	return self.property(self, "environment")
end
EnvironmentArea.set_environment = function (self, environment)
	self.set_property_string(self, "environment", environment)
	self._generate_id(self)

	return 
end
EnvironmentArea.permanent = function (self)
	return self.property(self, "permanent")
end
EnvironmentArea.set_permanent = function (self, permanent)
	self._properties.permanent = permanent

	return 
end
EnvironmentArea.transition_time = function (self)
	return self.property(self, "transition_time")
end
EnvironmentArea.set_transition_time = function (self, time)
	self._properties.transition_time = time

	return 
end
EnvironmentArea.bezier_curve = function (self)
	return self.property(self, "bezier_curve")
end
EnvironmentArea.set_bezier_curve = function (self, bezier_curve)
	self._properties.bezier_curve = bezier_curve

	return 
end
EnvironmentArea.filter_list = function (self)
	return self.property(self, "filter_list")
end
EnvironmentArea.set_filter_list = function (self, filter_list)
	self._properties.filter_list = filter_list

	self._generate_id(self)

	return 
end
EnvironmentArea.prio = function (self)
	return self.property(self, "prio")
end
EnvironmentArea.set_prio = function (self, prio)
	if self._properties.prio ~= prio then
		self._properties.prio = prio

		managers.environment_area:prio_order_areas()
	end

	return 
end
EnvironmentArea.is_higher_prio = function (self, min_prio)
	if min_prio then
		return self._properties.prio < min_prio
	else
		return true
	end

	return 
end

return 
