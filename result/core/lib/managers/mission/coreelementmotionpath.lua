core:module("CoreElementMotionPath")
core:import("CoreMissionScriptElement")

ElementMotionPathOperator = ElementMotionPathOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementMotionPathOperator.init = function (self, ...)
	ElementMotionPathOperator.super.init(self, ...)

	return 
end
ElementMotionPathOperator.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementMotionPathOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.operation == "activate_bridge" then
		managers.motion_path:motion_operation_activate_bridge(self._values.elements)
	elseif self._values.operation == "deactivate_bridge" then
		managers.motion_path:motion_operation_deactivate_bridge(self._values.elements)
	else
		for _, id in ipairs(self._values.elements) do
			local element = self.get_mission_element(self, id)

			if element then
				if self._values.operation == "goto_marker" then
					element.motion_operation_goto_marker(element, element.id(element), self._values.marker_ids[self._values.marker])
				elseif self._values.operation == "teleport" then
					element.motion_operation_teleport_to_marker(element, element.id(element), self._values.marker_ids[self._values.marker])
				elseif self._values.operation == "move" then
					element.motion_operation_set_motion_state(element, "move")
				elseif self._values.operation == "wait" then
					element.motion_operation_set_motion_state(element, "wait")
				elseif self._values.operation == "rotate" then
					element.motion_operation_set_rotation(element, self._id)
				end
			end
		end
	end

	ElementMotionPathOperator.super.on_executed(self, instigator)

	return 
end
ElementMotionPathTrigger = ElementMotionPathTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementMotionPathTrigger.init = function (self, ...)
	ElementMotionPathTrigger.super.init(self, ...)

	return 
end
ElementMotionPathTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_trigger(element, self._id, self._values.outcome, callback(self, self, "on_executed"))
	end

	return 
end
ElementMotionPathTrigger.client_on_executed = function (self, ...)
	return 
end
ElementMotionPathTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementMotionPathTrigger.super.on_executed(self, instigator)

	return 
end

return 
