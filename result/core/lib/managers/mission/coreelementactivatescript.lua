core:module("CoreElementActivateScript")
core:import("CoreMissionScriptElement")

ElementActivateScript = ElementActivateScript or class(CoreMissionScriptElement.MissionScriptElement)
ElementActivateScript.init = function (self, ...)
	ElementActivateScript.super.init(self, ...)

	return 
end
ElementActivateScript.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementActivateScript.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.activate_script ~= "none" then
		managers.mission:activate_script(self._values.activate_script, instigator)
	elseif Application:editor() then
		managers.editor:output_error("Cant activate script named \"none\" [" .. self._editor_name .. "]")
	end

	ElementActivateScript.super.on_executed(self, instigator)

	return 
end

return 
