core:module("CoreElementExecuteInOtherMission")
core:import("CoreMissionScriptElement")

ElementExecuteInOtherMission = ElementExecuteInOtherMission or class(CoreMissionScriptElement.MissionScriptElement)
ElementExecuteInOtherMission.init = function (self, ...)
	ElementExecuteInOtherMission.super.init(self, ...)

	return 
end
ElementExecuteInOtherMission.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementExecuteInOtherMission.get_mission_element = function (self, id)
	for name, script in pairs(managers.mission:scripts()) do
		if script.element(script, id) then
			return script.element(script, id)
		end
	end

	return 
end

return 
