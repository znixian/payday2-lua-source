core:module("CoreElementToggle")
core:import("CoreMissionScriptElement")

ElementToggle = ElementToggle or class(CoreMissionScriptElement.MissionScriptElement)
ElementToggle.init = function (self, ...)
	ElementToggle.super.init(self, ...)

	return 
end
ElementToggle.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementToggle.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self._values.toggle == "on" then
				element.set_enabled(element, true)

				if self._values.set_trigger_times and -1 < self._values.set_trigger_times then
					element.set_trigger_times(element, self._values.set_trigger_times)
				end
			elseif self._values.toggle == "off" then
				element.set_enabled(element, false)
			else
				element.set_enabled(element, not element.value(element, "enabled"))
			end

			element.on_toggle(element, element.value(element, "enabled"))
		end
	end

	ElementToggle.super.on_executed(self, instigator)

	return 
end

return 
