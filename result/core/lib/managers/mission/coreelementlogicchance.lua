core:module("CoreElementLogicChance")
core:import("CoreMissionScriptElement")

ElementLogicChance = ElementLogicChance or class(CoreMissionScriptElement.MissionScriptElement)
ElementLogicChance.init = function (self, ...)
	ElementLogicChance.super.init(self, ...)

	self._chance = self._values.chance
	self._triggers = {}

	return 
end
ElementLogicChance.client_on_executed = function (self, ...)
	return 
end
ElementLogicChance.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local roll = math.random(100)

	if self._chance < roll then
		self._trigger_outcome(self, "fail")

		return 
	end

	self._trigger_outcome(self, "success")
	ElementLogicChance.super.on_executed(self, instigator)

	return 
end
ElementLogicChance.chance_operation_add_chance = function (self, chance)
	self._chance = self._chance + chance

	return 
end
ElementLogicChance.chance_operation_subtract_chance = function (self, chance)
	self._chance = self._chance - chance

	return 
end
ElementLogicChance.chance_operation_reset = function (self)
	self._chance = self._values.chance

	return 
end
ElementLogicChance.chance_operation_set_chance = function (self, chance)
	self._chance = chance

	return 
end
ElementLogicChance.add_trigger = function (self, id, outcome, callback)
	self._triggers[id] = {
		outcome = outcome,
		callback = callback
	}

	return 
end
ElementLogicChance.remove_trigger = function (self, id)
	self._triggers[id] = nil

	return 
end
ElementLogicChance._trigger_outcome = function (self, outcome)
	for _, data in pairs(self._triggers) do
		if data.outcome == outcome then
			data.callback()
		end
	end

	return 
end
ElementLogicChanceOperator = ElementLogicChanceOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementLogicChanceOperator.init = function (self, ...)
	ElementLogicChanceOperator.super.init(self, ...)

	return 
end
ElementLogicChanceOperator.client_on_executed = function (self, ...)
	return 
end
ElementLogicChanceOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self._values.operation == "add_chance" then
				element.chance_operation_add_chance(element, self._values.chance)
			elseif self._values.operation == "subtract_chance" then
				element.chance_operation_subtract_chance(element, self._values.chance)
			elseif self._values.operation == "reset" then
				element.chance_operation_reset(element)
			elseif self._values.operation == "set_chance" then
				element.chance_operation_set_chance(element, self._values.chance)
			end
		end
	end

	ElementLogicChanceOperator.super.on_executed(self, instigator)

	return 
end
ElementLogicChanceTrigger = ElementLogicChanceTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementLogicChanceTrigger.init = function (self, ...)
	ElementLogicChanceTrigger.super.init(self, ...)

	return 
end
ElementLogicChanceTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_trigger(element, self._id, self._values.outcome, callback(self, self, "on_executed"))
	end

	return 
end
ElementLogicChanceTrigger.client_on_executed = function (self, ...)
	return 
end
ElementLogicChanceTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementLogicChanceTrigger.super.on_executed(self, instigator)

	return 
end

return 
