core:module("CoreMissionScriptElement")
core:import("CoreXml")
core:import("CoreCode")
core:import("CoreClass")

MissionScriptElement = MissionScriptElement or class()
MissionScriptElement.init = function (self, mission_script, data)
	self._mission_script = mission_script
	self._id = data.id
	self._editor_name = data.editor_name
	self._values = data.values

	return 
end
MissionScriptElement.on_created = function (self)
	return 
end
MissionScriptElement.on_script_activated = function (self)
	if self._values.rules_elements then
		self._rules_elements = {}

		for _, id in ipairs(self._values.rules_elements) do
			local element = self.get_mission_element(self, id)

			table.insert(self._rules_elements, element)
		end
	end

	return 
end
MissionScriptElement.get_mission_element = function (self, id)
	return self._mission_script:element(id)
end
MissionScriptElement.editor_name = function (self)
	return self._editor_name
end
MissionScriptElement.id = function (self)
	return self._id
end
MissionScriptElement.values = function (self)
	return self._values
end
MissionScriptElement.value = function (self, name)
	if self._values.instance_name and self._values.instance_var_names and self._values.instance_var_names[name] then
		local value = managers.world_instance:get_instance_param(self._values.instance_name, self._values.instance_var_names[name])

		if value then
			return value
		end
	end

	return self._values[name]
end
MissionScriptElement.get_random_table_value = function (self, value)
	if tonumber(value) then
		return value
	end

	return (value[1] + math.random(value[2] + 1)) - 1
end
MissionScriptElement.get_random_table_value_float = function (self, value)
	if tonumber(value) then
		return value
	end

	return value[1] + math.rand(value[2])
end
MissionScriptElement.enabled = function (self)
	return self._values.enabled
end
MissionScriptElement._check_instigator = function (self, instigator)
	if CoreClass.type_name(instigator) == "Unit" then
		return instigator
	end

	return managers.player:player_unit()
end
MissionScriptElement.on_executed = function (self, instigator, alternative, skip_execute_on_executed)
	if not self._values.enabled then
		return 
	end

	instigator = self._check_instigator(self, instigator)

	if Network:is_server() then
		if instigator and alive(instigator) and instigator.id(instigator) ~= -1 then
			managers.network:session():send_to_peers_synched("run_mission_element", self._id, instigator, self._last_orientation_index or 0)
		else
			managers.network:session():send_to_peers_synched("run_mission_element_no_instigator", self._id, self._last_orientation_index or 0)
		end
	end

	self._last_orientation_index = nil

	self._print_debug_on_executed(self, instigator)
	self._reduce_trigger_times(self)

	if not skip_execute_on_executed or CoreClass.type_name(skip_execute_on_executed) ~= "boolean" then
		self._trigger_execute_on_executed(self, instigator, alternative)
	end

	return 
end
MissionScriptElement._calc_base_delay = function (self)
	if not self._values.base_delay_rand then
		return self._values.base_delay
	end

	return self._values.base_delay + math.rand(self._values.base_delay_rand)
end
MissionScriptElement._trigger_execute_on_executed = function (self, instigator, alternative)
	local base_delay = self._calc_base_delay(self)

	if 0 < base_delay then
		self._mission_script:add(callback(self, self, "_execute_on_executed", {
			instigator = instigator,
			alternative = alternative
		}), base_delay, 1)
	else
		self.execute_on_executed(self, {
			instigator = instigator,
			alternative = alternative
		})
	end

	return 
end
MissionScriptElement._print_debug_on_executed = function (self, instigator)
	if self.is_debug(self) then
		self._print_debug(self, "Element '" .. self._editor_name .. "' executed.", instigator)

		instigator = instigator and instigator
	end

	return 
end
MissionScriptElement._print_debug = function (self, debug, instigator)
	if self.is_debug(self) then
		self._mission_script:debug_output(debug)
	end

	return 
end
MissionScriptElement._reduce_trigger_times = function (self)
	if 0 < self._values.trigger_times then
		self._values.trigger_times = self._values.trigger_times - 1

		if self._values.trigger_times <= 0 then
			self.set_enabled(self, false)
		end
	end

	return 
end
MissionScriptElement._execute_on_executed = function (self, params)
	self.execute_on_executed(self, params)

	return 
end
MissionScriptElement._calc_element_delay = function (self, params)
	if not params.delay_rand then
		return params.delay
	end

	return params.delay + math.rand(params.delay_rand)
end
MissionScriptElement.execute_on_executed = function (self, execute_params)
	for _, params in ipairs(self._values.on_executed) do
		if not execute_params.alternative or not params.alternative or execute_params.alternative == params.alternative then
			local element = self.get_mission_element(self, params.id)

			if element then
				local delay = self._calc_element_delay(self, params)

				if 0 < delay then
					if self.is_debug(self) or element.is_debug(element) then
						self._mission_script:debug_output("  Executing element '" .. element.editor_name(element) .. "' in " .. delay .. " seconds ...", Color(1, 0.75, 0.75, 0.75))
					end

					self._mission_script:add(callback(element, element, "on_executed", execute_params.instigator), delay, 1)
				else
					if self.is_debug(self) or element.is_debug(element) then
						self._mission_script:debug_output("  Executing element '" .. element.editor_name(element) .. "' ...", Color(1, 0.75, 0.75, 0.75))
					end

					element.on_executed(element, execute_params.instigator)
				end
			end
		end
	end

	return 
end
MissionScriptElement.on_execute_element = function (self, element, instigator)
	element.on_executed(element, instigator)

	return 
end
MissionScriptElement._has_on_executed_alternative = function (self, alternative)
	for _, params in ipairs(self._values.on_executed) do
		if params.alternative and params.alternative == alternative then
			return true
		end
	end

	return false
end
MissionScriptElement.set_enabled = function (self, enabled)
	self._values.enabled = enabled

	self.on_set_enabled(self)

	return 
end
MissionScriptElement.on_set_enabled = function (self)
	return 
end
MissionScriptElement.on_toggle = function (self, value)
	return 
end
MissionScriptElement.set_trigger_times = function (self, trigger_times)
	self._values.trigger_times = trigger_times

	return 
end
MissionScriptElement.is_debug = function (self)
	return self._values.debug or self._mission_script:is_debug()
end
MissionScriptElement.stop_simulation = function (self, ...)
	return 
end
MissionScriptElement.operation_add = function (self)
	if Application:editor() then
		managers.editor:output_error("Element " .. self.editor_name(self) .. " doesn't have an 'add' operator implemented.")
	end

	return 
end
MissionScriptElement.operation_remove = function (self)
	if Application:editor() then
		managers.editor:output_error("Element " .. self.editor_name(self) .. " doesn't have a 'remove' operator implemented.")
	end

	return 
end
MissionScriptElement.apply_job_value = function (self)
	if Application:editor() then
		managers.editor:output_error("Element " .. self.editor_name(self) .. " doesn't have a 'apply_job_value' function implemented.")
	end

	return 
end
MissionScriptElement.set_synced_orientation_element_index = function (self, orientation_element_index)
	if orientation_element_index and 0 < orientation_element_index then
		self._synced_orientation_element_index = orientation_element_index
	else
		self._synced_orientation_element_index = nil
	end

	return 
end
MissionScriptElement.get_orientation_by_index = function (self, index)
	if not index or index == 0 then
		return self._values.position, self._values.rotation
	end

	local id = self._values.orientation_elements[index]
	local element = self.get_mission_element(self, id)

	if self._values.disable_orientation_on_use then
		element.set_enabled(element, false)
	end

	return element.get_orientation_by_index(element, 0)
end
MissionScriptElement.get_orientation_index = function (self)
	if self._values.orientation_elements and 0 < #self._values.orientation_elements then
		if not self._unused_orientation_indices then
			self._unused_orientation_indices = {}

			for index, id in ipairs(self._values.orientation_elements) do
				table.insert(self._unused_orientation_indices, index)
			end
		end

		local alternatives = {}

		for i, index in ipairs(self._unused_orientation_indices) do
			local element_id = self._values.orientation_elements[index]
			local element = self.get_mission_element(self, element_id)

			if element.enabled(element) then
				table.insert(alternatives, i)
			end
		end

		if #alternatives == 0 then
			if #self._unused_orientation_indices == #self._values.orientation_elements then
				_G.debug_pause("There where no enabled orientation elements!", self.editor_name(self))

				return 
			elseif #self._unused_orientation_indices < #self._values.orientation_elements then
				self._unused_orientation_indices = nil

				return self.get_orientation_index(self)
			end
		end

		local use_i = alternatives[(self._values.use_orientation_sequenced and 1) or math.random(#alternatives)]
		local index = table.remove(self._unused_orientation_indices, use_i)
		self._unused_orientation_indices = (0 < #self._unused_orientation_indices and self._unused_orientation_indices) or nil

		return index
	else
		return 0
	end

	return 
end
MissionScriptElement.get_orientation = function (self, use_last_orientation_index)
	local index = use_last_orientation_index and self._last_orientation_index
	index = index or self._synced_orientation_element_index or self.get_orientation_index(self)
	self._last_orientation_index = index
	local pos, rot = self.get_orientation_by_index(self, index)

	return pos, rot
end
MissionScriptElement.debug_draw = function (self)
	return 
end
MissionScriptElement.pre_destroy = function (self)
	return 
end
MissionScriptElement.destroy = function (self)
	return 
end

return 
