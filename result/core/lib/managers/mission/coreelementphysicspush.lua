core:module("CoreElementPhysicsPush")
core:import("CoreMissionScriptElement")

ElementPhysicsPush = ElementPhysicsPush or class(CoreMissionScriptElement.MissionScriptElement)
ElementPhysicsPush.IDS_EFFECT = Idstring("core/physic_effects/hubelement_push")
ElementPhysicsPush.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementPhysicsPush.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	World:play_physic_effect(self.IDS_EFFECT, self._values.position, self._values.physicspush_range, self._values.physicspush_velocity, self._values.physicspush_mass)
	ElementPhysicsPush.super.on_executed(self, instigator)

	return 
end

return 
