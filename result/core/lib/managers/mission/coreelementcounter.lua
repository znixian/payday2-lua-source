core:module("CoreElementCounter")
core:import("CoreMissionScriptElement")
core:import("CoreClass")

ElementCounter = ElementCounter or class(CoreMissionScriptElement.MissionScriptElement)
ElementCounter.init = function (self, ...)
	ElementCounter.super.init(self, ...)

	self._digital_gui_units = {}
	self._triggers = {}

	return 
end
ElementCounter.on_script_activated = function (self)
	self._values.counter_target = self.value(self, "counter_target")
	self._original_value = self._values.counter_target

	if not Network:is_server() then
		return 
	end

	if self._values.digital_gui_unit_ids then
		for _, id in ipairs(self._values.digital_gui_unit_ids) do
			if Global.running_simulation then
				local unit = managers.editor:unit_with_id(id)

				table.insert(self._digital_gui_units, unit)
				unit.digital_gui(unit):number_set(self._values.counter_target)
			else
				local unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "_load_unit"))

				if unit then
					table.insert(self._digital_gui_units, unit)
					unit.digital_gui(unit):number_set(self._values.counter_target)
				end
			end
		end
	end

	return 
end
ElementCounter._load_unit = function (self, unit)
	table.insert(self._digital_gui_units, unit)
	unit.digital_gui(unit):number_set(self._values.counter_target)

	return 
end
ElementCounter.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if 0 < self._values.counter_target then
		self._values.counter_target = self._values.counter_target - 1

		self._update_digital_guis_number(self)

		if self.is_debug(self) then
			self._mission_script:debug_output("Counter " .. self._editor_name .. ": " .. self._values.counter_target .. " Previous value: " .. self._values.counter_target + 1, Color(1, 0, 0.75, 0))
		end

		if self._values.counter_target == 0 then
			ElementCounter.super.on_executed(self, instigator)
		end
	elseif self.is_debug(self) then
		self._mission_script:debug_output("Counter " .. self._editor_name .. ": already exhausted!", Color(1, 0, 0.75, 0))
	end

	return 
end
ElementCounter.reset_counter_target = function (self, counter_target)
	self._values.counter_target = counter_target

	self._update_digital_guis_number(self)

	return 
end
ElementCounter.counter_operation_add = function (self, amount)
	self._values.counter_target = self._values.counter_target + amount

	self._update_digital_guis_number(self)
	self._check_triggers(self, "add")
	self._check_triggers(self, "value")

	return 
end
ElementCounter.counter_operation_subtract = function (self, amount)
	self._values.counter_target = self._values.counter_target - amount

	self._update_digital_guis_number(self)
	self._check_triggers(self, "subtract")
	self._check_triggers(self, "value")

	return 
end
ElementCounter.counter_operation_reset = function (self, amount)
	self._values.counter_target = self._original_value

	self._update_digital_guis_number(self)
	self._check_triggers(self, "reset")
	self._check_triggers(self, "value")

	return 
end
ElementCounter.counter_operation_set = function (self, amount)
	self._values.counter_target = amount

	self._update_digital_guis_number(self)
	self._check_triggers(self, "set")
	self._check_triggers(self, "value")

	return 
end
ElementCounter.apply_job_value = function (self, amount)
	local type = CoreClass.type_name(amount)

	if type ~= "number" then
		Application:error("[ElementCounter:apply_job_value] " .. self._id .. "(" .. self._editor_name .. ") Can't apply job value of type " .. type)

		return 
	end

	self.counter_operation_set(self, amount)

	return 
end
ElementCounter.add_trigger = function (self, id, type, amount, callback)
	self._triggers[type] = self._triggers[type] or {}
	self._triggers[type][id] = {
		amount = amount,
		callback = callback
	}

	return 
end
ElementCounter.counter_value = function (self)
	return self._values.counter_target
end
ElementCounter._update_digital_guis_number = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):number_set(self._values.counter_target, true)
		end
	end

	return 
end
ElementCounter._check_triggers = function (self, type)
	if not self._triggers[type] then
		return 
	end

	for id, cb_data in pairs(self._triggers[type]) do
		if type ~= "value" or cb_data.amount == self._values.counter_target then
			cb_data.callback()
		end
	end

	return 
end
ElementCounterReset = ElementCounterReset or class(CoreMissionScriptElement.MissionScriptElement)
ElementCounterReset.init = function (self, ...)
	ElementCounterReset.super.init(self, ...)

	return 
end
ElementCounterReset.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self.is_debug(self) then
				self._mission_script:debug_output("Counter reset " .. element.editor_name(element) .. " to: " .. self._values.counter_target, Color(1, 0, 0.75, 0))
			end

			element.reset_counter_target(element, self._values.counter_target)
		end
	end

	ElementCounterReset.super.on_executed(self, instigator)

	return 
end
ElementCounterOperator = ElementCounterOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementCounterOperator.init = function (self, ...)
	ElementCounterOperator.super.init(self, ...)

	return 
end
ElementCounterOperator.client_on_executed = function (self, ...)
	return 
end
ElementCounterOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local amount = self.value(self, "amount")

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self._values.operation == "add" then
				element.counter_operation_add(element, amount)
			elseif self._values.operation == "subtract" then
				element.counter_operation_subtract(element, amount)
			elseif self._values.operation == "reset" then
				element.counter_operation_reset(element, amount)
			elseif self._values.operation == "set" then
				element.counter_operation_set(element, amount)
			end
		end
	end

	ElementCounterOperator.super.on_executed(self, instigator)

	return 
end
ElementCounterTrigger = ElementCounterTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementCounterTrigger.init = function (self, ...)
	ElementCounterTrigger.super.init(self, ...)

	return 
end
ElementCounterTrigger.on_script_activated = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_trigger(element, self._id, self._values.trigger_type, self._values.amount, callback(self, self, "on_executed"))
	end

	return 
end
ElementCounterTrigger.client_on_executed = function (self, ...)
	return 
end
ElementCounterTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementCounterTrigger.super.on_executed(self, instigator)

	return 
end
ElementCounterFilter = ElementCounterFilter or class(CoreMissionScriptElement.MissionScriptElement)
ElementCounterFilter.init = function (self, ...)
	ElementCounterFilter.super.init(self, ...)

	return 
end
ElementCounterFilter.on_script_activated = function (self)
	return 
end
ElementCounterFilter.client_on_executed = function (self, ...)
	return 
end
ElementCounterFilter.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if not self._values_ok(self) then
		return 
	end

	ElementCounterFilter.super.on_executed(self, instigator)

	return 
end
ElementCounterFilter._values_ok = function (self)
	if self._values.check_type == "counters_equal" then
		return self._all_counter_values_equal(self)
	end

	if self._values.check_type == "counters_not_equal" then
		return not self._all_counter_values_equal(self)
	end

	if self._values.needed_to_execute == "all" then
		return self._all_counters_ok(self)
	end

	if self._values.needed_to_execute == "any" then
		return self._any_counters_ok(self)
	end

	return 
end
ElementCounterFilter._all_counter_values_equal = function (self)
	local test_value = nil

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)
		test_value = test_value or element.counter_value(element)

		if test_value ~= element.counter_value(element) then
			return false
		end
	end

	return true
end
ElementCounterFilter._all_counters_ok = function (self)
	for _, id in ipairs(self._values.elements) do
		if not self._check_type(self, self.get_mission_element(self, id)) then
			return false
		end
	end

	return true
end
ElementCounterFilter._any_counters_ok = function (self)
	for _, id in ipairs(self._values.elements) do
		if self._check_type(self, self.get_mission_element(self, id)) then
			return true
		end
	end

	return false
end
ElementCounterFilter._check_type = function (self, element)
	if not self._values.check_type or self._values.check_type == "equal" then
		return element.counter_value(element) == self._values.value
	end

	if self._values.check_type == "less_or_equal" then
		return element.counter_value(element) <= self._values.value
	end

	if self._values.check_type == "greater_or_equal" then
		return self._values.value <= element.counter_value(element)
	end

	if self._values.check_type == "less_than" then
		return element.counter_value(element) < self._values.value
	end

	if self._values.check_type == "greater_than" then
		return self._values.value < element.counter_value(element)
	end

	return 
end

return 
