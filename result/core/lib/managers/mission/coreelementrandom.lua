core:module("CoreElementRandom")
core:import("CoreMissionScriptElement")
core:import("CoreTable")

ElementRandom = ElementRandom or class(CoreMissionScriptElement.MissionScriptElement)
ElementRandom.init = function (self, ...)
	ElementRandom.super.init(self, ...)

	self._original_on_executed = CoreTable.clone(self._values.on_executed)

	return 
end
ElementRandom.client_on_executed = function (self, ...)
	return 
end
ElementRandom.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self._unused_randoms = {}

	for i, element_data in ipairs(self._original_on_executed) do
		if not self._values.ignore_disabled or (self._values.ignore_disabled and self.get_mission_element(self, element_data.id):enabled()) then
			table.insert(self._unused_randoms, i)
		end
	end

	self._values.on_executed = {}
	local amount = self._calc_amount(self)

	if self._values.counter_id then
		local element = self.get_mission_element(self, self._values.counter_id)
		amount = element.counter_value(element)
	end

	for i = 1, math.min(amount, #self._original_on_executed), 1 do
		table.insert(self._values.on_executed, self._original_on_executed[self._get_random_elements(self)])
	end

	ElementRandom.super.on_executed(self, instigator)

	return 
end
ElementRandom._calc_amount = function (self)
	local amount = self._values.amount or 1

	if self._values.amount_random and 0 < self._values.amount_random then
		amount = (amount + math.random(self._values.amount_random + 1)) - 1
	end

	return amount
end
ElementRandom._get_random_elements = function (self)
	local t = {}
	local rand = math.random(#self._unused_randoms)

	return table.remove(self._unused_randoms, rand)
end

return 
