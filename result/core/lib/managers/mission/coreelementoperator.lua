core:module("CoreElementOperator")
core:import("CoreMissionScriptElement")

ElementOperator = ElementOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementOperator.init = function (self, ...)
	ElementOperator.super.init(self, ...)

	return 
end
ElementOperator.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self._values.operation == "add" then
				element.operation_add(element)
			elseif self._values.operation == "remove" then
				element.operation_remove(element)
			end
		end
	end

	ElementOperator.super.on_executed(self, instigator)

	return 
end

return 
