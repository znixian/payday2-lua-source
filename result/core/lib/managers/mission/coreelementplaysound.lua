core:module("CoreElementPlaySound")
core:import("CoreMissionScriptElement")

ElementPlaySound = ElementPlaySound or class(CoreMissionScriptElement.MissionScriptElement)
ElementPlaySound.init = function (self, ...)
	ElementPlaySound.super.init(self, ...)

	return 
end
ElementPlaySound.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementPlaySound.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._source and self.value(self, "interrupt") ~= false then
		self._source:stop()
	end

	if self._values.use_instigator then
		if Network:is_server() and alive(instigator) and instigator.id(instigator) ~= -1 then
			instigator.sound(instigator):say(self._values.sound_event, true, not self._values.append_prefix, true)
		end
	elseif not self._values.elements or #self._values.elements == 0 then
		self._play_sound(self)
	elseif Network:is_server() then
		self._play_sound_on_elements(self)
	end

	ElementPlaySound.super.on_executed(self, instigator)

	return 
end
ElementPlaySound._play_sound_on_elements = function (self)
	local function f(unit)
		if unit.id(unit) ~= -1 then
			unit.sound(unit):say(self._values.sound_event, true, not self._values.append_prefix, true)
		end

		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.execute_on_all_units(element, f)
	end

	return 
end
ElementPlaySound._play_sound = function (self)
	if self._values.sound_event then
		if self._source then
			self._source:stop()
		end

		self._source = SoundDevice:create_source(self._editor_name)

		self._source:set_position(self._values.position)
		self._source:set_orientation(self._values.rotation)

		if self._source:post_event(self._values.sound_event, callback(self, self, "sound_ended"), nil, "end_of_event") then
			self._mission_script:add_save_state_cb(self._id)
		end
	elseif Application:editor() then
		managers.editor:output_error("Cant play sound event nil [" .. self._editor_name .. "]")
	end

	return 
end
ElementPlaySound.sound_ended = function (self, ...)
	self._mission_script:remove_save_state_cb(self._id)

	return 
end
ElementPlaySound.operation_remove = function (self)
	if self._source then
		self._source:stop()
		self.sound_ended(self)
	end

	return 
end
ElementPlaySound.save = function (self, data)
	return 
end
ElementPlaySound.load = function (self, data)
	self._play_sound(self)

	return 
end
ElementPlaySound.stop_simulation = function (self)
	if self._source then
		self._source:stop()
	end

	ElementPlaySound.super.stop_simulation(self)

	return 
end
ElementPlaySound.pre_destroy = function (self)
	if self._source then
		self._source:stop()
		self.sound_ended(self)
	end

	return 
end
ElementPlaySound.destroy = function (self)
	if self._source then
		self._source:stop()
		self._source:delete()

		self._source = nil

		self.sound_ended(self)
	end

	return 
end

return 
