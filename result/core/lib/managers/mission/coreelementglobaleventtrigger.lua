core:module("CoreElementGlobalEventTrigger")
core:import("CoreMissionScriptElement")
core:import("CoreCode")

ElementGlobalEventTrigger = ElementGlobalEventTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementGlobalEventTrigger.init = function (self, ...)
	ElementGlobalEventTrigger.super.init(self, ...)

	return 
end
ElementGlobalEventTrigger.on_script_activated = function (self)
	managers.mission:add_global_event_listener(self._id, {
		self._values.global_event
	}, callback(self, self, (Network:is_client() and "send_to_host") or "on_executed"))

	return 
end
ElementGlobalEventTrigger.send_to_host = function (self, instigator)
	if instigator then
		managers.network:session():send_to_host("to_server_mission_element_trigger", self._id, instigator)
	end

	return 
end
ElementGlobalEventTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementGlobalEventTrigger.super.on_executed(self, instigator)

	return 
end

return 
