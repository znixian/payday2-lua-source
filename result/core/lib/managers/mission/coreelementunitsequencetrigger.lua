core:module("CoreElementUnitSequenceTrigger")
core:import("CoreMissionScriptElement")
core:import("CoreCode")

ElementUnitSequenceTrigger = ElementUnitSequenceTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementUnitSequenceTrigger.init = function (self, ...)
	ElementUnitSequenceTrigger.super.init(self, ...)

	if not self._values.sequence_list and self._values.sequence then
		self._values.sequence_list = {
			{
				unit_id = self._values.unit_id,
				sequence = self._values.sequence
			}
		}
	end

	return 
end
ElementUnitSequenceTrigger.on_script_activated = function (self)
	if not Network:is_client() or false then
		self._mission_script:add_save_state_cb(self._id)

		for _, data in pairs(self._values.sequence_list) do
			managers.mission:add_runned_unit_sequence_trigger(data.unit_id, data.sequence, callback(self, self, "on_executed"))
		end
	end

	self._has_active_callback = true

	return 
end
ElementUnitSequenceTrigger.send_to_host = function (self, instigator)
	if alive(instigator) then
		managers.network:session():send_to_host("to_server_mission_element_trigger", self._id, instigator)
	end

	return 
end
ElementUnitSequenceTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementUnitSequenceTrigger.super.on_executed(self, instigator)

	return 
end
ElementUnitSequenceTrigger.save = function (self, data)
	data.save_me = true

	return 
end
ElementUnitSequenceTrigger.load = function (self, data)
	if not self._has_active_callback then
		self.on_script_activated(self)
	end

	return 
end

return 
