local tmp_vec1 = Vector3()

core:module("CoreElementShape")
core:import("CoreShapeManager")
core:import("CoreMissionScriptElement")
core:import("CoreTable")

ElementShape = ElementShape or class(CoreMissionScriptElement.MissionScriptElement)
ElementShape.init = function (self, ...)
	ElementShape.super.init(self, ...)

	self._shapes = {}

	if not self._values.shape_type or self._values.shape_type == "box" then
		self._add_shape(self, CoreShapeManager.ShapeBoxMiddle:new({
			position = self._values.position,
			rotation = self._values.rotation,
			width = self._values.width,
			depth = self._values.depth,
			height = self._values.height
		}))
	elseif self._values.shape_type == "cylinder" then
		self._add_shape(self, CoreShapeManager.ShapeCylinderMiddle:new({
			position = self._values.position,
			rotation = self._values.rotation,
			height = self._values.height,
			radius = self._values.radius
		}))
	end

	return 
end
ElementShape.on_script_activated = function (self)
	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementShape._add_shape = function (self, shape)
	table.insert(self._shapes, shape)

	return 
end
ElementShape.get_shapes = function (self)
	return self._shapes
end
ElementShape.is_inside = function (self, pos)
	for _, shape in ipairs(self._shapes) do
		if shape.is_inside(shape, pos) then
			return true
		end
	end

	return false
end
ElementShape.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementShape.super.on_executed(self, instigator)

	return 
end
ElementShape.save = function (self, data)
	data.enabled = self._values.enabled

	return 
end
ElementShape.load = function (self, data)
	self.set_enabled(self, data.enabled)

	return 
end

return 
