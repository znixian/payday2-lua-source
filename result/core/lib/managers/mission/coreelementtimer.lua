core:module("CoreElementTimer")
core:import("CoreMissionScriptElement")

ElementTimer = ElementTimer or class(CoreMissionScriptElement.MissionScriptElement)
ElementTimer.init = function (self, ...)
	ElementTimer.super.init(self, ...)

	self._digital_gui_units = {}
	self._triggers = {}

	return 
end
ElementTimer.on_script_activated = function (self)
	self._timer = self.get_random_table_value_float(self, self.value(self, "timer"))

	if not Network:is_server() then
		return 
	end

	if self._values.digital_gui_unit_ids then
		for _, id in ipairs(self._values.digital_gui_unit_ids) do
			if Global.running_simulation then
				local unit = managers.editor:unit_with_id(id)

				table.insert(self._digital_gui_units, unit)
				unit.digital_gui(unit):timer_set(self._timer)
			else
				local unit = managers.worlddefinition:get_unit_on_load(id, callback(self, self, "_load_unit"))

				if unit then
					table.insert(self._digital_gui_units, unit)
					unit.digital_gui(unit):timer_set(self._timer)
				end
			end
		end
	end

	return 
end
ElementTimer._load_unit = function (self, unit)
	table.insert(self._digital_gui_units, unit)
	unit.digital_gui(unit):timer_set(self._timer)

	return 
end
ElementTimer.set_enabled = function (self, enabled)
	ElementTimer.super.set_enabled(self, enabled)

	return 
end
ElementTimer.add_updator = function (self)
	if not Network:is_server() then
		return 
	end

	if not self._updator then
		self._updator = true

		self._mission_script:add_updator(self._id, callback(self, self, "update_timer"))
	end

	return 
end
ElementTimer.remove_updator = function (self)
	if self._updator then
		self._mission_script:remove_updator(self._id)

		self._updator = nil
	end

	return 
end
ElementTimer.update_timer = function (self, t, dt)
	self._timer = self._timer - dt

	if self._timer <= 0 then
		self.remove_updator(self)
		self.on_executed(self)
	end

	for id, cb_data in pairs(self._triggers) do
		if self._timer <= cb_data.time and not cb_data.disabled then
			cb_data.callback()
			self.remove_trigger(self, id)
		end
	end

	return 
end
ElementTimer.client_on_executed = function (self, ...)
	return 
end
ElementTimer.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementTimer.super.on_executed(self, instigator)

	return 
end
ElementTimer.timer_operation_pause = function (self)
	self.remove_updator(self)
	self._pause_digital_guis(self)

	return 
end
ElementTimer.timer_operation_start = function (self)
	self.add_updator(self)
	self._start_digital_guis_count_down(self)

	return 
end
ElementTimer.timer_operation_add_time = function (self, time)
	self._timer = self._timer + time

	self._update_digital_guis_timer(self)

	return 
end
ElementTimer.timer_operation_subtract_time = function (self, time)
	self._timer = self._timer - time

	self._update_digital_guis_timer(self)

	return 
end
ElementTimer.timer_operation_reset = function (self)
	self._timer = self.get_random_table_value_float(self, self.value(self, "timer"))

	self._update_digital_guis_timer(self)

	return 
end
ElementTimer.timer_operation_set_time = function (self, time)
	self._timer = time

	self._update_digital_guis_timer(self)

	return 
end
ElementTimer._update_digital_guis_timer = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):timer_set(self._timer, true)
		end
	end

	return 
end
ElementTimer._start_digital_guis_count_down = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):timer_start_count_down(true)
		end
	end

	return 
end
ElementTimer._start_digital_guis_count_up = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):timer_start_count_up(true)
		end
	end

	return 
end
ElementTimer._pause_digital_guis = function (self)
	for _, unit in ipairs(self._digital_gui_units) do
		if alive(unit) then
			unit.digital_gui(unit):timer_pause(true)
		end
	end

	return 
end
ElementTimer.add_trigger = function (self, id, time, callback, disabled)
	self._triggers[id] = {
		time = time,
		callback = callback,
		disabled = disabled
	}

	return 
end
ElementTimer.remove_trigger = function (self, id)
	if not self._triggers[id].disabled then
		self._triggers[id] = nil
	end

	return 
end
ElementTimer.enable_trigger = function (self, id)
	if self._triggers[id] then
		self._triggers[id].disabled = false
	end

	return 
end
ElementTimerOperator = ElementTimerOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementTimerOperator.init = function (self, ...)
	ElementTimerOperator.super.init(self, ...)

	return 
end
ElementTimerOperator.client_on_executed = function (self, ...)
	return 
end
ElementTimerOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local time = self.get_random_table_value_float(self, self.value(self, "time"))

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self._values.operation == "pause" then
				element.timer_operation_pause(element)
			elseif self._values.operation == "start" then
				element.timer_operation_start(element)
			elseif self._values.operation == "add_time" then
				element.timer_operation_add_time(element, time)
			elseif self._values.operation == "subtract_time" then
				element.timer_operation_subtract_time(element, time)
			elseif self._values.operation == "reset" then
				element.timer_operation_reset(element, time)
			elseif self._values.operation == "set_time" then
				element.timer_operation_set_time(element, time)
			end
		end
	end

	ElementTimerOperator.super.on_executed(self, instigator)

	return 
end
ElementTimerTrigger = ElementTimerTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementTimerTrigger.init = function (self, ...)
	ElementTimerTrigger.super.init(self, ...)

	return 
end
ElementTimerTrigger.on_script_activated = function (self)
	self.activate_trigger(self)

	return 
end
ElementTimerTrigger.client_on_executed = function (self, ...)
	return 
end
ElementTimerTrigger.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementTimerTrigger.super.on_executed(self, instigator)

	return 
end
ElementTimerTrigger.activate_trigger = function (self)
	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.add_trigger(element, self._id, self._values.time, callback(self, self, "on_executed"), not self.enabled(self))
	end

	return 
end
ElementTimerTrigger.operation_add = function (self)
	self.activate_trigger(self)

	return 
end
ElementTimerTrigger.set_enabled = function (self, enabled)
	ElementTimerTrigger.super.set_enabled(self, enabled)

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		element.enable_trigger(element, self._id)
	end

	return 
end

return 
