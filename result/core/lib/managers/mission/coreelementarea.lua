local tmp_vec1 = Vector3()

core:module("CoreElementArea")
core:import("CoreShapeManager")
core:import("CoreMissionScriptElement")
core:import("CoreTable")

ElementAreaTrigger = ElementAreaTrigger or class(CoreMissionScriptElement.MissionScriptElement)
ElementAreaTrigger.init = function (self, ...)
	ElementAreaTrigger.super.init(self, ...)

	self._last_project_amount_all = 0
	self._shapes = {}
	self._shape_elements = {}
	self._rules_elements = {}

	if not self._values.use_shape_element_ids then
		if not self._values.shape_type or self._values.shape_type == "box" then
			self._add_shape(self, CoreShapeManager.ShapeBoxMiddle:new({
				position = self._values.position,
				rotation = self._values.rotation,
				width = self._values.width,
				depth = self._values.depth,
				height = self._values.height
			}))
		elseif self._values.shape_type == "cylinder" then
			self._add_shape(self, CoreShapeManager.ShapeCylinderMiddle:new({
				position = self._values.position,
				rotation = self._values.rotation,
				height = self._values.height,
				radius = self._values.radius
			}))
		end
	end

	self._inside = {}

	return 
end
ElementAreaTrigger.on_script_activated = function (self)
	self._on_script_activated_done = true

	if self._values.use_shape_element_ids then
		for _, id in ipairs(self._values.use_shape_element_ids) do
			local element = self.get_mission_element(self, id)

			table.insert(self._shape_elements, element)
		end
	end

	if self._values.rules_element_ids then
		for _, id in ipairs(self._values.rules_element_ids) do
			local element = self.get_mission_element(self, id)

			table.insert(self._rules_elements, element)
		end
	end

	self._mission_script:add_save_state_cb(self._id)

	if self._values.enabled then
		self.add_callback(self)
	end

	return 
end
ElementAreaTrigger._add_shape = function (self, shape)
	table.insert(self._shapes, shape)

	return 
end
ElementAreaTrigger.get_shapes = function (self)
	return self._shapes
end
ElementAreaTrigger.is_inside = function (self, pos)
	for _, shape in ipairs(self._shapes) do
		if shape.is_inside(shape, pos) then
			return true
		end
	end

	return false
end
ElementAreaTrigger._is_inside = function (self, pos)
	if self.is_inside(self, pos) then
		return true
	end

	for _, element in ipairs(self._shape_elements) do
		local use = self._values.use_disabled_shapes or element.enabled(element)

		if use and element.is_inside(element, pos) then
			return true
		end
	end

	return false
end
ElementAreaTrigger.set_enabled = function (self, enabled)
	if not enabled and Network:is_server() and self._values.trigger_on == "both" then
		for _, unit in ipairs(CoreTable.clone(self._inside)) do
			self.sync_exit_area(self, unit)
		end
	end

	ElementAreaTrigger.super.set_enabled(self, enabled)

	if enabled then
		self.add_callback(self)
	else
		self._inside = {}

		self.remove_callback(self)
	end

	return 
end
ElementAreaTrigger.add_callback = function (self)
	if not self._callback then
		self._callback = self._mission_script:add(callback(self, self, "update_area"), self._values.interval)
	end

	return 
end
ElementAreaTrigger.remove_callback = function (self)
	if self._callback then
		self._mission_script:remove(self._callback)

		self._callback = nil
	end

	return 
end
ElementAreaTrigger.on_executed = function (self, instigator, ...)
	if not self._values.enabled then
		return 
	end

	ElementAreaTrigger.super.on_executed(self, instigator, ...)

	if not self._values.enabled then
		self.remove_callback(self)
	end

	return 
end
ElementAreaTrigger.instigators = function (self)
	if self._values.unit_ids then
		local instigators = {}

		if Network:is_server() then
			for _, id in ipairs(self._values.unit_ids) do
				local unit = (Application:editor() and managers.editor:layer("Statics"):created_units_pairs()[id]) or managers.worlddefinition:get_unit(id)

				if alive(unit) then
					table.insert(instigators, unit)
				end
			end
		end

		return instigators
	end

	local instigators = self.project_instigators(self)

	for _, id in ipairs(self._values.spawn_unit_elements) do
		local element = self.get_mission_element(self, id)

		if element then
			for _, unit in ipairs(element.units(element)) do
				table.insert(instigators, unit)
			end
		end
	end

	return instigators
end
ElementAreaTrigger.project_instigators = function (self)
	return {}
end
ElementAreaTrigger.project_amount_all = function (self)
	return nil
end
ElementAreaTrigger.project_amount_inside = function (self)
	return #self._inside
end
ElementAreaTrigger.is_instigator_valid = function (self, unit)
	return true
end
ElementAreaTrigger.debug_draw = function (self)
	if self._values.instigator == "loot" or self._values.instigator == "unique_loot" then
		for _, shape in ipairs(self._shapes) do
			shape.draw(shape, 0, 0, (not self._values.enabled or 0) and 1, (self._values.enabled and 1) or 0, 0, 0.2)
		end

		for _, shape_element in ipairs(self._shape_elements) do
			for _, shape in ipairs(shape_element.get_shapes(shape_element)) do
				shape.draw(shape, 0, 0, (not self._values.enabled or 0) and 1, (self._values.enabled and 1) or 0, 0, 0.2)
			end
		end
	end

	return 
end
ElementAreaTrigger.update_area = function (self)
	if not self._values.enabled then
		return 
	end

	if self._values.trigger_on == "on_empty" then
		if Network:is_server() then
			self._inside = {}

			for _, unit in ipairs(self.instigators(self)) do
				if alive(unit) then
					self._should_trigger(self, unit)
				end
			end

			if #self._inside == 0 then
				self.on_executed(self)
			end
		end
	else
		local instigators = self.instigators(self)

		if #instigators == 0 and Network:is_server() then
			if self._should_trigger(self, nil) then
				self._check_amount(self, nil)
			end
		else
			for _, unit in ipairs(instigators) do
				if alive(unit) then
					if Network:is_client() then
						self._client_check_state(self, unit)
					elseif self._should_trigger(self, unit) then
						self._check_amount(self, unit)
					end
				end
			end
		end
	end

	return 
end
ElementAreaTrigger.sync_enter_area = function (self, unit)
	table.insert(self._inside, unit)

	if self._values.trigger_on == "on_enter" or self._values.trigger_on == "both" or self._values.trigger_on == "while_inside" then
		self._check_amount(self, unit)
	end

	return 
end
ElementAreaTrigger.sync_exit_area = function (self, unit)
	table.delete(self._inside, unit)

	if self._values.trigger_on == "on_exit" or self._values.trigger_on == "both" then
		self._check_amount(self, unit)
	end

	return 
end
ElementAreaTrigger.sync_while_in_area = function (self, unit)
	self._check_amount(self, unit)

	return 
end
ElementAreaTrigger._check_amount = function (self, unit)
	if self._values.trigger_on == "on_enter" then
		local amount = self._values.amount == "all" and self.project_amount_all(self)
		amount = amount or tonumber(self._values.amount)

		self._clean_destroyed_units(self)

		local inside = self.project_amount_inside(self)

		if 0 < inside and ((amount and amount <= inside) or not amount) then
			self.on_executed(self, unit)
		end
	elseif self.is_instigator_valid(self, unit) then
		self.on_executed(self, unit)
	end

	return 
end
ElementAreaTrigger._should_trigger = function (self, unit)
	if alive(unit) then
		local rule_ok = self._check_instigator_rules(self, unit)
		local inside = nil

		if unit.movement(unit) then
			inside = self._is_inside(self, unit.movement(unit):m_pos())
		else
			unit.m_position(unit, tmp_vec1)

			inside = self._is_inside(self, tmp_vec1)
		end

		if table.contains(self._inside, unit) then
			if not inside or not rule_ok then
				table.delete(self._inside, unit)

				if self._values.trigger_on == "on_exit" or self._values.trigger_on == "both" then
					return true
				end
			end
		elseif inside and rule_ok then
			table.insert(self._inside, unit)

			if self._values.trigger_on == "on_enter" or self._values.trigger_on == "both" then
				return true
			end
		end

		if self._values.trigger_on == "while_inside" and inside and rule_ok then
			return true
		end
	end

	if self._values.amount == "all" then
		local project_amount_all = self.project_amount_all(self)

		if project_amount_all ~= self._last_project_amount_all then
			self._last_project_amount_all = project_amount_all

			self._clean_destroyed_units(self)

			return true
		end
	end

	return false
end
ElementAreaTrigger._check_instigator_rules = function (self, unit)
	for _, element in ipairs(self._rules_elements) do
		if not element.check_rules(element, self._values.instigator, unit) then
			return false
		end
	end

	return true
end
ElementAreaTrigger._clean_destroyed_units = function (self)
	local i = 1

	while next(self._inside) and i <= #self._inside do
		if alive(self._inside[i]) then
			i = i + 1
		else
			table.remove(self._inside, i)
		end
	end

	return 
end
ElementAreaTrigger._client_check_state = function (self, unit)
	local rule_ok = self._check_instigator_rules(self, unit)
	local inside = self._is_inside(self, unit.position(unit))

	if table.contains(self._inside, unit) then
		if not inside or not rule_ok then
			table.delete(self._inside, unit)
			managers.network:session():send_to_host("to_server_area_event", 2, self._id, unit)
		elseif self._values.trigger_on == "while_inside" then
			managers.network:session():send_to_host("to_server_area_event", 3, self._id, unit)
		end
	elseif inside and rule_ok then
		table.insert(self._inside, unit)
		managers.network:session():send_to_host("to_server_area_event", 1, self._id, unit)
	end

	return 
end
ElementAreaTrigger.operation_set_interval = function (self, interval)
	self._values.interval = interval

	self.remove_callback(self)

	if self._values.enabled then
		self.add_callback(self)
	end

	return 
end
ElementAreaTrigger.operation_set_use_disabled_shapes = function (self, use_disabled_shapes)
	self._values.use_disabled_shapes = use_disabled_shapes

	return 
end
ElementAreaTrigger.operation_clear_inside = function (self)
	self._inside = {}

	return 
end
ElementAreaTrigger.save = function (self, data)
	data.enabled = self._values.enabled
	data.interval = self._values.interval
	data.use_disabled_shapes = self._values.use_disabled_shapes

	return 
end
ElementAreaTrigger.load = function (self, data)
	if not self._on_script_activated_done then
		self.on_script_activated(self)
	end

	self.set_enabled(self, data.enabled)
	self.operation_set_interval(self, data.interval)

	self._values.use_disabled_shapes = data.use_disabled_shapes

	return 
end
ElementAreaOperator = ElementAreaOperator or class(CoreMissionScriptElement.MissionScriptElement)
ElementAreaOperator.init = function (self, ...)
	ElementAreaOperator.super.init(self, ...)

	return 
end
ElementAreaOperator.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementAreaOperator.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	for _, id in ipairs(self._values.elements) do
		local element = self.get_mission_element(self, id)

		if element then
			if self._values.apply_on_interval then
				element.operation_set_interval(element, self._values.interval)
			end

			if self._values.apply_on_use_disabled_shapes then
				element.operation_set_use_disabled_shapes(element, self._values.use_disabled_shapes)
			end

			if self._values.operation == "clear_inside" then
				element.operation_clear_inside(element)
			end
		end
	end

	ElementAreaOperator.super.on_executed(self, instigator)

	return 
end
ElementAreaReportTrigger = ElementAreaReportTrigger or class(ElementAreaTrigger)
ElementAreaReportTrigger.update_area = function (self)
	if not self._values.enabled then
		return 
	end

	local instigators = self.instigators(self)

	if #instigators == 0 and Network:is_server() then
		self._check_state(self, nil)
	else
		for _, unit in ipairs(instigators) do
			if alive(unit) then
				if Network:is_client() then
					self._client_check_state(self, unit)
				elseif Network:is_server() then
					self._check_state(self, unit)
				end
			end
		end
	end

	return 
end
ElementAreaReportTrigger._check_state = function (self, unit)
	self._clean_destroyed_units(self)

	if alive(unit) then
		local rule_ok = self._check_instigator_rules(self, unit)
		local inside = nil

		if unit.movement(unit) then
			inside = self._is_inside(self, unit.movement(unit):m_pos())
		else
			unit.m_position(unit, tmp_vec1)

			inside = self._is_inside(self, tmp_vec1)
		end

		if inside and not rule_ok then
			self._rule_failed(self, unit)
		end

		if table.contains(self._inside, unit) then
			if not inside or not rule_ok then
				self._remove_inside(self, unit)
			elseif inside and rule_ok then
				self._while_inside(self, unit)
			end
		elseif inside and rule_ok then
			self._add_inside(self, unit)
		end
	end

	local project_amount_all = self.project_amount_all(self)

	if project_amount_all ~= self._last_project_amount_all then
		self._last_project_amount_all = project_amount_all

		self._clean_destroyed_units(self)

		return true
	end

	return false
end
ElementAreaReportTrigger._add_inside = function (self, unit)
	table.insert(self._inside, unit)

	if self._has_on_executed_alternative(self, "while_inside") then
		self.on_executed(self, unit, "while_inside")
	else
		self.on_executed(self, unit, "enter")
	end

	self._check_on_executed_reached_amount(self, unit)

	return 
end
ElementAreaReportTrigger._check_on_executed_reached_amount = function (self, unit)
	local amount = self._values.amount == "all" and self.project_amount_all(self)
	amount = amount or tonumber(self._values.amount)

	if amount == #self._inside and self._has_on_executed_alternative(self, "reached_amount") then
		self.on_executed(self, unit, "reached_amount")
	end

	return 
end
ElementAreaReportTrigger._while_inside = function (self, unit)
	if self._has_on_executed_alternative(self, "while_inside") then
		self.on_executed(self, unit, "while_inside")
	end

	return 
end
ElementAreaReportTrigger._rule_failed = function (self, unit)
	if self._has_on_executed_alternative(self, "rule_failed") then
		self.on_executed(self, unit, "rule_failed")
	end

	return 
end
ElementAreaReportTrigger._remove_inside = function (self, unit)
	table.delete(self._inside, unit)
	self.on_executed(self, unit, "leave")

	if #self._inside == 0 then
		self.on_executed(self, unit, "empty")
	end

	self._check_on_executed_reached_amount(self, unit)

	return 
end
ElementAreaReportTrigger._remove_inside_by_index = function (self, index)
	table.remove(self._inside, index)
	self.on_executed(self, nil, "leave")

	if #self._inside == 0 then
		self.on_executed(self, nil, "empty")
	end

	self._check_on_executed_reached_amount(self, nil)

	return 
end
ElementAreaReportTrigger._clean_destroyed_units = function (self)
	local i = 1

	while next(self._inside) and i <= #self._inside do
		local unit = self._inside[i]

		if alive(unit) and (not unit.character_damage(unit) or not unit.character_damage(unit):dead()) then
			i = i + 1
		elseif alive(unit) and unit.character_damage(unit) and unit.character_damage(unit):dead() then
			self.on_executed(self, unit, "on_death")
		end

		self._remove_inside_by_index(self, i)
	end

	return 
end
ElementAreaReportTrigger._client_check_state = function (self, unit)
	local rule_ok = self._check_instigator_rules(self, unit)
	local inside = self._is_inside(self, unit.position(unit))

	if table.contains(self._inside, unit) then
		if not inside or not rule_ok then
			table.delete(self._inside, unit)
			managers.network:session():send_to_host("to_server_area_event", 2, self._id, unit)
		end
	elseif inside and rule_ok then
		table.insert(self._inside, unit)
		managers.network:session():send_to_host("to_server_area_event", 1, self._id, unit)
	end

	if inside then
		if rule_ok then
			if self._has_on_executed_alternative(self, "while_inside") then
				managers.network:session():send_to_host("to_server_area_event", 3, self._id, unit)
			end
		elseif self._has_on_executed_alternative(self, "rule_failed") then
			managers.network:session():send_to_host("to_server_area_event", 4, self._id, unit)
		end
	end

	return 
end
ElementAreaReportTrigger.sync_enter_area = function (self, unit)
	self._add_inside(self, unit)

	return 
end
ElementAreaReportTrigger.sync_exit_area = function (self, unit)
	self._remove_inside(self, unit)

	return 
end
ElementAreaReportTrigger.sync_while_in_area = function (self, unit)
	self._while_inside(self, unit)

	return 
end
ElementAreaReportTrigger.sync_rule_failed = function (self, unit)
	self._rule_failed(self, unit)

	return 
end

return 
