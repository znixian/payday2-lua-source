core:module("CoreElementInstance")
core:import("CoreMissionScriptElement")

ElementInstanceInput = ElementInstanceInput or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstanceInput.init = function (self, ...)
	ElementInstanceInput.super.init(self, ...)

	if self._values.instance_name then
		managers.world_instance:register_input_element(self._values.instance_name, self._values.event, self)
	end

	return 
end
ElementInstanceInput.client_on_executed = function (self, ...)
	return 
end
ElementInstanceInput.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementInstanceInput.super.on_executed(self, instigator)

	return 
end
ElementInstanceOutput = ElementInstanceOutput or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstanceOutput.init = function (self, ...)
	ElementInstanceOutput.super.init(self, ...)

	return 
end
ElementInstanceOutput.on_created = function (self)
	self._output_elements = managers.world_instance:get_registered_output_event_elements(self._values.instance_name, self._values.event)

	return 
end
ElementInstanceOutput.client_on_executed = function (self, ...)
	return 
end
ElementInstanceOutput.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._output_elements then
		for _, element in ipairs(self._output_elements) do
			element.on_executed(element, instigator)
		end
	end

	ElementInstanceOutput.super.on_executed(self, instigator)

	return 
end
ElementInstanceInputEvent = ElementInstanceInputEvent or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstanceInputEvent.init = function (self, ...)
	ElementInstanceInputEvent.super.init(self, ...)

	return 
end
ElementInstanceInputEvent.on_created = function (self)
	return 
end
ElementInstanceInputEvent.client_on_executed = function (self, ...)
	return 
end
ElementInstanceInputEvent.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.instance then
		local input_elements = managers.world_instance:get_registered_input_elements(self._values.instance, self._values.event)

		if input_elements then
			for _, element in ipairs(input_elements) do
				element.on_executed(element, instigator)
			end
		end
	elseif self._values.event_list then
		for _, event_list_data in ipairs(self._values.event_list) do
			local input_elements = managers.world_instance:get_registered_input_elements(event_list_data.instance, event_list_data.event)

			if input_elements then
				for _, element in ipairs(input_elements) do
					element.on_executed(element, instigator)
				end
			end
		end
	end

	ElementInstanceInputEvent.super.on_executed(self, instigator)

	return 
end
ElementInstanceOutputEvent = ElementInstanceOutputEvent or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstanceOutputEvent.init = function (self, ...)
	ElementInstanceOutputEvent.super.init(self, ...)

	if self._values.instance then
		managers.world_instance:register_output_event_element(self._values.instance, self._values.event, self)
	end

	if self._values.event_list then
		for _, event_list_data in ipairs(self._values.event_list) do
			managers.world_instance:register_output_event_element(event_list_data.instance, event_list_data.event, self)
		end
	end

	return 
end
ElementInstanceOutputEvent.client_on_executed = function (self, ...)
	return 
end
ElementInstanceOutputEvent.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	ElementInstanceOutputEvent.super.on_executed(self, instigator)

	return 
end
ElementInstancePoint = ElementInstancePoint or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstancePoint.client_on_executed = function (self, ...)
	return 
end
ElementInstancePoint.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	self._create(self)
	ElementInstancePoint.super.on_executed(self, instigator)

	return 
end
ElementInstancePoint._create = function (self)
	if self._has_created then
		return 
	end

	self._has_created = true

	if Network:is_server() then
		self._mission_script:add_save_state_cb(self._id)
	end

	if self._values.instance then
		managers.world_instance:custom_create_instance(self._values.instance, {
			position = self._values.position,
			rotation = self._values.rotation
		})
	elseif Application:editor() then
		managers.editor:output_error("[ElementInstancePoint:_create()] No instance defined in [" .. self._editor_name .. "]")
	end

	return 
end
ElementInstancePoint.save = function (self, data)
	data.has_created = self._has_created

	return 
end
ElementInstancePoint.load = function (self, data)
	if data.has_created then
		self._create(self)
	end

	return 
end
ElementInstanceParams = ElementInstanceParams or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstanceSetParams = ElementInstanceSetParams or class(CoreMissionScriptElement.MissionScriptElement)
ElementInstanceSetParams.init = function (self, ...)
	ElementInstanceOutputEvent.super.init(self, ...)

	if not self._values.apply_on_execute then
		self._apply_instance_params(self)
	end

	return 
end
ElementInstanceSetParams.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementInstanceSetParams._apply_instance_params = function (self)
	if self._values.instance then
		managers.world_instance:set_instance_params(self._values.instance, self._values.params)
	elseif Application:editor() then
		managers.editor:output_error("[ElementInstanceSetParams:_apply_instance_params()] No instance defined in [" .. self._editor_name .. "]")
	end

	return 
end
ElementInstanceSetParams.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	if self._values.apply_on_execute then
		self._apply_instance_params(self)
	end

	ElementInstanceSetParams.super.on_executed(self, instigator)

	return 
end

return 
