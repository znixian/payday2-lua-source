core:module("CoreMissionManager")
core:import("CoreMissionScriptElement")
core:import("CoreEvent")
core:import("CoreClass")
core:import("CoreDebug")
core:import("CoreCode")
core:import("CoreTable")
require("core/lib/managers/mission/CoreElementDebug")

MissionManager = MissionManager or CoreClass.class(CoreEvent.CallbackHandler)
MissionManager.init = function (self)
	MissionManager.super.init(self)

	self._runned_unit_sequences_callbacks = {}
	self._scripts = {}
	self._active_scripts = {}
	self._area_instigator_categories = {}

	self.add_area_instigator_categories(self, "none")
	self.set_default_area_instigator(self, "none")

	self._global_event_listener = rawget(_G, "EventListenerHolder"):new()
	self._global_event_list = {}

	return 
end
MissionManager.post_init = function (self)
	self._workspace = managers.gui_data:create_saferect_workspace()

	managers.gui_data:layout_corner_saferect_workspace(self._workspace)
	self._workspace:set_timer(TimerManager:main())

	self._fading_debug_output = self._workspace:panel():gui(Idstring("core/guis/core_fading_debug_output"))

	self._fading_debug_output:set_leftbottom(0, self._workspace:height()/3)
	self._fading_debug_output:script().configure({
		font_size = 18,
		max_rows = 20
	})

	self._persistent_debug_output = self._workspace:panel():gui(Idstring("core/guis/core_persistent_debug_output"))

	self._persistent_debug_output:set_righttop(self._workspace:width(), 0)
	self.set_persistent_debug_enabled(self, false)
	self.set_fading_debug_enabled(self, true)
	managers.viewport:add_resolution_changed_func(callback(self, self, "_resolution_changed"))

	return 
end
MissionManager._resolution_changed = function (self)
	managers.gui_data:layout_corner_saferect_workspace(self._workspace)

	return 
end
MissionManager.parse = function (self, params, stage_name, offset, file_type)
	local file_path, activate_mission = nil

	if CoreClass.type_name(params) == "table" then
		file_path = params.file_path
		file_type = params.file_type or "mission"
		activate_mission = params.activate_mission
		offset = params.offset
	else
		file_path = params
		file_type = file_type or "mission"
	end

	CoreDebug.cat_debug("gaspode", "MissionManager", file_path, file_type, activate_mission)

	if not DB:has(file_type, file_path) then
		Application:error("Couldn't find", file_path, "(", file_type, ")")

		return false
	end

	local reverse = string.reverse(file_path)
	local i = string.find(reverse, "/")
	local file_dir = string.reverse(string.sub(reverse, i))
	local continent_files = self._serialize_to_script(self, file_type, file_path)
	continent_files._meta = nil

	for name, data in pairs(continent_files) do
		if not managers.worlddefinition:continent_excluded(name) then
			self._load_mission_file(self, file_dir, data)
		end
	end

	self._activate_mission(self, activate_mission)

	return true
end
MissionManager._serialize_to_script = function (self, type, name)
	if Application:editor() then
		return PackageManager:editor_load_script_data(type.id(type), name.id(name))
	else
		if not PackageManager:has(type.id(type), name.id(name)) then
			Application:throw_exception("Script data file " .. name .. " of type " .. type .. " has not been loaded. Could be that old mission format is being loaded. Try resaving the level.")
		end

		return PackageManager:script_data(type.id(type), name.id(name))
	end

	return 
end
MissionManager._load_mission_file = function (self, file_dir, data)
	local file_path = file_dir .. data.file
	local scripts = self._serialize_to_script(self, "mission", file_path)
	scripts._meta = nil

	for name, data in pairs(scripts) do
		data.name = name

		self._add_script(self, data)
	end

	return 
end
MissionManager._add_script = function (self, data)
	self._scripts[data.name] = MissionScript:new(data)

	return 
end
MissionManager.scripts = function (self)
	return self._scripts
end
MissionManager.script = function (self, name)
	return self._scripts[name]
end
MissionManager._activate_mission = function (self, name)
	CoreDebug.cat_debug("gaspode", "MissionManager:_activate_mission", name)

	if name then
		if self.script(self, name) then
			self.activate_script(self, name)
		else
			Application:throw_exception("There was no mission named " .. name .. " availible to activate!")
		end
	else
		for _, script in pairs(self._scripts) do
			if script.activate_on_parsed(script) then
				self.activate_script(self, script.name(script))
			end
		end
	end

	return 
end
MissionManager.activate_script = function (self, name, ...)
	CoreDebug.cat_debug("gaspode", "MissionManager:activate_script", name)

	if not self._scripts[name] then
		if Global.running_simulation then
			managers.editor:output_error("Can't activate mission script " .. name .. ". It doesn't exist.")

			return 
		else
			Application:throw_exception("Can't activate mission script " .. name .. ". It doesn't exist.")
		end
	end

	self._scripts[name]:activate(...)

	return 
end
MissionManager.update = function (self, t, dt)
	for _, script in pairs(self._scripts) do
		script.update(script, t, dt)
	end

	return 
end
MissionManager.stop_simulation = function (self, ...)
	self.pre_destroy(self)

	for _, script in pairs(self._scripts) do
		script.stop_simulation(script, ...)
	end

	self._scripts = {}
	self._runned_unit_sequences_callbacks = {}
	self._global_event_listener = rawget(_G, "EventListenerHolder"):new()

	return 
end
MissionManager.on_simulation_started = function (self)
	self._pre_destroyed = nil

	return 
end
MissionManager.add_runned_unit_sequence_trigger = function (self, id, sequence, callback)
	if self._runned_unit_sequences_callbacks[id] then
		if self._runned_unit_sequences_callbacks[id][sequence] then
			table.insert(self._runned_unit_sequences_callbacks[id][sequence], callback)
		else
			self._runned_unit_sequences_callbacks[id][sequence] = {
				callback
			}
		end
	else
		local t = {
			[sequence] = {
				callback
			}
		}
		self._runned_unit_sequences_callbacks[id] = t
	end

	return 
end
MissionManager.runned_unit_sequence = function (self, unit, sequence, params)
	if self._pre_destroyed then
		return 
	end

	if alive(unit) and unit.unit_data(unit) then
		local id = unit.unit_data(unit).unit_id
		id = (id ~= 0 and id) or unit.editor_id(unit)

		if self._runned_unit_sequences_callbacks[id] and self._runned_unit_sequences_callbacks[id][sequence] then
			for _, call in ipairs(self._runned_unit_sequences_callbacks[id][sequence]) do
				call(params and params.unit)
			end
		end
	end

	return 
end
MissionManager.add_area_instigator_categories = function (self, category)
	table.insert(self._area_instigator_categories, category)

	return 
end
MissionManager.area_instigator_categories = function (self)
	return self._area_instigator_categories
end
MissionManager.set_default_area_instigator = function (self, default)
	self._default_area_instigator = default

	return 
end
MissionManager.default_area_instigator = function (self)
	return self._default_area_instigator
end
MissionManager.default_instigator = function (self)
	return nil
end
MissionManager.persistent_debug_enabled = function (self)
	return self._persistent_debug_enabled
end
MissionManager.set_persistent_debug_enabled = function (self, enabled)
	self._persistent_debug_enabled = enabled

	if enabled then
		self._persistent_debug_output:show()
	else
		self._persistent_debug_output:hide()
	end

	return 
end
MissionManager.add_persistent_debug_output = function (self, debug, color)
	if not self._persistent_debug_enabled then
		return 
	end

	self._persistent_debug_output:script().log(debug, color)

	return 
end
MissionManager.set_fading_debug_enabled = function (self, enabled)
	self._fading_debug_enabled = enabled

	if enabled then
		self._fading_debug_output:show()
	else
		self._fading_debug_output:hide()
	end

	return 
end
MissionManager.add_fading_debug_output = function (self, debug, color, as_subtitle)
	if not Application:production_build() then
		return 
	end

	if not self._fading_debug_enabled then
		return 
	end

	if as_subtitle then
		self._show_debug_subtitle(self, debug, color)
	else
		local stuff = {
			" -",
			" \\",
			" |",
			" /"
		}
		self._fade_index = (self._fade_index or 0) + 1
		self._fade_index = (#stuff < self._fade_index and self._fade_index and 1) or self._fade_index

		self._fading_debug_output:script().log(stuff[self._fade_index] .. " " .. debug, color, nil)
	end

	return 
end
MissionManager._show_debug_subtitle = function (self, debug, color)
	if not self._debug_subtitle_text then
		slot3 = self._workspace:panel():text({
			font_size = 20,
			wrap = true,
			word_wrap = true,
			align = "center",
			font = "core/fonts/diesel",
			halign = "center",
			valign = "center",
			text = debug,
			color = color or Color.white
		})
	end

	self._debug_subtitle_text = slot3

	self._debug_subtitle_text:set_w(self._workspace:panel():w()/2)
	self._debug_subtitle_text:set_text(debug)

	local subtitle_time = math.max(4, utf8.len(debug)*0.04)
	local _, _, w, h = self._debug_subtitle_text:text_rect()

	self._debug_subtitle_text:set_h(h)
	self._debug_subtitle_text:set_center_x(self._workspace:panel():w()/2)
	self._debug_subtitle_text:set_top(self._workspace:panel():h()/1.4)
	self._debug_subtitle_text:set_color(color or Color.white)
	self._debug_subtitle_text:set_alpha(1)
	self._debug_subtitle_text:stop()
	self._debug_subtitle_text:animate(function (o)
		_G.wait(subtitle_time)
		self._debug_subtitle_text:set_alpha(0)

		return 
	end)

	return 
end
MissionManager.get_element_by_id = function (self, id)
	for name, script in pairs(self._scripts) do
		if script.element(script, id) then
			return script.element(script, id)
		end
	end

	return 
end
MissionManager.add_global_event_listener = function (self, key, events, clbk)
	self._global_event_listener:add(key, events, clbk)

	return 
end
MissionManager.remove_global_event_listener = function (self, key)
	self._global_event_listener:remove(key)

	return 
end
MissionManager.call_global_event = function (self, event, ...)
	self._global_event_listener:call(event, ...)

	return 
end
MissionManager.set_global_event_list = function (self, list)
	self._global_event_list = list

	return 
end
MissionManager.get_global_event_list = function (self)
	return self._global_event_list
end
MissionManager.save = function (self, data)
	local state = {}

	for _, script in pairs(self._scripts) do
		script.save(script, state)
	end

	data.MissionManager = state

	return 
end
MissionManager.load = function (self, data)
	local state = data.MissionManager

	for _, script in pairs(self._scripts) do
		script.load(script, state)
	end

	return 
end
MissionManager.pre_destroy = function (self)
	self._pre_destroyed = true

	for _, script in pairs(self._scripts) do
		script.pre_destroy(script)
	end

	return 
end
MissionManager.destroy = function (self)
	for _, script in pairs(self._scripts) do
		script.destroy(script)
	end

	return 
end
MissionScript = MissionScript or CoreClass.class(CoreEvent.CallbackHandler)
MissionScript.imported_modules = MissionScript.imported_modules or {}

for module_name, _ in pairs(MissionScript.imported_modules) do
	MissionScript.import(module_name)
end

MissionScript.import = function (module_name)
	MissionScript.imported_modules[module_name] = true
	local module = core:import(module_name)

	return module
end
MissionScript.init = function (self, data)
	MissionScript.super.init(self)

	self._elements = {}
	self._element_groups = {}
	self._name = data.name
	self._activate_on_parsed = data.activate_on_parsed

	CoreDebug.cat_debug("gaspode", "New MissionScript:", self._name)
	self._create_elements(self, data.elements)

	if data.instances then
		for _, instance_name in ipairs(data.instances) do
			local instance_data = managers.world_instance:get_instance_data_by_name(instance_name)
			local prepare_mission_data = managers.world_instance:prepare_mission_data_by_name(instance_name)

			if not instance_data.mission_placed then
				self.create_instance_elements(self, prepare_mission_data)
			else
				self._preload_instance_class_elements(self, prepare_mission_data)
			end
		end
	end

	self._updators = {}
	self._save_states = {}

	self._on_created(self, self._elements)

	return 
end
MissionScript.external_create_instance_elements = function (self, prepare_mission_data)
	local new_elements = self.create_instance_elements(self, prepare_mission_data)

	self._on_created(self, new_elements)

	if self._active then
		self._on_script_activated(self, new_elements)
	end

	return 
end
MissionScript.create_instance_elements = function (self, prepare_mission_data)
	local new_elements = {}

	for _, instance_mission_data in pairs(prepare_mission_data) do
		new_elements = self._create_elements(self, instance_mission_data.elements)
	end

	return new_elements
end
MissionScript._preload_instance_class_elements = function (self, prepare_mission_data)
	for _, instance_mission_data in pairs(prepare_mission_data) do
		for _, element in ipairs(instance_mission_data.elements) do
			self._element_class(self, element.module, element.class)
		end
	end

	return 
end
MissionScript._create_elements = function (self, elements)
	local new_elements = {}

	for _, element in ipairs(elements) do
		local class = element.class
		local new_element = self._element_class(self, element.module, class):new(self, element)
		self._elements[element.id] = new_element
		new_elements[element.id] = new_element
		self._element_groups[class] = self._element_groups[class] or {}

		table.insert(self._element_groups[class], new_element)
	end

	return new_elements
end
MissionScript.activate_on_parsed = function (self)
	return self._activate_on_parsed
end
MissionScript._on_created = function (self, elements)
	for _, element in pairs(elements) do
		element.on_created(element)
	end

	return 
end
MissionScript._element_class = function (self, module_name, class_name)
	local element_class = rawget(_G, class_name)

	if not element_class and module_name and module_name ~= "none" then
		local raw_module = rawget(_G, "CoreMissionManager")[module_name]
		element_class = (raw_module and raw_module[class_name]) or MissionScript.import(module_name)[class_name]
	end

	if not element_class then
		element_class = CoreMissionScriptElement.MissionScriptElement

		Application:error("[MissionScript]SCRIPT ERROR: Didn't find class", class_name, module_name)
	end

	return element_class
end
MissionScript.activate = function (self, ...)
	self._active = true

	managers.mission:add_persistent_debug_output("")
	managers.mission:add_persistent_debug_output("Activate mission " .. self._name, Color(1, 0, 1, 0))
	self._on_script_activated(self, CoreTable.clone(self._elements), ...)

	return 
end
MissionScript._on_script_activated = function (self, elements, ...)
	for _, element in pairs(elements) do
		element.on_script_activated(element)
	end

	for _, element in pairs(elements) do
		if element.value(element, "execute_on_startup") then
			element.on_executed(element, ...)
		end
	end

	return 
end
MissionScript.add_updator = function (self, id, updator)
	self._updators[id] = updator

	return 
end
MissionScript.remove_updator = function (self, id)
	self._updators[id] = nil

	return 
end
MissionScript.update = function (self, t, dt)
	MissionScript.super.update(self, dt)

	for _, updator in pairs(self._updators) do
		updator(t, dt)
	end

	return 
end
MissionScript._debug_draw = function (self, t, dt)
	local brush = Draw:brush(Color.red)
	local name_brush = Draw:brush(Color.red)

	name_brush.set_font(name_brush, Idstring("fonts/font_medium"), 16)
	name_brush.set_render_template(name_brush, Idstring("OverlayVertexColorTextured"))

	for _, element in pairs(self._elements) do
		brush.set_color(brush, (element.enabled(element) and Color.green) or Color.red)
		name_brush.set_color(name_brush, (element.enabled(element) and Color.green) or Color.red)

		if element.value(element, "position") then
			brush.sphere(brush, element.value(element, "position"), 5)

			if managers.viewport:get_current_camera() then
				local cam_up = managers.viewport:get_current_camera():rotation():z()
				local cam_right = managers.viewport:get_current_camera():rotation():x()

				name_brush.center_text(name_brush, element.value(element, "position") + Vector3(0, 0, 30), utf8.from_latin1(element.editor_name(element)), cam_right, -cam_up)
			end
		end

		if element.value(element, "rotation") then
			local rotation = (CoreClass.type_name(element.value(element, "rotation")) == "Rotation" and element.value(element, "rotation")) or Rotation(element.value(element, "rotation"), 0, 0)

			brush.cylinder(brush, element.value(element, "position"), element.value(element, "position") + rotation.y(rotation)*50, 2)
			brush.cylinder(brush, element.value(element, "position"), element.value(element, "position") + rotation.z(rotation)*25, 1)
		end

		element.debug_draw(element, t, dt)
	end

	return 
end
MissionScript.name = function (self)
	return self._name
end
MissionScript.element_groups = function (self)
	return self._element_groups
end
MissionScript.element_group = function (self, name)
	return self._element_groups[name]
end
MissionScript.elements = function (self)
	return self._elements
end
MissionScript.element = function (self, id)
	return self._elements[id]
end
MissionScript.debug_output = function (self, debug, color)
	managers.mission:add_persistent_debug_output(Application:date("%X") .. ": " .. debug, color)
	CoreDebug.cat_print("editor", debug)

	return 
end
MissionScript.is_debug = function (self)
	return true
end
MissionScript.add_save_state_cb = function (self, id)
	self._save_states[id] = true

	return 
end
MissionScript.remove_save_state_cb = function (self, id)
	self._save_states[id] = nil

	return 
end
MissionScript.save = function (self, data)
	local state = {}

	for id, _ in pairs(self._save_states) do
		state[id] = {}

		self._elements[id]:save(state[id])
	end

	data[self._name] = state

	return 
end
MissionScript.load = function (self, data)
	local state = data[self._name]

	if self._element_groups.ElementInstancePoint then
		for _, element in ipairs(self._element_groups.ElementInstancePoint) do
			if state[element.id(element)] then
				self._elements[element.id(element)]:load(state[element.id(element)])

				state[element.id(element)] = nil
			end
		end
	end

	for id, mission_state in pairs(state) do
		self._elements[id]:load(mission_state)
	end

	return 
end
MissionScript.stop_simulation = function (self, ...)
	for _, element in pairs(self._elements) do
		element.stop_simulation(element, ...)
	end

	MissionScript.super.clear(self)

	return 
end
MissionScript.pre_destroy = function (self, ...)
	for _, element in pairs(self._elements) do
		element.pre_destroy(element, ...)
	end

	MissionScript.super.clear(self)

	return 
end
MissionScript.destroy = function (self, ...)
	for _, element in pairs(self._elements) do
		element.destroy(element, ...)
	end

	MissionScript.super.clear(self)

	return 
end

return 
