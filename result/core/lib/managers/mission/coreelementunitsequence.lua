core:module("CoreElementUnitSequence")
core:import("CoreMissionScriptElement")
core:import("CoreCode")
core:import("CoreUnit")

ElementUnitSequence = ElementUnitSequence or class(CoreMissionScriptElement.MissionScriptElement)
ElementUnitSequence.init = function (self, ...)
	ElementUnitSequence.super.init(self, ...)

	self._unit = CoreUnit.safe_spawn_unit("core/units/run_sequence_dummy/run_sequence_dummy", self._values.position)

	managers.worlddefinition:add_trigger_sequence(self._unit, self._values.trigger_list)

	return 
end
ElementUnitSequence.on_script_activated = function (self)
	self._mission_script:add_save_state_cb(self._id)

	return 
end
ElementUnitSequence.client_on_executed = function (self, ...)
	self.on_executed(self, ...)

	return 
end
ElementUnitSequence.on_executed = function (self, instigator)
	if not self._values.enabled then
		return 
	end

	local run_sequence = true

	if self._values.only_for_local_player then
		run_sequence = not managers.player:player_unit() or instigator == managers.player:player_unit()
	end

	if run_sequence then
		self._unit:damage():run_sequence_simple("run_sequence")
	end

	ElementUnitSequence.super.on_executed(self, instigator)

	return 
end
ElementUnitSequence.save = function (self, data)
	data.enabled = self._values.enabled

	return 
end
ElementUnitSequence.load = function (self, data)
	self.set_enabled(self, data.enabled)

	return 
end

return 
