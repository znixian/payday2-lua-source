require("core/lib/managers/cutscene/CoreCutsceneKeys")
require("core/lib/managers/cutscene/CoreCutsceneKeyCollection")

CoreCutscene = CoreCutscene or frozen_class()

mixin(CoreCutscene, CoreCutsceneKeyCollection)

local CUTSCENE_FRAMES_PER_SECOND = 30
CoreCutscene._all_keys_sorted_by_time = function (self)
	return self._keys or {}
end
CoreCutscene.init = function (self, cutscene_node, cutscene_manager)
	assert(cutscene_node, "No cutscene XML node supplied.")
	assert(cutscene_manager, "Must supply a reference to the CutsceneManager.")

	self._name = cutscene_node.parameter(cutscene_node, "name")
	self._unit_name = cutscene_node.parameter(cutscene_node, "unit")
	self._frame_count = tonumber(cutscene_node.parameter(cutscene_node, "frames"))
	self._keys = {}
	self._unit_types = {}
	self._unit_animations = {}
	self._unit_blend_sets = {}
	self._camera_names = {}
	self._animation_blobs = self._parse_animation_blobs(self, cutscene_node)

	for collection_node in cutscene_node.children(cutscene_node) do
		if collection_node.name(collection_node) == "controlled_units" then
			for child_node in collection_node.children(collection_node) do
				local unit_name = child_node.parameter(child_node, "name")
				self._unit_types[unit_name] = cutscene_manager.cutscene_actor_unit_type(cutscene_manager, self._cutscene_specific_unit_type(self, child_node.parameter(child_node, "type")))
				self._unit_animations[unit_name] = child_node.parameter(child_node, "animation")
				self._unit_blend_sets[unit_name] = child_node.parameter(child_node, "blend_set")

				if string.begins(unit_name, "camera") then
					table.insert(self._camera_names, unit_name)
				end
			end
		elseif collection_node.name(collection_node) == "keys" then
			for child_node in collection_node.children(collection_node) do
				local cutscene_key = CoreCutsceneKey:create(child_node.name(child_node), self)

				cutscene_key.load(cutscene_key, child_node)
				table.insert(self._keys, freeze(cutscene_key))
			end
		end
	end

	table.sort(self._camera_names)
	table.sort(self._keys, function (a, b)
		return a.frame(a) < b.frame(b)
	end)
	freeze(self._keys, self._unit_types, self._unit_animations, self._unit_blend_sets, self._camera_names)

	return 
end
CoreCutscene.is_valid = function (self)
	return table.empty(self._unit_types) or DB:has("unit", self.unit_name(self))
end
CoreCutscene.name = function (self)
	return self._name or ""
end
CoreCutscene.unit_name = function (self)
	return self._unit_name or ""
end
CoreCutscene.frames_per_second = function (self)
	return CUTSCENE_FRAMES_PER_SECOND
end
CoreCutscene.frame_count = function (self)
	return self._frame_count or 1
end
CoreCutscene.duration = function (self)
	return self.frame_count(self)/self.frames_per_second(self)
end
CoreCutscene.is_optimized = function (self)
	return table.empty(self._unit_animations)
end
CoreCutscene.has_cameras = function (self)
	return not table.empty(self._camera_names)
end
CoreCutscene.has_unit = function (self, unit_name, include_units_spawned_through_keys)
	if self.controlled_unit_types(self)[unit_name] ~= nil then
		return true
	end

	if include_units_spawned_through_keys then
		for spawn_key in self.keys(self, CoreSpawnUnitCutsceneKey.ELEMENT_NAME) do
			if spawn_key.name(spawn_key) == unit_name then
				return true
			end
		end
	end

	return false
end
CoreCutscene.controlled_unit_types = function (self)
	return self._unit_types
end
CoreCutscene.camera_names = function (self)
	return self._camera_names
end
CoreCutscene.default_camera = function (self)
	for _, name in ipairs(self.camera_names(self)) do
		return name
	end

	return 
end
CoreCutscene.objects_in_unit = function (self, unit_name)
	return self._actor_database_info(self, unit_name):object_names()
end
CoreCutscene.extensions_on_unit = function (self, unit_name)
	return self._actor_database_info(self, unit_name):extensions()
end
CoreCutscene.animation_for_unit = function (self, unit_name)
	return self._unit_animations[unit_name]
end
CoreCutscene.blend_set_for_unit = function (self, unit_name)
	return self._unit_blend_sets[unit_name] or "all"
end
CoreCutscene.animation_blobs = function (self)
	return self._animation_blobs
end
CoreCutscene.find_spawned_orientation_unit = function (self)
	local spawned_cutscene_units = World:unit_manager():get_units(managers.slot:get_mask("cutscenes"))

	for _, unit in ipairs(spawned_cutscene_units) do
		if unit.name(unit) == self.unit_name(self) then
			return unit
		end
	end

	return 
end
CoreCutscene._parse_animation_blobs = function (self, cutscene_node)
	return self._parse_animation_blob_list(self, cutscene_node) or self._parse_single_animation_blob(self, cutscene_node)
end
CoreCutscene._parse_animation_blob_list = function (self, cutscene_node)
	for collection_node in cutscene_node.children(cutscene_node) do
		if collection_node.name(collection_node) == "animation_blobs" then
			local animation_blobs = {}

			for animation_blob_node in collection_node.children(collection_node) do
				local value = (animation_blob_node.name(animation_blob_node) == "part" and animation_blob_node.parameter(animation_blob_node, "animation_blob")) or nil

				if value then
					table.insert(animation_blobs, value)
				end
			end

			return animation_blobs
		end
	end

	return nil
end
CoreCutscene._parse_single_animation_blob = function (self, cutscene_node)
	for collection_node in cutscene_node.children(cutscene_node) do
		if collection_node.name(collection_node) == "controlled_units" then
			local animation_blob = collection_node.parameter(collection_node, "animation_blob")

			return animation_blob and {
				animation_blob
			}
		end
	end

	return nil
end
CoreCutscene._actor_database_info = function (self, unit_name)
	local unit_type = assert(self.controlled_unit_types(self)[unit_name], string.format("Unit \"%s\" is not in cutscene \"%s\".", unit_name, self.name(self)))
	local unit_info = assert(managers.cutscene:actor_database():unit_type_info(unit_type), string.format("Unit type \"%s\", used in cutscene \"%s\", is not registered in the actor database.", unit_type, self.name(self)))

	return unit_info
end
CoreCutscene._cutscene_specific_unit_type = function (self, unit_type)
	if unit_type ~= "locator" and DB:has("unit", unit_type .. "_cutscene") then
		unit_type = unit_type .. "_cutscene"
	end

	return unit_type
end
CoreCutscene._debug_persistent_keys = function (self)
	local persistent_keys = {}
	local unit_types = self.controlled_unit_types(self)

	for sequence_key in self.keys(self, CoreSequenceCutsceneKey.ELEMENT_NAME) do
		local unit_type = unit_types[sequence_key.unit_name(sequence_key)]
		persistent_keys[string.format("Sequence %s.%s", unit_type or "\"" .. sequence_key.unit_name(sequence_key) .. "\"", sequence_key.name(sequence_key))] = true
	end

	for callback_key in self.keys(self, CoreUnitCallbackCutsceneKey.ELEMENT_NAME) do
		local unit_type = unit_types[callback_key.unit_name(callback_key)]
		persistent_keys[string.format("Callback %s:%s():%s()", unit_type or "\"" .. callback_key.unit_name(callback_key) .. "\"", callback_key.extension(callback_key), callback_key.method(callback_key))] = true
	end

	return persistent_keys
end

return 
