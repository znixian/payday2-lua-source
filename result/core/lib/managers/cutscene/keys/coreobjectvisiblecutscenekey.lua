require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreObjectVisibleCutsceneKey = CoreObjectVisibleCutsceneKey or class(CoreCutsceneKeyBase)
CoreObjectVisibleCutsceneKey.ELEMENT_NAME = "object_visible"
CoreObjectVisibleCutsceneKey.NAME = "Object Visibility"

CoreObjectVisibleCutsceneKey:register_serialized_attribute("unit_name", "")
CoreObjectVisibleCutsceneKey:register_serialized_attribute("object_name", "")
CoreObjectVisibleCutsceneKey:register_serialized_attribute("visible", true, toboolean)

CoreObjectVisibleCutsceneKey.__tostring = function (self)
	return ((self.visible(self) and "Show") or "Hide") .. " \"" .. self.object_name(self) .. "\" in \"" .. self.unit_name(self) .. "\"."
end
CoreObjectVisibleCutsceneKey.unload = function (self, player)
	if player and self._cast then
		self.play(self, player, true)
	end

	return 
end
CoreObjectVisibleCutsceneKey.skip = function (self, player)
	if self._cast then
		self.play(self, player)
	end

	return 
end
CoreObjectVisibleCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		local preceeding_key = self.preceeding_key(self, {
			unit_name = self.unit_name(self),
			object_name = self.object_name(self)
		})

		if preceeding_key then
			preceeding_key.evaluate(preceeding_key, player, false)
		else
			self.evaluate(self, player, false, self._unit_initial_object_visibility(self, self.unit_name(self), self.object_name(self)))
		end
	else
		self.evaluate(self, player, fast_forward)
	end

	return 
end
CoreObjectVisibleCutsceneKey.evaluate = function (self, player, fast_forward, visible)
	assert(self._cast)

	local object = self._unit_object(self, self.unit_name(self), self.object_name(self))

	object.set_visibility(object, (visible == nil and self.visible(self)) or visible)

	return 
end
CoreObjectVisibleCutsceneKey.is_valid_object_name = function (self, object_name)
	if not self.super.is_valid_object_name(self, object_name) then
		return false
	else
		local object = self._unit_object(self, self.unit_name(self), object_name, true)

		return object and object.set_visibility ~= nil
	end

	return 
end

return 
