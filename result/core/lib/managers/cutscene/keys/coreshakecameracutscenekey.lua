require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreShakeCameraCutsceneKey = CoreShakeCameraCutsceneKey or class(CoreCutsceneKeyBase)
CoreShakeCameraCutsceneKey.ELEMENT_NAME = "camera_shaker"
CoreShakeCameraCutsceneKey.NAME = "Camera Shake"

CoreShakeCameraCutsceneKey:register_serialized_attribute("name", "")
CoreShakeCameraCutsceneKey:register_serialized_attribute("amplitude", 1, tonumber)
CoreShakeCameraCutsceneKey:register_serialized_attribute("frequency", 1, tonumber)
CoreShakeCameraCutsceneKey:register_serialized_attribute("offset", 0, tonumber)

CoreShakeCameraCutsceneKey.__tostring = function (self)
	return "Trigger camera shake \"" .. self.name(self) .. "\"."
end
CoreShakeCameraCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		self.stop(self)
	elseif not fast_forward then
		self.stop(self)

		self._shake_abort_func = player.play_camera_shake(player, self.name(self), self.amplitude(self), self.frequency(self), self.offset(self))
	end

	return 
end
CoreShakeCameraCutsceneKey.stop = function (self)
	if self._shake_abort_func then
		self._shake_abort_func()

		self._shake_abort_func = nil
	end

	return 
end

return 
