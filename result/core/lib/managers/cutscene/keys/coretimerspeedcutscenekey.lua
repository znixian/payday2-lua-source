require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreTimerSpeedCutsceneKey = CoreTimerSpeedCutsceneKey or class(CoreCutsceneKeyBase)
CoreTimerSpeedCutsceneKey.ELEMENT_NAME = "timer_speed"
CoreTimerSpeedCutsceneKey.NAME = "Timer Speed"

CoreTimerSpeedCutsceneKey:register_serialized_attribute("speed", 1, tonumber)
CoreTimerSpeedCutsceneKey:register_serialized_attribute("duration", 0, tonumber)

CoreTimerSpeedCutsceneKey.__tostring = function (self)
	return string.format("Change timer speed to \"%g\" over \"%g\" seconds.", self.speed(self), self.duration(self))
end
CoreTimerSpeedCutsceneKey.unload = function (self, player)
	self._set_timer_speed(self, 1, 0)

	return 
end
CoreTimerSpeedCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		local preceeding_key = self.preceeding_key(self)

		if preceeding_key then
			self._set_timer_speed(self, preceeding_key.speed(preceeding_key), preceeding_key.duration(preceeding_key))
		else
			self._set_timer_speed(self, 1, 0)
		end
	else
		self._set_timer_speed(self, self.speed(self), self.duration(self))
	end

	return 
end
CoreTimerSpeedCutsceneKey._set_timer_speed = function (self, speed, duration)
	speed = math.max(speed, 0)
	duration = math.max(duration, 0)

	if 0 < speed and speed < 0.035 then
		speed = 0.035
	end

	if 0 < duration and duration < 0.035 then
		duration = 0
	end

	TimerManager:ramp_multiplier(TimerManager:game(), speed, duration, TimerManager:pausable())
	TimerManager:ramp_multiplier(TimerManager:game_animation(), speed, duration, TimerManager:pausable())

	return 
end
CoreTimerSpeedCutsceneKey.is_valid_speed = function (self, speed)
	return speed ~= nil and 0.035 <= speed
end
CoreTimerSpeedCutsceneKey.is_valid_duration = function (self, duration)
	return duration ~= nil and 0 <= duration
end

return 
