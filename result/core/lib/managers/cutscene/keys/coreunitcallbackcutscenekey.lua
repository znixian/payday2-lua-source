require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreUnitCallbackCutsceneKey = CoreUnitCallbackCutsceneKey or class(CoreCutsceneKeyBase)
CoreUnitCallbackCutsceneKey.ELEMENT_NAME = "unit_callback"
CoreUnitCallbackCutsceneKey.NAME = "Unit Callback"

CoreUnitCallbackCutsceneKey:register_serialized_attribute("unit_name", "")
CoreUnitCallbackCutsceneKey:register_serialized_attribute("extension", "")
CoreUnitCallbackCutsceneKey:register_serialized_attribute("method", "")
CoreUnitCallbackCutsceneKey:register_serialized_attribute("enabled", true, toboolean)
CoreUnitCallbackCutsceneKey:register_serialized_attribute("arguments")
CoreUnitCallbackCutsceneKey:attribute_affects("unit_name", "extension")
CoreUnitCallbackCutsceneKey:attribute_affects("extension", "method")
CoreUnitCallbackCutsceneKey:attribute_affects("method", "arguments")

CoreUnitCallbackCutsceneKey.control_for_unit_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreUnitCallbackCutsceneKey.control_for_extension = CoreCutsceneKeyBase.standard_combo_box_control
CoreUnitCallbackCutsceneKey.control_for_method = CoreCutsceneKeyBase.standard_combo_box_control
CoreUnitCallbackCutsceneKey.__tostring = function (self)
	return "Call " .. self.unit_name(self) .. ":" .. self.extension(self) .. "():" .. self.method(self) .. "(" .. self.arguments_string(self) .. ")"
end
CoreUnitCallbackCutsceneKey.arguments_string = function (self)
	return string.join(", ", table.collect((self._method_params and self._method_params[self.method(self)]) or {}, function (p)
		return p.inspect(p)
	end))
end
CoreUnitCallbackCutsceneKey.load = function (self, key_node, loading_class)
	self.super.load(self, key_node, loading_class)

	self._method_params = {}
	local params = {}

	for param_node in key_node.children(key_node) do
		local param = CoreUnitCallbackCutsceneKeyParam:new()

		param.load(param, param_node)
		table.insert(params, param)
	end

	if self.is_valid_method(self, self.method(self)) then
		self._method_params[self.method(self)] = params
	end

	return 
end
CoreUnitCallbackCutsceneKey._save_under = function (self, parent_node)
	local key_node = self.super._save_under(self, parent_node)
	slot3 = ipairs
	slot4 = (self._method_params and self._method_params[self.method(self)]) or {}

	for _, param in slot3(slot4) do
		if not param.is_nil(param) then
			param._save_under(param, key_node)
		end
	end

	return key_node
end
CoreUnitCallbackCutsceneKey.play = function (self, player, undo, fast_forward)
	if self.enabled(self) then
		local method_name = (undo and "undo_" .. self.method(self)) or self.method(self)

		self._invoke_if_exists(self, method_name, player)
	end

	return 
end
CoreUnitCallbackCutsceneKey.skip = function (self, player)
	if self.enabled(self) then
		self._invoke_if_exists(self, "skip_" .. self.method(self), player)
	end

	return 
end
CoreUnitCallbackCutsceneKey.is_valid_unit_name = function (self, unit_name)
	return self.super.is_valid_unit_name(self, unit_name) and not table.empty(self._unit_extension_info(self, unit_name))
end
CoreUnitCallbackCutsceneKey.is_valid_extension = function (self, extension)
	local methods = self._unit_extension_info(self, self.unit_name(self))[extension]

	return methods and not table.empty(methods)
end
CoreUnitCallbackCutsceneKey.is_valid_method = function (self, method)
	return method ~= nil and not string.begins(method, "undo_") and not string.begins(method, "skip_")
end
CoreUnitCallbackCutsceneKey.refresh_control_for_extension = function (self, control)
	control.freeze(control)
	control.clear(control)

	local unit_extensions = table.find_all_values(table.map_keys(self._unit_extension_info(self, self.unit_name(self))), function (e)
		return self:is_valid_extension(e)
	end)

	if not table.empty(unit_extensions) then
		control.set_enabled(control, true)

		local value = self.extension(self)

		for _, extension in ipairs(unit_extensions) do
			control.append(control, extension)

			if extension == value then
				control.set_value(control, value)
			end
		end
	else
		control.set_enabled(control, false)
	end

	control.thaw(control)

	return 
end
CoreUnitCallbackCutsceneKey.refresh_control_for_method = function (self, control)
	control.freeze(control)
	control.clear(control)

	local methods = self._unit_extension_info(self, self.unit_name(self))[self.extension(self)]

	if methods then
		control.set_enabled(control, true)

		local value = self.method(self)

		for _, method in ipairs(table.map_keys(methods)) do
			control.append(control, method)

			if method == value then
				control.set_value(control, value)
			end
		end
	else
		control.set_enabled(control, false)
	end

	control.thaw(control)

	return 
end
CoreUnitCallbackCutsceneKey.refresh_control_for_arguments = function (self, panel)
	panel.freeze(panel)
	panel.destroy_children(panel)

	local panel_sizer = EWS:BoxSizer("VERTICAL")
	local methods = self._unit_extension_info(self, self.unit_name(self))[self.extension(self)]
	local method_arguments = (methods and methods[self.method(self)]) or {}

	if 0 < #method_arguments then
		local headline = EWS:StaticText(panel, "Method Arguments")

		headline.set_font_size(headline, 10)
		panel_sizer.add(panel_sizer, EWS:StaticLine(panel), 0, 10, "TOP,EXPAND")
		panel_sizer.add(panel_sizer, headline, 0, 5, "ALL,EXPAND")
		panel_sizer.add(panel_sizer, EWS:StaticLine(panel), 0, 0, "EXPAND")

		for _, argument_name in ipairs(method_arguments) do
			local param = self._param_with_name(self, argument_name)
			local value_field = EWS:TextCtrl(panel, "")

			value_field.set_min_size(value_field, value_field.get_min_size(value_field):with_x(0))
			value_field.connect(value_field, "EVT_COMMAND_TEXT_UPDATED", function ()
				param.string_value = value_field:get_value()

				return 
			end)
			value_field.set_value(value_field, param.string_value)
			value_field.set_enabled(value_field, param.value_type ~= "nil")

			local type_options = {
				"nil",
				"string",
				"number",
				"bool",
				"unit"
			}
			local type_selector = EWS:ComboBox(panel, "", "", "CB_DROPDOWN,CB_READONLY")

			type_selector.connect(type_selector, "EVT_COMMAND_COMBOBOX_SELECTED", function ()
				param.value_type = type_selector:get_value()

				value_field:set_enabled(param.value_type ~= "nil")

				return 
			end)

			for _, option in ipairs(type_options) do
				type_selector.append(type_selector, option)

				if param.value_type == option then
					type_selector.set_value(type_selector, option)
				end
			end

			local type_and_value_sizer = EWS:BoxSizer("HORIZONTAL")

			type_and_value_sizer.add(type_and_value_sizer, type_selector, 0, 5, "RIGHT,EXPAND")
			type_and_value_sizer.add(type_and_value_sizer, value_field, 1, 0, "EXPAND")
			panel_sizer.add(panel_sizer, EWS:StaticText(panel, string.pretty(param.name, true) .. ":"), 0, 5, "TOP,LEFT,RIGHT")
			panel_sizer.add(panel_sizer, type_and_value_sizer, 0, 5, "ALL,EXPAND")
		end
	end

	panel.set_sizer(panel, panel_sizer)
	panel.thaw(panel)

	return 
end
CoreUnitCallbackCutsceneKey.control_for_arguments = function (self, parent_frame, callback_func)
	local panel = EWS:Panel(parent_frame)

	return panel
end
CoreUnitCallbackCutsceneKey._invoke_if_exists = function (self, method_name, player)
	local extension = self._unit_extension(self, self.unit_name(self), self.extension(self), true)

	if not extension then
		Application:error("Unit \"" .. self.unit_name(self) .. "\" does not have the extension \"" .. self.extension(self) .. "\".")

		return 
	end

	local func = extension[method_name]

	if not func then
		Application:error(string.pretty(self.extension(self), true) .. " extension on unit \"" .. self.unit_name(self) .. "\" does not support the call \"" .. method_name .. "\".")

		return 
	end

	local params = (self._method_params and self._method_params[self.method(self)]) or {}
	local param_values = {}

	for index, param in ipairs(params) do
		local value = param.value(param, self, player)

		if value == nil and not param.is_nil(param) then
			local parameter_names = string.join(", ", table.collect(params, function (p)
				return p.name
			end))

			Application:error(string.format("Bad argument %s in call to %s:%s():%s(%s)", param.__tostring(param), self.unit_name(self), self.extension(self), method_name, parameter_names))

			return 
		else
			param_values[index] = value
		end
	end

	func(extension, table.unpack_sparse(param_values))

	return 
end
CoreUnitCallbackCutsceneKey._param_with_name = function (self, param_name)
	assert(self.is_valid_method(self, self.method(self)), "Method \"" .. self.method(self) .. "\" is invalid.")

	local params = self._method_params and self._method_params[self.method(self)]

	if params == nil then
		params = {}
		self._method_params = self._method_params or {}
		self._method_params[self.method(self)] = params
	end

	local param = table.find_value(params, function (p)
		return p.name == param_name
	end)

	if param == nil then
		param = CoreUnitCallbackCutsceneKeyParam.new()
		param.name = param_name

		table.insert(params, param)
	end

	return param
end
CoreUnitCallbackCutsceneKeyParam = CoreUnitCallbackCutsceneKeyParam or class()
CoreUnitCallbackCutsceneKeyParam.__tostring = function (self)
	return tostring(self.name) .. "=" .. tostring(self.inspect(self))
end
CoreUnitCallbackCutsceneKeyParam.init = function (self)
	self.value_type = "nil"
	self.name = "nil"
	self.string_value = ""

	return 
end
CoreUnitCallbackCutsceneKeyParam.load = function (self, param_node)
	self.value_type = param_node.name(param_node)
	self.name = param_node.parameter(param_node, "name")
	self.string_value = param_node.parameter(param_node, "value")

	return 
end
CoreUnitCallbackCutsceneKeyParam.value = function (self, cutscene_key)
	if not self.is_nil(self) then
		if self.value_type == "string" then
			return self.string_value
		elseif self.value_type == "number" then
			return tonumber(self.string_value)
		elseif self.value_type == "boolean" then
			return toboolean(self.string_value)
		elseif self.value_type == "unit" then
			return cutscene_key and cutscene_key._unit(cutscene_key, self.string_value, true)
		end
	end

	return nil
end
CoreUnitCallbackCutsceneKeyParam.is_nil = function (self)
	return self.value_type == "nil"
end
CoreUnitCallbackCutsceneKeyParam.inspect = function (self)
	return self.string_value
end
CoreUnitCallbackCutsceneKeyParam._save_under = function (self, parent_node)
	local param_node = parent_node.make_child(parent_node, self.value_type)

	param_node.set_parameter(param_node, "name", tostring(self.name))
	param_node.set_parameter(param_node, "value", self.string_value)

	return param_node
end

return 
