require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreSimpleAnimationCutsceneKey = CoreSimpleAnimationCutsceneKey or class(CoreCutsceneKeyBase)
CoreSimpleAnimationCutsceneKey.ELEMENT_NAME = "simple_animation"
CoreSimpleAnimationCutsceneKey.NAME = "Simple Animation"

CoreSimpleAnimationCutsceneKey:register_serialized_attribute("unit_name", "")
CoreSimpleAnimationCutsceneKey:register_serialized_attribute("group", "")
CoreSimpleAnimationCutsceneKey:attribute_affects("unit_name", "group")

CoreSimpleAnimationCutsceneKey.control_for_group = CoreCutsceneKeyBase.standard_combo_box_control
CoreSimpleAnimationCutsceneKey.__tostring = function (self)
	return "Trigger simple animation \"" .. self.group(self) .. "\" on \"" .. self.unit_name(self) .. "\"."
end
CoreSimpleAnimationCutsceneKey.skip = function (self, player)
	local unit = self._unit(self, self.unit_name(self))
	local group = self.group(self)

	unit.anim_play(unit, group, 0)
	unit.anim_set_time(unit, group, unit.anim_length(unit, group))

	return 
end
CoreSimpleAnimationCutsceneKey.evaluate = function (self, player, fast_forward)
	self._unit(self, self.unit_name(self)):anim_play(self.group(self), 0)

	return 
end
CoreSimpleAnimationCutsceneKey.revert = function (self, player)
	local unit = self._unit(self, self.unit_name(self))
	local group = self.group(self)

	if unit.anim_is_playing(unit, group) then
		unit.anim_set_time(unit, group, 0)
		unit.anim_stop(unit, group)
	end

	return 
end
CoreSimpleAnimationCutsceneKey.update = function (self, player, time)
	self._unit(self, self.unit_name(self)):anim_set_time(self.group(self), time)

	return 
end
CoreSimpleAnimationCutsceneKey.is_valid_unit_name = function (self, unit_name)
	return self.super.is_valid_unit_name(self, unit_name) and 0 < #self._unit_animation_groups(self, unit_name)
end
CoreSimpleAnimationCutsceneKey.is_valid_group = function (self, group)
	return table.contains(self._unit_animation_groups(self, self.unit_name(self)), group)
end
CoreSimpleAnimationCutsceneKey.refresh_control_for_group = function (self, control)
	control.freeze(control)
	control.clear(control)

	local groups = self._unit_animation_groups(self, self.unit_name(self))

	if not table.empty(groups) then
		control.set_enabled(control, true)

		local value = self.group(self)

		for _, group in ipairs(groups) do
			control.append(control, group)

			if group == value then
				control.set_value(control, group)
			end
		end
	else
		control.set_enabled(control, false)
	end

	control.thaw(control)

	return 
end

return 
