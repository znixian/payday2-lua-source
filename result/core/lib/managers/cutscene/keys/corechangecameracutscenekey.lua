require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreChangeCameraCutsceneKey = CoreChangeCameraCutsceneKey or class(CoreCutsceneKeyBase)
CoreChangeCameraCutsceneKey.ELEMENT_NAME = "change_camera"
CoreChangeCameraCutsceneKey.NAME = "Camera Change"

CoreChangeCameraCutsceneKey:register_serialized_attribute("camera", nil)

CoreChangeCameraCutsceneKey.__tostring = function (self)
	return "Change camera to \"" .. self.camera(self) .. "\"."
end
CoreChangeCameraCutsceneKey.load = function (self, key_node, loading_class)
	self.super.load(self, key_node, loading_class)

	if self.__camera == nil then
		self.__camera = key_node.parameter(key_node, "ref_obj_name") or "camera"
	end

	return 
end
CoreChangeCameraCutsceneKey.evaluate = function (self, player, fast_forward)
	player.set_camera(player, self.camera(self))

	return 
end
CoreChangeCameraCutsceneKey.is_valid_camera = function (self, camera)
	return self.super.is_valid_unit_name(self, camera) and string.begins(camera, "camera")
end

return 
