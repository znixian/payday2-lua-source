require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreVideoCutsceneKey = CoreVideoCutsceneKey or class(CoreCutsceneKeyBase)
CoreVideoCutsceneKey.ELEMENT_NAME = "video"
CoreVideoCutsceneKey.NAME = "Video Playback"

CoreVideoCutsceneKey:register_serialized_attribute("video", "")
CoreVideoCutsceneKey:register_serialized_attribute("gui_layer", 2, tonumber)
CoreVideoCutsceneKey:register_serialized_attribute("loop", 0, tonumber)
CoreVideoCutsceneKey:register_serialized_attribute("speed", 1, tonumber)

CoreVideoCutsceneKey.__tostring = function (self)
	return string.format("Play video \"%s\".", self.video(self))
end
CoreVideoCutsceneKey.can_evaluate_with_player = function (self, player)
	return true
end
CoreVideoCutsceneKey.play = function (self, player, undo, fast_forward)
	local video_ws = managers.cutscene:video_workspace()
	local was_paused = self._paused
	self._paused = false

	if undo then
		self._stop(self)
	else
		if alive(self._video_object) then
			if was_paused then
				self._play_video(self, video_ws)
			end

			if self.loop(self) < self._video_object:loop_count() then
				self._stop(self)
				managers.cutscene:_cleanup(true)
				managers.overlay_effect:play_effect(tweak_data.player.overlay.cutscene_fade_out)
			end
		elseif self.video(self) ~= "" then
			self._play_video(self, video_ws)
		end

		return true
	end

	return 
end
CoreVideoCutsceneKey.unload = function (self, player)
	self._stop(self)

	return 
end
CoreVideoCutsceneKey.update = function (self, player, time)
	if self.is_in_cutscene_editor then
		self._handle_cutscene_editor_scrubbing(self, time)
	end

	return 
end
CoreVideoCutsceneKey.is_valid_video = function (self, value)
	if self.is_in_cutscene_editor then
		return value ~= nil and value ~= "" and SystemFS:exists(value) and not SystemFS:is_dir(value)
	else
		return value ~= nil and value ~= ""
	end

	return 
end
CoreVideoCutsceneKey.on_attribute_changed = function (self, attribute_name, value, previous_value)
	self._stop(self)

	return 
end
CoreVideoCutsceneKey._handle_cutscene_editor_scrubbing = function (self, time)
	if self._last_evaluated_time then
		if time == self._last_evaluated_time then
			self._stopped_frame_count = (self._stopped_frame_count or 0) + 1

			if 5 < self._stopped_frame_count then
				self._stopped_frame_count = nil

				self.pause(self)
			end
		else
			self._stopped_frame_count = nil

			if alive(self._video_object) and (time < self._last_evaluated_time or 1 < time - self._last_evaluated_time) then
				self._video_object:goto_time(time)
			end

			self.resume(self)
		end
	end

	self._last_evaluated_time = time

	return 
end
CoreVideoCutsceneKey._play_video = function (self, video_ws)
	if not alive(self._video_object) or self.video(self) ~= self._video_played or self.loop(self) ~= self._loop_played or self.speed(self) ~= self._speed_played then
		video_ws.panel(video_ws):clear()
		video_ws.show(video_ws)
		video_ws.panel(video_ws):rect({
			width = video_ws.width(video_ws),
			height = video_ws.height(video_ws),
			color = Color.black
		})

		self._video_object = video_ws.panel(video_ws):video({
			video = self.video(self),
			loop = 0 < self.loop(self),
			speed = self.speed(self),
			layer = self.gui_layer(self)
		})
		self._video_played = self.video(self)
		self._loop_played = self.loop(self)
		self._speed_played = self.speed(self)
		local width, height = get_fit_size(self._video_object:video_width(), self._video_object:video_height(), video_ws.width(video_ws), video_ws.height(video_ws))

		self._video_object:set_size(width, height)
		self._video_object:set_center(video_ws.width(video_ws)/2, video_ws.height(video_ws)/2)
		self._video_object:set_volume_gain(Global.video_sound_volume or 1)
	end

	self._video_object:play()

	return 
end
CoreVideoCutsceneKey._stop = function (self)
	if alive(self._video_object) then
		local video_ws = managers.cutscene:video_workspace()

		video_ws.panel(video_ws):clear()
		video_ws.hide(video_ws)

		self._video_object = nil
	end

	self._last_evaluated_time = nil

	return 
end
CoreVideoCutsceneKey.pause = function (self)
	if not self._paused and alive(self._video_object) then
		self._video_object:pause()

		self._paused = true
	end

	return 
end
CoreVideoCutsceneKey.resume = function (self)
	if self._paused and alive(self._video_object) then
		self._video_object:play()

		self._paused = false
	end

	return 
end

return 
