require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreLocatorConstraintCutsceneKey = CoreLocatorConstraintCutsceneKey or class(CoreCutsceneKeyBase)
CoreLocatorConstraintCutsceneKey.ELEMENT_NAME = "locator_constraint"
CoreLocatorConstraintCutsceneKey.NAME = "Locator Constraint"

CoreLocatorConstraintCutsceneKey:register_serialized_attribute("locator_name", "")
CoreLocatorConstraintCutsceneKey:register_serialized_attribute("parent_unit_name", "")
CoreLocatorConstraintCutsceneKey:register_serialized_attribute("parent_object_name", "")

CoreLocatorConstraintCutsceneKey.control_for_locator_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreLocatorConstraintCutsceneKey.control_for_parent_unit_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreLocatorConstraintCutsceneKey.control_for_parent_object_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreLocatorConstraintCutsceneKey.__tostring = function (self)
	local attach_point_name = "disabled"

	if self.parent_unit_name(self) ~= "" and self.parent_object_name(self) ~= "" then
		attach_point_name = string.format("\"%s/%s\"", self.parent_unit_name(self), self.parent_object_name(self))
	end

	return string.format("Set constaint of locator \"%s\" to %s.", self.locator_name(self), attach_point_name)
end
CoreLocatorConstraintCutsceneKey.can_evaluate_with_player = function (self, player)
	return self._cast ~= nil
end
CoreLocatorConstraintCutsceneKey.evaluate = function (self, player, fast_forward)
	local parent_object = self._unit_object(self, self.parent_unit_name(self), self.parent_object_name(self), true)

	self._constrain_locator_to_object(self, parent_object)

	return 
end
CoreLocatorConstraintCutsceneKey.revert = function (self, player)
	local preceeding_key = self.preceeding_key(self, {
		locator_name = self.locator_name(self)
	})

	if preceeding_key then
		preceeding_key.evaluate(preceeding_key, player, false)
	else
		self._constrain_locator_to_object(self, nil)
	end

	return 
end
CoreLocatorConstraintCutsceneKey.update_gui = function (self, time, delta_time, player)
	local locator_object = self._unit_object(self, self.locator_name(self), "locator", true)

	if locator_object then
		local pen = Draw:pen()

		pen.set(pen, "no_z")
		pen.set(pen, Color(1, 0.5, 0))
		pen.sphere(pen, locator_object.position(locator_object), 1, 5, 1)

		local parent_object = self._unit_object(self, self.parent_unit_name(self), self.parent_object_name(self), true)

		if parent_object then
			pen.set(pen, Color(1, 0, 1))
			pen.line(pen, locator_object.position(locator_object), parent_object.position(parent_object))
			pen.rotation(pen, parent_object.position(parent_object), parent_object.rotation(parent_object), 10)
			pen.set(pen, Color(0, 1, 1))
			pen.sphere(pen, parent_object.position(parent_object), 1, 10, 1)
		end
	end

	return 
end
CoreLocatorConstraintCutsceneKey.is_valid_locator_name = function (self, locator_name)
	return string.begins(locator_name, "locator") and self._unit_type(self, locator_name) == "locator"
end
CoreLocatorConstraintCutsceneKey.is_valid_parent_unit_name = function (self, unit_name)
	return unit_name == nil or unit_name == "" or CoreCutsceneKeyBase.is_valid_unit_name(self, unit_name)
end
CoreLocatorConstraintCutsceneKey.is_valid_parent_object_name = function (self, object_name)
	return object_name == nil or object_name == "" or CoreCutsceneKeyBase.is_valid_object_name(self, object_name, self.parent_unit_name(self))
end
CoreLocatorConstraintCutsceneKey.refresh_control_for_locator_name = function (self, control)
	control.freeze(control)
	control.clear(control)

	local locator_names = table.find_all_values(self._unit_names(self), function (unit_name)
		return self:is_valid_locator_name(unit_name)
	end)

	for _, locator_name in ipairs(locator_names) do
		control.append(control, locator_name)
	end

	control.set_enabled(control, not table.empty(locator_names))
	control.append(control, "")
	control.set_value(control, self.locator_name(self))
	control.thaw(control)

	return 
end
CoreLocatorConstraintCutsceneKey.refresh_control_for_parent_unit_name = function (self, control)
	CoreCutsceneKeyBase.refresh_control_for_unit_name(self, control, self.parent_unit_name(self))
	control.append(control, "")

	if self.parent_unit_name(self) == "" then
		control.set_value(control, "")
	end

	return 
end
CoreLocatorConstraintCutsceneKey.refresh_control_for_parent_object_name = function (self, control)
	CoreCutsceneKeyBase.refresh_control_for_object_name(self, control, self.parent_unit_name(self), self.parent_object_name(self))
	control.append(control, "")

	if self.parent_object_name(self) == "" then
		control.set_value(control, "")
	end

	return 
end
CoreLocatorConstraintCutsceneKey.on_attribute_before_changed = function (self, attribute_name, value, previous_value)
	self.revert(self, nil)

	return 
end
CoreLocatorConstraintCutsceneKey.on_attribute_changed = function (self, attribute_name, value, previous_value)
	self.evaluate(self, nil)

	return 
end
CoreLocatorConstraintCutsceneKey._constrain_locator_to_object = function (self, parent_object)
	local locator_unit = self._unit(self, self.locator_name(self), true)

	if locator_unit == nil then
		return 
	end

	if parent_object then
		locator_unit.set_animations_enabled(locator_unit, false)

		local locator_object = locator_unit.get_object(locator_unit, "locator")
		local position = locator_object.position(locator_object)
		local rotation = locator_object.rotation(locator_object)
		local parent_unit = self._unit(self, self.parent_unit_name(self))

		locator_object.set_local_position(locator_object, Vector3(0, 0, 0))
		locator_object.set_local_rotation(locator_object, Rotation())
		locator_object.link(locator_object, parent_object)
		locator_object.set_position(locator_object, position)
		locator_object.set_rotation(locator_object, rotation)
		parent_unit.link(parent_unit, locator_unit)
	else
		local locator_object = locator_unit.get_object(locator_unit, "locator")

		locator_object.unlink(locator_object)
		locator_unit.unlink(locator_unit)
		locator_unit.set_animations_enabled(locator_unit, true)
	end

	return 
end

return 
