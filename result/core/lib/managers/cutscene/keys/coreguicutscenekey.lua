require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreGuiCutsceneKey = CoreGuiCutsceneKey or class(CoreCutsceneKeyBase)
CoreGuiCutsceneKey.ELEMENT_NAME = "gui"
CoreGuiCutsceneKey.NAME = "Gui"
CoreGuiCutsceneKey.VALID_ACTIONS = {
	"show",
	"hide"
}

CoreGuiCutsceneKey:register_serialized_attribute("action", "show")
CoreGuiCutsceneKey:register_serialized_attribute("name", "")

CoreGuiCutsceneKey.control_for_action = CoreCutsceneKeyBase.standard_combo_box_control
CoreGuiCutsceneKey.control_for_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreGuiCutsceneKey.refresh_control_for_action = CoreCutsceneKeyBase:standard_combo_box_control_refresh("action", CoreGuiCutsceneKey.VALID_ACTIONS)
CoreGuiCutsceneKey.__tostring = function (self)
	return string.capitalize(self.action(self)) .. " gui \"" .. self.name(self) .. "\"."
end
CoreGuiCutsceneKey.prime = function (self, player)
	if self.action(self) == "show" and self.is_valid_name(self, self.name(self)) then
		player.load_gui(player, self.name(self))
	end

	return 
end
CoreGuiCutsceneKey.unload = function (self, player)
	if player then
		self.play(self, player, true)
	end

	return 
end
CoreGuiCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		local preceeding_key = self.preceeding_key(self, {
			name = self.name(self)
		})

		if preceeding_key == nil or preceeding_key.action(preceeding_key) == self.inverse_action(self) then
			self._perform_action(self, self.inverse_action(self), player)
		end
	else
		self._perform_action(self, self.action(self), player)
	end

	return 
end
CoreGuiCutsceneKey.inverse_action = function (self)
	return (self.action(self) == "show" and "hide") or "show"
end
CoreGuiCutsceneKey._perform_action = function (self, action, player)
	player.set_gui_visible(player, self.name(self), action == "show")

	return 
end
CoreGuiCutsceneKey.is_valid_action = function (self, action)
	return table.contains(self.VALID_ACTIONS, action)
end
CoreGuiCutsceneKey.is_valid_name = function (self, name)
	return DB:has("gui", name)
end
CoreGuiCutsceneKey.refresh_control_for_name = function (self, control)
	control.freeze(control)
	control.clear(control)

	local value = self.name(self)

	for _, name in ipairs(managers.database:list_entries_of_type("gui")) do
		control.append(control, name)

		if name == value then
			control.set_value(control, value)
		end
	end

	control.thaw(control)

	return 
end

return 
