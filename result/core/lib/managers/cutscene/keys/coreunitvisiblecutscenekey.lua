require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreUnitVisibleCutsceneKey = CoreUnitVisibleCutsceneKey or class(CoreCutsceneKeyBase)
CoreUnitVisibleCutsceneKey.ELEMENT_NAME = "unit_visible"
CoreUnitVisibleCutsceneKey.NAME = "Unit Visibility"

CoreUnitVisibleCutsceneKey:register_serialized_attribute("unit_name", "")
CoreUnitVisibleCutsceneKey:register_serialized_attribute("visible", true, toboolean)

CoreUnitVisibleCutsceneKey.__tostring = function (self)
	return ((self.visible(self) and "Show") or "Hide") .. " \"" .. self.unit_name(self) .. "\"."
end
CoreUnitVisibleCutsceneKey.unload = function (self)
	if self._cast then
		self.play(self, nil, true)
	end

	return 
end
CoreUnitVisibleCutsceneKey.play = function (self, player, undo, fast_forward)
	assert(type(self.evaluate) == "function", "Cutscene key must define the \"evaluate\" method to use the default CoreCutsceneKeyBase:play method.")

	if undo then
		local preceeding_key = self.preceeding_key(self, {
			unit_name = self.unit_name(self)
		})

		if preceeding_key then
			preceeding_key.evaluate(preceeding_key, player, false)
		else
			self.evaluate(self, player, false, true)
		end
	else
		self.evaluate(self, player, fast_forward)
	end

	return 
end
CoreUnitVisibleCutsceneKey.evaluate = function (self, player, fast_forward, visible)
	assert(self._cast)

	visible = visible or self.visible(self)
	local cast_member = self._cast:unit(self.unit_name(self))

	if cast_member then
		self._cast:set_unit_visible(self.unit_name(self), visible)
	else
		local unit_in_world = self._unit(self, self.unit_name(self), true)

		if unit_in_world then
			set_unit_and_children_visible(unit_in_world, visible)
		end
	end

	return 
end

return 
