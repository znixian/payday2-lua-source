require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreChangeCameraAttributeCutsceneKey = CoreChangeCameraAttributeCutsceneKey or class(CoreCutsceneKeyBase)
CoreChangeCameraAttributeCutsceneKey.ELEMENT_NAME = "camera_attribute"
CoreChangeCameraAttributeCutsceneKey.NAME = "Camera Attribute"

CoreChangeCameraAttributeCutsceneKey:register_serialized_attribute("near_range", nil, tonumber)
CoreChangeCameraAttributeCutsceneKey:register_serialized_attribute("far_range", nil, tonumber)
CoreChangeCameraAttributeCutsceneKey:attribute_affects("near_range", "far_range")
CoreChangeCameraAttributeCutsceneKey:attribute_affects("far_range", "near_range")

CoreChangeCameraAttributeCutsceneKey.__tostring = function (self)
	return "Change camera attributes."
end
CoreChangeCameraAttributeCutsceneKey.populate_from_editor = function (self, cutscene_editor)
	self.super.populate_from_editor(self, cutscene_editor)

	local camera_attributes = cutscene_editor.camera_attributes(cutscene_editor)

	self.set_near_range(self, camera_attributes.near_range)
	self.set_far_range(self, camera_attributes.far_range)

	return 
end
CoreChangeCameraAttributeCutsceneKey.is_valid = function (self)
	return true
end
CoreChangeCameraAttributeCutsceneKey.evaluate = function (self, player, fast_forward)
	local function set_attribute_if_valid(attribute_name)
		local value = self:attribute_value(attribute_name)

		if value and self["is_valid_" .. attribute_name](self, value) then
			player:set_camera_attribute(attribute_name, value)
		end

		return 
	end

	for attribute_name, _ in pairs(self.__serialized_attributes) do
		set_attribute_if_valid(attribute_name)
	end

	return 
end
CoreChangeCameraAttributeCutsceneKey.is_valid_near_range = function (self, value)
	return value == nil or (0 < value and value < (self.far_range(self) or math.huge))
end
CoreChangeCameraAttributeCutsceneKey.is_valid_far_range = function (self, value)
	return value == nil or (self.near_range(self) or 0) < value
end

return 
