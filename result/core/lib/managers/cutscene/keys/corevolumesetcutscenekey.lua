require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreVolumeSetCutsceneKey = CoreVolumeSetCutsceneKey or class(CoreCutsceneKeyBase)
CoreVolumeSetCutsceneKey.ELEMENT_NAME = "volume_set"
CoreVolumeSetCutsceneKey.NAME = "Volume Set"
CoreVolumeSetCutsceneKey.VALID_ACTIONS = {
	"activate",
	"deactivate"
}

CoreVolumeSetCutsceneKey:register_serialized_attribute("action", "activate")
CoreVolumeSetCutsceneKey:register_serialized_attribute("name", "")

CoreVolumeSetCutsceneKey.control_for_action = CoreCutsceneKeyBase.standard_combo_box_control
CoreVolumeSetCutsceneKey.control_for_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreVolumeSetCutsceneKey.refresh_control_for_action = CoreCutsceneKeyBase:standard_combo_box_control_refresh("action", CoreVolumeSetCutsceneKey.VALID_ACTIONS)
CoreVolumeSetCutsceneKey.__tostring = function (self)
	return string.capitalize(self.action(self)) .. " volume set \"" .. self.name(self) .. "\"."
end
CoreVolumeSetCutsceneKey.unload = function (self, player)
	self.play(self, player, true)

	return 
end
CoreVolumeSetCutsceneKey.play = function (self, player, undo, fast_forward)
	if managers.volume == nil then
		return 
	end

	if undo then
		local preceeding_key = self.preceeding_key(self, {
			name = self.name(self)
		})

		if preceeding_key == nil or preceeding_key.action(preceeding_key) == self.inverse_action(self) then
			self._perform_action(self, self.inverse_action(self))
		end
	else
		self._perform_action(self, self.action(self))
	end

	return 
end
CoreVolumeSetCutsceneKey.inverse_action = function (self)
	return (self.action(self) == "activate" and "deactivate") or "activate"
end
CoreVolumeSetCutsceneKey._perform_action = function (self, action)
	if action == "deactivate" and managers.volume:is_active(self.name(self)) then
		managers.volume:deactivate_set(self.name(self))
	elseif action == "activate" and not managers.volume:is_active(self.name(self)) then
		managers.volume:activate_set(self.name(self))
	end

	return 
end
CoreVolumeSetCutsceneKey.is_valid_action = function (self, action)
	return table.contains(self.VALID_ACTIONS, action)
end
CoreVolumeSetCutsceneKey.is_valid_name = function (self, name)
	return (managers.volume and managers.volume:is_valid_volume_set_name(name)) or false
end
CoreVolumeSetCutsceneKey.refresh_control_for_name = function (self, control)
	control.freeze(control)
	control.clear(control)

	if managers.volume then
		local value = self.name(self)

		for _, entry in ipairs(managers.volume:volume_set_names()) do
			control.append(control, entry)

			if entry == value then
				control.set_value(control, value)
			end
		end
	end

	control.thaw(control)

	return 
end

return 
