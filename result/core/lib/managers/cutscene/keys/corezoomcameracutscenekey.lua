require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreZoomCameraCutsceneKey = CoreZoomCameraCutsceneKey or class(CoreCutsceneKeyBase)
CoreZoomCameraCutsceneKey.ELEMENT_NAME = "camera_zoom"
CoreZoomCameraCutsceneKey.NAME = "Camera Zoom"
CoreZoomCameraCutsceneKey.DEFAULT_CAMERA_FOV = 55
CoreZoomCameraCutsceneKey.INTERPOLATION_FUNCTIONS = {
	Linear = function (t, bias)
		return t
	end,
	["J curve"] = function (t, bias)
		local b = (bias - 1)*2
		local a = b - 1

		return a*t^2 + b*t
	end,
	["S curve"] = function (t, bias)
		local a = bias*2 + 1
		local b = a + 1

		return b*t^a - a*t^b
	end
}

CoreZoomCameraCutsceneKey:register_serialized_attribute("start_fov", CoreZoomCameraCutsceneKey.DEFAULT_CAMERA_FOV, tonumber)
CoreZoomCameraCutsceneKey:register_serialized_attribute("end_fov", CoreZoomCameraCutsceneKey.DEFAULT_CAMERA_FOV, tonumber)
CoreZoomCameraCutsceneKey:register_serialized_attribute("transition_time", 0, tonumber)
CoreZoomCameraCutsceneKey:register_serialized_attribute("interpolation", "Linear")
CoreZoomCameraCutsceneKey:register_serialized_attribute("interpolation_bias", 0.5, function (n)
	return (tonumber(n) or 0)/100
end)

CoreZoomCameraCutsceneKey.__tostring = function (self)
	return "Change camera zoom."
end
CoreZoomCameraCutsceneKey.populate_from_editor = function (self, cutscene_editor)
	self.super.populate_from_editor(self, cutscene_editor)

	local camera_attributes = cutscene_editor.camera_attributes(cutscene_editor)

	self.set_start_fov(self, camera_attributes.fov)
	self.set_end_fov(self, camera_attributes.fov)

	return 
end
CoreZoomCameraCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		local preceeding_key = self.preceeding_key(self)

		if preceeding_key then
			player.set_camera_attribute(player, "fov", preceeding_key.end_fov(preceeding_key))
		else
			player.set_camera_attribute(player, "fov", CoreZoomCameraCutsceneKey.DEFAULT_CAMERA_FOV)
		end
	else
		player.set_camera_attribute(player, "fov", self.start_fov(self))
	end

	return 
end
CoreZoomCameraCutsceneKey.update = function (self, player, time)
	local transition_time = self.transition_time(self)

	if time <= transition_time + 0.03333333333333333 then
		local t = (0 < transition_time and math.min(time/transition_time, 1)) or 1
		local alpha = self._calc_interpolation(self, t)
		local fov = self.start_fov(self) + (self.end_fov(self) - self.start_fov(self))*alpha

		player.set_camera_attribute(player, "fov", fov)
	end

	return 
end
CoreZoomCameraCutsceneKey.is_valid_start_fov = function (self, value)
	return value and 0 < value and value < 180
end
CoreZoomCameraCutsceneKey.is_valid_transition_time = function (self, value)
	return value and 0 <= value
end
CoreZoomCameraCutsceneKey.is_valid_interpolation = function (self, value)
	return self.INTERPOLATION_FUNCTIONS[value] ~= nil
end
CoreZoomCameraCutsceneKey.is_valid_interpolation_bias = function (self, value)
	return value and 0 <= value and value <= 1
end
CoreZoomCameraCutsceneKey.is_valid_end_fov = CoreZoomCameraCutsceneKey.is_valid_start_fov
CoreZoomCameraCutsceneKey.control_for_interpolation = CoreCutsceneKeyBase.standard_combo_box_control
CoreZoomCameraCutsceneKey.control_for_interpolation_bias = CoreCutsceneKeyBase.standard_percentage_slider_control
CoreZoomCameraCutsceneKey.refresh_control_for_interpolation = CoreCutsceneKeyBase:standard_combo_box_control_refresh("interpolation", table.map_keys(CoreZoomCameraCutsceneKey.INTERPOLATION_FUNCTIONS, function (a, b)
	return a == "Linear" or a < b
end))
CoreZoomCameraCutsceneKey.refresh_control_for_interpolation_bias = CoreCutsceneKeyBase:standard_percentage_slider_control_refresh("interpolation_bias")
CoreZoomCameraCutsceneKey._calc_interpolation = function (self, t)
	local interpolation_func = self.INTERPOLATION_FUNCTIONS[self.interpolation(self)]

	return interpolation_func(t, math.clamp(self.interpolation_bias(self), 0, 1))
end

return 
