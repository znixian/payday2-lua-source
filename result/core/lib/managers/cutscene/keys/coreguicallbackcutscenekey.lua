require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreGuiCallbackCutsceneKey = CoreGuiCallbackCutsceneKey or class(CoreCutsceneKeyBase)
CoreGuiCallbackCutsceneKey.ELEMENT_NAME = "gui_callback"
CoreGuiCallbackCutsceneKey.NAME = "Gui Callback"

CoreGuiCallbackCutsceneKey:register_serialized_attribute("name", "")
CoreGuiCallbackCutsceneKey:register_serialized_attribute("function_name", "")
CoreGuiCallbackCutsceneKey:register_serialized_attribute("enabled", true, toboolean)

CoreGuiCallbackCutsceneKey.control_for_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreGuiCallbackCutsceneKey.__tostring = function (self)
	return "Call " .. self.function_name(self) .. " in gui \"" .. self.name(self) .. "\"."
end
CoreGuiCallbackCutsceneKey.evaluate = function (self, player, fast_forward)
	if self.enabled(self) then
		player.invoke_callback_in_gui(player, self.name(self), self.function_name(self), player)
	end

	return 
end
CoreGuiCallbackCutsceneKey.is_valid_name = function (self, name)
	return DB:has("gui", name)
end
CoreGuiCallbackCutsceneKey.refresh_control_for_name = function (self, control)
	control.freeze(control)
	control.clear(control)

	local value = self.name(self)

	for _, name in ipairs(managers.database:list_entries_of_type("gui")) do
		control.append(control, name)

		if name == value then
			control.set_value(control, value)
		end
	end

	control.thaw(control)

	return 
end

return 
