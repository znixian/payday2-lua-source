require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreChangeEnvCutsceneKey = CoreChangeEnvCutsceneKey or class(CoreCutsceneKeyBase)
CoreChangeEnvCutsceneKey.ELEMENT_NAME = "change_env"
CoreChangeEnvCutsceneKey.NAME = "Environment Change"

CoreChangeEnvCutsceneKey:register_serialized_attribute("name", "")
CoreChangeEnvCutsceneKey:register_serialized_attribute("transition_time", 0, tonumber)

CoreChangeEnvCutsceneKey.__tostring = function (self)
	return "Change environment to \"" .. self.name(self) .. "\"."
end
CoreChangeEnvCutsceneKey.prime = function (self, player)
	managers.environment:preload_environment(self.name(self), false)

	return 
end
CoreChangeEnvCutsceneKey.unload = function (self, player)
	if self.__previous_environment_name then
		managers.viewport:first_active_viewport():set_environment(self.__previous_environment_name)
	end

	return 
end
CoreChangeEnvCutsceneKey.evaluate = function (self, player, fast_forward)
	self.__previous_environment_name = self.__previous_environment_name or managers.environment:get_current_environment_name()
	local transition_time = self.transition_time(self)

	if transition_time and 0 < transition_time then
		managers.viewport:first_active_viewport():set_environment(self.name(self), transition_time)
	else
		managers.viewport:first_active_viewport():set_environment(self.name(self))
	end

	return 
end
CoreChangeEnvCutsceneKey.can_evaluate_with_player = function (self, player)
	return true
end
CoreChangeEnvCutsceneKey.is_valid_name = function (self, name)
	return Database:has("environment", name)
end
CoreChangeEnvCutsceneKey.is_valid_transition_time = function (self, value)
	return value and 0 <= value
end
CoreChangeEnvCutsceneKey.control_for_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreChangeEnvCutsceneKey.refresh_control_for_name = function (self, control)
	control.freeze(control)
	control.clear(control)

	local value = self.name(self)

	for _, name in ipairs(managers.database:list_entries_of_type("environment")) do
		control.append(control, name)

		if name == value then
			control.set_value(control, value)
		end
	end

	control.thaw(control)

	return 
end

return 
