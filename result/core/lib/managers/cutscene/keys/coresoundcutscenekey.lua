require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreSoundCutsceneKey = CoreSoundCutsceneKey or class(CoreCutsceneKeyBase)
CoreSoundCutsceneKey.ELEMENT_NAME = "sound"
CoreSoundCutsceneKey.NAME = "Sound"

CoreSoundCutsceneKey:register_serialized_attribute("bank", "")
CoreSoundCutsceneKey:register_serialized_attribute("cue", "")
CoreSoundCutsceneKey:register_serialized_attribute("unit_name", "")
CoreSoundCutsceneKey:register_serialized_attribute("object_name", "")
CoreSoundCutsceneKey:register_serialized_attribute("sync_to_video", false, toboolean)
CoreSoundCutsceneKey:attribute_affects("bank", "cue")

CoreSoundCutsceneKey.control_for_unit_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreSoundCutsceneKey.control_for_object_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreSoundCutsceneKey.control_for_bank = CoreCutsceneKeyBase.standard_combo_box_control
CoreSoundCutsceneKey.__tostring = function (self)
	return "Trigger sound \"" .. self.bank(self) .. "/" .. self.cue(self) .. "\" on \"" .. self.unit_name(self) .. "\"."
end
CoreSoundCutsceneKey.prime = function (self, player)
	self.sound(self):prime()

	return 
end
CoreSoundCutsceneKey.skip = function (self, player)
	self.stop(self)

	return 
end
CoreSoundCutsceneKey.can_evaluate_with_player = function (self, player)
	return true
end
CoreSoundCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		self.stop(self)
	elseif not fast_forward then
		if self.unit_name(self) ~= "" and self.object_name(self) ~= "" then
			self.sound(self):set_output(self._unit_object(self, self.unit_name(self), self.object_name(self)))
		end

		self._trigger_sound(self)
	end

	return 
end
CoreSoundCutsceneKey.update = function (self, player, time)
	if self.is_in_cutscene_editor then
		self.handle_cutscene_editor_scrubbing(self, player, time)
	end

	return 
end
CoreSoundCutsceneKey.handle_cutscene_editor_scrubbing = function (self, player, time)
	if self._last_evaluated_time then
		if time == self._last_evaluated_time then
			self._stopped_frame_count = (self._stopped_frame_count or 0) + 1

			if 10 < self._stopped_frame_count then
				self._stopped_frame_count = nil

				self.stop(self)
			end
		else
			self._stopped_frame_count = nil

			if self._sound_abort_func == nil or time < self._last_evaluated_time or 1 < time - self._last_evaluated_time then
				self._trigger_sound(self, time)
			end
		end
	end

	self._last_evaluated_time = time

	return 
end
CoreSoundCutsceneKey.is_valid_unit_name = function (self, unit_name)
	return unit_name == nil or unit_name == "" or CoreCutsceneKeyBase.is_valid_unit_name(self, unit_name)
end
CoreSoundCutsceneKey.is_valid_object_name = function (self, object_name)
	return object_name == nil or object_name == "" or CoreCutsceneKeyBase.is_valid_object_name(self, object_name)
end
CoreSoundCutsceneKey.is_valid_bank = function (self, bank)
	return bank and bank ~= "" and table.contains(Sound:soundbanks(), bank)
end
CoreSoundCutsceneKey.is_valid_cue = function (self, cue)
	return cue and cue ~= "" and self.is_valid_bank(self, self.bank(self)) and Sound:make_bank(self.bank(self), cue) ~= nil
end
CoreSoundCutsceneKey.refresh_control_for_bank = function (self, control)
	control.freeze(control)
	control.clear(control)

	local value = self.bank(self)

	for _, bank_name in ipairs(Sound:soundbanks()) do
		control.append(control, bank_name)

		if bank_name == value then
			control.set_value(control, value)
		end
	end

	control.thaw(control)

	return 
end
CoreSoundCutsceneKey.refresh_control_for_unit_name = function (self, control)
	CoreCutsceneKeyBase.refresh_control_for_unit_name(self, control)
	control.append(control, "")

	if self.unit_name(self) == "" then
		control.set_value(control, "")
	end

	return 
end
CoreSoundCutsceneKey.refresh_control_for_object_name = function (self, control)
	CoreCutsceneKeyBase.refresh_control_for_object_name(self, control)
	control.append(control, "")

	if self.object_name(self) == "" then
		control.set_value(control, "")
	end

	return 
end
CoreSoundCutsceneKey.on_attribute_before_changed = function (self, attribute_name, value, previous_value)
	if attribute_name ~= "sync_to_video" then
		self.stop(self)
	end

	return 
end
CoreSoundCutsceneKey.on_attribute_changed = function (self, attribute_name, value, previous_value)
	if attribute_name == "bank" or attribute_name == "cue" then
		self._sound = nil

		if self.is_valid(self) then
			self.prime(self)
		end
	end

	return 
end
CoreSoundCutsceneKey.sound = function (self)
	if self._sound == nil then
		self._sound = assert(Sound:make_bank(self.bank(self), self.cue(self)), "Sound \"" .. self.bank(self) .. "/" .. self.cue(self) .. "\" not found.")
	end

	return self._sound
end
CoreSoundCutsceneKey.stop = function (self)
	if self._sound_abort_func then
		self._sound_abort_func()

		self._sound_abort_func = nil
	end

	self._last_evaluated_time = nil

	return 
end
CoreSoundCutsceneKey._trigger_sound = function (self, offset)
	self.stop(self)

	local instance = self.sound(self):play((self.sync_to_video(self) and "running_offset") or "offset", offset or 0)

	if alive(instance) then
		self._sound_abort_func = function ()
			if alive(instance) and instance:is_playing() then
				instance:stop()
			end

			return 
		end
	end

	return 
end

return 
