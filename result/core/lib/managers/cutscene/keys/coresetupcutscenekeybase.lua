require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreSetupCutsceneKeyBase = CoreSetupCutsceneKeyBase or class(CoreCutsceneKeyBase)
CoreSetupCutsceneKeyBase.populate_from_editor = function (self, cutscene_editor)
	return 
end
CoreSetupCutsceneKeyBase.frame = function (self)
	return 0
end
CoreSetupCutsceneKeyBase.set_frame = function (self, frame)
	return 
end
CoreSetupCutsceneKeyBase.on_gui_representation_changed = function (self, sender, sequencer_clip)
	return 
end
CoreSetupCutsceneKeyBase.prime = function (self, player)
	error("Cutscene keys deriving from CoreSetupCutsceneKeyBase must define the \"prime\" method.")

	return 
end
CoreSetupCutsceneKeyBase.play = function (self, player, undo, fast_forward)
	return 
end

return 
