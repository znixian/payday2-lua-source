require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")
core:import("CoreColorPickerDialog")

CoreOverlayFXCutsceneKey = CoreOverlayFXCutsceneKey or class(CoreCutsceneKeyBase)
CoreOverlayFXCutsceneKey.ELEMENT_NAME = "overlay_fx"
CoreOverlayFXCutsceneKey.NAME = "Overlay Effect"
CoreOverlayFXCutsceneKey.VALID_BLEND_MODES = {
	"normal",
	"add"
}

CoreOverlayFXCutsceneKey:register_serialized_attribute("blend_mode", CoreOverlayFXCutsceneKey.VALID_BLEND_MODES[1])
CoreOverlayFXCutsceneKey:register_serialized_attribute("color", Color.white, CoreCutsceneKeyBase.string_to_color)
CoreOverlayFXCutsceneKey:register_serialized_attribute("fade_in", 0, tonumber)
CoreOverlayFXCutsceneKey:register_serialized_attribute("sustain", 0, tonumber)
CoreOverlayFXCutsceneKey:register_serialized_attribute("fade_out", 0, tonumber)

CoreOverlayFXCutsceneKey.control_for_blend_mode = CoreCutsceneKeyBase.standard_combo_box_control
CoreOverlayFXCutsceneKey.refresh_control_for_blend_mode = CoreCutsceneKeyBase:standard_combo_box_control_refresh("blend_mode", CoreOverlayFXCutsceneKey.VALID_BLEND_MODES)
CoreOverlayFXCutsceneKey.__tostring = function (self)
	return "Trigger overlay effect."
end
CoreOverlayFXCutsceneKey.preroll = function (self, player)
	if self.fade_in(self) == 0 then
		local effect_data = self._effect_data(self)
		effect_data.fade_in = 0
		effect_data.sustain = nil
		effect_data.fade_out = 0

		managers.cutscene:play_overlay_effect(effect_data)
	end

	return 
end
CoreOverlayFXCutsceneKey.skip = function (self, player)
	local full_intensity_start = self.time(self) + self.fade_in(self)
	local full_intensity_end = full_intensity_start + self.sustain(self)
	local cutscene_end = player.cutscene_duration(player)

	if full_intensity_start <= cutscene_end and cutscene_end <= full_intensity_end then
		local effect_data = self._effect_data(self)
		effect_data.fade_in = 0
		effect_data.sustain = math.max(full_intensity_end - cutscene_end, 0)

		managers.cutscene:play_overlay_effect(effect_data)
	end

	return 
end
CoreOverlayFXCutsceneKey.evaluate = function (self, player, fast_forward)
	local effect_data = table.remap(self.attribute_names(self), function (_, attribute_name)
		return attribute_name, self:attribute_value(attribute_name)
	end)

	managers.cutscene:play_overlay_effect(effect_data)

	return 
end
CoreOverlayFXCutsceneKey.revert = function (self, player)
	self._stop(self)

	return 
end
CoreOverlayFXCutsceneKey.update_gui = function (self, time, delta_time, player)
	if self.__color_picker_dialog then
		self.__color_picker_dialog:update(time, delta_time)
	end

	return 
end
CoreOverlayFXCutsceneKey.is_valid_blend_mode = function (self, value)
	return table.contains(self.VALID_BLEND_MODES, value)
end
CoreOverlayFXCutsceneKey.is_valid_fade_in = function (self, value)
	return 0 <= value
end
CoreOverlayFXCutsceneKey.is_valid_sustain = function (self, value)
	return 0 <= value
end
CoreOverlayFXCutsceneKey.is_valid_fade_out = function (self, value)
	return 0 <= value
end
CoreOverlayFXCutsceneKey.control_for_color = function (self, parent_frame)
	local control = EWS:ColorWell(parent_frame, "")

	control.set_tool_tip(control, "Open Color Picker")
	control.set_background_colour(control, 255, 20, 255)
	control.set_color(control, self.color(self))
	control.connect(control, "EVT_LEFT_UP", callback(self, self, "_on_pick_color"), control)

	return control
end
CoreOverlayFXCutsceneKey._on_pick_color = function (self, sender)
	if self.__color_picker_dialog == nil then
		local cutscene_editor_window = self._top_level_window(self, sender)
		self.__color_picker_dialog = CoreColorPickerDialog.ColorPickerDialog:new(cutscene_editor_window, true, "HORIZONTAL", true)

		self.__color_picker_dialog:connect("EVT_CLOSE_WINDOW", function ()
			self.__color_picker_dialog = nil

			return 
		end)
		self.__color_picker_dialog:connect("EVT_COLOR_CHANGED", function ()
			local color = self.__color_picker_dialog:color()

			sender:set_color(color)
			self:set_color(color)

			return 
		end)
		self.__color_picker_dialog:center(cutscene_editor_window)
		self.__color_picker_dialog:set_color(self.color(self))
		self.__color_picker_dialog:set_visible(true)
	end

	return 
end
CoreOverlayFXCutsceneKey._effect_data = function (self)
	return table.remap(self.attribute_names(self), function (_, attribute_name)
		return attribute_name, self:attribute_value(attribute_name)
	end)
end
CoreOverlayFXCutsceneKey._stop = function (self)
	managers.cutscene:stop_overlay_effect()

	return 
end
CoreOverlayFXCutsceneKey._top_level_window = function (self, window)
	return ((type_name(window) == "EWSFrame" or type_name(window) == "EWSDialog") and window) or self._top_level_window(self, assert(window.parent(window)))
end

return 
