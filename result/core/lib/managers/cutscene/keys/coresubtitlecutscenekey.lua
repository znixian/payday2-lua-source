require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreSubtitleCutsceneKey = CoreSubtitleCutsceneKey or class(CoreCutsceneKeyBase)
CoreSubtitleCutsceneKey.ELEMENT_NAME = "subtitle"
CoreSubtitleCutsceneKey.NAME = "Subtitle"

CoreSubtitleCutsceneKey:register_serialized_attribute("category", "")
CoreSubtitleCutsceneKey:register_serialized_attribute("string_id", "")
CoreSubtitleCutsceneKey:register_serialized_attribute("duration", 3, tonumber)
CoreSubtitleCutsceneKey:register_control("divider")
CoreSubtitleCutsceneKey:register_control("localized_text")
CoreSubtitleCutsceneKey:attribute_affects("category", "string_id")
CoreSubtitleCutsceneKey:attribute_affects("string_id", "localized_text")

CoreSubtitleCutsceneKey.control_for_category = CoreCutsceneKeyBase.standard_combo_box_control
CoreSubtitleCutsceneKey.control_for_string_id = CoreCutsceneKeyBase.standard_combo_box_control
CoreSubtitleCutsceneKey.control_for_divider = CoreCutsceneKeyBase.standard_divider_control
CoreSubtitleCutsceneKey.__tostring = function (self)
	return "Display subtitle \"" .. self.string_id(self) .. "\"."
end
CoreSubtitleCutsceneKey.can_evaluate_with_player = function (self, player)
	return true
end
CoreSubtitleCutsceneKey.unload = function (self, player)
	managers.subtitle:clear_subtitle()

	return 
end
CoreSubtitleCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		managers.subtitle:clear_subtitle()
	elseif not fast_forward then
		managers.subtitle:show_subtitle(self.string_id(self), self.duration(self))
	end

	return 
end
CoreSubtitleCutsceneKey.is_valid_category = function (self, value)
	return value and value ~= ""
end
CoreSubtitleCutsceneKey.is_valid_string_id = function (self, value)
	return value and value ~= ""
end
CoreSubtitleCutsceneKey.is_valid_duration = function (self, value)
	return value and 0 < value
end
CoreSubtitleCutsceneKey.control_for_localized_text = function (self, parent_frame)
	local control = EWS:TextCtrl(parent_frame, "", "", "NO_BORDER,TE_RICH,TE_MULTILINE,TE_READONLY")

	control.set_min_size(control, control.get_min_size(control):with_y(160))
	control.set_background_colour(control, parent_frame.background_colour(parent_frame):unpack())

	return control
end
CoreSubtitleCutsceneKey.refresh_control_for_category = function (self, control)
	control.freeze(control)
	control.clear(control)

	local categories = managers.localization:xml_names()

	if table.empty(categories) then
		control.set_enabled(control, false)
	else
		control.set_enabled(control, true)

		local value = self.category(self)

		for _, category in ipairs(categories) do
			control.append(control, category)

			if category == value then
				control.set_value(control, value)
			end
		end
	end

	control.thaw(control)

	return 
end
CoreSubtitleCutsceneKey.refresh_control_for_string_id = function (self, control)
	control.freeze(control)
	control.clear(control)

	local string_ids = (self.category(self) ~= "" and managers.localization:string_map(self.category(self))) or {}

	if table.empty(string_ids) then
		control.set_enabled(control, false)
	else
		control.set_enabled(control, true)

		local value = self.string_id(self)

		for _, string_id in ipairs(string_ids) do
			control.append(control, string_id)

			if string_id == value then
				control.set_value(control, value)
			end
		end
	end

	control.thaw(control)

	return 
end
CoreSubtitleCutsceneKey.refresh_control_for_localized_text = function (self, control)
	if self.is_valid_category(self, self.category(self)) and self.is_valid_string_id(self, self.string_id(self)) then
		control.set_value(control, managers.localization:text(self.string_id(self)))
	else
		control.set_value(control, "<No String Id>")
	end

	return 
end
CoreSubtitleCutsceneKey.validate_control_for_attribute = function (self, attribute_name)
	if attribute_name ~= "localized_text" then
		return self.super.validate_control_for_attribute(self, attribute_name)
	end

	return true
end

return 
