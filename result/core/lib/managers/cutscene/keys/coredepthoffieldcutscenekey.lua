require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")
require("core/lib/utils/dev/ews/CoreCameraDistancePicker")

CoreDepthOfFieldCutsceneKey = CoreDepthOfFieldCutsceneKey or class(CoreCutsceneKeyBase)
CoreDepthOfFieldCutsceneKey.ELEMENT_NAME = "camera_focus"
CoreDepthOfFieldCutsceneKey.NAME = "Camera Focus"
CoreDepthOfFieldCutsceneKey.DEFAULT_NEAR_DISTANCE = 15
CoreDepthOfFieldCutsceneKey.DEFAULT_FAR_DISTANCE = 10000

CoreDepthOfFieldCutsceneKey:register_serialized_attribute("near_distance", CoreDepthOfFieldCutsceneKey.DEFAULT_NEAR_DISTANCE, tonumber)
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("far_distance", CoreDepthOfFieldCutsceneKey.DEFAULT_FAR_DISTANCE, tonumber)
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("tracked_unit_name", "")
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("tracked_object_name", "")
CoreDepthOfFieldCutsceneKey:register_control("divider1")
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("transition_time", 0, tonumber)
CoreDepthOfFieldCutsceneKey:register_control("divider2")
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("target_near_distance", CoreDepthOfFieldCutsceneKey.DEFAULT_NEAR_DISTANCE, tonumber)
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("target_far_distance", CoreDepthOfFieldCutsceneKey.DEFAULT_FAR_DISTANCE, tonumber)
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("target_tracked_unit_name", "")
CoreDepthOfFieldCutsceneKey:register_serialized_attribute("target_tracked_object_name", "")
CoreDepthOfFieldCutsceneKey:attribute_affects("tracked_unit_name", "near_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("tracked_unit_name", "far_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("tracked_unit_name", "tracked_object_name")
CoreDepthOfFieldCutsceneKey:attribute_affects("tracked_object_name", "near_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("tracked_object_name", "far_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("transition_time", "target_near_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("transition_time", "target_far_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("transition_time", "target_tracked_unit_name")
CoreDepthOfFieldCutsceneKey:attribute_affects("transition_time", "target_tracked_object_name")
CoreDepthOfFieldCutsceneKey:attribute_affects("target_tracked_unit_name", "target_near_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("target_tracked_unit_name", "target_far_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("target_tracked_unit_name", "target_tracked_object_name")
CoreDepthOfFieldCutsceneKey:attribute_affects("target_tracked_object_name", "target_near_distance")
CoreDepthOfFieldCutsceneKey:attribute_affects("target_tracked_object_name", "target_far_distance")

CoreDepthOfFieldCutsceneKey.control_for_tracked_unit_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreDepthOfFieldCutsceneKey.control_for_tracked_object_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreDepthOfFieldCutsceneKey.control_for_divider1 = CoreCutsceneKeyBase.standard_divider_control
CoreDepthOfFieldCutsceneKey.control_for_divider2 = CoreCutsceneKeyBase.standard_divider_control
CoreDepthOfFieldCutsceneKey.control_for_target_tracked_unit_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreDepthOfFieldCutsceneKey.control_for_target_tracked_object_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreDepthOfFieldCutsceneKey.__tostring = function (self)
	return "Change camera focus."
end
CoreDepthOfFieldCutsceneKey.populate_from_editor = function (self, cutscene_editor)
	self.super.populate_from_editor(self, cutscene_editor)

	local camera_attributes = cutscene_editor.camera_attributes(cutscene_editor)
	local near = camera_attributes.near_focus_distance_max or self.DEFAULT_NEAR_DISTANCE
	local far = camera_attributes.far_focus_distance_min or self.DEFAULT_FAR_DISTANCE

	self.set_near_distance(self, near)
	self.set_far_distance(self, far)
	self.set_target_near_distance(self, near)
	self.set_target_far_distance(self, far)

	return 
end
CoreDepthOfFieldCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		local preceeding_key = self.preceeding_key(self)

		if preceeding_key then
			self._set_camera_depth_of_field(self, player, preceeding_key._final_target_near_distance(preceeding_key, player), preceeding_key._final_target_far_distance(preceeding_key, player))
		else
			self._set_camera_depth_of_field(self, player, self.DEFAULT_NEAR_DISTANCE, self.DEFAULT_FAR_DISTANCE)
		end
	elseif self._is_editing_target_values(self) then
		self._set_camera_depth_of_field(self, player, self._final_target_near_distance(self, player), self._final_target_far_distance(self, player))
	else
		self._set_camera_depth_of_field(self, player, self._final_near_distance(self, player), self._final_far_distance(self, player))
	end

	return 
end
CoreDepthOfFieldCutsceneKey.update = function (self, player, time)
	local transition_time = self.transition_time(self)
	local t = (0 < transition_time and math.min(time/transition_time, 1)) or 1
	local alpha = nil

	if self._is_editing_initial_values(self) then
		alpha = 0
	elseif self._is_editing_target_values(self) then
		alpha = 1
	else
		alpha = self._calc_interpolation(self, t)
	end

	local start_near = self._final_near_distance(self, player)
	local end_near = (transition_time == 0 and start_near) or self._final_target_near_distance(self, player)
	local near = start_near + (end_near - start_near)*alpha
	local start_far = self._final_far_distance(self, player)
	local end_far = (transition_time == 0 and start_far) or self._final_target_far_distance(self, player)
	local far = start_far + (end_far - start_far)*alpha

	self._set_camera_depth_of_field(self, player, near, far)

	return 
end
CoreDepthOfFieldCutsceneKey.update_gui = function (self, time, delta_time, player)
	local cutscene_camera_enabled = player and player.is_viewport_enabled(player)

	if self.__near_distance_control then
		self.__near_distance_control:update(time, delta_time)
		self.__near_distance_control:set_pick_button_enabled(cutscene_camera_enabled)
	end

	if self.__far_distance_control then
		self.__far_distance_control:update(time, delta_time)
		self.__far_distance_control:set_pick_button_enabled(cutscene_camera_enabled)
	end

	if self.__target_near_distance_control then
		self.__target_near_distance_control:update(time, delta_time)
		self.__target_near_distance_control:set_pick_button_enabled(cutscene_camera_enabled)
	end

	if self.__target_far_distance_control then
		self.__target_far_distance_control:update(time, delta_time)
		self.__target_far_distance_control:set_pick_button_enabled(cutscene_camera_enabled)
	end

	return 
end
CoreDepthOfFieldCutsceneKey.is_valid_near_distance = function (self, value)
	return value == nil or 0 <= value
end
CoreDepthOfFieldCutsceneKey.is_valid_far_distance = function (self, value)
	return value == nil or 0 <= value
end
CoreDepthOfFieldCutsceneKey.is_valid_tracked_unit_name = function (self, value)
	return value == nil or value == "" or CoreCutsceneKeyBase.is_valid_unit_name(self, value)
end
CoreDepthOfFieldCutsceneKey.is_valid_tracked_object_name = function (self, value)
	return value == nil or value == "" or table.contains(self._unit_object_names(self, self.tracked_unit_name(self)), value) or false
end
CoreDepthOfFieldCutsceneKey.is_valid_transition_time = function (self, value)
	return value and 0 <= value
end
CoreDepthOfFieldCutsceneKey.is_valid_target_near_distance = function (self, value)
	return value == nil or 0 <= value
end
CoreDepthOfFieldCutsceneKey.is_valid_target_far_distance = function (self, value)
	return value == nil or 0 <= value
end
CoreDepthOfFieldCutsceneKey.is_valid_target_tracked_unit_name = function (self, value)
	return value == nil or value == "" or CoreCutsceneKeyBase.is_valid_unit_name(self, value)
end
CoreDepthOfFieldCutsceneKey.is_valid_target_tracked_object_name = function (self, value)
	return value == nil or value == "" or table.contains(self._unit_object_names(self, self.target_tracked_unit_name(self)), value) or false
end
CoreDepthOfFieldCutsceneKey.control_for_near_distance = function (self, parent_frame, callback_func)
	self.__near_distance_control = CoreCameraDistancePicker:new(parent_frame, self.near_distance(self))

	self.__near_distance_control:connect("EVT_COMMAND_TEXT_UPDATED", callback_func)

	return self.__near_distance_control
end
CoreDepthOfFieldCutsceneKey.control_for_far_distance = function (self, parent_frame, callback_func)
	self.__far_distance_control = CoreCameraDistancePicker:new(parent_frame, self.far_distance(self))

	self.__far_distance_control:connect("EVT_COMMAND_TEXT_UPDATED", callback_func)

	return self.__far_distance_control
end
CoreDepthOfFieldCutsceneKey.control_for_target_near_distance = function (self, parent_frame, callback_func)
	self.__target_near_distance_control = CoreCameraDistancePicker:new(parent_frame, self.target_near_distance(self))

	self.__target_near_distance_control:connect("EVT_COMMAND_TEXT_UPDATED", callback_func)

	return self.__target_near_distance_control
end
CoreDepthOfFieldCutsceneKey.control_for_target_far_distance = function (self, parent_frame, callback_func)
	self.__target_far_distance_control = CoreCameraDistancePicker:new(parent_frame, self.target_far_distance(self))

	self.__target_far_distance_control:connect("EVT_COMMAND_TEXT_UPDATED", callback_func)

	return self.__target_far_distance_control
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_tracked_unit_name = function (self, control)
	self.refresh_control_for_unit_name(self, control, self.tracked_unit_name(self))
	control.append(control, "")

	if self.tracked_unit_name(self) == "" then
		control.set_value(control, "")
	end

	return 
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_tracked_object_name = function (self, control)
	self.refresh_control_for_object_name(self, control, self.tracked_unit_name(self), self.tracked_object_name(self))
	control.append(control, "")

	if self.tracked_object_name(self) == "" or not self.is_valid_tracked_object_name(self, self.tracked_object_name(self)) then
		self.set_tracked_object_name(self, "")
		control.set_value(control, "")
	end

	return 
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_target_tracked_unit_name = function (self, control)
	self.refresh_control_for_unit_name(self, control, self.target_tracked_unit_name(self))
	control.append(control, "")

	if self.target_tracked_unit_name(self) == "" then
		control.set_value(control, "")
	end

	control.set_enabled(control, 0 < self.transition_time(self))

	return 
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_target_tracked_object_name = function (self, control)
	self.refresh_control_for_object_name(self, control, self.target_tracked_unit_name(self), self.target_tracked_object_name(self))
	control.append(control, "")

	if self.target_tracked_object_name(self) == "" or not self.is_valid_target_tracked_object_name(self, self.target_tracked_object_name(self)) then
		self.set_target_tracked_object_name(self, "")
		control.set_value(control, "")
	end

	control.set_enabled(control, 0 < self.transition_time(self) and self.target_tracked_unit_name(self) ~= "")

	return 
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_near_distance = function (self, control)
	control.set_value(control, tostring(self.near_distance(self)))
	control.set_enabled(control, not self.is_valid_object_name(self, self.tracked_object_name(self), self.tracked_unit_name(self)))

	return 
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_far_distance = function (self, control)
	control.set_value(control, tostring(self.far_distance(self)))
	control.set_enabled(control, not self.is_valid_object_name(self, self.tracked_object_name(self), self.tracked_unit_name(self)))

	return 
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_target_near_distance = function (self, control)
	control.set_value(control, tostring(self.target_near_distance(self)))
	control.set_enabled(control, 0 < self.transition_time(self) and not self.is_valid_object_name(self, self.target_tracked_object_name(self), self.target_tracked_unit_name(self)))

	return 
end
CoreDepthOfFieldCutsceneKey.refresh_control_for_target_far_distance = function (self, control)
	control.set_value(control, tostring(self.target_far_distance(self)))
	control.set_enabled(control, 0 < self.transition_time(self) and not self.is_valid_object_name(self, self.target_tracked_object_name(self), self.target_tracked_unit_name(self)))

	return 
end
CoreDepthOfFieldCutsceneKey._set_camera_depth_of_field = function (self, player, near, far)
	player.set_camera_depth_of_field(player, near, math.max(far, near))

	return 
end
CoreDepthOfFieldCutsceneKey._is_editing_initial_values = function (self)
	return Application:ews_enabled() and ((self.__near_distance_control and self.__near_distance_control:has_focus()) or (self.__far_distance_control and self.__far_distance_control:has_focus()))
end
CoreDepthOfFieldCutsceneKey._is_editing_target_values = function (self)
	return Application:ews_enabled() and ((self.__target_near_distance_control and self.__target_near_distance_control:has_focus()) or (self.__target_far_distance_control and self.__target_far_distance_control:has_focus()))
end
CoreDepthOfFieldCutsceneKey._final_near_distance = function (self, player)
	local distance = player.distance_from_camera(player, self.tracked_unit_name(self), self.tracked_object_name(self))
	local hyperfocal_distance = self._hyperfocal_distance(self)

	if distance and hyperfocal_distance then
		return (distance < hyperfocal_distance and (hyperfocal_distance*distance)/(hyperfocal_distance + distance)) or hyperfocal_distance/2
	else
		return self.near_distance(self)
	end

	return 
end
CoreDepthOfFieldCutsceneKey._final_far_distance = function (self, player)
	local distance = player.distance_from_camera(player, self.tracked_unit_name(self), self.tracked_object_name(self))
	local hyperfocal_distance = self._hyperfocal_distance(self)

	if distance and hyperfocal_distance then
		return (distance < hyperfocal_distance and (hyperfocal_distance*distance)/(hyperfocal_distance - distance)) or distance
	else
		return self.far_distance(self)
	end

	return 
end
CoreDepthOfFieldCutsceneKey._final_target_near_distance = function (self, player)
	local distance = player.distance_from_camera(player, self.target_tracked_unit_name(self), self.target_tracked_object_name(self))
	local hyperfocal_distance = self._hyperfocal_distance(self)

	if distance and hyperfocal_distance then
		return (distance < hyperfocal_distance and (hyperfocal_distance*distance)/(hyperfocal_distance + distance)) or hyperfocal_distance/2
	else
		return self.target_near_distance(self)
	end

	return 
end
CoreDepthOfFieldCutsceneKey._final_target_far_distance = function (self, player)
	local distance = player.distance_from_camera(player, self.target_tracked_unit_name(self), self.target_tracked_object_name(self))
	local hyperfocal_distance = self._hyperfocal_distance(self)

	if distance and hyperfocal_distance then
		return (distance < hyperfocal_distance and (hyperfocal_distance*distance)/(hyperfocal_distance - distance)) or distance
	else
		return self.target_far_distance(self)
	end

	return 
end
CoreDepthOfFieldCutsceneKey._hyperfocal_distance = function (self)
	return 1433
end
CoreDepthOfFieldCutsceneKey._calc_interpolation = function (self, t)
	return t^2*3 - t^3*2
end

return 
