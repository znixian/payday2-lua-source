require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreChangeShadowCutsceneKey = CoreChangeShadowCutsceneKey or class(CoreCutsceneKeyBase)
CoreChangeShadowCutsceneKey.ELEMENT_NAME = "change_shadow"
CoreChangeShadowCutsceneKey.NAME = "Shadow Change"

CoreChangeShadowCutsceneKey:register_serialized_attribute("name", "")

CoreChangeShadowCutsceneKey.init = function (self, keycollection)
	self.super.init(self, keycollection)

	self._modify_func_map = {}
	self._shadow_interface_id_map = nil
	local list = {
		"slice0",
		"slice1",
		"slice2",
		"slice3",
		"shadow_slice_overlap",
		"shadow_slice_depths"
	}
	local suffix = "post_effect/shadow_processor/shadow_rendering/shadow_modifier/"

	for _, var in ipairs(list) do
		local data_path_key = Idstring(suffix .. var):key()

		local function func()
			return managers.viewport:get_environment_value(self:name(), data_path_key), true
		end

		self._modify_func_map[data_path_key] = func
	end

	return 
end
CoreChangeShadowCutsceneKey.__tostring = function (self)
	return "Change shadow settings to \"" .. self.name(self) .. "\"."
end
CoreChangeShadowCutsceneKey.evaluate = function (self, player, fast_forward)
	local preceeding_key = self.preceeding_key(self, {
		unit_name = self.unit_name and self.unit_name(self),
		object_name = self.object_name and self.object_name(self)
	})

	if preceeding_key then
		preceeding_key.revert(preceeding_key)
	end

	self._shadow_interface_id_map = {}

	for data_path_key, func in pairs(self._modify_func_map) do
		self._shadow_interface_id_map[data_path_key] = managers.viewport:first_active_viewport():create_environment_modifier(data_path_key, true, func)
	end

	return 
end
CoreChangeShadowCutsceneKey.revert = function (self)
	self._reset_interface(self)

	return 
end
CoreChangeShadowCutsceneKey.unload = function (self)
	self._reset_interface(self)

	return 
end
CoreChangeShadowCutsceneKey.can_evaluate_with_player = function (self, player)
	return true
end
CoreChangeShadowCutsceneKey.is_valid_name = function (self, name)
	return name and DB:has("environment", name)
end
CoreChangeShadowCutsceneKey.control_for_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreChangeShadowCutsceneKey.refresh_control_for_name = function (self, control)
	control.freeze(control)
	control.clear(control)

	local value = self.name(self)

	for _, setting_name in ipairs(managers.database:list_entries_of_type("environment")) do
		control.append(control, setting_name)

		if setting_name == value then
			control.set_value(control, setting_name)
		end
	end

	control.thaw(control)

	return 
end
CoreChangeShadowCutsceneKey._reset_interface = function (self)
	if self._shadow_interface_id_map then
		for data_path_key, id in pairs(self._shadow_interface_id_map) do
			managers.viewport:first_active_viewport():destroy_modifier(id)
		end

		self._shadow_interface_id_map = nil
	end

	return 
end

return 
