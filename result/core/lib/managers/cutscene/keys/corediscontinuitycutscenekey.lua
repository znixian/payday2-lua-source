require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreDiscontinuityCutsceneKey = CoreDiscontinuityCutsceneKey or class(CoreCutsceneKeyBase)
CoreDiscontinuityCutsceneKey.ELEMENT_NAME = "discontinuity"
CoreDiscontinuityCutsceneKey.NAME = "Discontinuity"

CoreDiscontinuityCutsceneKey:register_control("description")

CoreDiscontinuityCutsceneKey.refresh_control_for_description = CoreCutsceneKeyBase.VOID
CoreDiscontinuityCutsceneKey.label_for_description = CoreCutsceneKeyBase.VOID
CoreDiscontinuityCutsceneKey.is_valid_description = CoreCutsceneKeyBase.TRUE
CoreDiscontinuityCutsceneKey.__tostring = function (self)
	return "Notifies a discontinuity in linear time."
end
CoreDiscontinuityCutsceneKey.play = function (self, player, undo, fast_forward)
	player._notify_discontinuity(player)

	return 
end
CoreDiscontinuityCutsceneKey.control_for_description = function (self, parent_frame)
	local text = "Discontinuity keys signify a break in linear time. They enable us to dampen physics, etc. during rapid actor movement.\n\nDiscontinuity keys are inserted by the optimizer as the cutscene is exported to the game, but you can also insert them yourself."
	local control = EWS:TextCtrl(parent_frame, text, "", "NO_BORDER,TE_RICH,TE_MULTILINE,TE_READONLY")

	control.set_min_size(control, control.get_min_size(control):with_y(160))
	control.set_background_colour(control, parent_frame.background_colour(parent_frame):unpack())

	return control
end
CoreDiscontinuityCutsceneKey.validate_control_for_attribute = function (self, attribute_name)
	if attribute_name ~= "description" then
		return self.super.validate_control_for_attribute(self, attribute_name)
	end

	return true
end

return 
