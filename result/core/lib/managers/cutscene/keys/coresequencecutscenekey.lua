require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreSequenceCutsceneKey = CoreSequenceCutsceneKey or class(CoreCutsceneKeyBase)
CoreSequenceCutsceneKey.ELEMENT_NAME = "sequence"
CoreSequenceCutsceneKey.NAME = "Sequence"

CoreSequenceCutsceneKey:register_serialized_attribute("unit_name", "")
CoreSequenceCutsceneKey:register_serialized_attribute("name", "")
CoreSequenceCutsceneKey:attribute_affects("unit_name", "name")

CoreSequenceCutsceneKey.control_for_name = CoreCutsceneKeyBase.standard_combo_box_control
CoreSequenceCutsceneKey.__tostring = function (self)
	return "Trigger sequence \"" .. self.name(self) .. "\" on \"" .. self.unit_name(self) .. "\"."
end
CoreSequenceCutsceneKey.evaluate = function (self, player, fast_forward)
	self._unit_extension(self, self.unit_name(self), "damage"):run_sequence_simple(self.name(self))

	return 
end
CoreSequenceCutsceneKey.revert = function (self, player)
	self._run_sequence_if_exists(self, "undo_" .. self.name(self))

	return 
end
CoreSequenceCutsceneKey.skip = function (self, player)
	self._run_sequence_if_exists(self, "skip_" .. self.name(self))

	return 
end
CoreSequenceCutsceneKey.is_valid_unit_name = function (self, unit_name)
	if not self.super.is_valid_unit_name(self, unit_name) then
		return false
	end

	local unit = self._unit(self, unit_name, true)

	return unit ~= nil and managers.sequence:has(unit_name)
end
CoreSequenceCutsceneKey.is_valid_name = function (self, name)
	local unit = self._unit(self, self.unit_name(self), true)

	return unit ~= nil and not string.begins(name, "undo_") and not string.begins(name, "skip_") and managers.sequence:has_sequence_name(self.unit_name(self), name)
end
CoreSequenceCutsceneKey.refresh_control_for_name = function (self, control)
	control.freeze(control)
	control.clear(control)

	local unit = self._unit(self, self.unit_name(self), true)
	local sequence_names = (unit and managers.sequence:get_sequence_list(self.unit_name(self))) or {}

	if not table.empty(sequence_names) then
		control.set_enabled(control, true)

		local value = self.name(self)

		for _, name in ipairs(sequence_names) do
			control.append(control, name)

			if name == value then
				control.set_value(control, value)
			end
		end
	else
		control.set_enabled(control, false)
	end

	control.thaw(control)

	return 
end
CoreSequenceCutsceneKey._run_sequence_if_exists = function (self, sequence_name)
	local unit = self._unit(self, self.unit_name(self))

	if managers.sequence:has_sequence_name(self.unit_name(self), sequence_name) then
		self._unit_extension(self, self.unit_name(self), "damage"):run_sequence_simple(sequence_name)
	end

	return 
end

return 
