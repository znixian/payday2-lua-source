require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")
core:import("CoreEngineAccess")

CoreVisualFXCutsceneKey = CoreVisualFXCutsceneKey or class(CoreCutsceneKeyBase)
CoreVisualFXCutsceneKey.ELEMENT_NAME = "visual_fx"
CoreVisualFXCutsceneKey.NAME = "Visual Effect"

CoreVisualFXCutsceneKey:register_serialized_attribute("unit_name", "")
CoreVisualFXCutsceneKey:register_serialized_attribute("object_name", "")
CoreVisualFXCutsceneKey:register_serialized_attribute("effect", "")
CoreVisualFXCutsceneKey:register_serialized_attribute("duration", nil, tonumber)
CoreVisualFXCutsceneKey:register_serialized_attribute("offset", Vector3(0, 0, 0), CoreCutsceneKeyBase.string_to_vector)
CoreVisualFXCutsceneKey:register_serialized_attribute("rotation", Rotation(), CoreCutsceneKeyBase.string_to_rotation)
CoreVisualFXCutsceneKey:register_serialized_attribute("force_synch", false, toboolean)

CoreVisualFXCutsceneKey.control_for_effect = CoreCutsceneKeyBase.standard_combo_box_control
CoreVisualFXCutsceneKey.__tostring = function (self)
	return "Trigger visual effect \"" .. self.effect(self) .. "\" on \"" .. self.object_name(self) .. " in " .. self.unit_name(self) .. "\"."
end
CoreVisualFXCutsceneKey.can_evaluate_with_player = function (self, player)
	return true
end
CoreVisualFXCutsceneKey.prime = function (self, player)
	return 
end
CoreVisualFXCutsceneKey.unload = function (self, player)
	self.stop(self)

	return 
end
CoreVisualFXCutsceneKey.play = function (self, player, undo, fast_forward)
	if undo then
		self.stop(self)
	elseif not fast_forward then
		self.stop(self)
		self.prime(self, player)

		local effect_manager = World:effect_manager()
		local parent_object = self._unit_object(self, self.unit_name(self), self.object_name(self), true)
		local effect_id = effect_manager.spawn(effect_manager, {
			effect = self.effect(self),
			parent = parent_object,
			position = self.offset(self),
			rotation = self.rotation(self),
			force_synch = self.force_synch(self)
		})
		self._effect_abort_func = function ()
			effect_manager:kill(effect_id)

			return 
		end
	end

	return 
end
CoreVisualFXCutsceneKey.update = function (self, player, time)
	if self.duration(self) and self.duration(self) < time then
		self.stop(self)
	end

	return 
end
CoreVisualFXCutsceneKey.is_valid_unit_name = function (self, value)
	return value == nil or value == "" or CoreCutsceneKeyBase.is_valid_unit_name(self, value)
end
CoreVisualFXCutsceneKey.is_valid_object_name = function (self, value)
	return value == nil or value == "" or table.contains(self._unit_object_names(self, self.unit_name(self)), value) or false
end
CoreVisualFXCutsceneKey.is_valid_effect = function (self, effect)
	return DB:has("effect", effect)
end
CoreVisualFXCutsceneKey.is_valid_duration = function (self, value)
	return value == nil or 0 < value
end
CoreVisualFXCutsceneKey.is_valid_offset = function (self, value)
	return value ~= nil
end
CoreVisualFXCutsceneKey.is_valid_rotation = function (self, value)
	return value ~= nil
end
CoreVisualFXCutsceneKey.refresh_control_for_unit_name = function (self, control)
	self.super.refresh_control_for_unit_name(self, control, self.unit_name(self))
	control.append(control, "")

	if self.unit_name(self) == "" then
		control.set_value(control, "")
	end

	return 
end
CoreVisualFXCutsceneKey.refresh_control_for_object_name = function (self, control)
	self.super.refresh_control_for_object_name(self, control, self.unit_name(self), self.object_name(self))
	control.append(control, "")

	if self.object_name(self) == "" or not self.is_valid_object_name(self, self.object_name(self)) then
		self.set_object_name(self, "")
		control.set_value(control, "")
	end

	control.set_enabled(control, self.unit_name(self) ~= "")

	return 
end
CoreVisualFXCutsceneKey.refresh_control_for_effect = function (self, control)
	control.freeze(control)
	control.clear(control)

	local value = self.effect(self)

	for _, name in ipairs(managers.database:list_entries_of_type("effect")) do
		control.append(control, name)

		if name == value then
			control.set_value(control, value)
		end
	end

	control.thaw(control)

	return 
end
CoreVisualFXCutsceneKey.on_attribute_before_changed = function (self, attribute_name, value, previous_value)
	self.stop(self)

	return 
end
CoreVisualFXCutsceneKey.stop = function (self)
	if self._effect_abort_func then
		self._effect_abort_func()

		self._effect_abort_func = nil
	end

	return 
end

return 
