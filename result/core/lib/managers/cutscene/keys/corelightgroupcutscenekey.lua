require("core/lib/managers/cutscene/keys/CoreCutsceneKeyBase")

CoreLightGroupCutsceneKey = CoreLightGroupCutsceneKey or class(CoreCutsceneKeyBase)
CoreLightGroupCutsceneKey.ELEMENT_NAME = "light_group"
CoreLightGroupCutsceneKey.NAME = "Light Group"

CoreLightGroupCutsceneKey:register_serialized_attribute("group", "")
CoreLightGroupCutsceneKey:register_serialized_attribute("enable", false, toboolean)
CoreLightGroupCutsceneKey:attribute_affects("group", "enable")

CoreLightGroupCutsceneKey.__tostring = function (self)
	return string.format("Change light group, %s stateto %s.", self.group(self), tostring(self.enable(self)))
end
CoreLightGroupCutsceneKey.prime = function (self)
	self._build_group_cache(self)

	return 
end
CoreLightGroupCutsceneKey.evaluate = function (self)
	local group = assert(self._light_groups(self)[self.group(self)], "Could not find group!")

	for _, unit in ipairs(group) do
		self._enable_lights(self, unit, self.enable(self))
	end

	return 
end
CoreLightGroupCutsceneKey.revert = function (self)
	local prev_key = self.preceeding_key(self, {
		group = self.group(self)
	})

	if prev_key then
		prev_key.evaluate(prev_key)
	else
		local group = assert(self._light_groups(self)[self.group(self)], "Could not find group!")

		for _, unit in ipairs(group) do
			self._enable_lights(self, unit, false)
		end
	end

	return 
end
CoreLightGroupCutsceneKey.unload = function (self)
	for group_name, group in pairs(self._light_groups(self)) do
		for _, unit in ipairs(group) do
			self._enable_lights(self, unit, false)
		end
	end

	return 
end
CoreLightGroupCutsceneKey.can_evaluate_with_player = function (self, player)
	return true
end
CoreLightGroupCutsceneKey.is_valid_group = function (self, name)
	for group_name, _ in pairs(self._light_groups(self)) do
		if group_name == name then
			return true
		end
	end

	return 
end
CoreLightGroupCutsceneKey.is_valid_enable = function (self)
	return true
end
CoreLightGroupCutsceneKey.on_attribute_changed = function (self, attribute_name, value, previous_value)
	if attribute_name == "group" or (attribute_name == "enable" and not value) then
		self._eval_prev_group(self)
	end

	return 
end
CoreLightGroupCutsceneKey._light_groups = function (self)
	if not self._light_groups_cache then
		self._build_group_cache(self)
	end

	return self._light_groups_cache
end
CoreLightGroupCutsceneKey._enable_lights = function (self, unit, enabled)
	local lights = unit.get_objects_by_type(unit, "light")

	if #lights == 0 then
		Application:stack_dump_error("[CoreLightGroupCutsceneKey] No lights in unit: " .. unit.name(unit))
	end

	for _, light in ipairs(lights) do
		light.set_enable(light, enabled)
	end

	return 
end
CoreLightGroupCutsceneKey._build_group_cache = function (self)
	self._light_groups_cache = {}

	for key, unit in pairs(managers.cutscene:cutscene_actors_in_world()) do
		local identifier, name, id = string.match(key, "(.+)_(.+)_(.+)")

		if identifier == "lightgroup" then
			if not self._light_groups_cache[name] then
				self._light_groups_cache[name] = {}
			end

			table.insert(self._light_groups_cache[name], unit)
			self._enable_lights(self, unit, false)
		end
	end

	return 
end
CoreLightGroupCutsceneKey._eval_prev_group = function (self)
	local prev_key = self.preceeding_key(self, {
		group = self.group(self)
	})

	if prev_key then
		prev_key.evaluate(prev_key)
	else
		self.evaluate(self)
	end

	return 
end
CoreLightGroupCutsceneKey.refresh_control_for_group = function (self, control)
	control.freeze(control)
	control.clear(control)

	local value = self.group(self)

	for group_name, _ in pairs(self._light_groups(self)) do
		control.append(control, group_name)

		if group_name == value then
			control.set_value(control, group_name)
		end
	end

	control.thaw(control)

	return 
end
CoreLightGroupCutsceneKey.check_box_control = function (self, parent_frame, callback_func)
	local control = EWS:CheckBox(parent_frame, "Enable", "", "")

	control.set_min_size(control, control.get_min_size(control):with_x(0))
	control.connect(control, "EVT_COMMAND_CHECKBOX_CLICKED", callback_func)

	return control
end
CoreLightGroupCutsceneKey.refresh_control_for_enable = function (self, control)
	control.set_value(control, self.enable(self))

	return 
end
CoreLightGroupCutsceneKey.control_for_group = CoreCutsceneKeyBase.standard_combo_box_control
CoreLightGroupCutsceneKey.control_for_enable = CoreLightGroupCutsceneKey.check_box_control

return 
