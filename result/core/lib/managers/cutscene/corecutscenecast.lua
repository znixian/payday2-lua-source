CoreCutsceneCast = CoreCutsceneCast or class()
CoreCutsceneCast.prime = function (self, cutscene)
	assert(cutscene and cutscene.is_valid(cutscene), "Attempting to prime invalid cutscene.")

	local preload = true

	self._actor_units_in_cutscene(self, cutscene)
	self._animation_blob_controller(self, cutscene, preload)

	return 
end
CoreCutsceneCast.unload = function (self)
	slot1 = pairs
	slot2 = self._animation_blob_controllers or {}

	for _, blob_controller in slot1(slot2) do
		if blob_controller ~= false and alive(blob_controller) then
			if blob_controller.is_playing(blob_controller) then
				blob_controller.stop(blob_controller)
			end

			blob_controller.destroy(blob_controller)
		end
	end

	self._animation_blob_controllers = nil
	slot1 = pairs
	slot2 = self._spawned_units or {}

	for _, unit in slot1(slot2) do
		if alive(unit) then
			local unit_type = unit.name(unit)

			World:delete_unit(unit)
		end
	end

	self._spawned_units = nil

	if alive(self.__root_unit) then
		World:delete_unit(self.__root_unit)
	end

	self.__root_unit = nil

	return 
end
CoreCutsceneCast.is_ready = function (self, cutscene)
	local blob_controller = cutscene and self._animation_blob_controller(self, cutscene)

	return blob_controller == nil or blob_controller.ready(blob_controller)
end
CoreCutsceneCast.set_timer = function (self, timer)
	slot2 = pairs
	slot3 = self._spawned_units or {}

	for _, unit in slot2(slot3) do
		if alive(unit) then
			unit.set_timer(unit, timer)
			unit.set_animation_timer(unit, timer)
		end
	end

	return 
end
CoreCutsceneCast.set_cutscene_visible = function (self, cutscene, visible)
	slot3 = pairs
	slot4 = self._spawned_units or {}

	for unit_name, unit in slot3(slot4) do
		if cutscene.has_unit(cutscene, unit_name, true) then
			self._set_unit_and_children_visible(self, unit, visible and self.unit_visible(self, unit_name))
		end
	end

	return 
end
CoreCutsceneCast.set_unit_visible = function (self, unit_name, visible)
	visible = not not visible
	self._hidden_units = self._hidden_units or {}
	local current_visibility = not self._hidden_units[unit_name]

	if visible ~= current_visibility then
		self._hidden_units[unit_name] = not visible or nil
		local unit = self.unit(self, unit_name)

		if unit then
			self._set_unit_and_children_visible(self, unit, visible)
		end
	end

	return 
end
CoreCutsceneCast.unit_visible = function (self, unit_name)
	return (self._hidden_units and self._hidden_units[unit_name]) == nil
end
CoreCutsceneCast.unit = function (self, unit_name)
	return self._spawned_units and self._spawned_units[unit_name]
end
CoreCutsceneCast.actor_unit = function (self, unit_name, cutscene)
	local unit = self.unit(self, unit_name)

	if unit and cutscene.has_unit(cutscene, unit_name) then
		return unit
	else
		return self._actor_units_in_cutscene(self, cutscene)[unit_name]
	end

	return 
end
CoreCutsceneCast.unit_names = function (self)
	return (self._spawned_units and table.map_keys(self._spawned_units)) or {}
end
CoreCutsceneCast.evaluate_cutscene_at_time = function (self, cutscene, time)
	self._last_evaluated_cutscene = self._last_evaluated_cutscene or cutscene

	if cutscene ~= self._last_evaluated_cutscene then
		self._stop_animations_on_actor_units_in_cutscene(self, self._last_evaluated_cutscene)
	end

	local orientation_unit = cutscene.find_spawned_orientation_unit(cutscene)

	if orientation_unit and self._root_unit(self):parent() ~= orientation_unit then
		self._reparent_to_locator_unit(self, orientation_unit, self._root_unit(self))
	end

	local blob_controller = self._animation_blob_controller(self, cutscene)

	if blob_controller then
		if blob_controller.ready(blob_controller) then
			if not blob_controller.is_playing(blob_controller) then
				local actor_units = self._actor_units_in_cutscene(self, cutscene)
				local blend_sets = table.remap(actor_units, function (unit_name)
					return unit_name, cutscene:blend_set_for_unit(unit_name)
				end)

				blob_controller.play(blob_controller, actor_units, blend_sets)
				blob_controller.pause(blob_controller)
			end

			blob_controller.set_time(blob_controller, time)
		end
	else
		for unit_name, unit in pairs(self._actor_units_in_cutscene(self, cutscene)) do
			local unit_animation = cutscene.animation_for_unit(cutscene, unit_name)

			if unit_animation then
				local machine = unit.anim_state_machine(unit)

				if not machine.enabled(machine) then
					machine.set_enabled(machine, true)

					if not self._state_machine_is_playing_raw_animation(self, machine, unit_animation) then
						machine.play_raw(machine, unit_animation)
					end
				end

				local anim_length = cutscene.duration(cutscene)
				local normalized_time = (anim_length ~= 0 or 0) and time/anim_length

				machine.set_parameter(machine, unit_animation, "t", normalized_time)
			end
		end
	end

	self._last_evaluated_cutscene = cutscene

	return 
end
CoreCutsceneCast.evaluate_object_at_time = function (self, cutscene, unit_name, object_name, time)
	assert(cutscene.is_optimized(cutscene), "Currently only supported with optimized cutscenes.")

	local blob_controller = self._animation_blob_controller(self, cutscene)

	if alive(blob_controller) and blob_controller.ready(blob_controller) and blob_controller.is_playing(blob_controller) then
		local bone_name = unit_name .. object_name

		return blob_controller.position(blob_controller, bone_name, time), blob_controller.rotation(blob_controller, bone_name, time)
	else
		return Vector3(0, 0, 0), Rotation()
	end

	return 
end
CoreCutsceneCast.spawn_unit = function (self, unit_name, unit_type)
	if DB:has("unit", unit_type) then
		cat_print("cutscene", string.format("[CoreCutsceneCast] Spawning \"%s\" named \"%s\".", unit_type, unit_name))
		World:effect_manager():set_spawns_enabled(false)

		local unit = safe_spawn_unit(unit_type, Vector3(0, 0, 0), Rotation())

		World:effect_manager():set_spawns_enabled(true)
		unit.set_timer(unit, managers.cutscene:timer())
		unit.set_animation_timer(unit, managers.cutscene:timer())
		self._reparent_to_locator_unit(self, self._root_unit(self), unit)
		self._set_unit_and_children_visible(self, unit, false)
		unit.set_animation_lod(unit, 1, 100000, 10000000, 10000000)

		if unit.cutscene(unit) and unit.cutscene(unit).setup then
			unit.cutscene(unit):setup()
		end

		if unit.anim_state_machine(unit) then
			unit.anim_state_machine(unit):set_enabled(false)
		end

		managers.cutscene:actor_database():append_unit_info(unit)

		self._spawned_units = self._spawned_units or {}
		self._spawned_units[unit_name] = unit

		return unit
	else
		error("Unit type \"" .. tostring(unit_type) .. "\" not found.")
	end

	return 
end
CoreCutsceneCast.delete_unit = function (self, unit_name)
	local unit = self.unit(self, unit_name)

	if unit and alive(unit) then
		local unit_type = unit.name(unit)

		World:delete_unit(unit)
	end

	if self._spawned_units then
		self._spawned_units[unit_name] = nil
	end

	if self._hidden_units then
		self._hidden_units[unit_name] = nil
	end

	return unit ~= nil
end
CoreCutsceneCast.rename_unit = function (self, unit_name, new_unit_name)
	local unit = self.unit(self, unit_name)

	if unit then
		self._spawned_units[unit_name] = nil
		self._spawned_units[new_unit_name] = unit

		if self._hidden_units and self._hidden_units[unit_name] then
			self._hidden_units[unit_name] = nil
			self._hidden_units[new_unit_name] = true
		end

		return true
	end

	return false
end
CoreCutsceneCast._stop_animations_on_actor_units_in_cutscene = function (self, cutscene)
	local blob_controller = self._animation_blob_controller(self, cutscene)

	if blob_controller then
		blob_controller.stop(blob_controller)
	else
		for unit_name, unit in pairs(self._actor_units_in_cutscene(self, cutscene)) do
			local machine = unit.anim_state_machine(unit)

			if machine then
				machine.set_enabled(machine, false)
			end
		end
	end

	return 
end
CoreCutsceneCast._state_machine_is_playing_raw_animation = function (self, machine, animation)
	local state_names = table.collect(machine.config(machine):states(), function (state)
		return state.name(state)
	end)

	return table.contains(state_names, animation) and machine.is_playing(machine, animation)
end
CoreCutsceneCast._reparent_to_locator_unit = function (self, parent, child)
	local parent_locator = assert(parent.get_object(parent, "locator"), "Parent does not have an Object named \"locator\".")

	child.unlink(child)
	parent.link(parent, parent_locator.name(parent_locator), child, child.orientation_object(child):name())

	return 
end
CoreCutsceneCast._set_unit_and_children_visible = function (self, unit, visible, excluded_units)
	unit.set_visible(unit, visible)
	unit.set_enabled(unit, visible)

	if not excluded_units then
		excluded_units = table.remap(self._spawned_units or {}, function (unit_name, unit)
			return unit, true
		end)
	end

	for _, child in ipairs(unit.children(unit)) do
		if not excluded_units[child] then
			self._set_unit_and_children_visible(self, child, visible, excluded_units)
		end
	end

	return 
end
CoreCutsceneCast._animation_blob_controller = function (self, cutscene, preloading)
	if cutscene.animation_blobs(cutscene) == nil then
		return nil
	end

	self._animation_blob_controllers = self._animation_blob_controllers or {}
	local blob_controller = self._animation_blob_controllers[cutscene]

	if blob_controller == nil then
		if not preloading then
			Application:error("The cutscene \"" .. cutscene.name(cutscene) .. "\" was not preloaded, causing a performance spike.")
		end

		blob_controller = CutScene:load(cutscene.animation_blobs(cutscene))
		self._animation_blob_controllers[cutscene] = blob_controller
	end

	return blob_controller
end
CoreCutsceneCast._actor_units_in_cutscene = function (self, cutscene)
	self._spawned_units = self._spawned_units or {}
	local result = {}

	for unit_name, unit_type in pairs(cutscene.controlled_unit_types(cutscene)) do
		local unit = self._spawned_units[unit_name]

		if unit == nil then
			unit = self.spawn_unit(self, unit_name, unit_type)
		elseif not alive(unit) then
			cat_print("debug", string.format("[CoreCutsceneCast] Zombie Unit detected! Actor \"%s\" of unit type \"%s\" in cutscene \"%s\".", unit_name, unit_type, cutscene.name(cutscene)))

			unit = nil
		else
			assert(unit.name(unit) == unit_type, "Named unit type mismatch.")
		end

		result[unit_name] = unit
	end

	return result
end
CoreCutsceneCast._root_unit = function (self)
	if self.__root_unit == nil then
		self.__root_unit = World:spawn_unit(Idstring("core/units/locator/locator"), Vector3(0, 0, 0), Rotation())
	end

	return self.__root_unit
end

return 
