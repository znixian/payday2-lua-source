require("core/lib/managers/cutscene/CoreCutsceneCast")
require("core/lib/managers/cutscene/CoreCutsceneKeyCollection")
core:import("CoreManagerBase")

CoreCutscenePlayer = CoreCutscenePlayer or class()

mixin(CoreCutscenePlayer, get_core_or_local("CutsceneKeyCollection"))

CoreCutscenePlayer.BLACK_BAR_GUI_LAYER = 29
CoreCutscenePlayer.BLACK_BAR_TOP_GUI_NAME = "__CutscenePlayer__black_bar_top"
CoreCutscenePlayer.BLACK_BAR_BOTTOM_GUI_NAME = "__CutscenePlayer__black_bar_bottom"
CoreCutscenePlayer._all_keys_sorted_by_time = function (self)
	return self._owned_cutscene_keys
end
CoreCutscenePlayer.init = function (self, cutscene, optional_shared_viewport, optional_shared_cast)
	self._cutscene = assert(cutscene, "No cutscene supplied.")
	self._viewport = optional_shared_viewport or self._create_viewport(self)
	self._cast = optional_shared_cast or self._create_cast(self)
	self._owned_cutscene_keys = {}
	self._time = 0

	cat_print("cutscene", string.format("[CoreCutscenePlayer] Created CutscenePlayer for \"%s\".", self.cutscene_name(self)))

	if not alive(self._viewport:camera()) then
		self._create_camera(self)
	end

	self._create_future_camera(self)
	self._clear_workspace(self)

	self._resolution_changed_callback_id = managers.viewport:add_resolution_changed_func(callback(self, self, "_configure_viewport"))

	return 
end
CoreCutscenePlayer._create_future_camera = function (self)
	self._future_camera_locator = World:spawn_unit(Idstring("core/units/locator/locator"), Vector3(0, 0, 0), Rotation())
	self._future_camera = World:create_camera()

	self._initialize_camera(self, self._future_camera)
	self._future_camera:link(self._future_camera_locator:get_object("locator"))
	self._future_camera:set_local_rotation(Rotation(-90, 0, 0))
	self._cast:_reparent_to_locator_unit(self._cast:_root_unit(), self._future_camera_locator)

	return 
end
CoreCutscenePlayer.add_keys = function (self, key_collection)
	key_collection = key_collection or self._cutscene

	for _, template_key in ipairs(key_collection._all_keys_sorted_by_time(key_collection)) do
		if self._is_driving_sound_key(self, template_key) then
			self._set_driving_sound_from_key(self, template_key)
		else
			local cutscene_key = template_key.clone(template_key)

			cutscene_key.set_key_collection(cutscene_key, self)
			cutscene_key.set_cast(cutscene_key, self._cast)
			table.insert(self._owned_cutscene_keys, cutscene_key)
		end
	end

	return 
end
CoreCutscenePlayer._is_driving_sound_key = function (self, cutscene_key)
	return cutscene_key.ELEMENT_NAME == CoreSoundCutsceneKey.ELEMENT_NAME and cutscene_key.frame(cutscene_key) == 0 and cutscene_key.sync_to_video(cutscene_key)
end
CoreCutscenePlayer._set_driving_sound_from_key = function (self, cutscene_key)
	cat_print("cutscene", string.format("[CoreCutscenePlayer] Using sound cue \"%s/%s\" to drive the playhead.", cutscene_key.bank(cutscene_key), cutscene_key.cue(cutscene_key)))

	self._driving_sound = assert(Sound:make_bank(cutscene_key.bank(cutscene_key), cutscene_key.cue(cutscene_key)), string.format("Driving sound cue \"%s/%s\" not found.", cutscene_key.bank(cutscene_key), cutscene_key.cue(cutscene_key)))

	return 
end
CoreCutscenePlayer.set_timer = function (self, timer)
	self._cast:set_timer(timer)

	if alive(self._workspace) then
		self._workspace:set_timer(timer)
	end

	local camera_controller = self._camera_controller(self)

	if alive(camera_controller) then
		camera_controller.set_timer(camera_controller, timer)
	end

	return 
end
CoreCutscenePlayer.viewport = function (self)
	return self._viewport
end
CoreCutscenePlayer.cutscene_name = function (self)
	return self._cutscene:name()
end
CoreCutscenePlayer.cutscene_duration = function (self)
	return self._cutscene:duration()
end
CoreCutscenePlayer.camera_attributes = function (self)
	local camera = self._camera(self)
	local attributes = {
		aspect_ratio = camera.aspect_ratio(camera),
		fov = camera.fov(camera),
		near_range = camera.near_range(camera),
		far_range = camera.far_range(camera)
	}

	if self._dof_attributes then
		for key, value in pairs(self._dof_attributes) do
			attributes[key] = value
		end
	end

	return attributes
end
CoreCutscenePlayer.depth_of_field_attributes = function (self)
	return self._dof_attributes
end
CoreCutscenePlayer.prime = function (self)
	if not self._primed then
		self._cast:prime(self._cutscene)

		for _, cutscene_key in dpairs(self._owned_cutscene_keys) do
			if cutscene_key.is_valid(cutscene_key) then
				self.prime_cutscene_key(self, cutscene_key)
			else
				Application:error(string.format("[CoreCutscenePlayer] Invalid cutscene key in \"%s\": %s", self.cutscene_name(self), (cutscene_key.__tostring and cutscene_key.__tostring(cutscene_key)) or tostring(cutscene_key)))
				table.delete(self._owned_cutscene_keys, cutscene_key)
			end
		end

		self._process_camera_cutscene_keys_between(self, -1, 0)

		if self._camera_object(self) ~= nil then
			self._reparent_camera(self)
		end

		if self._driving_sound then
			self._driving_sound:prime()
		end

		self._primed = true
	end

	return 
end
CoreCutscenePlayer.is_primed = function (self)
	return self._primed == true
end
CoreCutscenePlayer._driving_sound_offset = function (self)
	if alive(self._driving_sound_instance) then
		local master_sound_instance = self._master_driving_sound_instance(self, self._driving_sound_instance)

		if master_sound_instance == nil then
			return 0
		elseif not alive(master_sound_instance) or not master_sound_instance.is_playing(master_sound_instance) then
			return nil
		end

		local name, offset = master_sound_instance.offset(master_sound_instance)

		if offset < self._time then
			cat_print("cutscene", string.format("[CoreCutscenePlayer] Bad SoundInstance offset: Got %g, previous was %g.", offset, self._time))

			offset = self._time
		end

		return offset
	end

	return nil
end
CoreCutscenePlayer._master_driving_sound_instance = function (self, sound_instance)
	self._driving_sound_instance_map = self._driving_sound_instance_map or {}
	local master_instance = self._driving_sound_instance_map[sound_instance]

	if master_instance == nil then
		if sound_instance.playing_instances then
			master_instance = sound_instance.playing_instances(sound_instance)[1]
		else
			master_instance = sound_instance
		end

		self._driving_sound_instance_map[sound_instance] = master_instance
	end

	return master_instance
end
CoreCutscenePlayer.is_presentable = function (self)
	return self._cast:is_ready(self._cutscene) and 0 < (self._driving_sound_offset(self) or 1)
end
CoreCutscenePlayer.is_viewport_enabled = function (self)
	return managers.viewport and self._viewport and self._viewport:active()
end
CoreCutscenePlayer.unload = function (self)
	self.stop(self)

	for key in self.keys_between(self, math.huge, -math.huge) do
		key.unload(key, self)
	end

	if self._owned_cast then
		self._owned_cast:unload()
	end

	return 
end
CoreCutscenePlayer.destroy = function (self)
	cat_print("cutscene", string.format("[CoreCutscenePlayer] Destroying CutscenePlayer for \"%s\".", self.cutscene_name(self)))
	self.set_viewport_enabled(self, false)

	slot1 = pairs
	slot2 = self._owned_gui_objects or {}

	for gui_name, _ in slot1(slot2) do
		self.invoke_callback_in_gui(self, gui_name, "on_cutscene_player_destroyed", self)
	end

	self._owned_gui_objects = nil

	self.unload(self)

	if self._listener_id and managers.listener then
		managers.listener:remove_listener(self._listener_id)
	end

	self._listener_id = nil

	if self._resolution_changed_callback_id and managers.viewport then
		managers.viewport:remove_resolution_changed_func(self._resolution_changed_callback_id)
	end

	self._resolution_changed_callback_id = nil

	if self._owned_camera_controller then
		self._viewport:director():release_camera()
		assert(self._viewport:director():camera() == nil)
	end

	if alive(self._workspace) then
		Overlay:newgui():destroy_workspace(self._workspace)
	end

	self._workspace = nil

	if self._owned_viewport and self._owned_viewport:alive() then
		self._owned_viewport:destroy()
	end

	self._owned_viewport = nil

	if alive(self._owned_camera) then
		World:delete_camera(self._owned_camera)
	end

	self._owned_camera = nil

	if alive(self._future_camera) then
		World:delete_camera(self._future_camera)
	end

	self._future_camera = nil

	if alive(self._future_camera_locator) then
		World:delete_unit(self._future_camera_locator)
	end

	self._future_camera_locator = nil

	return 
end
CoreCutscenePlayer.update = function (self, time, delta_time)
	local done = false

	if self.is_playing(self) then
		if alive(self._driving_sound_instance) then
			self._driving_sound_instance:unpause()
		elseif self._driving_sound then
			self._driving_sound_instance = self._driving_sound:play("offset", self._time)
		end

		if self.is_presentable(self) then
			local offset = self._driving_sound_offset(self) or self._time + delta_time
			done = self.seek(self, offset, self._time == offset) == false
		end
	elseif alive(self._driving_sound_instance) then
		self._driving_sound_instance:pause()
	end

	if done then
		self.stop(self)
	elseif self.is_viewport_enabled(self) then
		self.refresh(self)
	end

	return not done
end
CoreCutscenePlayer.refresh = function (self)
	if self._camera_has_cut(self) and managers.environment then
		managers.environment:clear_luminance()
	end

	self._update_future_camera(self)

	return 
end
CoreCutscenePlayer.evaluate_current_frame = function (self)
	self._last_evaluated_time = self._last_evaluated_time or -1

	self._set_visible(self, true)
	self._process_discontinuity_cutscene_keys_between(self, self._last_evaluated_time, self._time)
	self._evaluate_animations(self)
	self._resume_discontinuity(self)
	self._process_camera_cutscene_keys_between(self, self._last_evaluated_time, self._time)
	self._reparent_camera(self)
	self._process_non_camera_cutscene_keys_between(self, self._last_evaluated_time, self._time)

	self._last_evaluated_time = self._time

	return 
end
CoreCutscenePlayer.preroll_cutscene_keys = function (self)
	if 0 < self._time then
		return 
	end

	for _, cutscene_key in ipairs(self._all_keys_sorted_by_time(self)) do
		if 0 < cutscene_key.frame(cutscene_key) then
			break
		end

		if cutscene_key.preroll then
			cutscene_key.preroll(cutscene_key, self)
		end
	end

	return 
end
CoreCutscenePlayer.is_playing = function (self)
	return self._playing or false
end
CoreCutscenePlayer.play = function (self)
	self._playing = true

	for _, cutscene_key in ipairs(self._all_keys_sorted_by_time(self)) do
		if cutscene_key.resume then
			cutscene_key.resume(cutscene_key, self)
		end
	end

	return 
end
CoreCutscenePlayer.pause = function (self)
	self._playing = nil

	for _, cutscene_key in ipairs(self._all_keys_sorted_by_time(self)) do
		if cutscene_key.pause then
			cutscene_key.pause(cutscene_key, self)
		end
	end

	return 
end
CoreCutscenePlayer.stop = function (self)
	self._playing = nil
	self._driving_sound_instance = nil

	self._set_visible(self, false)

	return 
end
CoreCutscenePlayer.skip_to_end = function (self)
	for key in self.keys_between(self, self._time, math.huge) do
		if key.skip then
			self.skip_cutscene_key(self, key)
		end
	end

	if alive(self._driving_sound_instance) then
		self._driving_sound_instance:stop()
	end

	self._driving_sound_instance = nil
	self._time = self.cutscene_duration(self)

	return 
end
CoreCutscenePlayer.seek = function (self, time, skip_evaluation)
	self._time = math.min(math.max(0, time), self.cutscene_duration(self))

	if not skip_evaluation then
		self.evaluate_current_frame(self)
	end

	return self._time == time
end
CoreCutscenePlayer.distance_from_camera = function (self, unit_name, object_name)
	local object = self._actor_object(self, unit_name, object_name)
	local distance = object and self._camera(self):world_to_screen(object.position(object)).z

	return distance
end
CoreCutscenePlayer.set_camera = function (self, camera)
	assert(camera == nil or string.begins(camera, "camera"))

	self._camera_name = camera

	return 
end
CoreCutscenePlayer.set_camera_attribute = function (self, attribute_name, value)
	local camera = self._camera(self)
	local func = assert(camera["set_" .. attribute_name], "Invalid camera attribute.")

	func(camera, value)
	func(self._future_camera, value)

	return 
end
CoreCutscenePlayer.set_camera_depth_of_field = function (self, near, far)
	local range = far - near
	self._dof_attributes = self._dof_attributes or {}
	self._dof_attributes.near_focus_distance_min = math.max(1e-06, near - range*0.33)
	self._dof_attributes.near_focus_distance_max = math.max(1e-06, near)
	self._dof_attributes.far_focus_distance_min = far
	self._dof_attributes.far_focus_distance_max = far + range*0.67

	return 
end
CoreCutscenePlayer.play_camera_shake = function (self, shake_name, amplitude, frequency, offset)
	local shake_id = self._viewport:director():shaker():play(shake_name, amplitude, frequency, offset)

	return function ()
		self._viewport:director():shaker():stop_immediately(shake_id)

		return 
	end
end
CoreCutscenePlayer.has_gui = function (self, gui_name)
	return self._owned_gui_objects ~= nil and self._owned_gui_objects[gui_name] ~= nil
end
CoreCutscenePlayer.load_gui = function (self, gui_name)
	local preload = true

	Overlay:newgui():preload(gui_name)
	self._gui_panel(self, gui_name, preload)
	self.set_gui_visible(self, gui_name, false)

	return 
end
CoreCutscenePlayer.set_gui_visible = function (self, gui_name, visible)
	local panel = self._gui_panel(self, gui_name)

	if not visible == panel.visible(panel) then
		self.invoke_callback_in_gui(self, gui_name, "on_cutscene_player_set_visible", visible, self)
		panel.set_visible(panel, visible)
	end

	return 
end
CoreCutscenePlayer.invoke_callback_in_gui = function (self, gui_name, function_name, ...)
	local gui_object = self._owned_gui_objects and self._owned_gui_objects[gui_name]

	if alive(gui_object) and gui_object.has_script(gui_object) then
		local script = gui_object.script(gui_object)
		local callback_func = rawget(script, function_name)

		if type(callback_func) == "function" then
			callback_func(...)
		end
	end

	return 
end
CoreCutscenePlayer._gui_panel = function (self, gui_name, preloading)
	local panel = self._workspace:panel():child(gui_name)

	if panel == nil then
		if not preloading then
			Application:error("[CoreCutscenePlayer] The gui \"" .. gui_name .. "\" was not preloaded, causing a performance spike.")
		end

		self._owned_gui_objects = self._owned_gui_objects or {}
		local viewport_rect = self._viewport_rect(self)
		panel = self._workspace:panel():panel({
			halign = "grow",
			visible = false,
			valign = "grow",
			name = gui_name,
			x = viewport_rect.px,
			y = viewport_rect.py,
			width = viewport_rect.pw,
			height = viewport_rect.ph
		})
		local gui_object = panel.gui(panel, gui_name)
		self._owned_gui_objects[gui_name] = gui_object
	end

	return panel
end
CoreCutscenePlayer.set_viewport_enabled = function (self, enabled)
	local is_enabled = self._viewport:active()

	if enabled ~= is_enabled then
		if enabled then
			self._viewport:set_active(true)
		else
			self._viewport:set_active(false)
		end

		self._set_listener_enabled(self, enabled)
		self._set_depth_of_field_enabled(self, enabled)
		self._viewport:set_width_mul_enabled(not enabled)

		local black_bars_enabled = self._widescreen and enabled

		self._workspace:panel():child(self.BLACK_BAR_TOP_GUI_NAME):set_visible(black_bars_enabled)
		self._workspace:panel():child(self.BLACK_BAR_BOTTOM_GUI_NAME):set_visible(black_bars_enabled)
	end

	return 
end
CoreCutscenePlayer.set_widescreen = function (self, enabled)
	self._widescreen = enabled or nil

	self._configure_viewport(self)

	return 
end
CoreCutscenePlayer.set_key_handler = function (self, delegate)
	self._key_handler = delegate

	return 
end
CoreCutscenePlayer.prime_cutscene_key = function (self, key, cast)
	local delegate = self._key_handler

	if delegate and delegate.prime_cutscene_key then
		return delegate.prime_cutscene_key(delegate, self, key, cast)
	else
		return key.prime(key, self)
	end

	return 
end
CoreCutscenePlayer.evaluate_cutscene_key = function (self, key, time, last_evaluated_time)
	local delegate = self._key_handler

	if delegate and delegate.evaluate_cutscene_key then
		return delegate.evaluate_cutscene_key(delegate, self, key, time, last_evaluated_time)
	else
		return key.play(key, self, false, false)
	end

	return 
end
CoreCutscenePlayer.revert_cutscene_key = function (self, key, time, last_evaluated_time)
	local delegate = self._key_handler

	if delegate and delegate.revert_cutscene_key then
		return delegate.revert_cutscene_key(delegate, self, key, time, last_evaluated_time)
	else
		return key.play(key, self, true, false)
	end

	return 
end
CoreCutscenePlayer.update_cutscene_key = function (self, key, time, last_evaluated_time)
	local delegate = self._key_handler

	if delegate and delegate.update_cutscene_key then
		return delegate.update_cutscene_key(delegate, self, key, time, last_evaluated_time)
	else
		return key.update(key, self, time)
	end

	return 
end
CoreCutscenePlayer.skip_cutscene_key = function (self, key)
	local delegate = self._key_handler

	if delegate and delegate.skip_cutscene_key then
		return delegate.skip_cutscene_key(delegate, self, key)
	else
		return key.skip(key, self)
	end

	return 
end
CoreCutscenePlayer.time_in_relation_to_cutscene_key = function (self, key)
	local delegate = self._key_handler

	if delegate and delegate.time_in_relation_to_cutscene_key then
		return delegate.time_in_relation_to_cutscene_key(delegate, key)
	else
		return self._time - key.time(key)
	end

	return 
end
CoreCutscenePlayer._set_visible = function (self, visible)
	self._cast:set_cutscene_visible(self._cutscene, visible)

	return 
end
CoreCutscenePlayer._set_listener_enabled = function (self, enabled)
	if enabled then
		if not self._listener_activation_id then
			self._listener_activation_id = managers.listener:activate_set("main", "cutscene")
		end
	else
		if self._listener_activation_id then
			managers.listener:deactivate_set(self._listener_activation_id)
		end

		self._listener_activation_id = nil
	end

	return 
end
CoreCutscenePlayer._set_depth_of_field_enabled = function (self, enabled)
	if enabled then
		managers.environment:disable_dof()
	else
		managers.environment:enable_dof()
		managers.environment:needs_update()
	end

	return 
end
CoreCutscenePlayer._viewport_rect = function (self)
	return (self._widescreen and self._wide_viewport_rect(self)) or self._full_viewport_rect(self)
end
CoreCutscenePlayer._full_viewport_rect = function (self)
	local resolution = RenderSettings.resolution

	return {
		px = 0,
		py = 0,
		h = 1,
		y = 0,
		w = 1,
		x = 0,
		pw = resolution.x,
		ph = resolution.y
	}
end
CoreCutscenePlayer._wide_viewport_rect = function (self)
	local resolution = RenderSettings.resolution
	local resolution_aspect = managers.viewport:aspect_ratio()/1
	local cutscene_aspect = 0.5625
	local viewport_width = math.min(resolution_aspect/cutscene_aspect, 1)
	local viewport_height = resolution_aspect/1*cutscene_aspect*viewport_width
	local viewport_x = (viewport_width - 1)/2
	local viewport_y = (viewport_height - 1)/2
	local rect = {
		x = viewport_x,
		y = viewport_y,
		w = viewport_width,
		h = viewport_height,
		px = rect.x*resolution.x,
		py = rect.y*resolution.y,
		pw = rect.w*resolution.x,
		ph = rect.h*resolution.y
	}

	return rect
end
CoreCutscenePlayer._camera = function (self)
	return self._viewport:camera() or self._create_camera(self)
end
CoreCutscenePlayer._camera_controller = function (self)
	local controller = self._viewport:director():camera()

	return controller or self._create_camera_controller(self)
end
CoreCutscenePlayer._camera_object = function (self)
	return self._camera_name and self._actor_object(self, self._camera_name, "locator")
end
CoreCutscenePlayer._actor_object = function (self, unit_name, object_name)
	local unit = self._cast:actor_unit(unit_name, self._cutscene)

	if unit == nil and managers.cutscene then
		unit = managers.cutscene:cutscene_actors_in_world()[unit_name]
	end

	return unit and unit.get_object(unit, object_name)
end
CoreCutscenePlayer._clear_workspace = function (self)
	if alive(self._workspace) then
		Overlay:newgui():destroy_workspace(self._workspace)
	end

	local resolution = RenderSettings.resolution
	self._workspace = Overlay:newgui():create_scaled_screen_workspace(resolution.x, resolution.y, 0, 0, resolution.x)

	self._workspace:set_timer(managers.cutscene:timer())
	self._workspace:panel():rect({
		visible = self._widescreen,
		layer = self.BLACK_BAR_GUI_LAYER,
		name = self.BLACK_BAR_TOP_GUI_NAME,
		color = Color.black
	})
	self._workspace:panel():rect({
		visible = self._widescreen,
		layer = self.BLACK_BAR_GUI_LAYER,
		name = self.BLACK_BAR_BOTTOM_GUI_NAME,
		color = Color.black
	})
	self._workspace:show()
	self._configure_viewport(self)

	return 
end
CoreCutscenePlayer._create_viewport = function (self)
	assert(self._owned_viewport == nil)

	self._owned_viewport = managers.viewport:new_vp(0, 0, 1, 1, "cutscene", CoreManagerBase.PRIO_CUTSCENE)

	return self._owned_viewport
end
CoreCutscenePlayer._configure_viewport = function (self)
	self.set_camera_attribute(self, "aspect_ratio", managers.viewport:aspect_ratio())

	if alive(self._workspace) then
		local resolution = RenderSettings.resolution

		self._workspace:set_screen(resolution.x, resolution.y, 0, 0, resolution.x)

		local viewport_rect = self._viewport_rect(self)
		local black_bars_enabled = self._widescreen and self.is_viewport_enabled(self)

		self._workspace:panel():child(self.BLACK_BAR_TOP_GUI_NAME):configure({
			y = 0,
			x = 0,
			visible = black_bars_enabled,
			width = viewport_rect.pw,
			height = viewport_rect.py
		})
		self._workspace:panel():child(self.BLACK_BAR_BOTTOM_GUI_NAME):configure({
			x = 0,
			visible = black_bars_enabled,
			y = resolution.y - viewport_rect.py,
			width = viewport_rect.pw,
			height = viewport_rect.py
		})

		if self._owned_gui_objects then
			for gui_name, _ in pairs(table.map_copy(self._owned_gui_objects)) do
				self.invoke_callback_in_gui(self, gui_name, "on_cutscene_player_set_visible", false, self)
				self.invoke_callback_in_gui(self, gui_name, "on_cutscene_player_destroyed", self)

				local panel = self._workspace:panel():child(gui_name)

				panel.clear(panel)
				panel.configure(panel, {
					x = viewport_rect.px,
					y = viewport_rect.py,
					width = viewport_rect.pw,
					height = viewport_rect.ph
				})

				self._owned_gui_objects[gui_name] = panel.gui(panel, gui_name)
			end
		end
	end

	return 
end
CoreCutscenePlayer._create_camera = function (self)
	assert(self._owned_camera == nil)

	self._owned_camera = World:create_camera()

	self._initialize_camera(self, self._owned_camera)
	self._viewport:set_camera(self._owned_camera)

	return self._owned_camera
end
CoreCutscenePlayer._initialize_camera = function (self, camera)
	camera.set_fov(camera, CoreZoomCameraCutsceneKey.DEFAULT_CAMERA_FOV)
	camera.set_near_range(camera, 7.5)
	camera.set_far_range(camera, 50000)
	camera.set_width_multiplier(camera, 1)

	return 
end
CoreCutscenePlayer._create_camera_controller = function (self)
	assert(self._owned_camera_controller == nil)

	self._owned_camera_controller = self._viewport:director():make_camera(self._camera(self), "cutscene_camera")

	self._owned_camera_controller:set_timer(managers.cutscene:timer())
	self._viewport:director():set_camera(self._owned_camera_controller)

	return self._owned_camera_controller
end
CoreCutscenePlayer._create_cast = function (self)
	assert(self._owned_cast == nil)

	self._owned_cast = core_or_local("CutsceneCast")

	return self._owned_cast
end
CoreCutscenePlayer._evaluate_animations = function (self)
	self._cast:evaluate_cutscene_at_time(self._cutscene, self._time)

	return 
end
CoreCutscenePlayer._notify_discontinuity = function (self)
	for unit_name, _ in pairs(self._cutscene:controlled_unit_types()) do
		local unit = self._cast:actor_unit(unit_name, self._cutscene)

		for index = 0, unit.num_bodies(unit) - 1, 1 do
			local body = unit.body(unit, index)

			if body.dynamic(body) and body.enabled(body) then
				body.set_enabled(body, false)

				self._disabled_bodies = self._disabled_bodies or {}

				table.insert(self._disabled_bodies, body)
			end
		end
	end

	return 
end
CoreCutscenePlayer._resume_discontinuity = function (self)
	if self._disabled_bodies then
		for _, body in ipairs(self._disabled_bodies) do
			body.enable_with_no_velocity(body)
		end

		self._disabled_bodies = nil
	end

	return 
end
CoreCutscenePlayer._process_discontinuity_cutscene_keys_between = function (self, start_time, end_time)
	for key in self.keys_between(self, start_time, end_time, CoreDiscontinuityCutsceneKey.ELEMENT_NAME) do
		self.evaluate_cutscene_key(self, key, end_time, start_time)
	end

	return 
end
CoreCutscenePlayer._process_camera_cutscene_keys_between = function (self, start_time, end_time)
	for key in self.keys_between(self, start_time, end_time, CoreChangeCameraCutsceneKey.ELEMENT_NAME) do
		if start_time < end_time then
			self.evaluate_cutscene_key(self, key, end_time, start_time)
		else
			self.revert_cutscene_key(self, key, end_time, start_time)
		end
	end

	for key in self.keys_to_update(self, end_time, CoreChangeCameraCutsceneKey.ELEMENT_NAME) do
		self.update_cutscene_key(self, key, end_time - key.time(key), math.max(0, start_time - key.time(key)))
	end

	return 
end
CoreCutscenePlayer._process_non_camera_cutscene_keys_between = function (self, start_time, end_time)
	for key in self.keys_between(self, start_time, end_time) do
		if key.ELEMENT_NAME ~= CoreChangeCameraCutsceneKey.ELEMENT_NAME and key.ELEMENT_NAME ~= CoreDiscontinuityCutsceneKey.ELEMENT_NAME then
			if start_time < end_time then
				self.evaluate_cutscene_key(self, key, end_time, start_time)
			else
				self.revert_cutscene_key(self, key, end_time, start_time)
			end
		end
	end

	for key in self.keys_to_update(self, end_time) do
		if key.ELEMENT_NAME ~= CoreChangeCameraCutsceneKey.ELEMENT_NAME and key.ELEMENT_NAME ~= CoreDiscontinuityCutsceneKey.ELEMENT_NAME then
			self.update_cutscene_key(self, key, end_time - key.time(key), math.max(0, start_time - key.time(key)))
		end
	end

	return 
end
CoreCutscenePlayer._reparent_camera = function (self)
	local camera_object = self._camera_name and assert(self._camera_object(self), string.format("Camera \"%s\" not found in cutscene \"%s\".", self._camera_name, self.cutscene_name(self)))

	if camera_object ~= nil and camera_object ~= self._camera_controller(self):get_camera() then
		self._camera_controller(self):set_both(camera_object)

		if self._listener_id then
			managers.listener:set_listener(self._listener_id, camera_object)
		else
			self._listener_id = managers.listener:add_listener("cutscene", camera_object)
		end
	end

	return 
end
CoreCutscenePlayer._update_future_camera = function (self)
	if self._cutscene:is_optimized() then
		local position, rotation = self._cast:evaluate_object_at_time(self._cutscene, "camera", "locator", self._time + 0.16666666666666666)

		self._future_camera_locator:warp_to(rotation, position)
		World:effect_manager():add_camera(self._future_camera)
		World:lod_viewers():add_viewer(self._future_camera)
	end

	return 
end
CoreCutscenePlayer._camera_has_cut = function (self)
	self._last_frame_camera_position = self._last_frame_camera_position or Vector3(0, 0, 0)
	self._last_frame_camera_rotation = self._last_frame_camera_rotation or Rotation()
	local camera = self._camera(self)
	local camera_position = camera.position(camera)
	local camera_rotation = camera.rotation(camera)
	local position_difference = self._last_frame_camera_position - camera_position
	local rotation_difference = Rotation:rotation_difference(self._last_frame_camera_rotation, camera_rotation)
	local position_threshold_reached = 50 < position_difference.length(position_difference)
	local rotation_threshold_reached = 5 < rotation_difference.yaw(rotation_difference) or 5 < rotation_difference.pitch(rotation_difference) or 5 < rotation_difference.roll(rotation_difference)
	self._last_frame_camera_position = camera_position
	self._last_frame_camera_rotation = camera_rotation

	return position_threshold_reached or rotation_threshold_reached
end

return 
