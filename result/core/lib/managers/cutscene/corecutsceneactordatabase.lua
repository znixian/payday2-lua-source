CoreCutsceneActorDatabase = CoreCutsceneActorDatabase or class()
CoreCutsceneActorDatabaseUnitTypeInfo = CoreCutsceneActorDatabaseUnitTypeInfo or class()
CoreCutsceneActorDatabase.unit_type_info = function (self, unit_type)
	return unit_type and self._registered_unit_types and self._registered_unit_types[unit_type]
end
CoreCutsceneActorDatabase.append_unit_info = function (self, unit)
	self._registered_unit_types = self._registered_unit_types or {}
	self._registered_unit_types[unit.name(unit)] = self._registered_unit_types[unit.name(unit)] or core_or_local("CutsceneActorDatabaseUnitTypeInfo", unit.name(unit))

	self._registered_unit_types[unit.name(unit)]:_append_unit_info(unit)

	return 
end
CoreCutsceneActorDatabaseUnitTypeInfo.init = function (self, unit_type)
	self._unit_type = unit_type

	return 
end
CoreCutsceneActorDatabaseUnitTypeInfo.unit_type = function (self)
	return self._unit_type
end
CoreCutsceneActorDatabaseUnitTypeInfo.object_names = function (self)
	return self._object_names or {}
end
CoreCutsceneActorDatabaseUnitTypeInfo.initial_object_visibility = function (self, object_name)
	return (self._object_visibilities and self._object_visibilities[object_name]) or false
end
CoreCutsceneActorDatabaseUnitTypeInfo.extensions = function (self)
	return self._extensions or {}
end
CoreCutsceneActorDatabaseUnitTypeInfo.animation_groups = function (self)
	return self._animation_groups or {}
end
CoreCutsceneActorDatabaseUnitTypeInfo._append_unit_info = function (self, unit)
	assert(self.unit_type(self) == unit.name(unit))

	if self._object_names == nil then
		self._object_names = table.collect(unit.get_objects(unit, "*"), function (object)
			return object.name(object)
		end)

		table.sort(self._object_names, string.case_insensitive_compare)
		freeze(self._object_names)
	end

	if self._object_visibilities == nil then
		self._object_visibilities = table.remap(unit.get_objects(unit, "*"), function (_, object)
			return object.name(object), (object.visibility and object.visibility(object)) or nil
		end)
	end

	if self._extensions == nil then
		self._extensions = {}

		for _, extension_name in ipairs(unit.extensions(unit)) do
			local extension = unit[extension_name] and unit[extension_name](unit)

			if extension then
				local methods = {}

				for key, value in pairs(getmetatable(extension)) do
					if type(value) == "function" and not string.begins(key, "_") and key ~= "new" and key ~= "init" then
						methods[key] = self._argument_names_for_function(self, value)
					end
				end

				self._extensions[extension_name] = methods
			end
		end

		freeze(self._extensions)
	end

	if self._animation_groups == nil then
		self._animation_groups = unit.anim_groups(unit)
	end

	freeze(self)

	return 
end
CoreCutsceneActorDatabaseUnitTypeInfo._argument_names_for_function = function (self, func)
	if not Application:ews_enabled() then
		return {}
	end

	local argument_names = {}
	local info = debug.getinfo(func)
	local source_path = managers.database:base_path() .. info.source
	local file = SystemFS:open(source_path, "r")
	local func_definition = self._file_line(self, file, info.linedefined)

	file.close(file)

	local arg_list = string.match(string.match(func_definition, "%b()") or "", "%((.+)%)")

	return (arg_list and string.split(arg_list, "[,%s]")) or {}
end
CoreCutsceneActorDatabaseUnitTypeInfo._file_line = function (self, file, line)
	while not file.at_end(file) do
		local text = file.gets(file)
		line = line - 1

		if line == 0 then
			return text
		end
	end

	return 
end

return 
