require("core/lib/managers/cutscene/CoreCutscene")
require("core/lib/managers/cutscene/CoreCutsceneKeys")
require("core/lib/managers/cutscene/CoreCutsceneCast")
require("core/lib/managers/cutscene/CoreCutscenePlayer")
require("core/lib/managers/cutscene/CoreCutsceneActorDatabase")

CoreCutsceneManager = CoreCutsceneManager or mixin(class(), BasicEventHandling)
CoreCutsceneManager.cutscene_actor_unit_type = function (self, original_unit_type)
	return (Global.__CutsceneManager__replaced_actor_unit_types and Global.__CutsceneManager__replaced_actor_unit_types[original_unit_type]) or original_unit_type
end
CoreCutsceneManager.replace_cutscene_actor_unit_type = function (self, original_unit_type, replacement_unit_type)
	assert(DB:has("unit", original_unit_type), string.format("Unrecognized Unit \"%s\".", original_unit_type.t(original_unit_type)))
	assert(replacement_unit_type == nil or DB:has("unit", replacement_unit_type), string.format("Unrecognized Unit \"%s\".", replacement_unit_type or "":t()))

	Global.__CutsceneManager__replaced_actor_unit_types = Global.__CutsceneManager__replaced_actor_unit_types or {}

	if replacement_unit_type then
		cat_print("cutscene", string.format("[CoreCutsceneManager] Replacing all \"%s\" actors with instances of \"%s\".", original_unit_type, replacement_unit_type))
	elseif Global.__CutsceneManager__replaced_actor_unit_types[original_unit_type] then
		cat_print("cutscene", string.format("[CoreCutsceneManager] Undoing replacement of all \"%s\" actors.", original_unit_type))
	end

	Global.__CutsceneManager__replaced_actor_unit_types[original_unit_type] = replacement_unit_type

	return 
end
CoreCutsceneManager.init = function (self)
	managers.listener:add_set("cutscene", {
		"cutscene"
	})

	self._timer = TimerManager:game_animation()
	self._actor_database = core_or_local("CutsceneActorDatabase")
	self._input_controller = managers.controller:create_controller()
	self._gui_workspace = self._create_gui_workspace(self)
	self._video_workspace = self._create_video_workspace(self)

	return 
end
CoreCutsceneManager.post_init = function (self)
	self._prime_cutscenes_in_world(self)

	return 
end
CoreCutsceneManager.destroy = function (self)
	if self._player then
		self._player:destroy()

		self._player = nil
	end

	self._destroy_units_with_cutscene_data_extension(self)

	if alive(self._video_workspace) then
		Overlay:newgui():destroy_workspace(self._video_workspace)
	end

	self._video_workspace = nil

	if alive(self._gui_workspace) then
		Overlay:newgui():destroy_workspace(self._gui_workspace)
	end

	self._gui_workspace = nil

	if self._input_controller then
		self._input_controller:destroy()

		self._input_controller = nil
	end

	managers.listener:remove_set("cutscene")

	return 
end
CoreCutsceneManager.timer = function (self)
	return self._timer
end
CoreCutsceneManager.set_timer = function (self, timer)
	self._timer = assert(timer, "Must supply a timer.")

	if self._player then
		self._player:set_timer(timer)
	end

	if self.gui_workspace(self) then
		self.gui_workspace(self):set_timer(self._timer)
	end

	if self.video_workspace(self) then
		self.video_workspace(self):set_timer(self._timer)
	end

	return 
end
CoreCutsceneManager.register_unit_with_cutscene_data_extension = function (self, unit)
	self._units_with_cutscene_data_extension = self._units_with_cutscene_data_extension or {}

	table.insert(self._units_with_cutscene_data_extension, unit)

	return 
end
CoreCutsceneManager.unregister_unit_with_cutscene_data_extension = function (self, unit)
	if self._units_with_cutscene_data_extension then
		table.delete(self._units_with_cutscene_data_extension, unit)

		if #self._units_with_cutscene_data_extension == 0 then
			self._units_with_cutscene_data_extension = nil
		end
	end

	return 
end
CoreCutsceneManager._prime_cutscenes_in_world = function (self)
	slot1 = ipairs
	slot2 = self._units_with_cutscene_data_extension or {}

	for _, unit in slot1(slot2) do
		if alive(unit) then
			local player = unit.cutscene_data(unit):cutscene_player()

			cat_print("cutscene", string.format("[CoreCutsceneManager] Priming in-world cutscene \"%s\".", player.cutscene_name(player)))
			player.prime(player)
		end
	end

	return 
end
CoreCutsceneManager._destroy_units_with_cutscene_data_extension = function (self)
	local units_to_destroy = table.list_copy(self._units_with_cutscene_data_extension or {})

	for _, unit in ipairs(units_to_destroy) do
		self.unregister_unit_with_cutscene_data_extension(self, unit)

		if alive(unit) then
			cat_print("cutscene", string.format("[CoreCutsceneManager] Destroying Unit with CutsceneData extension \"%s\".", unit.name(unit)))
			World:delete_unit(unit)
		end
	end

	assert(self._units_with_cutscene_data_extension == nil, "Not all units with the CutsceneData extension were destroyed.")

	return 
end
CoreCutsceneManager.register_cutscene_actor = function (self, unit)
	assert(alive(unit), "Zombie unit registered as cutscene actor.")

	local actor_name = unit.unit_data and unit.unit_data(unit).cutscene_actor

	assert(actor_name and actor_name ~= "", "Unnamed unit registered as cutscene actor.")

	local existing_unit = self.cutscene_actors_in_world(self)[actor_name]

	if existing_unit ~= nil then
		return existing_unit == unit
	end

	self.actor_database(self):append_unit_info(unit)

	self._cutscene_actors = self._cutscene_actors or {}
	self._cutscene_actors[actor_name] = unit

	return true
end
CoreCutsceneManager.unregister_cutscene_actor = function (self, unit)
	assert(alive(unit), "Zombie unit unregistered as cutscene actor.")

	local actor_name = unit.unit_data and unit.unit_data(unit).cutscene_actor

	assert(actor_name and actor_name ~= "", "Unnamed unit unregistered as cutscene actor.")

	local existing_unit = self.cutscene_actors_in_world(self)[actor_name]

	if existing_unit == nil then
		return false
	end

	self._cutscene_actors[actor_name] = nil

	if table.empty(self._cutscene_actors) then
		self._cutscene_actors = nil
	end

	return true
end
CoreCutsceneManager.cutscene_actors_in_world = function (self)
	if self._cutscene_actors == nil then
		return {}
	end

	local dead_units = table.collect(self._cutscene_actors, function (unit)
		return not alive(unit) or nil
	end)

	for dead_unit_name, _ in pairs(dead_units) do
		self._cutscene_actors[dead_unit_name] = nil
	end

	return self._cutscene_actors
end
CoreCutsceneManager.actor_database = function (self)
	return self._actor_database
end
CoreCutsceneManager.debug_next_exec = function (self, scene_name)
	self.delay_cutscene_debug(self)

	Global.debug_cutscene = scene_name

	return 
end
CoreCutsceneManager.delay_cutscene_debug = function (self)
	self._delay_cutscene_debug = true

	return 
end
CoreCutsceneManager.start_delayed_cutscene = function (self)
	local debug_scene = Global.debug_cutscene or arg_value("-debugcs")

	if not self._delay_cutscene_debug and debug_scene then
		self._stop_playback = nil
		self._disable_events = true

		self._cleanup(self)

		self._disable_events = nil

		managers.cutscene:play_cutscene(debug_scene)

		self._manager_locked = true
		Global.debug_cutscene = nil

		self.connect(self, "EVT_PLAYBACK_FINISHED", function ()
			managers.cutscene._manager_locked = nil

			return 
		end)
	end

	return 
end
CoreCutsceneManager.update = function (self)
	return 

	local time = self._timer:time()
	local delta_time = self._timer:delta_time()

	if self._stop_playback then
		self._cleanup(self)

		self._stop_playback = nil
		self._disable_events = nil
	else
		if self._player then
			if not self._player:is_primed() then
				self._player:prime()
			end

			if self._start_playback and not self.is_paused(self) then
				self._player:play()

				self._start_playback = nil
			end

			if self._player:is_presentable() and not self._player:is_viewport_enabled() then
				self._player:set_viewport_enabled(true)
				self.set_gui_visible(self, true)
				self._on_playback_started(self, self._player:cutscene_name())
				self._send_event(self, "EVT_PLAYBACK_STARTED", self._player:cutscene_name())
			end
		end

		local just_finished_playing_in_game_cutscene = self._player and self._player:update(time, delta_time) == false and self._video(self) == nil
		local just_finished_playing_video = self._video(self) ~= nil and 0 < self._video(self):loop_count()

		if just_finished_playing_in_game_cutscene or just_finished_playing_video then
			self._cleanup(self)
		end
	end

	return 
end
CoreCutsceneManager.paused_update = function (self)
	self.update(self)

	return 
end
CoreCutsceneManager.play_overlay_effect = function (self, effect_data)
	self.stop_overlay_effect(self)

	effect_data.play_paused = true
	effect_data.timer = self.timer(self)
	self.__overlay_effect_id = managers.overlay_effect:play_effect(effect_data)

	return 
end
CoreCutsceneManager.stop_overlay_effect = function (self, fade_out)
	if self.__overlay_effect_id then
		assert(fade_out == nil or type(fade_out) == "boolean")
		managers.overlay_effect[(fade_out and "fade_out_effect") or "stop_effect"](managers.overlay_effect, self.__overlay_effect_id)

		self.__overlay_effect_id = nil
	end

	return 
end
CoreCutsceneManager._create_gui_workspace = function (self)
	return nil
end
CoreCutsceneManager._create_video_workspace = function (self)
	local res = RenderSettings.resolution
	local workspace = Overlay:newgui():create_scaled_screen_workspace(res.x, res.x/managers.viewport:aspect_ratio(), 0, 0, res.x, res.y)

	workspace.set_timer(workspace, self._timer)

	return workspace
end
CoreCutsceneManager.input_controller = function (self)
	return self._input_controller
end
CoreCutsceneManager.gui_workspace = function (self)
	return self._gui_workspace
end
CoreCutsceneManager.video_workspace = function (self)
	return self._video_workspace
end
CoreCutsceneManager._video = function (self)
	local panel = self.video_workspace(self):panel()

	return (1 < panel.num_children(panel) and panel.child(panel, 1)) or nil
end
CoreCutsceneManager.set_gui_visible = function (self, visible)
	local gui_workspace = self.gui_workspace(self) or responder(visible)

	if gui_workspace.visible(gui_workspace) ~= visible then
		gui_workspace[(visible and "show") or "hide"](gui_workspace)
	end

	local input_controller = self.input_controller(self)

	input_controller[(visible and "enable") or "disable"](input_controller)

	return 
end
CoreCutsceneManager.get_cutscene_names = function (self)
	return managers.database:list_entries_of_type("cutscene")
end
CoreCutsceneManager.prime = function (self, name, time)
	time = time or 0
	local cutscene = self.get_cutscene(self, name)

	if self._player == nil then
		self._player = self._player_for_cutscene(self, cutscene)
	elseif self._player:cutscene_name() ~= name then
		self._cleanup(self, true)

		self._player = self._player_for_cutscene(self, cutscene)
	end

	self._player:seek(time, true)

	return 
end
CoreCutsceneManager._player_for_cutscene = function (self, cutscene)
	local orientation_unit = cutscene.find_spawned_orientation_unit(cutscene)
	local cutscene_data = orientation_unit and orientation_unit.cutscene_data and orientation_unit.cutscene_data(orientation_unit)

	if cutscene_data then
		return cutscene_data.cutscene_player(cutscene_data)
	else
		local player = core_or_local("CutscenePlayer", cutscene)

		player.add_keys(player)

		return player
	end

	return 
end
CoreCutsceneManager.play_cutscene = function (self, name)
	if not self._manager_locked then
		self.prime(self, name)
		self.play(self)
	end

	return 
end
CoreCutsceneManager.play = function (self)
	if self._player ~= nil and not self._player:is_playing() then
		if not self._is_overriding_user_music then
			managers.music:override_user_music(true)

			self._is_overriding_user_music = true
		end

		self._player:preroll_cutscene_keys()

		self._start_playback = true
	end

	return 
end
CoreCutsceneManager.stop = function (self, disable_events)
	self._start_playback = nil
	self._stop_playback = true
	self._paused = nil
	self._disable_events = disable_events

	return 
end
CoreCutsceneManager.skip = function (self)
	if self._player then
		self._player:skip_to_end()
	end

	self.stop(self, false)

	return 
end
CoreCutsceneManager._cleanup = function (self, called_from_prime)
	if self._is_overriding_user_music then
		managers.music:override_user_music(false)

		self._is_overriding_user_music = nil
	end

	local playing_cutscene_name = nil

	if self._player then
		if called_from_prime then
			self._player:skip_to_end()
		end

		playing_cutscene_name = self._player:cutscene_name()

		self._player:destroy()

		self._player = nil
	end

	if not called_from_prime then
		self.set_gui_visible(self, false)
	end

	if self._video(self) then
		self._video(self):pause()
		self.video_workspace(self):panel():clear()
	end

	if playing_cutscene_name and not self._disable_events then
		self._send_event(self, "EVT_PLAYBACK_FINISHED", playing_cutscene_name)
		self._on_playback_finished(self, playing_cutscene_name)
	end

	return 
end
CoreCutsceneManager.pause = function (self)
	self._paused = true

	if self.is_playing(self) then
		if self._is_overriding_user_music then
			managers.music:override_user_music(false)

			self._is_overriding_user_music = nil
		end

		self._player:pause()
		self._send_event(self, "EVT_PLAYBACK_PAUSED", self._player:cutscene_name())
	end

	return 
end
CoreCutsceneManager.resume = function (self)
	if self.is_paused(self) then
		self._paused = nil

		self.play(self)
	end

	return 
end
CoreCutsceneManager.evaluate_at_time = function (self, name, time)
	self.prime(self, name, time)
	self._player:evaluate_current_frame()

	return 
end
CoreCutsceneManager.evaluate_at_frame = function (self, name, frame)
	self.evaluate_at_time(self, name, frame/self.get_cutscene(self, name):frames_per_second())

	return 
end
CoreCutsceneManager.is_playing_cutscene = function (self, name)
	return self.is_playing(self) and self._player:cutscene_name() == name
end
CoreCutsceneManager.is_playing = function (self)
	return (self._player ~= nil and self._player:is_playing()) or self._video(self) ~= nil
end
CoreCutsceneManager.is_paused = function (self)
	return self._paused ~= nil
end
CoreCutsceneManager.add_playing_changed_callback = function (self, object, func_or_name)
	local func = (type(func_or_name) == "string" and callback(object, object, func_or_name)) or func_or_name

	self.connect(self, "EVT_PLAYBACK_STARTED", func, true)
	self.connect(self, "EVT_PLAYBACK_FINISHED", func, false)

	return 
end
CoreCutsceneManager.get_cutscene = function (self, name)
	local cutscene = self._cutscenes and self._cutscenes[name]

	if cutscene == nil then
		if not DB:has("cutscene", name) then
			error("Cutscene \"" .. tostring(name) .. "\" does not exist.")
		end

		cutscene = core_or_local("Cutscene", DB:load_node("cutscene", name), self)

		if not Application:ews_enabled() then
			assert(cutscene.is_optimized(cutscene), "Cutscene \"" .. tostring(name) .. "\" is production-only (un-optimized).")
		end

		self._cutscenes = self._cutscenes or {}
		self._cutscenes[name] = cutscene
	end

	return cutscene
end
CoreCutsceneManager._on_playback_started = function (self, cutscene_name)
	return 
end
CoreCutsceneManager._on_playback_finished = function (self, cutscene_name)
	return 
end
CoreCutsceneManager._debug_persistent_keys_per_cutscene = function (self)
	local persistent_keys_per_cutscene = {}

	for _, name in ipairs(self.get_cutscene_names(self)) do
		local cutscene = self.get_cutscene(self, name)
		local persistent_keys = cutscene._debug_persistent_keys(cutscene)
		persistent_keys_per_cutscene[name] = table.map_keys(persistent_keys)
	end

	return persistent_keys_per_cutscene
end
CoreCutsceneManager._debug_persistent_keys_report = function (self)
	local output_string = "Persistent Cutscene Keys Report\n"
	output_string = output_string .. "-------------------------------\n"

	for cutscene_name, persistent_keys in pairs(self._debug_persistent_keys_per_cutscene(self)) do
		if not table.empty(persistent_keys) then
			output_string = output_string .. "\n" .. cutscene_name .. "\n"

			for _, persistent_key_description in ipairs(persistent_keys) do
				output_string = output_string .. "\t" .. persistent_key_description .. "\n"
			end
		end
	end

	return output_string
end
CoreCutsceneManager._debug_dump_persistent_keys_report = function (self, path)
	if path then
		local file = SystemFS:open(path, "w")

		file.write(file, self._debug_persistent_keys_report(self))
		file.close(file)
		cat_print("debug", "Persistent Keys report written to \"" .. path .. "\"")
	else
		cat_print("debug", "")
		cat_print("debug", self._debug_persistent_keys_report(self))
	end

	return 
end
CoreCutsceneManager.set_active_camera = function (self)
	error("CoreCutsceneManager:set_active_camera() is deprecated. The camera is now kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.attempt_switch_to_active_camera = function (self)
	error("CoreCutsceneManager:attempt_switch_to_active_camera() is deprecated. The camera is now kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.set_cutscene_camera_enabled = function (self)
	error("CoreCutsceneManager:set_cutscene_camera_enabled() is deprecated. The camera is now kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.set_listener_enabled = function (self)
	error("CoreCutsceneManager:set_listener_enabled() is deprecated. The listener is now kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.set_camera_attribute = function (self)
	error("CoreCutsceneManager:set_camera_attribute() is deprecated. The camera is now kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.play_camera_shake = function (self)
	error("CoreCutsceneManager:play_camera_shake() is deprecated. The camera is now kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.load = function (self)
	Application:stack_dump_error("CoreCutsceneManager:load() is deprecated. There is no need to call it.")

	return 
end
CoreCutsceneManager.save = function (self)
	error("CoreCutsceneManager:save() is deprecated. The new Cutscene Editor uses CoreCutsceneEditorProjects.")

	return 
end
CoreCutsceneManager.save_all = function (self)
	error("CoreCutsceneManager:save_all() is deprecated. The new Cutscene Editor uses CoreCutsceneEditorProjects.")

	return 
end
CoreCutsceneManager.pre_load_cutscene_units = function (self)
	Application:stack_dump_error("CoreCutsceneManager:pre_load_cutscene_units() is deprecated. There is no need to call it.")

	return 
end
CoreCutsceneManager.internal_load = function (self)
	Application:stack_dump_error("CoreCutsceneManager:internal_load() is deprecated. There is no need to call it.")

	return 
end
CoreCutsceneManager.stop_cutscene = function (self)
	Application:stack_dump_error("CoreCutsceneManager:stop_cutscene() is deprecated. Use CoreCutsceneManager:stop() instead.")
	self.stop(self)

	return 
end
CoreCutsceneManager.set_stop_at_end = function (self)
	Application:stack_dump_error("CoreCutsceneManager:set_stop_at_end() is deprecated. There is no need to call it.")

	return 
end
CoreCutsceneManager.get_current_frame_nr = function (self)
	error("CoreCutsceneManager:get_current_frame_nr() is deprecated. The playhead state is kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.get_frame_count = function (self)
	error("CoreCutsceneManager:get_frame_count() is deprecated. The frame count is part of the CoreCutscene, but is not exposed here.")

	return 
end
CoreCutsceneManager.move_to_frame = function (self)
	error("CoreCutsceneManager:move_to_frame() is deprecated. The playhead state is kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end
CoreCutsceneManager.evaluate_current_frame = function (self)
	error("CoreCutsceneManager:evaluate_current_frame() is deprecated. The playhead state is kept in CoreCutscenePlayer, but is not exposed here.")

	return 
end

return 
