core:import("CoreShapeManager")

CoreSoundEnvironmentManager = CoreSoundEnvironmentManager or class()
CoreSoundEnvironmentManager.init = function (self)
	self._areas = {}
	self._areas_per_frame = 1
	self._check_objects = {}
	self._check_object_id = 0
	self._emitters = {}
	self._area_emitters = {}
	self._ambience_changed_callback = {}
	self._ambience_changed_callbacks = {}
	self._environment_changed_callback = {}
	self.GAME_DEFAULT_ENVIRONMENT = "padded_cell"
	self._default_environment = self.GAME_DEFAULT_ENVIRONMENT
	self._current_environment = self.GAME_DEFAULT_ENVIRONMENT

	self._set_environment(self, self.GAME_DEFAULT_ENVIRONMENT)

	local in_editor = Application:editor()

	if in_editor then
		self._environments = self._environment_effects(self)
		self.GAME_DEFAULT_ENVIRONMENT = self._environments[1] or nil
		self._default_environment = self.GAME_DEFAULT_ENVIRONMENT
		self._current_environment = self.GAME_DEFAULT_ENVIRONMENT

		self._set_environment(self, self.GAME_DEFAULT_ENVIRONMENT)
	else
		self._set_environment(self, self.GAME_DEFAULT_ENVIRONMENT)
	end

	if in_editor then
		self._find_emitter_events(self)
		self._find_ambience_events(self)
		self._find_scene_events(self)
		self._find_occasional_events(self)

		self.GAME_DEFAULT_EMITTER_PATH = self._emitter.paths[1]
		self.GAME_DEFAULT_AMBIENCE = self._ambience.events[1]
		self._default_ambience = self.GAME_DEFAULT_AMBIENCE
		self.GAME_DEFAULT_OCCASIONAL = self._occasional.events[1]
		self._default_occasional = self.GAME_DEFAULT_OCCASIONAL
		self.GAME_DEFAULT_SCENE_PATH = self._scene.paths[1]
	end

	self._ambience_enabled = false
	self._occasional_blocked_by_platform = SystemInfo:platform() == Idstring("X360")
	self._ambience_sources_count = 1
	self.POSITION_OFFSET = 50
	self._active_ambience_soundbanks = {}
	self._occasional_sound_source = SoundDevice:create_source("occasional")

	return 
end
CoreSoundEnvironmentManager._find_emitter_events = function (self)
	self._emitter = {
		events = {},
		paths = {},
		soundbanks = {}
	}

	for _, soundbank in ipairs(SoundDevice:sound_banks()) do
		for event, data in pairs(SoundDevice:events(soundbank)) do
			if string.match(event, "emitter") then
				if not table.contains(self._emitter.paths, data.path) then
					table.insert(self._emitter.paths, data.path)
				end

				self._emitter.events[data.path] = self._emitter.events[data.path] or {}

				table.insert(self._emitter.events[data.path], event)

				self._emitter.soundbanks[event] = soundbank
			end
		end
	end

	table.sort(self._emitter.paths)

	return 
end
CoreSoundEnvironmentManager._find_ambience_events = function (self)
	self._ambience = {
		events = {},
		soundbanks = {}
	}

	for _, soundbank in ipairs(SoundDevice:sound_banks()) do
		for event, data in pairs(SoundDevice:events(soundbank)) do
			if string.match(event, "ambience") then
				table.insert(self._ambience.events, event)

				self._ambience.soundbanks[event] = soundbank
			end
		end
	end

	table.sort(self._ambience.events)

	return 
end
CoreSoundEnvironmentManager._find_scene_events = function (self)
	self._scene = {
		events = {},
		paths = {},
		soundbanks = {}
	}

	for _, soundbank in ipairs(SoundDevice:sound_banks()) do
		for event, data in pairs(SoundDevice:events(soundbank)) do
			if not table.contains(self._scene.paths, data.path) then
				table.insert(self._scene.paths, data.path)
			end

			self._scene.events[data.path] = self._scene.events[data.path] or {}

			table.insert(self._scene.events[data.path], event)

			self._scene.soundbanks[event] = soundbank
		end
	end

	table.sort(self._scene.paths)

	return 
end
CoreSoundEnvironmentManager._find_occasional_events = function (self)
	self._occasional = {
		events = {},
		soundbanks = {}
	}

	for _, soundbank in ipairs(SoundDevice:sound_banks()) do
		for event, data in pairs(SoundDevice:events(soundbank)) do
			if string.match(event, "occasional") then
				table.insert(self._occasional.events, event)

				self._occasional.soundbanks[event] = soundbank
			end
		end
	end

	table.sort(self._occasional.events)

	return 
end
CoreSoundEnvironmentManager.areas = function (self)
	return self._areas
end
CoreSoundEnvironmentManager.game_default_ambience = function (self)
	return self.GAME_DEFAULT_AMBIENCE
end
CoreSoundEnvironmentManager.game_default_occasional = function (self)
	return self.GAME_DEFAULT_OCCASIONAL
end
CoreSoundEnvironmentManager.game_default_emitter_path = function (self)
	return self.GAME_DEFAULT_EMITTER_PATH
end
CoreSoundEnvironmentManager.emitter_paths = function (self)
	return self._emitter.paths
end
CoreSoundEnvironmentManager.emitter_events = function (self, path)
	return (path and self._emitter.events[path]) or self._emitter.events
end
CoreSoundEnvironmentManager.emitter_soundbank = function (self, event)
	if not self._emitter then
		return 
	end

	return self._emitter.soundbanks[event]
end
CoreSoundEnvironmentManager.emitter_soundbanks = function (self)
	if not self._emitter then
		return 
	end

	return self._emitter.soundbanks
end
CoreSoundEnvironmentManager.ambience_events = function (self)
	return self._ambience.events
end
CoreSoundEnvironmentManager.ambience_soundbank = function (self, event)
	if not self._ambience then
		return 
	end

	return self._ambience.soundbanks[event]
end
CoreSoundEnvironmentManager.ambience_soundbanks = function (self)
	if not self._ambience then
		return 
	end

	return self._ambience.soundbanks
end
CoreSoundEnvironmentManager.occasional_events = function (self)
	if not self._occasional then
		return 
	end

	return self._occasional.events
end
CoreSoundEnvironmentManager.occasional_soundbank = function (self, event)
	if not self._occasional then
		return 
	end

	return self._occasional.soundbanks[event]
end
CoreSoundEnvironmentManager.occasional_soundbanks = function (self)
	if not self._occasional then
		return 
	end

	return self._occasional.soundbanks
end
CoreSoundEnvironmentManager.game_default_scene_path = function (self)
	return self.GAME_DEFAULT_SCENE_PATH
end
CoreSoundEnvironmentManager.scene_paths = function (self)
	return self._scene.paths
end
CoreSoundEnvironmentManager.scene_events = function (self, path)
	return (path and self._scene.events[path]) or self._scene.events
end
CoreSoundEnvironmentManager.scene_soundbank = function (self, event)
	return self._scene.soundbanks[event]
end
CoreSoundEnvironmentManager.scene_soundbanks = function (self)
	return self._scene.soundbanks
end
CoreSoundEnvironmentManager.scene_path = function (self, event)
	for path, events in pairs(self._scene.events) do
		if table.contains(events, event) then
			return path
		end
	end

	return 
end
CoreSoundEnvironmentManager.emitters = function (self)
	return self._emitters
end
CoreSoundEnvironmentManager.area_emitters = function (self)
	return self._area_emitters
end
CoreSoundEnvironmentManager._environment_effects = function (self)
	local effects = {}

	for name, _ in pairs(SoundDevice:effects()) do
		table.insert(effects, name)
	end

	table.sort(effects)

	return effects
end
CoreSoundEnvironmentManager.environments = function (self)
	return self._environments
end
CoreSoundEnvironmentManager.game_default_environment = function (self)
	return self.GAME_DEFAULT_ENVIRONMENT
end
CoreSoundEnvironmentManager.default_environment = function (self)
	return self._default_environment
end
CoreSoundEnvironmentManager.set_default_environment = function (self, environment)
	self._default_environment = environment

	self._set_environment(self, self._default_environment)
	self._change_acoustic(self, self._default_environment)

	return 
end
CoreSoundEnvironmentManager._set_environment = function (self, environment)
	for _, func in ipairs(self._environment_changed_callback) do
		func(environment)
	end

	self._current_environment = environment

	SoundDevice:set_default_environment({
		gain = 1,
		effect = environment
	})

	return 
end
CoreSoundEnvironmentManager.current_environment = function (self)
	return self._current_environment
end
CoreSoundEnvironmentManager.set_default_ambience = function (self, ambience_event)
	if not ambience_event then
		return 
	end

	self._default_ambience = ambience_event

	if Application:editor() then
		self.add_soundbank(self, self.ambience_soundbank(self, self._default_ambience))
	end

	for id, data in pairs(self._check_objects) do
		self._change_ambience(self, data)
	end

	return 
end
CoreSoundEnvironmentManager.default_ambience = function (self)
	return self._default_ambience
end
CoreSoundEnvironmentManager.set_default_occasional = function (self, occasional_event)
	if not occasional_event then
		return 
	end

	if occasional_event and Application:editor() and not table.contains(managers.sound_environment:occasional_events(), occasional_event) then
		if managers.editor then
			managers.editor:output_error("Default occasional event " .. occasional_event .. " no longer exits. Falls back on default.")
		end

		occasional_event = managers.sound_environment:game_default_occasional()
	end

	self._default_occasional = occasional_event

	if Application:editor() then
		self.add_soundbank(self, self.occasional_soundbank(self, self._default_occasional))
	end

	return 
end
CoreSoundEnvironmentManager.default_occasional = function (self)
	return self._default_occasional
end
CoreSoundEnvironmentManager.add_soundbank = function (self, soundbank)
	if not soundbank then
		Application:error("Cant load nil soundbank")

		return 
	end

	if Application:editor() then
		CoreEngineAccess._editor_load("bnk":id(), soundbank.id(soundbank))
	end

	return 
end
CoreSoundEnvironmentManager.set_to_default = function (self)
	self.set_default_environment(self, self.GAME_DEFAULT_ENVIRONMENT)
	self.set_default_ambience(self, self.GAME_DEFAULT_AMBIENCE)
	self.set_default_occasional(self, self.GAME_DEFAULT_OCCASIONAL)
	self.set_ambience_enabled(self, false)

	return 
end
CoreSoundEnvironmentManager.add_area = function (self, area_params)
	local area = SoundEnvironmentArea:new(area_params)

	table.insert(self._areas, area)

	return area
end
CoreSoundEnvironmentManager.remove_area = function (self, area)
	area.remove(area)

	for _, data in pairs(self._check_objects) do
		if area == data.area then
			data.area = nil

			self._change_ambience(self, data)
		end

		data.sound_area_counter = 1
	end

	table.delete(self._areas, area)

	return 
end
CoreSoundEnvironmentManager.enable_area = function (self, name, enable)
	for _, area in pairs(self._areas) do
		if area.name(area) == name then
			if enable then
				area._add_environment(area)
				area.enable(area, true)
			else
				area._remove_environment(area)
				area.enable(area, false)
			end

			return 
		end
	end

	return 
end
CoreSoundEnvironmentManager.add_emitter = function (self, emitter_params)
	local emitter = SoundEnvironmentEmitter:new(emitter_params)

	table.insert(self._emitters, emitter)

	return emitter
end
CoreSoundEnvironmentManager.remove_emitter = function (self, emitter)
	emitter.destroy(emitter)
	table.delete(self._emitters, emitter)

	return 
end
CoreSoundEnvironmentManager.add_area_emitter = function (self, emitter_params)
	local emitter = SoundEnvironmentAreaEmitter:new(emitter_params)

	table.insert(self._area_emitters, emitter)

	return emitter
end
CoreSoundEnvironmentManager.remove_area_emitter = function (self, emitter)
	emitter.destroy(emitter)
	table.delete(self._area_emitters, emitter)

	return 
end
CoreSoundEnvironmentManager.add_listener = function (self, data)
	Application:throw_exception("add_listener function is no longer working because of new sound implementation. Use add_check_object instead.")

	local distance, orientation, occlusion = Sound:listener(data.listener)
	data.object = distance

	return self.add_check_object(self, data)
end
CoreSoundEnvironmentManager.add_check_object = function (self, data)
	if not data.object then
		Application:error("Must use an Object3D when adding check objects to sound environment manager.")

		return nil
	end

	self._disable_fallback(self)

	self._check_object_id = self._check_object_id + 1
	local soundsource = SoundDevice:create_source("ambience_source")

	soundsource.enable_env(soundsource, false)

	local surround = {}

	for i = 1, self._ambience_sources_count, 1 do
		local source = SoundDevice:create_source("ambience_surround_" .. i)

		source.enable_env(source, false)

		local distance = 15000
		local x = ((i == 1 or i == 4) and -distance) or distance
		local y = ((i == 1 or i == 2) and distance) or -distance
		local offset = Vector3(x, y, 0)

		source.set_position(source, data.object:position() + offset)
		table.insert(surround, {
			source = source,
			offset = offset
		})
	end

	local t = {
		sound_area_counter = 1,
		object = data.object,
		soundsource = soundsource,
		surround = surround,
		surround_iterator = (not surround or 0) and nil,
		active = data.active,
		listener = data.listener,
		primary = data.primary,
		id = self._check_object_id,
		next_occasional = self._next_occasional(self)
	}

	self._change_ambience(self, t)

	self._check_objects[self._check_object_id] = t

	return self._check_object_id
end
CoreSoundEnvironmentManager.remove_check_object = function (self, id)
	local remove_object = self._check_objects[id]

	remove_object.soundsource:stop()

	if remove_object.surround then
		for _, surround_data in ipairs(remove_object.surround) do
			surround_data.source:stop()
		end
	end

	self._check_objects[id] = nil

	self._enable_fallback(self)

	return 
end
CoreSoundEnvironmentManager.set_check_object_active = function (self, id, active)
	local object = self._check_objects[id]

	if object.active == active then
		return 
	end

	object.active = active

	if not active then
		object.soundsource:stop()

		if object.surround then
			for _, surround_data in ipairs(object.surround) do
				surround_data.source:stop()
			end
		end
	else
		self._check_inside(self, object)

		if not object.area then
			self._change_ambience(self, object, 1)
		end
	end

	return 
end
CoreSoundEnvironmentManager.obj_alive = function (self, id)
	local data = self._check_objects[id]

	return data and alive(data.object)
end
CoreSoundEnvironmentManager.check_object = function (self, id)
	return self._check_objects[id]
end
CoreSoundEnvironmentManager._disable_fallback = function (self)
	local fallback = self._check_objects[self._fallback_id]

	if fallback then
		self.set_check_object_active(self, self._fallback_id, false)
	end

	return 
end
CoreSoundEnvironmentManager._enable_fallback = function (self)
	local fallback = self._check_objects[self._fallback_id]

	if fallback and not fallback.active then
		for id, object in pairs(self._check_objects) do
			if object ~= fallback then
				return 
			end
		end

		self.set_check_object_active(self, self._fallback_id, true)
	end

	return 
end
CoreSoundEnvironmentManager._next_occasional = function (self)
	return Application:time() + math.rand(4) + 6
end
local check_pos = Vector3()
local mvec_surround_pos = Vector3()
CoreSoundEnvironmentManager._update_object = function (self, t, dt, id, data)
	data.object:m_position(check_pos)

	local still_inside = nil

	if data.surround then
		local surround_data = data.surround[data.surround_iterator + 1]

		mvector3.set(mvec_surround_pos, check_pos)
		mvector3.add(mvec_surround_pos, surround_data.offset)
		surround_data.source:set_position(mvec_surround_pos)

		data.surround_iterator = math.mod(data.surround_iterator + 1, self._ambience_sources_count)
	end

	if data.next_occasional < t then
		data.next_occasional = self._next_occasional(self)

		if self._ambience_enabled and not self._occasional_blocked_by_platform then
			local event = (data.area and data.area:use_occasional() and data.area:occasional_event()) or self._default_occasional

			if event then
				local x = math.rand(2) - 1
				local y = math.rand(2) - 1
				local pos = check_pos + Vector3(x, y, 0):normalized()*7500

				self._occasional_sound_source:set_position(pos)
				self._occasional_sound_source:post_event(event)
			end
		end
	end

	if data.area then
		still_inside = data.area:still_inside(check_pos)

		if still_inside then
			return data.area
		end

		if self._check_inside(self, data) then
			return data.area
		end

		self._change_acoustic(self, self._default_environment)
		self._change_ambience(self, data)
	end

	if self._check_inside(self, data) then
		return data.area
	end

	return nil
end
CoreSoundEnvironmentManager._fallback_on_camera = function (self)
	if not self._use_fallback_on_camera then
		return 
	end

	local vps = managers.viewport:active_viewports()

	if #vps == 0 then
		return 
	end

	local camera = vps[1]:camera()

	if not camera then
		return 
	end

	local fallback = self._check_objects[self._fallback_id]

	if fallback then
		if fallback.object ~= camera then
			fallback.object = camera
		end
	elseif not next(self._check_objects) then
		self._fallback_id = self.add_check_object(self, {
			primary = true,
			active = true,
			object = camera
		})
		self.check_object(self, self._fallback_id).fallback = true
	end

	return 
end
CoreSoundEnvironmentManager.update = function (self, t, dt)
	for id, data in pairs(self._check_objects) do
		if data.active then
			self._update_object(self, t, dt, id, data)
		end
	end

	return 
end
CoreSoundEnvironmentManager._change_ambience = function (self, data, sound_gain)
	local area = data.area
	local ambience_event = (area and area.ambience_event(area)) or self._default_ambience

	if self._ambience_changed_callbacks[data.id] then
		for _, func in ipairs(self._ambience_changed_callbacks[data.id]) do
			func(ambience_event)
		end
	end

	for _, func in ipairs(self._ambience_changed_callback) do
		func(ambience_event)
	end

	if not self._ambience_enabled then
		return 
	end

	if data.surround then
		for _, surround_data in ipairs(data.surround) do
			surround_data.source:post_event(ambience_event)
		end
	end

	return 
end
CoreSoundEnvironmentManager._change_acoustic = function (self, environment)
	self._acoustic = environment

	if tweak_data.sound.acoustics[environment] and tweak_data.sound.acoustics[environment].states then
		for state, value in pairs(tweak_data.sound.acoustics[environment].states) do
			SoundDevice:set_state(state, value)
		end
	end

	return 
end
local check_pos2 = Vector3()
CoreSoundEnvironmentManager._check_inside = function (self, data)
	if 0 < #self._areas then
		local check_pos = check_pos2

		data.object:m_position(check_pos)

		for i = 1, self._areas_per_frame, 1 do
			local area = self._areas[data.sound_area_counter]
			data.sound_area_counter = math.mod(data.sound_area_counter, #self._areas) + 1

			if area.is_inside(area, check_pos) then
				data.area = area

				self._change_acoustic(self, data.area:environment())
				self._change_ambience(self, data)

				return area
			end
		end
	end

	data.area = nil

	return data.area
end
CoreSoundEnvironmentManager.ambience_enabled = function (self)
	return self._ambience_enabled
end
CoreSoundEnvironmentManager.set_ambience_enabled = function (self, enabled)
	self._ambience_enabled = enabled

	if not self._default_ambience then
		return 
	end

	for _, data in pairs(self._check_objects) do
		if self._ambience_enabled and data.active then
			self._change_ambience(self, data)
		else
			data.soundsource:stop()

			if data.surround then
				for _, surround_data in ipairs(data.surround) do
					surround_data.source:stop()
				end
			end
		end
	end

	return 
end
CoreSoundEnvironmentManager.environment_at_position = function (self, pos)
	local environment = self._default_environment
	local ambience = self._default_ambience
	local occasional = self._default_occasional

	for _, area in ipairs(self._areas) do
		if area.is_inside(area, pos) then
			environment = area.environment(area)
			ambience = area.ambience_event(area)
			occasional = area.occasional_event(area)

			break
		end
	end

	return environment, ambience, occasional
end
CoreSoundEnvironmentManager.add_ambience_changed_callback = function (self, func, id)
	if id then
		self._ambience_changed_callbacks[id] = self._ambience_changed_callbacks[id] or {}

		table.insert(self._ambience_changed_callbacks[id], func)

		return 
	end

	table.insert(self._ambience_changed_callback, func)

	return 
end
CoreSoundEnvironmentManager.remove_ambience_changed_callback = function (self, func, id)
	if id and self._ambience_changed_callbacks[id] then
		table.delete(self._ambience_changed_callbacks[id], func)

		return 
	end

	table.delete(self._ambience_changed_callback, func)

	return 
end
CoreSoundEnvironmentManager.add_environment_changed_callback = function (self, func)
	table.insert(self._environment_changed_callback, func)

	return 
end
CoreSoundEnvironmentManager.remove_environment_changed_callback = function (self, func)
	table.delete(self._environment_changed_callback, func)

	return 
end
CoreSoundEnvironmentManager.destroy = function (self)
	for i, emitter in ipairs(self._emitters) do
		emitter.destroy(emitter)
	end

	self._emitters = {}

	for _, env_area in ipairs(self._areas) do
		env_area.remove(env_area)
	end

	self._areas = {}

	self._occasional_sound_source:stop()

	return 
end
SoundEnvironmentArea = SoundEnvironmentArea or class(CoreShapeManager.ShapeBox)
SoundEnvironmentArea.init = function (self, params)
	params.type = "box"

	SoundEnvironmentArea.super.init(self, params)

	self._environment = params.environment or managers.sound_environment:game_default_environment()
	self._ambience_event = params.ambience_event or managers.sound_environment:game_default_ambience()
	self._occasional_event = params.occasional_event or managers.sound_environment:game_default_occasional()
	self._use_environment = params.use_environment or (params.use_environment == nil and true)
	self._use_ambience = params.use_ambience or (params.use_ambience == nil and true)
	self._use_occasional = params.use_occasional or (params.use_occasional == nil and true)
	self._gain = params.gain or 0
	self._name = params.name or ""
	self._enable = true

	self._init_environment_effect(self)
	self._init_event(self)

	self._environment_shape = EnvironmentShape(self.position(self), self.size(self), self.rotation(self))

	self._add_environment(self)

	if Application:editor() then
		managers.sound_environment:add_soundbank(managers.sound_environment:ambience_soundbank(self._ambience_event))
		managers.sound_environment:add_soundbank(managers.sound_environment:occasional_soundbank(self._occasional_event))
	end

	return 
end
SoundEnvironmentArea._init_event = function (self)
	if Application:editor() then
		if not table.contains(managers.sound_environment:ambience_events(), self._ambience_event) then
			managers.editor:output_error("Ambience event " .. self._ambience_event .. " no longer exits. Falls back on default.")
			self.set_environment_ambience(self, managers.sound_environment:game_default_ambience())
		end

		if self._occasional_event and not table.contains(managers.sound_environment:occasional_events(), self._occasional_event) then
			managers.editor:output_error("Occasional event " .. self._occasional_event .. " no longer exits. Falls back on default.")
			self.set_environment_occasional(self, managers.sound_environment:game_default_occasional())
		end
	end

	return 
end
SoundEnvironmentArea._init_environment_effect = function (self)
	if Application:editor() and not table.contains(managers.sound_environment:environments(), self._environment) then
		managers.editor:output_error("Environment effect " .. self._environment .. " no longer exits. Falls back on default.")
		self.set_environment(self, managers.sound_environment:game_default_environment())
	end

	return 
end
SoundEnvironmentArea._add_environment = function (self)
	if self._use_environment and not self._environment_id then
		self._environment_id = SoundDevice:add_environment({
			effect = self._environment,
			gain = self._gain,
			shape = self._environment_shape
		})
	end

	return 
end
SoundEnvironmentArea._remove_environment = function (self)
	if self._environment_id then
		SoundDevice:remove_environment(self._environment_id)

		self._environment_id = nil
	end

	return 
end
SoundEnvironmentArea.enable = function (self, enable)
	self._enable = enable

	return 
end
SoundEnvironmentArea.name = function (self)
	return (self._unit and self._unit:unit_data().name_id) or self._name
end
SoundEnvironmentArea.environment = function (self)
	return self._environment
end
SoundEnvironmentArea.set_environment = function (self, environment)
	self._environment = environment

	self._update_environment(self)

	return 
end
SoundEnvironmentArea.ambience_event = function (self)
	return self._ambience_event
end
SoundEnvironmentArea.set_environment_ambience = function (self, ambience_event)
	if not ambience_event then
		return 
	end

	self._ambience_event = ambience_event

	if Application:editor() then
		managers.sound_environment:add_soundbank(managers.sound_environment:ambience_soundbank(self._ambience_event))
	end

	return 
end
SoundEnvironmentArea.set_use_ambience = function (self, use_ambience)
	self._use_ambience = use_ambience

	return 
end
SoundEnvironmentArea.use_ambience = function (self)
	return self._use_ambience
end
SoundEnvironmentArea.occasional_event = function (self)
	return self._occasional_event
end
SoundEnvironmentArea.set_environment_occasional = function (self, occasional_event)
	self._occasional_event = occasional_event

	if not occasional_event then
		return 
	end

	if Application:editor() then
		managers.sound_environment:add_soundbank(managers.sound_environment:occasional_soundbank(self._occasional_event))
	end

	return 
end
SoundEnvironmentArea.set_use_occasional = function (self, use_occasional)
	self._use_occasional = use_occasional

	return 
end
SoundEnvironmentArea.use_occasional = function (self)
	return self._use_occasional
end
SoundEnvironmentArea.set_use_environment = function (self, use_environment)
	self._use_environment = use_environment

	if self._use_environment then
		self._add_environment(self)
	else
		self._remove_environment(self)
	end

	return 
end
SoundEnvironmentArea.use_environment = function (self)
	return self._use_environment
end
SoundEnvironmentArea.set_unit = function (self, unit)
	SoundEnvironmentArea.super.set_unit(self, unit)
	self._environment_shape:link(unit.orientation_object(unit))

	return 
end
SoundEnvironmentArea._update_environment = function (self)
	if self._environment_id then
		SoundDevice:update_environment(self._environment_id, {
			effect = self._environment,
			gain = self._gain,
			shape = self._environment_shape
		})
	end

	return 
end
SoundEnvironmentArea._update_environment_size = function (self)
	self._environment_shape:set_size(self.size(self))
	self._update_environment(self)

	return 
end
SoundEnvironmentArea.set_property = function (self, property, value)
	SoundEnvironmentArea.super.set_property(self, property, value)
	self._update_environment_size(self)

	return 
end
SoundEnvironmentArea.set_width = function (self, width)
	SoundEnvironmentArea.super.set_width(self, width)
	self._update_environment_size(self)

	return 
end
SoundEnvironmentArea.set_depth = function (self, depth)
	SoundEnvironmentArea.super.set_depth(self, depth)
	self._update_environment_size(self)

	return 
end
SoundEnvironmentArea.set_height = function (self, height)
	SoundEnvironmentArea.super.set_height(self, height)
	self._update_environment_size(self)

	return 
end
SoundEnvironmentArea.remove = function (self)
	self._remove_environment(self)

	return 
end
SoundEnvironmentArea.still_inside = function (self, pos)
	if not self._enable or not self._use_ambience then
		return false
	end

	return SoundEnvironmentArea.super.still_inside(self, pos)
end
SoundEnvironmentArea.is_inside = function (self, pos)
	if not self._enable or not self._use_ambience then
		return false
	end

	return SoundEnvironmentArea.super.is_inside(self, pos)
end
SoundEnvironmentEmitter = SoundEnvironmentEmitter or class()
SoundEnvironmentEmitter.init = function (self, params)
	self._position = params.position or Vector3()
	self._rotation = params.rotation or Rotation()
	self._name = params.name or ""
	self._soundsource = SoundDevice:create_source(self._name)
	local emitter_path = managers.sound_environment:game_default_emitter_path()

	self.set_emitter_event(self, params.emitter_event or managers.sound_environment:emitter_events(emitter_path)[1])

	return 
end
SoundEnvironmentEmitter.save_xml = function (self, t)
	local properties = {
		name = self.name(self),
		position = self.position(self),
		rotation = self.rotation(self),
		emitter_event = self._emitter_event
	}

	return simple_value_string("properties", properties, t)
end
SoundEnvironmentEmitter.name = function (self)
	return (self._unit and self._unit:unit_data().name_id) or self._name
end
SoundEnvironmentEmitter.emitter_path = function (self)
	for path, events in pairs(managers.sound_environment:emitter_events()) do
		if table.contains(events, self._emitter_event) then
			return path
		end
	end

	return 
end
SoundEnvironmentEmitter.emitter_event = function (self)
	return self._emitter_event
end
SoundEnvironmentEmitter.set_emitter_path = function (self, emitter_path)
	if not emitter_path then
		return 
	end

	local current_path = self.emitter_path(self)

	if emitter_path == current_path then
		return 
	end

	self.set_emitter_event(self, managers.sound_environment:emitter_events(emitter_path)[1])

	return 
end
SoundEnvironmentEmitter.set_emitter_event = function (self, emitter_event)
	self._emitter_event = emitter_event

	if Application:editor() then
		managers.sound_environment:add_soundbank(managers.sound_environment:emitter_soundbank(self._emitter_event))
	end

	self.play_sound(self)

	return 
end
SoundEnvironmentEmitter.set_unit = function (self, unit)
	self._unit = unit

	self._soundsource:link(self._unit:orientation_object())

	return 
end
SoundEnvironmentEmitter.position = function (self)
	return (self._unit and self._unit:position()) or self._position
end
SoundEnvironmentEmitter.set_position = function (self, position)
	self._position = position

	return 
end
SoundEnvironmentEmitter.rotation = function (self)
	return (self._unit and self._unit:rotation()) or self._rotation
end
SoundEnvironmentEmitter.set_rotation = function (self, rotation)
	self._rotation = rotation

	return 
end
SoundEnvironmentEmitter.play_sound = function (self)
	if self._sound_event then
		self._sound_event:stop()
	end

	self._soundsource:stop()

	if self._unit then
		self._soundsource:link(self._unit:orientation_object())
	else
		self._soundsource:set_position(self.position(self))
		self._soundsource:set_orientation(self.rotation(self))
	end

	self._sound_event = self._soundsource:post_event(self._emitter_event)

	return 
end
SoundEnvironmentEmitter.restart = function (self)
	self.play_sound(self)

	return 
end
SoundEnvironmentEmitter.draw = function (self, t, dt, r, g, b)
	Application:draw_sphere(self.position(self), 75, r, g, b)
	Application:draw_cone(self.position(self), self.position(self) + self.rotation(self):y()*500, 500, r, g, b)
	Application:draw_cone(self.position(self), self.position(self) - self.rotation(self):y()*500, 500, r, g, b)

	return 
end
SoundEnvironmentEmitter.destroy = function (self)
	if self._sound_event then
		self._sound_event:stop()

		self._sound_event = nil
	end

	self._soundsource:delete()

	self._soundsource = nil

	return 
end
SoundEnvironmentAreaEmitter = SoundEnvironmentAreaEmitter or class(CoreShapeManager.ShapeBoxMiddle)
SoundEnvironmentAreaEmitter.init = function (self, params)
	params.type = "box_middle"

	SoundEnvironmentAreaEmitter.super.init(self, params)

	self._properties.name = params.name or ""
	self._soundsource = SoundDevice:create_source(self._properties.name)
	local emitter_path = managers.sound_environment:game_default_emitter_path()

	self.set_emitter_event(self, params.emitter_event or managers.sound_environment:emitter_events(emitter_path)[1])

	return 
end
SoundEnvironmentAreaEmitter.save = function (self, ...)
	self._properties.name = self.name(self)

	return SoundEnvironmentAreaEmitter.super.save(self, ...)
end
SoundEnvironmentAreaEmitter.name = function (self)
	return (self._unit and self._unit:unit_data().name_id) or self._name
end
SoundEnvironmentAreaEmitter.emitter_path = function (self)
	for path, events in pairs(managers.sound_environment:emitter_events()) do
		if table.contains(events, self._properties.emitter_event) then
			return path
		end
	end

	return 
end
SoundEnvironmentAreaEmitter.emitter_event = function (self)
	return self._properties.emitter_event
end
SoundEnvironmentAreaEmitter.set_emitter_path = function (self, emitter_path)
	if not emitter_path then
		return 
	end

	local current_path = self.emitter_path(self)

	if emitter_path == current_path then
		return 
	end

	self.set_emitter_event(self, managers.sound_environment:emitter_events(emitter_path)[1])

	return 
end
SoundEnvironmentAreaEmitter.set_emitter_event = function (self, emitter_event)
	self._properties.emitter_event = emitter_event

	if Application:editor() then
		managers.sound_environment:add_soundbank(managers.sound_environment:emitter_soundbank(self._properties.emitter_event))
	end

	self.play_sound(self)

	return 
end
SoundEnvironmentAreaEmitter.play_sound = function (self)
	if self._sound_event then
		self._sound_event:stop()
	end

	if self._unit then
		self._soundsource:link(self._unit:orientation_object())
	else
		self._soundsource:set_position(self.position(self))
	end

	self._sound_event = self._soundsource:post_event(self._properties.emitter_event)

	return 
end
SoundEnvironmentAreaEmitter.set_extent = function (self)
	return 
end
SoundEnvironmentAreaEmitter.extent = function (self)
	return Vector3(self._properties.width/2, self._properties.depth/2, self._properties.height/2)
end
SoundEnvironmentAreaEmitter.set_property = function (self, ...)
	SoundEnvironmentAreaEmitter.super.set_property(self, ...)
	self.set_extent(self)

	return 
end
SoundEnvironmentAreaEmitter.set_unit = function (self, unit)
	SoundEnvironmentAreaEmitter.super.set_unit(self, unit)
	self._soundsource:link(self._unit:orientation_object())

	return 
end
SoundEnvironmentAreaEmitter.restart = function (self)
	self.play_sound(self)

	return 
end
SoundEnvironmentAreaEmitter.destroy = function (self)
	if self._sound_event then
		self._sound_event:stop()

		self._sound_event = nil
	end

	self._soundsource:delete()

	self._soundsource = nil

	return 
end

return 
