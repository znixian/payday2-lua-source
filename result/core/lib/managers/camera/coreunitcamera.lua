core:module("CoreUnitCamera")
core:import("CoreClass")
core:import("CoreEvent")

UnitCamera = UnitCamera or CoreClass.class()
UnitCamera.init = function (self, unit)
	self._unit = unit
	self._active_count = 0

	return 
end
UnitCamera.destroy = function (self)
	return 
end
UnitCamera.create_layers = function (self)
	return 
end
UnitCamera.activate = function (self)
	local is_deactivated = self._active_count == 0
	self._active_count = self._active_count + 1

	if is_deactivated then
		self.on_activate(self, true)
	end

	return 
end
UnitCamera.deactivate = function (self)
	assert(0 < self._active_count)

	self._active_count = self._active_count - 1
	local should_deactivate = self._active_count == 0

	if should_deactivate then
		self.on_activate(self, false)
	end

	return should_deactivate
end
UnitCamera.on_activate = function (self, active)
	return 
end
UnitCamera.is_active = function (self)
	return 0 < self._active_count
end
UnitCamera.apply_camera = function (self, camera_manager)
	return 
end

return 
