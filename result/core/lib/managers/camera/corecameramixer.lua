core:module("CoreCameraMixer")
core:import("CoreClass")

local mvector3_add = mvector3.add
local mvector3_sub = mvector3.subtract
local mvector3_mul = mvector3.multiply
local mvector3_set = mvector3.set
local mvector3_copy = mvector3.copy
local mrotation_mul = mrotation.multiply
local mrotation_slerp = mrotation.slerp
local mrotation_set_zero = mrotation.set_zero

local function safe_divide(a, b)
	if b == 0 then
		return 1
	end

	return a/b
end

CameraMixer = CameraMixer or CoreClass.class()
CameraMixer.init = function (self, name)
	self._name = name
	self._cameras = {}

	return 
end
CameraMixer.destroy = function (self)
	for index, camera in ipairs(self._cameras) do
		camera.camera:destroy()
	end

	self._cameras = {}

	return 
end
CameraMixer.add_camera = function (self, camera, blend_time)
	table.insert(self._cameras, {
		time = 0,
		camera = camera,
		blend_time = blend_time
	})

	return 
end
CameraMixer.stop = function (self)
	for index, camera in ipairs(self._cameras) do
		camera.camera:destroy()
	end

	self._cameras = {}

	return 
end
CameraMixer.update = function (self, cud, cud_class, time, dt)
	for index, camera in ipairs(self._cameras) do
		local _camera = camera.camera
		local cam_data = cud_class.new(cud_class, cud)

		for _, cam in ipairs(_camera._nodes) do
			local local_cam_data = cud_class.new(cud_class)

			cam.update(cam, time, dt, cam_data, local_cam_data)
			cam_data.transform_with(cam_data, local_cam_data)
			mvector3_set(cam._position, cam_data._position)
			mrotation_set_zero(cam._rotation)
			mrotation_mul(cam._rotation, cam_data._rotation)
		end

		camera.cam_data = cam_data
	end

	local full_blend_index = 1

	for index, _camera in ipairs(self._cameras) do
		_camera.time = _camera.time + dt
		local factor = nil

		if 1 < index then
			factor = math.sin(math.clamp(safe_divide(_camera.time, _camera.blend_time), 0, 1)*90)
		else
			factor = 1
		end

		cud.interpolate_to_target(cud, _camera.cam_data, factor)

		if 1 <= factor then
			full_blend_index = index
		end
	end

	for i = 1, full_blend_index - 1, 1 do
		self._cameras[1].camera:destroy()
		table.remove(self._cameras, 1)
	end

	for index, camera in ipairs(self._cameras) do
		assert(not camera.camera._destroyed)
	end

	return 
end
CameraMixer.debug_render = function (self, t, dt)
	local pen = Draw:pen(Color(0.05, 0, 0, 1))

	for _, camera in ipairs(self._cameras) do
		local cam = camera.camera
		local parent_node = nil

		for _, node in ipairs(cam._nodes) do
			node.debug_render(node, t, dt)

			if parent_node then
				pen.line(pen, parent_node._position, node._position)
			end

			parent_node = node
		end
	end

	return 
end
CameraMixer.active_camera = function (self)
	local camera_count = #self._cameras

	if camera_count == 0 then
		return nil
	end

	return self._cameras[camera_count].camera
end
CameraMixer.cameras = function (self)
	local cameras = {}

	for _, camera in ipairs(self._cameras) do
		table.insert(cameras, camera.camera)
	end

	return cameras
end

return 
