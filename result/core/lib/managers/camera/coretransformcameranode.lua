core:module("CoreTransformCameraNode")
core:import("CoreClass")
core:import("CoreMath")

local mvector3_add = mvector3.add
local mvector3_sub = mvector3.subtract
local mvector3_set = mvector3.set
local mvector3_rotate_with = mvector3.rotate_with
local mrotation_set_zero = mrotation.set_zero
local mrotation_mul = mrotation.multiply
TransformCameraNode = TransformCameraNode or CoreClass.class()
TransformCameraNode.init = function (self, settings)
	self._child = nil
	self._parent_camera = nil
	self._local_position = settings.position
	self._local_rotation = settings.rotation
	self._local_fov = settings.fov
	self._local_dof_near_min = settings.dof_near_min
	self._local_dof_near_max = settings.dof_near_max
	self._local_dof_far_min = settings.dof_far_min
	self._local_dof_far_max = settings.dof_far_max
	self._local_dof_amount = settings.dof_amount
	self._position = Vector3(0, 0, 0)
	self._rotation = Rotation()
	self._name = settings.name
	self._settings = settings

	return 
end
TransformCameraNode.compile_settings = function (xml_node, settings)
	if xml_node.has_parameter(xml_node, "name") then
		settings.name = xml_node.parameter(xml_node, "name")
	end

	if xml_node.has_parameter(xml_node, "position") then
		settings.position = CoreMath.string_to_vector(xml_node.parameter(xml_node, "position"))
	else
		settings.position = Vector3(0, 0, 0)
	end

	if xml_node.has_parameter(xml_node, "rotation") then
		settings.rotation = CoreMath.string_to_rotation(xml_node.parameter(xml_node, "rotation"))
	else
		settings.rotation = Rotation()
	end

	if xml_node.has_parameter(xml_node, "fov") then
		settings.fov = tonumber(xml_node.parameter(xml_node, "fov"))
	else
		settings.fov = 0
	end

	if xml_node.has_parameter(xml_node, "dof_near_min") then
		settings.dof_near_min = tonumber(xml_node.parameter(xml_node, "dof_near_min"))
	else
		settings.dof_near_min = 0
	end

	if xml_node.has_parameter(xml_node, "dof_near_max") then
		settings.dof_near_max = tonumber(xml_node.parameter(xml_node, "dof_near_max"))
	else
		settings.dof_near_max = 0
	end

	if xml_node.has_parameter(xml_node, "dof_far_min") then
		settings.dof_far_min = tonumber(xml_node.parameter(xml_node, "dof_far_min"))
	else
		settings.dof_far_min = 0
	end

	if xml_node.has_parameter(xml_node, "dof_far_max") then
		settings.dof_far_max = tonumber(xml_node.parameter(xml_node, "dof_far_max"))
	else
		settings.dof_far_max = 0
	end

	if xml_node.has_parameter(xml_node, "dof_amount") then
		settings.dof_amount = tonumber(xml_node.parameter(xml_node, "dof_amount"))
	else
		settings.dof_amount = 0
	end

	return 
end
TransformCameraNode.destroy = function (self)
	self._child = nil
	self._parent_camera = nil

	return 
end
TransformCameraNode.name = function (self)
	return self._name
end
TransformCameraNode.set_parent = function (self, camera)
	camera._child = self
	self._parent_camera = camera

	return 
end
TransformCameraNode.child = function (self)
	return self._child
end
TransformCameraNode.set_local_position = function (self, position)
	self._local_position = position

	return 
end
TransformCameraNode.set_local_rotation = function (self, rotation)
	self._local_rotation = rotation

	return 
end
TransformCameraNode.set_local_position_from_world_position = function (self, position)
	local parent_camera = self._parent_camera

	if parent_camera then
		local parent_position = parent_camera.position(parent_camera)
		local parent_rotation = parent_camera.rotation(parent_camera)

		mvector3_set(self._local_position, position)
		mvector3_sub(self._local_position, parent_camera.position(parent_camera))
		mvector3_rotate_with(self._local_position, parent_camera.rotation(parent_camera):inverse())
	else
		mvector3_set(self._local_position, position)
	end

	return 
end
TransformCameraNode.set_local_rotation_from_world_rotation = function (self, rotation)
	local parent_camera = self._parent_camera

	if parent_camera then
		local parent_rotation = parent_camera.rotation(parent_camera)
		self._local_rotation = parent_rotation.inverse(parent_rotation)*rotation
	else
		self._local_rotation = rotation
	end

	return 
end
TransformCameraNode.position = function (self)
	return self._position
end
TransformCameraNode.rotation = function (self)
	return self._rotation
end
TransformCameraNode.local_position = function (self)
	return self._local_position
end
TransformCameraNode.local_rotation = function (self)
	return self._local_rotation
end
TransformCameraNode.update = function (self, t, dt, in_data, out_data)
	if self._pivot_position then
		out_data.set_pivot_position(out_data, self._pivot_position)
	end

	if self._pivot_rotation then
		out_data.set_pivot_rotation(out_data, self._pivot_rotation)
	end

	out_data.set_position(out_data, self._local_position)
	out_data.set_rotation(out_data, self._local_rotation)
	out_data.set_fov(out_data, self._local_fov)
	out_data.set_dof(out_data, self._local_dof_amount, self._local_dof_near_min, self._local_dof_near_max, self._local_dof_far_min, self._local_dof_far_max)

	return 
end
TransformCameraNode.debug_render = function (self, t, dt)
	local x_pen = Draw:pen(Color(0.05, 1, 0, 0))
	local y_pen = Draw:pen(Color(0.05, 0, 1, 0))
	local z_pen = Draw:pen(Color(0.05, 0, 0, 1))
	local position = self._position
	local rotation = self._rotation

	x_pen.line(x_pen, position, position + rotation.x(rotation)*2)
	y_pen.line(y_pen, position, position + rotation.y(rotation)*2)
	z_pen.line(z_pen, position, position + rotation.z(rotation)*2)

	local brush = Draw:brush(Color(0.3, 1, 1, 1))

	brush.sphere(brush, position, 1)

	return 
end
TransformCameraNode.parent_camera = function (self)
	return self._parent_camera
end

return 
