core:module("CoreGameState")
core:import("CoreFiniteStateMachine")
core:import("CoreGameStateInit")
core:import("CoreSessionGenericState")
core:import("CoreRequester")

GameState = GameState or class(CoreSessionGenericState.State)
GameState.init = function (self, player_slots, session_manager)
	self._player_slots = player_slots
	self._session_manager = session_manager
	self._game_requester = CoreRequester.Requester:new()
	self._front_end_requester = CoreRequester.Requester:new()

	assert(self._session_manager)

	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreGameStateInit.Init, "game_state", self)

	return 
end
GameState.set_debug = function (self, debug_on)
	self._state:set_debug(debug_on)

	return 
end
GameState.default_data = function (data)
	data.start_state = "GameStateInit"

	return 
end
GameState.save = function (self, data)
	self._state:save(data.start_state)

	return 
end
GameState.update = function (self, t, dt)
	if self._state:state().update then
		self._state:update(t, dt)
	end

	return 
end
GameState.end_update = function (self, t, dt)
	if self._state:state().end_update then
		self._state:state():end_update(t, dt)
	end

	return 
end
GameState.transition = function (self)
	self._state:transition()

	return 
end
GameState.player_slots = function (self)
	return self._player_slots
end
GameState.is_in_pre_front_end = function (self)
	return self._is_in_pre_front_end
end
GameState.is_in_front_end = function (self)
	return self._is_in_front_end
end
GameState.is_in_init = function (self)
	return self._is_in_init
end
GameState.is_in_editor = function (self)
	return self._is_in_editor
end
GameState.is_in_game = function (self)
	return self._is_in_game
end
GameState.is_preparing_for_loading_game = function (self)
	return self._is_preparing_for_loading_game
end
GameState.is_preparing_for_loading_front_end = function (self)
	return self._is_preparing_for_loading_front_end
end
GameState._session_info = function (self)
	return self._session_manager:session():session_info()
end
GameState.request_game = function (self)
	self._game_requester:request()

	return 
end
GameState.request_front_end = function (self)
	self._front_end_requester:request()

	return 
end

return 
