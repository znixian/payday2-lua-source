core:module("CoreGameStatePreFrontEnd")
core:import("CoreGameStateFrontEnd")

PreFrontEnd = PreFrontEnd or class()
PreFrontEnd.init = function (self)
	local player_slot = self.game_state:player_slots():primary_slot()

	player_slot._release_user_from_slot(player_slot)

	self.game_state._is_in_pre_front_end = true

	return 
end
PreFrontEnd.destroy = function (self)
	self.game_state._is_in_pre_front_end = false

	return 
end
PreFrontEnd.transition = function (self)
	if not self.game_state:player_slots():has_primary_local_user() then
		return 
	end

	local local_user = self.game_state:player_slots():primary_local_user()

	if local_user.profile_data_is_loaded(local_user) then
		return CoreGameStateFrontEnd.FrontEnd
	end

	return 
end

return 
