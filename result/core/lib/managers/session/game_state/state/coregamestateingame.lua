core:module("CoreGameStateInGame")
core:import("CoreGameStatePrepareLoadingFrontEnd")

InGame = InGame or class()
InGame.init = function (self, level_handler)
	self._level_handler = level_handler
	self.game_state._is_in_game = true

	EventManager:trigger_event(Idstring("game_state_ingame"), nil)

	return 
end
InGame.destroy = function (self)
	self.game_state._is_in_game = nil

	return 
end
InGame.transition = function (self)
	if not self.game_state._front_end_requester:is_requested() then
		return 
	end

	if self.game_state._session_manager:_main_systems_are_stable_for_loading() then
		return CoreGameStatePrepareLoadingFrontEnd.PrepareLoadingFrontEnd, self._level_handler
	end

	return 
end
InGame.end_update = function (self, t, dt)
	self._level_handler:end_update(t, dt)

	return 
end

return 
