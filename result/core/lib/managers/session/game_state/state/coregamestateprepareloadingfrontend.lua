core:module("CoreGameStatePrepareLoadingFrontEnd")
core:import("CoreGameStateLoadingFrontEnd")

PrepareLoadingFrontEnd = PrepareLoadingFrontEnd or class()
PrepareLoadingFrontEnd.init = function (self, level_handler)
	self.game_state._is_preparing_for_loading_front_end = true

	self.game_state._front_end_requester:task_started()
	self.game_state:_set_stable_for_loading()

	self._level_handler = level_handler

	return 
end
PrepareLoadingFrontEnd.destroy = function (self)
	self.game_state._front_end_requester:task_completed()

	self.game_state._is_preparing_for_loading_front_end = false
	local local_user_manager = self.game_state._session_manager._local_user_manager

	self.game_state:player_slots():leave_level_handler(self._level_handler)
	local_user_manager.leave_level_handler(local_user_manager, self._level_handler)
	self._level_handler:destroy()

	return 
end
PrepareLoadingFrontEnd.transition = function (self)
	if self.game_state._session_manager:all_systems_are_stable_for_loading() then
		return CoreGameStateLoadingFrontEnd.LoadingFrontEnd
	end

	return 
end

return 
