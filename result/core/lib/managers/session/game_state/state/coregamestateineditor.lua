core:module("CoreGameStateInEditor")
core:import("CoreGameStateInEditorPrepareStartSimulation")

InEditor = InEditor or class()
InEditor.init = function (self)
	self.game_state._is_in_editor = true

	EventManager:trigger_event(Idstring("game_state_editor"), nil)

	return 
end
InEditor.destroy = function (self)
	self.game_state._is_in_editor = false

	return 
end
InEditor.transition = function (self)
	if not self.game_state._game_requester:is_requested() then
		return 
	end

	if self.game_state._session_manager:_main_systems_are_stable_for_loading() then
		return CoreGameStateInEditorPrepareStartSimulation.PrepareStartSimulation
	end

	return 
end

return 
