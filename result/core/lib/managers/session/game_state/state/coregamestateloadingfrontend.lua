core:module("CoreGameStateLoadingFrontEnd")
core:import("CoreGameStatePreFrontEnd")

LoadingFrontEnd = LoadingFrontEnd or class()
LoadingFrontEnd.init = function (self)
	self._debug_time = self.game_state._session_manager:_debug_time()

	for _, unit in ipairs(World:find_units_quick("all")) do
		unit.set_slot(unit, 0)
	end

	return 
end
LoadingFrontEnd.destroy = function (self)
	return 
end
LoadingFrontEnd.transition = function (self)
	local current_time = self.game_state._session_manager:_debug_time()

	if self._debug_time + 2 < current_time then
		return CoreGameStatePreFrontEnd.PreFrontEnd
	end

	return 
end

return 
