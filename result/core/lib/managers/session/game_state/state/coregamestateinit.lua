core:module("CoreGameStateInit")
core:import("CoreGameStateInEditor")
core:import("CoreGameStatePreFrontEnd")

Init = Init or class()
Init.init = function (self)
	self.game_state._is_in_init = true

	return 
end
Init.destroy = function (self)
	self.game_state._is_in_init = false

	return 
end
Init.transition = function (self)
	if Application:editor() then
		return CoreGameStateInEditor.InEditor
	else
		return CoreGameStatePreFrontEnd.PreFrontEnd
	end

	return 
end

return 
