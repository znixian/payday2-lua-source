core:module("CoreGameStateFrontEnd")
core:import("CoreGameStatePrepareLoadingGame")

FrontEnd = FrontEnd or class()
FrontEnd.init = function (self)
	self.game_state._is_in_front_end = true

	return 
end
FrontEnd.destroy = function (self)
	self.game_state._is_in_front_end = false

	return 
end
FrontEnd.transition = function (self)
	if not self.game_state._game_requester:is_requested() then
		return 
	end

	if self.game_state._session_manager:_main_systems_are_stable_for_loading() then
		return CoreGameStatePrepareLoadingGame.PrepareLoadingGame
	end

	return 
end

return 
