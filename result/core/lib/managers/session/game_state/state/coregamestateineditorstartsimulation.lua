core:module("CoreGameStateInEditorStartSimulation")
core:import("CoreGameStateInGame")
core:import("CoreGameStateLoadingGame")

StartSimulation = StartSimulation or class(CoreGameStateLoadingGame.LoadingGame)
StartSimulation.transition = function (self)
	return CoregameStateInEditorSimulation.Simulation
end

return 
