core:module("CoreGameStateInEditorStopSimulation")
core:import("CoreGameStateInEditor")

StopSimulation = StopSimulation or class()
StopSimulation.init = function (self)
	self.game_state._front_end_requester:task_started()

	return 
end
StopSimulation.destroy = function (self)
	self.game_state._front_end_requester:task_completed()

	return 
end
StopSimulation.transition = function (self)
	return CoreGameStateInEditor.InEditor
end

return 
