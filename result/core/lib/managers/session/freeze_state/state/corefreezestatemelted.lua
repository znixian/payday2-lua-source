core:module("CoreFreezeStateMelted")

Melted = Melted or class()
Melted.init = function (self)
	self.freeze_state:_set_stable_for_loading()

	return 
end
Melted.destroy = function (self)
	self.freeze_state:_not_stable_for_loading()

	return 
end
Melted.transition = function (self)
	return 
end

return 
