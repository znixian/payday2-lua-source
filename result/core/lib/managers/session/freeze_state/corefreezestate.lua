core:module("CoreFreezeState")
core:import("CoreFiniteStateMachine")
core:import("CoreFreezeStateMelted")
core:import("CoreSessionGenericState")

FreezeState = FreezeState or class(CoreSessionGenericState.State)
FreezeState.init = function (self)
	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreFreezeStateMelted.Melted, "freeze_state", self)

	return 
end
FreezeState.set_debug = function (self, debug_on)
	self._state:set_debug(debug_on)

	return 
end
FreezeState.default_data = function (data)
	data.start_state = "CoreFreezeStateMelted.Melted"

	return 
end
FreezeState.save = function (self, data)
	self._state:save(data.start_state)

	return 
end
FreezeState.transition = function (self)
	self._state:transition()

	return 
end

return 
