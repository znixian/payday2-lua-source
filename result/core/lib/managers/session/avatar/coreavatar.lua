core:module("CoreAvatar")

Avatar = Avatar or class()
Avatar.init = function (self, avatar_handler)
	self._avatar_handler = avatar_handler

	return 
end
Avatar.destroy = function (self)
	if self._input_input_provider then
		self.release_input(self)
	end

	self._avatar_handler:destroy()

	return 
end
Avatar.set_input = function (self, input_input_provider)
	self._avatar_handler:enable_input(input_input_provider)

	self._input_input_provider = input_input_provider

	return 
end
Avatar.release_input = function (self)
	self._avatar_handler:disable_input()

	self._input_input_provider = nil

	return 
end
Avatar.avatar_handler = function (self)
	return self._avatar_handler
end

return 
