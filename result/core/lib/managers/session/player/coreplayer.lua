core:module("CorePlayer")
core:import("CoreAvatar")

Player = Player or class()
Player.init = function (self, player_slot, player_handler)
	self._player_slot = player_slot
	self._player_handler = player_handler

	assert(self._player_handler)

	self._player_handler._core_player = self

	return 
end
Player.destroy = function (self)
	if self._level_handler then
		self.leave_level(self, self._level_handler)
	end

	if self._avatar then
		self._destroy_avatar(self)
	end

	self._player_handler:destroy()

	self._player_handler = nil

	return 
end
Player.avatar = function (self)
	return self._avatar
end
Player.has_avatar = function (self)
	return self._avatar ~= nil
end
Player.is_alive = function (self)
	return self._player_handler ~= nil
end
Player._destroy_avatar = function (self)
	self._player_handler:release_avatar()
	self._avatar:destroy()

	self._avatar = nil

	return 
end
Player.avatar_handler = function (self)
	return self._avatar_handler
end
Player.enter_level = function (self, level_handler)
	self._player_handler:enter_level(level_handler)

	local avatar_handler = self._player_handler:spawn_avatar()
	self._avatar = CoreAvatar.Avatar:new(avatar_handler)
	avatar_handler._core_avatar = self._avatar

	self._player_handler:set_avatar(avatar_handler)

	self._level_handler = level_handler

	return 
end
Player.leave_level = function (self, level_handler)
	if self._avatar then
		self._destroy_avatar(self)
	end

	self._player_handler:leave_level(level_handler)

	self._level_handler = nil

	return 
end
Player.player_slot = function (self)
	return self._player_slot
end
Player.set_leaderboard_position = function (self, position)
	self._leaderboard_position = position

	return 
end
Player.set_team = function (self, team)
	self._team = team

	return 
end

return 
