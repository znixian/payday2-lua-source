core:module("CoreSessionState")
core:import("CorePortableSessionCreator")
core:import("CorePlayerSlots")
core:import("CoreRequester")
core:import("CoreFiniteStateMachine")
core:import("CoreSessionStateInit")
core:import("CoreSessionInfo")
core:import("CoreSessionGenericState")

SessionState = SessionState or class(CoreSessionGenericState.State)
SessionState.init = function (self, factory, player_slots, game_state)
	self._factory = factory
	self._session_creator = CorePortableSessionCreator.Creator:new(self)
	self._join_session_requester = CoreRequester.Requester:new()
	self._quit_session_requester = CoreRequester.Requester:new()
	self._start_session_requester = CoreRequester.Requester:new()
	self._player_slots = player_slots
	self._game_state = game_state
	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreSessionStateInit.Init, "session_state", self)
	self._session_info = CoreSessionInfo.Info:new()

	self._set_stable_for_loading(self)

	return 
end
SessionState.transition = function (self)
	self._state:transition()

	return 
end
SessionState.player_slots = function (self)
	return self._player_slots
end
SessionState.join_standard_session = function (self)
	self._join_session_requester:request()

	return 
end
SessionState.start_session = function (self)
	self._state:state():start_session()

	return 
end
SessionState.quit_session = function (self)
	self._quit_session_requester:request()

	return 
end
SessionState.end_session = function (self)
	self._state:state():end_session()

	return 
end
SessionState.session_info = function (self)
	return self._session_info
end

return 
