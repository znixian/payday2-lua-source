core:module("CoreSessionStateInSessionEnd")
core:import("CoreSessionStateInit")

InSessionEnd = InSessionEnd or class()
InSessionEnd.init = function (self, session)
	assert(session)

	self._session = session

	self._session._session_handler:session_ended()

	return 
end
InSessionEnd.destroy = function (self)
	return 
end
InSessionEnd.transition = function (self)
	return CoreSessionStateInit
end

return 
