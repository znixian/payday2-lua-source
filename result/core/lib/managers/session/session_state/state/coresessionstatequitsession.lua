core:module("CoreSessionStateQuitSession")
core:import("CoreSessionStateInit")

QuitSession = QuitSession or class()
QuitSession.init = function (self, session)
	self._session = session

	self.session_state._quit_session_requester:task_started()
	self.session_state:player_slots():clear_session()
	self.session_state._game_state:request_front_end()
	self._session:delete_session()

	return 
end
QuitSession.destroy = function (self)
	self.session_state._quit_session_requester:task_completed()

	self.session_state._session = nil

	return 
end
QuitSession.transition = function (self)
	return CoreSessionStateInit.Init, self._session
end

return 
