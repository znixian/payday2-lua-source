core:module("CoreSessionStateFindSession")
core:import("CoreSessionStateCreateSession")
core:import("CoreSessionStateJoinSession")

FindSession = FindSession or class()
FindSession.init = function (self)
	self.session_state._session_creator:find_session(self.session_state._session_info, callback(self, self, "_sessions_found"))

	return 
end
FindSession.destroy = function (self)
	return 
end
FindSession._sessions_found = function (self, sessions)
	if not sessions then
		self._session_to_join = false
	end

	self._session_id_to_join = sessions[1].info

	return 
end
FindSession.transition = function (self)
	if self._session_id_to_join == false then
		return CoreSessionStateCreateSession.CreateSession
	elseif self._session_id_to_join ~= nil then
		return CoreSessionStateJoinSession.JoinSession, self._session_id_to_join
	end

	return 
end

return 
