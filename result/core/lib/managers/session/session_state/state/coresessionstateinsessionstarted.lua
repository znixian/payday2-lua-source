core:module("CoreSessionStateInSessionStarted")
core:import("CoreSessionStateQuitSession")
core:import("CoreSessionStateInSessionEnd")

InSessionStarted = InSessionStarted or class()
InSessionStarted.init = function (self, session)
	assert(session)

	self._session = session

	self._session._session_handler:session_started()

	return 
end
InSessionStarted.destroy = function (self)
	return 
end
InSessionStarted.transition = function (self)
	if self.session_state._quit_session_requester:is_requested() then
		return CoreSessionStateQuitSession.Quit, self._session
	end

	if self._end_session then
		return CoreSessionStateInSessionEnd.InSessionEnd, self._session
	end

	return 
end
InSessionStarted.end_session = function (self)
	self._end_session = true

	return 
end

return 
