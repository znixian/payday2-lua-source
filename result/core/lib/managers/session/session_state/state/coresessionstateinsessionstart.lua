core:module("CoreSessionStateInSession")
core:import("CoreSessionStateInSessionStarted")

InSessionStart = InSessionStart or class()
InSessionStart.init = function (self, session)
	assert(session)

	self._session = session

	return 
end
InSessionStart.destroy = function (self)
	return 
end
InSessionStart.transition = function (self)
	return CoreSessionStateInSessionStarted.Started, self._session
end

return 
