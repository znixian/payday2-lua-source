core:module("CoreSessionStateInSession")
core:import("CoreSessionStateQuitSession")

InSession = InSession or class()
InSession.init = function (self, session)
	assert(session)

	self._session = session

	self._session._session_handler:joined_session()
	self.session_state._game_state:request_game()
	self.session_state:player_slots():create_players()

	return 
end
InSession.destroy = function (self)
	self.session_state:player_slots():remove_players()

	return 
end
InSession.transition = function (self)
	if self._start_session then
		return CoreSessionStateInSessionStart, self._session
	end

	if self.session_state._quit_session_requester:is_requested() then
		return CoreSessionStateQuitSession.QuitSession, self._session
	end

	return 
end
InSession.start_session = function (self)
	self._start_session = true

	return 
end

return 
