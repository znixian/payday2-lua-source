core:module("CoreSessionStateInit")
core:import("CoreSessionStateFindSession")

Init = Init or class()
Init.init = function (self)
	assert(not self.session_state._quit_session_requester:is_requested())

	return 
end
Init.transition = function (self)
	if self.session_state._join_session_requester:is_requested() and self.session_state:player_slots():has_primary_local_user() then
		return CoreSessionStateFindSession.FindSession
	end

	return 
end

return 
