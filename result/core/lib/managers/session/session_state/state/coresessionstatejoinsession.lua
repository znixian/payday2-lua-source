core:module("CoreSessionStateJoinSession")
core:import("CoreSessionStateInSession")

JoinSession = JoinSession or class()
JoinSession.init = function (self, session_id)
	self.session_state._join_session_requester:task_started()

	self._session = self.session_state._session_creator:join_session(session_id)
	self._session._session_handler = self.session_state._factory:create_session_handler()
	self._session._session_handler._core_session_control = self.session_state

	return 
end
JoinSession.destroy = function (self)
	self.session_state._join_session_requester:task_completed()

	return 
end
JoinSession.transition = function (self)
	return CoreSessionStateInSession.InSession, self._session
end

return 
