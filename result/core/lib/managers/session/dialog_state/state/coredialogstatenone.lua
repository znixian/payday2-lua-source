core:module("CoreDialogStateNone")

None = None or class()
None.init = function (self)
	self.dialog_state:_set_stable_for_loading()

	return 
end
None.destroy = function (self)
	self.dialog_state._not_stable_for_loading()

	return 
end
None.transition = function (self)
	return 
end

return 
