core:module("CoreDialogState")
core:import("CoreFiniteStateMachine")
core:import("CoreDialogStateNone")
core:import("CoreSessionGenericState")

DialogState = DialogState or class(CoreSessionGenericState.State)
DialogState.init = function (self)
	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreDialogStateNone.None, "dialog_state", self)

	return 
end
DialogState.set_debug = function (self, debug_on)
	self._state:set_debug(debug_on)

	return 
end
DialogState.default_data = function (data)
	data.start_state = "CoreFreezeStateMelted.Melted"

	return 
end
DialogState.save = function (self, data)
	self._state:save(data.start_state)

	return 
end
DialogState.transition = function (self)
	self._state:transition()

	return 
end

return 
