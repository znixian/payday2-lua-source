core:module("CoreLocalUserManager")
core:import("CoreLocalUser")
core:import("CoreSessionGenericState")

Manager = Manager or class(CoreSessionGenericState.State)
Manager.init = function (self, factory, profile_settings_handler, profile_progress_handler, input_manager)
	self._factory = factory
	self._controller_to_user = {}
	self._profile_settings_handler = profile_settings_handler
	self._profile_progress_handler = profile_progress_handler
	self._profile_data_loaded_callback = profile_data_loaded_callback
	self._input_manager = input_manager

	return 
end
Manager.has_local_user_with_input_provider_id = function (self, controller)
	return self._controller_to_user[controller.key(controller)] ~= nil
end
Manager.debug_bind_primary_input_provider_id = function (self, player_slot)
	local count = Input:num_real_controllers()
	local best_controller = nil

	for i = 0, count, 1 do
		local controller = Input:controller(i)

		if controller.connected(controller) then
			local c_type = controller.type(controller)

			if c_type == "xbox_controller" then
				best_controller = controller

				break
			elseif best_controller == nil then
				best_controller = controller
			end
		end
	end

	return self.bind_local_user(self, player_slot, best_controller)
end
Manager.bind_local_user = function (self, slot, input_provider_id)
	local input_provider = self._input_manager:_create_input_provider_for_controller(input_provider_id)
	local local_user = self._controller_to_user[input_provider_id.key(input_provider_id)]

	if not local_user then
		local user_index = nil

		if input_provider_id.user_index then
			user_index = input_provider_id.user_index(input_provider_id)
		end

		local_user = self._create_local_user(self, input_provider, user_index)
		self._controller_to_user[input_provider_id.key(input_provider_id)] = local_user
	end

	slot.assign_local_user(slot, local_user)

	return local_user
end
Manager._create_local_user = function (self, input_provider, user_index)
	local local_user_handler = self._factory:create_local_user_handler()
	local created_user = CoreLocalUser.User:new(local_user_handler, input_provider, user_index, self._profile_settings_handler, self._profile_progress_handler, self._profile_data_loaded_callback)
	local_user_handler._core_local_user = created_user

	return created_user
end
Manager.transition = function (self)
	for _, user in pairs(self._controller_to_user) do
		user.transition(user)
	end

	return 
end
Manager.is_stable_for_loading = function (self)
	for _, user in pairs(self._controller_to_user) do
		if not user.is_stable_for_loading(user) then
			return false
		end
	end

	return true
end
Manager.users = function (self)
	return self._controller_to_user
end
Manager.update = function (self, t, dt)
	for _, user in pairs(self._controller_to_user) do
		user.update(user, t, dt)
	end

	return 
end
Manager.enter_level_handler = function (self, level_handler)
	for _, user in pairs(self._controller_to_user) do
		user.enter_level(user, level_handler)
	end

	return 
end
Manager.leave_level_handler = function (self, level_handler)
	for _, user in pairs(self._controller_to_user) do
		user.leave_level(user, level_handler)
	end

	return 
end

return 
