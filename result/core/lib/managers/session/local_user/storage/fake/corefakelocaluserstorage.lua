core:module("CoreFakeLocalUserStorage")
core:import("CoreLocalUserStorage")

Storage = Storage or class(CoreLocalUserStorage.Storage)
Storage.save = function (self)
	return 
end
Storage._start_load_task = function (self)
	self._load_started_time = TimerManager:game():time()

	return 
end
Storage._load_status = function (self)
	local current_time = TimerManager:game():time()

	if self._load_started_time + 0.8 < current_time then
		self._create_profile_data(self)

		return SaveData.OK
	end

	return 
end
Storage._close_load_task = function (self)
	self._load_started_time = nil

	return 
end

return 
