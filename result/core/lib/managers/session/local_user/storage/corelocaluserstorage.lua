core:module("CoreLocalUserStorage")
core:import("CoreRequester")
core:import("CoreFiniteStateMachine")
core:import("CoreLocalUserStorageStates")
core:import("CoreSessionGenericState")

Storage = Storage or class(CoreSessionGenericState.State)
Storage.init = function (self, local_user, settings_handler, progress_handler, profile_data_loaded_callback)
	self._load = CoreRequester.Requester:new()
	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreLocalUserStorageStates.Init, "storage", self)
	self._local_user = local_user
	self._user_index = self._local_user._user_index
	self._settings_handler = settings_handler
	self._progress_handler = progress_handler
	self._profile_data_loaded_callback = profile_data_loaded_callback

	return 
end
Storage.transition = function (self)
	self._state:transition()

	return 
end
Storage.request_load = function (self)
	self._load:request()

	return 
end
Storage.request_save = function (self)
	return 
end
Storage._common_save_params = function (self)
	return {
		preview = false,
		save_slots = {
			1
		},
		user_index = self._user_index
	}
end
Storage._start_load_task = function (self)
	local save_param = self._common_save_params(self)
	local callback = nil
	self._load_task = NewSave:load(save_param, callback)

	self._load:task_started()

	return 
end
Storage._load_status = function (self)
	if not self._load_task:has_next() then
		return 
	end

	local profile_data = self._load_task:next()

	if profile_data.status(profile_data) == SaveData.OK then
		self._profile_data_loaded(self, profile_data.information(profile_data))
	end

	return profile_data.status(profile_data)
end
Storage._close_load_task = function (self)
	self._load_task = nil

	self._load:task_completed()

	return 
end
Storage._fast_forward_profile_data = function (self, handler, profile_data, stored_version)
	local func = nil

	repeat
		local function_name = "convert_from_" .. tostring(stored_version) .. "_to_" .. tostring(stored_version + 1)
		func = handler[function_name]

		if func ~= nil then
			profile_data = func(handler, profile_data)
			stored_version = stored_version + 1
		end
	until func == nil

	return profile_data, stored_version
end
Storage._profile_data_loaded = function (self, profile_data)
	profile_data.settings, profile_data.settings.version = self._fast_forward_profile_data(self, self._settings_handler, profile_data.settings.title_data, profile_data.settings.version)
	profile_data.progress, profile_data.progress.version = self._fast_forward_profile_data(self, self._progress_handler, profile_data.progress.title_data, profile_data.progress.version)
	self._profile_data = profile_data

	self._local_user:local_user_handler():profile_data_loaded()

	return 
end
Storage.profile_data_is_loaded = function (self)
	return self._profile_data ~= nil
end
Storage._create_profile_data = function (self)
	local profile_data = {
		settings = {}
	}
	profile_data.settings.title_data = {}
	profile_data.settings.version = 0
	profile_data.progress = {
		title_data = {},
		version = 0
	}

	self._profile_data_loaded(self, profile_data)

	return 
end
Storage.profile_settings = function (self)
	return self._profile_data.settings.title_data
end
Storage.profile_progress = function (self)
	return self._profile_data.progress.title_data
end

return 
