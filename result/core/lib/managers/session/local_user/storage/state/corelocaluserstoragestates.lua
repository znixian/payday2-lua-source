core:module("CoreLocalUserStorageStates")

Init = Init or class()
Init.transition = function (self)
	if self.storage._load:is_requested() then
		return Loading
	end

	return 
end
DetectSignOut = DetectSignOut or class()
DetectSignOut.init = function (self)
	return 
end
Loading = Loading or class(DetectSignOut)
Loading.init = function (self)
	DetectSignOut.init(self)
	self.storage:_start_load_task()

	return 
end
Loading.destroy = function (self)
	self.storage:_close_load_task()

	return 
end
Loading.transition = function (self)
	local status = self.storage:_load_status()

	if not status then
		return 
	end

	if status == SaveData.OK then
		return Ready
	elseif status == SaveData.FILE_NOT_FOUND then
		return NoSaveGameFound
	else
		return LoadError
	end

	return 
end
Ready = Ready or class()
Ready.init = function (self)
	self.storage:_set_stable_for_loading()

	return 
end
Ready.destroy = function (self)
	self.storage:_not_stable_for_loading()

	return 
end
Ready.transition = function (self)
	return 
end
NoSaveGameFound = NoSaveGameFound or class()
NoSaveGameFound.init = function (self)
	self.storage:_set_stable_for_loading()

	return 
end
NoSaveGameFound.transition = function (self)
	self.storage:_not_stable_for_loading()

	return 
end
LoadError = LoadError or class()
LoadError.transition = function (self)
	return 
end

return 
