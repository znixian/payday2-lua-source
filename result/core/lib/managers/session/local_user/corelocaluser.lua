core:module("CoreLocalUser")
core:import("CorePortableLocalUserStorage")
core:import("CoreSessionGenericState")

User = User or class(CoreSessionGenericState.State)
User.init = function (self, local_user_handler, input_input_provider, user_index, profile_settings_handler, profile_progress_handler)
	self._local_user_handler = local_user_handler
	self._input_input_provider = input_input_provider
	self._user_index = user_index
	self._storage = CorePortableLocalUserStorage.Storage:new(self, profile_settings_handler, profile_progress_handler, profile_data_loaded_callback)
	self._game_name = "Player #" .. tostring(self._user_index)

	return 
end
User.default_data = function (data)
	return 
end
User.save = function (self, data)
	return 
end
User.transition = function (self)
	self._storage:transition()

	return 
end
User._player_slot_assigned = function (self, player_slot)
	assert(self._player_slot == nil, "This user already has an assigned player slot")

	self._player_slot = player_slot

	self._storage:request_load()

	return 
end
User._player_slot_lost = function (self, player_slot)
	assert(self._player_slot ~= nil, "This user can get a lost player slot, no slot was assigned to begin with")
	assert(self._player_slot == player_slot, "Player has lost a player slot that wasn't assigned")

	self._player_slot = nil

	return 
end
User.profile_data_is_loaded = function (self)
	return self._storage:profile_data_is_loaded()
end
User.enter_level = function (self, level_handler)
	self._local_user_handler:enter_level(level_handler)

	return 
end
User.leave_level = function (self, level_handler)
	self._local_user_handler:leave_level(level_handler)
	self.release_player(self)

	return 
end
User.gamer_name = function (self)
	return self._game_name
end
User.is_stable_for_loading = function (self)
	return self._storage:is_stable_for_loading()
end
User.assign_player = function (self, player)
	self._player = player

	self._local_user_handler:player_assigned(self)

	return 
end
User.release_player = function (self)
	self._local_user_handler:player_removed()

	self._player = nil
	self._avatar = nil

	return 
end
User.assigned_player = function (self)
	return self._player
end
User.local_user_handler = function (self)
	return self._local_user_handler
end
User.profile_settings = function (self)
	return self._storage:profile_settings()
end
User.profile_progress = function (self)
	return self._storage:profile_progress()
end
User.save_profile_settings = function (self)
	return self._storage:request_save()
end
User.save_profile_progress = function (self)
	return self._storage:request_save()
end
User.engine_input_input_input_provider = function (self)
	return self._input_input_provider
end
User.update = function (self, t, dt)
	if not self._avatar and self._player and self._player:has_avatar() then
		local input_input_provider = self.engine_input_input_input_provider(self)
		local avatar = self._player:avatar()

		avatar.set_input(avatar, input_input_provider)

		self._avatar = avatar
	end

	self._local_user_handler:update(t, dt)

	return 
end

return 
