core:module("CoreMenuState")
core:import("CoreSessionGenericState")
core:import("CoreFiniteStateMachine")
core:import("CoreMenuStateNone")

MenuState = MenuState or class(CoreSessionGenericState.State)
MenuState.init = function (self, game_state, menu_handler, player_slots)
	self._game_state = game_state

	assert(self._game_state)

	self._menu_handler = menu_handler
	self._player_slots = player_slots
	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreMenuStateNone.None, "menu_state", self)

	return 
end
MenuState.set_debug = function (self, debug_on)
	self._state:set_debug(debug_on)

	return 
end
MenuState.default_data = function (data)
	data.start_state = "CoreMenuStateNone.None"

	return 
end
MenuState.save = function (self, data)
	self._state:save(data.start_state)

	return 
end
MenuState.transition = function (self)
	self._state:transition()

	return 
end
MenuState.game_state = function (self)
	return self._game_state
end

return 
