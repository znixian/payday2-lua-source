core:module("CoreMenuStateNone")
core:import("CoreMenuStatePreFrontEndOnce")
core:import("CoreMenuStateInEditor")

None = None or class()
None.transition = function (self)
	local state = self.menu_state._game_state

	if not state.is_in_init(state) then
		if state.is_in_editor(state) then
			return CoreMenuStateInEditor.InEditor
		else
			return CoreMenuStatePreFrontEndOnce.PreFrontEndOnce
		end
	end

	return 
end

return 
