core:module("CoreMenuStatePreFrontEnd")
core:import("CoreMenuStateFrontEnd")
core:import("CoreMenuStateStart")
core:import("CoreFiniteStateMachine")

PreFrontEnd = PreFrontEnd or class()
PreFrontEnd.init = function (self)
	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreMenuStateStart.Start, "pre_front_end", self)

	return 
end
PreFrontEnd.destroy = function (self)
	self._state:destroy()

	return 
end
PreFrontEnd.transition = function (self)
	self._state:transition()

	local state = self.menu_state._game_state

	if not state.is_in_pre_front_end(state) then
		return CoreMenuStateFrontEnd.FrontEnd
	end

	return 
end

return 
