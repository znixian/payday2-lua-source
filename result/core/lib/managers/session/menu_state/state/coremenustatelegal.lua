core:module("CoreMenuStateLegal")
core:import("CoreMenuStateIntroScreens")

Legal = Legal or class()
Legal.init = function (self)
	self._start_time = TimerManager:game():time()

	return 
end
Legal.transition = function (self)
	local current_time = TimerManager:game():time()
	local time_until_intro_screens = 1

	if self._start_time + time_until_intro_screens <= current_time then
		return CoreMenuStateIntroScreens.IntroScreens
	end

	return 
end

return 
