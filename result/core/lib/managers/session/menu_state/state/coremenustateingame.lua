core:module("CoreMenuStateInGame")
core:import("CoreMenuStatePrepareLoadingFrontEnd")

InGame = InGame or class()
InGame.transition = function (self)
	local game_state = self.menu_state._game_state

	if game_state.is_preparing_for_loading_front_end(game_state) then
		return CoreMenuStatePrepareLoadingFrontEnd.PrepareLoadingFrontEnd
	end

	return 
end

return 
