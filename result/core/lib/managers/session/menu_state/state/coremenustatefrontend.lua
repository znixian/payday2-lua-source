core:module("CoreMenuStateFrontEnd")
core:import("CoreMenuStatePrepareLoadingGame")
core:import("CoreMenuStatePreFrontEnd")

FrontEnd = FrontEnd or class()
FrontEnd.init = function (self)
	local menu_handler = self.menu_state._menu_handler

	menu_handler.front_end(menu_handler)

	return 
end
FrontEnd.transition = function (self)
	local game_state = self.menu_state._game_state

	if game_state.is_preparing_for_loading_game(game_state) then
		return CoreMenuStatePrepareLoadingGame.PrepareLoadingGame
	elseif game_state.is_in_pre_front_end(game_state) then
		return CoreMenuStatePreFrontEnd.PreFrontEnd
	end

	return 
end

return 
