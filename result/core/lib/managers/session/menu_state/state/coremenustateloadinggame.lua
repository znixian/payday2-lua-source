core:module("CoreMenuStateLoadingGame")
core:import("CoreMenuStateInGame")
core:import("CoreMenuStateStopLoadingGame")

LoadingGame = LoadingGame or class()
LoadingGame.init = function (self)
	self.menu_state:_set_stable_for_loading()

	local menu_handler = self.menu_state._menu_handler

	menu_handler.start_loading_game_environment(menu_handler)

	return 
end
LoadingGame.destroy = function (self)
	self.menu_state:_not_stable_for_loading()

	return 
end
LoadingGame.transition = function (self)
	local game_state = self.menu_state._game_state

	if game_state.is_in_game(game_state) then
		return CoreMenuStateStopLoadingGame.StopLoadingGame
	end

	return 
end

return 
