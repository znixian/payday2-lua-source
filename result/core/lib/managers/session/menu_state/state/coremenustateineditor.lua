core:module("CoreMenuStateInEditor")
core:import("CoreMenuStateInGame")

InEditor = InEditor or class(CoreMenuStateInGame.InGame)
InEditor.init = function (self)
	self.menu_state:_set_stable_for_loading()

	return 
end
InEditor.destroy = function (self)
	self.menu_state:_not_stable_for_loading()

	return 
end

return 
