core:module("CoreMenuStateStart")
core:import("CoreMenuStateAttract")

Start = Start or class()
Start.init = function (self)
	self._start_time = TimerManager:game():time()
	local player_slots = self.pre_front_end.menu_state._player_slots
	self._primary_slot = player_slots.primary_slot(player_slots)

	self._primary_slot:request_local_user_binding()

	local menu_handler = self.pre_front_end.menu_state._menu_handler

	menu_handler.start(menu_handler)

	return 
end
Start.destroy = function (self)
	self._primary_slot:stop_local_user_binding()

	return 
end
Start.transition = function (self)
	local current_time = TimerManager:game():time()
	local time_until_attract = 15

	if self._start_time + time_until_attract <= current_time then
		return CoreMenuStateAttract.Attract
	end

	return 
end

return 
