core:module("CoreMenuStateLoadingFrontEnd")
core:import("CoreMenuStateFrontEnd")
core:import("CoreMenuStateStopLoadingFrontEnd")

LoadingFrontEnd = LoadingFrontEnd or class()
LoadingFrontEnd.init = function (self)
	self.menu_state:_set_stable_for_loading()

	local menu_handler = self.menu_state._menu_handler

	menu_handler.start_loading_front_end_environment(menu_handler)

	return 
end
LoadingFrontEnd.destroy = function (self)
	self.menu_state:_not_stable_for_loading()

	return 
end
LoadingFrontEnd.transition = function (self)
	local game_state = self.menu_state._game_state

	if game_state.is_in_front_end(game_state) or game_state.is_in_pre_front_end(game_state) then
		return CoreMenuStateStopLoadingFrontEnd.StopLoadingFrontEnd
	end

	return 
end

return 
