core:module("CoreMenuStatePrepareLoadingFrontEnd")
core:import("CoreSessionResponse")
core:import("CoreMenuStatePreFrontEnd")
core:import("CoreMenuStateLoadingFrontEnd")

PrepareLoadingFrontEnd = PrepareLoadingFrontEnd or class()
PrepareLoadingFrontEnd.init = function (self)
	self._response = CoreSessionResponse.Done:new()
	local menu_handler = self.menu_state._menu_handler

	menu_handler.prepare_loading_front_end(menu_handler, self._response)

	return 
end
PrepareLoadingFrontEnd.destroy = function (self)
	self._response:destroy()

	return 
end
PrepareLoadingFrontEnd.transition = function (self)
	if self._response:is_done() then
		return CoreMenuStateLoadingFrontEnd.LoadingFrontEnd
	end

	return 
end

return 
