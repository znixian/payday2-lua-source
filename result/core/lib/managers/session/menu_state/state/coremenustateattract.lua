core:module("CoreMenuStateAttract")
core:import("CoreMenuStateStart")
core:import("CoreSessionResponse")

Attract = Attract or class()
Attract.init = function (self)
	local menu_handler = self.pre_front_end.menu_state._menu_handler
	self._response = CoreSessionResponse.Done:new()

	menu_handler.attract(menu_handler, self._response)

	return 
end
Attract.destroy = function (self)
	self._response:destroy()

	return 
end
Attract.transition = function (self)
	if self._response:is_done() or Input:any_input() then
		return CoreMenuStateStart.Start
	end

	return 
end

return 
