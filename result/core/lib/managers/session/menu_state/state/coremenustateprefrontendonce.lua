core:module("CoreMenuStatePreFrontEndOnce")
core:import("CoreMenuStatePreFrontEnd")
core:import("CoreFiniteStateMachine")
core:import("CoreMenuStateLegal")

PreFrontEndOnce = PreFrontEndOnce or class()
PreFrontEndOnce.init = function (self)
	self._state = CoreFiniteStateMachine.FiniteStateMachine:new(CoreMenuStateLegal.Legal, "pre_front_end_once", self)

	return 
end
PreFrontEndOnce.transition = function (self)
	self._state:transition()

	local state = self.menu_state._game_state

	if not state.is_in_pre_front_end(state) or self.intro_screens_done then
		return CoreMenuStatePreFrontEnd.PreFrontEnd
	end

	return 
end

return 
