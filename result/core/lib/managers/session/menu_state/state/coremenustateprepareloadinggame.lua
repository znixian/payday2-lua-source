core:module("CoreMenuStatePrepareLoadingGame")
core:import("CoreSessionResponse")
core:import("CoreMenuStateInGame")
core:import("CoreMenuStateLoadingGame")

PrepareLoadingGame = PrepareLoadingGame or class()
PrepareLoadingGame.init = function (self)
	self._response = CoreSessionResponse.Done:new()
	local menu_handler = self.menu_state._menu_handler

	menu_handler.prepare_loading_game(menu_handler, self._response)

	return 
end
PrepareLoadingGame.destroy = function (self)
	self._response:destroy()

	return 
end
PrepareLoadingGame.transition = function (self)
	if self._response:is_done() then
		return CoreMenuStateLoadingGame.LoadingGame
	end

	return 
end

return 
