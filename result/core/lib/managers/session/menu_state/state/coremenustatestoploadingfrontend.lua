core:module("CoreMenuStateStopLoadingFrontEnd")
core:import("CoreSessionResponse")
core:import("CoreMenuStatePreFrontEnd")
core:import("CoreMenuStateFrontEnd")

StopLoadingFrontEnd = StopLoadingFrontEnd or class()
StopLoadingFrontEnd.init = function (self)
	local menu_handler = self.menu_state._menu_handler
	self._response = CoreSessionResponse.Done:new()

	menu_handler.stop_loading_front_end_environment(menu_handler, self._response)

	return 
end
StopLoadingFrontEnd.destroy = function (self)
	self._response:destroy()

	return 
end
StopLoadingFrontEnd.transition = function (self)
	if not self._response:is_done() then
		return 
	end

	return CoreMenuStatePreFrontEnd.PreFrontEnd
end

return 
