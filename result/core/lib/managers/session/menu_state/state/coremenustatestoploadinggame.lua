core:module("CoreMenuStateStopLoadingGame")
core:import("CoreSessionResponse")
core:import("CoreMenuStatePreFrontEnd")
core:import("CoreMenuStateInGame")

StopLoadingGame = StopLoadingGame or class()
StopLoadingGame.init = function (self)
	local menu_handler = self.menu_state._menu_handler
	self._response = CoreSessionResponse.Done:new()

	menu_handler.stop_loading_game_environment(menu_handler, self._response)

	return 
end
StopLoadingGame.destroy = function (self)
	self._response:destroy()

	return 
end
StopLoadingGame.transition = function (self)
	if not self._response:is_done() then
		return 
	end

	return CoreMenuStateInGame.InGame
end

return 
