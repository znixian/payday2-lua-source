core:module("CoreMenuStateIntroScreens")
core:import("CoreSessionResponse")

IntroScreens = IntroScreens or class()
IntroScreens.init = function (self)
	self._response = CoreSessionResponse.DoneOrFinished:new()

	self.pre_front_end_once.menu_state._menu_handler:show_next_intro_screen(self._response)

	return 
end
IntroScreens.destroy = function (self)
	self._response:destroy()

	return 
end
IntroScreens.transition = function (self)
	if self._response:is_finished() then
		self.pre_front_end_once.intro_screens_done = true
	elseif self._response:is_done() or Input:any_input() then
		return IntroScreens
	end

	return 
end

return 
