core:module("CoreFakeSessionCreator")
core:import("CoreFakeSession")

Creator = Creator or class()
Creator.init = function (self)
	return 
end
Creator.create_session = function (self, session_info, player_slots)
	if session_info.is_ranked(session_info) then
		cat_print("debug", "create_session: is_ranked")
	end

	if session_info.can_join_in_progress(session_info) then
		cat_print("debug", "create_session: is_ranked")
	end

	return CoreFakeSession.Session:new()
end
Creator.join_session = function (self, session_id)
	return CoreFakeSession.Session:new()
end
Creator.find_session = function (self, session_info, callback)
	local fake_sessions = {
		{
			info = 2
		},
		{
			info = 3
		}
	}

	callback(fake_sessions)

	return 
end

return 
