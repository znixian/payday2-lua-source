core:module("CoreFakeSession")
core:import("CoreSession")

Session = Session or class()
Session.init = function (self)
	return 
end
Session.delete_session = function (self)
	cat_print("debug", "FakeSession: delete_session")

	return 
end
Session.start_session = function (self)
	cat_print("debug", "FakeSession: start_session")

	return 
end
Session.end_session = function (self)
	cat_print("debug", "FakeSession: end_session")

	return 
end
Session.join_local_user = function (self, local_user)
	cat_print("debug", "FakeSession: Local user:'" .. local_user.gamer_name(local_user) .. "' joined!")

	return 
end
Session.join_remote_user = function (self, remote_user)
	cat_print("debug", "FakeSession: Remote user:'" .. remote_user.gamer_name(remote_user) .. "' joined!")

	return 
end

return 
