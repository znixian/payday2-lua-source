core:module("CoreSessionResponse")

Done = Done or class()
Done.DONE = 1
Done.done = function (self)
	self._set_response(self, Done.DONE)

	return 
end
Done._is_response_value = function (self, value)
	assert(not self._is_destroyed, "You can not check a destroyed response object!")

	return self._response == value
end
Done.is_done = function (self)
	return self._is_response_value(self, Done.DONE)
end
Done._set_response = function (self, value)
	assert(not self._is_destroyed, "You can not respond to a destroyed response object!")
	assert(self._response == nil, "Response has already been set!")

	self._response = value

	return 
end
Done.destroy = function (self)
	self._is_destroyed = true

	return 
end
DoneOrFinished = DoneOrFinished or class(Done)
DoneOrFinished.FINISHED = 2
DoneOrFinished.finished = function (self)
	self._set_response(self, DoneOrFinished.FINISHED)

	return 
end
DoneOrFinished.is_finished = function (self)
	return self._is_response_value(self, DoneOrFinished.FINISHED)
end

return 
