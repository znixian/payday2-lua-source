core:module("CoreSessionGenericState")

State = State or class()
State.init = function (self)
	return 
end
State._set_stable_for_loading = function (self)
	self._is_stable_for_loading = true

	return 
end
State._not_stable_for_loading = function (self)
	self._is_stable_for_loading = nil

	return 
end
State.is_stable_for_loading = function (self)
	return self._is_stable_for_loading ~= nil
end
State.transition = function (self)
	assert(false, "you must override transition()")

	return 
end

return 
