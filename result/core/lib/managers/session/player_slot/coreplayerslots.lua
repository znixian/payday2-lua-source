core:module("CorePlayerSlots")
core:import("CorePlayerSlot")
core:import("CoreSessionGenericState")

PlayerSlots = PlayerSlots or class(CoreSessionGenericState.State)
PlayerSlots.init = function (self, local_user_manager, factory)
	self._slots = {}
	self._local_user_manager = local_user_manager

	self._set_stable_for_loading(self)

	self._factory = factory

	return 
end
PlayerSlots.clear = function (self)
	self._slots = {}

	return 
end
PlayerSlots.clear_session = function (self)
	for _, slot in pairs(self._slots) do
		slot.clear_session(slot)
	end

	return 
end
PlayerSlots.add_player_slot = function (self)
	local new_index = #self._slots + 1
	local new_slot = CorePlayerSlot.PlayerSlot:new(self, self._local_user_manager)
	self._slots[new_index] = new_slot

	return new_slot
end
PlayerSlots._remove_player_slot = function (self, player_slot)
	for index, slot in pairs(self._slots) do
		if slot == player_slot then
			self._slots[index] = nil

			return 
		end
	end

	assert(false, "couldn't find that player slot")

	return 
end
PlayerSlots.slots = function (self)
	return self._slots
end
PlayerSlots.transition = function (self)
	for _, slot in pairs(self._slots) do
		slot.transition(slot)
	end

	return 
end
PlayerSlots.primary_slot = function (self)
	local primary_slot = self._slots[1]

	assert(primary_slot, "No primary slot defined")

	return primary_slot
end
PlayerSlots.has_primary_local_user = function (self)
	local primary_slot = self._slots[1]

	return primary_slot ~= nil and primary_slot.has_assigned_user(primary_slot)
end
PlayerSlots.primary_local_user = function (self)
	local primary_slot = self._slots[1]

	assert(primary_slot, "No primary slot defined")
	assert(primary_slot.has_assigned_user(primary_slot), "No user assigned to primary slot")

	return primary_slot.assigned_user(primary_slot)
end
PlayerSlots.create_players = function (self)
	for index, slot in pairs(self._slots) do
		if slot.has_assigned_user(slot) then
			slot.create_player(slot)
		end
	end

	return 
end
PlayerSlots.remove_players = function (self)
	for index, slot in pairs(self._slots) do
		if slot.has_player(slot) then
			slot.remove_player(slot)
		end
	end

	return 
end
PlayerSlots.enter_level_handler = function (self, level_handler)
	for index, slot in pairs(self._slots) do
		local player = slot.player(slot)

		if player then
			player.enter_level(player, level_handler)
		end
	end

	return 
end
PlayerSlots.leave_level_handler = function (self, level_handler)
	for index, slot in pairs(self._slots) do
		local player = slot.player(slot)

		if player then
			player.leave_level(player, level_handler)
		end
	end

	return 
end

return 
