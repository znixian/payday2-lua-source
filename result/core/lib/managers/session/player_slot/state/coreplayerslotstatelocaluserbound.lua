core:module("CorePlayerSlotStateLocalUserBound")
core:import("CorePlayerSlotStateInit")

LocalUserBound = LocalUserBound or class()
LocalUserBound.init = function (self, local_user)
	self._local_user = local_user

	return 
end
LocalUserBound.destroy = function (self)
	return 
end
LocalUserBound.transition = function (self)
	if self.player_slot._init:is_requested() then
		return CorePlayerSlotStateInit.Init
	end

	return 
end

return 
