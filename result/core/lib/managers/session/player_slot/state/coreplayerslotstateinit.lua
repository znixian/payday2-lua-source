core:module("CorePlayerSlotStateInit")
core:import("CorePlayerSlotStateDetectLocalUser")
core:import("CorePlayerSlotStateLocalUserDebugBind")

Init = Init or class()
Init.init = function (self)
	self.player_slot._init:task_started()

	return 
end
Init.destroy = function (self)
	self.player_slot._init:task_completed()

	return 
end
Init.transition = function (self)
	if self.player_slot._perform_debug_local_user_binding:is_requested() then
		return CorePlayerSlotStateLocalUserDebugBind.LocalUserDebugBind
	elseif self.player_slot._perform_local_user_binding:is_requested() then
		return CorePlayerSlotStateDetectLocalUser.DetectLocalUser
	end

	return 
end

return 
