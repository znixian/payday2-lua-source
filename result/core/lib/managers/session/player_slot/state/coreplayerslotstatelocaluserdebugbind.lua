core:module("CorePlayerSlotStateLocalUserDebugBind")
core:import("CorePlayerSlotStateLocalUserBound")

LocalUserDebugBind = UserDebugBind or class()
LocalUserDebugBind.init = function (self)
	self.player_slot._local_user_manager:debug_bind_primary_input_provider_id(self.player_slot)

	return 
end
LocalUserDebugBind.transition = function (self)
	return CorePlayerSlotStateLocalUserBound.LocalUserBound, self.player_slot:assigned_user()
end

return 
