core:module("CorePlayerSlot")
core:import("CoreRequester")
core:import("CoreFiniteStateMachine")
core:import("CorePlayerSlotStateInit")
core:import("CorePlayer")

PlayerSlot = PlayerSlot or class()
PlayerSlot.init = function (self, player_slots_parent, local_user_manager)
	self._perform_local_user_binding = CoreRequester.Requester:new()
	self._perform_debug_local_user_binding = CoreRequester.Requester:new()
	self._init = CoreRequester.Requester:new()
	self._user_state = CoreFiniteStateMachine.FiniteStateMachine:new(CorePlayerSlotStateInit.Init, "player_slot", self)
	self._player_slots_parent = player_slots_parent
	self._local_user_manager = local_user_manager

	return 
end
PlayerSlot.destroy = function (self)
	self._player_slots_parent:_remove_player_slot(self)

	if self._player then
		self._player:destroy()
	end

	return 
end
PlayerSlot.clear_session = function (self)
	if self._player then
		self._player:destroy()

		self._player = nil
	end

	return 
end
PlayerSlot.remove = function (self)
	self.destroy(self)

	return 
end
PlayerSlot._release_user_from_slot = function (self)
	if self._assigned_user then
		self._assigned_user:_player_slot_lost(self)
	end

	self._assigned_user = nil

	self._init:request()

	return 
end
PlayerSlot.request_local_user_binding = function (self)
	self._perform_local_user_binding:request()

	return 
end
PlayerSlot.stop_local_user_binding = function (self)
	self._perform_local_user_binding:cancel_request()

	return 
end
PlayerSlot.request_debug_local_user_binding = function (self)
	self._perform_debug_local_user_binding:request()

	return 
end
PlayerSlot.has_assigned_user = function (self)
	return self._assigned_user ~= nil
end
PlayerSlot.assigned_user = function (self)
	return self._assigned_user
end
PlayerSlot.assign_local_user = function (self, local_user)
	assert(local_user, "Must specify a valid user")
	assert(self._assigned_user == nil, "A user has already been assigned to this slot")

	self._assigned_user = local_user

	self._assigned_user:_player_slot_assigned(self)

	return 
end
PlayerSlot.transition = function (self)
	self._user_state:transition()

	return 
end
PlayerSlot.create_player = function (self)
	assert(self._player == nil, "Player already created for this slot")

	local factory = self._player_slots_parent._factory
	local player_handler = factory.create_player_handler(factory)
	self._player = CorePlayer.Player:new(self, player_handler)
	player_handler.core_player = self._player

	if self._assigned_user then
		self._assigned_user:assign_player(self._player)
	end

	return 
end
PlayerSlot.remove_player = function (self)
	if self._assigned_user then
		self._assigned_user:release_player(self._player)
	end

	self._player:destroy()

	self._player = nil

	return 
end
PlayerSlot.has_player = function (self)
	return self._player ~= nil
end
PlayerSlot.player = function (self)
	return self._player
end

return 
