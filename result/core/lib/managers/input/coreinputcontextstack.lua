core:module("CoreInputContextStack")
core:import("CoreStack")

Stack = Stack or class()
Stack.init = function (self, device_type)
	self._device_type = device_type
	self._active_input_context = CoreStack.Stack:new()

	return 
end
Stack.destroy = function (self)
	self._active_input_context:destroy()

	return 
end
Stack.active_device_layout = function (self)
	local target_context = self._active_input_context:top()

	return target_context.device_layout(target_context, self._device_type)
end
Stack.active_context = function (self)
	if self._active_input_context:is_empty() then
		return 
	end

	return self._active_input_context:top()
end
Stack.pop_input_context = function (self, input_context)
	assert(self._active_input_context:top() == input_context)
	self._active_input_context:pop()

	return 
end
Stack.push_input_context = function (self, input_context)
	self._active_input_context:push(input_context)

	return 
end

return 
