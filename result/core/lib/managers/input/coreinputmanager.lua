core:module("CoreInputManager")
core:import("CoreInputContextFeeder")
core:import("CoreInputSettingsReader")

InputManager = InputManager or class()
InputManager.init = function (self)
	local settings_reader = CoreInputSettingsReader.SettingsReader:new()
	self._layer_descriptions = settings_reader.layer_descriptions(settings_reader)
	self._feeders = {}
	self._input_provider_to_feeder = {}

	return 
end
InputManager.destroy = function (self)
	return 
end
InputManager.update = function (self, t, dt)
	for _, feeder in pairs(self._feeders) do
		feeder.update(feeder)
	end

	return 
end
InputManager.input_provider_id_that_presses_start = function (self)
	local layer_description_ids = {}
	local count = Input:num_real_controllers()

	for i = 1, count, 1 do
		local controller = Input:controller(i)

		if controller.connected(controller) and controller.pressed(controller, Idstring("start")) then
			table.insert(layer_description_ids, controller)
		end
	end

	return layer_description_ids
end
InputManager.debug_primary_input_provider_id = function (self)
	local count = Input:num_real_controllers()
	local best_controller = nil

	for i = 1, count, 1 do
		local controller = Input:controller(i)

		if controller.connected(controller) then
			if controller.type(controller) == "xbox360" then
				best_controller = controller

				break
			elseif best_controller == nil then
				best_controller = controller
			end
		end
	end

	assert(best_controller, "You need at least one compatible controller plugged in")

	return best_controller
end
InputManager._create_input_provider_for_controller = function (self, engine_controller)
	local feeder = CoreInputContextFeeder.Feeder:new(engine_controller, self._layer_descriptions)
	local input_provider = feeder.input_provider(feeder)
	self._input_provider_to_feeder[input_provider] = feeder
	self._feeders[feeder] = feeder

	return input_provider
end
InputManager._destroy_input_provider = function (self, input_provider)
	local feeder = self._input_provider_to_feeder[input_provider]
	self._feeders[feeder] = nil

	return 
end

return 
