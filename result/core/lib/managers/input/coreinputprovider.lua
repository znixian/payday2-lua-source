core:module("CoreInputProvider")
core:import("CoreInputLayerDescriptionPrioritizer")
core:import("CoreInputLayer")

Provider = Provider or class()
Provider.init = function (self, input_layer_descriptions)
	self._layer_description_to_layer = {}
	self._input_layer_descriptions = input_layer_descriptions
	self._prioritizer = CoreInputLayerDescriptionPrioritizer.Prioritizer:new()

	return 
end
Provider.destroy = function (self)
	return 
end
Provider.context = function (self)
	local layer_description = self._prioritizer:active_layer_description()

	if not layer_description then
		return 
	end

	local layer = self._layer_description_to_layer[layer_description]

	return layer.context(layer)
end
Provider.create_layer = function (self, layer_description_name)
	local layer_description = self._input_layer_descriptions[layer_description_name]

	assert(layer_description, "Illegal layer description '" .. layer_description_name .. "'")

	local layer = CoreInputLayer.Layer:new(self, layer_description)
	self._layer_description_to_layer[layer_description] = layer

	self._prioritizer:add_layer_description(layer_description)

	return layer
end
Provider._layer_destroyed = function (self, layer)
	local layer_description = layer.layer_description(layer)

	self._prioritizer:remove_layer_description(layer_description)

	return 
end

return 
