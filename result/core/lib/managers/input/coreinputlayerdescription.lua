core:module("CoreInputLayerDescription")

LayerDescription = LayerDescription or class()
LayerDescription.init = function (self, name, priority)
	self._name = name
	self._priority = priority

	return 
end
LayerDescription.layer_description_name = function (self)
	return self._name
end
LayerDescription.set_context_description = function (self, context_description)
	assert(self._context_description == nil)

	self._context_description = context_description

	return 
end
LayerDescription.context_description = function (self)
	assert(self._context_description, "Must specify context for this layer_description")

	return self._context_description
end
LayerDescription.priority = function (self)
	return self._priority
end

return 
