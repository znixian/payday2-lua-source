core:module("CoreInputLayer")
core:import("CoreInputContextStack")
core:import("CoreInputProvider")
core:import("CoreInputContext")

Layer = Layer or class()
Layer.init = function (self, input_provider, layer_description)
	self._input_context_stack = CoreInputContextStack.Stack:new()
	self._layer_description = layer_description
	self._input_provider = input_provider

	return 
end
Layer.destroy = function (self)
	self._input_context_stack:destroy()
	self._input_provider:_layer_destroyed(self)

	return 
end
Layer.context = function (self)
	return self._input_context_stack:active_context()
end
Layer.layer_description = function (self)
	return self._layer_description
end
Layer.create_context = function (self)
	local context_description = self._layer_description:context_description()

	return CoreInputContext.Context:new(context_description, self._input_context_stack)
end

return 
