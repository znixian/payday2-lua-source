core:module("CoreInputContextDescription")

ContextDescription = ContextDescription or class()
ContextDescription.init = function (self, name)
	self._input_target_descriptions = {}
	self._layout_descriptions = {}
	self._context_descriptions = {}
	self._name = name

	assert(self._name, "You must specify a name for a context")

	return 
end
ContextDescription.add_input_target_description = function (self, input_target_description)
	self._input_target_descriptions[input_target_description.target_name(input_target_description)] = input_target_description

	return 
end
ContextDescription.add_layout_description = function (self, input_layout_description)
	self._layout_descriptions[input_layout_description.layout_name(input_layout_description)] = input_layout_description

	return 
end
ContextDescription.add_context_description = function (self, context_description)
	self._context_descriptions[context_description.context_description_name(context_description)] = context_description

	return 
end
ContextDescription.device_layout_description = function (self, device_type, layout_name)
	layout_name = layout_name or "default"
	local layout_description = self._layout_descriptions[layout_name]

	if layout_description == nil then
		return 
	end

	return layout_description.device_layout_description(layout_description, device_type)
end
ContextDescription.context_description_name = function (self)
	return self._name
end
ContextDescription.context_description = function (self, context_name)
	return self._context_descriptions[context_name]
end
ContextDescription.context_descriptions = function (self)
	return self._context_descriptions
end
ContextDescription.input_targets = function (self)
	return self._input_target_descriptions
end
ContextDescription.input_target_description = function (self, target_name)
	local input_target = self._input_target_descriptions[target_name]

	assert(input_target ~= nil, "Input Target with name '" .. target_name .. "' can not be found in context '" .. self._name .. "'")

	return input_target
end

return 
