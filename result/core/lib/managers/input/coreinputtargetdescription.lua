core:module("CoreInputTargetDescription")

TargetDescription = TargetDescription or class()
TargetDescription.init = function (self, name, type_name)
	self._name = name

	assert(type_name == "bool" or type_name == "vector")

	self._type_name = type_name

	return 
end
TargetDescription.target_name = function (self)
	return self._name
end
TargetDescription.target_type_name = function (self)
	return self._type_name
end

return 
