core:module("CoreInputLayerDescriptionPrioritizer")

Prioritizer = Prioritizer or class()
Prioritizer.init = function (self)
	self._layer_descriptions = {}

	return 
end
Prioritizer.add_layer_description = function (self, input_layer_description_description)
	self._layer_descriptions[input_layer_description_description] = input_layer_description_description

	if not self._layer_description or input_layer_description_description.priority(input_layer_description_description) < self._layer_description:priority() then
		self._layer_description = input_layer_description_description
	end

	return 
end
Prioritizer.remove_layer_description = function (self, input_layer_description_description)
	local needs_to_search = self._layer_description == input_layer_description_description

	assert(self._layer_descriptions[input_layer_description_description] ~= nil)

	self._layer_descriptions[input_layer_description_description] = nil
	local best_layer_description = nil

	for _, layer_description in pairs(self._layer_descriptions) do
		if not best_layer_description or layer_description.priority(layer_description) < best_layer_description.priority(best_layer_description) then
			best_layer_description = layer_description
		end
	end

	self._layer_description = best_layer_description

	return 
end
Prioritizer.active_layer_description = function (self)
	return self._layer_description
end

return 
