core:module("CorePortalManager")
core:import("CoreShapeManager")

PortalManager = PortalManager or class()
PortalManager.EFFECT_MANAGER = World:effect_manager()
PortalManager.init = function (self)
	self._portal_shapes = {}
	self._all_units = {}
	self._all_effects = {}
	self._unit_groups = {}
	self._check_positions = {}
	self._hide_list = {}
	self._deactivate_funtion = callback(self, self, "unit_deactivated")

	return 
end
PortalManager.clear = function (self)
	for _, portal in ipairs(self._portal_shapes) do
		portal.show_all(portal)
	end

	self._portal_shapes = {}
	self._all_units = {}
	self._unit_groups = {}
	self._hide_list = {}

	return 
end
PortalManager.pseudo_reset = function (self)
	for _, unit in ipairs(managers.editor:layer("Statics"):created_units()) do
		if alive(unit) then
			unit.unit_data(unit)._visibility_counter = 0
		end
	end

	for _, group in pairs(self._unit_groups) do
		group._is_inside = false

		for _, unit in ipairs(managers.editor:layer("Statics"):created_units()) do
			if group._ids[unit.unit_data(unit).unit_id] and alive(unit) then
				unit.set_visible(unit, true)

				unit.unit_data(unit)._visibility_counter = 0
			end
		end
	end

	return 
end
PortalManager.add_portal = function (self, polygon_tbl, min, max)
	cat_print("portal", "add_portal", #polygon_tbl)

	if 0 < #polygon_tbl then
		table.insert(self._portal_shapes, PortalShape:new(polygon_tbl, min, max))
	end

	return 
end
PortalManager.add_unit = function (self, unit)
	if unit.unit_data(unit).ignore_portal then
		return 
	end

	local added = nil

	for _, group in pairs(self._unit_groups) do
		added = group.add_unit(group, unit) or added
	end

	if added then
		return 
	end

	for _, portal in ipairs(self._portal_shapes) do
		local added, amount = portal.add_unit(portal, unit)

		if added then
			self._all_units[unit.key(unit)] = (self._all_units[unit.key(unit)] or 0) + amount
			local inverse = unit.unit_data(unit).portal_visible_inverse
			local i = 0

			if not portal.is_inside(portal) then
				if inverse then
					i = 1
				else
					i = -1
				end
			end

			self.change_visibility(self, unit, i, inverse)
		end
	end

	return 
end
PortalManager.remove_dynamic_unit = function (self, unit)
	self.remove_unit(self, unit)

	local check_body = unit.body(unit, unit.orientation_object(unit)) or unit.body(unit, 0)

	if alive(check_body) then
		check_body.set_activate_tag(check_body, "dynamic_portal")
		check_body.set_deactivate_tag(check_body, "dynamic_portal")
	end

	unit.add_body_activation_callback(unit, self._deactivate_funtion)

	return 
end
PortalManager.unit_deactivated = function (self, tag, unit, body, activated)
	if not activated then
		cat_print("portal", "should add unit here", tag, unit, body, activated)
		self.add_unit(self, unit)
		unit.remove_body_activation_callback(unit, self._deactivate_funtion)
	end

	return 
end
PortalManager.remove_unit = function (self, unit)
	cat_print("portal", "remove_unit", unit, unit.visible(unit))

	self._all_units[unit.key(unit)] = nil

	for _, portal in ipairs(self._portal_shapes) do
		portal.remove_unit(portal, unit)
	end

	unit.set_visible(unit, true)

	return 
end
PortalManager.delete_unit = function (self, unit)
	for name, group in pairs(self._unit_groups) do
		group.remove_unit_id(group, unit)
	end

	return 
end
PortalManager.change_visibility = function (self, unit, i, inverse)
	self._all_units[unit.key(unit)] = self._all_units[unit.key(unit)] + i

	if self._all_units[unit.key(unit)] == 0 then
		unit.set_visible(unit, inverse ~= false)
	elseif not unit.visible(unit) ~= inverse then
		unit.set_visible(unit, inverse ~= true)
	end

	return 
end
PortalManager.add_effect = function (self, effect)
	effect.id = self.EFFECT_MANAGER:spawn(effect)
	self._all_effects[effect] = 0

	for _, portal in ipairs(self._portal_shapes) do
		local added, amount = portal.add_effect(portal, effect)

		if added then
			self._all_effects[effect] = self._all_effects[effect] + amount
		end
	end

	return 
end
PortalManager.change_effect_visibility = function (self, effect, i)
	self._all_effects[effect] = self._all_effects[effect] + i

	if self._all_effects[effect] == 0 then
		effect.hidden = true

		self.EFFECT_MANAGER:set_frozen(effect.id, true)
		self.EFFECT_MANAGER:set_hidden(effect.id, true)
	elseif effect.hidden then
		effect.hidden = false

		self.EFFECT_MANAGER:set_frozen(effect.id, false)
		self.EFFECT_MANAGER:set_hidden(effect.id, false)
	end

	return 
end
PortalManager.restart_effects = function (self)
	for _, portal in ipairs(self._portal_shapes) do
		portal.clear_effects(portal)
	end

	local restart = {}

	for e, n in pairs(self._all_effects) do
		restart[e] = n
	end

	self._all_effects = {}

	for effect, _ in pairs(restart) do
		if effect.id then
			self.EFFECT_MANAGER:kill(effect.id)
		end

		self.add_effect(self, effect)
	end

	restart = nil

	return 
end
PortalManager.render = function (self)
	for _, portal in ipairs(self._portal_shapes) do
		portal.update(portal, TimerManager:wall():time(), TimerManager:wall():delta_time())
	end

	for _, group in pairs(self._unit_groups) do
		group.update(group, TimerManager:wall():time(), TimerManager:wall():delta_time())
	end

	local unit_id, unit = next(self._hide_list)

	if alive(unit) then
		unit.set_visible(unit, false)

		self._hide_list[unit_id] = nil
	end

	while table.remove(self._check_positions) do
	end

	return 
end
PortalManager.add_to_hide_list = function (self, unit)
	self._hide_list[unit.unit_data(unit).unit_id] = unit

	return 
end
PortalManager.remove_from_hide_list = function (self, unit)
	self._hide_list[unit.unit_data(unit).unit_id] = nil

	return 
end
PortalManager.debug_draw_border = function (self, polygon, min, max)
	local step = 500
	local time = 10
	local tbl = polygon.to_table(polygon)

	for x = 2, #tbl, 1 do
		local length = 0

		while length < time do
			if min and max then
				local start = Vector3(tbl[x - 1].x, tbl[x - 1].y, max)
				local stop = Vector3(tbl[x].x, tbl[x].y, max)
				local stop_vertical = Vector3(tbl[x - 1].x, tbl[x - 1].y, min)

				Application:draw_line(start, stop, 1, 0, 0)
				Application:draw_line(start, stop_vertical, 0, 1, 0)
			else
				local start = Vector3(tbl[x - 1].x, tbl[x - 1].y, step*length)
				local stop = Vector3(tbl[x].x, tbl[x].y, step*length)
				local stop_vertical = Vector3(tbl[x - 1].x, tbl[x - 1].y, step*(length + 1))

				Application:draw_line(start, stop, 1, 0, 0)
				Application:draw_line(start, stop_vertical, 0, 1, 0)
			end

			length = length + 1
		end
	end

	return 
end
PortalManager.unit_groups = function (self)
	return self._unit_groups
end
PortalManager.unit_group_on_shape = function (self, in_shape)
	for _, group in pairs(self._unit_groups) do
		for _, shape in ipairs(group.shapes(group)) do
			if shape == in_shape then
				return group
			end
		end
	end

	return 
end
PortalManager.rename_unit_group = function (self, name, new_name)
	if self._unit_groups[new_name] then
		return false
	end

	local group = self._unit_groups[name]
	self._unit_groups[name] = nil
	self._unit_groups[new_name] = group

	group.rename(group, new_name)

	return true
end
PortalManager.unit_group = function (self, name)
	return self._unit_groups[name]
end
PortalManager.add_unit_group = function (self, name)
	local group = PortalUnitGroup:new(name)
	self._unit_groups[name] = group

	return group
end
PortalManager.remove_unit_group = function (self, name)
	self._unit_groups[name] = nil

	return 
end
PortalManager.clear_unit_groups = function (self)
	self._unit_groups = {}

	return 
end
PortalManager.group_name = function (self)
	local name = "group"
	local i = 1

	while self._unit_groups[name .. i] do
		i = i + 1
	end

	return name .. i
end
PortalManager.check_positions = function (self)
	if 0 < #self._check_positions then
		return self._check_positions
	end

	for _, vp in ipairs(managers.viewport:all_really_active_viewports()) do
		local camera = vp.camera(vp)

		if alive(camera) and vp.is_rendering_scene(vp, "World") then
			table.insert(self._check_positions, camera.position(camera))
		end
	end

	return self._check_positions
end
PortalManager.unit_in_any_unit_group = function (self, unit)
	for name, group in pairs(self._unit_groups) do
		if group.unit_in_group(group, unit) then
			return true
		end
	end

	return false
end
PortalManager.save = function (self, t)
	local t = t or ""
	local s = ""

	for name, group in pairs(self._unit_groups) do
		s = s .. t .. "\t<unit_group name=\"" .. name .. "\">\n"

		for _, shape in ipairs(group.shapes(group)) do
			s = s .. shape.save(shape, t .. "\t\t") .. "\n"
		end

		s = s .. t .. "\t</unit_group>\n"
	end

	return s
end
PortalManager.save_level_data = function (self)
	local t = {}

	for name, group in pairs(self._unit_groups) do
		local shapes = {}

		for _, shape in ipairs(group.shapes(group)) do
			table.insert(shapes, shape.save_level_data(shape))
		end

		t[name] = {
			shapes = shapes,
			ids = group.ids(group)
		}
	end

	return t
end
PortalShape = PortalShape or class()
PortalShape.init = function (self, polygon_tbl, min, max)
	self._polygon = ScriptPolygon2D(polygon_tbl)
	self._units = {}
	self._inverse_units = {}
	self._effects = {}
	self._is_inside = true
	self._min = min
	self._max = max

	return 
end
PortalShape.add_unit = function (self, unit)
	if self.inside(self, unit.position(unit)) then
		self._units[unit.key(unit)] = unit
		local inverse = unit.unit_data(unit).portal_visible_inverse

		if inverse then
			unit.set_visible(unit, false)
		end

		return true, (inverse and -1) or 1, self
	end

	return 
end
PortalShape.remove_unit = function (self, unit)
	self._units[unit.key(unit)] = nil

	return 
end
PortalShape.add_effect = function (self, effect)
	if self.inside(self, effect.position) then
		table.insert(self._effects, effect)

		return true, 1
	end

	return 
end
PortalShape.clear_effects = function (self)
	self._effects = {}

	return 
end
PortalShape.show_all = function (self)
	for _, unit in pairs(self._units) do
		if alive(unit) then
			unit.set_visible(unit, true)
		end
	end

	return 
end
PortalShape.inside = function (self, pos)
	local is_inside = self._polygon:inside(pos)

	if is_inside and self._min and self._max then
		local z = pos.z

		if self._min < z and z < self._max then
			return true
		else
			return false
		end
	end

	return is_inside
end
PortalShape.is_inside = function (self)
	return self._is_inside
end
PortalShape.update = function (self, time, rel_time)
	local is_inside = false

	for _, pos in ipairs(managers.portal:check_positions()) do
		is_inside = self.inside(self, pos)

		if is_inside then
			break
		end
	end

	if self._is_inside ~= is_inside then
		self._is_inside = is_inside

		for _, unit in pairs(self._units) do
			self._change_visibility(self, unit)
		end

		for _, effect in ipairs(self._effects) do
			local i = (self._is_inside and 1) or -1

			managers.portal:change_effect_visibility(effect, i)
		end
	end

	return 
end
PortalShape._change_visibility = function (self, unit)
	if alive(unit) then
		local inverse = unit.unit_data(unit).portal_visible_inverse
		local i = (self._is_inside ~= inverse and 1) or -1

		managers.portal:change_visibility(unit, i, inverse)
	end

	return 
end
PortalUnitGroup = PortalUnitGroup or class()
PortalUnitGroup.init = function (self, name)
	self._name = name
	self._shapes = {}
	self._ids = {}
	self._r = math.rand(0.5) + 0.5
	self._g = math.rand(0.5) + 0.5
	self._b = math.rand(0.5) + 0.5
	self._units = {}
	self._is_inside = false

	return 
end
PortalUnitGroup.rename = function (self, new_name)
	self._name = new_name

	return 
end
PortalUnitGroup.name = function (self)
	return self._name
end
PortalUnitGroup.shapes = function (self)
	return self._shapes
end
PortalUnitGroup.ids = function (self)
	return self._ids
end
PortalUnitGroup.set_ids = function (self, ids)
	self._ids = ids or self._ids

	return 
end
PortalUnitGroup.add_shape = function (self, params)
	local shape = PortalUnitGroupShape:new(params)

	table.insert(self._shapes, shape)

	return shape
end
PortalUnitGroup.remove_shape = function (self, shape)
	shape.destroy(shape)
	table.delete(self._shapes, shape)

	return 
end
PortalUnitGroup.add_unit = function (self, unit)
	if self._ids[unit.unit_data(unit).unit_id] then
		unit.unit_data(unit)._visibility_counter = unit.unit_data(unit)._visibility_counter or 0

		self._change_visibility(self, unit, (self._is_inside and 1) or 0)
		table.insert(self._units, unit)

		return true
	end

	return false
end
PortalUnitGroup.add_unit_id = function (self, unit)
	if self._ids[unit.unit_data(unit).unit_id] then
		self.remove_unit_id(self, unit)

		return 
	end

	self._ids[unit.unit_data(unit).unit_id] = true

	return 
end
PortalUnitGroup.remove_unit_id = function (self, unit)
	self._ids[unit.unit_data(unit).unit_id] = nil

	return 
end
PortalUnitGroup.lock_units = function (self)
	for slot4, slot5 in ipairs(self._units) do
	end

	return 
end
PortalUnitGroup.inside = function (self, pos)
	for _, shape in ipairs(self._shapes) do
		if shape.is_inside(shape, pos) then
			return true
		end
	end

	return false
end
PortalUnitGroup.update = function (self, t, dt)
	local is_inside = false

	for _, pos in ipairs(managers.portal:check_positions()) do
		is_inside = self.inside(self, pos)

		if is_inside then
			break
		end
	end

	if self._is_inside ~= is_inside then
		self._is_inside = is_inside
		local diff = (self._is_inside and 1) or -1

		self._change_units_visibility(self, diff)
	end

	return 
end
PortalUnitGroup._change_units_visibility = function (self, diff)
	for _, unit in pairs(self._units) do
		self._change_visibility(self, unit, diff)
	end

	return 
end
PortalUnitGroup._change_units_visibility_in_editor = function (self, diff)
	for _, unit in ipairs(managers.editor:layer("Statics"):created_units()) do
		if self._ids[unit.unit_data(unit).unit_id] then
			self._change_visibility(self, unit, diff)
		end
	end

	return 
end
PortalUnitGroup._change_visibility = function (self, unit, diff)
	if alive(unit) then
		unit.unit_data(unit)._visibility_counter = unit.unit_data(unit)._visibility_counter + diff

		if 0 < unit.unit_data(unit)._visibility_counter then
			unit.set_visible(unit, true)
			managers.portal:remove_from_hide_list(unit)
		else
			managers.portal:add_to_hide_list(unit)
		end
	end

	return 
end
PortalUnitGroup.unit_in_group = function (self, unit)
	return (self._ids[unit.unit_data(unit).unit_id] and true) or false
end
PortalUnitGroup.draw = function (self, t, dt, mul, skip_shapes, skip_units)
	local r = self._r*mul
	local g = self._g*mul
	local b = self._b*mul
	local brush = Draw:brush()

	brush.set_color(brush, Color(0.25, r, g, b))

	if not skip_units then
		for _, unit in ipairs(managers.editor:layer("Statics"):created_units()) do
			if self._ids[unit.unit_data(unit).unit_id] then
				brush.unit(brush, unit)
				Application:draw(unit, r, g, b)
			end
		end
	end

	if not skip_shapes then
		for _, shape in ipairs(self._shapes) do
			shape.draw(shape, t, dt, r, g, b)
			shape.draw_outline(shape, t, dt, r/2, g/2, b/2)
		end
	end

	return 
end
PortalUnitGroupShape = PortalUnitGroupShape or class(CoreShapeManager.ShapeBox)
PortalUnitGroupShape.init = function (self, params)
	params.type = "box"

	PortalUnitGroupShape.super.init(self, params)

	return 
end
PortalUnitGroupShape.draw = function (self, t, dt, r, g, b)
	PortalUnitGroupShape.super.draw(self, t, dt, r, g, b)

	if alive(self._unit) then
		Application:draw(self._unit, r, g, b)
	end

	return 
end

return 
