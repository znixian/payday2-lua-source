core:module("CoreDatabaseManager")
core:import("CoreEngineAccess")
core:import("CoreApp")
core:import("CoreString")

DatabaseManager = DatabaseManager or class()
DatabaseManager.list_unit_types = function (self)
	if self.__unit_types == nil then
		self.__unit_types = {}

		for _, unit in ipairs(self.list_entries_of_type(self, "unit")) do
			local unit_data = CoreEngineAccess._editor_unit_data(unit.id(unit))

			table.insert(self.__unit_types, (unit_data and unit_data.type(unit_data)) or nil)
		end
	end

	return self.__unit_types
end
DatabaseManager.list_units_of_type = function (self, type)
	if self.__units_by_type == nil then
		self.__units_by_type = {}

		for _, unit in ipairs(self.list_entries_of_type(self, "unit")) do
			local unit_data = CoreEngineAccess._editor_unit_data(unit.id(unit))
			local key = unit_data and unit_data.type(unit_data) and unit_data.type(unit_data):key()

			if key then
				self.__units_by_type[key] = self.__units_by_type[key] or {}

				table.insert(self.__units_by_type[key], unit)
			end
		end
	end

	return self.__units_by_type[type.key(type)] or {}
end
DatabaseManager.list_entries_of_type = function (self, type, pattern)
	return self.list_entries_in_index(self, self._type_index(self, type), pattern)
end
DatabaseManager.list_entries_in_index = function (self, index, pattern)
	local entries = self._entries_in_index(self, index)

	return (pattern and table.find_all_values(entries, function (e)
		return string.find(e, pattern:s()) ~= nil
	end)) or entries
end
DatabaseManager.recompile = function (self, ...)
	local files = {}

	for _, v in pairs({
		...
	}) do
		table.insert(files, self.entry_relative_path(self, v))
	end

	Application:data_compile({
		target_db_name = "all",
		preprocessor_definitions = "preprocessor_definitions",
		verbose = false,
		platform = string.lower(SystemInfo:platform():s()),
		source_root = self.base_path(self),
		target_db_root = Application:base_path() .. "assets",
		source_files = files
	})
	DB:reload()
	self.clear_all_cached_indices(self)

	return 
end
DatabaseManager.clear_cached_index = function (self, index)
	if self.__entries then
		self.__entries[index] = nil
	end

	if index == self._type_index(self, "unit") then
		self.__unit_types = nil
		self.__units_by_type = nil
	end

	return 
end
DatabaseManager.clear_all_cached_indices = function (self)
	self.__entries = nil
	self.__unit_types = nil
	self.__units_by_type = nil

	return 
end
DatabaseManager.root_path = function (self)
	local path = Application:base_path() .. (CoreApp.arg_value("-assetslocation") or "..\\..\\")
	path = Application:nice_path(path, true)
	local f = nil

	function f(s)
		local str, i = string.gsub(s, "\\[%w_%.%s]+\\%.%.", "")

		return (0 < i and f(str)) or str
	end

	return f(path)
end
DatabaseManager.base_path = function (self)
	return self.root_path(self) .. "assets\\"
end
DatabaseManager.is_valid_database_path = function (self, path)
	return string.begins(Application:nice_path(path, true):lower(), Application:nice_path(self.base_path(self), true):lower())
end
DatabaseManager.entry_name = function (self, path)
	return string.match(self.entry_path(self, path), "([^/]*)$")
end
DatabaseManager.entry_type = function (self, path)
	if not string.find(path, "%.") then
		return nil
	end

	return string.match(path, "([^.]*)$")
end
DatabaseManager.entry_path = function (self, path)
	path = string.match(string.gsub(path, ".*assets[/\\]", ""), "([^.]*)")
	path = string.gsub(path, "[/\\]+", "/")

	return path
end
DatabaseManager.entry_path_with_properties = function (self, path)
	path = string.gsub(path, ".*assets[/\\]", "")
	path = string.gsub(path, "%.%w-$", "")
	path = string.gsub(path, "[/\\]+", "/")

	return path
end
DatabaseManager.entry_relative_path = function (self, path)
	path = string.gsub(path, ".*assets[/\\]", "")
	path = string.gsub(path, "[/\\]+", "/")

	return path
end
DatabaseManager.entry_expanded_path = function (self, type, entry_path, prop)
	local properties = prop or ""
	local path = string.gsub(self.base_path(self) .. entry_path .. properties .. "." .. type, "/", "\\")

	return path
end
DatabaseManager.entry_expanded_directory = function (self, entry_path)
	local path = string.gsub(self.base_path(self) .. entry_path, "/", "\\")

	return path
end
DatabaseManager.load_node_dialog = function (self, parent, file_pattern, start_path)
	local path = self.open_file_dialog(self, parent, file_pattern, start_path)
	local node = path and self.load_node(self, path)

	return node, path
end
DatabaseManager.open_file_dialog = function (self, parent, file_pattern, start_path)
	local path = start_path or self.base_path(self)
	local pattern = file_pattern or "*.*"
	local file_dialog = EWS:FileDialog(parent, "Choose file", path, "", pattern, "OPEN,FILE_MUST_EXIST")

	if file_dialog.show_modal(file_dialog) then
		return file_dialog.get_path(file_dialog), file_dialog.get_directory(file_dialog)
	end

	return 
end
DatabaseManager.save_file_dialog = function (self, parent, new, file_pattern, start_path, save_outside_project)
	local pattern = file_pattern or "*.*"
	local path = start_path or self.base_path(self)
	local name = ""
	local new_file = ""

	if start_path then
		name = self.entry_name(self, start_path)
	end

	if new then
		new_file = "new file "
	end

	local save_dialog = EWS:FileDialog(parent, "Save " .. new_file .. "as..", path, name, file_pattern, "SAVE")

	if save_dialog.show_modal(save_dialog) then
		local new_path = save_dialog.get_path(save_dialog)

		if save_outside_project or self.is_valid_database_path(self, new_path) then
			return new_path, save_dialog.get_directory(save_dialog)
		else
			EWS:MessageDialog(parent, "Invalid path. Cannot save outside of the project.", "Error", "OK,ICON_ERROR"):show_modal()
		end
	end

	return nil, nil
end
DatabaseManager.load_node = function (self, path)
	if self.has(self, path) then
		local file = SystemFS:open(path, "r")
		local content = file.read(file)

		file.close(file)

		return Node.from_xml(content)
	end

	return nil
end
DatabaseManager.save_node = function (self, node, path)
	local file = assert(SystemFS:open(path, "w"))

	file.write(file, node.to_xml(node))
	file.close(file)

	return 
end
DatabaseManager.delete = function (self, path)
	return SystemFS:delete_file(path)
end
DatabaseManager.has = function (self, path)
	return SystemFS:exists(path)
end
DatabaseManager._entries_in_index = function (self, index)
	local result = self.__entries and self.__entries[index]

	if result == nil then
		result = self._parse_entries_in_index(self, index)
		self.__entries = self.__entries or {}
		self.__entries[index] = result
	end

	return result
end
DatabaseManager._type_index = function (self, type)
	return string.format("indices/types/%s", type.s(type))
end
DatabaseManager._parse_entries_in_index = function (self, index)
	local file = (DB:has("index", index) and DB:open("index", index)) or nil

	if file == nil then
		return {}
	else
		local contents = file.read(file)

		file.close(file)

		return string.split(contents, "[\r\n]")
	end

	return 
end

return 
