core:module("CoreEnvironmentEffectsManager")
core:import("CoreTable")

EnvironmentEffectsManager = EnvironmentEffectsManager or class()
EnvironmentEffectsManager.init = function (self)
	self._effects = {}
	self._current_effects = {}
	self._mission_effects = {}
	self._repeat_mission_effects = {}

	return 
end
EnvironmentEffectsManager.add_effect = function (self, name, effect)
	self._effects[name] = effect

	if effect.default(effect) then
		self.use(self, name)
	end

	return 
end
EnvironmentEffectsManager.effect = function (self, name)
	return self._effects[name]
end
EnvironmentEffectsManager.effects = function (self)
	return self._effects
end
EnvironmentEffectsManager.effects_names = function (self)
	local t = {}

	for name, effect in pairs(self._effects) do
		if not effect.default(effect) then
			table.insert(t, name)
		end
	end

	table.sort(t)

	return t
end
EnvironmentEffectsManager.use = function (self, effect)
	if self._effects[effect] then
		if not table.contains(self._current_effects, self._effects[effect]) then
			self._effects[effect]:load_effects()
			self._effects[effect]:start()
			table.insert(self._current_effects, self._effects[effect])
		end
	else
		Application:error("No effect named, " .. effect .. " availible to use")
	end

	return 
end
EnvironmentEffectsManager.load_effects = function (self, effect)
	if self._effects[effect] then
		self._effects[effect]:load_effects()
	end

	return 
end
EnvironmentEffectsManager.stop = function (self, effect)
	if self._effects[effect] then
		self._effects[effect]:stop()
		table.delete(self._current_effects, self._effects[effect])
	end

	return 
end
EnvironmentEffectsManager.stop_all = function (self)
	for _, effect in ipairs(self._current_effects) do
		effect.stop(effect)
	end

	self._current_effects = {}

	return 
end
EnvironmentEffectsManager.update = function (self, t, dt)
	for _, effect in ipairs(self._current_effects) do
		effect.update(effect, t, dt)
	end

	for name, params in pairs(self._repeat_mission_effects) do
		params.next_time = params.next_time - dt

		if params.next_time <= 0 then
			params.next_time = params.base_time + math.rand(params.random_time)
			params.effect_id = World:effect_manager():spawn(params)

			if params.max_amount then
				params.max_amount = params.max_amount - 1

				if params.max_amount <= 0 then
					self._repeat_mission_effects[name] = nil
				end
			end
		end
	end

	return 
end
EnvironmentEffectsManager.gravity_and_wind_dir = function (self)
	local wind_importance = 0.5

	return Vector3(0, 0, -982) + Wind:wind_at(Vector3())*wind_importance
end
EnvironmentEffectsManager.spawn_mission_effect = function (self, name, params)
	if 0 < params.base_time or 0 < params.random_time then
		if self._repeat_mission_effects[name] then
			self.kill_mission_effect(self, name)
		end

		params.next_time = 0
		params.effect_id = nil
		self._repeat_mission_effects[name] = params

		return 
	end

	params.effect_id = World:effect_manager():spawn(params)
	self._mission_effects[name] = self._mission_effects[name] or {}

	table.insert(self._mission_effects[name], params)

	return 
end
EnvironmentEffectsManager.kill_all_mission_effects = function (self)
	for _, params in pairs(self._repeat_mission_effects) do
		if params.effect_id then
			World:effect_manager():kill(params.effect_id)
		end
	end

	self._repeat_mission_effects = {}

	for _, effects in pairs(self._mission_effects) do
		for _, params in ipairs(effects) do
			World:effect_manager():kill(params.effect_id)
		end
	end

	self._mission_effects = {}

	return 
end
EnvironmentEffectsManager.kill_mission_effect = function (self, name)
	self._kill_mission_effect(self, name, "kill")

	return 
end
EnvironmentEffectsManager.fade_kill_mission_effect = function (self, name)
	self._kill_mission_effect(self, name, "fade_kill")

	return 
end
EnvironmentEffectsManager._kill_mission_effect = function (self, name, type)
	local kill = callback(World:effect_manager(), World:effect_manager(), type)
	local params = self._repeat_mission_effects[name]

	if params then
		if params.effect_id then
			kill(params.effect_id)
		end

		self._repeat_mission_effects[name] = nil

		return 
	end

	local effects = self._mission_effects[name]

	if not effects then
		return 
	end

	for _, params in ipairs(effects) do
		kill(params.effect_id)
	end

	self._mission_effects[name] = nil

	return 
end
EnvironmentEffectsManager.save = function (self, data)
	local state = {
		mission_effects = {}
	}

	for name, effects in pairs(self._mission_effects) do
		state.mission_effects[name] = {}

		for _, params in pairs(effects) do
			if World:effect_manager():alive(params.effect_id) then
				table.insert(state.mission_effects[name], params)
			end
		end
	end

	data.EnvironmentEffectsManager = state

	return 
end
EnvironmentEffectsManager.load = function (self, data)
	local state = data.EnvironmentEffectsManager

	for name, effects in pairs(state.mission_effects) do
		for _, params in ipairs(effects) do
			self.spawn_mission_effect(self, name, params)
		end
	end

	return 
end
EnvironmentEffect = EnvironmentEffect or class()
EnvironmentEffect.init = function (self, default)
	self._default = default

	return 
end
EnvironmentEffect.load_effects = function (self)
	return 
end
EnvironmentEffect.update = function (self, t, dt)
	return 
end
EnvironmentEffect.start = function (self)
	return 
end
EnvironmentEffect.stop = function (self)
	return 
end
EnvironmentEffect.default = function (self)
	return self._default
end

return 
