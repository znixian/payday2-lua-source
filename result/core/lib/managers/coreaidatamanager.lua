core:module("CoreAiDataManager")
core:import("CoreTable")

AiDataManager = AiDataManager or class()
AiDataManager.init = function (self)
	self._setup(self)

	return 
end
AiDataManager._setup = function (self)
	self._data = {
		patrol_paths = {}
	}

	return 
end
AiDataManager.add_patrol_path = function (self, name)
	if self._data.patrol_paths[name] then
		Application:error("Patrol path with name " .. name .. " already exists!")

		return false
	end

	self._data.patrol_paths[name] = {
		points = {}
	}

	return true
end
AiDataManager.remove_patrol_path = function (self, name)
	if not self._data.patrol_paths[name] then
		Application:error("Patrol path with name " .. name .. " doesn't exist!")

		return false
	end

	self._data.patrol_paths[name] = nil

	return true
end
AiDataManager.add_patrol_point = function (self, name, unit)
	if not self._data.patrol_paths[name] then
		Application:error("Patrol path with name " .. name .. " doesn't exist!")

		return 
	end

	local t = {
		position = unit.position(unit),
		rotation = unit.rotation(unit),
		unit = unit,
		unit_id = unit.unit_data(unit).unit_id
	}

	table.insert(self._data.patrol_paths[name].points, t)

	return 
end
AiDataManager.delete_point_by_unit = function (self, unit)
	for name, path in pairs(self._data.patrol_paths) do
		for i, point in ipairs(path.points) do
			if point.unit == unit then
				table.remove(path.points, i)

				return 
			end
		end
	end

	return 
end
AiDataManager.patrol_path_by_unit = function (self, unit)
	for name, path in pairs(self._data.patrol_paths) do
		for i, point in ipairs(path.points) do
			if point.unit == unit then
				return name, path
			end
		end
	end

	return 
end
AiDataManager.patrol_point_index_by_unit = function (self, unit)
	for name, path in pairs(self._data.patrol_paths) do
		for i, point in ipairs(path.points) do
			if point.unit == unit then
				return i, point
			end
		end
	end

	return 
end
AiDataManager.patrol_path = function (self, name)
	return self._data.patrol_paths[name]
end
AiDataManager.all_patrol_paths = function (self)
	return self._data.patrol_paths
end
AiDataManager.patrol_path_names = function (self)
	local t = {}

	for name, path in pairs(self._data.patrol_paths) do
		table.insert(t, name)
	end

	table.sort(t)

	return t
end
AiDataManager.destination_path = function (self, position, rotation)
	return {
		points = {
			{
				position = position,
				rotation = rotation
			}
		}
	}
end
AiDataManager.clear = function (self)
	self._setup(self)

	return 
end
AiDataManager.save_data = function (self)
	local t = CoreTable.deep_clone(self._data)

	for name, path in pairs(t.patrol_paths) do
		for i, point in ipairs(path.points) do
			point.position = point.unit:position()
			point.rotation = point.unit:rotation()
			point.unit = nil
		end
	end

	return t
end
AiDataManager.load_data = function (self, data)
	if not data then
		return 
	end

	self._data = data

	return 
end
AiDataManager.load_units = function (self, units)
	for _, unit in ipairs(units) do
		for name, path in pairs(self._data.patrol_paths) do
			for i, point in ipairs(path.points) do
				if point.unit_id == unit.unit_data(unit).unit_id then
					point.unit = unit
				end
			end
		end
	end

	return 
end

return 
