core:module("CoreControllerWrapperPS4")
core:import("CoreControllerWrapper")

ControllerWrapperPS4 = ControllerWrapperPS4 or class(CoreControllerWrapper.ControllerWrapper)
ControllerWrapperPS4.TYPE = "ps4"
ControllerWrapperPS4.CONTROLLER_TYPE_LIST = {
	"ps4_controller"
}
ControllerWrapperPS4.init = function (self, manager, id, name, controller, setup, debug, skip_virtual_controller)
	local func_map = {
		confirm = callback(self, self, "virtual_connect_confirm"),
		cancel = callback(self, self, "virtual_connect_cancel")
	}

	ControllerWrapperPS4.super.init(self, manager, id, name, {
		ps4pad = controller
	}, "ps4pad", setup, debug, skip_virtual_controller, {
		ps4pad = func_map
	})

	return 
end
ControllerWrapperPS4.virtual_connect_confirm = function (self, controller_id, controller, input_name, connection_name, connection)
	if self.is_confirm_cancel_inverted(self) then
		input_name = "circle"
	else
		input_name = "cross"
	end

	self.virtual_connect2(self, controller_id, controller, input_name, connection_name, connection)

	return 
end
ControllerWrapperPS4.virtual_connect_cancel = function (self, controller_id, controller, input_name, connection_name, connection)
	if self.is_confirm_cancel_inverted(self) then
		input_name = "cross"
	else
		input_name = "circle"
	end

	self.virtual_connect2(self, controller_id, controller, input_name, connection_name, connection)

	return 
end
local is_PS4 = SystemInfo:platform() == Idstring("PS4")
ControllerWrapperPS4.is_confirm_cancel_inverted = function (self)
	return is_PS4 and PS3:pad_cross_circle_inverted()
end

return 
