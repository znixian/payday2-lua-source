core:module("CoreControllerWrapperDebug")
core:import("CoreControllerWrapper")

ControllerWrapperDebug = ControllerWrapperDebug or class(CoreControllerWrapper.ControllerWrapper)
ControllerWrapperDebug.TYPE = "debug"
ControllerWrapperDebug.init = function (self, controller_wrapper_list, manager, id, name, default_controller_wrapper, setup)
	self._controller_wrapper_list = controller_wrapper_list
	self._default_controller_wrapper = default_controller_wrapper

	ControllerWrapperDebug.super.init(self, manager, id, name, {}, default_controller_wrapper and default_controller_wrapper.get_default_controller_id(default_controller_wrapper), setup, true, true)

	for _, controller_wrapper in ipairs(controller_wrapper_list) do
		for controller_id, controller in pairs(controller_wrapper.get_controller_map(controller_wrapper)) do
			self._controller_map[controller_id] = controller
		end
	end

	return 
end
ControllerWrapperDebug.destroy = function (self)
	ControllerWrapperDebug.super.destroy(self)

	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.destroy(controller_wrapper)
	end

	return 
end
ControllerWrapperDebug.update = function (self, t, dt)
	ControllerWrapperDebug.super.update(self, t, dt)

	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.update(controller_wrapper, t, dt)
	end

	return 
end
ControllerWrapperDebug.paused_update = function (self, t, dt)
	ControllerWrapperDebug.super.paused_update(self, t, dt)

	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.paused_update(controller_wrapper, t, dt)
	end

	return 
end
ControllerWrapperDebug.connected = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connected(controller_wrapper, ...) then
			return true
		end
	end

	return false
end
ControllerWrapperDebug.rebind_connections = function (self, setup, setup_map)
	ControllerWrapperDebug.super.rebind_connections(self)

	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.rebind_connections(controller_wrapper, setup_map and setup_map[controller_wrapper.get_type(controller_wrapper)], setup_map)
	end

	return 
end
ControllerWrapperDebug.setup = function (self, ...)
	return 
end
ControllerWrapperDebug.get_any_input = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.get_any_input(controller_wrapper, ...) then
			return true
		end
	end

	return false
end
ControllerWrapperDebug.get_any_input_pressed = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.get_any_input_pressed(controller_wrapper, ...) then
			return true
		end
	end

	return false
end
ControllerWrapperDebug.get_input_pressed = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) and controller_wrapper.get_input_pressed(controller_wrapper, ...) then
			return true
		end
	end

	return false
end
ControllerWrapperDebug.get_input_bool = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) and controller_wrapper.get_input_bool(controller_wrapper, ...) then
			return true
		end
	end

	return false
end
ControllerWrapperDebug.get_input_float = function (self, ...)
	local input_float = 0

	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) then
			input_float = math.max(input_float, controller_wrapper.get_input_float(controller_wrapper, ...))
		end
	end

	return input_float
end
ControllerWrapperDebug.get_input_axis = function (self, ...)
	local input_axis = Vector3(0, 0, 0)

	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) then
			local next_input_axis = controller_wrapper.get_input_axis(controller_wrapper, ...)

			if input_axis.length(input_axis) < next_input_axis.length(next_input_axis) then
				input_axis = next_input_axis
			end
		end
	end

	return input_axis
end
ControllerWrapperDebug.get_connection_map = function (self, ...)
	local map = {}

	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		local sub_map = controller_wrapper.get_connection_map(controller_wrapper, ...)

		for k, v in pairs(sub_map) do
			map[k] = v
		end
	end

	return map
end
ControllerWrapperDebug.connection_exist = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) then
			return true
		end
	end

	return false
end
ControllerWrapperDebug.set_enabled = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.set_enabled(controller_wrapper, ...)
	end

	return 
end
ControllerWrapperDebug.enable = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.enable(controller_wrapper, ...)
	end

	self._enabled = true

	return 
end
ControllerWrapperDebug.disable = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.disable(controller_wrapper, ...)
	end

	self._enabled = false

	return 
end
ControllerWrapperDebug.add_trigger = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) then
			controller_wrapper.add_trigger(controller_wrapper, ...)
		end
	end

	return 
end
ControllerWrapperDebug.add_release_trigger = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) then
			controller_wrapper.add_release_trigger(controller_wrapper, ...)
		end
	end

	return 
end
ControllerWrapperDebug.remove_trigger = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.connection_exist(controller_wrapper, ...) then
			controller_wrapper.remove_trigger(controller_wrapper, ...)
		end
	end

	return 
end
ControllerWrapperDebug.clear_triggers = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.clear_triggers(controller_wrapper, ...)
	end

	return 
end
ControllerWrapperDebug.reset_cache = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.reset_cache(controller_wrapper, ...)
	end

	return 
end
ControllerWrapperDebug.restore_triggers = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.restore_triggers(controller_wrapper, ...)
	end

	return 
end
ControllerWrapperDebug.clear_connections = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.clear_connections(controller_wrapper, ...)
	end

	return 
end
ControllerWrapperDebug.get_setup = function (self, ...)
	return self._default_controller_wrapper and self._default_controller_wrapper:get_setup(...)
end
ControllerWrapperDebug.get_connection_settings = function (self, ...)
	return self._default_controller_wrapper and self._default_controller_wrapper:get_connection_settings(...)
end
ControllerWrapperDebug.get_connection_enabled = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		if controller_wrapper.get_connection_enabled(controller_wrapper, ...) then
			return true
		end
	end

	return false
end
ControllerWrapperDebug.set_connection_enabled = function (self, ...)
	for _, controller_wrapper in ipairs(self._controller_wrapper_list) do
		controller_wrapper.set_connection_enabled(controller_wrapper, ...)
	end

	return 
end

return 
