core:module("CoreControllerWrapperSteam")
core:import("CoreControllerWrapper")

ControllerWrapperSteam = ControllerWrapperSteam or class(CoreControllerWrapper.ControllerWrapper)
ControllerWrapperSteam.TYPE = "steam"
ControllerWrapperSteam.CONTROLLER_TYPE_LIST = {
	"steam_controller"
}
ControllerWrapperSteam.init = function (self, manager, id, name, controller, setup, debug, skip_virtual_controller)
	local func_map = {
		confirm = callback(self, self, "virtual_connect_confirm"),
		cancel = callback(self, self, "virtual_connect_cancel")
	}

	ControllerWrapperSteam.super.init(self, manager, id, name, {
		keyboard = Input:keyboard(),
		mouse = Input:mouse(),
		steampad = controller
	}, "steampad", setup, debug, skip_virtual_controller, {
		steampad = func_map
	})

	return 
end
ControllerWrapperSteam.virtual_connect_confirm = function (self, controller_id, controller, input_name, connection_name, connection)
	self.virtual_connect2(self, controller_id, controller, "button_a", connection_name, connection)

	return 
end
ControllerWrapperSteam.virtual_connect_cancel = function (self, controller_id, controller, input_name, connection_name, connection)
	self.virtual_connect2(self, controller_id, controller, "button_b", connection_name, connection)

	return 
end
ControllerWrapperSteam.show_binding_panel = function (self)
	if self._controller_map and self._controller_map.steampad then
		return self._controller_map.steampad:show_binding_panel()
	end

	return 
end
ControllerWrapperSteam.convert_virtual_action = function (action)
	if action == "confirm" then
		return "button_a"
	elseif action == "cancel" then
		return "button_b"
	end

	return action
end
ControllerWrapperSteam.change_mode = function (controller, mode)
	if controller and mode then
		controller.change_mode(controller, mode)

		return mode
	end

	return 
end

return 
