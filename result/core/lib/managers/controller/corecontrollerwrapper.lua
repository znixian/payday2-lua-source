core:module("CoreControllerWrapper")
core:import("CoreControllerWrapperSettings")
core:import("CoreAccessObjectBase")

ControllerWrapper = ControllerWrapper or class(CoreAccessObjectBase.AccessObjectBase)
ControllerWrapper.TYPE = "generic"
ControllerWrapper.init = function (self, manager, id, name, controller_map, default_controller_id, setup, debug, skip_virtual_controller, custom_virtual_connect_func_map)
	ControllerWrapper.super.init(self, manager, name)

	self._id = id
	self._name = name
	self._controller_map = controller_map
	self._default_controller_id = default_controller_id
	self._setup = setup
	self._debug = debug

	if not skip_virtual_controller then
		self._virtual_controller = Input:create_virtual_controller("ctrl_" .. tostring(self._id))
	end

	self._custom_virtual_connect_func_map = custom_virtual_connect_func_map or {}

	for controller_id in pairs(controller_map) do
		self._custom_virtual_connect_func_map[controller_id] = self._custom_virtual_connect_func_map[controller_id] or {}
	end

	self._connection_map = {}
	self._trigger_map = {}
	self._release_trigger_map = {}
	self._current_lerp_axis_map = {}
	self._claimed = false
	self._enabled = false
	self._delay_map = {}
	self._delay_bool_map = {}
	self._multi_input_map = {}

	self.setup(self, self._setup)

	self._was_connected = self.connected(self)
	self._reset_cache_time = TimerManager:wall_running():time() - 1
	self._delay_trigger_queue = {}
	self._input_pressed_cache = {}
	self._input_bool_cache = {}
	self._input_float_cache = {}
	self._input_axis_cache = {}

	self.reset_cache(self, false)

	self._destroy_callback_list = {}
	self._last_destroy_callback_id = 0
	self._connect_changed_callback_list = {}
	self._last_connect_changed_callback_id = 0
	self._rebind_callback_list = {}
	self._last_rebind_callback_id = 0

	return 
end
ControllerWrapper.destroy = function (self)
	for id, func in pairs(self._destroy_callback_list) do
		func(self, id)
	end

	if alive(self._virtual_controller) then
		Input:destroy_virtual_controller(self._virtual_controller)

		self._virtual_controller = nil
	end

	return 
end
ControllerWrapper.update = function (self, t, dt)
	self.reset_cache(self, true)
	self.update_delay_trigger_queue(self)
	self.check_connect_changed_status(self)

	if alive(self._virtual_controller) then
		self._virtual_controller:clear_axis_triggers()
	end

	return 
end
ControllerWrapper.paused_update = function (self, t, dt)
	self.reset_cache(self, true)
	self.update_delay_trigger_queue(self)
	self.check_connect_changed_status(self)

	if alive(self._virtual_controller) then
		self._virtual_controller:clear_axis_triggers()
	end

	return 
end
ControllerWrapper.reset_cache = function (self, check_time)
	local reset_cache_time = TimerManager:wall_running():time()

	if not check_time or self._reset_cache_time < reset_cache_time then
		self._input_any_cache = nil
		self._input_any_pressed_cache = nil
		self._input_any_released_cache = nil

		if next(self._input_pressed_cache) then
			self._input_pressed_cache = {}
		end

		if next(self._input_bool_cache) then
			self._input_bool_cache = {}
		end

		if next(self._input_float_cache) then
			self._input_float_cache = {}
		end

		if next(self._input_axis_cache) then
			self._input_axis_cache = {}
		end

		self.update_multi_input(self)
		self.update_delay_input(self)

		self._reset_cache_time = reset_cache_time
	end

	return 
end
ControllerWrapper.update_delay_trigger_queue = function (self)
	if self._enabled and self._virtual_controller then
		for connection_name, data in pairs(self._delay_trigger_queue) do
			if not self._virtual_controller:down(Idstring(connection_name)) then
				self._delay_trigger_queue[connection_name] = nil
			elseif self.get_input_bool(self, connection_name) then
				self._delay_trigger_queue[connection_name] = nil

				data.func(unpack(data.func_params))
			end
		end
	end

	return 
end
ControllerWrapper.check_connect_changed_status = function (self)
	local connected = self.connected(self)

	if connected ~= self._was_connected then
		for callback_id, func in pairs(self._connect_changed_callback_list) do
			func(self, connected, callback_id)
		end

		self._was_connected = connected
	end

	return 
end
ControllerWrapper.update_multi_input = function (self)
	if self._enabled and self._virtual_controller then
		for connection_name, single_connection_name_list in pairs(self._multi_input_map) do
			if self.get_connection_enabled(self, connection_name) then
				local bool = nil

				for _, single_connection_name in ipairs(single_connection_name_list) do
					bool = self._virtual_controller:down(Idstring(single_connection_name))

					if not bool then
						break
					end
				end

				if bool then
					self._input_bool_cache[connection_name] = bool
				else
					self._input_bool_cache[connection_name] = false
					self._input_pressed_cache[connection_name] = false
					self._input_float_cache[connection_name] = 0
					self._input_axis_cache[connection_name] = Vector3()
				end
			end
		end
	end

	return 
end
ControllerWrapper.update_delay_input = function (self)
	if self._enabled and self._virtual_controller then
		local wall_time = TimerManager:wall():time()

		for connection_name, delay_data in pairs(self._delay_map) do
			if self.get_connection_enabled(self, connection_name) then
				local delay_time_map = delay_data.delay_time_map
				local connection = delay_data.connection
				local delay = connection.get_delay(connection)

				if 0 < delay then
					if not self.get_input_bool(self, connection_name) then
						for delay_connection_name, delay_time in pairs(delay_time_map) do
							local down = self.get_input_bool(self, delay_connection_name)
							local allow = nil

							if down then
								if not delay_time then
									delay_time_map[delay_connection_name] = wall_time + delay
								elseif delay_time <= wall_time then
									allow = true
								end

								if not allow then
									self._input_bool_cache[delay_connection_name] = false
									self._input_pressed_cache[connection_name] = false
									self._input_float_cache[delay_connection_name] = 0
									self._input_axis_cache[delay_connection_name] = Vector3()
								end
							elseif delay_time then
								delay_time_map[delay_connection_name] = false
							end
						end
					else
						for delay_connection_name, delay_time in pairs(delay_time_map) do
							delay_time_map[delay_connection_name] = wall_time - delay
						end
					end
				end
			end
		end
	end

	return 
end
ControllerWrapper.add_destroy_callback = function (self, func)
	self._last_destroy_callback_id = self._last_destroy_callback_id + 1
	self._destroy_callback_list[self._last_destroy_callback_id] = func

	return self._last_destroy_callback_id
end
ControllerWrapper.remove_destroy_callback = function (self, id)
	self._destroy_callback_list[id] = nil

	return 
end
ControllerWrapper.add_connect_changed_callback = function (self, func)
	self._last_connect_changed_callback_id = self._last_connect_changed_callback_id + 1
	self._connect_changed_callback_list[self._last_connect_changed_callback_id] = func

	return self._last_connect_changed_callback_id
end
ControllerWrapper.remove_connect_changed_callback = function (self, id)
	self._connect_changed_callback_list[id] = nil

	return 
end
ControllerWrapper.add_rebind_callback = function (self, func)
	self._last_rebind_callback_id = self._last_rebind_callback_id + 1
	self._rebind_callback_list[self._last_rebind_callback_id] = func

	return self._last_rebind_callback_id
end
ControllerWrapper.remove_rebind_callback = function (self, id)
	self._rebind_callback_list[id] = nil

	return 
end
ControllerWrapper.rebind_connections = function (self, setup, setup_map)
	self.clear_connections(self, false)
	self.clear_triggers(self, true)
	self.setup(self, setup or self._setup)

	if self._enabled then
		self.restore_triggers(self)
	end

	for id, func in pairs(self._rebind_callback_list) do
		func(self, id)
	end

	return 
end
ControllerWrapper.setup = function (self, setup)
	if setup then
		self._setup = setup
		local connection_map = setup.get_connection_map(setup)

		for _, connection_name in ipairs(setup.get_connection_list(setup)) do
			local connection = connection_map[connection_name]
			local controller_id = connection.get_controller_id(connection) or self._default_controller_id
			local controller = self._controller_map[controller_id]

			self.setup_connection(self, connection_name, connection, controller_id, controller)
		end
	end

	return 
end
ControllerWrapper.setup_connection = function (self, connection_name, connection, controller_id, controller)
	if self._debug or not connection.get_debug(connection) then
		local input_name_list = connection.get_input_name_list(connection)

		for index, input_name in ipairs(input_name_list) do
			self.connect(self, controller_id, input_name, connection_name, connection, index ~= 1, 0 < #input_name_list and not connection.get_any_input(connection))
		end

		local delay_data = nil
		local delay_connection_list = connection.get_delay_connection_list(connection)

		for index, delay_connection_referer in ipairs(delay_connection_list) do
			local delay_connection_name = delay_connection_referer.get_name(delay_connection_referer)
			local delay_connection = self._setup:get_connection(delay_connection_name)

			if delay_connection then
				local delay_input_name_list = delay_connection.get_input_name_list(delay_connection)
				local can_delay = nil

				for _, delay_input_name in ipairs(delay_input_name_list) do
					for _, input_name in ipairs(input_name_list) do
						if delay_input_name == input_name then
							if not delay_data then
								delay_data = {
									delay_time_map = {},
									connection = connection
								}
							end

							delay_data.delay_time_map[delay_connection_name] = false
							self._delay_bool_map[delay_connection_name] = true
							can_delay = true

							break
						end
					end

					if can_delay then
						break
					end
				end
			else
				Application:error(self.to_string(self) .. " Unable to setup delay on non-existing connection \"" .. tostring(delay_connection_name) .. "\" in the \"" .. tostring(connection_name) .. "\" connection.")
			end
		end

		self._delay_map[connection_name] = delay_data

		if connection.IS_AXIS then
			self._current_lerp_axis_map[connection_name] = connection.get_init_lerp_axis(connection) or (self._virtual_controller and self._virtual_controller:axis(Idstring(connection_name))) or Vector3()
		end
	end

	return 
end
ControllerWrapper.connect = function (self, controller_id, input_name, connection_name, connection, allow_multiple, is_multi)
	local controller = self._controller_map[controller_id or self._default_controller_id]

	if controller then
		if self._virtual_controller then
			if not allow_multiple and self._connection_map[connection_name] then
				Application:error(self.to_string(self) .. " Controller already has a \"" .. tostring(connection_name) .. "\" connection. Overwrites existing one.")
			end

			self.virtual_connect(self, controller_id, controller, input_name, connection_name, connection)

			self._connection_map[connection_name] = true

			if is_multi then
				local single_connection_name_list = self._multi_input_map[connection_name] or {}
				local single_connection_name = tostring(connection_name) .. "_for_single_input_" .. tostring(input_name)

				self.virtual_connect(self, controller_id, controller, input_name, single_connection_name, connection)
				table.insert(single_connection_name_list, single_connection_name)

				self._multi_input_map[connection_name] = single_connection_name_list
			end
		else
			Application:stack_dump_error("Tried to connect to a destroyed virtual controller.")
		end
	else
		error("Invalid controller wrapper. Tried to connect to non-existing controller id \"" .. tostring(controller_id) .. "\".")
	end

	return 
end
ControllerWrapper.virtual_connect = function (self, controller_id, controller, input_name, connection_name, connection)
	local func = self._custom_virtual_connect_func_map[controller_id][input_name]

	if func then
		func(controller_id, controller, input_name, connection_name, connection)
	else
		self.virtual_connect2(self, controller_id, controller, input_name, connection_name, connection)
	end

	return 
end
ControllerWrapper.virtual_connect2 = function (self, controller_id, controller, input_name, connection_name, connection)
	local min_src, max_src, min_dest, max_dest = connection.get_range(connection)
	local connect_src_type = connection.get_connect_src_type(connection)
	local connect_dest_type = connection.get_connect_dest_type(connection)

	if connection._btn_connections and input_name == "buttons" then
		local btn_data = {
			up = {
				1,
				0,
				1
			},
			down = {
				1,
				0,
				-1
			},
			left = {
				0,
				0,
				-1
			},
			right = {
				0,
				0,
				1
			},
			accelerate = {
				1,
				0,
				1
			},
			brake = {
				1,
				0,
				-1
			},
			turn_left = {
				0,
				0,
				-1
			},
			turn_right = {
				0,
				0,
				1
			}
		}

		if not self._virtual_controller:has_axis(Idstring(connection_name)) then
			self._virtual_controller:add_axis(Idstring(connection_name))
		end

		for btn, input in pairs(connection._btn_connections) do
			if (controller.has_button(controller, Idstring(input.name)) and input.type == "button") or (controller.has_axis(controller, Idstring(input.name)) and input.type == "axis") then
				if input.type == "axis" then
					self._virtual_controller:connect(controller, Idstring("axis"), Idstring(input.name), tonumber(input.dir), Idstring("range"), tonumber(input.range1), tonumber(input.range2), Idstring("axis"), Idstring(connection_name), btn_data[btn][1], Idstring("range"), btn_data[btn][2], btn_data[btn][3])
				else
					self._virtual_controller:connect(controller, Idstring("button"), Idstring(input.name), Idstring("axis"), Idstring(connection_name), btn_data[btn][1], Idstring("range"), btn_data[btn][2], btn_data[btn][3])
				end
			end
		end
	else
		if type(input_name) ~= "number" then
			input_name = Idstring(input_name)
		end

		if controller.has_button(controller, input_name) or controller.has_axis(controller, input_name) then
			self._virtual_controller:connect(controller, Idstring(connect_src_type), input_name, Idstring("range"), min_src, max_src, Idstring(connect_dest_type), Idstring(connection_name), Idstring("range"), min_dest, max_dest)
		elseif self._virtual_controller:has_button(input_name) or self._virtual_controller:has_axis(input_name) then
			self._virtual_controller:connect(self._virtual_controller, Idstring(connect_src_type), input_name, Idstring("range"), min_src, max_src, Idstring(connect_dest_type), Idstring(connection_name), Idstring("range"), min_dest, max_dest)
		else
			Application:error("Invalid input name \"" .. tostring(input_name) .. "\". Controller type: \"" .. tostring(controller_id) .. "\", Connection name: \"" .. tostring(connection_name) .. "\".")
		end
	end

	return 
end
ControllerWrapper.connected = function (self, controller_id)
	if controller_id then
		return self._controller_map[controller_id]:connected()
	else
		for _, controller in pairs(self._controller_map) do
			if not controller.connected(controller) then
				return false
			end
		end

		return true
	end

	return 
end
ControllerWrapper.get_setup = function (self)
	return self._setup
end
ControllerWrapper.get_connection_settings = function (self, connection_name)
	return self._setup:get_connection(connection_name)
end
ControllerWrapper.get_default_controller_id = function (self)
	return self._default_controller_id
end
ControllerWrapper.get_type = function (self)
	return self.TYPE
end
ControllerWrapper.get_id = function (self)
	return self._id
end
ControllerWrapper.get_name = function (self)
	return self._name
end
ControllerWrapper.get_debug = function (self)
	return self._debug
end
ControllerWrapper.get_connection_map = function (self)
	return self._connection_map
end
ControllerWrapper.get_controller_map = function (self)
	return self._controller_map
end
ControllerWrapper.get_controller = function (self, controller_id)
	return self._controller_map[controller_id or self._default_controller_id]
end
ControllerWrapper.connection_exist = function (self, connection_name)
	return self._connection_map[connection_name] ~= nil
end
ControllerWrapper.set_enabled = function (self, bool)
	if bool then
		self.enable(self)
	else
		self.disable(self)
	end

	return 
end
ControllerWrapper.enable = function (self)
	self.set_active(self, true)

	return 
end
ControllerWrapper.disable = function (self)
	self.set_active(self, false)

	return 
end
ControllerWrapper._really_activate = function (self)
	ControllerWrapper.super._really_activate(self)

	if not self._enabled then
		cat_print("controller_manager", "[ControllerManager] Enabled controller \"" .. tostring(self._name) .. "\".")

		if self._virtual_controller then
			self._virtual_controller:set_enabled(true)
		end

		self._enabled = true

		self.clear_triggers(self, true)
		self.restore_triggers(self)
		self.reset_cache(self, false)

		self._was_connected = self.connected(self)
	end

	return 
end
ControllerWrapper._really_deactivate = function (self)
	ControllerWrapper.super._really_deactivate(self)

	if self._enabled then
		cat_print("controller_manager", "[ControllerManager] Disabled controller \"" .. tostring(self._name) .. "\".")

		self._enabled = false

		self.clear_triggers(self, true)
		self.reset_cache(self, false)

		if self._virtual_controller then
			self._virtual_controller:set_enabled(false)
		end
	end

	return 
end
ControllerWrapper.enabled = function (self)
	return self._enabled
end
ControllerWrapper.is_claimed = function (self)
	return self._claimed
end
ControllerWrapper.set_claimed = function (self, bool)
	self._claimed = bool

	return 
end
ControllerWrapper.add_trigger = function (self, connection_name, func)
	local trigger = {}
	self._trigger_map[connection_name] = self._trigger_map[connection_name] or {}

	if self._trigger_map[connection_name][func] then
		Application:error(self.to_string(self) .. " Unable to register already existing trigger for function \"" .. tostring(func) .. "\" on connection \"" .. tostring(connection_name) .. "\".")

		return 
	end

	trigger.original_func = func
	trigger.func = self.get_trigger_func(self, connection_name, func)

	if self._enabled and self._virtual_controller and self.get_connection_enabled(self, connection_name) then
		trigger.id = self._virtual_controller:add_trigger(Idstring(connection_name), trigger.func)
	end

	self._trigger_map[connection_name][func] = trigger

	return 
end
ControllerWrapper.add_release_trigger = function (self, connection_name, func)
	local trigger = {}
	self._release_trigger_map[connection_name] = self._release_trigger_map[connection_name] or {}
	trigger.original_func = func
	trigger.func = self.get_release_trigger_func(self, connection_name, func)

	if self._virtual_controller and self.get_connection_enabled(self, connection_name) then
		trigger.id = self._virtual_controller:add_release_trigger(Idstring(connection_name), trigger.func)
	end

	self._release_trigger_map[connection_name][func] = trigger

	return 
end
ControllerWrapper.get_trigger_func = function (self, connection_name, func)
	local wrapper = self

	if self._delay_bool_map[connection_name] or self._multi_input_map[connection_name] then
		return function (...)
			wrapper:reset_cache(true)

			if wrapper:get_input_bool(connection_name) then
				func(...)
			else
				wrapper:queue_delay_trigger(connection_name, func, ...)
			end

			return 
		end
	else
		return function (...)
			wrapper:reset_cache(true)
			func(...)

			return 
		end
	end

	return 
end
ControllerWrapper.get_release_trigger_func = function (self, connection_name, func)
	local wrapper = self

	if self._delay_bool_map[connection_name] or self._multi_input_map[connection_name] then
		return function (...)
			wrapper:reset_cache(true)

			if wrapper:get_input_bool(connection_name) then
				func(...)
			end

			return 
		end
	else
		return function (...)
			wrapper:reset_cache(true)
			func(...)

			return 
		end
	end

	return 
end
ControllerWrapper.queue_delay_trigger = function (self, connection_name, func, ...)
	self._delay_trigger_queue[connection_name] = {
		func = func,
		func_params = {
			...
		}
	}

	return 
end
ControllerWrapper.has_trigger = function (self, connection_name, func)
	local trigger_sub_map = self._trigger_map[connection_name]

	return trigger_sub_map and trigger_sub_map[func]
end
ControllerWrapper.has_release_trigger = function (self, connection_name, func)
	local release_trigger_sub_map = self._release_trigger_map[connection_name]

	return release_trigger_sub_map and release_trigger_sub_map[func]
end
ControllerWrapper.remove_trigger = function (self, connection_name, func)
	local trigger_sub_map = self._trigger_map[connection_name]

	if trigger_sub_map then
		if func then
			local trigger = trigger_sub_map[func]

			if trigger then
				local queued_trigger = self._delay_trigger_queue[connection_name]

				if queued_trigger and trigger.original_func == queued_trigger.func then
					self._delay_trigger_queue[connection_name] = nil
				end

				if trigger.id then
					self._virtual_controller:remove_trigger(trigger.id)

					trigger.id = nil
				end
			else
				Application:error(self.to_string(self) .. " Unable to remove non-existing trigger for function \"" .. tostring(func) .. "\" on connection \"" .. tostring(connection_name) .. "\".")
			end

			trigger_sub_map[func] = nil

			if not next(trigger_sub_map) then
				trigger_sub_map = nil
			end
		else
			self._delay_trigger_queue[connection_name] = nil

			for _, trigger in pairs(trigger_sub_map) do
				self._virtual_controller:remove_trigger(trigger.id)

				trigger.id = nil
			end

			trigger_sub_map = nil
		end

		self._trigger_map[connection_name] = trigger_sub_map
	else
		Application:error(self.to_string(self) .. " Unable to remove trigger on non-existing connection \"" .. tostring(connection_name) .. "\".")
	end

	return 
end
ControllerWrapper.remove_release_trigger = function (self, connection_name, func)
	local trigger_sub_map = self._release_trigger_map[connection_name]

	if trigger_sub_map then
		if func then
			trigger = trigger_sub_map[func]

			if trigger then
				if trigger.id then
					self._virtual_controller:remove_trigger(trigger.id)

					trigger.id = nil
				end
			else
				Application:error(self.to_string(self) .. " Unable to remove non-existing release trigger for function \"" .. tostring(func) .. "\" on connection \"" .. tostring(connection_name) .. "\".")
			end

			trigger_sub_map[func] = nil

			if not next(trigger_sub_map) then
				trigger_sub_map = nil
			end
		else
			for _, trigger in pairs(trigger_sub_map) do
				self._virtual_controller:remove_trigger(trigger.id)

				trigger.id = nil
			end

			trigger_sub_map = nil
		end

		self._release_trigger_map[connection_name] = trigger_sub_map
	else
		Application:error(self.to_string(self) .. " Unable to remove release trigger on non-existing connection \"" .. tostring(connection_name) .. "\".")
	end

	return 
end
ControllerWrapper.clear_triggers = function (self, temporary)
	if self._virtual_controller then
		self._virtual_controller:clear_triggers()
	end

	self._delay_trigger_queue = {}

	if temporary then
		for _, trigger_sub_map in pairs(self._trigger_map) do
			for _, trigger in pairs(trigger_sub_map) do
				trigger.id = nil
			end
		end

		for _, release_trigger_sub_map in pairs(self._release_trigger_map) do
			for _, release_trigger in pairs(release_trigger_sub_map) do
				release_trigger.id = nil
			end
		end
	else
		self._trigger_map = {}
		self._release_trigger_map = {}
	end

	return 
end
ControllerWrapper.restore_triggers = function (self)
	if self._virtual_controller then
		for connection_name, trigger_sub_map in pairs(self._trigger_map) do
			for _, trigger in pairs(trigger_sub_map) do
				if self.get_connection_enabled(self, connection_name) then
					trigger.id = self._virtual_controller:add_trigger(Idstring(connection_name), trigger.func)
				end
			end
		end

		for connection_name, trigger_sub_map in pairs(self._release_trigger_map) do
			for _, trigger in pairs(trigger_sub_map) do
				if self.get_connection_enabled(self, connection_name) then
					trigger.id = self._virtual_controller:add_release_trigger(Idstring(connection_name), trigger.func)
				end
			end
		end
	end

	return 
end
ControllerWrapper.clear_connections = function (self, temporary)
	if self._virtual_controller then
		self._virtual_controller:clear_connections()
	end

	if not temporary then
		self._connection_map = {}
		self._delay_map = {}
		self._delay_bool_map = {}
		self._multi_input_map = {}
	end

	return 
end
ControllerWrapper.get_any_input = function (self)
	if self._input_any_cache == nil then
		self._input_any_cache = self._enabled and self._virtual_controller and 0 < #self._virtual_controller:down_list()
		self._input_any_cache = not not self._input_any_cache
	end

	return self._input_any_cache
end
ControllerWrapper.get_any_input_pressed = function (self)
	if self._input_any_pressed_cache == nil then
		self._input_any_pressed_cache = self._enabled and self._virtual_controller and 0 < #self._virtual_controller:pressed_list()
		self._input_any_pressed_cache = not not self._input_any_pressed_cache
	end

	return self._input_any_pressed_cache
end
ControllerWrapper.get_any_input_released = function (self)
	if self._input_any_released_cache == nil then
		self._input_any_released_cache = self._enabled and self._virtual_controller and 0 < #self._virtual_controller:released_list()
		self._input_any_released_cache = not not self._input_any_released_cache
	end

	return self._input_any_released_cache
end
local id_strings = {}
ControllerWrapper.get_input_pressed = function (self, connection_name)
	local cache = self._input_pressed_cache[connection_name]

	if cache == nil then
		if self._connection_map[connection_name] then
			id_strings[connection_name] = id_strings[connection_name] or Idstring(connection_name)
			local ids = id_strings[connection_name]
			cache = (self._enabled and self._virtual_controller and self.get_connection_enabled(self, connection_name) and self._virtual_controller:pressed(ids)) or false
			cache = not not cache
		else
			self.print_invalid_connection_error(self, connection_name)

			cache = false
		end

		self._input_pressed_cache[connection_name] = cache
	end

	return cache
end
ControllerWrapper.print_invalid_connection_error = function (self, connection_name)
	ControllerWrapper.INVALID_CONNECTION_ERROR = ControllerWrapper.INVALID_CONNECTION_ERROR or {}

	if not ControllerWrapper.INVALID_CONNECTION_ERROR[connection_name] then
		Application:stack_dump_error(self.to_string(self) .. " No controller input binded to connection \"" .. tostring(connection_name) .. "\".")

		ControllerWrapper.INVALID_CONNECTION_ERROR[connection_name] = true
	end

	return 
end
ControllerWrapper.get_input_bool = function (self, connection_name)
	local cache = self._input_bool_cache[connection_name]

	if cache == nil then
		if self._connection_map[connection_name] then
			id_strings[connection_name] = id_strings[connection_name] or Idstring(connection_name)
			local ids = id_strings[connection_name]
			cache = (self._enabled and self._virtual_controller and self.get_connection_enabled(self, connection_name) and self._virtual_controller:down(ids)) or false
			cache = not not cache
		else
			self.print_invalid_connection_error(self, connection_name)

			cache = false
		end

		self._input_bool_cache[connection_name] = cache
	end

	return cache
end
ControllerWrapper.get_input_float = function (self, connection_name)
	local cache = self._input_float_cache[connection_name]

	if cache == nil then
		if self._connection_map[connection_name] then
			id_strings[connection_name] = id_strings[connection_name] or Idstring(connection_name)
			local ids = id_strings[connection_name]
			cache = (self._enabled and self._virtual_controller and self.get_connection_enabled(self, connection_name) and self._virtual_controller:button(ids)) or 0
		else
			self.print_invalid_connection_error(self, connection_name)

			cache = 0
		end

		self._input_float_cache[connection_name] = cache
	end

	return cache
end
ControllerWrapper.get_input_axis_clbk = function (self, connection_name, func)
	if not self.enabled(self) then
		return 
	end

	local id = id_strings[connection_name]

	if not id then
		id = Idstring(connection_name)
		id_strings[connection_name] = id
	end

	local connection = self._setup:get_connection(connection_name)

	local function f(axis_id, controller_name, axis)
		local unscaled_axis = mvector3.copy(axis)

		func(self:get_modified_axis(connection_name, connection, axis), self:get_unscaled_axis(connection_name, connection, unscaled_axis))

		return 
	end

	self._virtual_controller:add_axis_trigger(id, f)

	return 
end
ControllerWrapper.get_input_axis = function (self, connection_name)
	local cache = self._input_axis_cache[connection_name]

	if cache == nil then
		if self._connection_map[connection_name] then
			id_strings[connection_name] = id_strings[connection_name] or Idstring(connection_name)
			local ids = id_strings[connection_name]
			cache = self._enabled and self._virtual_controller and self.get_connection_enabled(self, connection_name) and self._virtual_controller:axis(ids)

			if cache then
				local connection = self._setup:get_connection(connection_name)
				cache = self.get_modified_axis(self, connection_name, connection, cache)
			else
				cache = Vector3()
			end
		else
			self.print_invalid_connection_error(self, connection_name)

			cache = Vector3()
		end

		self._input_axis_cache[connection_name] = cache
	end

	return cache
end
ControllerWrapper.get_unscaled_axis = function (self, connection_name, connection, axis)
	local inversion = connection.get_inversion and connection.get_inversion(connection)

	if inversion then
		mvector3.set_static(axis, mvector3.x(axis)*inversion.x, mvector3.y(axis)*inversion.y, mvector3.z(axis)*inversion.z)
	end

	return axis
end
ControllerWrapper.get_modified_axis = function (self, connection_name, connection, axis)
	local multiplier = connection.get_multiplier and connection.get_multiplier(connection)

	if multiplier then
		mvector3.set_static(axis, mvector3.x(axis)*multiplier.x, mvector3.y(axis)*multiplier.y, mvector3.z(axis)*multiplier.z)
	end

	local inversion = connection.get_inversion and connection.get_inversion(connection)

	if inversion then
		mvector3.set_static(axis, mvector3.x(axis)*inversion.x, mvector3.y(axis)*inversion.y, mvector3.z(axis)*inversion.z)
	end

	local x = self.rescale_x_axis(self, connection_name, connection, axis.x)
	local y = self.rescale_y_axis(self, connection_name, connection, axis.y)
	local z = self.rescale_z_axis(self, connection_name, connection, axis.z)

	mvector3.set_static(axis, x, y, z)

	return self.lerp_axis(self, connection_name, connection, axis)
end
ControllerWrapper.lerp_axis = function (self, connection_name, connection, axis)
	local lerp = connection.get_lerp and connection.get_lerp(connection)

	if lerp then
		local current_axis = self._current_lerp_axis_map[connection_name]

		mvector3.lerp(axis, current_axis, axis, lerp)

		self._current_lerp_axis_map[connection_name] = axis
	end

	return axis
end
ControllerWrapper.rescale_x_axis = function (self, connection_name, connection, x)
	return self.rescale_axis_component(self, connection_name, connection, x)
end
ControllerWrapper.rescale_y_axis = function (self, connection_name, connection, y)
	return self.rescale_axis_component(self, connection_name, connection, y)
end
ControllerWrapper.rescale_z_axis = function (self, connection_name, connection, z)
	return self.rescale_axis_component(self, connection_name, connection, z)
end
ControllerWrapper.rescale_axis_component = function (self, connection_name, connection, comp)
	return comp
end
ControllerWrapper.set_connection_enabled = function (self, connection_name, enabled)
	local connection = self._connection_map[connection_name] and self._setup:get_connection(connection_name)

	if connection then
		if not connection.get_enabled(connection) ~= not enabled then
			connection.set_enabled(connection, enabled)

			local trigger_sub_map = self._trigger_map[connection_name]

			if trigger_sub_map then
				for _, trigger in pairs(trigger_sub_map) do
					if enabled then
						if not trigger.id then
							trigger.id = self._virtual_controller:add_trigger(Idstring(connection_name), trigger.func)
						end
					elseif trigger.id then
						self._virtual_controller:remove_trigger(trigger.id)

						trigger.id = nil
					end
				end
			end

			trigger_sub_map = self._release_trigger_map[connection_name]

			if trigger_sub_map then
				for _, trigger in pairs(trigger_sub_map) do
					if enabled then
						if not trigger.id then
							trigger.id = self._virtual_controller:add_release_trigger(Idstring(connection_name), trigger.func)
						end
					elseif trigger.id then
						self._virtual_controller:remove_trigger(trigger.id)

						trigger.id = nil
					end
				end
			end

			if not enabled then
				self._delay_trigger_queue[connection_name] = nil
			end

			local delay_data = self._delay_map[connection_name]

			if delay_data then
				for delay_connection_name in pairs(delay_data.delay_time_map) do
					local trigger_sub_map = self._trigger_map[delay_connection_name]

					if trigger_sub_map then
						for _, trigger in pairs(trigger_sub_map) do
							trigger.func = self.get_trigger_func(self, delay_connection_name, trigger.original_func)

							if trigger.id then
								self._virtual_controller:remove_trigger(trigger.id)

								trigger.id = self._virtual_controller:add_trigger(Idstring(delay_connection_name), trigger.func)
							end
						end
					end

					local release_trigger_sub_map = self._release_trigger_map[delay_connection_name]

					if release_trigger_sub_map then
						for _, release_trigger in pairs(release_trigger_sub_map) do
							release_trigger.func = self.get_trigger_func(self, delay_connection_name, release_trigger.original_func)

							if release_trigger.id then
								self._virtual_controller:remove_trigger(release_trigger.id)

								release_trigger.id = self._virtual_controller:add_release_trigger(Idstring(delay_connection_name), release_trigger.func)
							end
						end
					end
				end

				self.update_delay_trigger_queue(self)
			end
		end
	else
		Application:error(self.to_string(self) .. " Controller can't enable connection \"" .. tostring(connection_name) .. "\" because it doesn't exist.")
	end

	return 
end
ControllerWrapper.get_connection_enabled = function (self, connection_name)
	local connection = self._connection_map[connection_name] and self._setup:get_connection(connection_name)
	local ret = connection and connection.get_enabled(connection)

	return ret
end
ControllerWrapper.to_string = function (self)
	return self.__tostring(self)
end
ControllerWrapper.__tostring = function (self)
	return string.format("[Controller][Wrapper][ID: %s, Type: %s, Name: %s, Enabled: %s, Debug: %s]", tostring(self._id), tostring(self.get_type(self)), tostring(self._name or "N/A"), tostring((self._enabled and "Yes") or "No"), tostring((self._debug and "Yes") or "No"))
end
ControllerWrapper.change_mode = function (controller, mode)
	return nil
end

return 
