core:module("CoreShapeManager")
core:import("CoreXml")
core:import("CoreMath")

ShapeManager = ShapeManager or class()
ShapeManager.init = function (self)
	self._shapes = {}
	self._shape_types = {
		box = ShapeBox,
		sphere = ShapeSphere,
		cylinder = ShapeCylinder,
		box_middle = ShapeBoxMiddle
	}

	return 
end
ShapeManager.update = function (self, t, dt)
	for _, shape in ipairs(self._shapes) do
		shape.draw(shape, t, dt, 0.8, 0.8)
	end

	return 
end
ShapeManager.add_shape = function (self, type, params)
	params.type = type
	local shape = self._shape_types[type]:new(params)

	table.insert(self._shapes, shape)

	return shape
end
ShapeManager.shape_type = function (self, type)
	return self._shape_types[type]
end
ShapeManager.remove_shape = function (self, shape)
	shape.destroy(shape)
	table.delete(self._shapes, shape)

	return 
end
ShapeManager.clear_shapes = function (self)
	for _, shape in ipairs(clone(self._shapes)) do
		self.remove_shape(self, shape)
	end

	return 
end
ShapeManager.save = function (self)
	return 
end
ShapeManager.parse = function (self, shape)
	local t = {
		type = shape.parameter(shape, "type"),
		position = math.string_to_vector(shape.parameter(shape, "position")),
		rotation = math.string_to_rotation(shape.parameter(shape, "rotation"))
	}

	for properties in shape.children(shape) do
		for value in properties.children(properties) do
			t[value.parameter(value, "name")] = CoreMath.string_to_value(value.parameter(value, "type"), value.parameter(value, "value"))
		end
	end

	return t
end
local mvec1 = Vector3()
local mvec2 = Vector3()
local mvec3 = Vector3()
local mposition = Vector3()
Shape = Shape or class()
Shape.init = function (self, params)
	self._name = params.name or ""
	self._type = params.type or "none"
	self._position = params.position or Vector3()
	self._rotation = params.rotation or Rotation()
	self._properties = {}

	if Application:editor() then
		self._properties_ctrls = {}
		self._min_value = 10
		self._max_value = 10000000
	end

	return 
end
Shape.build_dialog = function (self)
	if not Application:editor() then
		return 
	end

	self._dialog = EWS:Dialog(nil, "Shape properties", "", Vector3(200, 100, 0), Vector3(750, 600, 0), "DEFAULT_DIALOG_STYLE,RESIZE_BORDER,STAY_ON_TOP")
	self._dialog_sizer = EWS:BoxSizer("VERTICAL")

	self._dialog:set_sizer(self._dialog_sizer)
	self.build_properties_ctrls(self)

	return 
end
Shape.build_properties_ctrls = function (self)
	return 
end
Shape.name = function (self)
	return (self._unit and self._unit:unit_data().name_id) or self._name
end
Shape.unit = function (self)
	return self._unit
end
Shape.set_unit = function (self, unit)
	self._unit = unit

	return 
end
Shape.position = function (self)
	return (self._unit and self._unit:position()) or self._position
end
Shape.set_position = function (self, position)
	self._position = position

	return 
end
Shape.rotation = function (self)
	return (self._unit and self._unit:rotation()) or self._rotation
end
Shape.set_rotation = function (self, rotation)
	self._rotation = rotation

	return 
end
Shape.properties = function (self)
	return self._properties
end
Shape.property = function (self, property)
	return self._properties[property]
end
Shape.set_property = function (self, property, value)
	if not self._properties[property] then
		return 
	end

	value = math.clamp(value, self._min_value, self._max_value)
	self._properties[property] = value

	if self._properties_ctrls and self._properties_ctrls[property] then
		for _, ctrl in ipairs(self._properties_ctrls[property]) do
			ctrl.set_value(ctrl, string.format("%.2f", value/100))
		end
	end

	return 
end
Shape.set_property_string = function (self, property, value)
	self._properties[property] = value

	return 
end
Shape.scale = function (self)
	return 
end
Shape.set_dialog_visible = function (self, visible)
	if not self._dialog then
		self.build_dialog(self)
	end

	self._dialog:set_visible(visible)

	return 
end
Shape.panel = function (self, panel, sizer)
	if not self._panel and panel and sizer then
		self.create_panel(self, panel, sizer)
	end

	return self._panel
end
Shape.create_panel = function (self, parent, sizer)
	self._panel = EWS:Panel(parent, "", "TAB_TRAVERSAL")

	self._panel:set_extension({
		alive = true
	})

	self._panel_sizer = EWS:BoxSizer("VERTICAL")

	self._panel:set_sizer(self._panel_sizer)
	sizer.add(sizer, self._panel, 0, 0, "EXPAND")

	return 
end
Shape._create_size_ctrl = function (self, name, property, value, parent, sizer)
	local ctrl_sizer = EWS:BoxSizer("HORIZONTAL")

	ctrl_sizer.add(ctrl_sizer, EWS:StaticText(parent, name, "", "ALIGN_LEFT"), 2, 0, "EXPAND")

	local ctrl = EWS:TextCtrl(parent, string.format("%.2f", value/100), "", "TE_PROCESS_ENTER")

	ctrl.set_min_size(ctrl, Vector3(-1, 10, 0))

	local spin = EWS:SpinButton(parent, "", "SP_VERTICAL")

	spin.set_min_size(spin, Vector3(-1, 10, 0))

	local slider = EWS:Slider(parent, 100, 1, 200, "", "")

	ctrl.connect(ctrl, "EVT_CHAR", callback(nil, _G, "verify_number"), ctrl)
	ctrl.set_tool_tip(ctrl, "Type in property " .. name)
	ctrl.connect(ctrl, "EVT_COMMAND_TEXT_ENTER", callback(self, self, "update_size"), {
		ctrl = ctrl,
		property = property
	})
	ctrl.connect(ctrl, "EVT_KILL_FOCUS", callback(self, self, "update_size"), {
		ctrl = ctrl,
		property = property
	})
	spin.connect(spin, "EVT_SCROLL_LINEUP", callback(self, self, "update_size_spin"), {
		step = 0.1,
		ctrl = ctrl,
		property = property
	})
	spin.connect(spin, "EVT_SCROLL_LINEDOWN", callback(self, self, "update_size_spin"), {
		step = -0.1,
		ctrl = ctrl,
		property = property
	})

	local params = {
		ctrl = ctrl,
		slider = slider,
		property = property
	}

	slider.connect(slider, "EVT_SCROLL_CHANGED", callback(self, self, "update_slider_size"), params)
	slider.connect(slider, "EVT_SCROLL_THUMBTRACK", callback(self, self, "update_slider_size"), params)
	slider.connect(slider, "EVT_SCROLL_CHANGED", callback(self, self, "update_slider_release"), params)
	slider.connect(slider, "EVT_SCROLL_THUMBRELEASE", callback(self, self, "update_slider_release"), params)
	ctrl_sizer.add(ctrl_sizer, ctrl, 2, 0, "EXPAND")
	ctrl_sizer.add(ctrl_sizer, spin, 0, 0, "EXPAND")
	ctrl_sizer.add(ctrl_sizer, slider, 2, 0, "EXPAND")

	self._properties_ctrls[property] = self._properties_ctrls[property] or {}

	table.insert(self._properties_ctrls[property], ctrl)
	sizer.add(sizer, ctrl_sizer, 1, 0, "EXPAND")

	return ctrl
end
Shape.connect_event = function (self, name, event, callback, params)
	local ctrls = self._properties_ctrls[name] or {}

	for _, ctrl in ipairs(ctrls) do
		ctrl.connect(ctrl, event, callback, params)
	end

	return 
end
Shape.update_size = function (self, data)
	local value = data.ctrl:get_value()

	self.set_property(self, data.property, value*100)
	data.ctrl:set_selection(-1, -1)

	return 
end
Shape.update_size_spin = function (self, data)
	local value = data.ctrl:get_value() + data.step

	self.set_property(self, data.property, value*100)

	return 
end
Shape.update_slider_size = function (self, data)
	data.start_value = data.start_value or data.ctrl:get_value()
	local value = data.start_value

	self.set_property(self, data.property, value*data.slider:get_value()/100*100)

	return 
end
Shape.update_slider_release = function (self, data)
	local value = data.start_value

	self.set_property(self, data.property, value*data.slider:get_value()/100*100)

	data.start_value = nil

	data.slider:set_value(100)

	return 
end
Shape.draw = function (self, t, dt, r, g, b)
	return 
end
Shape.is_inside = function (self, pos)
	return 
end
Shape.is_outside = function (self, pos)
	return 
end
Shape.save = function (self, t)
	local t = t or ""
	local s = t
	local pos = CoreMath.vector_to_string(self.position(self), "%.4f")
	local rot = CoreMath.rotation_to_string(self.rotation(self), "%.4f")
	s = s .. "<shape type=\"" .. self._type .. "\" position=\"" .. pos .. "\" rotation=\"" .. rot .. "\">\n"
	s = s .. CoreXml.save_value_string(self, "_properties", t .. "\t") .. "\n"
	s = s .. t .. "</shape>"

	return s
end
Shape.save_level_data = function (self)
	local t = {
		type = self._type,
		position = self.position(self),
		rotation = self.rotation(self)
	}

	for name, value in pairs(self._properties) do
		t[name] = value
	end

	return t
end
Shape.destroy = function (self)
	if self._panel then
		self._panel:extension().alive = false

		self._panel:destroy()
	end

	if self._dialog then
		self._dialog:destroy()
	end

	return 
end
ShapeBox = ShapeBox or class(Shape)
ShapeBox.init = function (self, params)
	Shape.init(self, params)

	self._properties.width = params.width or 1000
	self._properties.depth = params.depth or 1000
	self._properties.height = params.height or 1000

	return 
end
ShapeBox.create_panel = function (self, parent, sizer)
	Shape.create_panel(self, parent, sizer)

	local width = self._create_size_ctrl(self, "Width [m]", "width", self._properties.width, self._panel, self._panel_sizer)
	local depth = self._create_size_ctrl(self, "Depth [m]", "depth", self._properties.depth, self._panel, self._panel_sizer)
	local height = self._create_size_ctrl(self, "Height [m]", "height", self._properties.height, self._panel, self._panel_sizer)

	self._panel:set_min_size(Vector3(-1, 70, 0))

	return width, depth, height
end
ShapeBox.build_properties_ctrls = function (self)
	if not Application:editor() then
		return 
	end

	self._create_size_ctrl(self, "Width [m]", "width", self._properties.width, self._dialog, self._dialog_sizer)
	self._create_size_ctrl(self, "Depth [m]", "depth", self._properties.depth, self._dialog, self._dialog_sizer)
	self._create_size_ctrl(self, "Height [m]", "height", self._properties.height, self._dialog, self._dialog_sizer)
	self._dialog:set_size(Vector3(190, 90, 0))

	return 
end
ShapeBox.size = function (self)
	return Vector3(self._properties.width, self._properties.depth, self._properties.height)
end
ShapeBox.width = function (self)
	return self._properties.width
end
ShapeBox.set_width = function (self, width)
	self.set_property(self, "width", width)

	return 
end
ShapeBox.depth = function (self)
	return self._properties.depth
end
ShapeBox.set_depth = function (self, depth)
	self.set_property(self, "depth", depth)

	return 
end
ShapeBox.height = function (self)
	return self._properties.height
end
ShapeBox.set_height = function (self, height)
	self.set_property(self, "height", height)

	return 
end
ShapeBox.still_inside = function (self, pos)
	return self.is_inside(self, pos)
end
ShapeBox.is_inside = function (self, pos)
	mvector3.set(mvec1, pos)
	mvector3.subtract(mvec1, self.position(self))

	local rot = self.rotation(self)

	mrotation.x(rot, mvec2)

	local inside = mvector3.dot(mvec2, mvec1)

	if 0 < inside and inside < self._properties.width then
		mrotation.y(rot, mvec2)

		inside = mvector3.dot(mvec2, mvec1)

		if 0 < inside and inside < self._properties.depth then
			mrotation.z(rot, mvec2)

			inside = mvector3.dot(mvec2, mvec1)

			if 0 < inside and inside < self._properties.height then
				return true
			end
		end
	end

	return false
end
ShapeBox.draw = function (self, t, dt, r, g, b)
	local brush = Draw:brush()

	brush.set_color(brush, Color(0.5, r, g, b))

	local pos = self.position(self)
	local rot = self.rotation(self)
	pos = pos + (rot.x(rot)*self._properties.width)/2 + (rot.y(rot)*self._properties.depth)/2 + (rot.z(rot)*self._properties.height)/2

	brush.box(brush, pos, (rot.x(rot)*self._properties.width)/2, (rot.y(rot)*self._properties.depth)/2, (rot.z(rot)*self._properties.height)/2)
	self.draw_outline(self, t, dt, r, g, b)

	return 
end
ShapeBox.draw_outline = function (self, t, dt, r, g, b)
	local rot = self.rotation(self)

	Application:draw_box_rotation(self.position(self), rot, self._properties.width, self._properties.depth, self._properties.height, r, g, b)

	return 
end
ShapeBoxMiddle = ShapeBoxMiddle or class(ShapeBox)
ShapeBoxMiddle.init = function (self, params)
	ShapeBox.init(self, params)

	return 
end
ShapeBoxMiddle.is_inside = function (self, pos)
	local rot = self.rotation(self)
	local x = mvec1
	local y = mvec2
	local z = mvec3

	mrotation.x(rot, x)
	mvector3.multiply(x, self._properties.width/2)
	mrotation.y(rot, y)
	mvector3.multiply(y, self._properties.depth/2)
	mrotation.z(rot, z)
	mvector3.multiply(z, self._properties.height/2)

	local position = mposition

	mvector3.set(position, self.position(self))
	mvector3.subtract(position, x)
	mvector3.subtract(position, y)
	mvector3.subtract(position, z)

	local pos_dir = position

	mvector3.multiply(pos_dir, -1)
	mvector3.add(pos_dir, pos)
	mrotation.x(rot, x)

	local inside = mvector3.dot(x, pos_dir)

	if 0 < inside and inside < self._properties.width then
		mrotation.y(rot, y)

		inside = mvector3.dot(y, pos_dir)

		if 0 < inside and inside < self._properties.depth then
			mrotation.z(rot, z)

			inside = mvector3.dot(z, pos_dir)

			if 0 < inside and inside < self._properties.height then
				return true
			end
		end
	end

	return false
end
ShapeBoxMiddle.draw = function (self, t, dt, r, g, b, a)
	local brush = Draw:brush()

	brush.set_color(brush, Color(a or 0.5, r, g, b))

	local pos = self.position(self)
	local rot = self.rotation(self)

	brush.box(brush, pos, (rot.x(rot)*self._properties.width)/2, (rot.y(rot)*self._properties.depth)/2, (rot.z(rot)*self._properties.height)/2)

	local c1 = self.position(self) - (rot.x(rot)*self._properties.width)/2 - (rot.y(rot)*self._properties.depth)/2 - (rot.z(rot)*self._properties.height)/2

	Application:draw_box_rotation(c1, rot, self._properties.width, self._properties.depth, self._properties.height, r, g, b)

	return 
end
ShapeBoxMiddleBottom = ShapeBoxMiddleBottom or class(ShapeBox)
ShapeBoxMiddleBottom.init = function (self, params)
	ShapeBox.init(self, params)

	return 
end
ShapeBoxMiddleBottom.is_inside = function (self, pos)
	local rot = self.rotation(self)
	local x = (rot.x(rot)*self._properties.width)/2
	local y = (rot.y(rot)*self._properties.depth)/2
	local position = self.position(self) - x - y
	local pos_dir = pos - position
	local inside = rot.x(rot):dot(pos_dir)

	if 0 < inside and inside < self._properties.width then
		inside = rot.y(rot):dot(pos_dir)

		if 0 < inside and inside < self._properties.depth then
			inside = rot.z(rot):dot(pos_dir)

			if 0 < inside and inside < self._properties.height then
				return true
			end
		end
	end

	return false
end
ShapeBoxMiddleBottom.draw = function (self, t, dt, r, g, b)
	local brush = Draw:brush()

	brush.set_color(brush, Color(0.5, r, g, b))

	local pos = self.position(self)
	local rot = self.rotation(self)
	pos = pos + (rot.z(rot)*self._properties.height)/2

	brush.box(brush, pos, (rot.x(rot)*self._properties.width)/2, (rot.y(rot)*self._properties.depth)/2, (rot.z(rot)*self._properties.height)/2)

	local c1 = self.position(self) - (rot.x(rot)*self._properties.width)/2 - (rot.y(rot)*self._properties.depth)/2

	Application:draw_box_rotation(c1, rot, self._properties.width, self._properties.depth, self._properties.height, r, g, b)

	return 
end
ShapeSphere = ShapeSphere or class(Shape)
ShapeSphere.init = function (self, params)
	Shape.init(self, params)

	self._properties.radius = params.radius or 1000

	return 
end
ShapeSphere.build_properties_ctrls = function (self)
	if not Application:editor() then
		return 
	end

	self._create_size_ctrl(self, "Radius [m]", "radius", self._properties.radius, self._dialog_sizer)
	self._dialog:set_size(Vector3(190, 50, 0))

	return 
end
ShapeSphere.radius = function (self)
	return self._properties.radius
end
ShapeSphere.set_radius = function (self, radius)
	self.set_property(self, "radius", radius)

	return 
end
ShapeSphere.is_inside = function (self, pos)
	return pos - self.position(self):length() < self._properties.radius
end
ShapeSphere.draw = function (self, t, dt, r, g, b)
	local brush = Draw:brush()

	brush.set_color(brush, Color(0.5, r, g, b))
	brush.sphere(brush, self.position(self), self._properties.radius, 4)
	Application:draw_sphere(self.position(self), self._properties.radius, r, g, b)

	return 
end
ShapeCylinder = ShapeCylinder or class(Shape)
ShapeCylinder.init = function (self, params)
	Shape.init(self, params)

	self._properties.radius = params.radius or 1000
	self._properties.height = params.height or 1000

	return 
end
ShapeCylinder.build_properties_ctrls = function (self)
	if not Application:editor() then
		return 
	end

	self._create_size_ctrl(self, "Radius [m]", "radius", self._properties.radius, self._dialog, self._dialog_sizer)
	self._create_size_ctrl(self, "Height [m]", "height", self._properties.height, self._dialog, self._dialog_sizer)
	self._dialog:set_size(Vector3(190, 70, 0))

	return 
end
ShapeCylinder.radius = function (self)
	return self._properties.radius
end
ShapeCylinder.set_radius = function (self, radius)
	self.set_property(self, "radius", radius)

	return 
end
ShapeCylinder.height = function (self)
	return self._properties.height
end
ShapeCylinder.set_height = function (self, height)
	self.set_property(self, "height", height)

	return 
end
ShapeCylinder.draw = function (self, t, dt, r, g, b)
	local brush = Draw:brush()

	brush.set_color(brush, Color(0.5, r, g, b))

	local pos = self.position(self)
	local rot = self.rotation(self)

	brush.cylinder(brush, pos, pos + rot.z(rot)*self._properties.height, self._properties.radius, 100)
	Application:draw_cylinder(pos, pos + rot.z(rot)*self._properties.height, self._properties.radius, r, g, b)

	return 
end
ShapeCylinder.is_inside = function (self, pos)
	local pos_dir = pos - self.position(self)
	local rot = self.rotation(self)
	local inside = rot.z(rot):dot(pos_dir)

	if 0 < inside and inside < self._properties.height then
		local pos_a = self.position(self)
		local pos_b = pos_a + rot.z(rot)*self._properties.height

		if math.distance_to_segment(pos, pos_a, pos_b) <= self._properties.radius then
			return true
		end
	end

	return false
end
ShapeCylinderMiddle = ShapeCylinderMiddle or class(ShapeCylinder)
ShapeCylinderMiddle.init = function (self, params)
	ShapeCylinderMiddle.super.init(self, params)

	return 
end
ShapeCylinderMiddle.is_inside = function (self, pos)
	local rot = self.rotation(self)
	local z = mvec3

	mrotation.z(rot, z)
	mvector3.multiply(z, self._properties.height/2)

	local position = mposition

	mvector3.set(position, self.position(self))
	mvector3.subtract(position, z)

	local pos_dir = mvec1

	mvector3.set(pos_dir, position)
	mvector3.multiply(pos_dir, -1)
	mvector3.add(pos_dir, pos)
	mrotation.z(rot, z)

	local inside = mvector3.dot(z, pos_dir)

	if 0 < inside and inside < self._properties.height then
		local to = mvec1

		mvector3.set(to, z)
		mvector3.multiply(to, self._properties.height)
		mvector3.add(to, position)

		if math.distance_to_segment(pos, position, to) <= self._properties.radius then
			return true
		end
	end

	return false
end
ShapeCylinderMiddle.draw = function (self, t, dt, r, g, b)
	local brush = Draw:brush()

	brush.set_color(brush, Color(0.5, r, g, b))

	local pos = self.position(self)
	local rot = self.rotation(self)
	local from = pos - (rot.z(rot)*self._properties.height)/2
	local to = pos + (rot.z(rot)*self._properties.height)/2

	brush.cylinder(brush, from, to, self._properties.radius, 100)
	Application:draw_cylinder(from, to, self._properties.radius, r, g, b)

	return 
end

return 
