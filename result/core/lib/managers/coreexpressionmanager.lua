core:module("CoreExpressionManager")

ExpressionManager = ExpressionManager or class()
ExpressionManager.init = function (self)
	self._units = {}
	self._preloads = {}

	return 
end
ExpressionManager.update = function (self, t, dt)
	for i, exp in pairs(self._units) do
		if not exp.update(exp, t, dt) then
			self._units[i] = nil
		end
	end

	return 
end
ExpressionManager.preload = function (self, movie_name)
	self._preloads[movie_name] = Database:load_node(Database:lookup("expression", movie_name))

	return 
end
ExpressionManager.play = function (self, unit, target, movie_name, loop)
	self._units[unit.key(unit)] = CoreExpressionMovie:new(unit, target, movie_name, self._preloads[movie_name], loop)

	return 
end
ExpressionManager.stop = function (self, unit)
	self._units[unit.key(unit)] = nil

	return 
end

return 
