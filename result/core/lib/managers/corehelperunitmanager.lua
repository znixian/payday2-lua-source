core:module("CoreHelperUnitManager")
core:import("CoreClass")

HelperUnitManager = HelperUnitManager or CoreClass.class()
HelperUnitManager.init = function (self)
	self._setup(self)

	return 
end
HelperUnitManager.clear = function (self)
	self._setup(self)

	return 
end
HelperUnitManager._setup = function (self)
	self._types = {}

	return 
end
HelperUnitManager.add_unit = function (self, unit, type)
	self._types[type] = self._types[type] or {}

	table.insert(self._types[type], unit)

	return 
end
HelperUnitManager.get_units_by_type = function (self, type)
	return self._types[type] or {}
end

return 
