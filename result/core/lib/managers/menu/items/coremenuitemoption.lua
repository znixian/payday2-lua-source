core:module("CoreMenuItemOption")

ItemOption = ItemOption or class()
ItemOption.init = function (self, data_node, parameters)
	local params = parameters or {}

	if data_node then
		for key, value in pairs(data_node) do
			if key ~= "_meta" and type(value) ~= "table" then
				params[key] = value
			end
		end
	end

	self.set_parameters(self, params)

	return 
end
ItemOption.value = function (self)
	return self._parameters.value
end
ItemOption.parameters = function (self)
	return self._parameters
end
ItemOption.set_parameters = function (self, parameters)
	self._parameters = parameters

	return 
end

return 
