core:module("CoreMenuItem")

Item = Item or class()
Item.TYPE = "item"
Item.init = function (self, data_node, parameters)
	self._type = ""
	local params = parameters or {}
	params.info_panel = ""

	if data_node then
		for key, value in pairs(data_node) do
			if key ~= "_meta" and type(value) ~= "table" then
				params[key] = value
			end
		end
	end

	local required_params = {
		"name"
	}

	for _, p_name in ipairs(required_params) do
		if not params[p_name] then
			Application:error("Menu item without parameter '" .. p_name .. "'")
		end
	end

	if params.visible_callback then
		self._visible_callback_name_list = string.split(params.visible_callback, " ")
	end

	if params.enabled_callback then
		self._enabled_callback_name_list = string.split(params.enabled_callback, " ")
	end

	if params.icon_visible_callback then
		self._icon_visible_callback_name_list = string.split(params.icon_visible_callback, " ")
	end

	if params.callback then
		params.callback = string.split(params.callback, " ")
	else
		params.callback = {}
	end

	if params.callback then
		params.callback_name = params.callback
		params.callback = {}
	end

	if params.callback_disabled then
		params.callback_disabled = string.split(params.callback_disabled, " ")
	else
		params.callback_disabled = {}
	end

	if params.callback_disabled then
		params.callback_disabled_name = params.callback_disabled
		params.callback_disabled = {}
	end

	self.set_parameters(self, params)

	self._enabled = true

	return 
end
Item.set_enabled = function (self, enabled)
	self._enabled = enabled

	self.dirty(self)

	return 
end
Item.enabled = function (self)
	return self._enabled
end
Item.type = function (self)
	return self._type
end
Item.name = function (self)
	return self._parameters.name
end
Item.info_panel = function (self)
	return self._parameters.info_panel
end
Item.parameters = function (self)
	return self._parameters
end
Item.parameter = function (self, name)
	return self._parameters[name]
end
Item.set_parameter = function (self, name, value)
	self._parameters[name] = value

	return 
end
Item.set_parameters = function (self, parameters)
	self._parameters = parameters

	return 
end
Item.set_callback_handler = function (self, callback_handler)
	self._callback_handler = callback_handler

	for _, callback_name in pairs(self._parameters.callback_name) do
		table.insert(self._parameters.callback, callback(callback_handler, callback_handler, callback_name))
	end

	for _, callback_name in pairs(self._parameters.callback_disabled_name) do
		table.insert(self._parameters.callback_disabled, callback(callback_handler, callback_handler, callback_name))
	end

	if self._visible_callback_name_list then
		for _, visible_callback_name in pairs(self._visible_callback_name_list) do
			self._visible_callback_list = self._visible_callback_list or {}

			table.insert(self._visible_callback_list, callback(callback_handler, callback_handler, visible_callback_name))
		end
	end

	if self._icon_visible_callback_name_list then
		for _, visible_callback_name in pairs(self._icon_visible_callback_name_list) do
			self._icon_visible_callback_list = self._icon_visible_callback_list or {}

			table.insert(self._icon_visible_callback_list, callback(callback_handler, callback_handler, visible_callback_name))
		end
	end

	if self._enabled_callback_name_list then
		for _, enabled_callback_name in pairs(self._enabled_callback_name_list) do
			if callback_handler[enabled_callback_name] then
				if not callback_handler[enabled_callback_name](self) then
					self.set_enabled(self, false)

					break
				end
			else
				Application:error("[Item:set_callback_handler] inexistent callback:", enabled_callback_name)
			end
		end
	end

	return 
end
Item.trigger = function (self)
	slot1 = pairs
	slot2 = ((self.enabled(self) or self.parameters(self).ignore_disabled) and self.parameters(self).callback) or self.parameters(self).callback_disabled

	for _, callback in slot1(slot2) do
		callback(self)
	end

	return 
end
Item.dirty = function (self)
	if self.dirty_callback then
		self.dirty_callback(self)
	end

	return 
end
Item.set_visible = function (self, visible)
	self._visible = visible

	return 
end
Item.visible = function (self)
	if self._visible == false then
		return false
	end

	if self._visible_callback_list then
		for _, visible_callback in pairs(self._visible_callback_list) do
			if not visible_callback(self) then
				return false
			end
		end
	end

	return true
end
Item.on_delete_row_item = function (self)
	return 
end
Item.on_delete_item = function (self)
	self._parameters.callback = {}
	self._parameters.callback_disabled = {}
	self._visible_callback_list = nil
	self._icon_visible_callback_list = nil

	return 
end
Item.on_item_position = function (self, row_item, node)
	return 
end
Item.on_item_positions_done = function (self, row_item, node)
	return 
end
Item.get_h = function (self, row_item)
	return nil
end
Item.setup_gui = function (self, node, row_item)
	return false
end
Item.reload = function (self, row_item)
	return false
end
Item.highlight_row_item = function (self, node, row_item, mouse_over)
	return false
end
Item.fade_row_item = function (self, node, row_item)
	return false
end
Item.menu_unselected_visible = function (self)
	return true
end
Item.icon_visible = function (self)
	if self._icon_visible_callback_list then
		for _, visible_callback in pairs(self._icon_visible_callback_list) do
			if not visible_callback(self) then
				return false
			end
		end
	end

	return true
end

return 
