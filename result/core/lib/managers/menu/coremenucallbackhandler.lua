core:module("CoreMenuCallbackHandler")

CallbackHandler = CallbackHandler or class()
CallbackHandler.init = function (self)
	getmetatable(self).__index = function (t, key)
		local value = rawget(getmetatable(t), key)

		if value then
			return value
		end

		return 
	end

	return 
end

return 
