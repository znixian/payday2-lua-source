core:module("CoreMenuLogic")

Logic = Logic or class()
Logic.init = function (self, menu_data)
	self._data = menu_data
	self._node_stack = {}
	self._callback_map = {
		renderer_show_node = nil,
		renderer_refresh_node_stack = nil,
		renderer_refresh_node = nil,
		renderer_update_node = nil,
		renderer_select_item = nil,
		renderer_deselect_item = nil,
		renderer_trigger_item = nil,
		renderer_navigate_back = nil,
		renderer_node_item_dirty = nil,
		input_accept_input = nil,
		menu_manager_menu_closed = nil,
		menu_manager_select_node = nil
	}
	self._action_queue = {}
	self._action_callback_map = {
		select_node = callback(self, self, "_select_node"),
		navigate_back = callback(self, self, "_navigate_back"),
		select_item = callback(self, self, "_select_item"),
		trigger_item = callback(self, self, "_trigger_item"),
		refresh_node = callback(self, self, "_refresh_node"),
		refresh_node_stack = callback(self, self, "_refresh_node_stack"),
		update_node = callback(self, self, "_update_node")
	}

	return 
end
Logic.open = function (self, ...)
	self._accept_input = not managers.system_menu:is_active()

	self.select_node(self, nil, true)

	return 
end
Logic._queue_action = function (self, action_name, ...)
	table.insert(self._action_queue, {
		action_name = action_name,
		parameters = {
			...
		}
	})

	return 
end
Logic._execute_action_queue = function (self)
	while self._accept_input and 0 < #self._action_queue do
		local action = self._action_queue[1]

		if self._action_callback_map[action.action_name] then
			self._action_callback_map[action.action_name](unpack(action.parameters))
		end

		table.remove(self._action_queue, 1)
	end

	return 
end
Logic.update = function (self, t, dt)
	if self.selected_node(self) then
		self.selected_node(self):update(t, dt)
	end

	self._execute_action_queue(self)

	return 
end
Logic.select_node = function (self, node_name, queue, ...)
	if self._accept_input or queue then
		self._queue_action(self, "select_node", node_name, ...)
	end

	return 
end
Logic._select_node = function (self, node_name, ...)
	local node = self.get_node(self, node_name, ...)
	local has_active_menu = (managers.menu._open_menus and 0 < #managers.menu._open_menus and true) or false

	if has_active_menu and node then
		local selected_node = self.selected_node(self)

		if selected_node then
			selected_node.trigger_focus_changed(selected_node, false)
		end

		node.trigger_focus_changed(node, true, ...)

		if node.parameters(node).menu_components then
			managers.menu_component:set_active_components(node.parameters(node).menu_components, node)
		end

		node.parameters(node).create_params = {
			...
		}

		table.insert(self._node_stack, node)
		self._call_callback(self, "renderer_show_node", node)
		node.select_item(node)
		self._call_callback(self, "renderer_select_item", node.selected_item(node))
		self._call_callback(self, "menu_manager_select_node", node)
	end

	return 
end
Logic.refresh_node_stack = function (self, queue, ...)
	self._queue_action(self, "refresh_node_stack", ...)

	return 
end
Logic._refresh_node_stack = function (self, ...)
	for i, node in ipairs(self._node_stack) do
		if node.parameters(node).refresh then
			for _, refresh_func in ipairs(node.parameters(node).refresh) do
				node = refresh_func(node, ...)
			end
		end

		local selected_item = node.selected_item(node)

		node.select_item(node, selected_item and selected_item.name(selected_item))
	end

	self._call_callback(self, "renderer_refresh_node_stack")

	return 
end
Logic.refresh_node = function (self, node_name, queue, ...)
	self._queue_action(self, "refresh_node", node_name, ...)

	return 
end
Logic._refresh_node = function (self, node_name, ...)
	local node = self.selected_node(self)

	if node and node.parameters(node).refresh then
		for _, refresh_func in ipairs(node.parameters(node).refresh) do
			node = refresh_func(node, ...)
		end
	end

	if node then
		self._call_callback(self, "renderer_refresh_node", node)

		local selected_item = node.selected_item(node)

		node.select_item(node, selected_item and selected_item.name(selected_item))
		self._call_callback(self, "renderer_select_item", node.selected_item(node))
	end

	return 
end
Logic.update_node = function (self, node_name, queue, ...)
	self._queue_action(self, "update_node", node_name, ...)

	return 
end
Logic._update_node = function (self, node_name, ...)
	local node = self.selected_node(self)

	if node then
		if node.parameters(node).update then
			for _, update_func in ipairs(node.parameters(node).update) do
				node = update_func(node, ...)
			end
		end
	else
		Application:error("[CoreLogic:_update_node] Trying to update selected node, but none is selected!")
	end

	return 
end
Logic.navigate_back = function (self, queue, skip_nodes)
	if self._accept_input or queue then
		self._queue_action(self, "navigate_back", skip_nodes)
	end

	return 
end
Logic._navigate_back = function (self, skip_nodes)
	local node = self._node_stack[#self._node_stack]

	if node then
		if node.trigger_back(node) then
			return 
		end

		node.trigger_focus_changed(node, false)
	end

	skip_nodes = (type(skip_nodes) == "number" and skip_nodes) or 0

	if skip_nodes + 1 < #self._node_stack then
		for i = 1, skip_nodes + 1, 1 do
			table.remove(self._node_stack, #self._node_stack)
			self._call_callback(self, "renderer_navigate_back")
		end

		node = self._node_stack[#self._node_stack]

		if node then
			node.trigger_focus_changed(node, true)

			if node.parameters(node).menu_components then
				managers.menu_component:set_active_components(node.parameters(node).menu_components, node)
			end
		end
	end

	self._call_callback(self, "menu_manager_select_node", node)

	return 
end
Logic.soft_open = function (self)
	local node = self._node_stack[#self._node_stack]

	if node then
		if node.parameters(node).menu_components then
			managers.menu_component:set_active_components(node.parameters(node).menu_components, node)
		end

		self._call_callback(self, "menu_manager_select_node", node)
	end

	return 
end
Logic.selected_node = function (self)
	return self._node_stack[#self._node_stack]
end
Logic.selected_node_name = function (self)
	return self.selected_node(self):parameters().name
end
Logic.select_item = function (self, item_name, queue)
	if self._accept_input or queue then
		self._queue_action(self, "select_item", item_name)
	end

	return 
end
Logic.mouse_over_select_item = function (self, item_name, queue)
	if self._accept_input or queue then
		self._queue_action(self, "select_item", item_name, true)
	end

	return 
end
Logic._select_item = function (self, item_name, mouse_over)
	local current_node = self.selected_node(self)

	if current_node then
		local current_item = current_node.selected_item(current_node)

		if current_item then
			self._call_callback(self, "renderer_deselect_item", current_item)
		end

		current_node.select_item(current_node, item_name)
		self._call_callback(self, "renderer_select_item", current_node.selected_item(current_node), mouse_over)
	end

	return 
end
Logic.trigger_item = function (self, queue, item)
	if self._accept_input or queue then
		self._queue_action(self, "trigger_item", item)
	end

	return 
end
Logic._trigger_item = function (self, item)
	item = item or self.selected_item(self)

	if item then
		item.trigger(item)
		self._call_callback(self, "renderer_trigger_item", item)
	end

	return 
end
Logic.selected_item = function (self)
	local item = nil
	local node = self.selected_node(self)

	if node then
		item = node.selected_item(node)
	end

	return item
end
Logic.get_item = function (self, name)
	local item = nil
	local node = self.selected_node(self)

	if node then
		item = node.item(node, name)
	end

	return item
end
Logic.get_node = function (self, node_name, ...)
	local node = self._data:get_node(node_name, ...)

	if node and not node.dirty_callback then
		node.dirty_callback = callback(self, self, "node_item_dirty")
	end

	return node
end
Logic.accept_input = function (self, accept)
	self._accept_input = accept

	self._call_callback(self, "input_accept_input", accept)

	return 
end
Logic.register_callback = function (self, id, callback)
	self._callback_map[id] = callback

	return 
end
Logic._call_callback = function (self, id, ...)
	if self._callback_map[id] then
		self._callback_map[id](...)
	else
		Application:error("Logic:_call_callback: Callback " .. id .. " not found.")
	end

	return 
end
Logic.node_item_dirty = function (self, node, item)
	self._call_callback(self, "renderer_node_item_dirty", node, item)

	return 
end
Logic.renderer_closed = function (self)
	self._call_callback(self, "menu_manager_menu_closed")

	return 
end
Logic.close = function (self, closing_menu)
	local selected_node = self.selected_node(self)

	managers.menu_component:set_active_components({})

	self._action_queue = {}

	for index = #self._node_stack, 1, -1 do
		local node = self._node_stack[index]

		if not closing_menu and node then
			node.trigger_back(node)
		end
	end

	self._node_stack = {}

	self._call_callback(self, "menu_manager_select_node", false)

	return 
end

return 
