core:module("CoreMenuInput")
core:import("CoreDebug")
core:import("CoreMenuItem")
core:import("CoreMenuItemSlider")
core:import("CoreMenuItemToggle")

MenuInput = MenuInput or class()
MenuInput.init = function (self, logic, menu_name)
	self._logic = logic
	self._menu_name = menu_name
	self._accept_input = true

	self._logic:register_callback("input_accept_input", callback(self, self, "accept_input"))

	self._axis_delay_timer = {
		x = 0,
		y = 0
	}
	self._item_input_action_map = {
		[CoreMenuItem.Item.TYPE] = callback(self, self, "input_item"),
		[CoreMenuItemSlider.ItemSlider.TYPE] = callback(self, self, "input_slider"),
		[CoreMenuItemToggle.ItemToggle.TYPE] = callback(self, self, "input_toggle")
	}

	return 
end
MenuInput.open = function (self, ...)
	self.create_controller(self)

	return 
end
MenuInput.close = function (self)
	self.destroy_controller(self)

	return 
end
MenuInput.axis_timer = function (self)
	return self._axis_delay_timer
end
MenuInput.set_axis_x_timer = function (self, time)
	self._axis_delay_timer.x = time

	return 
end
MenuInput.set_axis_y_timer = function (self, time)
	self._axis_delay_timer.y = time

	return 
end
MenuInput._input_hijacked = function (self)
	local active_menu = managers.menu:active_menu()

	return active_menu and active_menu.renderer:input_focus()
end
MenuInput.input_item = function (self, item, controller, mouse_click)
	if controller.get_input_pressed(controller, "confirm") or mouse_click then
		if item.parameters(item).sign_in then
			print("requires sign in")

			local function f(success)
				print(success)

				if success then
					self._logic:trigger_item(true, item)
					self:select_node()
				end

				return 
			end

			managers.menu:open_sign_in_menu(f)
		else
			local node_gui = managers.menu:active_menu().renderer:active_node_gui()

			if node_gui and node_gui._listening_to_input then
				return 
			end

			self._logic:trigger_item(true, item)
			self.select_node(self)
		end
	end

	return 
end
MenuInput.input_slider = function (self, item, controller)
	local slider_delay_down = 0.1
	local slider_delay_pressed = 0.2

	if self.menu_right_input_bool(self) then
		item.increase(item)
		self._logic:trigger_item(true, item)
		self.set_axis_x_timer(self, slider_delay_down)

		if self.menu_right_pressed(self) then
			local percentage = item.percentage(item)

			if 0 < percentage and percentage < 100 then
				self.post_event(self, "slider_increase")
			end

			self.set_axis_x_timer(self, slider_delay_pressed)
		end
	elseif self.menu_left_input_bool(self) then
		item.decrease(item)
		self._logic:trigger_item(true, item)
		self.set_axis_x_timer(self, slider_delay_down)

		if self.menu_left_pressed(self) then
			self.set_axis_x_timer(self, slider_delay_pressed)

			local percentage = item.percentage(item)

			if 0 < percentage and percentage < 100 then
				self.post_event(self, "slider_decrease")
			end
		end
	end

	return 
end
MenuInput.input_toggle = function (self, item, controller, mouse_click)
	local toggle_delay_down = 0.3
	local toggle_delay_pressed = 0.6

	if controller.get_input_pressed(controller, "confirm") or mouse_click then
		item.toggle(item)
		self._logic:trigger_item(true, item)
	end

	return 
end
MenuInput.update = function (self, t, dt)
	self._check_releases(self)
	self.any_keyboard_used(self)

	local axis_timer = self.axis_timer(self)

	if 0 < axis_timer.y then
		self.set_axis_y_timer(self, axis_timer.y - dt)
	end

	if 0 < axis_timer.x then
		self.set_axis_x_timer(self, axis_timer.x - dt)
	end

	if self._input_hijacked(self) then
		return false
	end

	if self._accept_input and self._controller then
		if axis_timer.y <= 0 then
			if self.menu_up_input_bool(self) then
				self.prev_item(self)
				self.set_axis_y_timer(self, 0.12)

				if self.menu_up_pressed(self) then
					self.set_axis_y_timer(self, 0.3)
				end
			elseif self.menu_down_input_bool(self) then
				self.next_item(self)
				self.set_axis_y_timer(self, 0.12)

				if self.menu_down_pressed(self) then
					self.set_axis_y_timer(self, 0.3)
				end
			end
		end

		if axis_timer.x <= 0 then
			local item = self._logic:selected_item()

			if item then
				self._item_input_action_map[item.TYPE](item, self._controller)
			end
		end

		if self._controller:get_input_pressed("menu_update") then
			print("update something")
			self._logic:update_node()
		end
	end

	return true
end
MenuInput.menu_up_input_bool = function (self)
	if self._controller then
		return self._controller:get_input_bool("menu_up")
	end

	return false
end
MenuInput.menu_up_pressed = function (self)
	if self._controller then
		return self._controller:get_input_pressed("menu_up")
	end

	return false
end
MenuInput.menu_up_released = function (self)
	if self._controller then
		return self._controller:get_input_released("menu_up")
	end

	return false
end
MenuInput.menu_down_input_bool = function (self)
	if self._controller then
		return self._controller:get_input_bool("menu_down")
	end

	return false
end
MenuInput.menu_down_pressed = function (self)
	if self._controller then
		return self._controller:get_input_pressed("menu_down")
	end

	return false
end
MenuInput.menu_down_released = function (self)
	if self._controller then
		return self._controller:get_input_released("menu_down")
	end

	return false
end
MenuInput.menu_left_input_bool = function (self)
	if self._controller then
		return self._controller:get_input_bool("menu_left")
	end

	return false
end
MenuInput.menu_left_pressed = function (self)
	if self._controller then
		return self._controller:get_input_pressed("menu_left")
	end

	return false
end
MenuInput.menu_left_released = function (self)
	if self._controller then
		return self._controller:get_input_released("menu_left")
	end

	return false
end
MenuInput.menu_right_input_bool = function (self)
	if self._controller then
		return self._controller:get_input_bool("menu_right")
	end

	return false
end
MenuInput.menu_right_pressed = function (self)
	if self._controller then
		return self._controller:get_input_pressed("menu_right")
	end

	return false
end
MenuInput.menu_right_released = function (self)
	if self._controller then
		return self._controller:get_input_released("menu_right")
	end

	return false
end
MenuInput._check_releases = function (self)
	if self.menu_left_released(self) or self.menu_right_released(self) then
		self.set_axis_x_timer(self, 0.01)
	end

	if self.menu_up_released(self) or self.menu_down_released(self) then
		self.set_axis_y_timer(self, 0.01)
	end

	return 
end
MenuInput.accept_input = function (self, accept)
	self._accept_input = accept

	return 
end
MenuInput.focus = function (self, focus)
	if focus then
		self.create_controller(self)
	else
		self.destroy_controller(self)
	end

	return 
end
MenuInput.create_controller = function (self)
	if not self._controller then
		local controller = managers.controller:create_controller(nil, nil, false)

		controller.add_trigger(controller, "cancel", callback(self, self, "back"))
		controller.set_enabled(controller, true)

		self._controller = controller
	end

	return 
end
MenuInput.destroy_controller = function (self)
	if self._controller then
		self._controller:destroy()

		self._controller = nil
	end

	return 
end
MenuInput.logic_changed = function (self)
	return 
end
MenuInput.next_item = function (self)
	if not self._accept_input then
		return 
	end

	local current_item = self._logic:selected_item()

	if current_item then
		local current_item_name = current_item.parameters(current_item).name
		local items = self._logic:selected_node():items()
		local done = nil

		for i, v in ipairs(items) do
			if v.parameters(v).name == current_item_name then
				for check = 1, #items - 1, 1 do
					local next_item = items[((i + check) - 1)%#items + 1]

					if next_item.visible(next_item) and not next_item.no_select then
						self._logic:select_item(next_item.parameters(next_item).name, true)

						done = true

						break
					end
				end

				if done then
					break
				end
			end
		end
	end

	return 
end
MenuInput.prev_item = function (self)
	local current_item = self._logic:selected_item()

	if current_item then
		local current_item_name = current_item.parameters(current_item).name
		local items = self._logic:selected_node():items()
		local done = nil

		for i, v in ipairs(items) do
			if v.parameters(v).name == current_item_name then
				for check = 1, #items - 1, 1 do
					local prev_item = items[(i - check - 1)%#items + 1]

					if prev_item.visible(prev_item) and not prev_item.no_select then
						self._logic:select_item(prev_item.parameters(prev_item).name, true)

						done = true

						break
					end
				end

				if done then
					break
				end
			end
		end
	end

	return 
end
MenuInput.back = function (self, queue, skip_nodes)
	if self._input_hijacked(self) == true then
		return 
	end

	if self._logic:selected_node() and self._logic:selected_node():parameters().block_back then
		return 
	end

	self._logic:navigate_back(queue == true or false, (type(skip_nodes) == "number" and skip_nodes) or false)

	return 
end
MenuInput.select_node = function (self)
	local item = self._logic:selected_item()

	if item and item.visible(item) then
		local parameters = item.parameters(item)

		if parameters.next_node and (item.enabled(item) or parameters.ignore_disabled) then
			self._logic:select_node(parameters.next_node, true, unpack(parameters.next_node_parameters or {}))
		end

		if parameters.previous_node then
			self.back(self)
		end
	end

	return 
end
MenuInput.any_keyboard_used = function (self)
	if self._keyboard_used or not self._controller or (managers.controller:get_default_wrapper_type() ~= "pc" and managers.controller:get_default_wrapper_type() ~= "steam") then
		return 
	end

	for _, key in ipairs({
		"menu_right",
		"menu_left",
		"menu_up",
		"menu_down",
		"confirm"
	}) do
		if self._controller:get_input_bool(key) then
			self._keyboard_used = true

			return 
		end
	end

	return 
end

return 
