core:module("CoreMenuRenderer")
core:import("CoreMenuNodeGui")

Renderer = Renderer or class()
Renderer.border_height = 44
Renderer.preload = function (self)
	return 
end
Renderer.init = function (self, logic, parameters)
	parameters = parameters or {}
	self._logic = logic

	self._logic:register_callback("renderer_show_node", callback(self, self, "show_node"))
	self._logic:register_callback("renderer_refresh_node_stack", callback(self, self, "refresh_node_stack"))
	self._logic:register_callback("renderer_refresh_node", callback(self, self, "refresh_node"))
	self._logic:register_callback("renderer_select_item", callback(self, self, "highlight_item"))
	self._logic:register_callback("renderer_deselect_item", callback(self, self, "fade_item"))
	self._logic:register_callback("renderer_trigger_item", callback(self, self, "trigger_item"))
	self._logic:register_callback("renderer_navigate_back", callback(self, self, "navigate_back"))
	self._logic:register_callback("renderer_node_item_dirty", callback(self, self, "node_item_dirty"))

	self._timer = 0
	self._base_layer = parameters.layer or 200
	self.ws = managers.gui_data:create_saferect_workspace()
	self._fullscreen_ws = managers.gui_data:create_fullscreen_workspace()

	if _G.IS_VR then
		self._fullscreen_ws:set_pinned_screen(true)
	end

	self._fullscreen_panel = self._fullscreen_ws:panel():panel({
		valign = "scale",
		halign = "scale",
		layer = self._base_layer
	})

	self._fullscreen_ws:hide()

	local safe_rect_pixels = managers.viewport:get_safe_rect_pixels()
	self._main_panel = self.ws:panel():panel({
		layer = self._base_layer
	})
	self.safe_rect_panel = self.ws:panel():panel({
		w = safe_rect_pixels.width,
		h = safe_rect_pixels.height,
		layer = self._base_layer
	})

	return 
end
Renderer._scaled_size = function (self)
	return managers.gui_data:scaled_size()
end
Renderer.open = function (self, ...)
	managers.gui_data:layout_workspace(self.ws)
	managers.gui_data:layout_fullscreen_workspace(self._fullscreen_ws)
	self._layout_main_panel(self)

	self._resolution_changed_callback_id = managers.viewport:add_resolution_changed_func(callback(self, self, "resolution_changed"))
	self._node_gui_stack = {}
	self._open = true

	self._fullscreen_ws:show()

	return 
end
Renderer.is_open = function (self)
	return self._open
end
Renderer.show_node = function (self, node, parameters)
	local layer = self._base_layer
	local previous_node_gui = self.active_node_gui(self)

	if previous_node_gui then
		layer = previous_node_gui.layer(previous_node_gui)

		previous_node_gui.set_visible(previous_node_gui, false)
	end

	local new_node_gui = nil

	if parameters.node_gui_class then
		new_node_gui = parameters.node_gui_class:new(node, layer + 1, parameters)
	else
		new_node_gui = CoreMenuNodeGui.NodeGui:new(node, layer + 1, parameters)
	end

	table.insert(self._node_gui_stack, new_node_gui)

	if not managers.system_menu:is_active() then
		self.disable_input(self, 0.2)
	end

	return 
end
Renderer.refresh_node_stack = function (self, parameters)
	for i, node_gui in ipairs(self._node_gui_stack) do
		node_gui.refresh_gui(node_gui, node_gui.node, parameters)

		local selected_item = node_gui.node and node_gui.node:selected_item()

		if selected_item then
			node_gui.highlight_item(node_gui, selected_item)
		end
	end

	return 
end
Renderer.refresh_node = function (self, node, parameters)
	local layer = self._base_layer
	local node_gui = self.active_node_gui(self)

	node_gui.refresh_gui(node_gui, node, parameters)

	return 
end
Renderer.highlight_item = function (self, item, mouse_over)
	local active_node_gui = self.active_node_gui(self)

	if active_node_gui then
		active_node_gui.highlight_item(active_node_gui, item, mouse_over)
	end

	return 
end
Renderer.fade_item = function (self, item)
	local active_node_gui = self.active_node_gui(self)

	if active_node_gui then
		active_node_gui.fade_item(active_node_gui, item)
	end

	return 
end
Renderer.trigger_item = function (self, item)
	local node_gui = self.active_node_gui(self)

	if node_gui then
		node_gui.reload_item(node_gui, item)
	end

	return 
end
Renderer.navigate_back = function (self)
	local active_node_gui = self.active_node_gui(self)

	if active_node_gui then
		active_node_gui.close(active_node_gui)
		table.remove(self._node_gui_stack, #self._node_gui_stack)
		self.active_node_gui(self):set_visible(true)
		self.disable_input(self, 0.2)
	end

	return 
end
Renderer.node_item_dirty = function (self, node, item)
	local node_name = node.parameters(node).name

	for _, gui in pairs(self._node_gui_stack) do
		if gui.name == node_name then
			gui.reload_item(gui, item)
		end
	end

	return 
end
Renderer.update = function (self, t, dt)
	self.update_input_timer(self, dt)

	for _, node_gui in ipairs(self._node_gui_stack) do
		node_gui.update(node_gui, t, dt)
	end

	return 
end
Renderer.update_input_timer = function (self, dt)
	if 0 < self._timer then
		self._timer = self._timer - dt

		if self._timer <= 0 then
			self._logic:accept_input(true)
		end
	end

	return 
end
Renderer.active_node_gui = function (self)
	return self._node_gui_stack[#self._node_gui_stack]
end
Renderer.disable_input = function (self, time)
	self._timer = time

	self._logic:accept_input(false)

	return 
end
Renderer.close = function (self)
	self._fullscreen_ws:hide()

	self._open = false

	if self._resolution_changed_callback_id then
		managers.viewport:remove_resolution_changed_func(self._resolution_changed_callback_id)
	end

	for _, node_gui in ipairs(self._node_gui_stack) do
		node_gui.close(node_gui)
	end

	self._main_panel:clear()
	self._fullscreen_panel:clear()
	self.safe_rect_panel:clear()

	self._node_gui_stack = {}

	self._logic:renderer_closed()

	return 
end
Renderer.hide = function (self)
	local active_node_gui = self.active_node_gui(self)

	if active_node_gui then
		active_node_gui.set_visible(active_node_gui, false)
	end

	return 
end
Renderer.show = function (self)
	local active_node_gui = self.active_node_gui(self)

	if active_node_gui then
		active_node_gui.set_visible(active_node_gui, true)
	end

	return 
end
Renderer._layout_main_panel = function (self)
	local scaled_size = self._scaled_size(self)

	self._main_panel:set_shape(0, 0, scaled_size.width, scaled_size.height)

	local safe_rect = self._scaled_size(self)

	self.safe_rect_panel:set_shape(safe_rect.x, safe_rect.y, safe_rect.width, safe_rect.height)

	return 
end
Renderer.resolution_changed = function (self)
	local res = RenderSettings.resolution

	managers.gui_data:layout_workspace(self.ws)
	managers.gui_data:layout_fullscreen_workspace(self._fullscreen_ws)
	self._layout_main_panel(self)

	for _, node_gui in ipairs(self._node_gui_stack) do
		node_gui.resolution_changed(node_gui)
	end

	return 
end
Renderer.selected_node = function (self)
	local stack = self._node_gui_stack

	return stack[#stack]
end

return 
