CoreUnderlayMaterial = CoreUnderlayMaterial or class()
CoreUnderlayMaterial.init = function (self)
	self._params = {}

	return 
end
CoreUnderlayMaterial.add = function (self, from)
	for key, value in pairs(from._params) do
		if self._params[key] then
			self._params[key] = self._params[key] + from._params[key]
		elseif type(from._params[key]) ~= "number" then
			self._params[key] = Vector3(from._params[key].x, from._params[key].y, from._params[key].z)
		else
			self._params[key] = from._params[key]
		end
	end

	return 
end
CoreUnderlayMaterial.scale = function (self, scale)
	for key, value in pairs(self._params) do
		self._params[key] = self._params[key]*scale
	end

	return 
end
CoreUnderlayMaterial.copy = function (self, from)
	for key, value in pairs(from._params) do
		if type(value) ~= "number" then
			self._params[key] = Vector3(value.x, value.y, value.z)
		else
			self._params[key] = value
		end
	end

	return 
end
CoreUnderlayMaterial.interpolate = function (self, postfx, with, scale)
	for key, value in pairs(postfx._params) do
		if not with._params[key] then
			self._params[key] = nil
		else
			local invscale = scale - 1
			self._params[key] = postfx._params[key]*invscale + with._params[key]*scale
		end
	end

	return 
end
CoreUnderlayMaterial.interpolate_value = function (self, postfx, with, key, scale)
	if not with._params[key] or not postfx._params[key] then
		return 
	else
		local invscale = scale - 1
		self._params[key] = postfx._params[key]*invscale + with._params[key]*scale
	end

	return 
end
CoreUnderlayMaterial.parse = function (self, xml_node)
	self._params = {}

	for child in xml_node.children(xml_node) do
		local key = child.parameter(child, "key")
		local value = child.parameter(child, "value")

		if child.name(child) == "param" and key and key ~= "" and value and value ~= "" then
			if math.string_is_good_vector(value) then
				self._params[key] = math.string_to_vector(value)
			elseif tonumber(value) then
				self._params[key] = tonumber(value)
			elseif string.sub(value, 1, 1) == "#" then
				self._params[key] = self.database_lookup(self, string.sub(value, 2))
			else
				self._params[key] = tostring(value)
			end
		end
	end

	return 
end
CoreUnderlayMaterial.set_value = function (self, key, value)
	self._params[key] = value

	return 
end
CoreUnderlayMaterial.value = function (self, key)
	return self._params[key]
end
CoreUnderlayMaterial.database_lookup = function (self, str)
	local i = string.find(str, "#")
	local db_key = string.sub(str, 1, i - 1)
	local value_key = string.sub(str, i + 1)

	assert(db_key == "LightIntensityDB")

	local value = LightIntensityDB:lookup(value_key)

	assert(value)

	return value
end
CoreUnderlayEffect = CoreUnderlayEffect or class()
CoreUnderlayEffect.init = function (self)
	self.set_default(self)

	return 
end
CoreUnderlayEffect.set_default = function (self)
	self._materials = {}
	self._name = "default"

	return 
end
CoreUnderlayEffect.set_name = function (self, name)
	self._name = name

	return 
end
CoreUnderlayEffect.add = function (self, from)
	for name, material in pairs(from._materials) do
		if not self._materials[name] then
			self._materials[name] = CoreUnderlayMaterial:new()
		end

		material.add(material, from._materials[name])
	end

	return 
end
CoreUnderlayEffect.scale = function (self, scale)
	for name, material in pairs(self._materials) do
		material.scale(material, scale)
	end

	return 
end
CoreUnderlayEffect.copy = function (self, from)
	for name, material in pairs(from._materials) do
		if not self._materials[name] then
			self._materials[name] = CoreUnderlayMaterial:new()
		end

		self._materials[name]:copy(material)
	end

	self._name = from._name

	return 
end
CoreUnderlayEffect.interpolate = function (self, postfx, with, scale)
	for name, material in pairs(postfx._materials) do
		if not with._materials[name] then
			with._materials[name] = CoreUnderlayMaterial:new()
		end

		if not self._materials[name] then
			self._materials[name] = CoreUnderlayMaterial:new()
		end
	end

	for name, material in pairs(with._materials) do
		if not postfx._materials[name] then
			postfx._materials[name] = CoreUnderlayMaterial:new()
		end

		if not self._materials[name] then
			self._materials[name] = CoreUnderlayMaterial:new()
		end
	end

	for name, material in pairs(self._materials) do
		material.interpolate(material, postfx._materials[name], with._materials[name], scale)
	end

	self._name = postfx._name

	return 
end
CoreUnderlayEffect.interpolate_value = function (self, postfx, with, material, key, scale)
	if not with._materials[material] or not postfx._materials[material] then
		return 
	end

	if not self._materials[material] then
		self._materials[material] = CoreUnderlayMaterial:new()
	end

	self._name = postfx._name

	self._materials[material]:interpolate_value(postfx._materials[material], with._materials[material], key, scale)

	return 
end
CoreUnderlayEffect.parse = function (self, xml_node)
	for child in xml_node.children(xml_node) do
		local name = child.parameter(child, "name")

		if child.name(child) == "material" and name and name ~= "" then
			if not self._materials[name] then
				self._materials[name] = CoreUnderlayMaterial:new()
			end

			self._materials[name]:parse(child)
		end
	end

	return 
end
CoreUnderlayEffect.set_value = function (self, material, key, value)
	if not self._materials[material] then
		self._materials[material] = CoreUnderlayMaterial:new()
	end

	self._materials[material]:set_value(key, value)

	return 
end
CoreUnderlayEffect.value = function (self, material, key)
	if self._materials[material] then
		return self._materials[material]:value(key)
	else
		return nil
	end

	return 
end

return 
