core:module("CoreViewportManager")
core:import("CoreApp")
core:import("CoreCode")
core:import("CoreEvent")
core:import("CoreManagerBase")
core:import("CoreScriptViewport")
core:import("CoreEnvironmentManager")

ViewportManager = ViewportManager or class(CoreManagerBase.ManagerBase)
ViewportManager.init = function (self, aspect_ratio)
	ViewportManager.super.init(self, "viewport")
	assert(type(aspect_ratio) == "number")

	self._aspect_ratio = aspect_ratio
	self._resolution_changed_event_handler = CoreEvent.CallbackEventHandler:new()
	self._env_manager = CoreEnvironmentManager.EnvironmentManager:new()
	Global.render_debug.render_sky = true
	self._current_camera_position = Vector3()

	if Application:editor() then
		self.preload_environment(self, "core/environments/default")
	end

	return 
end
ViewportManager.update = function (self, t, dt)
	for i, svp in ipairs(self._all_really_active(self)) do
		svp._update(svp, i, t, dt)
	end

	return 
end
ViewportManager.paused_update = function (self, t, dt)
	self.update(self, t, dt)

	return 
end
ViewportManager.render = function (self)
	for i, svp in ipairs(self._all_really_active(self)) do
		svp._render(svp, i)
	end

	return 
end
ViewportManager.end_frame = function (self, t, dt)
	if self._render_settings_change_map then
		local is_resolution_changed = self._render_settings_change_map.resolution ~= nil

		for setting_name, setting_value in pairs(self._render_settings_change_map) do
			RenderSettings[setting_name] = setting_value
		end

		self._render_settings_change_map = nil

		Application:apply_render_settings()
		Application:save_render_settings()

		if is_resolution_changed then
			self.resolution_changed(self)
		end
	end

	self._current_camera = nil
	self._current_camera_position_updated = nil
	self._current_camera_rotation = nil

	return 
end
ViewportManager.destroy = function (self)
	local _, svp = next(self._all_ao(self))

	while svp do
		svp.destroy(svp)

		_, svp = next(self._all_ao(self))
	end

	self._env_manager:destroy()

	return 
end
ViewportManager.new_vp = function (self, x, y, width, height, name, priority)
	local name = name or ""
	local prio = priority or CoreManagerBase.PRIO_DEFAULT
	local svp = CoreScriptViewport._ScriptViewport:new(x, y, width, height, self, name)

	self._add_accessobj(self, svp, prio)

	return svp
end
ViewportManager.vp_by_name = function (self, name)
	return self._ao_by_name(self, name)
end
ViewportManager.active_viewports = function (self)
	return self._all_active_requested_by_prio(self, CoreManagerBase.PRIO_DEFAULT)
end
ViewportManager.all_really_active_viewports = function (self)
	return self._all_really_active(self)
end
ViewportManager.num_active_viewports = function (self)
	return #self.active_viewports(self)
end
ViewportManager.first_active_viewport = function (self)
	local all_active = self._all_really_active(self)

	return (0 < #all_active and all_active[1]) or nil
end
ViewportManager.viewports = function (self)
	return self._all_ao(self)
end
ViewportManager.add_resolution_changed_func = function (self, func)
	self._resolution_changed_event_handler:add(func)

	return func
end
ViewportManager.remove_resolution_changed_func = function (self, func)
	self._resolution_changed_event_handler:remove(func)

	return 
end
ViewportManager.resolution_changed = function (self)
	managers.gui_data:resolution_changed()

	if rawget(_G, "tweak_data").resolution_changed then
		rawget(_G, "tweak_data"):resolution_changed()
	end

	for i, svp in ipairs(self.viewports(self)) do
		svp._resolution_changed(svp, i)
	end

	self._resolution_changed_event_handler:dispatch()

	return 
end
ViewportManager.editor_reload_environment = function (self, name)
	self._env_manager:editor_reload(name)

	return 
end
ViewportManager.editor_add_environment_created_callback = function (self, func)
	self._env_manager:editor_add_created_callback(func)

	return 
end
ViewportManager.preload_environment = function (self, name)
	self._env_manager:preload_environment(name)

	return 
end
ViewportManager.get_predefined_environment_filter_map = function (self)
	return self._env_manager:get_predefined_environment_filter_map()
end
ViewportManager.get_environment_value = function (self, path, data_path_key)
	return self._env_manager:get_value(path, data_path_key)
end
ViewportManager.has_data_path_key = function (self, data_path_key)
	return self._env_manager:has_data_path_key(data_path_key)
end
ViewportManager.is_deprecated_data_path = function (self, data_path)
	return self._env_manager:is_deprecated_data_path(data_path)
end
ViewportManager.create_global_environment_modifier = function (self, data_path_key, is_override, modifier_func)
	for _, vp in ipairs(self.viewports(self)) do
		vp.create_environment_modifier(vp, data_path_key, is_override, modifier_func)
	end

	self._env_manager:set_global_environment_modifier(data_path_key, is_override, modifier_func)

	return data_path_key
end
ViewportManager.destroy_global_environment_modifier = function (self, data_path_key)
	for _, vp in ipairs(self.viewports(self)) do
		vp.destroy_environment_modifier(vp, data_path_key)
	end

	self._env_manager:set_global_environment_modifier(data_path_key, nil, nil)

	return 
end
ViewportManager.update_global_environment_value = function (self, data_path_key)
	for _, vp in ipairs(self.viewports(self)) do
		vp.update_environment_value(vp, data_path_key)
	end

	return 
end
ViewportManager.set_default_environment = function (self, default_environment_path, blend_duration, blend_bezier_curve)
	self._env_manager:set_default_environment(default_environment_path)

	for _, viewport in ipairs(self.viewports(self)) do
		viewport.on_default_environment_changed(viewport, default_environment_path, blend_duration, blend_bezier_curve)
	end

	return 
end
ViewportManager.default_environment = function (self)
	return self._env_manager:default_environment()
end
ViewportManager.game_default_environment = function (self)
	return self._env_manager:game_default_environment()
end
ViewportManager.editor_reset_environment = function (self)
	for _, vp in ipairs(self.active_viewports(self)) do
		vp.set_environment(vp, self.game_default_environment(self), nil, nil, nil, nil)
	end

	return 
end
ViewportManager.set_override_environment = function (self, environment_path, blend_duration, blend_bezier_curve)
	self._env_manager:set_override_environment(environment_path)

	for _, viewport in ipairs(self.viewports(self)) do
		viewport.on_override_environment_changed(viewport, environment_path, blend_duration, blend_bezier_curve)
	end

	return 
end
ViewportManager.move_to_front = function (self, vp)
	self._move_ao_to_front(self, vp)

	return 
end
ViewportManager._viewport_destroyed = function (self, vp)
	self._del_accessobj(self, vp)

	self._current_camera = nil

	return 
end
ViewportManager._get_environment_manager = function (self)
	return self._env_manager
end
ViewportManager._prioritize_and_activate = function (self)
	local old_first_vp = self.first_active_viewport(self)

	ViewportManager.super._prioritize_and_activate(self)

	local first_vp = self.first_active_viewport(self)

	if old_first_vp ~= first_vp then
		if old_first_vp then
			old_first_vp.set_first_viewport(old_first_vp, false)
		end

		if first_vp then
			first_vp.set_first_viewport(first_vp, true)
		end
	end

	return 
end
ViewportManager.first_active_world_viewport = function (self)
	for _, vp in ipairs(self.active_viewports(self)) do
		if vp.is_rendering_scene(vp, "World") then
			return vp
		end
	end

	return 
end
ViewportManager.get_current_camera = function (self)
	if self._current_camera then
		return self._current_camera
	end

	local vps = self._all_really_active(self)
	self._current_camera = 0 < #vps and vps[1]:camera()

	return self._current_camera
end
ViewportManager.get_current_camera_position = function (self)
	if self._current_camera_position_updated then
		return self._current_camera_position
	end

	if self.get_current_camera(self) then
		self.get_current_camera(self):m_position(self._current_camera_position)

		self._current_camera_position_updated = true
	end

	return self._current_camera_position
end
ViewportManager.get_current_camera_rotation = function (self)
	if self._current_camera_rotation then
		return self._current_camera_rotation
	end

	self._current_camera_rotation = self.get_current_camera(self) and self.get_current_camera(self):rotation()

	return self._current_camera_rotation
end
ViewportManager.get_active_vp = function (self)
	return self.active_vp(self):vp()
end
ViewportManager.active_vp = function (self)
	local vps = self.active_viewports(self)

	return 0 < #vps and vps[1]
end
local is_win32 = SystemInfo:platform() == Idstring("WIN32")
local is_ps4 = SystemInfo:platform() == Idstring("PS4")
local is_xb1 = SystemInfo:platform() == Idstring("XB1")
ViewportManager.get_safe_rect = function (self)
	local a = (is_win32 and 0.032) or ((is_ps4 or is_xb1) and 0.05) or 0.075
	local b = a*2 - 1

	return {
		x = a,
		y = a,
		width = b,
		height = b
	}
end
ViewportManager.get_safe_rect_pixels = function (self)
	local res = RenderSettings.resolution
	local safe_rect_scale = self.get_safe_rect(self)
	local safe_rect_pixels = {
		x = math.round(safe_rect_scale.x*res.x),
		y = math.round(safe_rect_scale.y*res.y),
		width = math.round(safe_rect_scale.width*res.x),
		height = math.round(safe_rect_scale.height*res.y)
	}

	return safe_rect_pixels
end
ViewportManager.set_resolution = function (self, resolution)
	if RenderSettings.resolution ~= resolution or (self._render_settings_change_map and self._render_settings_change_map.resolution ~= resolution) then
		self._render_settings_change_map = self._render_settings_change_map or {}
		self._render_settings_change_map.resolution = resolution
	end

	return 
end
ViewportManager.is_fullscreen = function (self)
	if self._render_settings_change_map and self._render_settings_change_map.fullscreen ~= nil then
		return self._render_settings_change_map.fullscreen
	else
		return RenderSettings.fullscreen
	end

	return 
end
ViewportManager.set_fullscreen = function (self, fullscreen)
	if not RenderSettings.fullscreen ~= not fullscreen or (self._render_settings_change_map and not self._render_settings_change_map.fullscreen ~= not fullscreen) then
		self._render_settings_change_map = self._render_settings_change_map or {}
		self._render_settings_change_map.fullscreen = not not fullscreen
	end

	return 
end
ViewportManager.set_aspect_ratio = function (self, aspect_ratio)
	if RenderSettings.aspect_ratio ~= aspect_ratio or (self._render_settings_change_map and self._render_settings_change_map.aspect_ratio ~= aspect_ratio) then
		self._render_settings_change_map = self._render_settings_change_map or {}
		self._render_settings_change_map.aspect_ratio = aspect_ratio
		self._aspect_ratio = aspect_ratio
	end

	return 
end
ViewportManager.set_vsync = function (self, vsync)
	if RenderSettings.v_sync ~= vsync or (self._render_settings_change_map and self._render_settings_change_map.v_sync ~= vsync) then
		self._render_settings_change_map = self._render_settings_change_map or {}
		self._render_settings_change_map.v_sync = vsync
		self._v_sync = vsync
	end

	return 
end
ViewportManager.set_adapter_index = function (self, adapter_index)
	if RenderSettings.adapter_index ~= adapter_index or (self._render_settings_change_map and self._render_settings_change_map.adapter_index ~= adapter_index) then
		self._render_settings_change_map = self._render_settings_change_map or {}
		self._render_settings_change_map.adapter_index = adapter_index
	end

	return 
end
ViewportManager.reset_viewport_settings = function (self)
	Application:reset_render_settings({
		"adapter_index",
		"aspect_ratio",
		"fullscreen",
		"resolution",
		"v_sync"
	})

	self._render_settings_change_map = nil

	self.resolution_changed(self)

	return 
end
ViewportManager.aspect_ratio = function (self)
	return self._aspect_ratio
end
ViewportManager.set_aspect_ratio2 = function (self, aspect_ratio)
	self._aspect_ratio = aspect_ratio

	return 
end

return 
