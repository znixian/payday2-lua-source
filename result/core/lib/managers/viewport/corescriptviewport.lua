core:module("CoreScriptViewport")
core:import("CoreApp")
core:import("CoreMath")
core:import("CoreCode")
core:import("CoreAccessObjectBase")
core:import("CoreEnvironmentFeeder")
core:import("CoreEnvironmentHandler")

_ScriptViewport = _ScriptViewport or class(CoreAccessObjectBase.AccessObjectBase)
DEFAULT_NETWORK_PORT = 31254
DEFAULT_NETWORK_LSPORT = 31255
NETWORK_SLAVE_RECEIVER = Idstring("scriptviewport_slave")
NETWORK_MASTER_RECEIVER = Idstring("scriptviewport_master")
_ScriptViewport.init = function (self, x, y, width, height, vpm, name)
	_ScriptViewport.super.init(self, vpm, name)

	self._vp = Application:create_world_viewport(x, y, width, height)
	self._vpm = vpm
	self._replaced_vp = false
	self._width_mul_enabled = true
	self._render_params = (Global.render_debug.render_sky and {
		"World",
		self._vp,
		nil,
		"Underlay",
		self._vp
	}) or {
		"World",
		self._vp
	}
	self._env_handler = CoreEnvironmentHandler.EnvironmentHandler:new(vpm._get_environment_manager(vpm), self == vpm.active_vp(vpm))
	self._ref_fov_stack = {}
	self._enable_adaptive_quality = true
	self._init_trace = debug.traceback()

	return 
end
_ScriptViewport.enable_slave = function (self, port)
	Application:stack_dump_error("Deprecated call")

	return 
end
_ScriptViewport.enable_master = function (self, host_name, port, master_listener_port, net_pump)
	Application:stack_dump_error("Deprecated call")

	return 
end
_ScriptViewport.render_params = function (self)
	return self._render_params
end
_ScriptViewport.set_render_params = function (self, ...)
	self._render_params = {
		...
	}

	return 
end
_ScriptViewport.destroy = function (self)
	self.set_active(self, false)

	local vp = not self._replaced_vp and self._vp

	if CoreCode.alive(vp) then
		Application:destroy_viewport(vp)
	end

	self._vpm:_viewport_destroyed(self)
	self._env_handler:destroy()

	return 
end
_ScriptViewport.set_width_mul_enabled = function (self, b)
	self._width_mul_enabled = b

	return 
end
_ScriptViewport.width_mul_enabled = function (self)
	return self._width_mul_enabled
end
_ScriptViewport.set_first_viewport = function (self, set_first_viewport)
	self._env_handler:set_first_viewport(set_first_viewport)

	return 
end
_ScriptViewport.get_environment_value = function (self, data_path_key)
	return self._env_handler:get_value(data_path_key)
end
_ScriptViewport.get_environment_default_value = function (self, data_path_key)
	return self._env_handler:get_default_value(data_path_key)
end
_ScriptViewport.get_environment_path = function (self)
	return self._env_handler:get_path()
end
_ScriptViewport.set_environment = function (self, environment_path, blend_duration, blend_bezier_curve, filter_list, unfiltered_environment_path)
	self._env_handler:set_environment(environment_path, blend_duration, blend_bezier_curve, filter_list, unfiltered_environment_path)

	return 
end
_ScriptViewport.on_default_environment_changed = function (self, environment_path, blend_duration, blend_bezier_curve)
	self._env_handler:on_default_environment_changed(environment_path, blend_duration, blend_bezier_curve)

	return 
end
_ScriptViewport.on_override_environment_changed = function (self, environment_path, blend_duration, blend_bezier_curve)
	self._env_handler:on_override_environment_changed(environment_path, blend_duration, blend_bezier_curve)

	return 
end
_ScriptViewport.create_environment_modifier = function (self, data_path_key, is_override, modifier_func)
	return self._env_handler:create_modifier(data_path_key, is_override, modifier_func)
end
_ScriptViewport.destroy_environment_modifier = function (self, data_path_key)
	self._env_handler:destroy_modifier(data_path_key)

	return 
end
_ScriptViewport.force_apply_feeders = function (self)
	self._env_handler:force_apply_feeders()

	return 
end
_ScriptViewport.update_environment_value = function (self, data_path_key)
	return self._env_handler:update_value(data_path_key)
end
local mvec1 = Vector3()
local mvec2 = Vector3()
_ScriptViewport.update_environment_area = function (self, area_list, position_offset)
	local camera = self._vp:camera()

	if not camera then
		return 
	end

	local check_pos = mvec1
	local c_fwd = mvec2

	camera.m_position(camera, check_pos)
	mrotation.y(camera.rotation(camera), c_fwd)
	mvector3.multiply(c_fwd, position_offset)
	mvector3.add(check_pos, c_fwd)
	self._env_handler:update_environment_area(check_pos, area_list)

	return 
end
_ScriptViewport.on_environment_area_removed = function (self, area)
	self._env_handler:on_environment_area_removed(area)

	return 
end
_ScriptViewport.set_camera = function (self, camera)
	self._vp:set_camera(camera)
	self._set_width_multiplier(self)

	return 
end
_ScriptViewport.camera = function (self)
	return self._vp:camera()
end
_ScriptViewport.director = function (self)
	return self._vp:director()
end
_ScriptViewport.shaker = function (self)
	return self.director(self):shaker()
end
_ScriptViewport.vp = function (self)
	return self._vp
end
_ScriptViewport.alive = function (self)
	return CoreCode.alive(self._vp)
end
_ScriptViewport.reference_fov = function (self)
	local scene = self._render_params[1]
	local fov = -1
	local sh_pro = self._vp:get_post_processor_effect(scene, Idstring("shadow_processor"), Idstring("shadow_rendering"))

	if sh_pro then
		local sh_mod = sh_pro.modifier(sh_pro, Idstring("shadow_modifier"))

		if sh_mod then
			fov = math.deg(sh_mod.reference_fov(sh_mod))
		end
	end

	return fov
end
_ScriptViewport.push_ref_fov = function (self, fov)
	local scene = self._render_params[1]

	if fov < math.rad(self._vp:camera() and self._vp:camera():fov()) then
		return false
	end

	local sh_pro = self._vp:get_post_processor_effect(scene, Idstring("shadow_processor"), Idstring("shadow_rendering"))

	if sh_pro then
		local sh_mod = sh_pro.modifier(sh_pro, Idstring("shadow_modifier"))

		if sh_mod then
			table.insert(self._ref_fov_stack, sh_mod.reference_fov(sh_mod))
			sh_mod.set_reference_fov(sh_mod, math.rad(fov))

			return true
		end
	end

	return false
end
_ScriptViewport.pop_ref_fov = function (self)
	local scene = self._render_params[1]
	local sh_pro = self._vp:get_post_processor_effect(scene, Idstring("shadow_processor"), Idstring("shadow_rendering"))

	if sh_pro then
		local sh_mod = sh_pro.modifier(sh_pro, Idstring("shadow_modifier"))

		if sh_mod and 0 < #self._ref_fov_stack then
			local last = self._ref_fov_stack[#self._ref_fov_stack]

			if not self._vp:camera() or math.rad(self._vp:camera():fov()) <= last then
				sh_mod.set_reference_fov(sh_mod, self._ref_fov_stack[#self._ref_fov_stack])
				table.remove(self._ref_fov_stack, #self._ref_fov_stack)

				return true
			end
		end
	end

	return false
end
_ScriptViewport.set_visualization_mode = function (self, effect_name)
	local scene = self._render_params[1]
	local hdr_effect_interface = self._vp:get_post_processor_effect(scene, Idstring("hdr_post_processor"))
	local bloom_effect_interface = self._vp:get_post_processor_effect(scene, Idstring("bloom_combine_post_processor"))
	local is_deferred = effect_name == "deferred_lighting"

	if hdr_effect_interface then
		hdr_effect_interface.set_visibility(hdr_effect_interface, is_deferred)
	end

	if bloom_effect_interface then
		bloom_effect_interface.set_visibility(bloom_effect_interface, is_deferred)
	end

	self._vp:set_post_processor_effect(scene, Idstring("deferred"), Idstring(effect_name)):set_visibility(true)

	return 
end
_ScriptViewport.is_rendering_scene = function (self, scene_name)
	for _, param in ipairs(self.render_params(self)) do
		if param == scene_name then
			return true
		end
	end

	return false
end
_ScriptViewport.set_dof = function (self, clamp, near_focus_distance_min, near_focus_distance_max, far_focus_distance_min, far_focus_distance_max)
	return 
end
_ScriptViewport.replace_engine_vp = function (self, vp)
	self.destroy(self)

	self._replaced_vp = true
	self._vp = vp

	return 
end
_ScriptViewport.set_environment_editor_callback = function (self, env_editor_callback)
	self._env_editor_callback = env_editor_callback

	return 
end
_ScriptViewport.set_enable_adaptive_quality = function (self, enable)
	self._enable_adaptive_quality = enable

	return 
end
_ScriptViewport.use_adaptive_quality = function (self)
	return self._enable_adaptive_quality
end
_ScriptViewport._update = function (self, nr, t, dt)
	local is_first_viewport = nr == 1
	local scene = self._render_params[1]

	self._vp:update()

	if self._env_editor_callback then
		self._env_editor_callback(self._env_handler, self._vp, scene)
	else
		self._env_handler:update(is_first_viewport, self._vp, dt)
	end

	self._env_handler:apply(is_first_viewport, self._vp, scene)

	return 
end
_ScriptViewport._render = function (self, nr)
	if Global.render_debug.render_world then
		Application:render(unpack(self._render_params))
	end

	return 
end
_ScriptViewport._resolution_changed = function (self)
	self._set_width_multiplier(self)

	return 
end
_ScriptViewport._set_width_multiplier = function (self)
	local camera = self.camera(self)

	if CoreCode.alive(camera) and self._width_mul_enabled then
		local screen_res = Application:screen_resolution()
		local screen_pixel_aspect = screen_res.x/screen_res.y
		local rect = self._vp:get_rect()
		local vp_pixel_aspect = screen_pixel_aspect

		if 0 < rect.ph then
			vp_pixel_aspect = rect.pw/rect.ph
		end

		camera.set_width_multiplier(camera, CoreMath.width_mul(self._vpm:aspect_ratio())*vp_pixel_aspect/screen_pixel_aspect)
	end

	return 
end
_ScriptViewport.set_active = function (self, state)
	_ScriptViewport.super.set_active(self, state)

	if alive(self._vp) then
		self._vp:set_LOD_active(state)
	end

	return 
end

return 
