core:module("CoreEnvironmentHandler")
core:import("CoreClass")

local dummy_material = {
	set_variable = function ()
		return 
	end
}
EnvironmentHandler = EnvironmentHandler or CoreClass.class()
EnvironmentHandler.AREAS_PER_FRAME = 1
EnvironmentHandler.init = function (self, env_manager, is_first_viewport)
	self._env_manager = env_manager
	self._is_first_viewport = is_first_viewport
	self._feeder_map = {}
	self._update_feeder_map = {}
	self._apply_prio_feeder_map = {}
	self._apply_feeder_map = {}
	self._post_processor_modifier_material_map = {}
	self._area_iterator = 1

	self.set_environment(self, self._env_manager:default_environment(), nil, nil, nil, nil)

	return 
end
EnvironmentHandler.destroy = function (self)
	if self._feeder_map then
		for data_path_key, feeder in pairs(self._feeder_map) do
			feeder.destroy(feeder)
		end
	else
		Application:error("[EnvironmentManager] Destroy called too many times.")
	end

	self._env_manager = nil
	self._feeder_map = nil
	self._update_feeder_map = nil
	self._apply_prio_feeder_map = nil
	self._apply_feeder_map = nil
	self._post_processor_modifier_material_map = nil

	return 
end
EnvironmentHandler.set_environment = function (self, path, blend_duration, blend_bezier_curve, filter_list, unfiltered_environment_path)
	if self._override_environment ~= path then
		self._path = path
	end

	if self._override_environment then
		path = self._override_environment
	end

	local env_data = self._env_manager:_get_data(path)
	local filtered_env_data = nil

	if filter_list then
		filtered_env_data = {}

		for _, data_path_key in ipairs(filter_list) do
			filtered_env_data[data_path_key] = env_data[data_path_key]
		end

		if unfiltered_environment_path then
			local unfiltered_env_data = self._env_manager:_get_data(unfiltered_environment_path)

			for data_path_key, value in pairs(unfiltered_env_data) do
				if filtered_env_data[data_path_key] == nil then
					filtered_env_data[data_path_key] = value
				end
			end
		end
	else
		filtered_env_data = env_data
	end

	if blend_duration and 0 < blend_duration then
		self._blend_time = 0
		self._blend_duration = blend_duration
		self._blend_bezier_curve = blend_bezier_curve
	else
		self._blend_duration = nil
		blend_duration = nil
	end

	for data_path_key, value in pairs(filtered_env_data) do
		local feeder = self._feeder_map[data_path_key]

		if not feeder then
			feeder = self._env_manager:_create_feeder(data_path_key, value)
			self._feeder_map[data_path_key] = feeder

			self._add_apply_feeder(self, feeder)

			if feeder.get_modifier(feeder) then
				self._update_feeder_map[data_path_key] = feeder
			end
		else
			local modifier_func, is_modifier_override = feeder.get_modifier(feeder)
			local is_changed = modifier_func or not feeder.equals(feeder, value)

			if blend_duration or is_modifier_override then
				feeder.set_target(feeder, value)
			else
				feeder.set(feeder, value)
			end

			if not is_modifier_override then
				if is_changed then
					if blend_duration then
						self._update_feeder_map[data_path_key] = feeder
					else
						self._add_apply_feeder(self, feeder)
					end
				else
					self._update_feeder_map[data_path_key] = nil
				end
			end
		end
	end

	return 
end
EnvironmentHandler.get_path = function (self)
	return self._path
end
EnvironmentHandler.create_modifier = function (self, data_path_key, is_override, func)
	local feeder = self._feeder_map[data_path_key]

	if not feeder then
		Application:stack_dump_error("[EnvironmentManager] Environment need to be set before creating a modifier or the requested modifier doesn't exist.")

		return nil
	else
		feeder.set_modifier(feeder, func, is_override)

		self._update_feeder_map[data_path_key] = feeder

		return data_path_key
	end

	return 
end
EnvironmentHandler.destroy_modifier = function (self, data_path_key)
	local feeder = self._feeder_map[data_path_key]

	if not feeder then
		Application:stack_dump_error("[EnvironmentManager] Environment need to be set before destroying a modifier.")
	else
		feeder.set_modifier(feeder, nil, nil)

		self._update_feeder_map[data_path_key] = feeder

		self._add_apply_feeder(self, feeder)
	end

	return 
end
EnvironmentHandler.update_value = function (self, data_path_key)
	local feeder = self._feeder_map[data_path_key]

	if feeder then
		self._update_feeder_map[data_path_key] = feeder
	else
		Application:stack_dump_error("[EnvironmentManager] Invalid data path key.")
	end

	return 
end
EnvironmentHandler.get_value = function (self, data_path_key)
	local feeder = self._feeder_map[data_path_key]

	if feeder then
		return feeder.get_current(feeder)
	else
		Application:stack_dump_error("[EnvironmentManager] Invalid data path key.")

		return nil
	end

	return 
end
EnvironmentHandler.get_default_value = function (self, data_path_key)
	return self._env_manager:get_default_value(data_path_key)
end
EnvironmentHandler.editor_set_value = function (self, data_path_key, value)
	local feeder = self._feeder_map[data_path_key]

	if not feeder and self._env_manager:has_data_path_key(data_path_key) then
		feeder = self._env_manager:_create_feeder(data_path_key, value)
		self._feeder_map[data_path_key] = feeder
	end

	if not feeder then
		return false
	elseif not feeder.equals(feeder, value) then
		feeder.set(feeder, value)
		self._add_apply_feeder(self, feeder)
	end

	return true
end
EnvironmentHandler.update = function (self, is_first_viewport, viewport, dt)
	local scale = nil

	if self._blend_duration then
		self._blend_time = self._blend_time + dt
		scale = self._blend_time/self._blend_duration

		if self._blend_bezier_curve then
			scale = math.bezier(self._blend_bezier_curve, math.clamp(scale, 0, 1))
		end

		scale = math.clamp(scale, 0, 1)

		if scale == 1 then
			self._blend_duration = nil
		end
	else
		scale = 1
	end

	self.set_first_viewport(self, is_first_viewport)

	local remove_update_list = nil

	for data_path_key, feeder in pairs(self._update_feeder_map) do
		local is_done, is_not_changed = feeder.update(feeder, self, scale)

		if is_done then
			remove_update_list = remove_update_list or {}

			table.insert(remove_update_list, data_path_key)
		end

		if not is_not_changed then
			self._add_apply_feeder(self, feeder)
		end
	end

	if remove_update_list then
		for _, data_path_key in ipairs(remove_update_list) do
			self._update_feeder_map[data_path_key] = nil
		end
	end

	return 
end
EnvironmentHandler.force_apply_feeders = function (self)
	self._post_processor_modifier_material_map = {}

	for _, feeder in pairs(self._feeder_map) do
		self._add_apply_feeder(self, feeder)
	end

	return 
end
EnvironmentHandler.apply = function (self, is_first_viewport, viewport, scene)
	self.set_first_viewport(self, is_first_viewport)

	if next(self._apply_prio_feeder_map) then
		for _, feeder in pairs(self._apply_prio_feeder_map) do
			if not feeder.IS_GLOBAL or is_first_viewport then
				feeder.apply(feeder, self, viewport, scene)
			end
		end

		self._apply_prio_feeder_map = {}
	end

	if next(self._apply_feeder_map) then
		for _, feeder in pairs(self._apply_feeder_map) do
			if not feeder.IS_GLOBAL or is_first_viewport then
				feeder.apply(feeder, self, viewport, scene)
			end
		end

		self._apply_feeder_map = {}
	end

	return 
end
EnvironmentHandler.update_environment_area = function (self, check_pos, area_list)
	if self._current_area then
		local is_still_inside = self._current_area:still_inside(check_pos)
		local is_inside_new = self._check_inside(self, check_pos, area_list, (is_still_inside and self._current_area:prio()) or nil)

		if not is_still_inside and not is_inside_new then
			self._leave_current_area(self)
		end
	else
		self._check_inside(self, check_pos, area_list, nil)
	end

	return 
end
EnvironmentHandler.on_environment_area_removed = function (self, area)
	if area == self._current_area then
		self._leave_current_area(self)
	end

	self._area_iterator = 1

	return 
end
EnvironmentHandler.on_default_environment_changed = function (self, default_environment_path, blend_duration, blend_bezier_curve)
	if not self._current_area then
		self.set_environment(self, default_environment_path, blend_duration, blend_bezier_curve, nil, nil)
	end

	return 
end
EnvironmentHandler.on_override_environment_changed = function (self, override_environment_path, blend_duration, blend_bezier_curve)
	self._override_environment = override_environment_path

	self.set_environment(self, self._override_environment or self._path, blend_duration, blend_bezier_curve, nil, nil)

	return 
end
EnvironmentHandler.set_first_viewport = function (self, is_first_viewport)
	if self._is_first_viewport ~= is_first_viewport then
		self._is_first_viewport = is_first_viewport

		if is_first_viewport then
			for data_path_key, feeder in pairs(self._feeder_map) do
				local old_feeder = self._env_manager:_set_global_feeder(feeder)

				if not old_feeder or not feeder.equals(feeder, old_feeder.get_current(old_feeder)) then
					self._add_apply_feeder(self, feeder)
				end
			end
		end
	end

	return 
end
EnvironmentHandler._check_inside = function (self, check_pos, area_list, min_prio)
	local area_count = #area_list

	if 0 < area_count then
		local areas_per_frame = self.AREAS_PER_FRAME
		local check_count = 0

		for i = 1, area_count, 1 do
			local area = area_list[self._area_iterator]
			self._area_iterator = math.mod(self._area_iterator, area_count) + 1

			if area.is_higher_prio(area, min_prio) then
				if area.is_inside(area, check_pos) then
					local id = area.id(area)

					if self._current_area_id ~= id then
						local environment = area.environment(area)
						local blend_duration = area.transition_time(area)
						local blend_bezier_curve = area.bezier_curve(area)
						local filter_list = area.filter_list(area)

						if area.permanent(area) then
							managers.viewport:set_default_environment(environment, blend_duration, blend_bezier_curve)

							if self._current_area then
								self.set_environment(self, environment, blend_duration, blend_bezier_curve, filter_list, self._env_manager:default_environment())
							end
						else
							self.set_environment(self, environment, blend_duration, blend_bezier_curve, filter_list, self._current_area ~= nil and self._env_manager:default_environment())
						end
					end

					self._current_area = area
					self._current_area_id = id
					self._area_iterator = 1

					return true
				end

				check_count = check_count + 1

				if areas_per_frame <= check_count then
					break
				end
			end
		end
	end

	return false
end
EnvironmentHandler._leave_current_area = function (self)
	self.set_environment(self, self._env_manager:default_environment(), self._current_area:transition_time(), self._current_area:bezier_curve(), nil, nil)

	self._current_area = nil
	self._current_area_id = nil

	return 
end
EnvironmentHandler._get_post_processor_modifier_material = function (self, viewport, scene, id, ids_processor_name, ids_effect_name, ids_modifier)
	local scene_map = self._post_processor_modifier_material_map[scene]
	local material = nil

	if not scene_map then
		scene_map = {}
		self._post_processor_modifier_material_map[scene] = scene_map
	else
		material = scene_map[id]
	end

	if not material then
		local post_processor = viewport.get_post_processor_effect(viewport, scene, ids_processor_name, ids_effect_name)

		if post_processor then
			local modifier = post_processor.modifier(post_processor, ids_modifier)

			if modifier then
				material = modifier.material(modifier)
			else
				material = dummy_material

				Application:stack_dump_error("[EnvironmentManager] Invalid post processor. Scene: " .. tostring(scene) .. ", Processor: " .. tostring(ids_processor_name) .. ", Effect: " .. tostring(ids_effect_name) .. ", Modifier: " .. tostring(ids_modifier))
			end
		else
			material = dummy_material

			Application:stack_dump_error("[EnvironmentManager] Invalid post processor. Scene: " .. tostring(scene) .. ", Processor: " .. tostring(ids_processor_name) .. ", Effect: " .. tostring(ids_effect_name) .. ", Modifier: " .. tostring(ids_modifier))
		end

		scene_map[id] = material
	end

	return material
end
EnvironmentHandler._add_apply_feeder = function (self, feeder)
	if feeder.AFFECTED_LIST then
		self._apply_prio_feeder_map[feeder.APPLY_GROUP_ID] = feeder

		for _, affected_feeder in ipairs(feeder.AFFECTED_LIST) do
			self._apply_feeder_map[affected_feeder.APPLY_GROUP_ID] = self._feeder_map[affected_feeder.DATA_PATH_KEY]
		end
	else
		self._apply_feeder_map[feeder.APPLY_GROUP_ID] = feeder
	end

	return 
end

return 
