core:module("CoreSubtitleManager")
core:import("CoreClass")
core:import("CoreDebug")
core:import("CoreTable")
core:import("CoreSubtitlePresenter")
core:import("CoreSubtitleSequence")
core:import("CoreSubtitleSequencePlayer")

SubtitleManager = SubtitleManager or CoreClass.class()
SubtitleManager.init = function (self)
	self.__subtitle_sequences = {}
	self.__loaded_sequence_file_paths = {}
	self.__presenter = CoreSubtitlePresenter.DebugPresenter:new()

	self._update_presenter_visibility(self)

	return 
end
SubtitleManager.destroy = function (self)
	self.set_presenter(self, nil)

	return 
end
SubtitleManager.presenter = function (self)
	return assert(self.__presenter, "Invalid presenter. SubtitleManager might have been destroyed.")
end
SubtitleManager.set_presenter = function (self, presenter)
	assert(presenter == nil or type(presenter.preprocess_sequence) == "function", "Invalid presenter.")

	if self.__presenter then
		self.__presenter:destroy()
	end

	self.__presenter = presenter

	if self.__presenter then
		self._update_presenter_visibility(self)
	end

	return 
end
SubtitleManager.load_sequences = function (self, sequence_file_path)
	local root_node = DB:load_node("subtitle_sequence", sequence_file_path)

	assert(root_node.name(root_node) == "subtitle_sequence", "File is not a subtitle sequence file.")

	self.__loaded_sequence_file_paths[sequence_file_path] = true

	for sequence_node in root_node.children(root_node) do
		if sequence_node.name(sequence_node) == "sequence" then
			local sequence = CoreSubtitleSequence.SubtitleSequence:new(sequence_node)
			self.__subtitle_sequences[sequence.name(sequence)] = sequence
		end
	end

	return 
end
SubtitleManager.reload_sequences = function (self)
	self.__subtitle_sequences = {}

	for sequence_file_path, _ in pairs(self.__loaded_sequence_file_paths) do
		self.load_sequences(self, sequence_file_path)
	end

	return 
end
SubtitleManager.update = function (self, time, delta_time)
	if self.__player then
		self.__player:update(time, delta_time)

		if self.__player:is_done() then
			self.__player = nil
		end
	end

	self.presenter(self):update(time, delta_time)

	return 
end
SubtitleManager.enabled = function (self)
	return Global.__SubtitleManager__enabled or false
end
SubtitleManager.set_enabled = function (self, enabled)
	Global.__SubtitleManager__enabled = not not enabled

	self._update_presenter_visibility(self)

	return 
end
SubtitleManager.visible = function (self)
	return not self.__hidden
end
SubtitleManager.set_visible = function (self, visible)
	self.__hidden = not visible or nil

	self._update_presenter_visibility(self)

	return 
end
SubtitleManager.clear_subtitle = function (self)
	self.show_subtitle_localized(self, "")

	return 
end
SubtitleManager.is_showing_subtitles = function (self)
	return self.enabled(self) and self.visible(self) and self.__player ~= nil
end
SubtitleManager.show_subtitle = function (self, string_id, duration, macros)
	self.show_subtitle_localized(self, managers.localization:text(string_id, macros), duration)

	return 
end
SubtitleManager.show_subtitle_localized = function (self, localized_string, duration)
	local sequence = CoreSubtitleSequence.SubtitleSequence:new()

	sequence.add_subtitle(sequence, CoreSubtitleSequence.Subtitle:new(localized_string, 0, duration or 3))

	self.__player = CoreSubtitleSequencePlayer.SubtitleSequencePlayer:new(sequence, self.presenter(self))

	return 
end
SubtitleManager.run_subtitle_sequence = function (self, sequence_id)
	local sequence = sequence_id and assert(self.__subtitle_sequences[sequence_id], string.format("Sequence \"%s\" not found.", sequence_id))
	self.__player = sequence and CoreSubtitleSequencePlayer.SubtitleSequencePlayer:new(sequence, self.presenter(self))

	return 
end
SubtitleManager.subtitle_sequence_ids = function (self)
	return CoreTable.table.map_keys(self.__subtitle_sequences or {})
end
SubtitleManager.has_subtitle_sequence = function (self, sequence_id)
	return (self.__subtitle_sequences and self.__subtitle_sequences[sequence_id]) ~= nil
end
SubtitleManager._update_presenter_visibility = function (self)
	local presenter = self.presenter(self)
	local show_presenter = self.enabled(self) and self.visible(self) and (not managers.user or managers.user:get_setting("subtitle"))
	show_presenter = false

	presenter[(show_presenter and "show") or "hide"](presenter)

	return 
end

return 
