core:module("CoreSubtitleSequence")
core:import("CoreClass")

SubtitleSequence = SubtitleSequence or CoreClass.class()
Subtitle = Subtitle or CoreClass.class()
StringIDSubtitle = StringIDSubtitle or CoreClass.class(Subtitle)
SubtitleSequence.init = function (self, sequence_node)
	if sequence_node then
		self._load_from_xml(self, sequence_node)
	end

	return 
end
SubtitleSequence.name = function (self)
	return self.parameters(self).name or ""
end
SubtitleSequence.duration = function (self)
	return self.__subtitles and self.__subtitles[#self.__subtitles]:end_time()
end
SubtitleSequence.parameters = function (self)
	return self.__parameters or {}
end
SubtitleSequence.subtitles = function (self)
	return self.__subtitles or {}
end
SubtitleSequence.add_subtitle = function (self, subtitle)
	self.__subtitles = self.__subtitles or {}

	table.insert_sorted(self.__subtitles, subtitle, function (a, b)
		return a.start_time(a) < b.start_time(b)
	end)

	return 
end
SubtitleSequence._load_from_xml = function (self, sequence_node)
	assert(managers.localization, "Localization Manager not ready.")
	assert(sequence_node and sequence_node.name(sequence_node) == "sequence", "Attempting to construct from non-sequence XML node.")
	assert(sequence_node.parameter(sequence_node, "name"), "Sequence must have a name.")

	self.__parameters = sequence_node.parameter_map(sequence_node)
	self.__subtitles = {}

	for node in sequence_node.children(sequence_node) do
		local string_id = self._xml_assert(self, node.parameter(node, "text_id"), node, string.format("Sequence \"%s\" has entries without text_ids.", self.name(self)))

		if not managers.localization:exists(string_id) then
			self._report_bad_string_id(self, string_id)
		end

		local start_time = self._xml_assert(self, tonumber(node.parameter(node, "time")), node, string.format("Sequence \"%s\" has entries without valid times.", self.name(self)))
		local subtitle = StringIDSubtitle:new(string_id, start_time, tonumber(node.parameter(node, "duration") or 2))

		self.add_subtitle(self, CoreClass.freeze(subtitle))
	end

	CoreClass.freeze(self.__subtitles)

	return 
end
SubtitleSequence._report_bad_string_id = function (self, string_id)
	Localizer:lookup(string_id)

	return 
end
SubtitleSequence._xml_assert = function (self, condition, node, message)
	return condition or error(string.format("Error parsing \"%s\" - %s", string.gsub(node.file(node), "^.*[/\\]", ""), message))
end
Subtitle.init = function (self, string_data, start_time, duration)
	self.__string_data = (string_data ~= nil and assert(tostring(string_data), "Invalid string argument.")) or ""
	self.__start_time = assert(tonumber(start_time), "Invalid start time argument.")
	self.__duration = (duration ~= nil and assert(tonumber(duration), "Invalid duration argument.")) or nil

	return 
end
Subtitle.string = function (self)
	return self.__string_data
end
Subtitle.start_time = function (self)
	return self.__start_time
end
Subtitle.end_time = function (self)
	return self.start_time(self) + (self.duration(self) or math.huge)
end
Subtitle.duration = function (self)
	return self.__duration
end
Subtitle.is_active_at_time = function (self, time)
	return self.start_time(self) < time and time < self.end_time(self)
end
StringIDSubtitle.string = function (self)
	assert(managers.localization, "Localization Manager not ready.")

	return managers.localization:text(self.__string_data)
end

return 
