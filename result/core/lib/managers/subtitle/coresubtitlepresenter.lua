core:module("CoreSubtitlePresenter")
core:import("CoreClass")
core:import("CoreCode")
core:import("CoreEvent")
core:import("CoreDebug")
core:import("CoreSubtitleSequence")

SubtitlePresenter = SubtitlePresenter or CoreClass.class()
DebugPresenter = DebugPresenter or CoreClass.class(SubtitlePresenter)
OverlayPresenter = OverlayPresenter or CoreClass.class(SubtitlePresenter)
SubtitlePresenter.destroy = function (self)
	return 
end
SubtitlePresenter.update = function (self, time, delta_time)
	return 
end
SubtitlePresenter.show = function (self)
	return 
end
SubtitlePresenter.hide = function (self)
	return 
end
SubtitlePresenter.show_text = function (self, text, duration)
	return 
end
SubtitlePresenter.preprocess_sequence = function (self, sequence)
	return sequence
end
DebugPresenter.destroy = function (self)
	CoreDebug.cat_print("subtitle_manager", string.format("SubtitlePresenter is destroyed."))

	return 
end
DebugPresenter.show = function (self)
	CoreDebug.cat_print("subtitle_manager", string.format("SubtitlePresenter is shown."))

	return 
end
DebugPresenter.hide = function (self)
	CoreDebug.cat_print("subtitle_manager", string.format("SubtitlePresenter hides."))

	return 
end
DebugPresenter.show_text = function (self, text, duration)
	CoreDebug.cat_print("subtitle_manager", string.format("SubtitlePresenter displays \"%s\" %s.", text, (duration and string.format("for %g seconds", duration)) or "until further notice"))

	return 
end
OverlayPresenter.init = function (self, font_name, font_size)
	self.set_font(self, font_name or self._default_font_name(self), font_size or self._default_font_size(self))
	self._clear_workspace(self)

	self.__resolution_changed_id = managers.viewport:add_resolution_changed_func(CoreEvent.callback(self, self, "_on_resolution_changed"))

	return 
end
OverlayPresenter.destroy = function (self)
	if self.__resolution_changed_id and managers.viewport then
		managers.viewport:remove_resolution_changed_func(self.__resolution_changed_id)
	end

	self.__resolution_changed_id = nil

	if CoreCode.alive(self.__subtitle_panel) then
		self.__subtitle_panel:stop()
		self.__subtitle_panel:clear()
	end

	self.__subtitle_panel = nil

	if CoreCode.alive(self.__ws) then
		self.__ws:gui():destroy_workspace(self.__ws)
	end

	self.__ws = nil

	return 
end
OverlayPresenter.show = function (self)
	self.__ws:show()

	return 
end
OverlayPresenter.hide = function (self)
	self.__ws:hide()

	return 
end
OverlayPresenter.set_debug = function (self, enabled)
	if self.__ws then
		self.__ws:panel():set_debug(enabled)
	end

	return 
end
OverlayPresenter.set_font = function (self, font_name, font_size)
	self.__font_name = assert(tostring(font_name), "Invalid font name parameter.")
	self.__font_size = assert(tonumber(font_size), "Invalid font size parameter.")

	if self.__subtitle_panel then
		for _, ui_element_name in ipairs({
			"layout",
			"label",
			"shadow"
		}) do
			local ui_element = self.__subtitle_panel:child(ui_element_name)

			if ui_element then
				ui_element.set_font(ui_element, Idstring(self.__font_name))
				ui_element.set_font_size(ui_element, self.__font_size)
			end
		end
	end

	local string_width_measure_text_field = CoreCode.alive(self.__ws) and self.__ws:panel():child("string_width")

	if string_width_measure_text_field then
		string_width_measure_text_field.set_font(string_width_measure_text_field, Idstring(self.__font_name))
		string_width_measure_text_field.set_font_size(string_width_measure_text_field, self.__font_size)
	end

	return 
end
OverlayPresenter.set_width = function (self, pixels)
	local safe_width = self._gui_width(self)
	self.__width = math.min(pixels, safe_width)

	if CoreCode.alive(self.__subtitle_panel) then
		self._layout_text_field(self):set_width(self.__width)
	end

	return 
end
OverlayPresenter.show_text = function (self, text, duration)
	local label = self.__subtitle_panel:child("label") or self.__subtitle_panel:text({
		name = "label",
		vertical = "bottom",
		word_wrap = true,
		wrap = true,
		align = "center",
		y = 1,
		x = 1,
		layer = 1,
		font = self.__font_name,
		font_size = self.__font_size,
		color = Color.white
	})
	local shadow = self.__subtitle_panel:child("shadow") or self.__subtitle_panel:text({
		y = 2,
		name = "shadow",
		vertical = "bottom",
		wrap = true,
		align = "center",
		word_wrap = true,
		visible = false,
		x = 2,
		layer = 0,
		font = self.__font_name,
		font_size = self.__font_size,
		color = Color.black:with_alpha(0.5)
	})

	label.set_text(label, text)
	shadow.set_text(shadow, text)

	return 
end
OverlayPresenter.preprocess_sequence = function (self, sequence)
	local new_sequence = CoreSubtitleSequence.SubtitleSequence:new()

	for _, subtitle in ipairs(sequence.subtitles(sequence)) do
		local subtitle_string = subtitle.string(subtitle)
		local wrapped_lines = self._split_string_into_lines(self, subtitle_string, sequence)
		local lines_per_batch = 2
		local batch_count = math.max(math.ceil(#wrapped_lines/lines_per_batch), 1)
		local batch_duration = subtitle.duration(subtitle)/batch_count
		local batch = 0

		for line = 1, batch_count*lines_per_batch, 2 do
			local wrapped_string = table.concat({
				wrapped_lines[line],
				wrapped_lines[line + 1]
			}, "\n")

			new_sequence.add_subtitle(new_sequence, CoreSubtitleSequence.Subtitle:new(wrapped_string, subtitle.start_time(subtitle) + batch_duration*batch, batch_duration))

			batch = batch + 1
		end
	end

	return new_sequence
end
OverlayPresenter._clear_workspace = function (self)
	if CoreCode.alive(self.__ws) then
		managers.gui_data:destroy_workspace(self.__ws)
	end

	self.__ws = managers.gui_data:create_saferect_workspace("screen", Overlay:gui())
	self.__subtitle_panel = self.__ws:panel():panel({
		layer = 150
	})

	self._on_resolution_changed(self)

	return 
end
OverlayPresenter._split_string_into_lines = function (self, subtitle_string, owning_sequence)
	return self._auto_word_wrap_string(self, subtitle_string)
end
OverlayPresenter._auto_word_wrap_string = function (self, subtitle_string)
	local layout_text_field = self._layout_text_field(self)

	layout_text_field.set_text(layout_text_field, subtitle_string)

	local line_breaks = table.collect(layout_text_field.line_breaks(layout_text_field), function (index)
		return index + 1
	end)
	local wrapped_lines = {}

	for line = 1, #line_breaks, 1 do
		local range_start = line_breaks[line]
		local range_end = line_breaks[line + 1]
		local string_range = utf8.sub(subtitle_string, range_start, (range_end or 0) - 1)

		table.insert(wrapped_lines, string.trim(string_range))
	end

	return wrapped_lines
end
OverlayPresenter._layout_text_field = function (self)
	assert(self.__subtitle_panel)

	return self.__subtitle_panel:child("layout") or self.__subtitle_panel:text({
		name = "layout",
		vertical = "bottom",
		word_wrap = true,
		wrap = true,
		align = "center",
		visible = false,
		width = self.__width,
		font = self.__font_name,
		font_size = self.__font_size
	})
end
OverlayPresenter._string_width = function (self, subtitle_string)
	local string_width_measure_text_field = self.__ws:panel():child("string_width") or self.__ws:panel():text({
		name = "string_width",
		wrap = false,
		visible = false,
		font = self.__font_name,
		font_size = self.__font_size
	})

	string_width_measure_text_field.set_text(string_width_measure_text_field, subtitle_string)

	local x, y, width, height = string_width_measure_text_field.text_rect(string_width_measure_text_field)

	return width
end
OverlayPresenter._on_resolution_changed = function (self)
	self.set_font(self, self.__font_name or self._default_font_name(self), self.__font_size or self._default_font_size(self))

	local width = self._gui_width(self)
	local height = self._gui_height(self)
	local safe_rect = managers.gui_data:corner_scaled_size()

	managers.gui_data:layout_corner_saferect_workspace(self.__ws)
	self.__subtitle_panel:set_width(safe_rect.width)
	self.__subtitle_panel:set_height(safe_rect.height - 120)
	self.__subtitle_panel:set_x(0)
	self.__subtitle_panel:set_y(0)
	self.set_width(self, self._string_width(self, "The quick brown fox jumped over the lazy dog bla bla bla bla bla bla bla bla bla blah blah blah blah blah ."))

	local label = self.__subtitle_panel:child("label")

	if label then
		label.set_h(label, self.__subtitle_panel:h())
		label.set_w(label, self.__subtitle_panel:w())
	end

	local shadow = self.__subtitle_panel:child("shadow")

	if shadow then
		shadow.set_h(shadow, self.__subtitle_panel:h())
		shadow.set_w(shadow, self.__subtitle_panel:w())
	end

	return 
end
OverlayPresenter._gui_width = function (self)
	return self.__subtitle_panel:width()
end
OverlayPresenter._gui_height = function (self)
	return self.__subtitle_panel:width()
end
OverlayPresenter._default_font_name = function (self)
	return "core/fonts/system_font"
end
OverlayPresenter._default_font_size = function (self)
	return 22
end

return 
