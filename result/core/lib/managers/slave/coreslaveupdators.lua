core:module("CoreSlaveUpdators")
core:import("CoreTable")
core:import("CoreCode")
core:import("CoreEngineAccess")

NETWORK_SLAVE_RECEIVER = Idstring("slaveupdator")
NETWORK_MASTER_RECEIVER = Idstring("masterupdator")
DEFAULT_NETWORK_PORT = 31254
DEFAULT_NETWORK_LSPORT = 31255
UNITS_PER_FRAME = 1
SlaveManager = SlaveManager or class()
Updator = Updator or class()
Updator.init = function (self)
	return 
end
Updator.peer = function (self)
	return self._peer
end
Updator.update = function (self)
	return 
end
Updator.set_batch_count = function (self)
	return 
end
SlaveUpdator = SlaveUpdator or class(Updator)
SlaveUpdator.init = function (self, vp, port)
	Network:bind(port or DEFAULT_NETWORK_PORT, self)
	Network:set_receiver(NETWORK_SLAVE_RECEIVER, self)

	self._units = {}
	self._pings = {}

	vp.enable_slave(vp, port)

	return true
end
SlaveUpdator.type = function (self)
	return "slave"
end
SlaveUpdator.slaveupdators_sync = function (self, key, name, pos, rot, rpc)
	local unit = self._units[key]

	if CoreCode.alive(unit) then
		unit.set_position(unit, pos)
		unit.set_rotation(unit, rot)

		self._pings[key] = nil
	else
		CoreEngineAccess._editor_load(Idstring("unit"), name.id(name))

		unit = World:spawn_unit_without_extensions(name.id(name), pos, rot)
		self._units[key] = unit
	end

	rpc.slaveupdators_ready_to_send(rpc)

	return 
end
SlaveUpdator.slaveupdators_reset = function (self, rpc)
	for _, unit in pairs(self._pings) do
		if CoreCode.alive(unit) then
			World:delete_unit(unit)
		end
	end

	self._pings = table.map_copy(self._units)

	rpc.slaveupdators_ready_to_send(rpc)

	return 
end
SlaveUpdator.slaveupdators_init = function (self)
	for _, unit in ipairs(World:find_units_quick("all")) do
		if CoreCode.alive(unit) then
			World:delete_unit(unit)
		end
	end

	self._units = {}
	self._pings = {}

	return 
end
MasterUpdator = MasterUpdator or class(Updator)
MasterUpdator.init = function (self, vp, host, port, master_listener_port, manual_pumping)
	self._peer = Network:handshake(host or "localhost", port or DEFAULT_NETWORK_PORT)

	if not self._peer then
		return false
	end

	Network:bind(master_listener_port or DEFAULT_NETWORK_LSPORT, self)
	Network:set_receiver(NETWORK_MASTER_RECEIVER, self)

	self._unitqueue = {}
	self._ready_to_send = true

	vp.enable_master(vp, host, port, master_listener_port, manual_pumping)
	self.set_batch_count(self)
	self._peer:slaveupdators_init()

	return true
end
MasterUpdator.type = function (self)
	return "master"
end
MasterUpdator.set_batch_count = function (self, count)
	self._units_per_frame = count or UNITS_PER_FRAME

	return 
end
MasterUpdator.update = function (self, t, dt)
	if #self._unitqueue == 0 then
		self._peer:slaveupdators_reset()

		self._unitqueue = World:find_units_quick("all")
	end

	if not self._ready_to_send then
		return 
	end

	local num_sent = 0

	for i = #self._unitqueue, 1, -1 do
		local unit = self._unitqueue[i]

		table.remove(self._unitqueue, i)

		if CoreCode.alive(unit) and unit.visible(unit) and unit.enabled(unit) and not unit.mover(unit) then
			self._peer:slaveupdators_sync(tostring(unit.key(unit)), assert(unit.name(unit):s()), unit.position(unit), unit.rotation(unit))

			self._ready_to_send = false
			num_sent = num_sent + 1

			if self._units_per_frame <= num_sent then
				break
			end
		end
	end

	return 
end
MasterUpdator.slaveupdators_ready_to_send = function (self)
	self._ready_to_send = true

	return 
end

return 
