core:module("CoreSlaveManager")
core:import("CoreCode")
core:import("CoreSlaveUpdators")

SlaveManager = SlaveManager or class()
SlaveManager.init = function (self)
	self._updator = nil
	self._status = false

	return 
end
SlaveManager.update = function (self, t, dt)
	if self._status then
		self._updator:update(t, dt)
	end

	return 
end
SlaveManager.paused_update = function (self, t, dt)
	self.update(self, t, dt)

	return 
end
SlaveManager.start = function (self, vp, port)
	assert(not self._status, "[SlaveManager] Already started!")

	self._updator, self._status = CoreSlaveUpdators.SlaveUpdator:new(vp, port)

	return self._status
end
SlaveManager.act_master = function (self, host, port, lsport, manual_pumping)
	self._updator, self._status = CoreSlaveUpdators.MasterUpdator:new(assert(managers.viewport:first_active_viewport()), host, port, lsport, manual_pumping)

	return self._status
end
SlaveManager.set_batch_count = function (self, count)
	assert(not count or 0 < count, "[SlaveManager] Batch count must be more then 0!")
	self._updator:set_batch_count(count)

	return 
end
SlaveManager.connected = function (self)
	return self._status
end
SlaveManager.type = function (self)
	return (self._updator and self._updator:type()) or nil
end
SlaveManager.peer = function (self)
	return (self._updator and self._updator:peer()) or nil
end

return 
