if core then
	core:module("CoreGuiDataManager")
end

GuiDataManager = GuiDataManager or class()
GuiDataManager.init = function (self, scene_gui, res, safe_rect_pixels, safe_rect, static_aspect_ratio)
	self._ws_size_data = {}
	self._scene_gui = scene_gui
	self._static_resolution = res
	self._safe_rect_pixels = safe_rect_pixels
	self._safe_rect = safe_rect
	self._static_aspect_ratio = static_aspect_ratio

	self._setup_workspace_data(self)

	self._workspace_configuration = {}

	return 
end
GuiDataManager.destroy = function (self)
	return 
end
GuiDataManager.create_saferect_workspace = function (self, workspace_object, scene)
	local ws = scene or self._scene_gui or Overlay:gui():create_scaled_screen_workspace(10, 10, 10, 10, 10)
	self._workspace_configuration[ws.key(ws)] = {
		workspace_object = workspace_object
	}

	self.layout_workspace(self, ws)

	return ws
end
GuiDataManager.create_fullscreen_workspace = function (self, workspace_object, scene)
	local ws = scene or self._scene_gui or Overlay:gui():create_scaled_screen_workspace(10, 10, 10, 10, 10)
	self._workspace_configuration[ws.key(ws)] = {
		workspace_object = workspace_object
	}

	self.layout_fullscreen_workspace(self, ws)

	return ws
end
GuiDataManager.create_fullscreen_16_9_workspace = function (self, workspace_object, scene)
	local ws = scene or self._scene_gui or Overlay:gui():create_scaled_screen_workspace(10, 10, 10, 10, 10)
	self._workspace_configuration[ws.key(ws)] = {
		workspace_object = workspace_object
	}

	self.layout_fullscreen_16_9_workspace(self, ws)

	return ws
end
GuiDataManager.create_corner_saferect_workspace = function (self, workspace_object, scene)
	local ws = scene or self._scene_gui or Overlay:gui():create_scaled_screen_workspace(10, 10, 10, 10, 10)
	self._workspace_configuration[ws.key(ws)] = {
		workspace_object = workspace_object
	}

	self.layout_corner_saferect_workspace(self, ws)

	return ws
end
GuiDataManager.create_1280_workspace = function (self, workspace_object, scene)
	local ws = scene or self._scene_gui or Overlay:gui():create_scaled_screen_workspace(10, 10, 10, 10, 10)
	self._workspace_configuration[ws.key(ws)] = {
		workspace_object = workspace_object
	}

	self.layout_1280_workspace(self, ws)

	return ws
end
GuiDataManager.create_corner_saferect_1280_workspace = function (self, workspace_object, scene)
	local ws = scene or self._scene_gui or Overlay:gui():create_scaled_screen_workspace(10, 10, 10, 10, 10)
	self._workspace_configuration[ws.key(ws)] = {
		workspace_object = workspace_object
	}

	self.layout_corner_saferect_1280_workspace(self, ws)

	return ws
end
GuiDataManager.destroy_workspace = function (self, ws)
	if not ws then
		return 
	end

	if ws then
		self._workspace_configuration[ws.key(ws)] = nil
	end

	ws.gui(ws):destroy_workspace(ws)

	return 
end
GuiDataManager.get_scene_gui = function (self)
	return self._scene_gui or Overlay:gui()
end
GuiDataManager._get_safe_rect_pixels = function (self)
	if self._safe_rect_pixels then
		return self._safe_rect_pixels
	end

	return managers.viewport:get_safe_rect_pixels()
end
GuiDataManager._get_safe_rect = function (self)
	if self._safe_rect then
		return self._safe_rect
	end

	return managers.viewport:get_safe_rect()
end
GuiDataManager._aspect_ratio = function (self)
	if self._static_aspect_ratio then
		return self._static_aspect_ratio
	end

	return managers.viewport:aspect_ratio()
end
local base_res = {
	x = 1280,
	y = 720
}
GuiDataManager._setup_workspace_data = function (self)
	print("[GuiDataManager:_setup_workspace_data]")

	self._saferect_data = {}
	self._corner_saferect_data = {}
	self._fullrect_data = {}
	self._fullrect_16_9_data = {}
	self._fullrect_1280_data = {}
	self._corner_saferect_1280_data = {}
	local safe_rect = self._get_safe_rect_pixels(self)
	local scaled_size = self.scaled_size(self)
	local res = self._static_resolution or RenderSettings.resolution
	local w = scaled_size.width
	local h = scaled_size.height
	local sh = math.min(safe_rect.height, safe_rect.width/w/h)
	local sw = math.min(safe_rect.width, safe_rect.height*w/h)
	local x = res.x/2 - (sh*w/h)/2
	local y = res.y/2 - sw/w/h/2
	self._safe_x = x
	self._safe_y = y
	self._saferect_data.w = w
	self._saferect_data.h = h
	self._saferect_data.width = self._saferect_data.w
	self._saferect_data.height = self._saferect_data.h
	self._saferect_data.x = x
	self._saferect_data.y = y
	self._saferect_data.on_screen_width = sw
	local h_c = w/safe_rect.width/safe_rect.height
	h = math.max(h, h_c)
	local w_c = h_c/h
	w = math.max(w, w/w_c)
	self._corner_saferect_data.w = w
	self._corner_saferect_data.h = h
	self._corner_saferect_data.width = self._corner_saferect_data.w
	self._corner_saferect_data.height = self._corner_saferect_data.h
	self._corner_saferect_data.x = safe_rect.x
	self._corner_saferect_data.y = safe_rect.y
	self._corner_saferect_data.on_screen_width = safe_rect.width
	sh = base_res.x/self._aspect_ratio(self)
	h = math.max(base_res.y, sh)
	sw = sh/h
	w = math.max(base_res.x, base_res.x/sw)
	self._fullrect_data.w = w
	self._fullrect_data.h = h
	self._fullrect_data.width = self._fullrect_data.w
	self._fullrect_data.height = self._fullrect_data.h
	self._fullrect_data.x = 0
	self._fullrect_data.y = 0
	self._fullrect_data.on_screen_width = res.x
	self._fullrect_data.convert_x = math.floor((w - self._saferect_data.w)/2)
	self._fullrect_data.convert_y = (h - scaled_size.height)/2
	self._fullrect_data.corner_convert_x = math.floor((self._fullrect_data.width - self._corner_saferect_data.width)/2)
	self._fullrect_data.corner_convert_y = math.floor((self._fullrect_data.height - self._corner_saferect_data.height)/2)
	w = base_res.x
	h = base_res.y
	sh = math.min(res.y, res.x/w/h)
	sw = math.min(res.x, res.y*w/h)
	x = res.x/2 - (sh*w/h)/2
	y = res.y/2 - sw/w/h/2
	self._fullrect_16_9_data.w = w
	self._fullrect_16_9_data.h = h
	self._fullrect_16_9_data.width = self._fullrect_16_9_data.w
	self._fullrect_16_9_data.height = self._fullrect_16_9_data.h
	self._fullrect_16_9_data.x = x
	self._fullrect_16_9_data.y = y
	self._fullrect_16_9_data.on_screen_width = sw
	self._fullrect_16_9_data.convert_x = math.floor((self._fullrect_16_9_data.w - self._saferect_data.w)/2)
	self._fullrect_16_9_data.convert_y = (self._fullrect_16_9_data.h - self._saferect_data.h)/2
	local aspect = math.clamp(res.x/res.y, 1, 1.7777777777777777)
	w = base_res.x
	h = base_res.x/aspect
	sw = math.min(res.x, res.y*aspect)
	sh = sw/w*h
	x = (res.x - sw)/2
	y = (res.y - sh)/2
	self._fullrect_1280_data.w = w
	self._fullrect_1280_data.h = h
	self._fullrect_1280_data.width = self._fullrect_1280_data.w
	self._fullrect_1280_data.height = self._fullrect_1280_data.h
	self._fullrect_1280_data.x = x
	self._fullrect_1280_data.y = y
	self._fullrect_1280_data.on_screen_width = sw
	self._fullrect_1280_data.sw = sw
	self._fullrect_1280_data.sh = sh
	self._fullrect_1280_data.aspect = aspect
	self._fullrect_1280_data.convert_x = math.floor((self._fullrect_data.w - self._fullrect_1280_data.w)/2)
	self._fullrect_1280_data.convert_y = math.floor((self._fullrect_data.h - self._fullrect_1280_data.h)/2)
	w = scaled_size.width
	h = scaled_size.width/aspect
	sw = math.min(safe_rect.width, safe_rect.height*aspect)
	sh = sw/w*h
	x = (res.x - sw)/2
	y = (res.y - sh)/2
	self._corner_saferect_1280_data.w = w
	self._corner_saferect_1280_data.h = h
	self._corner_saferect_1280_data.width = self._corner_saferect_1280_data.w
	self._corner_saferect_1280_data.height = self._corner_saferect_1280_data.h
	self._corner_saferect_1280_data.x = x
	self._corner_saferect_1280_data.y = y
	self._corner_saferect_1280_data.on_screen_width = sw

	return 
end
GuiDataManager.layout_workspace = function (self, ws)
	self._set_layout(self, ws, self._saferect_data)

	return 
end
GuiDataManager.layout_fullscreen_workspace = function (self, ws)
	self._set_layout(self, ws, self._fullrect_data)

	return 
end
GuiDataManager.layout_fullscreen_16_9_workspace = function (self, ws)
	self._set_layout(self, ws, self._fullrect_16_9_data)

	return 
end
GuiDataManager.layout_corner_saferect_workspace = function (self, ws)
	self._set_layout(self, ws, self._corner_saferect_data)

	return 
end
GuiDataManager.layout_1280_workspace = function (self, ws)
	self._set_layout(self, ws, self._fullrect_1280_data)

	return 
end
GuiDataManager.layout_corner_saferect_1280_workspace = function (self, ws)
	self._set_layout(self, ws, self._corner_saferect_1280_data)

	return 
end
GuiDataManager._set_linked_ws = function (self, ws, obj, screen_data)
	local rot = obj.rotation(obj)
	local size = obj.oobb(obj):size()
	local w = size.x
	local h = size.y
	local on_screen_height = screen_data.on_screen_width/screen_data.w*screen_data.h
	local gui = ws.gui(ws)
	local scale_w = w/gui.width(gui)
	local scale_h = h/gui.height(gui)
	local x_axis = Vector3(scale_w*screen_data.on_screen_width, 0, 0)

	mvector3.rotate_with(x_axis, rot)

	local y_axis = Vector3(0, scale_h*on_screen_height, 0)

	mvector3.rotate_with(y_axis, rot)

	local center = Vector3(w/2, h/2, 0)

	mvector3.rotate_with(center, rot)

	local offset = Vector3(screen_data.x*scale_w, screen_data.y*scale_h, 0)

	mvector3.rotate_with(offset, rot)
	ws.set_linked(ws, screen_data.w, screen_data.h, obj, obj.position(obj) - center + offset, x_axis, y_axis)

	return 
end
GuiDataManager._set_layout = function (self, ws, screen_data)
	self._ws_size_data[ws.key(ws)] = screen_data

	if self._workspace_objects then
		local conf = self._workspace_configuration[ws.key(ws)]
		local is_screen = conf and conf.workspace_object == "screen"

		if not is_screen then
			local obj = (conf and conf.workspace_object and self._workspace_objects[conf.workspace_object]) or self._workspace_objects.default

			if obj then
				self._set_linked_ws(self, ws, obj, screen_data)

				return 
			end
		end
	end

	ws.set_screen(ws, screen_data.w, screen_data.h, screen_data.x, screen_data.y, screen_data.on_screen_width)

	return 
end
GuiDataManager.scaled_size = function (self)
	local w = math.round(self._get_safe_rect(self).width*base_res.x)
	local h = math.round(self._get_safe_rect(self).height*base_res.y)

	return {
		x = 0,
		y = 0,
		width = w,
		height = h
	}
end
GuiDataManager.safe_scaled_size = function (self)
	return self._saferect_data
end
GuiDataManager.corner_scaled_size = function (self)
	return self._corner_saferect_data
end
GuiDataManager.full_scaled_size = function (self)
	return self._fullrect_data
end
GuiDataManager.full_16_9_size = function (self)
	return self._fullrect_16_9_data
end
GuiDataManager.full_1280_size = function (self)
	return self._fullrect_1280_data
end
GuiDataManager.convert_pos = function (self, ...)
	local x, y = self.convert_pos_float(self, ...)

	return math.round(x), math.round(y)
end
GuiDataManager.convert_pos_float = function (self, from_ws, to_ws, in_x, in_y)
	local from = self._ws_size_data[from_ws.key(from_ws)]
	local to = self._ws_size_data[to_ws.key(to_ws)]

	if not from or not to then
		return 
	end

	local scale = from.on_screen_width/from.w
	local x = in_x*scale + from.x
	local y = in_y*scale + from.y
	local scale = to.on_screen_width/to.w

	return (x - to.x)/scale, (y - to.y)/scale
end
GuiDataManager.full_to_full_16_9 = function (self, in_x, in_y)
	return self.safe_to_full_16_9(self, self.full_to_safe(self, in_x, in_y))
end
GuiDataManager.safe_to_full_16_9 = function (self, in_x, in_y)
	return self._fullrect_16_9_data.convert_x + in_x, self._fullrect_16_9_data.convert_y + in_y
end
GuiDataManager.full_16_9_to_safe = function (self, in_x, in_y)
	return in_x - self._fullrect_16_9_data.convert_x, in_y - self._fullrect_16_9_data.convert_y
end
GuiDataManager.safe_to_full = function (self, in_x, in_y)
	return self._fullrect_data.convert_x + in_x, self._fullrect_data.convert_y + in_y
end
GuiDataManager.full_to_safe = function (self, in_x, in_y)
	return in_x - self._fullrect_data.convert_x, in_y - self._fullrect_data.convert_y
end
GuiDataManager.corner_safe_to_full = function (self, in_x, in_y)
	return self._fullrect_data.corner_convert_x + in_x, self._fullrect_data.corner_convert_y + in_y
end
GuiDataManager.y_safe_to_full = function (self, in_y)
	return self._fullrect_data.convert_y + in_y
end
GuiDataManager.resolution_changed = function (self)
	self._setup_workspace_data(self)

	return 
end
GuiDataManager.set_scene_gui = function (self, gui)
	self._scene_gui = gui

	return 
end
GuiDataManager.set_workspace_objects = function (self, workspace_objects)
	self._workspace_objects = workspace_objects

	return 
end
GuiDataManager.add_workspace_object = function (self, name, workspace_object)
	self._workspace_objects = self._workspace_objects or {}
	self._workspace_objects[name] = workspace_object

	return 
end

return 
