CoreSky = CoreSky or class()
CoreSky.init = function (self)
	self._params = {}
	self._name = "default"

	return 
end
CoreSky.set_name = function (self, name)
	self._name = name

	return 
end
CoreSky.set_value = function (self, key, value)
	self._params[key] = value

	return 
end
CoreSky.value = function (self, key)
	return self._params[key]
end
CoreSky.parse = function (self, xml_node)
	self._params = {}

	for child in xml_node.children(xml_node) do
		local key = child.parameter(child, "key")
		local value = child.parameter(child, "value")

		if child.name(child) == "param" and key and key ~= "" and value and value ~= "" then
			if math.string_is_good_vector(value) then
				self._params[key] = math.string_to_vector(value)
			elseif tonumber(value) then
				self._params[key] = tonumber(value)
			elseif string.sub(value, 1, 1) == "#" then
				self._params[key] = self.database_lookup(self, string.sub(value, 2))
			else
				self._params[key] = tostring(value)
			end
		end
	end

	return 
end
CoreSky.copy = function (self, from)
	for key, value in pairs(from._params) do
		if type(value) == "string" then
			self._params[key] = value
		elseif type(value) ~= "number" then
			self._params[key] = Vector3(value.x, value.y, value.z)
		else
			self._params[key] = value
		end
	end

	self._name = from._name

	return 
end
CoreSky.interpolate = function (self, target, with, scale)
	local invscale = scale - 1

	for key, value in pairs(with._params) do
		if not target._params[key] then
			return 
		elseif type(value) ~= "string" then
			self._params[key] = target._params[key]*invscale + value*scale
		else
			self._params[key] = value
		end
	end

	self._name = with._name

	return 
end
CoreSky.database_lookup = function (self, str)
	local i = string.find(str, "#")
	local db_key = string.sub(str, 1, i - 1)
	local value_key = string.sub(str, i + 1)

	assert(db_key == "LightIntensityDB")

	local value = LightIntensityDB:lookup(value_key)

	assert(value)

	return value
end

return 
