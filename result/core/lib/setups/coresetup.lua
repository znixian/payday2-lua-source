core:import("CoreClass")
core:import("CoreEngineAccess")
core:import("CoreLocalizationManager")
core:import("CoreNewsReportManager")
core:import("CoreSubtitleManager")
core:import("CoreViewportManager")
core:import("CoreSequenceManager")
core:import("CoreMissionManager")
core:import("CoreControllerManager")
core:import("CoreListenerManager")
core:import("CoreSlotManager")
core:import("CoreCameraManager")
core:import("CoreExpressionManager")
core:import("CoreShapeManager")
core:import("CorePortalManager")
core:import("CoreDOFManager")
core:import("CoreRumbleManager")
core:import("CoreOverlayEffectManager")
core:import("CoreSessionManager")
core:import("CoreInputManager")
core:import("CoreGTextureManager")
core:import("CoreSmoketestManager")
core:import("CoreEnvironmentAreaManager")
core:import("CoreEnvironmentEffectsManager")
core:import("CoreSlaveManager")
core:import("CoreHelperUnitManager")
require("core/lib/managers/cutscene/CoreCutsceneManager")
require("core/lib/managers/CoreWorldCameraManager")
require("core/lib/managers/CoreSoundEnvironmentManager")
require("core/lib/managers/CoreMusicManager")
require("core/lib/managers/CoreWorldInstanceManager")
require("core/lib/utils/dev/editor/WorldHolder")
require("core/lib/managers/CoreEnvironmentControllerManager")
require("core/lib/units/CoreSpawnSystem")
require("core/lib/units/CoreUnitDamage")
require("core/lib/units/CoreEditableGui")
require("core/lib/units/data/CoreScriptUnitData")
require("core/lib/units/data/CoreWireData")
require("core/lib/units/data/CoreCutsceneData")

if Application:ews_enabled() then
	core:import("CoreLuaProfilerViewer")
	core:import("CoreDatabaseManager")
	core:import("CoreToolHub")
	core:import("CoreInteractionEditor")
	core:import("CoreInteractionEditorConfig")
	require("core/lib/utils/dev/tools/CoreUnitReloader")
	require("core/lib/utils/dev/tools/CoreUnitTestBrowser")
	require("core/lib/utils/dev/tools/CoreEnvEditor")
	require("core/lib/utils/dev/tools/CoreDatabaseBrowser")
	require("core/lib/utils/dev/tools/CoreLuaProfiler")
	require("core/lib/utils/dev/tools/CoreXMLEditor")
	require("core/lib/utils/dev/ews/CoreEWSDeprecated")
	require("core/lib/utils/dev/tools/CorePuppeteer")
	require("core/lib/utils/dev/tools/material_editor/CoreMaterialEditor")
	require("core/lib/utils/dev/tools/particle_editor/CoreParticleEditor")
	require("core/lib/utils/dev/tools/cutscene_editor/CoreCutsceneEditor")
end

CoreSetup = CoreSetup or class()
local _CoreSetup = CoreSetup
CoreSetup.init = function (self)
	CoreClass.close_override()

	self.__quit = false
	self.__exec = false
	self.__context = nil
	self.__firstupdate = true

	return 
end
CoreSetup.init_category_print = function (self)
	return 
end
CoreSetup.load_packages = function (self)
	return 
end
CoreSetup.unload_packages = function (self)
	return 
end
CoreSetup.start_boot_loading_screen = function (self)
	return 
end
CoreSetup.init_managers = function (self, managers)
	return 
end
CoreSetup.init_toolhub = function (self, toolhub)
	return 
end
CoreSetup.init_game = function (self)
	return 
end
CoreSetup.init_finalize = function (self)
	managers.mission:post_init()

	return 
end
CoreSetup.start_loading_screen = function (self)
	return 
end
CoreSetup.stop_loading_screen = function (self)
	return 
end
CoreSetup.update = function (self, t, dt)
	return 
end
CoreSetup.paused_update = function (self, t, dt)
	return 
end
CoreSetup.render = function (self)
	return 
end
CoreSetup.end_frame = function (self, t, dt)
	return 
end
CoreSetup.end_update = function (self, t, dt)
	return 
end
CoreSetup.paused_end_update = function (self, t, dt)
	return 
end
CoreSetup.save = function (self, data)
	return 
end
CoreSetup.load = function (self, data)
	return 
end
CoreSetup.destroy = function (self)
	return 
end
CoreSetup.freeflight = function (self)
	return self.__freeflight
end
CoreSetup.exec = function (self, context)
	self.__exec = true
	self.__context = context

	return 
end
CoreSetup.quit = function (self)
	if not Application:editor() then
		self.__quit = true
	end

	return 
end
CoreSetup.block_exec = function (self)
	return false
end
CoreSetup.block_quit = function (self)
	return false
end
CoreSetup.has_queued_exec = function (self)
	return self.__exec
end
CoreSetup.__pre_init = function (self)
	if Application:editor() then
		managers.global_texture = CoreGTextureManager.GTextureManager:new()
		local frame_resolution = SystemInfo:desktop_resolution()
		local appwin_resolution = Vector3(frame_resolution.x*0.75, frame_resolution.y*0.75, 0)
		local frame = EWS:Frame("World Editor", Vector3(0, 0, 0), frame_resolution, "CAPTION,CLOSE_BOX,MINIMIZE_BOX,MAXIMIZE_BOX,MAXIMIZE,SYSTEM_MENU,RESIZE_BORDER")

		frame.set_icon(frame, CoreEWS.image_path("world_editor_16x16.png"))

		local frame_panel = EWS:Panel(frame, "", "")
		local appwin = EWS:AppWindow(frame_panel, appwin_resolution, "SUNKEN_BORDER")

		appwin.set_max_size(appwin, Vector3(-1, -1, 0))

		Global.application_window = appwin
		Global.frame = frame

		appwin.connect(appwin, "EVT_LEAVE_WINDOW", callback(nil, _G, "leaving_window"))
		appwin.connect(appwin, "EVT_ENTER_WINDOW", callback(nil, _G, "entering_window"))
		appwin.connect(appwin, "EVT_KILL_FOCUS", callback(nil, _G, "kill_focus"))
		Application:set_ews_window(appwin)

		local top_sizer = EWS:BoxSizer("VERTICAL")
		local main_sizer = EWS:BoxSizer("HORIZONTAL")
		local left_toolbar_sizer = EWS:BoxSizer("VERTICAL")

		main_sizer.add(main_sizer, left_toolbar_sizer, 0, 0, "EXPAND")

		local app_sizer = EWS:BoxSizer("VERTICAL")

		main_sizer.add(main_sizer, app_sizer, 4, 0, "EXPAND")
		app_sizer.add(app_sizer, appwin, 5, 0, "EXPAND")
		top_sizer.add(top_sizer, main_sizer, 1, 0, "EXPAND")
		frame_panel.set_sizer(frame_panel, top_sizer)

		Global.main_sizer = main_sizer
		Global.v_sizer = app_sizer
		Global.frame_panel = frame_panel
		Global.left_toolbar_sizer = left_toolbar_sizer
	end

	return 
end
CoreSetup.__init = function (self)
	self.init_category_print(self)

	if not PackageManager:loaded("core/packages/base") then
		PackageManager:load("core/packages/base")
	end

	managers.global_texture = managers.global_texture or CoreGTextureManager.GTextureManager:new()

	if not Global.__coresetup_bootdone then
		self.start_boot_loading_screen(self)

		Global.__coresetup_bootdone = true
	end

	self.load_packages(self)
	World:set_raycast_bounds(Vector3(-50000, -80000, -20000), Vector3(90000, 50000, 30000))
	World:load((Application:editor() and "core/levels/editor/editor") or "core/levels/zone", false)
	min_exe_version("1.0.0.7000", "Core Systems")
	rawset(_G, "UnitDamage", rawget(_G, "UnitDamage") or CoreUnitDamage)
	rawset(_G, "EditableGui", rawget(_G, "EditableGui") or CoreEditableGui)

	local aspect_ratio = nil

	if Application:editor() then
		local frame_resolution = SystemInfo:desktop_resolution()
		aspect_ratio = frame_resolution.x/frame_resolution.y
	elseif SystemInfo:platform() == Idstring("WIN32") then
		aspect_ratio = RenderSettings.aspect_ratio

		if aspect_ratio == 0 then
			aspect_ratio = RenderSettings.resolution.x/RenderSettings.resolution.y
		end
	elseif SystemInfo:platform() == Idstring("X360") or (SystemInfo:platform() == Idstring("PS3") and SystemInfo:widescreen()) then
		aspect_ratio = RenderSettings.resolution.x/RenderSettings.resolution.y
	else
		aspect_ratio = RenderSettings.resolution.x/RenderSettings.resolution.y
	end

	if Application:ews_enabled() then
		managers.database = CoreDatabaseManager.DatabaseManager:new()
	end

	managers.localization = CoreLocalizationManager.LocalizationManager:new()
	managers.controller = CoreControllerManager.ControllerManager:new()
	managers.slot = CoreSlotManager.SlotManager:new()
	managers.listener = CoreListenerManager.ListenerManager:new()
	managers.viewport = CoreViewportManager.ViewportManager:new(aspect_ratio)
	managers.mission = CoreMissionManager.MissionManager:new()
	managers.expression = CoreExpressionManager.ExpressionManager:new()
	managers.worldcamera = CoreWorldCameraManager:new()
	managers.environment_effects = CoreEnvironmentEffectsManager.EnvironmentEffectsManager:new()
	managers.shape = CoreShapeManager.ShapeManager:new()
	managers.portal = CorePortalManager.PortalManager:new()
	managers.sound_environment = CoreSoundEnvironmentManager:new()
	managers.environment_area = CoreEnvironmentAreaManager.EnvironmentAreaManager:new()
	managers.cutscene = CoreCutsceneManager:new()
	managers.rumble = CoreRumbleManager.RumbleManager:new()
	managers.DOF = CoreDOFManager.DOFManager:new()
	managers.subtitle = CoreSubtitleManager.SubtitleManager:new()
	managers.overlay_effect = CoreOverlayEffectManager.OverlayEffectManager:new()
	managers.sequence = CoreSequenceManager.SequenceManager:new()
	managers.camera = CoreCameraManager.CameraTemplateManager:new()
	managers.slave = CoreSlaveManager.SlaveManager:new()
	managers.world_instance = CoreWorldInstanceManager:new()
	managers.environment_controller = CoreEnvironmentControllerManager:new()
	managers.helper_unit = CoreHelperUnitManager.HelperUnitManager:new()
	self._input = CoreInputManager.InputManager:new()
	self._session = CoreSessionManager.SessionManager:new(self.session_factory, self._input)
	self._smoketest = CoreSmoketestManager.Manager:new(self._session:session())

	managers.sequence:internal_load()
	self.init_managers(self, managers)

	if Application:ews_enabled() then
		managers.news = CoreNewsReportManager.NewsReportManager:new()
		managers.toolhub = CoreToolHub.ToolHub:new()

		managers.toolhub:add("Environment Editor", CoreEnvEditor)
		managers.toolhub:add(CoreLuaProfilerViewer.TOOLHUB_NAME, CoreLuaProfilerViewer.LuaProfilerViewer)
		managers.toolhub:add(CoreMaterialEditor.TOOLHUB_NAME, CoreMaterialEditor)
		managers.toolhub:add("LUA Profiler", CoreLuaProfiler)
		managers.toolhub:add("Particle Editor", CoreParticleEditor)
		managers.toolhub:add(CorePuppeteer.EDITOR_TITLE, CorePuppeteer)
		managers.toolhub:add(CoreCutsceneEditor.EDITOR_TITLE, CoreCutsceneEditor)

		if not Application:editor() then
			managers.toolhub:add("Unit Reloader", CoreUnitReloader)
		end

		self.init_toolhub(self, managers.toolhub)
		managers.toolhub:buildmenu()
	end

	self.__gsm = assert(self.init_game(self), "self:init_game must return a GameStateMachine.")

	managers.cutscene:post_init()
	self._smoketest:post_init()

	slot2 = Application:editor() or slot2

	self.init_finalize(self)

	return 
end
CoreSetup.__destroy = function (self)
	self.destroy(self)
	self.__gsm:destroy()
	managers.global_texture:destroy()
	managers.cutscene:destroy()
	managers.subtitle:destroy()
	managers.viewport:destroy()
	managers.worldcamera:destroy()
	managers.overlay_effect:destroy()
	self._session:destroy()
	self._input:destroy()
	self._smoketest:destroy()

	return 
end
CoreSetup.loading_update = function (self, t, dt)
	return 
end
CoreSetup.__update = function (self, t, dt)
	if self.__firstupdate then
		self.stop_loading_screen(self)

		self.__firstupdate = false
	end

	managers.controller:update(t, dt)
	managers.cutscene:update()
	managers.sequence:update(t, dt)
	managers.worldcamera:update(t, dt)
	managers.environment_effects:update(t, dt)
	managers.sound_environment:update(t, dt)
	managers.environment_area:update(t, dt)
	managers.expression:update(t, dt)
	managers.subtitle:update(TimerManager:game_animation():time(), TimerManager:game_animation():delta_time())
	managers.overlay_effect:update(t, dt)
	managers.viewport:update(t, dt)
	managers.mission:update(t, dt)
	managers.slave:update(t, dt)
	self._session:update(t, dt)
	self._input:update(t, dt)
	self._smoketest:update(t, dt)
	managers.environment_controller:update(t, dt)
	self.update(self, t, dt)

	return 
end
CoreSetup.__paused_update = function (self, t, dt)
	managers.viewport:paused_update(t, dt)

	if SystemInfo:platform() == Idstring("XB1") then
		managers.controller:update(t, dt)
	else
		managers.controller:paused_update(t, dt)
	end

	managers.cutscene:paused_update(t, dt)
	managers.overlay_effect:paused_update(t, dt)
	managers.slave:paused_update(t, dt)
	self._session:update(t, dt)
	self._input:update(t, dt)
	self._smoketest:update(t, dt)
	self.paused_update(self, t, dt)

	return 
end
CoreSetup.__end_update = function (self, t, dt)
	managers.camera:update(t, dt)
	self._session:end_update(t, dt)
	self.end_update(self, t, dt)
	self.__gsm:end_update(t, dt)
	managers.viewport:end_update(t, dt)
	managers.controller:end_update(t, dt)
	managers.DOF:update(t, dt)

	if Application:ews_enabled() then
		managers.toolhub:end_update(t, dt)
	end

	return 
end
CoreSetup.__paused_end_update = function (self, t, dt)
	self.paused_end_update(self, t, dt)
	self.__gsm:end_update(t, dt)
	managers.DOF:paused_update(t, dt)

	return 
end
CoreSetup.__render = function (self)
	managers.portal:render()
	managers.viewport:render()
	managers.overlay_effect:render()
	self.render(self)

	return 
end
CoreSetup.__end_frame = function (self, t, dt)
	self.end_frame(self, t, dt)
	managers.viewport:end_frame(t, dt)

	if self.__quit then
		if not self.block_quit(self) then
			CoreEngineAccess._quit()
		end
	elseif self.__exec and not self.block_exec(self) then
		if managers.network and managers.network:session() then
			managers.network:save()
		end

		if managers.mission then
			managers.mission:destroy()
		end

		if managers.menu_scene then
			managers.menu_scene:pre_unload()
		end

		World:unload_all_units()

		if managers.menu_scene then
			managers.menu_scene:unload()
		end

		if managers.blackmarket then
			managers.blackmarket:release_preloaded_blueprints()
		end

		if managers.dyn_resource and not managers.dyn_resource:is_ready_to_close() then
			Application:cleanup_thread_garbage()
			managers.dyn_resource:update()
		end

		if managers.sound_environment then
			managers.sound_environment:destroy()
		end

		TextureCache:abort_all_script_requests()
		self.start_loading_screen(self)
		managers.dyn_resource:set_file_streaming_chunk_size_mul(0.1, 10)

		if managers.worlddefinition then
			managers.worlddefinition:unload_packages()
		end

		self.unload_packages(self)
		managers.menu:destroy()
		Overlay:newgui():destroy_all_workspaces()
		Application:cleanup_thread_garbage()
		CoreEngineAccess._exec("core/lib/CoreEntry", self.__context)
	end

	return 
end
CoreSetup.__loading_update = function (self, t, dt)
	self._session:update(t, dt)
	self.loading_update(self)

	return 
end
CoreSetup.__animations_reloaded = function (self)
	return 
end
CoreSetup.__script_reloaded = function (self)
	return 
end
CoreSetup.__entering_window = function (self, user_data, event_object)
	if Global.frame:is_active() then
		Global.application_window:set_focus()
		Input:keyboard():acquire()
	end

	return 
end
CoreSetup.__leaving_window = function (self, user_data, event_object)
	if not managers.editor or managers.editor._in_mixed_input_mode then
		Input:keyboard():unacquire()
	end

	return 
end
CoreSetup.__kill_focus = function (self, user_data, event_object)
	if managers.editor and not managers.editor:in_mixed_input_mode() and not Global.running_simulation then
		managers.editor:set_in_mixed_input_mode(true)
	end

	return 
end
CoreSetup.__save = function (self, data)
	self.save(self, data)

	return 
end
CoreSetup.__load = function (self, data)
	self.load(self, data)

	return 
end

core:module("CoreSetup")

CoreSetup = _CoreSetup
CoreSetup.make_entrypoint = function (self)
	if not _G.CoreSetup.__entrypoint_is_setup then
		assert(rawget(_G, "pre_init") == nil)
		assert(rawget(_G, "init") == nil)
		assert(rawget(_G, "destroy") == nil)
		assert(rawget(_G, "update") == nil)
		assert(rawget(_G, "end_update") == nil)
		assert(rawget(_G, "paused_update") == nil)
		assert(rawget(_G, "paused_end_update") == nil)
		assert(rawget(_G, "render") == nil)
		assert(rawget(_G, "end_frame") == nil)
		assert(rawget(_G, "animations_reloaded") == nil)
		assert(rawget(_G, "script_reloaded") == nil)
		assert(rawget(_G, "entering_window") == nil)
		assert(rawget(_G, "leaving_window") == nil)
		assert(rawget(_G, "kill_focus") == nil)
		assert(rawget(_G, "save") == nil)
		assert(rawget(_G, "load") == nil)

		_G.CoreSetup.__entrypoint_is_setup = true
	end

	rawset(_G, "pre_init", callback(self, self, "__pre_init"))
	rawset(_G, "init", callback(self, self, "__init"))
	rawset(_G, "destroy", callback(self, self, "__destroy"))
	rawset(_G, "update", callback(self, self, "__update"))
	rawset(_G, "end_update", callback(self, self, "__end_update"))
	rawset(_G, "loading_update", callback(self, self, "__loading_update"))
	rawset(_G, "paused_update", callback(self, self, "__paused_update"))
	rawset(_G, "paused_end_update", callback(self, self, "__paused_end_update"))
	rawset(_G, "render", callback(self, self, "__render"))
	rawset(_G, "end_frame", callback(self, self, "__end_frame"))
	rawset(_G, "animations_reloaded", callback(self, self, "__animations_reloaded"))
	rawset(_G, "script_reloaded", callback(self, self, "__script_reloaded"))
	rawset(_G, "entering_window", callback(self, self, "__entering_window"))
	rawset(_G, "leaving_window", callback(self, self, "__leaving_window"))
	rawset(_G, "kill_focus", callback(self, self, "__kill_focus"))
	rawset(_G, "save", callback(self, self, "__save"))
	rawset(_G, "load", callback(self, self, "__load"))

	return 
end

return 
