function class(...)
	local super = ...

	if 1 <= select("#", ...) and super == nil then
		error("trying to inherit from nil", 2)
	end

	local class_table = {
		super = super,
		__index = class_table
	}

	setmetatable(class_table, super)

	class_table.new = function (klass, ...)
		local object = {}

		setmetatable(object, class_table)

		if object.init then
			return object, object.init(object, ...)
		end

		return object
	end

	return class_table
end

function callback(o, base_callback_class, base_callback_func_name, base_callback_param)
	if base_callback_class and base_callback_func_name and base_callback_class[base_callback_func_name] then
		if base_callback_param ~= nil then
			if o then
				return function (...)
					return base_callback_class[base_callback_func_name](o, base_callback_param, ...)
				end
			else
				return function (...)
					return base_callback_class[base_callback_func_name](base_callback_param, ...)
				end
			end
		elseif o then
			return function (...)
				return base_callback_class[base_callback_func_name](o, ...)
			end
		else
			return function (...)
				return base_callback_class[base_callback_func_name](...)
			end
		end
	elseif base_callback_class then
		if base_callback_class then
			local class_name = CoreDebug.class_name(getmetatable(base_callback_class) or base_callback_class)
		end

		error("Callback on class \"" .. tostring(class_name) .. "\" refers to a non-existing function \"" .. tostring(base_callback_func_name) .. "\".")
	elseif base_callback_func_name then
		error("Callback to function \"" .. tostring(base_callback_func_name) .. "\" is on a nil class.")
	else
		error("Callback class and function was nil.")
	end

	return 
end

CoreLoadingSetup = CoreLoadingSetup or class()
CoreLoadingSetup.init = function (self)
	return 
end
CoreLoadingSetup.init_managers = function (self, managers)
	return 
end
CoreLoadingSetup.init_gp = function (self)
	return 
end
CoreLoadingSetup.post_init = function (self)
	return 
end
CoreLoadingSetup.update = function (self, t, dt)
	return 
end
CoreLoadingSetup.destroy = function (self)
	return 
end
CoreLoadingSetup.__init = function (self)
	self.init_managers(self, managers)
	self.init_gp(self)
	self.post_init(self)

	return 
end
CoreLoadingSetup.__update = function (self, t, dt)
	self.update(self, t, dt)

	return 
end
CoreLoadingSetup.__destroy = function (self, t, dt)
	self.destroy(self)

	return 
end
CoreLoadingSetup.make_entrypoint = function (self)
	rawset(_G, "init", callback(self, self, "__init"))
	rawset(_G, "update", callback(self, self, "__update"))
	rawset(_G, "destroy", callback(self, self, "__destroy"))

	return 
end

return 
