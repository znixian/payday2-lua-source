#!/usr/bin/ruby
require 'find'
require 'set'

scripts = Set.new

Find.find("result") do |path|
	next if File.directory? path
	next unless path.end_with? ".lua"

	File.readlines(path).each do |line|
		match = /require\("(.*)"\)/.match(line)
		next unless match

		script = match[1]
		next unless script

		scripts << script.downcase
	end

	# Make sure all the scripts end up in there
	name = /result\/(.*).lua/.match(path)[1]
	scripts << name
end

mappings = {}

scripts.each do |name|
	hash = `./idstring "#{name}"`.strip
	mappings[hash] = name
	#puts "#{hash}: #{name}"
end

def check_parent child
	parent = File.expand_path("..", child)
	unless File.directory? parent
		check_parent parent
		puts "Creating #{parent}"
		Dir.mkdir parent
	end
end

def process_unknown_file mappings, hash, input
	name = mappings[hash]
	unless name
		puts "Missing hash for #{hash}"
		return
	end

	filename = "result/#{name}.lua"
	puts "#{hash}: #{input} -> #{filename}"

	check_parent filename

	File.rename input, filename
end

Dir["result/*.lua"].each do |input|
	hash = /result\/(.*).lua/.match input
	next unless hash
	hash = hash[1]
	next unless hash

	process_unknown_file mappings, hash, input
end

Dir["result/hashed/*.lua"].each do |input|
	hash = /result\/hashed\/(.*).lua/.match input
	next unless hash
	hash = hash[1]
	next unless hash

	process_unknown_file mappings, hash, input
end

File.open("result/dictionary.txt", 'w') do |file|
	mappings.each_pair do |hash, name|
		file.write("#{hash}: #{name}\n")
	end
end


