# PAYDAY 2 Lua Sources
VR Branch

This is a dump of all the Lua sources from PAYDAY 2. Either look around inside the 'result' folder, or
run ./install on a GNU+Linux machine to decompile the sources yourself.

Many sources failed to decompile for whatever reason - they're listed in `result/failed.txt`. If you
know a lot about LuaJIT and can be bothered, I'd gladly welcome any information about this and can provide
error logs.

