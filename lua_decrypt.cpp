// From https://www.reddit.com/r/paydaytheheistmods/comments/4bw4d1/most_recent_lua_dump/d1db4h2/
// Thanks, Snh20 and Yaph1l

#include <cstdio>
#include <new>
#include <string.h>

// Thanks to YaPh1l for the key
static const char* key = "asljfowk4_5u2333crj";

static const char* pExtension = ".luac";

int main(int argC, const char* argV[])
{
	if (argC < 2)
	{
		printf("Usage: %s <input filename>\n", argV[0]);
		return 1;
	}

	FILE* pFile = fopen(argV[1], "rb");
	if (!pFile)
	{
		fprintf(stderr, "Error: Failed to open file \"%s\" for reading, aborting\n", argV[1]);
		return 1;
	}

	int errcode = fseek(pFile, 0, SEEK_END);
	if (errcode != 0)
	{
		fprintf(stderr, "Error: Failed to seek to end of file \"%s\" (%d), aborting\n", argV[1], errcode);
		fclose(pFile);
		return 1;
	}

	int iFileSize = ftell(pFile);
	if (iFileSize < 1)
	{
		fprintf(stderr, "Error: The input file \"%s\" is empty or has an invalid size, aborting\n", argV[1]);
		fclose(pFile);
		return 1;
	}
	size_t uSize = static_cast<size_t>(iFileSize);

	fseek(pFile, 0, SEEK_SET);

	if (uSize < 6)
	{
		fprintf(stderr, "Error: The input file is abnormally small (\"%lu\" bytes), aborting\n", uSize);
		fclose(pFile);
		return 1;
	}

	char* pBuf = new (std::nothrow) char[uSize];
	if (!pBuf)
	{
		fprintf(stderr, "Error: Failed to allocate \"%lu\" bytes (file data), aborting\n", uSize);
		fclose(pFile);
		return 1;
	}

	fread(pBuf, sizeof(char), uSize, pFile);
	fclose(pFile);
	pFile = NULL;

	if (!strncmp(pBuf + 1, "LuaQ", 4))
	{
		fprintf(stderr, "Error: The input file \"%s\" has already been decrypted, aborting\n", argV[1]);
		return 1;
	}

#ifndef PASSTHROUGHTEST
	// Credit goes to MMavipc for this snippet
	int keyIndex = 0;
	for (int i = 0; i < iFileSize; ++i)
	{
		keyIndex = ((iFileSize+i)*7)%strlen(key);
		pBuf[i] ^= key[keyIndex]*(iFileSize-i);
	}
#endif	// !PASSTHROUGHTEST

// 	if (strncmp(pBuf + 1, "LuaQ", 4))
// 		fprintf(stderr, "Warning: The decrypted output is lacking the Lua signature, but proceeding with write to file regardless\n");
// 
// 	size_t uFileNameLen = strlen(argV[1]) + 1;
// 	if (!uFileNameLen || uFileNameLen > (static_cast<size_t>(MAX_PATH) - strlen(pExtension)))
// 	{
// 		fprintf(stderr, "Error: Failed to get filename length or input filename is too long, aborting\n");
// 		delete[] pBuf;
// 		return 1;
// 	}
// 
// 	char* pInputFileNameTmp = new (std::nothrow) char[MAX_PATH];
// 	if (!pInputFileNameTmp)
// 	{
// 		fprintf(stderr, "Error: Failed to allocate \"%u\" bytes (input filename), aborting\n", MAX_PATH);
// 		delete[] pBuf;
// 		return 1;
// 	}
// 	strncpy(pInputFileNameTmp, argV[1], uFileNameLen);
// 	pInputFileNameTmp[uFileNameLen - 1] = '\0';
// 
// 	for (size_t i = uFileNameLen - 2; i >= 0; --i)
// 	{
// 		if (pInputFileNameTmp[i] != '.')
// 		{
// 			if (pInputFileNameTmp[i] == '/' || pInputFileNameTmp[i] == '\\')
// 			{
// 				// Found a path separator instead of a period (i.e. filename has no extension), just tack the extension on behind
// 				break;
// 			}
// 		}
// 		else
// 		{
// 			pInputFileNameTmp[i] = '\0';
// 			break;
// 		}
// 	}
// 
// 	char* pOutputFileName = new (std::nothrow) char[MAX_PATH];
// 	if (!pOutputFileName)
// 	{
// 		fprintf(stderr, "Error: Failed to allocate \"%u\" bytes (output filename), aborting\n", MAX_PATH);
// 		delete[] pInputFileNameTmp;
// 		delete[] pBuf;
// 		return 1;
// 	}
// 
// 	_snprintf(pOutputFileName, MAX_PATH, "%s%s", pInputFileNameTmp, pExtension);

	const char* pOutputFileName = "decrypted.luac";
	pFile = fopen(pOutputFileName, "wb+");
	if (!pFile)
	{
		fprintf(stderr, "Error: Failed to open file \"%s\" for writing, aborting\n", pOutputFileName);
// 		delete[] pOutputFileName;
// 		delete[] pInputFileNameTmp;
		delete[] pBuf;
		return 1;
	}

	fwrite(pBuf, sizeof(char), uSize, pFile);

	fclose(pFile);
	pFile = NULL;

	printf("Decryption complete, written to output file \"%s\"\n", pOutputFileName);

// 	delete[] pOutputFileName;
// 	delete[] pInputFileNameTmp;
	delete[] pBuf;

	return 0;
}
